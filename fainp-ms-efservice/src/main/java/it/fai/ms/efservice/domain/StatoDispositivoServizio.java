package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.listener.StatoDispositivoServizioEntityListener;

/**
 * A StatoDispositivoServizio.
 */
@Entity
@EntityListeners({ StatoDispositivoServizioEntityListener.class })
@Audited(withModifiedFlag = true, targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "stato_dispositivo_servizio")
public class StatoDispositivoServizio implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "pan")
  private String pan;

  @Column(name = "data_attivazione")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataAttivazione;

  @Column(name = "data_disattivazione")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataDisattivazione;

  @Column(name = "data_scadenza")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataScadenza;

  @Enumerated(EnumType.STRING)
  @Column(name = "stato")
  private StatoDS stato;

  @OneToOne
  @JoinColumn
  private TipoServizio tipoServizio;

  @ManyToOne
  @JsonIgnore
  private Dispositivo dispositivo;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPan() {
    return pan;
  }

  public StatoDispositivoServizio pan(String pan) {
    this.pan = pan;
    return this;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public Instant getDataAttivazione() {
    return dataAttivazione;
  }

  public StatoDispositivoServizio dataAttivazione(Instant dataAttivazione) {
    this.dataAttivazione = dataAttivazione;
    return this;
  }

  public void setDataAttivazione(Instant dataAttivazione) {
    this.dataAttivazione = dataAttivazione;
  }

  public Instant getDataDisattivazione() {
    return dataDisattivazione;
  }

  public StatoDispositivoServizio dataDisattivazione(Instant dataDisattivazione) {
    this.dataDisattivazione = dataDisattivazione;
    return this;
  }

  public void setDataDisattivazione(Instant dataDisattivazione) {
    this.dataDisattivazione = dataDisattivazione;
  }

  public Instant getDataScadenza() {
    return dataScadenza;
  }

  public StatoDispositivoServizio dataScadenza(Instant dataScadenza) {
    this.dataScadenza = dataScadenza;
    return this;
  }

  public void setDataScadenza(Instant dataScadenza) {
    this.dataScadenza = dataScadenza;
  }

  public StatoDS getStato() {
    return stato;
  }

  public StatoDispositivoServizio stato(StatoDS stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(StatoDS stato) {
    this.stato = stato;
  }

  public TipoServizio getTipoServizio() {
    return tipoServizio;
  }

  public StatoDispositivoServizio tipoServizio(TipoServizio tipoServizio) {
    this.tipoServizio = tipoServizio;
    return this;
  }

  public void setTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public Dispositivo getDispositivo() {
    return dispositivo;
  }

  public StatoDispositivoServizio dispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
    return this;
  }

  public void setDispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
  }
  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatoDispositivoServizio statoDispositivoServizio = (StatoDispositivoServizio) o;
    if (statoDispositivoServizio.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), statoDispositivoServizio.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("StatoDispositivoServizio [id=")
           .append(id)
           .append(", pan=")
           .append(pan)
           .append(", dataAttivazione=")
           .append(dataAttivazione)
           .append(", dataDisattivazione=")
           .append(dataDisattivazione)
           .append(", dataScadenza=")
           .append(dataScadenza)
           .append(", stato=")
           .append(stato)
           .append(", tipoServizio=")
           .append(tipoServizio)
           .append(", dispositivo=")
           .append(dispositivo)
           .append("]");
    return builder.toString();
  }

}
