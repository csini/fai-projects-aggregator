package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the IndirizzoSpedizioneOrdini entity. This class is used in IndirizzoSpedizioneOrdiniResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /indirizzo-spedizione-ordinis?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class IndirizzoSpedizioneOrdiniCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter via;

    private StringFilter citta;

    private StringFilter cap;

    private StringFilter provincia;

    private StringFilter paese;

    private BooleanFilter fai;

    public IndirizzoSpedizioneOrdiniCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getVia() {
        return via;
    }

    public void setVia(StringFilter via) {
        this.via = via;
    }

    public StringFilter getCitta() {
        return citta;
    }

    public void setCitta(StringFilter citta) {
        this.citta = citta;
    }

    public StringFilter getCap() {
        return cap;
    }

    public void setCap(StringFilter cap) {
        this.cap = cap;
    }

    public StringFilter getProvincia() {
        return provincia;
    }

    public void setProvincia(StringFilter provincia) {
        this.provincia = provincia;
    }

    public StringFilter getPaese() {
        return paese;
    }

    public void setPaese(StringFilter paese) {
        this.paese = paese;
    }

    public BooleanFilter getFai() {
        return fai;
    }

    public void setFai(BooleanFilter fai) {
        this.fai = fai;
    }

    @Override
    public String toString() {
        return "IndirizzoSpedizioneOrdiniCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (via != null ? "via=" + via + ", " : "") +
                (citta != null ? "citta=" + citta + ", " : "") +
                (cap != null ? "cap=" + cap + ", " : "") +
                (provincia != null ? "provincia=" + provincia + ", " : "") +
                (paese != null ? "paese=" + paese + ", " : "") +
                (fai != null ? "fai=" + fai + ", " : "") +
            "}";
    }

}
