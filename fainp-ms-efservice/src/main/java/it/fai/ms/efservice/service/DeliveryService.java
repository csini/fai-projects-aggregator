package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.dml.document.DeliveryPrintProduttoreRequestDTO;
import it.fai.ms.common.dml.document.DeliveryPrintRequestDTO;
import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;

@Service
@Transactional
public class DeliveryService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  private final DocumentClient                   documentClient;
  private final ProcessTransitiService           transitiService;
  private final RichiestaRepositoryExt           richiestaRepository;
  private final DispositivoRepositoryExt         dispositivoRepository;
  private final DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService;
  private final VehicleClient                    vehicleClient;
  private final DeliveryDeviceService            deliveryDeviceService;
  private final FsmDispositivo                   fsmDispositivo;

  public DeliveryService(DocumentClient documentClient, ProcessTransitiService transitiService, RichiestaRepositoryExt richiestaRepository,
                         DispositivoRepositoryExt dispositivoRepository, DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService,
                         VehicleClient vehicleClient, DeliveryDeviceService deliveryDeviceService, final FsmDispositivo fsmDispositivo) {
    this.documentClient = documentClient;
    this.transitiService = transitiService;
    this.richiestaRepository = richiestaRepository;
    this.dispositivoRepository = dispositivoRepository;
    this.dispositiviDaSpedireCacheService = dispositiviDaSpedireCacheService;
    this.vehicleClient = vehicleClient;
    this.deliveryDeviceService = deliveryDeviceService;
    this.fsmDispositivo = fsmDispositivo;
  }

  private Boolean canBePrinted(Richiesta richiesta) {
    log.debug("Checking richiesta if can be printed id: {}", richiesta.getId());
    Optional<Dispositivo> dispositivoOp = richiesta.getDispositivos()
                                                   .stream()
                                                   .findFirst();

    if (dispositivoOp.isPresent()) {
      Dispositivo dispositivo = dispositivoOp.get();
      Boolean canBePrinted = BooleanUtils.isTrue(dispositiviDaSpedireCacheService.get(dispositivo.getIdentificativo()));

      if (canBePrinted) {
        log.debug("Richiesta with ID: {}, can be printed because is forced", richiesta.getId());
        return true;
      }

      Set<AssociazioneDV> associazioneDispositivoVeicolos = dispositivo.getAssociazioneDispositivoVeicolos();
      if (associazioneDispositivoVeicolos == null || associazioneDispositivoVeicolos.isEmpty()) {
        log.info("Not found associazione DVs on Device: {}", dispositivo.getIdentificativo());
        return true;
      } else {
        List<String> uuidsVehicle = associazioneDispositivoVeicolos.parallelStream()
                                                                   .filter(adv -> adv != null
                                                                                  && StringUtils.isNotBlank(adv.getUuidVeicolo()))
                                                                   .map(AssociazioneDV::getUuidVeicolo)
                                                                   .collect(toList());

        if (uuidsVehicle == null || uuidsVehicle.isEmpty()) {
          log.info("Not found UUID vehicle related on Device: {}", dispositivo.getIdentificativo());
          return true;
        } else {
          List<VehicleDTO> issues = vehicleClient.findVehicleWithoutBooklet(jwt, uuidsVehicle);
          Boolean hasIssues = issues != null && issues.isEmpty();
          canBePrinted = !hasIssues;
          log.debug("Richiesta with ID: {} has issues: {}", richiesta.getId(), hasIssues);
          return canBePrinted;
        }
      }
    }
    log.debug("Richiesta has not dispositivo and cannot be printend");
    return false;
  }

  @Transactional
  public AsyncJobResponse process(DeliveryPrintDTO dto) {
    log.info("processing print request with dto: {}", dto);

    DeliveryPrintRequestDTO deliveryPrintRequestDTO = new DeliveryPrintRequestDTO();

    LocalDate dataSpedizioni = LocalDateTime.ofInstant(dto.getDataSpedizione(), ZoneOffset.UTC)
                                            .toLocalDate();
    deliveryPrintRequestDTO.setDeliveryDate(dataSpedizioni);
    deliveryPrintRequestDTO.setMultiDispositivo(true);
    deliveryPrintRequestDTO.setCompanyGroups(new ArrayList<>());

    String tipoOperazioneSpedizione = RichiestaEvent.MU_SPEDIZIONE_COMPLETA.name();

    List<String> spedizioni = dto.getSpedizioni();
    log.info("Spedizioni: {}", spedizioni);
    if (spedizioni == null) {
      richiestaRepository.findAllByStato(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
                         .stream()
                         .filter(this::canBePrinted)
                         .collect(Collectors.groupingBy(req -> {
                           return req.getContratto()
                                     .getClienteFai()
                                     .getCodiceCliente();
                         }))
                         .forEach((codiceCliente, richieste) -> {
                           changeRichiestaStatusAndDeliveryDate(dto.getDataSpedizione(), richieste, tipoOperazioneSpedizione);

                           deliveryDeviceService.getDeliveryDtoByRequestsCustomer(codiceCliente, richieste, deliveryPrintRequestDTO);
                         });
    } else {
      spedizioni.stream()
                .forEach(codiceCliente -> {
                  List<Richiesta> richieste = richiestaRepository.findAllByCodiceClienteFaiAndStatoRichiesta(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI,
                                                                                                             codiceCliente)
                                                                 .stream()
                                                                 .filter(this::canBePrinted)
                                                                 .collect(Collectors.toList());

                  changeRichiestaStatusAndDeliveryDate(dto.getDataSpedizione(), richieste, tipoOperazioneSpedizione);

                  deliveryDeviceService.getDeliveryDtoByRequestsCustomer(codiceCliente, richieste, deliveryPrintRequestDTO);
                });
    }

    log.info("Finish proccess of print request, asking for MSdocument to print pdf : {}", deliveryPrintRequestDTO);
    Map<String, String> documentResponse = documentClient.printDelivery(jwt, deliveryPrintRequestDTO);

    return new AsyncJobResponse(StateChangeStatusFsm.SUCCESS, documentResponse);
  }

  @Transactional
  public AsyncJobResponse processProduttore(DeliveryPrintDTO dto) {
    log.info("processing Produttore print request with dto : {}", dto);

    final Instant deliveryDate = dto.getDataSpedizione();
    LocalDate dataSpedizioni = LocalDateTime.ofInstant(deliveryDate, ZoneOffset.UTC)
                                            .toLocalDate();

    DeliveryPrintProduttoreRequestDTO deliveryPrintProduttoreRequestDTO = new DeliveryPrintProduttoreRequestDTO();
    deliveryPrintProduttoreRequestDTO.setDeliveryDate(dataSpedizioni);

    List<Dispositivo> devicesToDelivery = new ArrayList<>();
    if (dto.getSpedizioni() == null) {

      List<Dispositivo> devices = dispositivoRepository.findByStatoIn(Arrays.asList(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE));

      changeStatusToDeviceAndSetDelivery(devices, deliveryDate);
      devicesToDelivery.addAll(devices);
    } else {
      dto.getSpedizioni()
         .forEach(tipoDispositivo -> {
           TipoDispositivoEnum deviceType = TipoDispositivoEnum.valueOf(tipoDispositivo);
           log.debug("Managed request to device type: {}", deviceType);

           List<Dispositivo> devices = dispositivoRepository.findByStatoInAndTipoDispositivo_nome(Arrays.asList(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE),
                                                                                                  deviceType);

           changeStatusToDeviceAndSetDelivery(devices, deliveryDate);
           devicesToDelivery.addAll(devices);
         });
    }

    log.info("Start generate DTO to send information to faiDocument");
    deliveryDeviceService.getDeliveryDtoByProduttore(devicesToDelivery, deliveryPrintProduttoreRequestDTO);
    log.info("Finished generate DTO...");

    log.info("Feign printDeliveryProduttore : {}", deliveryPrintProduttoreRequestDTO);
    Map<String, String> documentResponse = documentClient.printDeliveryProduttore(jwt, deliveryPrintProduttoreRequestDTO);

    return new AsyncJobResponse(StateChangeStatusFsm.SUCCESS, documentResponse);
  }

  private void changeStatusToDeviceAndSetDelivery(List<Dispositivo> devices, Instant deliveryDate) {

    if (devices == null || devices.isEmpty()) {
      log.error("Not device to change status");
      throw new IllegalArgumentException("Not device to change status");
    }

    devices.forEach(d -> {
      Set<Richiesta> richiestas = d.getRichiestas();
      
      if (richiestas == null || richiestas.isEmpty()) {
        changeStatusDeviceAndSetDeliveryDate(d, deliveryDate);
      } else {
        List<Richiesta> richiesteModifica = richiestas.stream()
                                                      .filter(r -> r.getStato()
                                                                     .equals(StatoRichiesta.DISPOSITIVO_DISATTIVATO))
                                                      .collect(toList());
        if (richiesteModifica == null || richiesteModifica.isEmpty()) {
          changeStatusDeviceAndSetDeliveryDate(d, deliveryDate);
        } else {
          setDeliveryDateOnDevice(deliveryDate, d);
          Set<String> identifierRichieste = richiestas.stream()
                                                      .map(Richiesta::getIdentificativo)
                                                      .collect(toSet());
          transitiService.changeRichiesteStatus(identifierRichieste, RichiestaEvent.MU_LETTERA_VETTURA.name());
          log.info("Changed status on Richieste: {}", identifierRichieste);
        }
      }
    });

  }

  private void changeStatusDeviceAndSetDeliveryDate(Dispositivo d, Instant deliveryDate) {
    try {
      d = fsmDispositivo.executeCommandToChangeState(DispositivoEvent.RIENTRO, d);
      log.debug("Updated status by FSM: {}", d);
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }

    d = dispositivoRepository.save(d.dataSpedizione(deliveryDate));
    log.info("Changed status and update Delivery Date: Identificativo: {} - Stato: {} - DataSpedizione: {}", d.getIdentificativo(),
             d.getStato(), d.getDataSpedizione());

  }

  private void setDeliveryDateOnDevice(Instant deliveryDate, Dispositivo device) {
    device.setDataSpedizione(deliveryDate);
    device = dispositivoRepository.save(device);
    Instant deliveryDateDevice = (device != null) ? device.getDataSpedizione() : null;
    String identifierDevice = (device != null) ? device.getIdentificativo() : null;
    log.info("Update deliveryDate [{}] to device: {}", deliveryDateDevice, identifierDevice);
  }

  private void changeRichiestaStatusAndDeliveryDate(Instant deliveryDate, List<Richiesta> richieste, String tipoOperazione) {
    log.debug("Richieste found: {}", richieste.size());

    Set<String> richiesteIdentificativi = richieste.stream()
                                                   .map(Richiesta::getIdentificativo)
                                                   .collect(Collectors.toSet());

    if (!richiesteIdentificativi.isEmpty()) {
      log.debug("updating delivery date for Devices related on request: {}", richiesteIdentificativi);
      setDeliveryDateOnDeviceByRequest(deliveryDate, richieste);
      log.debug("updating status for Richieste : {}", richiesteIdentificativi);
      transitiService.changeRichiesteStatus(richiesteIdentificativi, tipoOperazione);
      log.debug("updated Richieste status");
    } else {
      log.debug("No richieste with tipo Operazione {} found, won't change any status", tipoOperazione);
    }
  }

  private void setDeliveryDateOnDeviceByRequest(Instant deliveryDate, List<Richiesta> requests) {

    requests.stream()
            .flatMap(richiesta -> richiesta.getDispositivos()
                                           .stream())
            .forEach(device -> {
              device.setDataSpedizione(deliveryDate);
              device = dispositivoRepository.save(device);
              Instant deliveryDateDevice = (device != null) ? device.getDataSpedizione() : null;
              String identifierDevice = (device != null) ? device.getIdentificativo() : null;
              log.info("Update deliveryDate [{}] to device: {}", deliveryDateDevice, identifierDevice);
            });
  }

}
