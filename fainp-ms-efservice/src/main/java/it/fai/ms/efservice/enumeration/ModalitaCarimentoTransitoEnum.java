package it.fai.ms.efservice.enumeration;

public enum ModalitaCarimentoTransitoEnum {
  FILE_DA_FORNITORE,
  FILE_GENERICO,
  FILE_CSV,
  MANUALE;
}
