package it.fai.ms.efservice.service.fsm.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;

/**
 * @author Luca Vassallo Enumeration for Richiesta State; All state for FSM Accettazione, Inoltro, Modifica; If state
 *         for FSM is duplicate, insert only one;
 */
public class StatoRichiestaUtil {

  private final static Logger log = LoggerFactory.getLogger(StatoRichiestaUtil.class);

  public static Set<StatoRichiesta> getStateOfAccettazione() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    states.add(StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE);
    states.add(StatoRichiesta.RICHIESTA_ANNULLATA);
    states.add(StatoRichiesta.SOSPESA);
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.RICHIESTA_RIFIUTATA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroTE() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO);
    states.add(StatoRichiesta.ATTESA_RISPOSTA);
    states.add(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO);
    states.add(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
    states.add(StatoRichiesta.COMPLETO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ANALISI_RISPOSTA_ORDINE);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.PORTALE_WEB_FORNITORE);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.ACCETTATO_FORNITORE);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    states.add(StatoRichiesta.SERVIZI_INOLTRATI);
    states.add(StatoRichiesta.ERRORE_GESTIONE_SERVIZIO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroTrackyCard() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.SERVIZI_INOLTRATI);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO);
    states.add(StatoRichiesta.ATTESA_RISPOSTA);
    states.add(StatoRichiesta.COMPLETO);
    states.add(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO);
    states.add(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ANALISI_RISPOSTA_ORDINE);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.ACCETTATO_FORNITORE);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroViaCard() {
    Set<StatoRichiesta> states = getStateOfInoltroTE();
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.remove(StatoRichiesta.SERVIZI_INOLTRATI);
    states.remove(StatoRichiesta.ERRORE_GESTIONE_SERVIZIO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroTSAT() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC);
    states.add(StatoRichiesta.VERIFICA_ACCETTAZIONE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ABBINATO);
    states.add(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.ACCETTATO_FORNITORE);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTeFurtoSmarrimento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.SMARRIMENTO);
    states.add(StatoRichiesta.FURTO);
    states.add(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTItaFurtoSmarrimento() {
    Set<StatoRichiesta> states = getStateOfModificaTeFurtoSmarrimento();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTrackyCardFurtoSmarrimento() {
    Set<StatoRichiesta> states = getStateOfModificaTItaFurtoSmarrimento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaViaCardFurtoSmarrimento() {
    Set<StatoRichiesta> states = getStateOfModificaTItaFurtoSmarrimento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_VC);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTeSatFurtoSmarrimento() {
    Set<StatoRichiesta> states = getStateOfModificaTeFurtoSmarrimento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificateTSatFurtoSmarrimento() {
    Set<StatoRichiesta> states = getStateOfModificaTeFurtoSmarrimento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTeVarTargaRitargatoMalfSostituzione() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTItaVarTargaRitargatoMalfSostituzione() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTrackyCardVarTargaRitargatoMalfSostituzione() {
    Set<StatoRichiesta> states = getStateOfTItaVarTargaRitargatoMalfSostituzione();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfViaCardMalfSostituzione() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_VC);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTeSatVarTarga() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTSatVarTargaRitargatoMalfSostituzione() {
    Set<StatoRichiesta> set = getStateOfTeVarTargaRitargatoMalfSostituzione();
    set.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    set.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT);
    return set;
  }

  public static Set<StatoRichiesta> getStateOfTeRientroMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.ANNULLATO_UTENTE);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTItaRientroMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.ANNULLATO_UTENTE);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTrackyCardRientroMalfunzionamento() {
    Set<StatoRichiesta> states = getStateOfTItaRientroMalfunzionamento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfViaCardRientroMalfunzionamento() {
    Set<StatoRichiesta> states = getStateOfTItaRientroMalfunzionamento();
    states.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TI);
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_VC);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTeSatRientroMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTeSatMalfunzionamentoSost() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.DISATTIVAZIONE_SOSPESA);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfTSatRientroMalfunzionamento() {
    Set<StatoRichiesta> set = getStateOfTeRientroMalfunzionamento();
    set.remove(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    set.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT);
    return set;
  }

  public static Set<StatoRichiesta> getStateOfRescRiattSospensione() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.IN_SOSPENSIONE);
    states.add(StatoRichiesta.IN_RESCISSIONE);
    states.add(StatoRichiesta.SOSPESO);
    states.add(StatoRichiesta.CESSATO);
    states.add(StatoRichiesta.IN_RIATTIVAZIONE);
    states.add(StatoRichiesta.RIATTIVATO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroTI() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VERIFICA_VIACARD);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC);
    states.add(StatoRichiesta.VERIFICA_TI_IN_DEPOSITO);
    states.add(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO);
    states.add(StatoRichiesta.IN_ATTESA_INOLTRO);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ACCETTATO_FORNITORE);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroGoBox() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.SERVIZI_INOLTRATI);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroTollCollect() {
    HashSet<StatoRichiesta> states = new HashSet<>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.CONTROLLO_DOCUMENTI);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO);
    states.add(StatoRichiesta.DA_VALIDARE);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.INOLTRO_PARZIALE);
    states.add(StatoRichiesta.VALIDAZIONE_CODICE_CONTRATTO);
    states.add(StatoRichiesta.VALIDATO_COMPLETO);
    states.add(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
    states.add(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroToll2Go() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    return states;
  }

  public static List<StatoRichiesta> getStatoRichiestaConcluso() {
    return ImmutableList.of(StatoRichiesta.ATTIVO_EVASO, StatoRichiesta.EVASO_CON_RICONSEGNA, StatoRichiesta.EVASO_CON_DENUNCIA,
                            StatoRichiesta.EVASO_SENZA_DENUNCIA, StatoRichiesta.EVASO_POST_ALLINEAMENTO, StatoRichiesta.EVASO, StatoRichiesta.CONCLUSO_NON_SPEDITO, StatoRichiesta.ANNULLATO_OPERATORE_FAI,
                            StatoRichiesta.ANNULLATO_UTENTE, StatoRichiesta.CESSATO, StatoRichiesta.RICHIESTA_RIFIUTATA, StatoRichiesta.RICHIESTA_ANNULLATA,
                            StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE);
  }

  public static StatoRichiesta getStatoInizialeByTipoDispositivo(TipoDispositivoEnum deviceType) {
    StatoRichiesta stato = null;
    switch (deviceType) {
    case TELEPASS_EUROPEO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TI;
      break;
    case VIACARD:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_VC;
      break;
    case TRACKYCARD:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO;
      break;
    case TOLL_COLLECT:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT;
      break;
    case LIBER_T:
      stato = StatoRichiesta.ATTIVO_PER_RIENTRO_MAGAZZINO;
      break;
    default:
      log.error("Not manage device type to return state Richiesta init to modifica");
      break;
    }
    return stato;
  }

  public static Set<StatoRichiesta> getStateOfModificaGoBox() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_GOBOX);
    states.add(StatoRichiesta.INIZIALE);
    states.add(StatoRichiesta.RICHIESTA_RIFIUTATA);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ATTIVO_EVASO);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroFrejus() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    states.add(StatoRichiesta.ACCETTATO_FORNITORE);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    return states;
  }

  private static Set<StatoRichiesta> getDefaultStateOfFsmModificaSimple() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.INIZIALE);
    states.add(StatoRichiesta.RICHIESTA_RIFIUTATA);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTollCollect() {
    Set<StatoRichiesta> states = getDefaultStateOfFsmModificaSimple();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaFrejus() {
    Set<StatoRichiesta> states = getDefaultStateOfFsmModificaSimple();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }
  public static Set<StatoRichiesta> getStateOfMalfunzionamentoFrejus() {
    Set<StatoRichiesta> states = getDefaultStateOfFsmModificaSimple();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.SMARRIMENTO);
    states.add(StatoRichiesta.FURTO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.EVASO);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaTesseraCaronte() {
    Set<StatoRichiesta> states = getDefaultStateOfFsmModificaSimple();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_TESSERA_CARONTE);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroDartFordCrossing() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();

    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaDartFordCrossingVarTraga() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();

    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING);
    states.add(StatoRichiesta.INIZIALE);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.RICHIESTA_RIFIUTATA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaAssistenzaVeicoliVarTarga() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_ASSISTENZA_VEICOLI);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.DISATTIVAZIONE);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroGranSanBernardo() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VERIFICA_TTGSB_IN_DEPOSITO);
    states.add(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaGranSanBernardoFurtoSmarrimento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.FURTO);
    states.add(StatoRichiesta.SMARRIMENTO);
    states.add(StatoRichiesta.EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaGranSanBernardoVarTargaMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);

    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroHGV() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();

    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.DA_INOLTRARE);
    states.add(StatoRichiesta.INOLTRATO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroAssistenzaVeicoli() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO);
    states.add(StatoRichiesta.CONTROLLO_DOCUMENTI);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroLiberT() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VERIFICA_LIBER_T_IN_DEPOSITO);
    states.add(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO);
    states.add(StatoRichiesta.PREPARAZIONE_SPEDIZIONE);
    states.add(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    states.add(StatoRichiesta.CONCLUSO_NON_SPEDITO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaLiberTFurtoSmarrimento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.FURTO);
    states.add(StatoRichiesta.SMARRIMENTO);
    states.add(StatoRichiesta.EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaLiberTVarTargaMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfInoltroViaToll() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.INIZIALE);
    states.add(StatoRichiesta.RICHIESTA_RIFIUTATA);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;

  }

  public static Set<StatoRichiesta> getStateOfInoltroVispro() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.CONTROLLO_DOCUMENTI);
    states.add(StatoRichiesta.INCOMPLETO_TECNICO);
    states.add(StatoRichiesta.ATTIVO_EVASO);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaViaTollFurtoSmarrimento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.SMARRIMENTO);
    states.add(StatoRichiesta.FURTO);
    states.add(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA);
    states.add(StatoRichiesta.DA_PROCESSARE);
    states.add(StatoRichiesta.ORDINE_SOSPESO);
    states.add(StatoRichiesta.ESAME_DENUNCIA);
    states.add(StatoRichiesta.ASSENZA_ALLEGATO);
    states.add(StatoRichiesta.EVASO_SENZA_DENUNCIA);
    states.add(StatoRichiesta.EVASO_POST_ALLINEAMENTO);
    states.add(StatoRichiesta.EVASO_CON_DENUNCIA);
    return states;

  }

  public static Set<StatoRichiesta> getStateOfModificaViaTollVarTargaMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VARIAZIONE_TARGA);
    states.add(StatoRichiesta.MEZZO_RITARGATO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    states.add(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;
  }

  public static Set<StatoRichiesta> getStateOfModificaLocalizzatoreSatMalfunzionamento() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();

    states.add(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT);
    states.add(StatoRichiesta.ATTIVO);
    states.add(StatoRichiesta.MALFUNZIONAMENTO);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.ANNULLATO_OPERATORE_FAI);

    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    states.add(StatoRichiesta.EVASO_CON_RICONSEGNA);
    return states;

  }

}
