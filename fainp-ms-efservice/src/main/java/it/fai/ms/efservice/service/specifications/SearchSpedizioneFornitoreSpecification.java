package it.fai.ms.efservice.service.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import it.fai.ms.efservice.domain.SpedizioneFornitoreView;
import it.fai.ms.efservice.domain.SpedizioneFornitoreView_;
import it.fai.ms.efservice.service.dto.SearchSpedizioneFornitoreDTO;

public class SearchSpedizioneFornitoreSpecification implements Specification<SpedizioneFornitoreView> {

  private final SearchSpedizioneFornitoreDTO dto;

  public SearchSpedizioneFornitoreSpecification(SearchSpedizioneFornitoreDTO dto) {
    this.dto = dto;
  }

  private String likeIt(String param) {
    return "%" + param + "%";
  }

  @Override
  public Predicate toPredicate(Root<SpedizioneFornitoreView> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
    List<Predicate> predicates = new ArrayList<>();

    if (dto != null) {

      if (StringUtils.isNotEmpty(dto.getCerca())) {
        predicates.add(builder.or(builder.like(root.get(SpedizioneFornitoreView_.produttore), likeIt(dto.getCerca()))));
      }

      if (StringUtils.isNotEmpty(dto.getSeriale())) {
        predicates.add(builder.like(root.get(SpedizioneFornitoreView_.seriali), likeIt(dto.getSeriale())));
      }

      if (StringUtils.isNotEmpty(dto.getTarga())) {
        predicates.add(builder.like(root.get(SpedizioneFornitoreView_.targhe), likeIt(dto.getTarga())));
      }

      if (StringUtils.isNotEmpty(dto.getDataRientroDispositivo())) {
        predicates.add(builder.like(root.get(SpedizioneFornitoreView_.dataRientroDispositivo), likeIt(
          dto.getDataRientroDispositivo()
        )));
      }
    }

    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
  }
}
