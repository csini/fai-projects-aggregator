package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.List;

public class DispositivoCarrelloDTO implements Serializable {

  private static final long serialVersionUID = 1263638947933966700L;

  private String tipoDispositivo;

  private List<VeicoloCarrelloDTO> veicoli;

  private boolean puntoBlu;

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public List<VeicoloCarrelloDTO> getVeicoli() {
    return veicoli;
  }

  public void setVeicoli(List<VeicoloCarrelloDTO> veicoli) {
    this.veicoli = veicoli;
  }

  public boolean isPuntoBlu() {
    return puntoBlu;
  }

  public void setPuntoBlu(boolean puntoBlu) {
    this.puntoBlu = puntoBlu;
  }

  @Override
  public String toString() {
    return "DispositivoCarrelloDTO [tipoDispositivo=" + tipoDispositivo + ", veicoli=" + veicoli + ", puntoBlu=" + puntoBlu + "]";
  }

}
