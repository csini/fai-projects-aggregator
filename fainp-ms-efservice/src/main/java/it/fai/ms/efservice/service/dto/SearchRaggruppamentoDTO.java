package it.fai.ms.efservice.service.dto;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.enumeration.StatoRaggruppamento;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

public class SearchRaggruppamentoDTO implements Serializable {

  private String produttore;

  private String tipoDispositivo;

  private String stato;

  private Instant dataInvioFile;

  private Instant dataAcquisizioneFile;

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public Instant getDataInvioFile() {
    return dataInvioFile;
  }

  public void setDataInvioFile(Instant dataInvioFile) {
    this.dataInvioFile = dataInvioFile;
  }

  public Instant getDataAcquisizioneFile() {
    return dataAcquisizioneFile;
  }

  public void setDataAcquisizioneFile(Instant dataAcquisizioneFile) {
    this.dataAcquisizioneFile = dataAcquisizioneFile;
  }

  @Override
  public String toString() {
    return "SearchRaggruppamentoDTO{" +
      "produttore='" + produttore + '\'' +
      ", tipoDispositivo=" + tipoDispositivo +
      ", stato=" + stato +
      ", dataInvioFile=" + dataInvioFile +
      ", dataAcquisizioneFile=" + dataAcquisizioneFile +
      '}';
  }
}
