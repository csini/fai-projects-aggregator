package it.fai.ms.efservice.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;

@Mapper(componentModel = "spring", uses = { DispositivoMapper.class })
public interface AssociazioneDVMapper extends EntityMapper<AssociazioneDVDTO, AssociazioneDV> {

  @Override
  @Mapping(source = "dispositivo.id", target = "dispositivoId")
  @Mapping(source = "dispositivo.seriale", target = "dispositivoSeriale")
  AssociazioneDVDTO toDto(AssociazioneDV associazioneDV);

  @Override
  @Mapping(source = "dispositivoId", target = "dispositivo")
  AssociazioneDV toEntity(AssociazioneDVDTO associazioneDVDTO);

  default AssociazioneDV fromId(Long id) {
    if (id == null) {
      return null;
    }
    AssociazioneDV associazioneDV = new AssociazioneDV();
    associazioneDV.setId(id);
    return associazioneDV;
  }
}
