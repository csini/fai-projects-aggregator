package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceAssociationMessageProducerService;

public class FsmActionSendMessageToWsTelepassIta implements Action<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmActionSendMessageToWsTelepassIta.class);

  private final DeviceAssociationMessageProducerService deviceAssociationProduceService;

  public FsmActionSendMessageToWsTelepassIta(final DeviceAssociationMessageProducerService _deviceAssociationProduceService) {
    deviceAssociationProduceService = _deviceAssociationProduceService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        log.info("Richiesta... {}", richiesta);
        try {
          sendMessage(richiesta);
        } catch (Exception e) {
          log.error("Exception in sender message to WS telepass ITA", e);
          throw new RuntimeException(e);
        }
      } else {
        log.warn("Not instance object {} to Richiesta", object);
      }
    } else {
      log.error("Message by context {} is null", context);
    }
  }

  private void sendMessage(Richiesta richiesta) throws Exception {
    log.info("Send message to WebService Telepass ITA");
    deviceAssociationProduceService.associationDeviceTelepassIta(richiesta);
  }

}
