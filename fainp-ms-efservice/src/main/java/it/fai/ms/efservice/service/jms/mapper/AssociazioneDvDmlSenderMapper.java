package it.fai.ms.efservice.service.jms.mapper;

import it.fai.ms.common.dml.efservice.dto.AssociazioneDvDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractDmlSenderMapper;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class AssociazioneDvDmlSenderMapper extends AbstractDmlSenderMapper<AssociazioneDvDMLDTO, AssociazioneDV> {
  
  private Logger log = LoggerFactory.getLogger(getClass());

  public AssociazioneDvDmlSenderMapper() {
    super(AssociazioneDvDMLDTO::new);
  }

  protected AssociazioneDvDMLDTO toDMLDTO(AssociazioneDV assDv, AssociazioneDvDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyDMLDTO(String.valueOf(assDv.getId()));
    }

    dml.setDmlRevisionTimestamp(Instant.now());
    dml.setIdentificativoVeicolo(assDv.getUuidVeicolo());
    String identificativoDispositivo = null;
    String identificativoContratto = null;
    Dispositivo dispositivo = assDv.getDispositivo();
    if (dispositivo != null) {
      identificativoDispositivo = dispositivo
                                       .getIdentificativo();
      Contratto contratto = dispositivo.getContratto();
      if(contratto != null) {
        identificativoContratto = contratto.getIdentificativo();
      }
    } 
    
    log.debug("Identificativo Dispositivo: {}", identificativoDispositivo);
    dml.setIdentificativoDispositivo(identificativoDispositivo);
    dml.setData(assDv.getData());

    log.debug("Identificativo Contratto: {}", identificativoDispositivo);
    dml.setIdentificativoContratto(identificativoContratto);
    String oldUuidVeicolo = assDv.getOldUuidVeicolo();
    log.debug("Old UUID Veicolo: {}", oldUuidVeicolo);
    dml.setOldUuidVeicolo(oldUuidVeicolo);

    return dml;
  }
}
