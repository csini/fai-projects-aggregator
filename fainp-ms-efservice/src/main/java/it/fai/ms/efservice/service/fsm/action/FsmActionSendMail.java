/**
 * 
 */
package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.state.State;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.notification.NotificationMailClient;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

/**
 * @author Luca Vassallo
 */
public class FsmActionSendMail implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(FsmActionSendMail.class);

  private final JmsProperties       jmsProperties;

  public FsmActionSendMail(JmsProperties appProperties) {

    this.jmsProperties = appProperties;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.action.Action#execute(org.springframework.statemachine.StateContext)
   */
  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    State<StatoRichiesta, RichiestaEvent> source = context.getSource();
    State<StatoRichiesta, RichiestaEvent> target = context.getTarget();

    StatoRichiesta fromState = source.getId();
    StatoRichiesta toState = target.getId();

    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;

        ClienteFai clienteFai = richiesta.getOrdineCliente().getClienteAssegnatario();
        if (clienteFai != null) {
          String codiceCliente = clienteFai.getCodiceCliente();
          String ragioneSociale = clienteFai.getRagioneSociale();

          if (fromState == StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO && toState == StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO) {
            log.info("Send mail for Telepass EU CTRRI => CodiceCliente: " + codiceCliente + " - RagioneSociale: " + ragioneSociale);
            NotificationMailClient.sendTelepassEuInoltroCTRRIKo(jmsProperties, codiceCliente, ragioneSociale);
            return;
          }

          if (fromState == StatoRichiesta.ANALISI_RISPOSTA_ORDINE && toState == StatoRichiesta.ORDINE_SOSPESO) {
            log.info("Send mail for Telepass EU Richiesta Sospeso => CodiceCliente: " + codiceCliente + " - RagioneSociale: "
                     + ragioneSociale);
            // commentato perchè non esiste il template mail
            //NotificationMailClient.sendTelepassEuInoltroRichiestaSospeso(jmsProperties, codiceCliente, ragioneSociale);
            return;
          }
        } else {
          log.warn("Not found Cliente Fai for richiesta: " + richiesta.getIdentificativo() + "[" + richiesta.getId() + "]");
        }

      }
    }

  }

}
