package it.fai.ms.efservice.web.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.ms.efservice.web.rest.errors.Errno;

@RestController
@RequestMapping(ErrorResourceExt.BASE_PATH)
public class ErrorResourceExt {

//	private final Logger log = LoggerFactory.getLogger(ErrorResourceExt.class);

	public static final String BASE_PATH = "/api/public";

	public static final String ERRORS = "/errors";

	public static final String API_ERRORS = BASE_PATH + ERRORS;
	
	public ErrorResourceExt() {
		
	}

	@GetMapping(ERRORS)
	@Timed
	@ApiOperation(httpMethod = "GET", value = "Return list of errors")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "get list of errors") })
	public ResponseEntity<Object> getErrors()
			throws Exception {

		List<Object> arr = new ArrayList<Object>();
		for (Errno errno : Errno.values()) {
			arr.add(new ApiErrno(errno));
		}
		
		return ResponseEntity.ok(arr);
	}
	
	class ApiErrno implements Serializable {

		private final static long serialVersionUID = 6391160249852336471L;

		@JsonProperty("code")
		private final Integer code;
		@JsonProperty("internalMessage")
		private final String internalMessage;
		@JsonProperty("description")
		private final String description;
		
		public ApiErrno(Errno errno) {
			this.code = errno.code();
			this.internalMessage = errno.internalMessage();
			this.description = errno.description();
		}

		public Integer getCode() {
			return code;
		}

		public String getInternalMessage() {
			return internalMessage;
		}

		public String getDescription() {
			return description;
		}
		
	}
}
