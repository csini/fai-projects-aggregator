package it.fai.ms.efservice.service.fsm.action;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Transactional
public class FsmActionActiveDevice implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue      fsmSenderToQueue;
  private final DeviceProducerService deviceProducerService;
  private final DispositivoEvent      event;

  public FsmActionActiveDevice(final FsmSenderToQueue fsmSenderToQueue, final DeviceProducerService deviceProducerService,
                               final DispositivoEvent event) {
    this.fsmSenderToQueue = fsmSenderToQueue;
    this.deviceProducerService = deviceProducerService;
    this.event = event;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      if (log.isDebugEnabled()) {
        log.debug("Change Stato Richiesta [ID: " + ((richiesta != null) ? richiesta.getId() : null) + "] per spedizioni Dispositivi...");
      }

      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      sendChangeStatus(dispositivi);

      changeDeviceServiceStatus(richiesta);
    }
  }

  private void sendChangeStatus(Set<Dispositivo> dispositivi) {
    Set<Dispositivo> dispositiviChangesStatus = dispositivi.stream()
                                                           .map(d -> {
                                                             fsmSenderToQueue.sendMessageForChangeStatoDispositivo(d, event);
                                                             return d;
                                                           })
                                                           .collect(toSet());
    log.info("Change status with command {} for dispositivi: {}", event, dispositiviChangesStatus);
  }

  private void changeDeviceServiceStatus(Richiesta richiesta) {
    log.info("Active/DeActive service on Device to request: {}", richiesta.getIdentificativo());
    deviceProducerService.updateDeviceServiceStatus(richiesta);
    log.info("Finish update device services status to Richiesta: {}", richiesta);
  }

}
