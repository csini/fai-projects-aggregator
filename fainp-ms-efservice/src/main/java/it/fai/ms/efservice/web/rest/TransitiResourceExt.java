package it.fai.ms.efservice.web.rest;

import java.util.Collections;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.dto.ResponseTessereFrejusDto;
import it.fai.ms.efservice.service.ProcessTransitiService;
import it.fai.ms.efservice.service.TransitiAsyncServiceExt;
import it.fai.ms.efservice.service.dto.ConfermaTransitoDTO;

@RestController
@RequestMapping(TransitiResourceExt.BASE_URL)
public class TransitiResourceExt {

  private final Logger log = LoggerFactory.getLogger(TransitiResourceExt.class);

  final static String BASE_URL = "/api/transito";

  private static final String POLLING = "/polling";

  private static final String CONFERMA = "/conferma";
  private static final String CONFERMA_POLLING = CONFERMA + POLLING;

  private static final String KEY_JOB = "job";

  private static final String CONFERMA_JEY_JOB = "JOB_CONFERMA_GRUPPO_RICHIESTE";

  private final TransitiAsyncServiceExt transitiAsyncServiceExt;
  private final AsyncPollingJobServiceImpl asyncPollingJobService;
  private final ProcessTransitiService processTransitiService;

  public TransitiResourceExt(
    TransitiAsyncServiceExt transitiAsyncServiceExt,
    AsyncPollingJobServiceImpl asyncPollingJobService,
    ProcessTransitiService processTransitiService
  ) {
    this.transitiAsyncServiceExt = transitiAsyncServiceExt;
    this.asyncPollingJobService = asyncPollingJobService;
    this.processTransitiService = processTransitiService;
  }

  @PostMapping(CONFERMA)
  public ResponseEntity<Map<String,String>> confermaTransito(@Valid @RequestBody ConfermaTransitoDTO confermaTransitoDTO) {
    log.debug("REST request to cofirm transit with dto : {}", confermaTransitoDTO);
    String identifierJob = transitiAsyncServiceExt.getKeyCacheAndPushDefaultValue(CONFERMA_JEY_JOB);

    transitiAsyncServiceExt.processAsyncTransiti(identifierJob ,confermaTransitoDTO);

    return ResponseEntity.ok(Collections.singletonMap(KEY_JOB, identifierJob));
  }
  @PostMapping(CONFERMA+"Frejus/{paese}")
  public ResponseEntity<AsyncJobResponse> confermaTransito(@PathVariable String paese,@RequestBody ResponseTessereFrejusDto data) {
    AsyncJobResponse output = processTransitiService.processFrejus(data,paese);
    return ResponseEntity.ok(output);
  }

  @GetMapping(CONFERMA_POLLING + "/{identificativo}")
  @Timed
  public ResponseEntity<AsyncJobResponse> pollingFsmContratto(@PathVariable String identificativo) {
    log.debug("Rest request for polling FSM-Contratto change status {}", identificativo);
    AsyncJobResponse ouput = (AsyncJobResponse) asyncPollingJobService.getValue(
      identificativo
    );
    log.info("Response polling is {}", ouput);
    return ResponseEntity.ok(ouput);
  }

}
