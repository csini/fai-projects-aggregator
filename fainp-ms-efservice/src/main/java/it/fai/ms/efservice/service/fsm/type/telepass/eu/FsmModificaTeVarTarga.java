/**
 * 
 */
package it.fai.ms.efservice.service.fsm.type.telepass.eu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.eu.FsmModificaTeVarTargaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

/**
 * @author Luca Vassallo
 */
@Service(value = FsmModificaTeVarTarga.FSM_MOD_TE_VARTARGA)
public class FsmModificaTeVarTarga extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TE_VARTARGA = "fsmModificaTeVarTarga";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTeVarTarga(@Qualifier(FsmModificaTeVarTargaConfig.MOD_TE_VARTARGA) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                               FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TE_VARIAZIONE_TARGA;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_EUROPEO };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
