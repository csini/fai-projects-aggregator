package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceNotReturnableMessage;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.service.jms.JmsDeviceErrorService;

@Service
@Transactional(rollbackFor = IllegalStateException.class)
public class DeviceNotReturnableConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseKOConsumer deviceResponseKoConsumer;

  private final JmsDeviceErrorService jmsDeviceErrorService;

  public DeviceNotReturnableConsumer(final DeviceResponseKOConsumer _deviceResponseKoConsumer,
                                     final JmsDeviceErrorService _jmsDeviceErrorService) {
    deviceResponseKoConsumer = _deviceResponseKoConsumer;
    jmsDeviceErrorService = _jmsDeviceErrorService;
  }

  //FIXME mai chiamato? E' corretto?!
  public void consume(DeviceNotReturnableMessage message) {
    log.info("Manage message: {}", message);
    String errorMsg = jmsDeviceErrorService.buildErrorMessage(message);
    OrderRequestUuid orderRequestUuid = message.getOrderRequestUuid();
    deviceResponseKoConsumer.consume(message.getContractUuid().getUuid(), message.getObuNumber().getCode(), errorMsg);
  }

  public void consume(DeviceEventMessage eventMessage) {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();
    String errorMsg = jmsDeviceErrorService.buildErrorMessage(eventMessage);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

}
