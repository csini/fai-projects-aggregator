package it.fai.ms.efservice.client;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.client.dto.VehicleDTO;

@FeignClient(name = "faivehicle")
public interface VehicleClient {

  public static final String BASE_PATH = "/api/public";
  public static final String VEHICLES  = BASE_PATH + "/vehicles";

  @GetMapping(VEHICLES + "/findByCodiceAzienda/{codiceAzienda}")
  @Timed
  public List<VehicleDTO> findAllByCodiceAzienda(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                                 @PathVariable("codiceAzienda") String codiceAzienda);


  @PostMapping(VEHICLES + "/findVehicleWithoutBooklet")
  public List<VehicleDTO> findVehicleWithoutBooklet(
    @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
    @RequestBody List<String> uuids
  );
  
  @GetMapping(VEHICLES + "/uuid/{uuid}")
    public VehicleDTO findVehicleByUUID(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                        @PathVariable(required = true, name = "uuid") String uuid);
  
  
  @GetMapping("/api/dmlbulk/vehicles/identificativi/{uuid}")
  @Async
  public void sendVehicleByUUID(
		  @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
		  @PathVariable(required = true, name = "uuid")String uuid);
  
  @GetMapping(VEHICLES + "/findVehiclesByTargaAndNazione/{targa}/{nazione}")
  @Timed
  public VehicleDTO findVehiclesByTargaAndNazione(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
		  														@PathVariable(required = true, name = "targa") String targa, @PathVariable(required = true, name = "nazione") String nazione) ;
}
