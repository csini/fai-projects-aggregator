package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.ContrattoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Contratto and its DTO ContrattoDTO.
 */
@Mapper(componentModel = "spring", uses = {ProduttoreMapper.class, ClienteFaiMapper.class, })
public interface ContrattoMapper extends EntityMapper <ContrattoDTO, Contratto> {

    @Mapping(source = "produttore.id", target = "produttoreId")
    @Mapping(source = "produttore.nome", target = "produttoreNome")
    @Mapping(source = "clienteFai.id", target = "clienteFaiId")
    @Mapping(source = "clienteFai.ragioneSociale", target = "clienteFaiRagioneSociale")
    ContrattoDTO toDto(Contratto contratto); 

    @Mapping(source = "paeseRiferimentoIva", target = "paeseRiferimentoIva")
    @Mapping(source = "produttoreId", target = "produttore")
    @Mapping(source = "clienteFaiId", target = "clienteFai")
    @Mapping(target = "richiestas", ignore = true)
    Contratto toEntity(ContrattoDTO contrattoDTO); 
    default Contratto fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contratto contratto = new Contratto();
        contratto.setId(id);
        return contratto;
    }
}
