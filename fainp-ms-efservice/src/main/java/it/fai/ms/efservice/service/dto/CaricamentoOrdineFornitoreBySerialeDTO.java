package it.fai.ms.efservice.service.dto;

import java.time.ZonedDateTime;
import java.util.Collection;

public class CaricamentoOrdineFornitoreBySerialeDTO extends CaricamentoOrdineFornitoreDTO {

  public static class OrdineFornitoreDispositivo {
    private String seriale;
    private String pan;
    private String tipoHardware;
    private ZonedDateTime dataScadenza;

    public String getSeriale() {
      return seriale;
    }

    public void setSeriale(String seriale) {
      this.seriale = seriale;
    }

    public String getPan() {
      return pan;
    }

    public void setPan(String pan) {
      this.pan = pan;
    }

    public String getTipoHardware() {
      return tipoHardware;
    }

    public void setTipoHardware(String tipoHardware) {
      this.tipoHardware = tipoHardware;
    }

    public ZonedDateTime getDataScadenza() {
      return dataScadenza;
    }

    public void setDataScadenza(ZonedDateTime dataScadenza) {
      this.dataScadenza = dataScadenza;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("OrdineFornitoreDispositivo [seriale=");
      builder.append(seriale);
      builder.append(", pan=");
      builder.append(pan);
      builder.append(", tipoHardware=");
      builder.append(tipoHardware);
      builder.append(", dataScadenza=");
      builder.append(dataScadenza);
      builder.append("]");
      return builder.toString();
    }



  }

  private static final long serialVersionUID = 1146756881557223786L;

  private Collection<OrdineFornitoreDispositivo> dispositivi;

  public Collection<OrdineFornitoreDispositivo> getDispositivi() {
    return dispositivi;
  }

  public void setDispositivi(Collection<OrdineFornitoreDispositivo> dispositivi) {
    this.dispositivi = dispositivi;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("CaricamentoOrdineFornitoreBySerialeDTO [dispositivi=");
    builder.append(dispositivi);
    builder.append(", getId()=");
    builder.append(getId());
    builder.append(", isSkipValidation()=");
    builder.append(isSkipValidation());
    builder.append(", getNote()=");
    builder.append(getNote());
    builder.append("]");
    return builder.toString();
  }



}
