package it.fai.ms.efservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.SpedizioneDispositiviClienteView;

@SuppressWarnings("unused")
@Repository
public interface SpedizioneDispositiviClienteViewRepositoryExt extends
  JpaRepository<SpedizioneDispositiviClienteView, Long>,
  JpaSpecificationExecutor<SpedizioneDispositiviClienteView> {

}
