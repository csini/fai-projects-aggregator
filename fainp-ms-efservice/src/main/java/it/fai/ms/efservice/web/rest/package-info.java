/**
 * Spring MVC REST controllers.
 */
package it.fai.ms.efservice.web.rest;

/**
 * 
 * https://exagespa.atlassian.net/browse/FAINP-812 (Come Sistema voglio esporre su EFSERVICE i dari relativi ai dispositivi attivi sul veicolo)
 * 
 * https://exagespa.atlassian.net/browse/FAINP-1007 (Come Sistema devo esporre le API per recuperare l'elenco dei dispositivi NOTARGA e SCORTA associati all'Utente Master attuale)
 * 
 * 
 * SQL: use tempdb; drop database faiefservice; create database faiefservice;
 * 
 * 
 */
