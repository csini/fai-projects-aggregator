package it.fai.ms.efservice.service.fsm.bean.libert;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.libert.FsmModificaLiberTVarTargaMalfunzionamento;

@WithStateMachine(id = FsmModificaLiberTVarTargaMalfunzionamento.FSM_MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaLiberTVarTargaMalfunzionamentoTransitionBean extends AbstractFsmRichiestaTransition {
}
