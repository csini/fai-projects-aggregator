package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.efservice.dto.FsmCommandDTO;
import it.fai.ms.efservice.service.jms.consumer.FsmChangeStatusConsumer;

@Service
@Transactional
public class JmsListenerCommandToFsmService extends JmsObjectMessageListenerTemplate<FsmCommandDTO> implements JmsQueueListener {

  private final Logger                          log = LoggerFactory.getLogger(getClass());
  private final FsmChangeStatusConsumer fsmChangeStatusConsumer;

  public JmsListenerCommandToFsmService(final FsmChangeStatusConsumer fsmChangeStatusConsumer) {
    this.fsmChangeStatusConsumer = fsmChangeStatusConsumer;
  }


  @Override
  protected void consumeMessage(FsmCommandDTO dto) {
    try {
      fsmChangeStatusConsumer.consume(dto);
    } catch (Exception e) {
      String errorMessage = String.format("Error on consuming message %s", dto.toString());
      log.error(errorMessage, e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.FSM_COMMAND_EVENT;
  }

}