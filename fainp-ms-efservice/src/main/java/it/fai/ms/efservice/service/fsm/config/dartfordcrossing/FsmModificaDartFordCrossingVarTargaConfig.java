package it.fai.ms.efservice.service.fsm.config.dartfordcrossing;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaDartFordCrossingVarTargaConfig.MOD_DARTFORDCROSSING_VARTARGA)
public class FsmModificaDartFordCrossingVarTargaConfig extends FsmRichiestaConfig {

  public static final String MOD_DARTFORDCROSSING_VARTARGA = "ModDartFordCrossingVarTarga";

  private final static Logger log = LoggerFactory.getLogger(FsmModificaDartFordCrossingVarTargaConfig.class);

  public FsmModificaDartFordCrossingVarTargaConfig(final FsmSenderToQueue _senderFsmUtil,
                                                   final DeviceServiceActivationMessageProducerService _deviceServiceActivationService) {

  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_DARTFORD_CROSSING_VARAZIONE_TARGA.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaDartFordCrossingVarTraga();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(getStatoRichiestaInitial())
               .target(StatoRichiesta.INIZIALE)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.MU_SOSPENDI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .action(new FsmActionGeneric());
  }

}
