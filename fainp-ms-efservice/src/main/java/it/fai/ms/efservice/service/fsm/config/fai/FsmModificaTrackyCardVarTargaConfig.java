package it.fai.ms.efservice.service.fsm.config.fai;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDisableAllService;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardGestioneRientro;
import it.fai.ms.efservice.service.fsm.guard.GuardMalfunzionamentoSostituzione;
import it.fai.ms.efservice.service.fsm.guard.GuardMezzoRitargatoTE;
import it.fai.ms.efservice.service.fsm.guard.GuardTipoRichiesta;
import it.fai.ms.efservice.service.fsm.guard.GuardVariazioneTargaTE;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTrackyCardVarTargaConfig.MOD_TRACKYCARD_VARTARGA)
public class FsmModificaTrackyCardVarTargaConfig extends FsmRichiestaConfig {

  public static final String MOD_TRACKYCARD_VARTARGA = "ModTrackyCardVarTarga";

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTrackyCardVarTargaConfig.class);

  private final FsmSenderToQueue                              senderFsmUtil;
  private final DeviceServiceActivationMessageProducerService deviceServiceActivationService;

  public FsmModificaTrackyCardVarTargaConfig(final FsmSenderToQueue _senderFsmUtil,
                                             final DeviceServiceActivationMessageProducerService _deviceServiceActivationService) {
    senderFsmUtil = _senderFsmUtil;
    deviceServiceActivationService = _deviceServiceActivationService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TRACKY_VARIAZIONE_TARGA.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfTrackyCardVarTargaRitargatoMalfSostituzione();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .guard(new GuardTipoRichiesta(TipoRichiesta.VARIAZIONE_TARGA))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.ATTESA_RIENTRO))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MEZZO_RITARGATO))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.ATTESA_RIENTRO))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.ATTESA_RIENTRO))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(senderFsmUtil))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .action(new FsmActionDisableAllService(deviceServiceActivationService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmUtil));
  }

}
