package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SearchSpedizioneFornitoreDTO implements Serializable {

  private String produttore;

  private String cerca;

  private String targa;

  private String seriale;

  private LocalDate dataRientroDispositivo;

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  public String getCerca() {
    return cerca;
  }

  public void setCerca(String cerca) {
    this.cerca = cerca;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public String getDataRientroDispositivo() {
    if(dataRientroDispositivo == null) {
      return "";
    }
    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    return dataRientroDispositivo.format(dateTimeFormatter);
  }

  public void setDataRientroDispositivo(LocalDate dataRientroDispositivo) {
    this.dataRientroDispositivo = dataRientroDispositivo;
  }

  @Override
  public String toString() {
    return "SearchSpedizioneFornitoreDTO{" +
      "produttore='" + produttore + '\'' +
      ", cerca='" + cerca + '\'' +
      ", targa='" + targa + '\'' +
      ", seriale='" + seriale + '\'' +
      ", dataRientroDispositivo=" + dataRientroDispositivo +
      '}';
  }
}
