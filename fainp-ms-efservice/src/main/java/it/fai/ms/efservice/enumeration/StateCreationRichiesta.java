package it.fai.ms.efservice.enumeration;

public enum StateCreationRichiesta {
  
  PENDING, SUCCESS, ERROR, WARNING;

}
