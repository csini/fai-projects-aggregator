package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.LicensePlate;
import it.fai.ms.common.jms.efservice.message.device.DeviceLicensePlateChangedMessage;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceLicensePlaceChangedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseOkConsumer deviceResponseOkConsumer;

  public DeviceLicensePlaceChangedConsumer(final DeviceResponseOkConsumer _deviceResponseOkConsumer) {
    deviceResponseOkConsumer = _deviceResponseOkConsumer;
  }

  public void consume(DeviceLicensePlateChangedMessage message) throws FsmExecuteCommandException {
    log.info("Manage message: {}", message);
    // Manage TARRC to TELEPASS EU / EU SAT
    String codiceContratto = message.getContractUuid().getUuid();
    LicensePlate licensePlate = message.getLicensePlate();
    String targa = licensePlate.getLicenseId();
    String country = licensePlate.getCountryId();

    deviceResponseOkConsumer.consumeMessageByContrattoAndTargaNazione(codiceContratto, targa, country);
  }

  public void consume(DeviceEventMessage eventMessage) throws FsmExecuteCommandException {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();
    
    deviceResponseOkConsumer.consumeLicensePlateChange(identificativoRichiesta);
  }

}
