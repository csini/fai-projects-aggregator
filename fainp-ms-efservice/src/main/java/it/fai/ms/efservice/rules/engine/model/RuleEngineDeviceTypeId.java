package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class RuleEngineDeviceTypeId implements Serializable {

  private static final long serialVersionUID = -7203738657069535038L;

  public static RuleEngineDeviceTypeId ofWildcard() {
    return new RuleEngineDeviceTypeId("*");
  }

  private String id;

  public RuleEngineDeviceTypeId(final String _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineDeviceType Id is mandatory"));
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleEngineDeviceTypeId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineDeviceTypeId [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
