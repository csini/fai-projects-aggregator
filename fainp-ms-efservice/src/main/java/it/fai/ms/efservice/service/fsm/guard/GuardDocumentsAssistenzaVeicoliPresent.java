package it.fai.ms.efservice.service.fsm.guard;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.ContrattoRichiestaException;

public class GuardDocumentsAssistenzaVeicoliPresent implements Guard<StatoRichiesta, RichiestaEvent> {

  protected final Logger        log = LoggerFactory.getLogger(this.getClass());
  private final DocumentService documentService;

  public GuardDocumentsAssistenzaVeicoliPresent(DocumentService documentService) {
    this.documentService = documentService;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean result = true;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        Contratto contratto = richiesta.getContratto();
        if (contratto == null) {
          throw new ContrattoRichiestaException("Not found contract related on Richiesta ID: " + richiesta.getId() + " - Identificativo: "
                                                + richiesta.getIdentificativo());
        }
        String codiceCliente = richiesta.getContratto()
                                        .getClienteFai()
                                        .getCodiceCliente();
        Set<String> documentUUIDs = new HashSet<>(richiesta.getUuidDocuments());
        try {
          TipoDispositivoEnum deviceType = TipoDispositivoEnum.ASSISTENZA_VEICOLI;
          log.info("Search document CONTRATTO for device {} and codice cliente {}", deviceType, codiceCliente);
          Optional<DocumentoDTO> documentContratto = documentService.findLastDocument(codiceCliente,
                                                                                      DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                                                      deviceType, null, null);
          if (documentContratto != null && documentContratto.isPresent()) {
            documentUUIDs.add(documentContratto.get()
                                               .getIdentificativo());
          } else {
            log.warn("Not found document {} for device {} and codice cliente {}", DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                     deviceType, codiceCliente);
            result = false;
          }
        } catch (RuntimeException e) {
          log.error("Unable to reach the document service ", e);
          return false;
        }
      }
    }
    return result;
  }
}
