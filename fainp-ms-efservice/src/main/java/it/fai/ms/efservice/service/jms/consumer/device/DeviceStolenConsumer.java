package it.fai.ms.efservice.service.jms.consumer.device;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.message.device.AbstractDeviceMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceStolenMessage;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceStolenConsumer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final DeviceResponseOkConsumer deviceResponseOkConsumer;

	private final RichiestaRepositoryExt repo;

	public DeviceStolenConsumer(final DeviceResponseOkConsumer _deviceResponseOkConsumer,
			RichiestaRepositoryExt _repo) {
		deviceResponseOkConsumer = _deviceResponseOkConsumer;
		repo = _repo;
	}

	public void consume(DeviceStolenMessage message) throws FsmExecuteCommandException {
		log.info("Manage message: {}", message);
		String identificativoRichiesta = getOrderRequestUuid(message,TipoRichiesta.FURTO, TipoRichiesta.FURTO_CON_SOSTITUZIONE);
		if(identificativoRichiesta!=null) 
		  deviceResponseOkConsumer.changeStatoRichiesta(identificativoRichiesta);
	}

	/*
	 * DeviceStolenMessage [contractOrderRequest=ContractOrderRequest
	 * [numeroContratto=17647608, codiceClie
	 * nte=null],orderRequestUuid=null,obuNumber=ObuNumber
	 * [code=00049000000788586998],obuId=ObuId [code=0788586998],obuType=ObuType
	 * [id=AZ],deviceInfo=it.fai.ms.common.jms.efservice.message.device.
	 * ObuDeviceInfo
	 * 
	 * @691f2c96,services=[]]
	 * 
	 */

	public String getOrderRequestUuid(AbstractDeviceMessage message,TipoRichiesta ... tipoRichiesta ) {
		final String contractNumber = message.getContractOrderRequest().getNumeroContratto();
		String seriale = message.getObuNumber().getCode();
		TipoDispositivoEnum[] tipiDispositivo = { TipoDispositivoEnum.TELEPASS_EUROPEO_SAT,
				TipoDispositivoEnum.TELEPASS_EUROPEO };
		Set<TipoRichiesta> tipiRichiesta = new HashSet<>(Arrays.asList(tipoRichiesta));
		// vehicle

		List<Richiesta> list = repo.findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(contractNumber, seriale,
				StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA, tipiRichiesta);

		for (Richiesta optRichiesta : list) {
			return optRichiesta.getIdentificativo();
		}

		log.warn("Nessuna Richiesta trovata per {}", message);
		return null;

	}

}
