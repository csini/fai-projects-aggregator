package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.efservice.domain.enumeration.StatoDS;

public class WizardService implements Serializable {

  private static final long serialVersionUID = -8507415820869523044L;

  private Instant           activationDate;
  private WizardServiceId   id;
  private String            state;
  private WizardServiceType type;

  public WizardService(final WizardServiceId _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((WizardService) _obj).getId(), id);
    }
    return isEquals;
  }

  public Instant getActivationDate() {
    return activationDate;
  }

  public WizardServiceId getId() {
    return this.id;
  }

  public String getState() {
    return state;
  }

  public WizardServiceType getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  public boolean isActive() {
    return state != null && (state.trim()
                                  .equalsIgnoreCase(StatoDS.ATTIVO.name())
                             || state.trim()
                                     .equalsIgnoreCase(StatoDS.IN_ATTIVAZIONE.name()));
  }

  public boolean isOfTypeId(final WizardServiceTypeId _wizardServiceTypeId) {
    return getType().getId()
                    .equals(_wizardServiceTypeId);
  }

  public void setActivationDate(final Instant _activationDate) {
    activationDate = _activationDate;
  }

  public void setState(final String _state) {
    state = _state;
  }

  public void setType(final WizardServiceType _type) {
    type = _type;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardService [activationDate=");
    builder.append(activationDate);
    builder.append(", id=");
    builder.append(id);
    builder.append(", state=");
    builder.append(state);
    builder.append(", type=");
    builder.append(type);
    builder.append("]");
    return builder.toString();
  }

}
