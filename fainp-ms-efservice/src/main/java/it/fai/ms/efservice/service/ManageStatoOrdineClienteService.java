package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Set;

import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

public interface ManageStatoOrdineClienteService {

  void manageOrdiniInCache();

  void manageOrdineAndRemoveFromCache(String idOrdineCliente, String identificativoOrdineCliente);

  boolean expireOrdineInCache(Long idOrdineCliente);

  Set<String> getAllKeyInCache();

  void putOrdiniInCache();

  void putOrdiniNotCompletedInCache(List<OrdineClienteCacheDTO> ordiniCliente);
}
