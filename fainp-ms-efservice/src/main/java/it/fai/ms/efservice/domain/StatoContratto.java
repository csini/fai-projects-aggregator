package it.fai.ms.efservice.domain;

public enum StatoContratto {
  ATTIVO, SOSPESO, REVOCATO;
}
