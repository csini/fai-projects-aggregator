package it.fai.ms.efservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.RequestTheftLossMalfunctionService;
import it.fai.ms.efservice.service.RequestVehicleChangeService;
import it.fai.ms.efservice.service.RequestVehicleLicensePlateChangeService;
import it.fai.ms.efservice.service.RichiestaModificaGenerationService;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;

@Service
@Transactional
public class RichiestaModificaGenerationServiceImpl implements RichiestaModificaGenerationService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RequestTheftLossMalfunctionService requestTheftLossMalfunctionService;

  private final RequestVehicleLicensePlateChangeService requestVehicleLicensePlateChangeService;

  private final RequestVehicleChangeService requestVehicleChangeService;

  private final DeviceAssociationService deviceAssociationService;

  private RequestPinService requestPinService;

  public RichiestaModificaGenerationServiceImpl(final RequestTheftLossMalfunctionService _requestTheftLossMalfunctionService,
                                                final RequestVehicleLicensePlateChangeService _requestVehicleLicensePlateChangeService,
                                                final RequestVehicleChangeService _requestVehicleChangeService,
                                                final DeviceAssociationService _deviceAssociationService,
                                                final RequestPinService _requestPinService) {
    requestTheftLossMalfunctionService = _requestTheftLossMalfunctionService;
    requestVehicleLicensePlateChangeService = _requestVehicleLicensePlateChangeService;
    requestVehicleChangeService = _requestVehicleChangeService;
    deviceAssociationService = _deviceAssociationService;
    this.requestPinService = _requestPinService;
  }

  @Override
  public List<Richiesta> generateRichieste(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException {
    List<Richiesta> modificationRequest = new ArrayList<>();
      TipoRichiesta modificationOperationType = carrelloDTO.getTipoOperazioneModifica();
      log.debug("Generate Modification request of type: {}", modificationOperationType);
      switch (modificationOperationType) {
      case FURTO:
      case FURTO_CON_SOSTITUZIONE:
      case SMARRIMENTO:
      case SMARRIMENTO_CON_SOSTITUZIONE:
      case MALFUNZIONAMENTO:
      case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
      case DISATTIVAZIONE:
        modificationRequest = requestTheftLossMalfunctionService.generateModificationRequestDeviceService(carrelloDTO);
        break;

      case MEZZO_RITARGATO:
        modificationRequest = requestVehicleLicensePlateChangeService.generateModificationRequestDeviceService(carrelloDTO);
        break;

      case VARIAZIONE_TARGA:
        modificationRequest = requestVehicleChangeService.generateModificationRequestDeviceService(carrelloDTO);
        break;

      default:
        log.error("Operation type is not supported: " + modificationOperationType);
        break;
    }

    return modificationRequest;
  }



  @Override
  public List<Long> associaTarga(TipoDispositivoEnum tipoDispositivo, String identifierDevice, String vehicle) {
    log.info("generate Association between device: {} and Vehicle: {}", identifierDevice, vehicle);
    try {
      return deviceAssociationService.generateAssociationDeviceVehicle(tipoDispositivo, identifierDevice, vehicle);
    } catch (Exception e) {
      log.error("Exception on generate association between device identifier: " + identifierDevice + " and vehicle: " + vehicle + "", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Long> richiestaPin(TipoDispositivoEnum tipoDispositivo, String codiceCliente, String identifierDevice) {
    log.info("recover and send pin for tipoDispositivo:=[{}], codiceCliente:=[{}], identifierDevice:=[{}]", tipoDispositivo, codiceCliente, identifierDevice);
    try {
      return requestPinService.requestTrackycardPin(tipoDispositivo, codiceCliente, identifierDevice);
    } catch (Exception e) {
      log.error("Exception during recover and send pin for tipoDispositivo:=[{}], codiceCliente:=[{}], identifierDevice:=[{}]", tipoDispositivo, codiceCliente, identifierDevice, e);
      throw new RuntimeException(e);
    }
  }

}
