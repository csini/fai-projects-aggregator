package it.fai.ms.efservice.asyncjob;

import java.io.Serializable;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
//@Transactional
public class AsyncPollingJobServiceImpl {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());

  final AsyncJobCacheRepositoryImpl fsmAsyncRepo;

  public AsyncPollingJobServiceImpl(AsyncJobCacheRepositoryImpl _fsmAsyncRepo) {
    fsmAsyncRepo = _fsmAsyncRepo;
  }

  public Serializable getValue(final String uuid) {
    return fsmAsyncRepo.restore(uuid);
  }
  
  public StateChangeStatusFsm pollingRichiesteApiGateway(String identificativoJob) throws InterruptedException {
    StateChangeStatusFsm result = null;
    int count = 0;
    boolean response = false;
    while (!response) {
      Thread.sleep(500);
      if (count > 6) {
        response = true;
      }

      if (response == false) {
        result = (StateChangeStatusFsm) getValue(identificativoJob);
        log.debug("Result of {}: {}", identificativoJob, result);
        if (result == StateChangeStatusFsm.SUCCESS) {
          response = true;
        }

        count++;
      }
    }
    
    return result;
  }
}
