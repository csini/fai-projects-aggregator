package it.fai.ms.efservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_hardware_dispositivo")
public class TipoHardwareDispositivo implements Serializable {

  private static final long serialVersionUID = 7015873372644891308L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  private TipoDispositivo tipoDispositivo;

  @Column(name = "tipo_hardware")
  private String hardwareType;

  @Column(name = "default_hardware")
  private boolean defaultHardwareType = false;

  @Column(name = "descrizione")
  private String descrizione;

  @Column(name = "attivo")
  private boolean attivo = false;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }
  
  public TipoHardwareDispositivo tipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getHardwareType() {
    return hardwareType;
  }
  
  public TipoHardwareDispositivo hardwareType(String hardwareType) {
    this.hardwareType = hardwareType;
    return this;
  }

  public void setHardwareType(String hardwareType) {
    this.hardwareType = hardwareType;
  }

  public boolean isDefaultHardwareType() {
    return defaultHardwareType;
  }
  
  public TipoHardwareDispositivo defaultHardwareType(boolean defaultHardwareType) {
    this.defaultHardwareType = defaultHardwareType;
    return this;
  }

  public void setDefaultHardwareType(boolean defaultHarwareType) {
    this.defaultHardwareType = defaultHarwareType;
  }

  public String getDescrizione() {
    return descrizione;
  }
  
  public TipoHardwareDispositivo descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public boolean isAttivo() {
    return attivo;
  }
  
  public TipoHardwareDispositivo attivo(boolean attivo) {
    this.attivo = attivo;
    return this;
  }

  public void setAttivo(boolean attivo) {
    this.attivo = attivo;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoHardwareDispositivo [id=");
    builder.append(id);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", hardwareType=");
    builder.append(hardwareType);
    builder.append(", defaultHardwareType=");
    builder.append(defaultHardwareType);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", attivo=");
    builder.append(attivo);
    builder.append("]");
    return builder.toString();
  }

}
