package it.fai.ms.efservice.domain.enumeration;

import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * The TipoRichiesta enumeration.
 */
public enum TipoRichiesta {
  //@formatter:off
  NUOVO_DISPOSITIVO,
  NUOVO_ORDINE,
  FURTO,
  SMARRIMENTO,
  FURTO_CON_SOSTITUZIONE,
  SMARRIMENTO_CON_SOSTITUZIONE,
  VARIAZIONE_TARGA,
  MEZZO_RITARGATO,
  MALFUNZIONAMENTO_CON_SOSTITUZIONE,
  MALFUNZIONAMENTO,
  RESCISSIONE,
  SOSPENSIONE,
  RIATTIVAZIONE,
  ATTIVAZIONE_SERVIZIO,
  DISATTIVAZIONE_SERVIZIO,
  RIENTRO_DA_BACKOFFICE,
  ASSOCIA_TARGA,
  RICHIESTA_PIN, DISATTIVAZIONE
  ;
  //@formatter:on

  public static final List<TipoRichiesta> TIPORICHIESTE_MODIFICA = ImmutableList.of(
    TipoRichiesta.FURTO,
    TipoRichiesta.FURTO_CON_SOSTITUZIONE,
    TipoRichiesta.SMARRIMENTO,
    TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE,
    TipoRichiesta.MALFUNZIONAMENTO,
    TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE,
    TipoRichiesta.VARIAZIONE_TARGA,
    TipoRichiesta.MEZZO_RITARGATO

  );
  
  public static final List<TipoRichiesta> OPERAZIONI_MODIFICA_DISPOSITIVO = ImmutableList.of(
    TipoRichiesta.FURTO,
    TipoRichiesta.FURTO_CON_SOSTITUZIONE,
    TipoRichiesta.SMARRIMENTO,
    TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE,
    TipoRichiesta.MALFUNZIONAMENTO,
    TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE,
    TipoRichiesta.VARIAZIONE_TARGA

  );
}
