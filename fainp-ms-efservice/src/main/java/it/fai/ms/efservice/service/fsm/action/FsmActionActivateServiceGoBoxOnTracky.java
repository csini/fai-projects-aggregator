package it.fai.ms.efservice.service.fsm.action;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import com.google.common.collect.ImmutableSet;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.TrackyCardGoBoxService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class FsmActionActivateServiceGoBoxOnTracky implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final TrackyCardGoBoxService trackyCardGoBoxService;

  private Set<StatoDispositivo> statiDispositiviAttivi = ImmutableSet.of(StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                                                                         StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.ATTIVO_SPEDITO_A_FAI);

  public FsmActionActivateServiceGoBoxOnTracky(final TrackyCardGoBoxService _trackyCardGoBoxService) {
    trackyCardGoBoxService = _trackyCardGoBoxService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;

        ClienteFai clienteFai = null;
        Contratto contratto = richiesta.getContratto();
        if (contratto != null) {
          clienteFai = contratto.getClienteFai();
        }
        String targa = richiesta.getAssociazione();
        String nazione = richiesta.getCountry();
        String codiceCliente = (clienteFai != null) ? clienteFai.getCodiceCliente() : null;
        Optional<Dispositivo> optGoBox = trackyCardGoBoxService.findPendingGoBoxRelatedByRichiesta(richiesta);
        Optional<Dispositivo> optTrackycard = trackyCardGoBoxService.findActiveDeviceByTypeAndLicensePlateAndCountry(TipoDispositivoEnum.TRACKYCARD,
                                                                                                                 statiDispositiviAttivi,
                                                                                                                 targa, nazione,
                                                                                                                 codiceCliente);

        if (optTrackycard.isPresent()) {
          Dispositivo trackycard = optTrackycard.get();
          log.info("Found trackycard: {}", trackycard);

          StatoDispositivoServizio pedaggiAustriaOnTrackycard = retrieveStatoDispositivoServizioOrCreateNewOne(trackycard, "PEDAGGI_AUSTRIA");

          if (pedaggiAustriaOnTrackycard != null) {
            log.info("Update StatoDispositivo Servizio....");
            trackyCardGoBoxService.updateStato(pedaggiAustriaOnTrackycard, StatoDS.ATTIVO);
          }
          if (optGoBox.isPresent()) {
            Dispositivo goBox = optGoBox.get();
            log.info("Found goBox: {}", goBox);

            StatoDispositivoServizio pedaggiAustriaOnGoBox = retrieveStatoDispositivoServizioOrCreateNewOne(goBox, "PEDAGGI_AUSTRIA");

            if (pedaggiAustriaOnGoBox != null) {
              log.info("Update StatoDispositivo Servizio....");
              pedaggiAustriaOnGoBox.setPan(trackycard.getSeriale());
              trackyCardGoBoxService.updateStato(pedaggiAustriaOnGoBox, StatoDS.ATTIVO);
            }
          } else {
            log.warn("Not found Dispositivo GoBox related on Richiesta: {}", richiesta);
          }
        } else {
          log.warn("Not found Dispositivo TrackyCard related on Richiesta GoBox: {}", richiesta);
        }

      }
    }
  }

  private StatoDispositivoServizio retrieveStatoDispositivoServizioOrCreateNewOne(Dispositivo device, String nomeServizio) {
    Optional<StatoDispositivoServizio> optSds = device.getStatoDispositivoServizios()
      .stream()
      .filter(sds -> sds.getTipoServizio() != null && sds.getTipoServizio()
        .getNome()
        .equals(nomeServizio))
      .findFirst();

    StatoDispositivoServizio sds = null;
    if (optSds.isPresent()) {
      sds = optSds.get();
    } else {
      TipoServizio pedaggiAustria = trackyCardGoBoxService.findByNomeServizio(nomeServizio);
      sds = new StatoDispositivoServizio().tipoServizio(pedaggiAustria)
                                          .dispositivo(device)
                                          .dataAttivazione(Instant.now());
    }
    return sds;
  }

}
