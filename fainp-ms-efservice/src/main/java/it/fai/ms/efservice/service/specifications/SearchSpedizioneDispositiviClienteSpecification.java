package it.fai.ms.efservice.service.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import it.fai.ms.efservice.domain.SpedizioneDispositiviClienteView;
import it.fai.ms.efservice.domain.SpedizioneDispositiviClienteView_;
import it.fai.ms.efservice.service.dto.SearchSpedizioneDispositiviClienteDTO;

public class SearchSpedizioneDispositiviClienteSpecification implements Specification<SpedizioneDispositiviClienteView> {

  private final SearchSpedizioneDispositiviClienteDTO dto;
  
  public SearchSpedizioneDispositiviClienteSpecification(SearchSpedizioneDispositiviClienteDTO dto) {
    this.dto = dto;
  }

  private String likeIt(String param) {
    return "%" + param + "%";
  }

  @Override
  public Predicate toPredicate(Root<SpedizioneDispositiviClienteView> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
    List<Predicate> predicates = new ArrayList<>();

    if (StringUtils.isNotEmpty(dto.getCerca())) {
      predicates.add(builder.or(builder.like(root.get(SpedizioneDispositiviClienteView_.codiceAzienda), likeIt(dto.getCerca())),
                                builder.like(root.get(SpedizioneDispositiviClienteView_.ragioneSociale), likeIt(dto.getCerca()))));
    }
    
    if (StringUtils.isNotEmpty(dto.getCerca())) {
      predicates.add(builder.or(builder.like(root.get(SpedizioneDispositiviClienteView_.codiceAzienda), likeIt(dto.getCerca())),
                                builder.like(root.get(SpedizioneDispositiviClienteView_.ragioneSociale), likeIt(dto.getCerca()))));
    }

    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
  }
}
