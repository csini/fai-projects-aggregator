package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface ViaCardPuntoBluMessageProducerService {

  void notificationViaCardActivated(Richiesta richiesta);

}
