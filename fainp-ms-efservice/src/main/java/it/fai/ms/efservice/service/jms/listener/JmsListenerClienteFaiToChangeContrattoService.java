package it.fai.ms.efservice.service.jms.listener;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.service.jms.consumer.ClienteFaiToChangeStatusContrattoConsumer;

@Service
@Transactional
public class JmsListenerClienteFaiToChangeContrattoService extends JmsObjectMessageListenerTemplate<AziendaDMLDTO> implements JmsTopicListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Value("${application.isIntilialLoading:}")
  private String isInitialLoading;

  private final ClienteFaiToChangeStatusContrattoConsumer consumer;

  public JmsListenerClienteFaiToChangeContrattoService(final ClienteFaiToChangeStatusContrattoConsumer _consumer) {
    consumer = _consumer;
  }
  
  @PostConstruct
  public void postConstruct(){
    log.error(" ### isInitialLoading:" +  isInitialLoading + " ### ");
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_AZIENDE_SAVE;
  }

  @Override
  protected void consumeMessage(AziendaDMLDTO dmlDto) {
    if ("true".equalsIgnoreCase(isInitialLoading)) {
      if (log.isDebugEnabled()) log.debug("Is initial loading! returning from message elaboration...");
      return;
    }
    
    log.trace("receive Message: {}", dmlDto);
    if (isNotValidMessage(dmlDto)) {
      throw new RuntimeException("Not valid message to revoke contract: " + dmlDto);
    }
    consumer.consume(dmlDto);
  }

  private boolean isNotValidMessage(AziendaDMLDTO dmlDto) {
    boolean isNotValidMsg = false;
    if (dmlDto == null) {
      isNotValidMsg = true;
    }
    if (dmlDto != null && StringUtils.isBlank(dmlDto.getCodiceAzienda())) {
      isNotValidMsg = true;
    }
    if (dmlDto != null && StringUtils.isBlank(dmlDto.getStato())) {
      isNotValidMsg = true;
    }
    return isNotValidMsg;
  }

}
