package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toMap;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;

public class TestToMap {

  public TestToMap() {
    // TODO Auto-generated constructor stub
  }

  public static void main(String[] args) {
    TestToMap testToMap = new TestToMap();
    testToMap.test();
  }

  private void test() {

    List<Dispositivo> devices = new LinkedList<>();
    Dispositivo d1 = new Dispositivo();
    Richiesta o1 = new Richiesta();
    o1.addDispositivo(d1);
    o1.setIdentificativo("o1");
    d1.setIdentificativo("d1");
    d1.addRichiesta(o1);

    devices.add(d1);

    Dispositivo d3 = new Dispositivo();
    Richiesta o2 = new Richiesta();
    o2.setIdentificativo("o2");
    o2.addDispositivo(d3);
    d3.addRichiesta(o2);
    d3.setIdentificativo("d3");
    devices.add(d3);

    Dispositivo d2 = new Dispositivo();
    d2.addRichiesta(o1);
    d2.setIdentificativo("d2");
    devices.add(d2);

    Dispositivo d4 = new Dispositivo();
    d4.setIdentificativo("d4");
    devices.add(d4);

    Map<String, Set<String>> collect = devices.stream()
                                              .map(device -> device)
                                              .filter(device -> !device.getRichiestas()
                                                                       .isEmpty())
                                              .collect(toMap(k -> {
                                                String uuid = k.getRichiestas()
                                                               .stream()
                                                               .findFirst()
                                                               .get()
                                                               .getIdentificativo();
                                                return uuid;
                                              }, v -> {
                                                Set<String> deviceIds = new HashSet<>();
                                                deviceIds.add(v.getIdentificativo());
                                                return deviceIds;
                                              }, (s, devicesIds) -> {
                                                devicesIds.addAll(s);
                                                return devicesIds;
                                              }));

    System.out.println(collect);

  }

}
