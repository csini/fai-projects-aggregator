/**
 *
 */
package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.common.jms.dto.RichiestaNewDispositivoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

/**
 * @author Luca Vassallo
 */
public class FsmActionCreateNewRichiesta implements Action<StatoRichiesta, RichiestaEvent> {

  Logger logger = LoggerFactory.getLogger(getClass());

  private final JmsTopicSenderService senderJmsService;

  public FsmActionCreateNewRichiesta(JmsTopicSenderService senderJmsService) {
    this.senderJmsService = senderJmsService;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.action.Action#execute(org.springframework.statemachine.StateContext)
   */
  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Richiesta richiesta = null;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        richiesta = (Richiesta) object;

        switch (richiesta.getTipo()) {
        case FURTO_CON_SOSTITUZIONE:
        case SMARRIMENTO_CON_SOSTITUZIONE:
        case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
        case VARIAZIONE_TARGA:
        case MEZZO_RITARGATO:
          createNewOrder(richiesta);
          break;
        default:
          logger.warn("Order Type is: {}, so not generated new richiesta", richiesta.getTipo());
          break;
        }
      }
    }
  }

  private void createNewOrder(Richiesta richiesta) {
    Long idRichiesta = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    RichiestaNewDispositivoDTO dispositivoNewRichiestaDTO = new RichiestaNewDispositivoDTO(idRichiesta, identificativo);
    try {
      logger.info("Send message to generate new richiesta of sostitution from richiesta: " + identificativo);
      senderJmsService.publishRequestNewDispositivoMessage(dispositivoNewRichiestaDTO);
    } catch (Exception e) {
      logger.error("Error", e);
    }

  }

}
