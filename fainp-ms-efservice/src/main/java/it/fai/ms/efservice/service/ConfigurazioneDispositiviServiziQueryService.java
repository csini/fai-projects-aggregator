package it.fai.ms.efservice.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.*; // for static metamodels
import it.fai.ms.efservice.repository.ConfigurazioneDispositiviServiziRepository;
import it.fai.ms.efservice.service.dto.ConfigurazioneDispositiviServiziCriteria;

import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;

/**
 * Service for executing complex queries for ConfigurazioneDispositiviServizi entities in the database.
 * The main input is a {@link ConfigurazioneDispositiviServiziCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link ConfigurazioneDispositiviServizi} or a {@link Page} of {%link ConfigurazioneDispositiviServizi} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class ConfigurazioneDispositiviServiziQueryService extends QueryService<ConfigurazioneDispositiviServizi> {

    private final Logger log = LoggerFactory.getLogger(ConfigurazioneDispositiviServiziQueryService.class);


    private final ConfigurazioneDispositiviServiziRepository configurazioneDispositiviServiziRepository;

    public ConfigurazioneDispositiviServiziQueryService(ConfigurazioneDispositiviServiziRepository configurazioneDispositiviServiziRepository) {
        this.configurazioneDispositiviServiziRepository = configurazioneDispositiviServiziRepository;
    }

    /**
     * Return a {@link List} of {%link ConfigurazioneDispositiviServizi} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConfigurazioneDispositiviServizi> findByCriteria(ConfigurazioneDispositiviServiziCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<ConfigurazioneDispositiviServizi> specification = createSpecification(criteria);
        return configurazioneDispositiviServiziRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link ConfigurazioneDispositiviServizi} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConfigurazioneDispositiviServizi> findByCriteria(ConfigurazioneDispositiviServiziCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<ConfigurazioneDispositiviServizi> specification = createSpecification(criteria);
        return configurazioneDispositiviServiziRepository.findAll(specification, page);
    }

    /**
     * Function to convert ConfigurazioneDispositiviServiziCriteria to a {@link Specifications}
     */
    private Specifications<ConfigurazioneDispositiviServizi> createSpecification(ConfigurazioneDispositiviServiziCriteria criteria) {
        Specifications<ConfigurazioneDispositiviServizi> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ConfigurazioneDispositiviServizi_.id));
            }
            if (criteria.getModalitaGestione() != null) {
                specification = specification.and(buildSpecification(criteria.getModalitaGestione(), ConfigurazioneDispositiviServizi_.modalitaGestione));
            }
            if (criteria.getTipoDispositivoId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTipoDispositivoId(), ConfigurazioneDispositiviServizi_.tipoDispositivo, TipoDispositivo_.id));
            }
            if (criteria.getTipoServizioId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTipoServizioId(), ConfigurazioneDispositiviServizi_.tipoServizio, TipoServizio_.id));
            }
        }
        return specification;
    }

}
