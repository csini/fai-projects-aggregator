package it.fai.ms.efservice.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.efservice.domain.enumeration.StatoDS;

/**
 * A DTO for the StatoDispositivoServizio entity.
 */
public class StatoDispositivoServizioDTO implements Serializable {

    private Long id;

    private StatoDS stato;

    private String pan;

    private Instant dataAttivazione;

    private Instant dataDisattivazione;

    private Instant dataScadenza;

    private Long tipoServizioId;

    private String tipoServizioNome;

    private Long dispositivoId;

    private String dispositivoSeriale;

    private String dispositivoIdentificativo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StatoDS getStato() {
        return stato;
    }

    public void setStato(StatoDS stato) {
        this.stato = stato;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Instant getDataAttivazione() {
        return dataAttivazione;
    }

    public void setDataAttivazione(Instant dataAttivazione) {
        this.dataAttivazione = dataAttivazione;
    }

    public Instant getDataDisattivazione() {
        return dataDisattivazione;
    }

    public void setDataDisattivazione(Instant dataDisattivazione) {
        this.dataDisattivazione = dataDisattivazione;
    }

    public Instant getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(Instant dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public Long getTipoServizioId() {
        return tipoServizioId;
    }

    public void setTipoServizioId(Long tipoServizioId) {
        this.tipoServizioId = tipoServizioId;
    }

    @JsonProperty("tipoServizio")
    public String getTipoServizio() {
        return tipoServizioNome;
    }

    public Long getDispositivoId() {
        return dispositivoId;
    }

    public void setDispositivoId(Long dispositivoId) {
        this.dispositivoId = dispositivoId;
    }

    public String getDispositivoSeriale() {
        return dispositivoSeriale;
    }

    public void setDispositivoSeriale(String dispositivoSeriale) {
        this.dispositivoSeriale = dispositivoSeriale;
    }

    public String getDispositivoIdentificativo() {
      return dispositivoIdentificativo;
    }

    public void setDispositivoIdentificativo(String dispositivoIdentificativo) {
      this.dispositivoIdentificativo = dispositivoIdentificativo;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StatoDispositivoServizioDTO statoDispositivoServizioDTO = (StatoDispositivoServizioDTO) o;
        if(statoDispositivoServizioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), statoDispositivoServizioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

  @Override
  public String toString() {
    return "StatoDispositivoServizioDTO{" +
      "id=" + id +
      ", stato=" + stato +
      ", pan='" + pan + '\'' +
      ", dataAttivazione=" + dataAttivazione +
      ", dataDisattivazione=" + dataDisattivazione +
      ", dataScadenza=" + dataScadenza +
      ", tipoServizioId=" + tipoServizioId +
      ", tipoServizioNome='" + tipoServizioNome + '\'' +
      ", dispositivoId=" + dispositivoId +
      ", dispositivoSeriale='" + dispositivoSeriale + '\'' +
      ", dispositivoIdentificativo='" + dispositivoIdentificativo + '\'' +
      '}';
  }

  public String getTipoServizioNome() {
    return tipoServizioNome;
  }

  public void setTipoServizioNome(String tipoServizioNome) {
    this.tipoServizioNome = tipoServizioNome;
  }
}
