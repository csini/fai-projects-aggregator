package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OrdineFornitore and its DTO OrdineFornitoreDTO.
 */
@Mapper(componentModel = "spring", uses = {AnagraficaGiacenzaMapper.class})
public interface OrdineFornitoreMapper extends EntityMapper<OrdineFornitoreDTO, OrdineFornitore> {

    @Mapping(source = "anagraficaGiacenza.id", target = "anagraficaGiacenzaId")
    OrdineFornitoreDTO toDto(OrdineFornitore ordineFornitore);

    @Mapping(source = "anagraficaGiacenzaId", target = "anagraficaGiacenza")
    OrdineFornitore toEntity(OrdineFornitoreDTO ordineFornitoreDTO);

    default OrdineFornitore fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrdineFornitore ordineFornitore = new OrdineFornitore();
        ordineFornitore.setId(id);
        return ordineFornitore;
    }
}
