package it.fai.ms.efservice.service.fsm.exception;

public class FsmSwitchStateException extends Exception {

  private static final long serialVersionUID = 3917433148209276818L;

  public FsmSwitchStateException() {
    this(FsmSwitchStateException.class.toString());
  }

  public FsmSwitchStateException(String msgId, Throwable cause) {
    super(msgId, cause);
  }

  public FsmSwitchStateException(Throwable cause) {
    super(cause);
  }

  public FsmSwitchStateException(String message) {
    super(message);
  }

}