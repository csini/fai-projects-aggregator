package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity IndirizzoSpedizioneOrdini and its DTO IndirizzoSpedizioneOrdiniDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IndirizzoSpedizioneOrdiniMapper extends EntityMapper <IndirizzoSpedizioneOrdiniDTO, IndirizzoSpedizioneOrdini> {
    
    
    default IndirizzoSpedizioneOrdini fromId(Long id) {
        if (id == null) {
            return null;
        }
        IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini = new IndirizzoSpedizioneOrdini();
        indirizzoSpedizioneOrdini.setId(id);
        return indirizzoSpedizioneOrdini;
    }
}
