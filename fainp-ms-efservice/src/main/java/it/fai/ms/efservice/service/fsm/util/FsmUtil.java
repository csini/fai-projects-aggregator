package it.fai.ms.efservice.service.fsm.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaMappingService;

@Service
public class FsmUtil {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private FsmRichiestaMappingService fsmMap;

  public List<String> getAvailableUserCommand(String idFsm, Richiesta richiesta) {
    List<String> userAvailableCommand = new ArrayList<>();
    AbstractFsmRichiesta fsm = fsmMap.getFSM(idFsm);
    if (fsm == null) {
      log.info("FSM: " + null);
      return userAvailableCommand;
    }

    log.info("FSM: " + fsm.getClass()
                          .getName());
    userAvailableCommand = fsm.getUserAvailableCommand(richiesta);
    return userAvailableCommand;
  }

  public String getOperazioniPossibili(String idFsm, Richiesta richiesta) {
    AbstractFsmRichiesta fsm = fsmMap.getFSM(idFsm);
    if (fsm == null) {
      log.error("FSM: " + null);
      return "";
    }

    log.info("FSM: " + fsm.getClass()
                          .getName());
    String operations = fsm.getOperazioniPossibili(richiesta);
    if(StringUtils.isBlank(operations)) {
      log.warn("Not found operations for FSM {} and Stato Richiesta: {}", fsm, richiesta.getStato());
    }
    
    return operations;
  }

}
