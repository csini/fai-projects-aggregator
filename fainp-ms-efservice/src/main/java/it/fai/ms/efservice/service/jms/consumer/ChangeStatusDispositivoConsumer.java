package it.fai.ms.efservice.service.jms.consumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.dto.DispositivoFSMChangeStatusDTO;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;

@Service
@Transactional
public class ChangeStatusDispositivoConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt dispositivoRepoExt;
  private final FsmDispositivo           fsmDispositivo;

  private final List<DispositivoEvent> eventsDevice = Arrays.asList(DispositivoEvent.ATTIVO, DispositivoEvent.SPEDITO_DA_FAI,
                                                                    DispositivoEvent.SPEDITO_DAL_FORNITORE);

  /**
   * @param dispositivoRepoExt
   * @param fsmDispositivo
   */
  public ChangeStatusDispositivoConsumer(DispositivoRepositoryExt dispositivoRepoExt, FsmDispositivo fsmDispositivo) {
    this.dispositivoRepoExt = dispositivoRepoExt;
    this.fsmDispositivo = fsmDispositivo;
  }

  public void changeStatusByDTO(DispositivoFSMChangeStatusDTO dto) {
    String command = dto.getCommand();
    String identificativo = dto.getIdentificativoDispositivo();

    log.debug("Find dispositivo by identificativo: {}", identificativo);

    Dispositivo dispositivo = dispositivoRepoExt.findOneByIdentificativo(identificativo);
    if (dispositivo != null) {
      try {
        DispositivoEvent eventDispositivo = DispositivoEvent.valueOf(command);
        boolean availableCommand = fsmDispositivo.isAvailableCommand(eventDispositivo, dispositivo);
        if (!availableCommand) {

          boolean isTheSame = fsmDispositivo.isAlreadyInTargetState(eventDispositivo, dispositivo);

          boolean isAlreadyActive = isAlreadyDeviceActiveAndIsCommandToActivateDevice(eventDispositivo, dispositivo);

          if (isTheSame) {
            log.warn("Device is already in the desidered ( command={} ) status: {}", eventDispositivo, dispositivo);
          } else if (isAlreadyActive) {
            log.warn("Command is {} and Device is already {}, so skip change status", eventDispositivo,
                     dispositivo != null ? dispositivo.getStato() : null);
          } else {

            String messageError = String.format("Command %s is not available for device %s in status %s", eventDispositivo, identificativo,
                                                dispositivo.getStato());
            log.error(messageError);
            Thread.sleep(5000);
            throw new RuntimeException(messageError);
          }
        }

        log.info("Call SWITCH STATE with Command: " + eventDispositivo.name() + " for Dispositivo [" + dispositivo.getIdentificativo()
                 + "]");
        Dispositivo dispositivoWithStateChange = fsmDispositivo.executeCommandToChangeState(eventDispositivo, dispositivo);
        dispositivoRepoExt.save(dispositivoWithStateChange);
        log.info("New state dispositivo [Identificativo: {} - Stato: {}]", dispositivoWithStateChange.getIdentificativo(),
                 dispositivoWithStateChange.getStato()
                                           .name());
      } catch (Exception e) {
        log.error("Exception on change status Dispositivo by DTO: ", e);
        throw new RuntimeException(e);
      }
    } else {
      log.error("Not found Dispositivo with identificativo {}", identificativo);
    }
  }

  private boolean isAlreadyDeviceActiveAndIsCommandToActivateDevice(DispositivoEvent eventDispositivo, Dispositivo dispositivo) {
    StatoDispositivo stato = dispositivo.getStato();
    if (stato.isGreen() && eventsDevice.contains(eventDispositivo)) {
      return true;
    }
    return false;
  }
}
