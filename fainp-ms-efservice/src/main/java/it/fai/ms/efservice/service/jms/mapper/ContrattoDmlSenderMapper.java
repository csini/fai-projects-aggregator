package it.fai.ms.efservice.service.jms.mapper;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.ContrattoDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractDmlSenderMapper;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;

@Component
public class ContrattoDmlSenderMapper extends AbstractDmlSenderMapper<ContrattoDMLDTO, Contratto> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public ContrattoDmlSenderMapper() {
    super(ContrattoDMLDTO::new);
  }

  @Override
  protected ContrattoDMLDTO toDMLDTO(Contratto contract, ContrattoDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyDMLDTO(String.valueOf(contract.getIdentificativo()));
    }
    dml.setCodContrattoCliente(contract.getCodContrattoCliente());
    ClienteFai clienteFai = contract.getClienteFai();
    log.debug("ClienteFai contratto {}: {}", contract.getIdentificativo(), clienteFai);
    dml.setCodiceAzienda((clienteFai != null) ? clienteFai.getCodiceCliente() : null);
    Produttore produttore = contract.getProduttore();
    log.debug("Produttore contratto {}: {}", contract.getIdentificativo(), produttore);
    dml.setCodiceFornitoreNav((produttore != null) ? produttore.getCodiceFornitoreNav() : null);
    dml.setProduttoreNome((produttore != null) ? produttore.getNome() : null);
    dml.setDataUltimaVariazione(contract.getDataModificaStato());
    dml.setDmlRevisionTimestamp(Instant.now());
    dml.setDmlUniqueIdentifier(contract.getIdentificativo());
    dml.setOperazioneDisponibili(contract.getOperazioniPossibili());
    dml.setStato(contract.getStato()
                         .name());
    dml.setPrimario(contract.isPrimario() == null ? true : contract.isPrimario());
    dml.setNazioneFatturazione(contract.getPaeseRiferimentoIva());
    log.debug("DML contratto: {}", dml);
    return dml;
  }

}
