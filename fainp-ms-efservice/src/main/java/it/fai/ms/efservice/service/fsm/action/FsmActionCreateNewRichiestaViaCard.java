package it.fai.ms.efservice.service.fsm.action;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionCreateNewRichiestaViaCard implements Action<StatoRichiesta, RichiestaEvent> {
  
  private final static Logger log = LoggerFactory.getLogger(FsmActionCreateNewRichiestaViaCard.class);
  
  private final FsmSenderToQueue senderFsmUtil;
  
  public FsmActionCreateNewRichiestaViaCard(FsmSenderToQueue senderFsmUtil) {
    this.senderFsmUtil = senderFsmUtil;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    // TODO request for new Via Card;
    
    // il numero necessario di via card da ordinare va recuperato dall'richiesta attualmente creato;
    
    // Change status Dispositivi.
    
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        Set<Dispositivo> dispositivos = richiesta.getDispositivos();
        sendMessageToChangeStatusDispositivi(dispositivos);
      }
    }

  }
  
  private void sendMessageToChangeStatusDispositivi(Set<Dispositivo> dispositivi) {
    for (Dispositivo dispositivo : dispositivi) {
      log.debug("Send message to change status for dispositivo {}", dispositivo.getIdentificativo());
      senderFsmUtil.sendMessageForChangeStatoDispositivo(dispositivo, DispositivoEvent.TSAT_DEPOSITO_ATTESA_SPEDIZIONE);
    }
    log.debug("FINISH send message to change status for dispositivi");
  }

}
