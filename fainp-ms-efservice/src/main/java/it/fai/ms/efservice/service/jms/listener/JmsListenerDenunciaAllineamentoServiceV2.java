package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.TelepassEuDenunciaAllegatoDTO;
import it.fai.ms.efservice.service.jms.consumer.DenunciaDaAllineamentoConsumer;

@Service
@Transactional
public class JmsListenerDenunciaAllineamentoServiceV2 extends JmsObjectMessageListenerTemplate<TelepassEuDenunciaAllegatoDTO> implements JmsQueueListener {

  private final Logger                         log = LoggerFactory.getLogger(getClass());
  private final DenunciaDaAllineamentoConsumer denunciaDaAllineamentoConsumer;

  public JmsListenerDenunciaAllineamentoServiceV2(DenunciaDaAllineamentoConsumer denunciaDaAllineamentoConsumer) throws Exception {
    this.denunciaDaAllineamentoConsumer = denunciaDaAllineamentoConsumer;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.TELEPASS_EU_EVENTI_ALLEGATO_DENUNCIA;
  }

  @Override
  protected void consumeMessage(TelepassEuDenunciaAllegatoDTO dto) {
    try {
      denunciaDaAllineamentoConsumer.ricevutaDenuncia(dto);
    } catch (Exception e) {
      log.error("Error", e);
      throw new RuntimeException(e);
    }

  }

}