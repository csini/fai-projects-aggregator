package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmContratto;
import it.fai.ms.efservice.service.mapper.ContrattoMapper;

@Service
@Transactional
public class ContrattoServiceImplExt implements ContrattoServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ContrattoRepositoryExt    contrattoRepoExt;
  private final FsmContratto              fsmContratto;
  private final TipoDispositivoServiceExt tipoDispostivoService;
  private final ClienteFaiServiceExt      clienteService;
  private final ContrattoMapper           contrattoMapper;

  @Autowired
  public ContrattoServiceImplExt(final ContrattoRepositoryExt _contractRepository, final FsmContratto _fsmContratto,
                                 TipoDispositivoServiceExt _tipoDispostivoService, ClienteFaiServiceExt _clienteService,
                                 ContrattoMapper _contrattoMapper) {

    this.contrattoRepoExt = _contractRepository;
    this.fsmContratto = _fsmContratto;
    this.tipoDispostivoService = _tipoDispostivoService;
    this.clienteService = _clienteService;
    this.contrattoMapper = _contrattoMapper;
  }

  @Override
  public Contratto createContrattoByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai) {
    Contratto contract = new Contratto();
    contract.setClienteFai(clienteFai);
    contract.setProduttore(produttore);
    contract.setStato(StatoContratto.ATTIVO);
    contract.setDataModificaStato(Instant.now());
    fsmContratto.setOperazioniPossibili(contract);
    try {
      contract = save(contract);
    } catch (Exception e) {
      log.error("Contratto Exception", e);
      throw new RuntimeException(e);
    }
    log.debug("Contract saved: {}", contract);
    return contract;
  }

  @Override
  public Contratto setOperazioniPossibiliOnContratto(Contratto contract) {
    fsmContratto.setOperazioniPossibili(contract);
    return contract;
  }

  @Override
  public Optional<Contratto> findByCodiceContrattoAndNomeProduttore(final String _codiceContratto, final String nomeProduttore) {
    return contrattoRepoExt.findFirstByCodContrattoClienteAndProduttore_nome(_codiceContratto, nomeProduttore);
  }

  @Override
  public Optional<Contratto> findContrattoByProduttoreAndCodiceAzienda(Produttore produttore, String codiceAzienda) {
    Optional<Contratto> optContract = Optional.ofNullable(null);
    if (produttore != null && StringUtils.isNotBlank(codiceAzienda)) {
      String nomeProduttore = produttore.getNome();
      log.debug("Find contratto by Produttore {} and codice azienda {}", nomeProduttore, codiceAzienda);
      List<Contratto> contracts = contrattoRepoExt.findByNomeProduttoreAndCodiceAzienda(nomeProduttore, codiceAzienda);
      if (contracts != null && !contracts.isEmpty()) {
        optContract = contracts.stream()
                               .filter(c -> c.getStato() != StatoContratto.REVOCATO)
                               .filter(c -> c.isPrimario())
                               .findFirst();
      } else {
        log.warn("Not found contracts by {} and {}", nomeProduttore, codiceAzienda);
      }
    }

    return optContract;
  }

  @Override
  public Contratto save(Contratto contratto) {
    Contratto contractSaved = contrattoRepoExt.save(contratto);
    log.debug("Saved Contract: {}", contractSaved);
    if (contractSaved.getCodContrattoCliente() != null) {
      contrattoRepoExt.flush();
    }
    return contractSaved;
  }

  @Override
  public void delete(Contratto contratto) {
    log.info("Delete contract {}", contratto);
    contrattoRepoExt.delete(contratto);

    if (contratto.getCodContrattoCliente() != null) {
      contrattoRepoExt.flush();
    }
  }

  @Override
  public boolean executeFsm(Map<String, String> mapCodContrattoProduttore, String operazione, String nota) {
    ContrattoEvent command = ContrattoEvent.valueOf(operazione);

    Set<String> codiciContratto = mapCodContrattoProduttore.keySet();
    Set<Boolean> esiti = codiciContratto.stream()
                                        .map(codiceContratto -> {
                                          String nomeProduttore = mapCodContrattoProduttore.get(codiceContratto);
                                          Optional<Contratto> contrattoOpt = findByCodiceContrattoAndNomeProduttore(codiceContratto,
                                                                                                                    nomeProduttore);
                                          if (contrattoOpt.isPresent()) {
                                            Contratto contratto = contrattoOpt.get();
                                            StatoContratto statoIniziale = contratto.getStato();
                                            try {
                                              contratto = execeuteChangeStatuContract(contratto, command, nota);
                                            } catch (Exception e) {
                                              log.error("Exception on change status to contratto: " + codiceContratto, e);
                                              throw new RuntimeException(e);
                                            }
                                            contratto = save(contratto);
                                            log.info("Changed status FROM {} TP {} for Contratto with code {} ", statoIniziale,
                                                     contratto.getStato(), codiceContratto);
                                            return true;
                                          } else {
                                            log.warn("Contratto not found for codice {}", codiceContratto);
                                            return false;
                                          }
                                        })
                                        .collect(toSet());

    boolean isPresent = esiti.stream()
                             .filter(b -> b == false)
                             .findFirst()
                             .isPresent();
    if (isPresent) {
      log.warn("Per alcuni contratti selezionati non è stato possibile efffettuare il cambio stato");
    }

    return true;
  }

  @Override
  public Contratto findContrattoByServiceNameAndCodiceAzienda(String nomeServizio, String codiceAzienda) {
    return contrattoRepoExt.findByServiceNameAndCodiceAzienda(nomeServizio, codiceAzienda)
                           .stream()
                           .findFirst()
                           .orElse(null);
  }

  @Override
  public List<Contratto> findContrattoByCodiceAziendaAndTipoDispositivo(String codiceAzienda, TipoDispositivoEnum tipoDispositivo) {

    TipoDispositivo tp = tipoDispostivoService.findOneByNome(tipoDispositivo);
    ClienteFai cliente = clienteService.findByCodiceCliente(codiceAzienda);
    return contrattoRepoExt.findByClienteFaiAndProduttore(cliente, tp.getProduttore());
  }

  @Override
  public List<ContrattoDTO> findAllByVehicleUuid(String uuidVeicolo) {
    log.debug("Request all contract by vehicle uuid : {}", uuidVeicolo);
    return contrattoRepoExt.findAllByVehicleUuid(uuidVeicolo)
                           .stream()
                           .map(contrattoMapper::toDto)
                           .collect(Collectors.toList());
  }

  @Override
  public List<Contratto> findByCodiceContratto(final String codiceContratto) {
    log.debug("Find all contract by codice contratto: {}", codiceContratto);
    return contrattoRepoExt.findByCodContrattoCliente(codiceContratto);
  }

  @Override
  public boolean checkContrattoByCodiceClienteAndTipoDispositivo(String codiceCliente, TipoDispositivoEnum tipoDispositivo) {
    log.debug("Find contratto by codice cliente: {} and tipo dispositivo: {}", codiceCliente, tipoDispositivo);
    Optional<Contratto> optContratto = contrattoRepoExt.findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue(codiceCliente,
                                                                                                                                         tipoDispositivo);

    return optContratto.isPresent();
  }

  @Override
  public ContrattoDTO updateCodiceContratto(String identifier, String newCode) {
    log.debug("Request to update contract : {} with the new contract code : {}", identifier, newCode);
    ContrattoDTO contractDTO = null;
    Optional<Contratto> contrattoOpt = contrattoRepoExt.findByIdentificativo(identifier);
    if (contrattoOpt.isPresent()) {
      Contratto contratto = contrattoOpt.get();
      contratto.setCodContrattoCliente(newCode);
      contractDTO = contrattoMapper.toDto(save(contratto));
    }
    return contractDTO;
  }

  @Override
  public boolean existsCodContrattoCliente(String codContrattoCliente, Long produttoreId) {
    log.debug("Request to exists by codContrattoCliente:=[{}], produttoreId:=[{}]", codContrattoCliente, produttoreId);
    return contrattoRepoExt.existsByCodContrattoClienteAndProduttore_Id(codContrattoCliente, produttoreId);
  }

  @Override
  public boolean existsCodContrattoClienteOnDifferentClienteFai(Long myClienteFaiId, String codContrattoCliente, Long produttoreId) {
    log.debug("Request to exists by codContrattoCliente:=[{}], produttoreId:=[{}] but not myClienteFaiId:=[{}]", myClienteFaiId,
              codContrattoCliente, produttoreId);
    return contrattoRepoExt.existsByCodContrattoClienteAndProduttore_IdAndClienteFai_IdNot(codContrattoCliente, produttoreId,
                                                                                           myClienteFaiId);
  }

  @Override
  public List<Contratto> findAll() {
    return contrattoRepoExt.findAll();
  }

  @Override
  public Optional<Contratto> findByIdentificativo(String identificativo) {
    return contrattoRepoExt.findByIdentificativo(identificativo);
  }

  @Override
  public Contratto fsmChangeStatus(Contratto contract, ContrattoEvent command) throws Exception {
    String nota = "";
    contract = execeuteChangeStatuContract(contract, command, nota);
    contract = save(contract);
    return contract;
  }

  private Contratto execeuteChangeStatuContract(Contratto contract, ContrattoEvent command, String nota) throws Exception {
    contract = fsmContratto.executeCommandToChangeState(command, contract, nota);
    fsmContratto.setOperazioniPossibili(contract);
    return contract;
  }

  @Override
  public List<Contratto> findByCodiceCliente(String codiceCliente) {
    return contrattoRepoExt.findByClienteFai_codiceCliente(codiceCliente);
  }

  @Override
  public boolean isAvailableCommandFsm(Contratto contract, ContrattoEvent event) {
    return fsmContratto.isAvailableCommand(event, contract);
  }

  @Override
  public boolean existsByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai) {
    log.debug("Request to exists by Produttore '{}' and ClienteFai '{}'", produttore, clienteFai);
    return contrattoRepoExt.existsByProduttoreAndClienteFai(produttore, clienteFai);
  }

  @Override
  public List<Contratto> contractsNotFound(final List<String> contracts) {
    log.debug("Request to contractsNotFound");
    List<Contratto> arr = new ArrayList<>();

    contrattoRepoExt.findAll()
                    .stream()
                    .forEach(c -> {
                      if (!contracts.contains(c.getCodContrattoCliente())) {
                        arr.add(c);
                      }
                    });

    return arr;
  }

  @Override
  public List<Contratto> findAllByProduttoreAndCodiceAzienda(Produttore produttore, String codiceCliente) {
    String nomeProduttore = produttore.getNome();
    log.debug("Find all Contracts by Produttore: {} and Codice Azienda: {}", nomeProduttore, codiceCliente);
    return contrattoRepoExt.findByNomeProduttoreAndCodiceAzienda(nomeProduttore, codiceCliente);
  }

  @Override
  public void setPrimarioFalse(List<Contratto> contracts) {
    contracts.forEach(c -> {
      Contratto contract = save(c.primario(false));
      log.trace("Contratto updated: {}", contract);
    });

  }

  @Override
  @Transactional(readOnly = true)
  public List<Contratto> findAllByProduttoreAndCodiceClienteAndStatusIn(String nomeProduttore, String codiceCliente,
                                                                        List<StatoContratto> statusContract) {
    log.debug("Search contracts by codice cliente {}, produttore {}, and status {}", codiceCliente,
              nomeProduttore, statusContract);
    List<Contratto> contracts = contrattoRepoExt.findByClienteFai_codiceClienteAndProduttore_nomeAndStatoIn(codiceCliente,
                                                                                                            nomeProduttore,
                                                                                                            statusContract);
    return contracts;
  }

}
