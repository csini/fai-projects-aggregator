package it.fai.ms.efservice.service.fsm.config.telepass.sat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = "ModTSatFurtoSmarrimento")
public class FsmModificaTSatFurtoSmarrimentoConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTSatFurtoSmarrimentoConfig.class);

  private final FsmSenderToQueue senderFsmService;

  public FsmModificaTSatFurtoSmarrimentoConfig(FsmSenderToQueue _senderFsmService) {
    senderFsmService = _senderFsmService;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmModificaTSatFurtoSmarrimento");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT, ctx -> log.trace("Initial: " + ctx.getTarget()
                                                                                              .getIds()))
          .states(StatoRichiestaUtil.getStateOfModificateTSatFurtoSmarrimento());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.SMARRIMENTO)
               .guard(new GuardSmarrimento())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.FURTO)
               .guard(new GuardFurto())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.SMARRIMENTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               //.action(new FsmActionFurtoSmarrimento(senderFsmService)) //FIXME-fsm commentato perchè l'evento è già stato lanciato sopra
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.FURTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               //.action(new FsmActionFurtoSmarrimento(senderFsmService)) //FIXME-fsm commentato perchè l'evento è già stato lanciato sopra

               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.EVASO)
               .action(new FsmActionGeneric());
  }

}
