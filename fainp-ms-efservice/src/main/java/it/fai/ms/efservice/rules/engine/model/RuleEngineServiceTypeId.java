package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class RuleEngineServiceTypeId implements Serializable {

  private static final long serialVersionUID = 3860587883885254063L;

  public static RuleEngineServiceTypeId ofWildcard() {
    return new RuleEngineServiceTypeId("*");
  }

  private String id;

  public RuleEngineServiceTypeId(final String _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineServiceType Id is mandatory"));
  }

  @Override
  public boolean equals(final Object obj) {
    boolean isEquals = false;
    if (getClass().isInstance(obj)) {
      isEquals = Objects.equals(((RuleEngineServiceTypeId) obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineServiceTypeId [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
