package it.fai.ms.efservice.wizard.model.matrix;

import java.io.Serializable;

public abstract class WizardMatrixVehicle implements Serializable {

  private static final long serialVersionUID = 8149634296573549128L;

  private String  uuid;
  private String  euroClass;
  private String  licensePlate;
  private String  targaNazione;
  private boolean hasDevicesActive;
  private boolean expiring;
  private boolean expired;

  public String getEuroClass() {
    return euroClass;
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
  }

  public String getUuid() {
    return uuid;
  }

  public WizardMatrixVehicle(final String _id) {
    uuid = _id;
  }

  public String getTargaNazione() {
    return targaNazione;
  }

  public void setTargaNazione(String targaNazione) {
    this.targaNazione = targaNazione;
  }

  public boolean isHasDevicesActive() {
    return hasDevicesActive;
  }

  public void setHasDevicesActive(boolean hasDevicesActive) {
    this.hasDevicesActive = hasDevicesActive;
  }

  public boolean isExpiring() {
    return expiring;
  }

  public void setExpiring(boolean expiring) {
    this.expiring = expiring;
  }

  public boolean isExpired() {
    return expired;
  }

  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardMatrixVehicle [uuid=");
    builder.append(uuid);
    builder.append(", euroClass=");
    builder.append(euroClass);
    builder.append(", licensePlate=");
    builder.append(licensePlate);
    builder.append(", targaNazione=");
    builder.append(targaNazione);
    builder.append(", hasDevicesActive=");
    builder.append(hasDevicesActive);
    builder.append(", expiring=");
    builder.append(expiring);
    builder.append(", expired=");
    builder.append(expired);
    builder.append("]");
    return builder.toString();
  }
}
