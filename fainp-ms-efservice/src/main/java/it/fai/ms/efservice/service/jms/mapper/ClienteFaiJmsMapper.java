package it.fai.ms.efservice.service.jms.mapper;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.enumeration.StatoAzienda;

public class ClienteFaiJmsMapper {

  public ClienteFai toClienteFai(AziendaDMLDTO dml, ClienteFai ord) {

    if (ord == null) {
      ord = new ClienteFai();
    }

    ord.setCap(dml.getCap());
    ord.setCitta(dml.getCitta());
    ord.setCodiceAgente(dml.getCodiceAgente());
    ord.setCodiceCliente(dml.getCodiceAzienda());
    //ord.setCodiceClienteFatturazione(dml.getCodiceClienteFatturazione());
    ord.setCodiceFiscale(dml.getCodiceFiscale());
    ord.setCognomeLegaleRappresentante(dml.getCognomeLegaleRappresentante());
    ord.setConsorzioPadre(dml.getConsorzioPadre());
    ord.setContattoTelefonicoLegaleRappresentante(dml.getContattoTelefonicoLegaleRappresentante());
    ord.setDescrizioneRaggruppamentoImpresa(dml.getDescrizioneRaggruppamentoImpresa());

    ord.setEmail(dml.getEmail());
    ord.setEmailLegaleRappresentante(dml.getEmailLegaleRappresentante());
    ord.setNomeLegaleRappresentante(dml.getNomeLegaleRappresentante());
    ord.setPaese(dml.getPaese());
    ord.setPartitaIva(dml.getPartitaIva());
    ord.setProvincia(dml.getProvincia());
    ord.setRaggruppamentoImpresa(dml.getRaggruppamentoImpresa());
    ord.setRagioneSociale(dml.getRagioneSociale());

    ord.setStato(StatoAzienda.valueOf(dml.getStato()));
    ord.setVia(dml.getVia());

    ord.setDmlUniqueIdentifier(dml.getDmlUniqueIdentifier());
    ord.setDmlRevisionTimestamp(dml.getDmlRevisionTimestamp());

    ord.setNomeIntestazioneTessere(dml.getIntestazioneTessere());

    return ord;
  }

  public AziendaDMLDTO toAziendaDMLDTO(ClienteFai src, AziendaDMLDTO to) {

    if (to == null) {
      to = new AziendaDMLDTO();
    }

    to.setCap(src.getCap());
    to.setCitta(src.getCitta());
    to.setCodiceAgente(src.getCodiceAgente());
    to.setCodiceAzienda(src.getCodiceCliente());
    //to.setCodiceClienteFatturazione(src.getCodiceClienteFatturazione());
    to.setCodiceFiscale(src.getCodiceFiscale());
    to.setCognomeLegaleRappresentante(src.getCognomeLegaleRappresentante());
    to.setConsorzioPadre(src.isConsorzioPadre());
    to.setContattoTelefonicoLegaleRappresentante(src.getContattoTelefonicoLegaleRappresentante());
    to.setDescrizioneRaggruppamentoImpresa(src.getDescrizioneRaggruppamentoImpresa());

    to.setEmail(src.getEmail());
    to.setEmailLegaleRappresentante(src.getEmailLegaleRappresentante());
    to.setNomeLegaleRappresentante(src.getNomeLegaleRappresentante());
    to.setPaese(src.getPaese());
    to.setPartitaIva(src.getPartitaIva());
    to.setProvincia(src.getProvincia());
    to.setRaggruppamentoImpresa(src.getRaggruppamentoImpresa());
    to.setRagioneSociale(src.getRagioneSociale());

    to.setStato(src.getStato()
                   .name());
    to.setVia(src.getVia());

    to.setDmlUniqueIdentifier(src.getDmlUniqueIdentifier());
    to.setDmlRevisionTimestamp(src.getDmlRevisionTimestamp());

    to.setIntestazioneTessere(src.getNomeIntestazioneTessere());

    return to;
  }

}
