package it.fai.ms.efservice.service.jms.listener.device;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceServiceActivatedMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceServiceActivatedConsumer;

@Service
@Transactional
public class JmsListenerDeviceServiceActivated implements MessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceServiceActivatedConsumer consumer;

  @Autowired
  public JmsListenerDeviceServiceActivated(DeviceServiceActivatedConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  public void onMessage(final Message _message) {
    DeviceServiceActivatedMessage activationServiceMessage = null;
    try {
      try {
        activationServiceMessage = (DeviceServiceActivatedMessage) ((ObjectMessage) _message).getObject();
        _log.info("Received jms message {}", activationServiceMessage);
        consumer.consume(activationServiceMessage);
      } catch (@SuppressWarnings("unused") final ClassCastException _e) {
        _log.warn("JMS ObjectMessage isn't {}", DeviceServiceActivatedMessage.class.getSimpleName());
      }
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

}