package it.fai.ms.efservice.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the AssociazioneDV entity.
 */
public class AssociazioneDVDTO implements Serializable {

    private Long id;

    private Instant data;

    private String uuidVeicolo;

    private Long dispositivoId;

    private String dispositivoSeriale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getData() {
        return data;
    }

    public void setData(Instant data) {
        this.data = data;
    }

    public String getUuidVeicolo() {
        return uuidVeicolo;
    }

    public void setUuidVeicolo(String uuidVeicolo) {
        this.uuidVeicolo = uuidVeicolo;
    }

    public Long getDispositivoId() {
        return dispositivoId;
    }

    public void setDispositivoId(Long dispositivoId) {
        this.dispositivoId = dispositivoId;
    }

    public String getDispositivoSeriale() {
        return dispositivoSeriale;
    }

    public void setDispositivoSeriale(String dispositivoSeriale) {
        this.dispositivoSeriale = dispositivoSeriale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AssociazioneDVDTO associazioneDVDTO = (AssociazioneDVDTO) o;
        if(associazioneDVDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), associazioneDVDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AssociazioneDVDTO{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", uuidVeicolo='" + getUuidVeicolo() + "'" +
            "}";
    }
}
