package it.fai.ms.efservice.rules.engine.repository;

import static java.util.stream.Collectors.toSet;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;

@Service
public class RuleContextServiceTypeRepositoryImpl implements RuleContextServiceTypeRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final String CACHE_KEY = "TIPO_SERVIZI";

  private HashMap<String, Set<TipoServizio>> cache;

  private TipoServizioRepository serviceTypeRepository;

  public RuleContextServiceTypeRepositoryImpl(final TipoServizioRepository _serviceTypeRepository) {
    serviceTypeRepository = _serviceTypeRepository;
    cache = new HashMap<>();
  }

  @Override
  @Transactional(readOnly = true)
  public Set<TipoServizio> findAll() {
    if (!cache.containsKey(CACHE_KEY)) {
      _log.info("Search....");
      cache.put(CACHE_KEY, serviceTypeRepository.findAll()
                                                .stream()
                                                .collect(toSet()));
    }

    Set<TipoServizio> serviceTypes = cache.get(CACHE_KEY);
    _log.info("Found {}", (serviceTypes != null ? serviceTypes.size() : 0));
    return serviceTypes;
  }

  @Override
  @Transactional(readOnly = true)
  public RuleEngineServiceType newStarContextServiceType() {
    return new RuleEngineServiceType(RuleEngineServiceTypeId.ofWildcard());
  }

}
