package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.repository.dispositivo.TipoDispositivoAssegnabile;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.ProduttoreServiceExt;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.mapper.ContrattoMapper;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.HeaderCustom;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.web.rest.vm.ContrattoRequestExt;

/**
 * REST controller for managing Contratto.
 */
@RestController
@RequestMapping(ContrattoResourceExt.BASE_PATH)
public class ContrattoResourceExt {

  private final Logger               log              = LoggerFactory.getLogger(getClass());
  private final List<StatoContratto> statusNotRevoked = Arrays.asList(StatoContratto.ATTIVO, StatoContratto.SOSPESO);

  public static final String BASE_PATH = "/api/public";

  private static final String ENTITY_NAME = "contratto";

  private static final String CHECK_CONTRATTO = "checkcontratto";

  private static final String CONTRATTO_PRIMARIO     = "/contrattoprimario";
  public static final String  API_CONTRATTO_PRIMARIO = BASE_PATH + CONTRATTO_PRIMARIO;

  public static final String UPDATE_CONTRACT_CODE     = "/contratto/codice";
  public static final String API_UPDATE_CONTRACT_CODE = BASE_PATH + UPDATE_CONTRACT_CODE;

  public static final String CREATE_CONTRACT     = "/contrattos";
  public static final String API_CREATE_CONTRACT = BASE_PATH + CREATE_CONTRACT;

  public static final String CONTRACTS_NOT_FOUND     = "/contratti/not-found";
  public static final String API_CONTRACTS_NOT_FOUND = BASE_PATH + CONTRACTS_NOT_FOUND;

  private final ContrattoServiceExt  contrattoService;
  private final ClienteFaiServiceExt aziendaService;
  private final ProduttoreServiceExt produttoreService;
  private final ContrattoMapper      contrattoMapper;

  public ContrattoResourceExt(ContrattoServiceExt contrattoService, ClienteFaiServiceExt aziendaService,
                              ProduttoreServiceExt produttoreService, ContrattoMapper contrattoMapper) {
    super();
    this.contrattoService = contrattoService;
    this.aziendaService = aziendaService;
    this.produttoreService = produttoreService;
    this.contrattoMapper = contrattoMapper;
  }

  /**
   * POST /contrattos : Create a new contratto.
   *
   * @param request
   *          the contrattoDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new contrattoDTO, or with status 400 (Bad
   *         Request) if the contratto has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping(CREATE_CONTRACT)
  @Timed
  @ApiOperation(value = "Crea un nuovo contratto", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = ""),
                          @ApiResponse(code = 400, message = "Found contract with producer and customer code"),
                          @ApiResponse(code = 400, message = "Found contract with producer and contract code"),
                          @ApiResponse(code = 400, message = "Supplied customer code not found") })
  public ResponseEntity<Contratto> createContrattoApi(@ApiParam(
                                                                value = "request",
                                                                required = true) @RequestBody ContrattoRequestExt request) throws Exception {
    log.debug("REST request to save Contratto : {}", request);

    String codiceCliente = request.getCodiceCliente();
    ClienteFai clienteFai = aziendaService.findByCodiceCliente(codiceCliente);
    if (clienteFai == null) {
      log.error("CodiceCliente '{}' non trovato", codiceCliente);
      // return ResponseEntity.notFound().build();
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.INVALID_CUSTOMER);
    }

    Produttore produttore = produttoreService.findById(request.getIdProduttore());
    log.debug("Produttore found: {}", (produttore != null ? produttore.getNome() : null));

    String nomeProduttore = produttore.getNome();
    String codiceContratto = request.getCodiceContratto();
    Optional<Contratto> optContract = contrattoService.findByCodiceContrattoAndNomeProduttore(codiceContratto, nomeProduttore);

    if (optContract.isPresent()) {
      log.error("Exists another contract with codice contratto {} and produttore {}", codiceContratto, nomeProduttore);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.CONTRACT_FOUND_PRODUCER_CODE);
    }

    List<Contratto> contractsNotRevoked = contrattoService.findAllByProduttoreAndCodiceClienteAndStatusIn(nomeProduttore, codiceCliente,
                                                                                                          statusNotRevoked);
    if (contractsNotRevoked != null && !contractsNotRevoked.isEmpty()) {
      log.error("Contracts found with producer {} and customer code {} and status ACTIVED or SUSPENDED", nomeProduttore, codiceCliente);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.CONTRACT_FOUND_PRODUCER_CUSTOMER_WITH_STATUS_NOT_REVOKED);
    }

    Contratto result = null;
    // Search with PRIMARIO TRUE
    List<Contratto> contracts = contrattoService.findAllByProduttoreAndCodiceAzienda(produttore, codiceCliente);
    try {
      if (contracts != null && !contracts.isEmpty()) {
        contrattoService.setPrimarioFalse(contracts);
      } else {
        log.debug("Not found contract with produttore [{}] and codice cliente {}", produttore, codiceCliente);
      }

      /*
       * Contratto result = new Contratto(); result.setCodContrattoCliente(request.getCodiceContratto());
       * result.setStato(request.getStato()); result.setDataModificaStato(request.getDataModificaStato());
       * result.setClienteFai(clienteFai); result.setProduttore(produttore);
       * result.setPaeseRiferimentoIva(request.getNazioneFatturazione()); // Calculate Operazioni Possibili to contract
       * will be created; result = contrattoService.setOperazioniPossibiliOnContratto(result); result =
       * contrattoService.save(result);
       */
      result = createContratto(clienteFai, produttore, request.getCodiceContratto(), request.getStato(), request.getDataModificaStato(),
                               request.getNazioneFatturazione());
      log.debug("Contratto created: {}", result);
    } catch (Exception e) {
      log.error("Exception on create contracts: ", e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                           .build();
    }

    return ResponseEntity.created(new URI(API_CREATE_CONTRACT + "/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                          .toString()))
                         .body(result);
  }

  public Contratto createContratto(ClienteFai clienteFai, Produttore produttore, String codiceContratto, StatoContratto statoContratto,
                                   Instant dataModificStato, String nazioneFatturazione) {

    Contratto result = new Contratto();
    result.setCodContrattoCliente(codiceContratto);
    result.setStato(statoContratto);
    result.setDataModificaStato(dataModificStato);
    result.setClienteFai(clienteFai);
    result.setProduttore(produttore);
    result.setPaeseRiferimentoIva(nazioneFatturazione);
    // Calculate Operazioni Possibili to contract will be created;
    result = contrattoService.setOperazioniPossibiliOnContratto(result);

    result = contrattoService.save(result);
    return result;
  }

  @GetMapping("/contrattos/{codiceAzienda}/{nomeServizio}")
  public ResponseEntity<String> getContrattoByCodiceAziendaAndServizio(@PathVariable("codiceAzienda") String codiceAzienda,
                                                                       @PathVariable("nomeServizio") String nomeServizio) {
    Contratto contratto = contrattoService.findContrattoByServiceNameAndCodiceAzienda(nomeServizio, codiceAzienda);
    if (contratto == null) {
      return ResponseEntity.ok("-");
    }

    return ResponseEntity.ok(contratto.getCodContrattoCliente());
  }

  @GetMapping("/contrattos")
  public ResponseEntity<List<String>> getContrattoByCodiceAzienda(@RequestParam("codiceAzienda") String codiceAzienda,
                                                                  @RequestParam("tipoDispositivo") String tipoDispositivostr) {

    TipoDispositivoEnum tipoDispositivo = (tipoDispositivostr.equals(TipoDispositivoAssegnabile.TELEPASS_ARIANNA_1.name())) ? TipoDispositivoEnum.TELEPASS_EUROPEO
                                                                                                                            : TipoDispositivoEnum.valueOf(tipoDispositivostr);

    List<Contratto> contratto = contrattoService.findContrattoByCodiceAziendaAndTipoDispositivo(codiceAzienda, tipoDispositivo);
    List<String> codiciContratto = contratto.stream()
                                            .map(Contratto::getCodContrattoCliente)
                                            .collect(toList());

    return ResponseEntity.ok(codiciContratto);
  }

  @GetMapping("/find/codicecontratto/{codiceContratto}")
  @Timed
  public ResponseEntity<Set<ContrattoDTO>> findByCodice(@PathVariable String codiceContratto) throws Exception {
    log.debug("REST request to get Contratto by codice : {}", codiceContratto);

    List<Contratto> contratti = contrattoService.findByCodiceContratto(codiceContratto);
    Set<ContrattoDTO> contrattiDTO = (contratti != null && !contratti.isEmpty()) ? contratti.parallelStream()
                                                                                            .map(c -> contrattoMapper.toDto(c))
                                                                                            .collect(toSet())
                                                                                 : null;

    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("codice", codiceContratto)
                                              .headers())
                         .body(contrattiDTO);
  }

  @GetMapping("/find/codicecontratto/{codiceContratto}/{produttore}")
  @Timed
  public ResponseEntity<ContrattoDTO> findByCodiceAndProduttore(@PathVariable String codiceContratto,
                                                                @PathVariable String produttore) throws Exception {
    log.debug("REST request to get Contratto by codice : {} and Produttore: {}", codiceContratto, produttore);

    Optional<Contratto> contratto = contrattoService.findByCodiceContrattoAndNomeProduttore(codiceContratto, produttore);
    log.info("Contract found: {}", contratto);
    ContrattoDTO contrattoDTO = (contratto.isPresent()) ? contrattoMapper.toDto(contratto.get()) : null;

    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("codice", codiceContratto)
                                              .headers())
                         .body(contrattoDTO);
  }

  @GetMapping("/find/contratto/{uuidVeicolo}")
  @Timed
  public ResponseEntity<List<ContrattoDTO>> findAllByVehicleUuid(@PathVariable String uuidVeicolo) throws Exception {
    log.debug("REST Request all contract by vehicle uuid : {}", uuidVeicolo);

    List<ContrattoDTO> body = contrattoService.findAllByVehicleUuid(uuidVeicolo);

    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("x-size", body != null ? body.size() : -1)
                                              .add("x-uuid", uuidVeicolo)
                                              .headers())
                         .body(body);
  }

  @GetMapping("/" + CHECK_CONTRATTO + "/{codiceAzienda}/{tipoDispositivo}")
  @Timed
  public ResponseEntity<Void> checkContratto(@PathVariable String codiceAzienda,
                                             @PathVariable TipoDispositivoEnum tipoDispositivo) throws Exception {
    log.debug("REST Request check exists contract by codiceCliente: {} and TipoDispositivo: {}", codiceAzienda, tipoDispositivo);

    if (StringUtils.isBlank(codiceAzienda) || tipoDispositivo == null) {
      throw CustomException.builder(HttpStatus.BAD_GATEWAY)
                           .add(Errno.VALIDATION_FAILED);
    }

    boolean isPresent = contrattoService.checkContrattoByCodiceClienteAndTipoDispositivo(codiceAzienda, tipoDispositivo);
    if (!isPresent) {
      log.info("Contratto to codice cliente: {} and tipo dispositivo: {} does not exist...", codiceAzienda, tipoDispositivo);
      return ResponseEntity.notFound()
                           .build();
    }

    return ResponseEntity.ok()
                         .build();
  }

  @PutMapping(UPDATE_CONTRACT_CODE + "/{identifier}")
  @Timed
  @ApiOperation(value = "Aggiornamento codice contratto", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "contract code exists"),
                          @ApiResponse(code = 404, message = "id is unknown"),
                          @ApiResponse(code = 412, message = "path variable is required, codice contratto is required") })
  public ResponseEntity<ContrattoDTO> updateCodiceContratto(@ApiParam(
                                                                      value = "identifier",
                                                                      required = true) @PathVariable(
                                                                                                     name = "identifier",
                                                                                                     required = true) String identifier,
                                                            @ApiParam(
                                                                      value = "dto",
                                                                      required = true) @RequestBody ContrattoDTO dto) throws Exception {
    log.debug("REST Request to update contract : {} with the new contract code : {}", identifier, dto.getCodContrattoCliente());

    if (identifier == null || StringUtils.isBlank(dto.getCodContrattoCliente())) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.PARAMETERS_REQUIRED);
    }
    ContrattoDTO contractDTO = null;
    Optional<Contratto> contrattoOpt = contrattoService.findByIdentificativo(identifier);
    if (contrattoOpt.isPresent()) {
      Contratto contract = contrattoOpt.get();
      log.debug("Loaded contract: {}", contract);
      contract = contrattoService.save(contract.dataModificaStato(dto.getDataModificaStato())
                                               .primario(dto.isPrimario()));
      if (contract != null) {
        contractDTO = contrattoMapper.toDto(contract);
      }
    }
    return ResponseEntity.status(contractDTO == null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                         .body(contractDTO);
  }

  @PostMapping(CONTRACTS_NOT_FOUND)
  @Timed
  public ResponseEntity<List<ContrattoDTO>> contractsNotFound(@ApiParam(
                                                                        value = "contracts",
                                                                        required = true) @RequestBody List<String> contracts) throws Exception {
    log.debug("REST request contractsNotFound : {}", contracts);

    List<Contratto> contratti = contrattoService.contractsNotFound(contracts);

    List<ContrattoDTO> body = (contratti != null && !contratti.isEmpty()) ? contratti.parallelStream()
                                                                                     .map(c -> contrattoMapper.toDto(c))
                                                                                     .collect(toList())
                                                                          : null;

    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("x-size", body != null ? body.size() : -1)
                                              .add("x-uri", API_CONTRACTS_NOT_FOUND)
                                              .headers())
                         .body(body);
  }

  @PostMapping(CONTRATTO_PRIMARIO)
  @Timed
  public ResponseEntity<Contratto> setContrattoToPrimario(@ApiParam(
                                                                    value = "contracts",
                                                                    required = true) @RequestParam String identifier) throws Exception {
    log.debug("REST request set flag primario to true on contracts identifier : {}", identifier);

    Optional<Contratto> optContract = contrattoService.findByIdentificativo(identifier);
    if (!optContract.isPresent()) {
      throw CustomException.builder(HttpStatus.BAD_GATEWAY)
                           .add(Errno.NOT_FOUND_CONTRATTO);
    }

    Contratto contratto = optContract.get();
    log.debug("Contracts found: {}", contratto);

    Produttore produttore = contratto.getProduttore();
    ClienteFai clienteFai = contratto.getClienteFai();
    List<Contratto> contracts = contrattoService.findAllByProduttoreAndCodiceAzienda(produttore, clienteFai.getCodiceCliente());
    if (contracts != null && !contracts.isEmpty()) {
      log.debug("Set primario false on contracts founded by produttore [{}] and cliente [{}]", produttore, clienteFai);
      contracts.forEach(c -> {
        if (identifier.compareTo(c.getIdentificativo()) != 0) {
          contrattoService.save(c.primario(false));
        } else {
          log.debug("Skip update, because contract with identifier {} should be updated", identifier);
        }
      });
    }
    log.debug("Contracts setted flag primario to false");

    Contratto contrattoSaved = contrattoService.save(contratto.primario(true));

    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("x-uri", API_CONTRATTO_PRIMARIO)
                                              .headers())
                         .body(contrattoSaved);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }
  

}
