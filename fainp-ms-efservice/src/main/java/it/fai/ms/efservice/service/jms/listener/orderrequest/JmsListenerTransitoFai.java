package it.fai.ms.efservice.service.jms.listener.orderrequest;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.orderrequest.OrderNotificaTransitoMessage;
import it.fai.ms.efservice.service.jms.consumer.OrdineClienteTransitoFaiConsumer;

@Service
@Transactional
public class JmsListenerTransitoFai implements MessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final OrdineClienteTransitoFaiConsumer consumer;

  @Autowired
  public JmsListenerTransitoFai(OrdineClienteTransitoFaiConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  public void onMessage(final Message _message) {
    OrderNotificaTransitoMessage msg = null;
    try {
      try {
        msg = (OrderNotificaTransitoMessage) ((ObjectMessage) _message).getObject();
        _log.info("Received jms message {}", msg);
        consumer.consume(msg);
      } catch (final ClassCastException _e) {
        _log.error("JMS ObjectMessage isn't {}", OrderNotificaTransitoMessage.class.getSimpleName());
      }
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

}