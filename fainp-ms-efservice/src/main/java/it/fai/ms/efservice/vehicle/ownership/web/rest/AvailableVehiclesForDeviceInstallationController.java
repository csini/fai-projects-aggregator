package it.fai.ms.efservice.vehicle.ownership.web.rest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.service.AvailableVehicleService;
import it.fai.ms.efservice.vehicle.ownership.web.rest.vm.AvailableVehicle;

@RestController
@RequestMapping("/api/vehicle/ownership/")
public class AvailableVehiclesForDeviceInstallationController {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final AvailableVehicleService availableVehicleService;
  
  @Autowired
  public AvailableVehiclesForDeviceInstallationController(final AvailableVehicleService _availableVehicleService) {
    availableVehicleService = _availableVehicleService;
  }

  @GetMapping("{companyCode}/{vehicleUuid}")
  public ResponseEntity<List<AvailableVehicle>> getAllVehiclesAvailableForDeviceTypeId(final @PathVariable(
                                                                                                          name = "companyCode") @NotNull String _companyCode,
                                                                                      final @PathVariable(
                                                                                                          name = "vehicleUuid") @NotNull String _vehicleUuid) {
    _log.info("Creating available vehicles list for companyCode {} and vehicleUuid {}", _companyCode, _vehicleUuid);

    Set<AvailableVehicle> availableVehicles = availableVehicleService.getAvailableVehicleFiltered(_companyCode, _vehicleUuid);

    _log.debug("Available vehicles list created size: {}", ((availableVehicles != null && isNotEmpty(availableVehicles)) ? availableVehicles.size() : null));
    
    List<AvailableVehicle> rs =  (availableVehicles!=null) ? availableVehicles.stream().sorted(Comparator.comparing(AvailableVehicle::getLicensePlate)).collect(Collectors.toList()) : new ArrayList<AvailableVehicle>();
    
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(rs);
  }

  @GetMapping("{companyCode}/{deviceType}/{vehicleUuid}")
  public ResponseEntity<List<AvailableVehicle>> getAllVehiclesAvailableForDeviceTypeAndVehicle(final @PathVariable(
                                                                                                                  name = "companyCode") @NotNull String _companyCode,
                                                                                              final @PathVariable(
                                                                                                                  name = "deviceType") @NotNull String _deviceType,
                                                                                              final @PathVariable(
                                                                                                                  name = "vehicleUuid") @NotNull String _vehicleUuid) {

    _log.info("Get available vehicles list filtered by CompanyCode {}, DeviceType {} and vehicleUuid {}", _companyCode, _deviceType,
              _vehicleUuid);

    Set<AvailableVehicle> availableVehicles = availableVehicleService.getAvailableVehicleFiltered(_companyCode, _deviceType, _vehicleUuid);
    availableVehicles = availableVehicleService.filterVehicleByDeviceTypeIsNot(availableVehicles, _deviceType);
    List<AvailableVehicle> rs =  (availableVehicles!=null) ? availableVehicles.stream().sorted(Comparator.comparing(AvailableVehicle::getLicensePlate)).collect(Collectors.toList()) : new ArrayList<AvailableVehicle>();
    
    _log.debug("Available vehicles list size: {}",
              (availableVehicles != null && isNotEmpty(availableVehicles)) ? availableVehicles.size() : 0);

    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(rs);
  }
  
  private boolean isNotEmpty(Set<AvailableVehicle> availableVehicles) {
    return (availableVehicles.isEmpty() ? false : true);
  }

}
