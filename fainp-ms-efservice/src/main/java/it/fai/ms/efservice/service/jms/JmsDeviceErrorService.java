package it.fai.ms.efservice.service.jms;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.ErrorDetailsDTO;
import it.fai.ms.common.jms.efservice.ErrorDetails;
import it.fai.ms.common.jms.efservice.message.device.AbstractDeviceErrorMessage;

@Service
public class JmsDeviceErrorService {

  private static final String SEPARATOR = " - ";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public String buildErrorMessage(AbstractDeviceErrorMessage message) {
    StringBuilder sb = new StringBuilder();
    ErrorDetails errorDetails = message.getErrorDetails();
    if (errorDetails != null) {
      String errorCode = errorDetails.getErrorCode();
      String errorMessage = errorDetails.getErrorMessage();
      sb.append(errorCode);
      if (StringUtils.isNotBlank(sb.toString())) {
        sb.append(SEPARATOR);
      }
      sb.append(errorMessage);
    }

    String errMsg = sb.toString();
    log.info("Error message retrieve by {} is : {}", message, errMsg);
    return StringUtils.isNotBlank(errMsg) ? errMsg : null;
  }

  public String buildErrorMessage(DeviceEventMessage eventMessage) {
    StringBuilder sb = new StringBuilder();
    ErrorDetailsDTO errorDetails = eventMessage.getErrorDetailsDTO();
    if (errorDetails != null) {
      String errorCode = errorDetails.getErrorCode();
      String errorMessage = errorDetails.getErrorMessage();
      sb.append(errorCode);
      if (StringUtils.isNotBlank(sb.toString())) {
        sb.append(SEPARATOR);
      }
      sb.append(errorMessage);
    }

    String errMsg = sb.toString();
    log.info("Error message retrieve by {} is : {}", eventMessage, errMsg);
    return StringUtils.isNotBlank(errMsg) ? errMsg : null;
  }
}
