package it.fai.ms.efservice.service.dto;

public class CaricamentoOrdineFornitoreByRangeDTO extends CaricamentoOrdineFornitoreDTO {

  private static final long serialVersionUID = -7819866193625523812L;

  private String rangeStart;

  private String rangeStop;

  private String pan;

  public String getRangeStart() {
    return rangeStart;
  }

  public void setRangeStart(String rangeStart) {
    this.rangeStart = rangeStart;
  }

  public String getRangeStop() {
    return rangeStop;
  }

  public void setRangeStop(String rangeStop) {
    this.rangeStop = rangeStop;
  }

  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("CaricamentoOrdineFornitoreByRangeDTO [");
    sb.append("OrdineFornitoreId=");
    sb.append(getId());
    sb.append(", pan=");
    sb.append(getPan());
    sb.append(", RangeStart=");
    sb.append(rangeStart);
    sb.append(", RangeStop=");
    sb.append(rangeStop);
    sb.append(", SkipValidation=");
    sb.append(isSkipValidation());
    sb.append(", Note=");
    sb.append(getNote());
    sb.append("]");
    return sb.toString();
  }

}
