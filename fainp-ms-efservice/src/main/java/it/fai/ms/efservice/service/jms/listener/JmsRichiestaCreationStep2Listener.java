package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.efservice.service.jms.consumer.NewRequestConsumer;

@Service
@Transactional
public class JmsRichiestaCreationStep2Listener extends JmsObjectMessageListenerTemplate<String> implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());
  
  private NewRequestConsumer consumer;

  public JmsRichiestaCreationStep2Listener(NewRequestConsumer consumer) {
    this.consumer = consumer;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.RICHIESTA_CREATION_STEP2;
  }

  @Override
  protected void consumeMessage(String identificativoRichiesta) {
    log.info("Consume message: {}", identificativoRichiesta);
    try {
      consumer.consume(identificativoRichiesta);
    } catch (Exception e) {
      log.error("Exception on consumer CreationRequest: ", e);
      throw new RuntimeException(e);
    }
  }

}
