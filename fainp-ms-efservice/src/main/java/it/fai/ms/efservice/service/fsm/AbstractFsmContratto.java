package it.fai.ms.efservice.service.fsm;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

public abstract class AbstractFsmContratto {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String USER_MANUAL_PREFIX = "MU_";

  private final String MANUAL_SYSTEM_PREFIX = "MS_";

  protected abstract StateMachine<StatoContratto, ContrattoEvent> getStateMachine();

  public Contratto executeCommandToChangeState(ContrattoEvent command, Contratto contratto) throws Exception {
    return executeCommandToChangeState(command, contratto, null);
  }

  public abstract Contratto executeCommandToChangeState(ContrattoEvent command, Contratto contratto, String nota) throws Exception;

  public boolean sendEventToFsm(StateMachine<StatoContratto, ContrattoEvent> sm, ContrattoEvent command, Contratto contratto, String nota) {
    final Message<ContrattoEvent> message = MessageBuilder.withPayload(command)
                                                          .setHeader("object", contratto)
                                                          .setHeader("nota", nota)
                                                          .build();
    boolean sended = sm.sendEvent(message);
    log.debug("Sended event to change status: {}", sended);
    if(!sended) {
      throw new RuntimeException("Not sended event...so not change status by command: " + command);
    }
    return sended;
  }

  public Collection<ContrattoEvent> getAllAvailableCommand(Contratto contratto) {
    Enum<StatoContratto> fsmStatus = contratto.getStato();
    Collection<ContrattoEvent> events = new ArrayList<>();
    Collection<Transition<StatoContratto, ContrattoEvent>> transitions = getStateMachine().getTransitions();

    Set<ContrattoEvent> eventsSet = transitions.parallelStream()
                                               .map(tx -> tx)
                                               .filter(tx -> (tx != null && tx.getSource() != null && tx.getSource()
                                                                                                        .getId()
                                                                                                        .name()
                                                                                                        .equals(fsmStatus.name())))
                                               .map(tx -> tx)
                                               .filter(tx -> tx.getTrigger() != null)
                                               .map(tx -> tx.getTrigger()
                                                            .getEvent())
                                               .collect(toSet());

    events.addAll(eventsSet);
    return events;
  }

  public List<String> getUserAvailableCommand(Contratto obj) {
    Collection<ContrattoEvent> allAvailableCommand = getAllAvailableCommand(obj);
    List<String> userAvailableCommand = new ArrayList<>();

    for (ContrattoEvent event : allAvailableCommand) {
      String eventName = event.name();
      if (eventName.startsWith(USER_MANUAL_PREFIX)) {
        userAvailableCommand.add(eventName);
      }
    }

    return userAvailableCommand;
  }

  public boolean isAvailableCommand(Enum<?> command, Contratto obj) {
    Collection<ContrattoEvent> allAvailableCommand = getAllAvailableCommand(obj);
    for (ContrattoEvent event : allAvailableCommand) {
      Enum<?> enumCmd = null;
      if (event instanceof Enum<?>) {
        enumCmd = (Enum<?>) event;

      }
      if (enumCmd.name()
                 .equalsIgnoreCase(command.name())) {
        return true;
      }
    }
    return false;
  }

  public Set<Transition<StatoContratto, ContrattoEvent>> getTransitions() {

    Collection<Transition<StatoContratto, ContrattoEvent>> transitions = getStateMachine().getTransitions();
    Set<Transition<StatoContratto, ContrattoEvent>> allTransitions = new HashSet<>();
    allTransitions.addAll(transitions);

    return allTransitions;
  }

  public boolean isAutoTransition(Enum<?> command) {
    boolean foundCommand = true;

    Collection<Transition<StatoContratto, ContrattoEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoContratto, ContrattoEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {

      Transition<StatoContratto, ContrattoEvent> tx = iterator.next();
      Trigger<StatoContratto, ContrattoEvent> trigger = tx.getTrigger();
      if (trigger != null) {
        ContrattoEvent event = trigger.getEvent();
        if (event.name()
                 .equalsIgnoreCase(command.name())) {
          foundCommand = false;
          break;
        }
      }
    }

    return foundCommand;
  }

  public boolean isStatoPossibile(StatoContratto statoPrecedente, StatoRichiesta statoAttuale) {
    Collection<Transition<StatoContratto, ContrattoEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoContratto, ContrattoEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StatoContratto, ContrattoEvent> tx = iterator.next();
      State<StatoContratto, ContrattoEvent> source = tx.getSource();
      if (source != null) {
        StatoContratto statoPartenza = source.getId();
        if (statoPartenza.name()
                         .equals(statoPrecedente.name())) {

          State<StatoContratto, ContrattoEvent> target = tx.getTarget();
          if (target != null) {
            StatoContratto destinazione = target.getId();
            if (destinazione.name()
                            .equals(statoAttuale.name())) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  public boolean isStatoManualeSistema(StatoContratto statoPrecedente, StatoContratto statoAttuale) {
    ContrattoEvent richiestaEvent = getContrattoEvent(statoPrecedente, statoAttuale);
    if (richiestaEvent == null) {
      return false;
    }

    if (richiestaEvent.name()
                      .startsWith(MANUAL_SYSTEM_PREFIX)) {
      return true;
    }
    return false;
  }

  public ContrattoEvent getContrattoEvent(StatoContratto statoPrecedente, StatoContratto statoAttuale) {
    Collection<Transition<StatoContratto, ContrattoEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoContratto, ContrattoEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StatoContratto, ContrattoEvent> tx = iterator.next();
      State<StatoContratto, ContrattoEvent> source = tx.getSource();
      if (source != null) {
        StatoContratto statoPartenza = source.getId();
        if (statoPartenza.name()
                         .equals(statoPrecedente.name())) {

          State<StatoContratto, ContrattoEvent> target = tx.getTarget();
          if (target != null) {
            StatoContratto destinazione = target.getId();
            if (destinazione.name()
                            .equals(statoAttuale.name())) {
              Trigger<StatoContratto, ContrattoEvent> trigger = tx.getTrigger();
              if (trigger != null) {
                ContrattoEvent event = trigger.getEvent();
                return event;
              }
            }
          }
        }
      }
    }

    return null;
  }

  public Contratto getUserAvailableCommandAndSetOnContratto(Contratto contratto) {
    List<String> userAvailableCommand = getUserAvailableCommand(contratto);
    return setOperazioniPossibiliContratto(userAvailableCommand, contratto);
  }

  public Contratto setOperazioniPossibiliContratto(List<String> userAvailableCommand, Contratto contratto) {
    String operations = getOperazioniPossibiliByUserAvailableCommand(userAvailableCommand, contratto);
    return contratto.operazioniPossibili(operations);
  }

  public String getOperazioniPossibili(Contratto contratto) {
    List<String> userAvailableCommand = getUserAvailableCommand(contratto);

    StringBuilder sb = new StringBuilder();
    if (userAvailableCommand != null) {
      for (String command : userAvailableCommand) {
        sb.append(command);
        sb.append(',');
      }
    }

    String operations = sb.toString();
    if (StringUtils.isNotBlank(operations)) {
      operations = operations.substring(0, (operations.length() - 1));
    }

    log.debug("Operazioni possibili for [{} Codice {}]: {}", contratto.getClass(), contratto.getCodContrattoCliente(), operations);
    return operations;
  }

  public void setOperazioniPossibili(Contratto contract) {
    String operazioniPossibili = getOperazioniPossibili(contract);
    log.debug("Set Operazioni possibili on Contratto {}: {}", contract, operazioniPossibili);
    contract.setOperazioniPossibili(operazioniPossibili);
  }

  private List<String> filterAvailableCommand(Contratto contratto, List<String> userAvailableCommand) {
    // TODO implements if necessary....
    return userAvailableCommand;
  }

  public String getOperazioniPossibiliByUserAvailableCommand(List<String> userAvailableCommand, Contratto contratto) {
    StringBuilder sb = new StringBuilder();
    for (String command : userAvailableCommand) {
      sb.append(command);
      sb.append(',');
    }

    String operations = sb.toString();
    if (StringUtils.isNotBlank(operations)) {
      operations = operations.substring(0, (operations.length() - 1));
    }

    log.debug("Operazioni possibili for [{} Codice {}]: {}", contratto.getClass(), contratto.getCodContrattoCliente(), operations);
    return operations;
  }

}
