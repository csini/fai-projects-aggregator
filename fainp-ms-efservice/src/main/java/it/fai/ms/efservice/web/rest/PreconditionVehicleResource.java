package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.rules.engine.repository.PreconditionValidationRepository;
import it.fai.ms.efservice.web.rest.errors.CustomException;

@RestController
@RequestMapping(PreconditionVehicleResource.BASE_PATH)
public class PreconditionVehicleResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  private static final String PRECONDITION = "/precondizioni/polling";

  private final PreconditionValidationRepository preconditionValidationRepo;

  public PreconditionVehicleResource(final PreconditionValidationRepository _preconditionValidationRepo) {
    preconditionValidationRepo = _preconditionValidationRepo;
  }

  @GetMapping(PRECONDITION + "/{identificativo}")
  @Timed
  public ResponseEntity<AsyncJobResponse> pollingPreconditionCompleted(@PathVariable("identificativo") String deviceUuid,
                                                                       @RequestParam("timestamp") Instant timestamp) throws URISyntaxException,
                                                                                                                     CustomException {
    log.debug("REST request to get finish precondition calculation by: UUID Vehicle {} - Timestamp: {}", deviceUuid, timestamp);
    StateChangeStatusFsm state = StateChangeStatusFsm.SUCCESS;
    if (!preconditionValidationRepo.isPreconditionValid(deviceUuid, timestamp)) {
      state = StateChangeStatusFsm.PENDING;
    }
    AsyncJobResponse asyncJobResponse = new AsyncJobResponse();
    asyncJobResponse.setState(state);
    return ResponseEntity.ok(asyncJobResponse);
  }

}
