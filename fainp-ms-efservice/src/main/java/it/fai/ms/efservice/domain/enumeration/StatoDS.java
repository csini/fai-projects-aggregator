package it.fai.ms.efservice.domain.enumeration;

/**
 * The StatoDispositivoServizio enumeration.
 */
public enum StatoDS {
    NON_ATTIVO, ATTIVO, IN_ATTIVAZIONE, IN_DISATTIVAZIONE
}
