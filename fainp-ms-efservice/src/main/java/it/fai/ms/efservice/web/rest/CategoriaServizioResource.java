package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.CategoriaServizioService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CategoriaServizio.
 */
@RestController
@RequestMapping("/api")
public class CategoriaServizioResource {

    private final Logger log = LoggerFactory.getLogger(CategoriaServizioResource.class);

    private static final String ENTITY_NAME = "categoriaServizio";

    private final CategoriaServizioService categoriaServizioService;

    public CategoriaServizioResource(CategoriaServizioService categoriaServizioService) {
        this.categoriaServizioService = categoriaServizioService;
    }

    /**
     * POST  /categoria-servizios : Create a new categoriaServizio.
     *
     * @param categoriaServizioDTO the categoriaServizioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new categoriaServizioDTO, or with status 400 (Bad Request) if the categoriaServizio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/categoria-servizios")
    @Timed
    public ResponseEntity<CategoriaServizioDTO> createCategoriaServizio(@Valid @RequestBody CategoriaServizioDTO categoriaServizioDTO) throws URISyntaxException {
        log.debug("REST request to save CategoriaServizio : {}", categoriaServizioDTO);
        if (categoriaServizioDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new categoriaServizio cannot already have an ID")).body(null);
        }
        CategoriaServizioDTO result = categoriaServizioService.save(categoriaServizioDTO);
        return ResponseEntity.created(new URI("/api/categoria-servizios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /categoria-servizios : Updates an existing categoriaServizio.
     *
     * @param categoriaServizioDTO the categoriaServizioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated categoriaServizioDTO,
     * or with status 400 (Bad Request) if the categoriaServizioDTO is not valid,
     * or with status 500 (Internal Server Error) if the categoriaServizioDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/categoria-servizios")
    @Timed
    public ResponseEntity<CategoriaServizioDTO> updateCategoriaServizio(@Valid @RequestBody CategoriaServizioDTO categoriaServizioDTO) throws URISyntaxException {
        log.debug("REST request to update CategoriaServizio : {}", categoriaServizioDTO);
        if (categoriaServizioDTO.getId() == null) {
            return createCategoriaServizio(categoriaServizioDTO);
        }
        CategoriaServizioDTO result = categoriaServizioService.save(categoriaServizioDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, categoriaServizioDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /categoria-servizios : get all the categoriaServizios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of categoriaServizios in body
     */
    @GetMapping("/categoria-servizios")
    @Timed
    public List<CategoriaServizioDTO> getAllCategoriaServizios() {
        log.debug("REST request to get all CategoriaServizios");
        return categoriaServizioService.findAll();
        }

    /**
     * GET  /categoria-servizios/:id : get the "id" categoriaServizio.
     *
     * @param id the id of the categoriaServizioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the categoriaServizioDTO, or with status 404 (Not Found)
     */
    @GetMapping("/categoria-servizios/{id}")
    @Timed
    public ResponseEntity<CategoriaServizioDTO> getCategoriaServizio(@PathVariable Long id) {
        log.debug("REST request to get CategoriaServizio : {}", id);
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(categoriaServizioDTO));
    }

    /**
     * DELETE  /categoria-servizios/:id : delete the "id" categoriaServizio.
     *
     * @param id the id of the categoriaServizioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/categoria-servizios/{id}")
    @Timed
    public ResponseEntity<Void> deleteCategoriaServizio(@PathVariable Long id) {
        log.debug("REST request to delete CategoriaServizio : {}", id);
        categoriaServizioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
