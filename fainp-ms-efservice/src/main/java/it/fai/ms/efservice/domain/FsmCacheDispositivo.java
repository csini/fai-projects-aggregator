package it.fai.ms.efservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fsm_dispositivo_cache")
public class FsmCacheDispositivo implements Serializable {
  private static final long serialVersionUID = -3662399825118798142L;

  @Id
  @Column(name = "id")
  private Long id;
  
  @Column(name = "data")
  private String data;
  
  
  public FsmCacheDispositivo() {}
  
  public FsmCacheDispositivo(Long id, String data) {
    this.id = id;
    this.data = data;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("FsmCacheDispositivo [id=");
    builder.append(id);
    builder.append(", data=");
    builder.append(data);
    builder.append("]");
    return builder.toString();
  }
  
  
}
