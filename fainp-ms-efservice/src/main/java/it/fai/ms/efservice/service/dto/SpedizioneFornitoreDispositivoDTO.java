package it.fai.ms.efservice.service.dto;

import java.time.Instant;

public class SpedizioneFornitoreDispositivoDTO {
  private String  identificativo;
  private String  targa;
  private String  tipoDispositivo;
  private String  seriale;
  private Instant dataRichiestaDispositivo;
  private Instant dataAccettazioneRichiesta;

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public Instant getDataRichiestaDispositivo() {
    return dataRichiestaDispositivo;
  }

  public void setDataRichiestaDispositivo(Instant dataRichiestaDispositivo) {
    this.dataRichiestaDispositivo = dataRichiestaDispositivo;
  }

  public Instant getDataAccettazioneRichiesta() {
    return dataAccettazioneRichiesta;
  }

  public void setDataAccettazioneRichiesta(Instant dataAccettazioneRichiesta) {
    this.dataAccettazioneRichiesta = dataAccettazioneRichiesta;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  @Override
  public String toString() {
    return "SpedizioneFornitoreDispositivoDTO [identificativo=" + identificativo + ", targa=" + targa + ", tipoDispositivo="
           + tipoDispositivo + ", seriale=" + seriale + ", dataRichiestaDispositivo=" + dataRichiestaDispositivo
           + ", dataAccettazioneRichiesta=" + dataAccettazioneRichiesta + "]";
  }

}
