package it.fai.ms.efservice.service.dto;

public class VistaListaServiziUtentiAppoggioDTO {
	
	private Long count;
	private String nome;
	private String stato;
	
	public VistaListaServiziUtentiAppoggioDTO(Long count, String nome, String stato) {
		this.count = count;
		this.nome = nome;
		this.stato = stato;
	}
	
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	
}
