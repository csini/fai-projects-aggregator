package it.fai.ms.efservice.service.fsm.config;

import java.util.Set;

import org.slf4j.Logger;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;

public abstract class FsmRichiestaConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  protected abstract Logger getLogger();

  protected abstract String getMachineId();

  protected abstract StatoRichiesta getStatoRichiestaInitial();

  protected abstract Set<StatoRichiesta> getAvailableStates();

  protected abstract void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception;

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId(getMachineId());
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());

  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(getStatoRichiestaInitial(), ctx -> getLogger().trace("Target: " + ctx.getTarget()
                                                                                        .getIds()))
          .states(getAvailableStates());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    configTransitions(transitions);
  }

}
