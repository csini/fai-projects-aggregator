package it.fai.ms.efservice.wizard.repository;

import java.util.Optional;
import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

public interface WizardVehicleRepository {

  Integer count();

  Optional<WizardVehicle> find(String uuid);

  void save(WizardVehicle wizardVehicle);

  WizardVehicleEntity remove(String _uuid);

  Set<WizardVehicle> find(Set<String> uuidVehicles);

}
