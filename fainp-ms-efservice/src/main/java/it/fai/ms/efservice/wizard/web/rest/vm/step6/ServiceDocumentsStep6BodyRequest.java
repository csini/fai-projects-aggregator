package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;
import java.util.List;

public class ServiceDocumentsStep6BodyRequest implements Serializable {
  private static final long serialVersionUID = 6036609154785959431L;

  private List<String> servicetypes;

  public ServiceDocumentsStep6BodyRequest() {
  }

  public List<String> getServicetypes() {
    return servicetypes;
  }

  public void setServicetypes(List<String> servicetypes) {
    this.servicetypes = servicetypes;
  }

  @Override
  public String toString() {
    return "DocumentsStep6BodyRequest [servicetypes=" + servicetypes + "]";
  }
}
