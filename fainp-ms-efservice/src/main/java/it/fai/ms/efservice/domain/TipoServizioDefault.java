package it.fai.ms.efservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A TipoServizioDefault.
 */
@Entity
@Table(name = "tipo_servizio_default")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TipoServizioDefault implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TipoDispositivo tipoDispositivo;

    @OneToMany(mappedBy = "tipoServizioDefault")
    @JsonIgnore
    private Set<TipoServizio> tipoServizios = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoDispositivo getTipoDispositivo() {
        return tipoDispositivo;
    }

    public TipoServizioDefault tipoDispositivo(TipoDispositivo tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
        return this;
    }

    public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public Set<TipoServizio> getTipoServizios() {
        return tipoServizios;
    }

    public TipoServizioDefault tipoServizios(Set<TipoServizio> tipoServizios) {
        this.tipoServizios = tipoServizios;
        return this;
    }

    public TipoServizioDefault addTipoServizio(TipoServizio tipoServizio) {
        this.tipoServizios.add(tipoServizio);
        tipoServizio.setTipoServizioDefault(this);
        return this;
    }

    public TipoServizioDefault removeTipoServizio(TipoServizio tipoServizio) {
        this.tipoServizios.remove(tipoServizio);
        tipoServizio.setTipoServizioDefault(null);
        return this;
    }

    public void setTipoServizios(Set<TipoServizio> tipoServizios) {
        this.tipoServizios = tipoServizios;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoServizioDefault tipoServizioDefault = (TipoServizioDefault) o;
        if (tipoServizioDefault.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoServizioDefault.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoServizioDefault{" +
            "id=" + getId() +
            "}";
    }
}
