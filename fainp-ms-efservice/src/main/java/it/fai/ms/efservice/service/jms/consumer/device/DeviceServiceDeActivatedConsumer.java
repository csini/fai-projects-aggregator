package it.fai.ms.efservice.service.jms.consumer.device;

import static java.util.stream.Collectors.toSet;

import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ServiceUuid;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceDeactivationMessage;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceServiceDeActivatedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils fsmRichiestaUtils;

  private final DispositivoServiceExt dispositivoServiceExt;

  public DeviceServiceDeActivatedConsumer(final FsmRichiestaUtils _fsmRichiestaUtils, final DispositivoServiceExt _dispositivoServiceExt) {
    fsmRichiestaUtils = _fsmRichiestaUtils;
    dispositivoServiceExt = _dispositivoServiceExt;
  }

  public void consume(DeviceServiceDeactivationMessage message) throws FsmExecuteCommandException {
    log.debug("Manage message: {}", message);

    ObuNumber obuNumber = message.getObuNumber();
    String serialeDispositivo = obuNumber.getCode();
    String contractCode = message.getContractUuid()
                                 .getUuid();
    Optional<Dispositivo> dispositivoOpt = dispositivoServiceExt.findByCodiceContrattoAndSeriale(contractCode, serialeDispositivo);
    if (!dispositivoOpt.isPresent()) {
      throw new RuntimeException("Not found dispositivo with Contratto: " + contractCode + " and Seriale: " + serialeDispositivo);
    }

    Dispositivo dispositivo = dispositivoOpt.get();
    log.info("Found dispositivo {}", dispositivo);

    Set<StatoDispositivoServizio> statoDispositivoServizios = dispositivo.getStatoDispositivoServizios();
    Set<Richiesta> richiestas = dispositivo.getRichiestas();
    Set<DeviceService> services = message.getServices();
    for (DeviceService s : services) {
      log.info("Manage ServiceUUID: {}", s);
      for (Richiesta r : richiestas) {
        if (r.getTipo() == TipoRichiesta.DISATTIVAZIONE_SERVIZIO && StringUtils.isNotBlank(r.getNomeServizio())
            && ServiceUuid.fromServiceTypeName(r.getNomeServizio()) == s.getServiceUuid()) {
          log.info("Find Richiesta {} for ServiceUUID {}", r, s);
          String nomeServizio = r.getNomeServizio();
          for (StatoDispositivoServizio sds : statoDispositivoServizios) {
            if (sds.getTipoServizio()
                   .getNome()
                   .equalsIgnoreCase(nomeServizio)) {
              log.debug("StatoDispositivoServizio before: {}", sds);
              sds = dispositivoServiceExt.updateStatoDeviceService(sds, StatoDS.NON_ATTIVO);
              log.info("StatoDispositivoServizio after: {}", sds);
            }
          }
          
          Richiesta richiesta = fsmRichiestaUtils.changeStatusToRichiesta(r, RichiestaEvent.RESPONSE_OK.name(), null, null);
          log.info("Change status Richiesta DISATTIVAZIONE SERVIZI: {}", richiesta);
        }
      }
    }

  }

}
