package it.fai.ms.efservice.service.jms.producer.device;

import static java.util.stream.Collectors.toList;

import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.ServiceUuid;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoDS;

public class OrderRequestDecorator {

  private final static String SEPARATOR_TARGA_NAZIONE = "§";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private Richiesta orderRequestEntity;

  public OrderRequestDecorator(final Richiesta _orderRequestEntity) {
    orderRequestEntity = Optional.ofNullable(_orderRequestEntity)
      .orElseThrow(() -> new IllegalArgumentException("OrderRequestDecorator entity parameter is mandatory"));
  }

  public String getVehicleUuid() {
    return getDispositivo().get().getAssociazioneDispositivoVeicolos().stream().findFirst().get().getUuidVeicolo();
  }

  public String getHardwareDeviceType() {
    String                tipoHardwareFound = "";
    Optional<Dispositivo> deviceOpt         = getDispositivo();
    if (deviceOpt.isPresent()) {
      String tipoHardware = deviceOpt.get().getTipoHardware();
      if (StringUtils.isNotBlank(tipoHardware)) {
        tipoHardwareFound = tipoHardware;
      }
    }

    _log.info("Tipo Hardware: {}", tipoHardwareFound);
    return tipoHardwareFound;
  }

  public List<String> findActiveServicesNames() {
    final List<String> activeServicesNames = getDispositivo().get()
      .getStatoDispositivoServizios()
      .stream()
      .filter(deviceServiceState -> deviceServiceState.getStato().equals(StatoDS.ATTIVO) || deviceServiceState.getStato().equals(StatoDS.IN_ATTIVAZIONE))
      .map(deviceServiceState -> deviceServiceState.getTipoServizio())
      .map(serviceType -> serviceType.getNome())
      .collect(toList());
    _log.info("OrderRequestDecorator {} active services names : {}", orderRequestEntity.getIdentificativo(), activeServicesNames);
    return activeServicesNames;
  }
  
  public List<String> findNotActiveServicesNames() {
    final List<String> activeServicesNames = getDispositivo().get()
      .getStatoDispositivoServizios()
      .stream()
      .filter(deviceServiceState -> deviceServiceState.getStato().equals(StatoDS.NON_ATTIVO) || deviceServiceState.getStato().equals(StatoDS.IN_DISATTIVAZIONE))
      .map(deviceServiceState -> deviceServiceState.getTipoServizio())
      .map(serviceType -> serviceType.getNome())
      .collect(toList());
    _log.info("OrderRequestDecorator {} NOT active services names : {}", orderRequestEntity.getIdentificativo(), activeServicesNames);
    return activeServicesNames;
  }

  public String findCodiceCliente() {
    String code = null;

    Optional<Contratto> contractOpt = getContratto();
    if (contractOpt.isPresent()) {
      code = contractOpt.get().getClienteFai() != null ? contractOpt.get().getClienteFai().getCodiceCliente() : null;
    } else {
      throw new IllegalStateException("Contract not found for OrderRequestDecorator " + orderRequestEntity.getIdentificativo());
    }
    _log.info("OrderRequestDecorator {} codice cliente : {}", orderRequestEntity.getIdentificativo(), code);
    return code;
  }

  public String findNumeroContract() {
    String contractUuid = null;

    Optional<Contratto> contractOpt = getContratto();
    if (contractOpt.isPresent()) {
      contractUuid = contractOpt.get().getCodContrattoCliente();
    } else {
      throw new IllegalStateException("Contract not found for OrderRequestDecorator " + orderRequestEntity.getIdentificativo());
    }
    _log.info("OrderRequestDecorator {} contract numero : {}", orderRequestEntity.getIdentificativo(), contractUuid);
    return contractUuid;
  }

  public String getDeviceSerialId() {
    final String deviceSerialId = getDispositivo().get().getSeriale();
    _log.info("OrderRequestDecorator {} device serial id : {}", orderRequestEntity.getIdentificativo(), deviceSerialId);
    return deviceSerialId;
  }

  public String getDeviceTypeName() {
    final String deviceTypeName = getDeviceType().name();
    _log.info("OrderRequestDecorator {} device type name : {}", orderRequestEntity.getIdentificativo(), deviceTypeName);
    return deviceTypeName;
  }

  public String getServiceName() {
    final String serviceName = orderRequestEntity.getNomeServizio();
    _log.info("OrderRequestDecorator {} service name : {}", orderRequestEntity.getIdentificativo(), serviceName);
    return serviceName;
  }

  public String getUuid() {
    return orderRequestEntity.getIdentificativo();
  }

  public TipoDispositivoEnum getDeviceType() {
    TipoDispositivoEnum deviceType = orderRequestEntity.getTipoDispositivo().getNome();
    _log.debug("Device type: {}", deviceType);
    return deviceType;
  }

  public String getLicensePlate() {
    return orderRequestEntity.getAssociazione();
  }

  public String getIdentificativo() {
    return orderRequestEntity.getIdentificativo();
  }

  public String getCountry() {
    return orderRequestEntity.getCountry();
  }

  public String getOldLicensePlate() {
    String newTargaNazione = orderRequestEntity.getNewTargaNazione();
    if (StringUtils.isNotBlank(newTargaNazione)) {
      String[] split = newTargaNazione.split(SEPARATOR_TARGA_NAZIONE);
      if (split != null && split.length > 0) {
        return split[0];
      }
    }
    return null;
  }

  public String getOldCountry() {
    String newTargaNazione = orderRequestEntity.getNewTargaNazione();
    if (StringUtils.isNotBlank(newTargaNazione)) {
      String[] split = newTargaNazione.split(SEPARATOR_TARGA_NAZIONE);
      if (split != null && split.length > 1) {
        return split[1];
      }
    }
    return null;
  }

  private Optional<Contratto> getContratto() {
    return getContrattoByRequest().isPresent() ? getContrattoByRequest() : getContrattoByDevice();
  }

  private Optional<Contratto> getContrattoByRequest() {
    Optional<Contratto> optContractOfRequest = Optional.ofNullable(orderRequestEntity.getContratto());
    _log.debug("Contract of Request: {}", optContractOfRequest);
    return optContractOfRequest;
  }

  private Optional<Dispositivo> getDispositivo() {
    Optional<Dispositivo> deviceOpt = orderRequestEntity.getDispositivos().stream().findFirst();
    _log.debug("Device: {}", deviceOpt);
    return deviceOpt;
  }

  private Optional<Contratto> getContrattoByDevice() {
    Optional<Dispositivo> optDevice   = getDispositivo();
    Optional<Contratto>   optContract = optDevice.isPresent() ? Optional.ofNullable(optDevice.get().getContratto())
                                                              : Optional.empty();
    _log.debug("Contract of Device: {}", optContract);
    return optContract;
  }

  public Set<DeviceService> getServices() {
    return getDispositivo().map(d -> d.getStatoDispositivoServizios()).map(services -> services.stream().map(s -> {
      DeviceService ser = new DeviceService(ServiceUuid.fromServiceTypeName(s.getTipoServizio().getNome()));
      if(s.getDataScadenza() != null) {
        ser.setExpirationDate(s.getDataScadenza().atZone(ZoneId.systemDefault()).toLocalDate());
      }
      ser.setPan(s.getPan());
      return ser;
    }).collect(Collectors.toSet())).orElse(Collections.emptySet());
  }

}
