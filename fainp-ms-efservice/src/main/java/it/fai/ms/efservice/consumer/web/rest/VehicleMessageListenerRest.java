package it.fai.ms.efservice.consumer.web.rest;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import it.fai.ms.efservice.consumer.VehicleMessageListener;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

@Deprecated
@Controller
@RequestMapping("/api/listener")
public class VehicleMessageListenerRest implements VehicleMessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private List<VehicleConsumer> consumers = new LinkedList<>();

  @Autowired
  public VehicleMessageListenerRest(final List<VehicleConsumer> _consumers) {
    consumers = _consumers;
    _log.info("Consumers: {}", _consumers);
  }

  @Override
  @PostMapping("/vehicle")
  public void receive(@RequestBody @NotNull final VehicleMessage _vehicleMessage) {
    _log.info("Received message: {}", _vehicleMessage);

    final CompletableFuture<Void> runAsync = CompletableFuture.runAsync(() -> {
      consumers.stream()
               .forEach(consumer -> {
                 _log.debug("Dispatching Message {} to consumer {}", _vehicleMessage, consumer);
                 consumer.consume(_vehicleMessage);
               });
      })
      .exceptionally(t -> {
        _log.error("Error dispatching VehicleMessage {} {}", _vehicleMessage, t);
        return null;
      });

    runAsync.thenRun(() -> _log.info("Message {} consumed", _vehicleMessage))
            .exceptionally(t -> {
              _log.error("Error consuming VehicleMessage {} {}", _vehicleMessage, t);
              return null;
            });
    _log.info("Message queued: {}", _vehicleMessage);
  }

  @Override
  public void persistVehicleNoTargaScorta(Map<String, String> mapVehicle) {


  }



}
