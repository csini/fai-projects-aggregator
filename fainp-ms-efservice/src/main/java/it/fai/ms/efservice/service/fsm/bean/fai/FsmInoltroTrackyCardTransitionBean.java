package it.fai.ms.efservice.service.fsm.bean.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.bean.StatesOnTransition;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.fai.FsmInoltroTrackyCard;
import it.fai.ms.efservice.service.fsm.util.NotificationActivationTrackyCardService;

@WithStateMachine(id = FsmInoltroTrackyCard.FSM_INOLTRO_TRACKYCARD)
public class FsmInoltroTrackyCardTransitionBean extends AbstractFsmRichiestaTransition {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private NotificationActivationTrackyCardService notificationActivatedTrackyService;

  @StatesOnTransition(source = StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI, target = StatoRichiesta.ATTIVO_EVASO)
  public void switchStateTransition(StateContext<StatoRichiesta, RichiestaEvent> stateContext) {
    log.info("Transition from {} to {}", stateContext.getSource()
                                                     .getId(),
             stateContext.getTarget()
                         .getId());
    Message<RichiestaEvent> message = stateContext.getMessage();
    MessageHeaders headers = message.getHeaders();
    Object object = headers.get("object");
    if (object instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) object;
      log.info("Call notification activation to Richiesta: {}", richiesta);
      notificationActivatedTrackyService.notifyActivation(richiesta);
    } else {
      log.warn("Not notify activation TrackyCard....");
    }
  }

}
