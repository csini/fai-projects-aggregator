package it.fai.ms.efservice.service.jms.listener.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceReturnedConsumer;

@Service
@Transactional
public class JmsListenerDeviceReturned
  extends JmsObjectMessageListenerTemplate<DeviceEventMessage>
  implements JmsQueueListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceReturnedConsumer consumer;

  @Autowired
  public JmsListenerDeviceReturned(DeviceReturnedConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  protected void consumeMessage(DeviceEventMessage eventMessage) {
    _log.info("Received jms message {}", eventMessage);
    try {
    consumer.consume(eventMessage);
    } catch (Exception e) {
      _log.error("Exception on consumer message DeviceReturned", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_RETURNED;
  }
}
