package it.fai.ms.efservice.service;

import it.fai.ms.efservice.repository.SpedizioneFornitoreViewRepositoryExt;
import it.fai.ms.efservice.service.dto.SearchSpedizioneFornitoreDTO;
import it.fai.ms.efservice.service.dto.SpedizioneFornitoreViewDTO;
import it.fai.ms.efservice.service.specifications.SearchSpedizioneFornitoreSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SpedizioneFornitoreServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final SpedizioneFornitoreViewRepositoryExt repositoryExt;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public SpedizioneFornitoreServiceExt(SpedizioneFornitoreViewRepositoryExt repositoryExt) {
    this.repositoryExt = repositoryExt;
  }

  public Page<SpedizioneFornitoreViewDTO> findAllWithFilter(SearchSpedizioneFornitoreDTO dto, Pageable page) {

    SearchSpedizioneFornitoreSpecification searchSpeficification = new SearchSpedizioneFornitoreSpecification(dto);

    Page<SpedizioneFornitoreViewDTO> result = repositoryExt.findAll(searchSpeficification, page)
      .map(spedizioneFornitoreView -> {
        log.debug("Element: {}", spedizioneFornitoreView);
        SpedizioneFornitoreViewDTO spedizioneDto = new SpedizioneFornitoreViewDTO();
        spedizioneDto.setId(spedizioneFornitoreView.getId());
        spedizioneDto.setProduttore(spedizioneFornitoreView.getProduttore());
        spedizioneDto.setNumeroDispositiviDaRicevere(spedizioneFornitoreView.getNumeroDispositiviDaRicevere());
        spedizioneDto.setNumeroDispositiviDisponibili(spedizioneFornitoreView.getNumeroDispositiviDisponibili());
        spedizioneDto.setNumeroDispositiviSpedibili(spedizioneFornitoreView.getNumeroDispositiviSpedibili());
        spedizioneDto.setTipoDispositivo(spedizioneFornitoreView.getTipoDispositivo());
        spedizioneDto.setNotcheckable(
          spedizioneDto.getNumeroDispositiviSpedibili() == 0
        );
        log.debug("Generate {}", spedizioneDto);
        return spedizioneDto;
      });

    return new PageImpl<>(result.getContent(), page, result.getTotalElements());
  }

}
