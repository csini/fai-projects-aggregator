package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmSwitchStateException;
import it.fai.ms.efservice.service.fsm.util.FsmContrattoUtilService;

public class AbstractFsmContrattoTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private FsmContrattoUtilService fsmServiceUtil;

  @OnTransition
  public void anyTransition(StateContext<StatoContratto, ContrattoEvent> stateContext) throws FsmSwitchStateException {
    State<StatoContratto, ContrattoEvent> source = stateContext.getSource();
    if (source != null) {
      Transition<StatoContratto, ContrattoEvent> transition = stateContext.getTransition();
      State<StatoContratto, ContrattoEvent> target = stateContext.getTarget();
      StatoContratto toState = target.getId();
      Message<ContrattoEvent> message = stateContext.getMessage();

      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      boolean isValidEntity = validateEntity(object);
      if (isValidEntity) {
        if (object instanceof Contratto) {
          Contratto contratto = (Contratto) object;
          String nota = getNotaFromMessage(headers);
          log.debug("NOTA NOT SET ON CONTRATTO..... {}", nota);
          logTransitionFromTo(source, target);
          fsmServiceUtil.switchToState(contratto, toState, transition);
        }
      }
    }
  }

  private String getNotaFromMessage(MessageHeaders headers) {
    String nota = null;
    Object object = headers.get("nota");
    if (object != null) {
      if (object instanceof String) {
        nota = (String) object;
      }
    } else {
      log.warn("Nota is null");
    }
    return nota;
  }

  private boolean validateEntity(Object obj) {
    if (obj == null) {
      log.error("Entity to change status is null");
      return false;
    }
    return true;
  }

  private void logTransitionFromTo(State<StatoContratto, ContrattoEvent> source, State<StatoContratto, ContrattoEvent> target) {
    log.debug("Transition From [" + ((source != null) ? source.getId() : null) + "] To [" + ((target != null) ? target.getId() : null)
              + "].");
  }

}
