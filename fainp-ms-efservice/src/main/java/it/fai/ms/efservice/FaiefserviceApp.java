package it.fai.ms.efservice;

import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_CLOUD;
import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_DEVELOPMENT;
import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_PRODUCTION;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.ms.integration.system.MessageSystemConfiguration;
import it.fai.ms.integration.system.plug.artemis.client.ArtemisClient;
import it.fai.ms.integration.system.plug.artemis.client.ArtemisFaiJmsListenerEndpointRegistrar;
import it.fai.ms.integration.system.plug.artemis.config.ArtemisAdapter;
import it.fai.ms.integration.system.plug.artemis.config.ArtemisJmsListenerAdapter;
import it.fai.ms.integration.system.sock.core.factory.MessageSystemManager;
import it.fai.ms.integration.system.sock.core.factory.PluginFactory;
import it.fai.ms.integration.system.plug.artemis.config.PluginConfig;
import it.fai.ms.integration.system.sock.core.SpringPluginContextAutoConfiguration;
import it.fai.ms.integration.system.sock.core.internal.CCLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.MetricFilterAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.MetricRepositoryAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.efservice.config.ApplicationProperties;
import it.fai.ms.efservice.config.DefaultProfileUtil;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.statemachine.config.common.annotation.EnableAnnotationConfiguration;

@ComponentScan
///@ConditionalOnBean({PluginFactory.class,
//PluginConfig.class})
@EnableAnnotationConfiguration
@EnableAutoConfiguration(exclude =
  { MetricFilterAutoConfiguration.class, MetricRepositoryAutoConfiguration.class,
LiquibaseAutoConfiguration.class})
@EnableConfigurationProperties({ LiquibaseProperties.class, ApplicationProperties.class })
@EnableDiscoveryClient
@EnableFeignClients
  @EntityScan( basePackages = { "it.fai.ms.efservice",
  "it.fai.common.notification.domain",
  "it.fai.ms.integration.system.plug.artemis.config",
  "it.fai.ms.integration.system.plug.artemis.client"},
    basePackageClasses = {MessageSystemConfiguration.class}
  )
@EnableJpaRepositories({"it.fai.ms.efservice.repository", "it.fai.common.notification.repository"})
@Import({MessageSystemConfiguration.class, ArtemisAdapter.class,ArtemisClient.class,ArtemisFaiJmsListenerEndpointRegistrar.class,ArtemisJmsListenerAdapter.class,PluginConfig.class,SpringPluginContextAutoConfiguration.class,
          JmsConfiguration.class, NotificationsConfiguration.class})
public class FaiefserviceApp {

  private static final Logger log = LoggerFactory.getLogger(FaiefserviceApp.class);

  /**
   * Main method, used to run the application.
   *
   * @param args
   *          the command line arguments
   * @throws UnknownHostException
   *           if the local host name could not be resolved into an address
   */
  public static void main(String[] args) throws UnknownHostException, InvocationTargetException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException {
    SpringApplication app = new SpringApplication(FaiefserviceApp.class);
    DefaultProfileUtil.addDefaultProfile(app);
    Environment env = app.run(args)
                         .getEnvironment();
    String protocol = "http";
    if (env.getProperty("server.ssl.key-store") != null) {
      protocol = "https";
    }
    log.info("\n----------------------------------------------------------\n\t" + "Application '{}' is running! Access URLs:\n\t"
             + "Local: \t\t{}://localhost:{}\n\t" + "External: \t{}://{}:{}\n\t"
             + "Profile(s): \t{}\n----------------------------------------------------------", env.getProperty("spring.application.name"),
             protocol, env.getProperty("server.port"), protocol, InetAddress.getLocalHost()
                                                                            .getHostAddress(),
             env.getProperty("server.port"), env.getActiveProfiles());

    String configServerStatus = env.getProperty("configserver.status");
    log.info("\n----------------------------------------------------------\n\t"
             + "Config Server: \t{}\n----------------------------------------------------------",
             configServerStatus == null ? "Not found or not setup for this application" : configServerStatus);

    String progClass = args[0];
    String[] proArgs = new String[args.length - 1];
    System.arraycopy(args, 1, proArgs, 0, proArgs.length);
    /*
    CCLoader ccl = new CCLoader(FaiefserviceApp.class.getClassLoader());
    Class clas = ccl.loadClass(progClass);
    Class[] mainArgsType = { (new String[0]).getClass() };
    Method mainMethod = clas.getMethod("main", mainArgsType);

    Object[] argsArray = {proArgs};
    mainMethod.invoke(null, argsArray);

     */
  }


  private final Environment env;

  public FaiefserviceApp(final Environment _env) {
    env = _env;
  }

  /**
   * Initializes faiefservice.
   * <p>
   * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
   * <p>
   * You can find more information on how profiles work with JHipster on
   * <a href="http://www.jhipster.tech/profiles/">http://www.jhipster.tech/profiles/</a>.
   */
  @PostConstruct
  public void initApplication() {
    Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    if (activeProfiles.contains(SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(SPRING_PROFILE_PRODUCTION)) {
      log.error("You have misconfigured your application! It should not run with both the 'dev' and 'prod' profiles at the same time.");
    }
    if (activeProfiles.contains(SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(SPRING_PROFILE_CLOUD)) {
      log.error("You have misconfigured your application! It should not run with both the 'dev' and 'cloud' profiles at the same time.");
    }
  }
}
