/**
 * 
 */
package it.fai.ms.efservice.service.fsm.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;

public class StateMachineEventListener<S, E> extends StateMachineListenerAdapter<S, E> {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Override
  public void stateChanged(State<S, E> from, State<S, E> to) {
    if (log.isDebugEnabled()) {
      StringBuilder sb = new StringBuilder("Changed state\n");
      sb.append("From: ")
        .append(from)
        .append("\n");
      sb.append("TO: ")
        .append(to);
      log.debug(sb.toString());
    }
  }

  @Override
  public void stateEntered(State<S, E> state) {
  }

  @Override
  public void stateExited(State<S, E> state) {
  }

  @Override
  public void transition(Transition<S, E> transition) {
  }

  @Override
  public void transitionStarted(Transition<S, E> transition) {
  }

  @Override
  public void transitionEnded(Transition<S, E> transition) {
  }

  @Override
  public void stateMachineStarted(StateMachine<S, E> stateMachine) {
  }

  @Override
  public void stateMachineStopped(StateMachine<S, E> stateMachine) {
  }

  @Override
  public void eventNotAccepted(Message<E> event) {
    Object object = event.getHeaders()
                         .get("object");
    if (object instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) object;
      log.error("StateMachine Listener Richiesta {} => Event Not Accepted: {}", richiesta.getIdentificativo(),
                ((event != null) ? event.getPayload() : null));
    }

    if (object instanceof Dispositivo) {
      Dispositivo dispositivo = (Dispositivo) object;
      log.error("StateMachine Listener Dispositivo {} => Event Not Accepted: {}", dispositivo.getIdentificativo(),
                ((event != null) ? event.getPayload() : null));
    }

    throw new RuntimeException("Event not accepted to State Machine...");
  }

  @Override
  public void extendedStateChanged(Object key, Object value) {
  }

  @Override
  public void stateMachineError(StateMachine<S, E> stateMachine, Exception exception) {
    log.error("Exception on FSM: {}", exception);
    throw new RuntimeException(exception);
  }

  @Override
  public void stateContext(StateContext<S, E> stateContext) {
  }
}
