package it.fai.ms.efservice.rules.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

@Service
public class RulePrecalculatorImpl implements RulePrecalculator {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineFactory     ruleEngineFactory;
  private RuleOutcomeRepository ruleOutcomeRepository;

  public RulePrecalculatorImpl(final RuleEngineFactory _ruleEngineFactory, final RuleOutcomeRepository _ruleOutcomeRepository) {
    ruleEngineFactory = _ruleEngineFactory;
    ruleOutcomeRepository = _ruleOutcomeRepository;
  }

  @Override
  public RuleOutcomeEntity process(final RuleContext _ruleContext, final RuleBucket _ruleId) {
    _log.debug("Processing rule context {} for ruleId {}", _ruleContext, _ruleId);
    return ruleOutcomeRepository.save(_ruleContext, ruleEngine(_ruleContext, _ruleId).executeRule());
  }

  private RuleEngine ruleEngine(final RuleContext _ruleContext, final RuleBucket _ruleId) {
    return ruleEngineFactory.newRuleEngine(_ruleContext, _ruleId);
  }

}
