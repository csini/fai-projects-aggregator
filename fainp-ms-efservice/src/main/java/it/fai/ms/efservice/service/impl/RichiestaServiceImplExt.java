package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.MezzoRitargatoDTO;
import it.fai.ms.common.jms.dto.telepass.TelepassEuGiustificativoAggiuntoDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.KeyDatiAggiuntivi;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TypeClassValue;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.DatiAggiuntiviRichiestaService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.TipoServizioService;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;
import it.fai.ms.efservice.service.dto.OrdineDTO;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.dto.datiAggiuntiviCarrelloDTO;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.fsm.util.FsmUtil;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.dml.OrdineClienteDmlSenderUtil;
import it.fai.ms.efservice.service.mapper.ClienteFaiMapper;
import it.fai.ms.efservice.service.mapper.IndirizzoSpedizioneOrdiniMapper;
import it.fai.ms.efservice.service.mapper.RichiestaMapper;
import it.fai.ms.efservice.service.mapper.TipoDispositivoMapper;

/**
 * Service Implementation for managing Richiesta.
 */
@Service
@Transactional
public class RichiestaServiceImplExt implements RichiestaServiceExt {

  public static final String PEDAGGI_AUSTRIA = "PEDAGGI_AUSTRIA";
  private final Logger       log             = LoggerFactory.getLogger(RichiestaServiceImplExt.class);

  private final static String DISPOSITIVI_DA_RESTITUIRE = "DA_RESTITUIRE";

  private final static String NUOVO_DISPOSITIVO = "NUOVO_DISPOSITIVO";

  private final static String SEPARATOR_TARGA_NAZIONE = "§";

  @Autowired
  JmsProperties jmsProperties;

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private RichiestaMapper richiestaMapper;

  @Autowired
  private DispositivoServiceExt dispositivoServiceExt;

  @Autowired
  private ContrattoServiceExt contrattoServiceExt;

  @Autowired
  private AssociazioneDVRepository assDispVeicoloRepository;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioniMapper;

  @Autowired
  private ClienteFaiMapper clienteFaiMapper;

  @Autowired
  private JmsTopicSenderService jmsSenderService;

  @Autowired
  private TipoDispositivoMapper tipoDispositivoServiceMapper;

  @Autowired
  private TipoServizioService tipoServizioService;

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtils;

  @Autowired
  private FsmUtil fsmUtil;

  @Autowired
  private DatiAggiuntiviRichiestaService datiAggiungitiviRichiestaService;

  private final OrdineClienteServiceExt ordineClienteServiceExt;

  public RichiestaServiceImplExt(final OrdineClienteServiceExt _ordineClienteServiceExt) {
    ordineClienteServiceExt = _ordineClienteServiceExt;
  }

  @Override
  public RichiestaDTO getRichiestaByIdentificativo(String identificativoRichiesta, boolean fetchEager) throws Exception {

    if (StringUtils.isBlank(identificativoRichiesta)) {
      String identificativoNull = "Identificativo richiesta is NULL";
      log.error(identificativoNull);
      throw new Exception(identificativoNull);
    }

    RichiestaDTO richiestaDTO = findRichiestaDTOByIdentificativo(identificativoRichiesta, fetchEager);
    if (richiestaDTO == null) {
      if (StringUtils.isBlank(identificativoRichiesta)) {
        String notFoundOrder = "Not found Order for identificativo: " + identificativoRichiesta;
        log.error(notFoundOrder);
        throw new Exception(notFoundOrder);
      }
    }

    return richiestaDTO;
  }

  @Override
  public List<Richiesta> save(List<Richiesta> richieste) {
    return richiestaRepositoryExt.save(richieste);
  }

  @Override
  public Richiesta save(Richiesta richiesta) {
    return richiestaRepositoryExt.save(richiesta);
  }

  @Override
  public Richiesta saveAndFlush(Richiesta richiesta) {
    return richiestaRepositoryExt.saveAndFlush(richiesta);
  }

  @Override
  public Richiesta associateContrattoToRichiesta(String _codiceContratto, String _orderRequestUuid) throws Exception {
    Contratto contract = null;

    Richiesta orderRequest = findRichiestaByIdentificativo(_orderRequestUuid, true);
    if (orderRequest == null) {
      log.error("Not Found richiesta: " + _orderRequestUuid + " to create contratto " + _codiceContratto);
      return null;
    }

    Produttore produttore = getProduttoreByRichiesta(orderRequest);
    String nomeProduttore = (produttore != null) ? produttore.getNome() : null;
    final Optional<Contratto> optionalContratto = contrattoServiceExt.findByCodiceContrattoAndNomeProduttore(_codiceContratto,
                                                                                                             nomeProduttore);
    Richiesta savedRichiesta = null;
    if (optionalContratto.isPresent()) {
      Contratto contratto = optionalContratto.get();
      log.info("Contract {} already present", contratto);
      savedRichiesta = save(orderRequest.contratto(contratto));
    } else {
      log.info("Creating contract {} ...", _codiceContratto);
      ClienteFai clienteFai = getClienteFaiByRichiesta(orderRequest);
      if (clienteFai != null && produttore != null) {
        Optional<Contratto> contractOpt = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(produttore,
                                                                                                        clienteFai.getCodiceCliente());
        if (contractOpt.isPresent()) {
          contract = contractOpt.get();
          contract = contrattoServiceExt.save(contract.codContrattoCliente(_codiceContratto));
          assignContractOnDispositiviAndSave(orderRequest.getDispositivos(), contract);
          savedRichiesta = save(orderRequest.contratto(contract));
        } else {
          throw new Exception("Not found Contract for Cliente Fai: " + clienteFai + " and Produttore" + produttore + "");
        }
      } else {
        throw new Exception("Not found Cliente Fai and Produttore to set codice contratto " + _orderRequestUuid + " for richiesta");
      }
    }

    return savedRichiesta;
  }

  @Override
  /**
   * Generazione dell'richiesta a partire dal Carrello DTO (Wizard di front-end);
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public List<Richiesta> generateAndSaveOrdineAndRichiesteByCarrelloDTO(CarrelloDTO carrelloDTO) throws Exception {

    if (carrelloDTO == null) {
      String errorMessage = "Cart is null. Service hasn't information to generate request.";
      log.error(errorMessage);
      throw new RuntimeException(errorMessage);
    }

    String operazioniPossibili = fsmUtil.getOperazioniPossibili(FsmType.ACCETTAZIONE.fsmName(),
                                                                new Richiesta().stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    log.info("Operazioni possibili on new Richieste by Carrello DTO: {}", operazioniPossibili);

    ClienteFai clienteFai = null;
    OrdineCliente ordineCliente = null;
    HashMap<String, List<Dispositivo>> mapDispositivoVeicolo = new HashMap<>();
    String codiceAzienda = carrelloDTO.getCodiceAzienda();
    // codice azienda is equal to codice cliente;
    clienteFai = clienteFaiRepo.findOneByCodiceCliente(codiceAzienda);
    // inizializzo l'ordine da parte del cliente;
    ordineCliente = initAndSaveOrdineClienteByCarrelloDTO(carrelloDTO, clienteFai);

    String nomeServizioDefault = carrelloDTO.getTipoServizio();
    log.debug("Tipo Servizio selezionato/richiesto allo step 2 del wizard: {}", nomeServizioDefault);
    // dispositivi => Identifica il numero di tipi di dispositivo che l'utente ha richiesto;
    List<DispositivoCarrelloDTO> devicesCartDTO = carrelloDTO.getDispositivi();
    // Lista di tutti i veicoli per i quali e' stato richiesto un dispositivo;
    List<String> uuidVehicles = carrelloDTO.getUuidVeicoli();
    List<Richiesta> richieste = generateRequest(carrelloDTO, operazioniPossibili, clienteFai, ordineCliente, mapDispositivoVeicolo,
                                                codiceAzienda, devicesCartDTO, uuidVehicles);

    List<Richiesta> richiestasCreated = new ArrayList<>();
    if (!richieste.isEmpty()) {
      // Creazione e persistenza dell'richiesta e dei relativi dati annessi;
      richiestasCreated = createAndPersistRichieste(richieste, mapDispositivoVeicolo);
      Set<Richiesta> richiestas = richiestasCreated.stream()
                                                   .map(r -> r)
                                                   .collect(toSet());
      ordineCliente.setRichiestas(richiestas);
      ordineCliente = ordineClienteServiceExt.save(ordineCliente);
    }

    List<Richiesta> requestsViaCardCreated = generateAndSaveViaCardRequestNeeded(carrelloDTO, operazioniPossibili, clienteFai, ordineCliente,
                                                                          richiestasCreated);
    richiestasCreated.addAll(requestsViaCardCreated);

    ordineCliente = ordineClienteServiceExt.saveAndFlush(ordineCliente);

    if(ordineCliente.getRichiestas()==null || ordineCliente.getRichiestas().isEmpty()) {
    	log.info("DELETE Order perchè non ha richieste valide: {} - {}", ordineCliente.getIdentificativo(), ordineCliente.getNumeroOrdine());
       ordineClienteServiceExt.delete(ordineCliente);
    } else {
      try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(ordineCliente);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
        //new OrdineClienteDmlSenderUtil(jmsProperties).sendSaveNotification(ordineCliente);
    }

    return richiestasCreated;
  }

  private List<Richiesta> generateAndSaveViaCardRequestNeeded(CarrelloDTO carrelloDTO, String operazioniPossibili, ClienteFai clienteFai,
                                                       OrdineCliente ordineCliente, List<Richiesta> richiestasCreated) throws Exception {
    List<Richiesta> requestsViaCardCreated = new ArrayList<>();
    Set<Dispositivo> viaCardGenerated = checkAndCreateViaCard(carrelloDTO, clienteFai);
    if (viaCardGenerated != null && !viaCardGenerated.isEmpty()) {
      TipoDispositivo viaCardType = dispositivoServiceExt.searchTipoDispositivoByNome(TipoDispositivoEnum.VIACARD.name());
      requestsViaCardCreated = mapToRichieste(viaCardGenerated, viaCardType, clienteFai, ordineCliente, operazioniPossibili, null);
      Contratto contrattoViaCard = manageContrattoByProduttoreAndCliente(viaCardType, clienteFai);
      log.debug("Contratto ViaCard - ClienteFai [{}]: {}", clienteFai, contrattoViaCard);

      requestsViaCardCreated = requestsViaCardCreated.stream()
                                                     .map(r -> r.contratto(contrattoViaCard))
                                                     .collect(toList());
      requestsViaCardCreated = setPuntoBluTrueAndContractOnDeviceRequestViaCard(requestsViaCardCreated, contrattoViaCard);
      requestsViaCardCreated = richiestaRepositoryExt.save(requestsViaCardCreated);
    }
    return requestsViaCardCreated;
  }

  private List<Richiesta> setPuntoBluTrueAndContractOnDeviceRequestViaCard(List<Richiesta> requestViaCardCreated,
                                                                           Contratto contrattoViaCard) {
    return requestViaCardCreated.stream()
                                .map(r -> {
                                  Set<Dispositivo> dispositivi = r.getDispositivos();
                                  Optional<Dispositivo> optDispositivo = dispositivi.stream()
                                                                                    .findFirst();
                                  if (optDispositivo.isPresent()) {
                                    Dispositivo device = optDispositivo.get();
                                    TipoMagazzino tipoMagazzino = device.getTipoMagazzino();
                                    if (tipoMagazzino == null || tipoMagazzino != TipoMagazzino.SCORTA) {
                                      r.setPuntoBlu(true);
                                      r.setContratto(contrattoViaCard);
                                    }
                                  } else {
                                    throw new RuntimeException("Not found Device on Request: " + r);
                                  }
                                  return r;
                                })
                                .collect(toList());
  }

  private List<Richiesta> generateRequest(CarrelloDTO carrelloDTO, String operazioniPossibili, ClienteFai clienteFai,
                                          OrdineCliente ordineCliente, HashMap<String, List<Dispositivo>> mapDispositivoVeicolo,
                                          String codiceAzienda, List<DispositivoCarrelloDTO> devicesCartDTO,
                                          List<String> uuidVehicles) throws Exception {

    List<Richiesta> richieste = new ArrayList<>();
    Iterator<DispositivoCarrelloDTO> devicesCartDTOIterator = devicesCartDTO.iterator();
    // Ogni istanza di dispositivoCarrelloDTO identifica una differente tipologia di Dispositivo.
    // Itero ogni tipologia di dispositivo in modo da generare almeno un richiesta per tipologia dispositivo.
    // Potrebbe essere necessario generare un richiesta di malfunzionamento/rientro, nel caso su un dispositivo sia
    // necessario aggiungere un nuovo servizio.
    while (devicesCartDTOIterator != null && devicesCartDTOIterator.hasNext()) {
      DispositivoCarrelloDTO deviceCartDTO = devicesCartDTOIterator.next();
      String deviceTypeName = deviceCartDTO.getTipoDispositivo();
      log.debug("Nome Tipo Dispositivo: {}", deviceTypeName);
      TipoDispositivo tipoDispositivo = dispositivoServiceExt.searchTipoDispositivoByNome(deviceTypeName);
      if (tipoDispositivo == null) {
        throw new Exception("Tipo dispositivo [" + tipoDispositivo + "] for dispositivoDTO: "
                            + ((deviceCartDTO != null) ? deviceCartDTO.toString() : null));
      }
      TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
      log.debug("Manage device type: {}", deviceType);

      boolean puntoBlu = deviceCartDTO.isPuntoBlu();
      if (tipoDispositivo.getNome() == TipoDispositivoEnum.TELEPASS_ITALIANO && puntoBlu) {
        log.info("Skip generation order request because device type is {} and user {} has selected PUNTO_BLU: [{}]", deviceType,
                 codiceAzienda, puntoBlu);
        continue;
      }

      Contratto contract = manageContrattoByProduttoreAndCliente(tipoDispositivo, clienteFai);
      List<VeicoloCarrelloDTO> vehicleByDeviceType = deviceCartDTO.getVeicoli();
      List<VeicoloCarrelloDTO> vehiclesThatNeededTrackycard = new ArrayList<>();
      TipoDispositivo trackyCard = null;
      if (tipoDispositivo.getNome()
                         .equals(TipoDispositivoEnum.TOLL_COLLECT)) {
        trackyCard = dispositivoServiceExt.searchTipoDispositivoByNome(TipoDispositivoEnum.TRACKYCARD.name());
        String tipoServizio = PEDAGGI_AUSTRIA;
        List<String> uuidVehicleNeedTrackyCard = getVehichlesNeedTrackycard(carrelloDTO, deviceCartDTO, tipoServizio);
        vehiclesThatNeededTrackycard = createVehicleCartDtoToVehiclesNeedTrackyCard(uuidVehicleNeedTrackyCard, tipoServizio);
        log.debug("Founded " + vehiclesThatNeededTrackycard.size() + " vehicles that need the trackycard");
      }
      // Per ogni veicolo genero un dispositivo.
      for (VeicoloCarrelloDTO vehicleCartDTO : vehicleByDeviceType) {
        String uuidVehicleToManage = vehicleCartDTO.getUuid();
        if (uuidVehicles.contains(uuidVehicleToManage)) {
          log.debug("Uuid Veicolo: {} for Device Type: {}", uuidVehicleToManage, tipoDispositivo.getNome());
          List<datiAggiuntiviCarrelloDTO> datiAggiuntiviCarrello = null;
          HashMap<TipoDispositivoEnum, List<datiAggiuntiviCarrelloDTO>> reservations = carrelloDTO.getReservations();
          if (reservations != null && reservations.containsKey(deviceType)) {
            List<datiAggiuntiviCarrelloDTO> list = reservations.get(deviceType);
            if (list != null && !list.isEmpty()) {
              datiAggiuntiviCarrello = list.stream()
                                           .filter(data -> data.getIdentificativo()
                                                               .equals(uuidVehicleToManage))
                                           .collect(toList());
            }
          }

          List<Richiesta> newRichieste = manageDevicesAndServicesToMapRichieste(uuidVehicleToManage, codiceAzienda, tipoDispositivo,
                                                                                   vehicleCartDTO, datiAggiuntiviCarrello,
                                                                                   mapDispositivoVeicolo, ordineCliente, clienteFai,
                                                                                   operazioniPossibili, richieste);

          Optional<VeicoloCarrelloDTO> trackyCardToAdd = findVehicleCartDtoTrackyCardToGenerate(vehiclesThatNeededTrackycard,
                                                                                                uuidVehicleToManage);
          if (trackyCardToAdd.isPresent()) {
            log.debug("Adding TrackyCard on Vehicle " + trackyCardToAdd.get()
                                                                       .getUuid());
            Contratto contrattoTrackycard = manageContrattoByProduttoreAndCliente(trackyCard, clienteFai);
            Dispositivo trackyCardDevice = generateTrackyCardDeviceAndPutInMap(mapDispositivoVeicolo, trackyCard, uuidVehicleToManage,
                                                                               contrattoTrackycard);
            Richiesta richiestaTrackyCard = mapToNewRichiesta(TipoRichiesta.NUOVO_ORDINE, StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE,
                                                               trackyCard, trackyCardDevice, ordineCliente, operazioniPossibili, null);
            richiestaTrackyCard.setContratto(contrattoTrackycard);
            richieste.add(richiestaTrackyCard);
          }

          newRichieste = saveContractDevicesAssociation(newRichieste, contract);
          // add richiesta to list will be persist on DB;
          log.info("Add {} richieste da creare...", (newRichieste != null) ? newRichieste.size() : 0);
          if (newRichieste != null) {
            richieste.addAll(newRichieste);
          }
        } // End if condition;
      } // End for-cicle for Vehicle;
    } // End iterator on TipoDispositivo;

    return richieste;
  }

  private Dispositivo generateTrackyCardDeviceAndPutInMap(HashMap<String, List<Dispositivo>> mapDispositivoVeicolo,
                                                          TipoDispositivo trackyCard, String uuidVehicleToManage,
                                                          Contratto contrattoTrackycard) throws Exception {
    List<String> serviziDaAttivare = trackyCard.getTipoServizios()
                                               .stream()
                                               .map(TipoServizio::getNome)
                                               .collect(toList());
    serviziDaAttivare.add(PEDAGGI_AUSTRIA);

    Dispositivo trackyCardDevice = dispositivoServiceExt.createDispositivoServizi(trackyCard, serviziDaAttivare,
                                                                                  Arrays.asList(PEDAGGI_AUSTRIA));
    trackyCardDevice.setContratto(contrattoTrackycard);

    List<Dispositivo> dispositivoByUuidVehicle = mapDispositivoVeicolo.get(uuidVehicleToManage);
    if (dispositivoByUuidVehicle == null) {
      dispositivoByUuidVehicle = new ArrayList<>();
    }
    dispositivoByUuidVehicle.add(trackyCardDevice);

    mapDispositivoVeicolo.put(uuidVehicleToManage, dispositivoByUuidVehicle);
    return trackyCardDevice;
  }

  private Optional<VeicoloCarrelloDTO> findVehicleCartDtoTrackyCardToGenerate(List<VeicoloCarrelloDTO> vehiclesThatNeededTrackycard,
                                                                              String uuidVehicleToManage) {
    return vehiclesThatNeededTrackycard.stream()
                                       .filter(vc -> uuidVehicleToManage.equals(vc.getUuid()))
                                       .findFirst();
  }

  private List<VeicoloCarrelloDTO> createVehicleCartDtoToVehiclesNeedTrackyCard(List<String> uuidVehicleNeedTrackyCard,
                                                                                String tipoServizio) {
    return uuidVehicleNeedTrackyCard.stream()
                                    .map(uuid -> {
                                      VeicoloCarrelloDTO veicoloCarrelloDTO = new VeicoloCarrelloDTO();
                                      veicoloCarrelloDTO.setNomeTipiServizio(Arrays.asList(tipoServizio));
                                      veicoloCarrelloDTO.setUuid(uuid);
                                      return veicoloCarrelloDTO;
                                    })
                                    .collect(toList());
  }

  private List<String> getVehichlesNeedTrackycard(CarrelloDTO carrelloDTO, DispositivoCarrelloDTO tipoDispositivoDTO, String tipoServizio) {
    List<String> vehiclesList = new ArrayList<>();
    // veicoli per i quali è stato chiesto un toll collect con pedaggi austria
    List<String> vehicleWithPedaggiAustria = tipoDispositivoDTO.getVeicoli()
                                                               .stream()
                                                               .filter(vc -> vc.getNomeTipiServizio()
                                                                               .contains(tipoServizio))
                                                               .map(VeicoloCarrelloDTO::getUuid)
                                                               .collect(toList());
    if (!vehicleWithPedaggiAustria.isEmpty()) {
      // veicoli nel carrello per cui è stata richiesta una trackycard
      List<String> vehiclesInCarrelloWithTrackyCard = carrelloDTO.getDispositivi()
                                                                 .stream()
                                                                 .filter(dc -> dc.getTipoDispositivo()
                                                                                 .equals(TipoDispositivoEnum.TRACKYCARD.name()))
                                                                 .flatMap(dc -> dc.getVeicoli()
                                                                                  .stream())
                                                                 .map(vc -> vc.getUuid())
                                                                 .collect(toList());

      vehicleWithPedaggiAustria = vehicleWithPedaggiAustria.stream()
                                                           .filter(vehicleUUID -> !vehiclesInCarrelloWithTrackyCard.contains(vehicleUUID))
                                                           .collect(toList());
      if (!vehicleWithPedaggiAustria.isEmpty()) {
        Set<String> targaToOrderTrackycard = dispositivoServiceExt.checkDispositivoExists(carrelloDTO.getCodiceAzienda(),
                                                                                          TipoDispositivoEnum.TRACKYCARD,
                                                                                          new HashSet<>(vehicleWithPedaggiAustria));
        // da targa a vehicleDTO
        vehiclesList = vehicleWithPedaggiAustria.stream()
                                                .filter(uuid -> targaToOrderTrackycard.contains(uuid))
                                                .collect(toList());
      }
    }
    return vehiclesList;
  }

  private Contratto manageContrattoByProduttoreAndCliente(TipoDispositivo tipoDispositivo, ClienteFai clienteFai) {
    Produttore produttore = tipoDispositivo.getProduttore();
    log.debug("Produttore {} for TipoDispositivo: {}", produttore, tipoDispositivo);
    Contratto contract = null;
    Optional<Contratto> optContract = checkExistsContractByProduttoreAndClienteFai(produttore, clienteFai.getCodiceCliente());
    if (optContract.isPresent()) {
      contract = optContract.get();
    } else {
      contract = newDefaultContract(produttore, clienteFai);
    }
    return contract;
  }

  private Set<Dispositivo> checkAndCreateViaCard(CarrelloDTO carrelloDTO, ClienteFai clienteFai) throws Exception {
    Set<Dispositivo> viaCardDevices = new HashSet<>();
    int numViaCard = carrelloDTO.getNumViaCard();
    List<DispositivoCarrelloDTO> dispositivi = carrelloDTO.getDispositivi();
    Set<String> veicoliWithoutViaCard = dispositivi.stream()
                                                   .filter(deviceCart -> deviceCart.isPuntoBlu()
                                                                         && dispositivoServiceExt.searchTipoDispositivoByNome(deviceCart.getTipoDispositivo())
                                                                                                 .getNome() == TipoDispositivoEnum.TELEPASS_ITALIANO)
                                                   .map(deviceCart -> deviceCart.getVeicoli())
                                                   .flatMap(v -> v.stream())
                                                   .filter(v -> !dispositivoServiceExt.findViaCardByIdentificativoVeicolo(v.getUuid())
                                                                                      .isPresent())
                                                   .map(v -> v.getUuid())
                                                   .collect(toSet());

    for (String identificativoVeicolo : veicoliWithoutViaCard) {
      if (numViaCard > 0) {
        Dispositivo device = createViaCardForTelepassIta(identificativoVeicolo, clienteFai);
        log.info("Created ViaCard: {}", device);
        viaCardDevices.add(device);
        numViaCard--;
      } else {
        break;
      }
    }

    if (numViaCard > 0) {
      while (numViaCard > 0) {
        Dispositivo viaCard = createViaCardForTelepassIta(null, clienteFai);
        log.info("Created ViaCard SCORTA: {}", viaCard);
        viaCardDevices.add(viaCard);
        numViaCard--;
      }
    }

    return viaCardDevices;
  }

  private Dispositivo createViaCardForTelepassIta(String identificativoVeicolo, ClienteFai clienteFai) throws Exception {
    Dispositivo viaCard = null;
    if (StringUtils.isNotBlank(identificativoVeicolo)) {
      viaCard = dispositivoServiceExt.createViaCardOnVehicle(identificativoVeicolo);
    } else {
      viaCard = dispositivoServiceExt.createViaCardOnScorta();
    }

    TipoDispositivo tipoDispositivo = viaCard.getTipoDispositivo();
    Contratto contract = manageContrattoByProduttoreAndCliente(tipoDispositivo, clienteFai);
    Set<Dispositivo> dispositivos = new HashSet<>();
    dispositivos.add(viaCard);
    log.info("Associate Contract: {} on Dispositivo [Type: {}]: {}", contract, tipoDispositivo.getNome(), viaCard);
    dispositivoServiceExt.associateContract(dispositivos, contract);
    return viaCard;
  }

  private List<Richiesta> manageDevicesAndServicesToMapRichieste(String uuid, String codiceAzienda, TipoDispositivo tipoDispositivo,
                                                                    VeicoloCarrelloDTO veicoloCarrelloDTO,
                                                                    List<datiAggiuntiviCarrelloDTO> datiAggiuntiviCarrello,
                                                                    HashMap<String, List<Dispositivo>> mapDispositivoVeicolo,
                                                                    OrdineCliente ordineCliente, ClienteFai clienteFai,
                                                                    String operazioniPossibili,
                                                                    List<Richiesta> richieste) throws Exception {

    Set<TipoServizio> serviziDaDisattivare = null;
    Set<TipoServizio> serviziDaAttivare = null;
    Set<TipoServizio> serviziDaAttivareDisattivare = null;
    boolean isServiziGestibili = false;

    List<AssociazioneDV> associazioniDv = dispositivoServiceExt.getAssociazioneDvByUuidVeicoloAndCodiceClienteFai(uuid, codiceAzienda);
    log.debug("Associazioni DispositivoVeicoli per UUID_Vehicle {} and codice cliente {}: {}", uuid, codiceAzienda, associazioniDv);
    Dispositivo device = null;
    if (!dispositivoServiceExt.isHgvDevice(tipoDispositivo))
      device = dispositivoServiceExt.findDispositivoByAssociazioneDvTypeAndCliente(associazioniDv, tipoDispositivo, codiceAzienda);

    List<String> nomeTipiServizio = veicoloCarrelloDTO.getNomeTipiServizio();
    if (device != null) {
      Set<StatoDispositivoServizio> sdss = device.getStatoDispositivoServizios();
      log.debug("Stato Dispositivo Servizio {} on Dispositivo {}", (sdss != null) ? sdss.size() : 0, device);
      Set<TipoServizio> serviziAttivi = dispositivoServiceExt.getServiziAttivi(sdss, device);
      serviziDaDisattivare = dispositivoServiceExt.getServiziDaDisattivare(serviziAttivi, nomeTipiServizio);
      serviziDaAttivare = dispositivoServiceExt.getServiziDaAttivare(serviziAttivi, nomeTipiServizio);
      serviziDaAttivareDisattivare = Stream.concat(serviziDaDisattivare.stream(), serviziDaAttivare.stream())
                                           .collect(toSet());

      log.info("Servizi da attivare/disattivare: {}", serviziDaAttivareDisattivare);

      isServiziGestibili = dispositivoServiceExt.isServiziGestibili(tipoDispositivo.getNome(), serviziDaAttivareDisattivare);
      log.info("Is Service Manageable: {}", isServiziGestibili);
    }
    log.info("Found service {} for device {} in config table: {}", nomeTipiServizio, tipoDispositivo.getNome(), isServiziGestibili);
    boolean contains = containsDeviceType(associazioniDv, tipoDispositivo);
    log.info("AssociazioniDv contains TipoDispositivo: {}", contains);

    List<Richiesta> newRichieste = null;
    if (!contains || (contains && !isServiziGestibili)) {
      log.info("Start generation new Device and return old device....");
      newRichieste = generateNewDevicesAndDevicesToReturn(uuid, codiceAzienda, tipoDispositivo, veicoloCarrelloDTO, datiAggiuntiviCarrello,
                                                          mapDispositivoVeicolo, ordineCliente, clienteFai, operazioniPossibili, richieste,
                                                          associazioniDv, nomeTipiServizio);
    } else {
      boolean isServiziGestibiliDaRemoto = dispositivoServiceExt.isServiziGestibiliDaRemoto(tipoDispositivo.getNome(),
                                                                                            serviziDaAttivareDisattivare);
      log.info("Is service manageable by REMOTE: {}", isServiziGestibiliDaRemoto);
      if (isServiziGestibiliDaRemoto) {
        HashMap<TipoRichiesta, Set<TipoServizio>> mapServiziGestibiliDaRemoto = dispositivoServiceExt.getServiziGestibiliDaRemoto(serviziDaDisattivare,
                                                                                                                                  serviziDaAttivare,
                                                                                                                                  device);

        log.debug("Start create richieste di ATTIVAZIONE/DISATTIVAZIONE servizi al dispositivo");
        newRichieste = createRichiesteAttivazioneDisattivazioneServizio(associazioniDv, tipoDispositivo, codiceAzienda,
                                                                        mapServiziGestibiliDaRemoto,

                                                                        ordineCliente);
      } else {
        if (dispositivoServiceExt.isServiziGestibiliOffline(tipoDispositivo.getNome(), serviziDaAttivareDisattivare)) {
          log.info("Is service manageable OFFLINE.");
          dispositivoServiceExt.manageAttivazioneDisattivazioneServiziOffline(serviziDaDisattivare, serviziDaAttivare, device);
        } else {
          throw new Exception("This case is not possible....");
        }
      }
    }

    return newRichieste;
  }

  private List<Richiesta> generateNewDevicesAndDevicesToReturn(String uuid, String codiceAzienda, TipoDispositivo tipoDispositivo,
                                                               VeicoloCarrelloDTO veicoloCarrelloDTO,
                                                               List<datiAggiuntiviCarrelloDTO> datiAggiuntiviCarrello,
                                                               HashMap<String, List<Dispositivo>> mapDispositivoVeicolo,
                                                               OrdineCliente ordineCliente, ClienteFai clienteFai,
                                                               String operazioniPossibili, List<Richiesta> richieste,
                                                               List<AssociazioneDV> associazioniDv,
                                                               List<String> nomeTipiServizio) throws Exception {
    List<Richiesta> newRichieste = new ArrayList<>();
    // Generazione dei dispositivi a partire dall'identificativo del veicolo;
    Map<String, Set<Dispositivo>> dispositiviMap = generateDispositivi(uuid, tipoDispositivo, veicoloCarrelloDTO, mapDispositivoVeicolo,
                                                                       associazioniDv);

    log.debug("MAP DISPOSITIVI: {}", dispositiviMap);
    Set<Dispositivo> dispositiviDaRestituire = dispositiviMap.get(DISPOSITIVI_DA_RESTITUIRE);
    if (dispositiviDaRestituire != null && !dispositiviDaRestituire.isEmpty()) {
      log.debug("Dispositivi da restituire: {}", dispositiviDaRestituire.size());
      // generazione e creazione dell'richiesta dei dispositivi da rientrare perche' e' stato modificato un
      // dispositivo
      // che era gia' in possesso del cliente FAI;
      List<Richiesta> richiesteDispositiviDaRestituire = generateRichiestaDispositiviDaRestituire(dispositiviDaRestituire, codiceAzienda,
                                                                                                  ordineCliente);
      richieste.addAll(richiesteDispositiviDaRestituire);
    }

    // Creazione della richiesta;
    Set<Dispositivo> dispositiviNuoveRichieste = dispositiviMap.get(NUOVO_DISPOSITIVO);
    newRichieste = mapToRichieste(dispositiviNuoveRichieste, tipoDispositivo, clienteFai, ordineCliente, operazioniPossibili,
                                   datiAggiuntiviCarrello);
    newRichieste = setNomiServiziOnRichieste(newRichieste, nomeTipiServizio);
    return newRichieste;
  }

  private List<Richiesta> setNomiServiziOnRichieste(List<Richiesta> newRichieste, List<String> nomeTipiServizio) {
    boolean isFirst = true;
    String nomiServiziStr = "";
    for (String servizioName : nomeTipiServizio) {
      if (!isFirst) {
        nomiServiziStr += ", ";
      }

      nomiServiziStr += servizioName;
      isFirst = false;
    }

    final String strServizi = nomiServiziStr;
    List<Richiesta> newRichiesteWithServizi = newRichieste.stream()
                                                          .map(r -> r.nomeServizio(strServizi))
                                                          .collect(toList());
    return (newRichiesteWithServizi != null && !newRichiesteWithServizi.isEmpty()) ? newRichiesteWithServizi : newRichieste;
  }

  private boolean containsDeviceType(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo) {
    if (associazioniDv == null) {
      return false;
    }
    // E' possibile ordinare sempre un nuovo dispositivo HGV;
    if (TipoDispositivoEnum.HGV.equals(tipoDispositivo.getNome())) {
      return false;
    }
    boolean isPresent = associazioniDv.stream()
                                      .filter(asdv -> asdv.getDispositivo() != null)
                                      .filter(asdv -> asdv.getDispositivo()
                                                          .getTipoDispositivo() != null)
                                      .filter(asdv -> asdv.getDispositivo()
                                                          .getTipoDispositivo()
                                                          .getNome() == tipoDispositivo.getNome())
                                      .findFirst()
                                      .isPresent();

    log.info("Found tipoDispositivo {} in associazioniDv {} ? {}", tipoDispositivo.getNome(), associazioniDv, isPresent);
    return isPresent;
  }

  @Override
  public Richiesta createRichiestaDiSostituzioneFromIdentificativo(String identificativoRichiesta) throws Exception {
    log.info("Start creation Richiesta di sostituzione....");
    Richiesta richiesta = richiestaRepositoryExt.findOneByIdentificativo(identificativoRichiesta);
    if (richiesta == null) {
      throw new NullPointerException("Richiesta with identificativo: " + identificativoRichiesta + " is null.");
    }

    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    log.info("Start creation Richiesta di sostituzione per la Richiesta: [{}] relazionata all'ordine cliente: [{}]", richiesta,
             ordineCliente);
    String uuidVeicolo = null;
    TipoRichiesta tipoRichiesta = richiesta.getTipo();
    log.info("Tipo richiesta {} for Richiesta [ID: {} - Identificativo: {}]", tipoRichiesta, richiesta.getId(), identificativoRichiesta);
    if (tipoRichiesta != null && tipoRichiesta.name() == TipoRichiesta.VARIAZIONE_TARGA.name()) {
      uuidVeicolo = ordineCliente.getUuidVeicolo();
    }

    Richiesta newRichiesta = new Richiesta();
    Set<Dispositivo> dispositivos = richiesta.getDispositivos();
    log.debug("Dispositivi for richiesta [ ID: {} - Identificativo: {}] are: {}", richiesta.getId(), richiesta.getIdentificativo(),
              dispositivos.size());

    TipoDispositivo tipoDisp = richiesta.getTipoDispositivo();
    Set<Dispositivo> newDispositivos = dispositivoServiceExt.cloneDispositivi(dispositivos, richiesta, tipoDisp, uuidVeicolo);
    newDispositivos.forEach(device -> device.dataSpedizione(null));
    newRichiesta.setContratto(richiesta.getContratto());
    newRichiesta.setStato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    newRichiesta.setTipo(TipoRichiesta.NUOVO_ORDINE);
    newRichiesta.setTipoDispositivo(tipoDisp);
    newRichiesta.setData(Instant.now());
    newRichiesta.setDispositivos(newDispositivos);
    manageNewTargaNazioneOnRichiestaSostituzione(newRichiesta, richiesta);

    OrdineCliente ordineClienteUpdated = ordineClienteServiceExt.save(ordineCliente);
    log.info("Save OrdineCliente [Identificativo: {} - NumeroOrdine: {}] for new Richiesta...", ordineClienteUpdated.getIdentificativo(),
             ordineClienteUpdated.getNumeroOrdine());
    newRichiesta.setOrdineCliente(ordineClienteUpdated);

    Richiesta richiestaSaved = save(newRichiesta);
    log.info("Created Richiesta di sostituzione [ID: {} - Identificativo: {} - OrdineCliente {}] for request new Dispositivo [Tipo: {} ]",
             richiestaSaved.getId(), richiestaSaved.getIdentificativo(), richiesta.getOrdineCliente(),
             ((tipoDisp != null) ? tipoDisp.getNome() : null));

    ordineClienteUpdated = ordineClienteServiceExt.save(ordineCliente.addRichiesta(richiestaSaved));
    log.info("Saved ordineCliente {} for add Richiesta {}", ordineClienteUpdated, richiestaSaved);
    return richiestaSaved;
  }

  private void manageNewTargaNazioneOnRichiestaSostituzione(Richiesta newRichiesta, Richiesta richiesta) {
    String newTargaNazione = richiesta.getNewTargaNazione();
    if (StringUtils.isNotBlank(newTargaNazione)) {
      log.info("newTargaNazione: {}", newTargaNazione);
      if (newTargaNazione.contains(SEPARATOR_TARGA_NAZIONE)) {
        String[] splittedString = newTargaNazione.split(SEPARATOR_TARGA_NAZIONE);
        if (splittedString != null) {
          String targa = splittedString[0];
          String nazione = splittedString[1];

          log.info("Set Targa {} and Country {} on new Richiesta....", targa, nazione);
          newRichiesta.setAssociazione(targa);
          newRichiesta.setCountry(nazione);
        }
      } else {
        log.warn("NewTargaNazione {} does not contain {}", newTargaNazione, SEPARATOR_TARGA_NAZIONE);
      }
    } else {
      newRichiesta.setAssociazione(richiesta.getAssociazione());
      newRichiesta.setCountry(richiesta.getCountry());
    }
  }

  @SuppressWarnings("unused")
  @Override
  public List<Richiesta> generateRichiestaModifica(CarrelloModificaRichiestaDTO carrelloDTO, boolean newOrder) throws Exception {
    StringBuilder sb = new StringBuilder();

    log.debug("Recupero informazioni per generare richiesta di MODIFICA");
    String codiceAzienda = carrelloDTO.getCodiceAzienda();
    List<String> dispositivi = carrelloDTO.getDispositivi();

    TipoDispositivoEnum tipoDispositivo = carrelloDTO.getTipoDispositivo();
    TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
    String uuidDocumento = carrelloDTO.getDocumento();
    String uuidVeicolo = carrelloDTO.getVeicolo();
    String uuidNewVeicolo = carrelloDTO.getNewVeicolo();
    String targa = carrelloDTO.getTarga();
    String nazione = carrelloDTO.getNazione();

    log.info("Start create richiestas for operation " + tipoOperazioneModifica.name());
    List<Richiesta> richieste = new ArrayList<>();
    switch (tipoOperazioneModifica) {
    case FURTO:
    case FURTO_CON_SOSTITUZIONE:
    case SMARRIMENTO:
    case SMARRIMENTO_CON_SOSTITUZIONE:
    case MALFUNZIONAMENTO:
    case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
      richieste = manageDispositivoAndRichiesta(tipoOperazioneModifica, tipoDispositivo, dispositivi, codiceAzienda, uuidDocumento);
      break;

    case MEZZO_RITARGATO:
      List<Richiesta> richiesteNotDeviceViaCard = null;
      TipoDispositivoEnum viaCard = TipoDispositivoEnum.VIACARD;
      List<Richiesta> richiesteCreated = new ArrayList<>();
      Set<Dispositivo> viaCardDevices = dispositivi.stream()
                                                   .map(identificativoDispositivo -> dispositivoServiceExt.findOneByIdentificativo(identificativoDispositivo))
                                                   .filter(d -> d.getTipoDispositivo() != null && d.getTipoDispositivo()
                                                                                                   .getNome() == viaCard)
                                                   .collect(toSet());
      if (viaCardDevices != null && !viaCardDevices.isEmpty()) {
        richieste = createRichiestaVariazioneTargaOrMezzoRitargatoViaCard(uuidVeicolo, null, targa, nazione, codiceAzienda, dispositivi,
                                                                          tipoOperazioneModifica);
      }
      richiesteNotDeviceViaCard = manageDispositivoMezzoRitargatoAndContrattoRichiesta(uuidVeicolo, targa, nazione, tipoOperazioneModifica,
                                                                                       codiceAzienda, uuidDocumento, viaCard);

      if (richieste == null) {
        richieste = new ArrayList<>();
      }
      if (richiesteNotDeviceViaCard != null) {
        richieste.addAll(richiesteNotDeviceViaCard);
      }

      sendMessageToChangeTargaNazione(uuidVeicolo, targa, nazione);

      break;
    case VARIAZIONE_TARGA:
      // Esiste solo per singolo dispositivo;
      if (tipoDispositivo.name()
                         .equals(TipoDispositivoEnum.VIACARD.name())) {
        // loggare l'azione di variazione targa da vecchio veicolo a nuovo veicolo;
        log.info("VARIAZIONE TARGA: Vecchio veicolo [UUID: {}] to Nuovo Veicolo [UUID: {}]", uuidVeicolo, uuidNewVeicolo);
        // 1 - ricercare tutte le associazione DV e modificare l'identificativo del veicolo con il nuovo.

        richieste = createRichiestaVariazioneTargaOrMezzoRitargatoViaCard(uuidVeicolo, uuidNewVeicolo, null, null, codiceAzienda,
                                                                          dispositivi, tipoOperazioneModifica);
      } else {
        richieste = manageDispositivoVariazioneTargaAndContrattoRichiesta(uuidVeicolo, uuidNewVeicolo, tipoOperazioneModifica,
                                                                          codiceAzienda, uuidDocumento, dispositivi);

        richieste = richieste.stream()
                             .map(r -> assignNewTargaNazione(r, targa, nazione))
                             .collect(toList());
      }
      break;

    default:
      log.error("Operation type is not supported: " + tipoOperazioneModifica);
      break;
    }

    if (richieste == null) {
      return null;
    }

    // Serve per gestire gli ordini di rientro dovuti ad un cambiamento dei servizi attivi sui dispositivi;
    if (newOrder) {
      OrdineCliente ordineCliente = initOrdineClienteToRichiesta(codiceAzienda);
      ordineCliente.setRichiedente(carrelloDTO.getRichiedente());
      ordineCliente.setTipo(TipoOrdineCliente.VARIAZIONE);
      log.info("Set defualt value to NULL for spesa prevista");
      ordineCliente.setPrevisioneSpesaMensile(null);
      String richiedente = carrelloDTO.getRichiedente();
      log.debug("Richiedente operazione di modifica {}", richiedente);
      ordineCliente.setRichiedente(richiedente);
      if (StringUtils.isNoneBlank(uuidNewVeicolo)) {
        log.info("Set UUID new Vehicle {}", uuidNewVeicolo);
        ordineCliente.setUuidVeicolo(uuidNewVeicolo);
      }
      ordineCliente = ordineClienteServiceExt.save(ordineCliente);
      ordineCliente = ordineClienteServiceExt.updateNumeroOrdine(ordineCliente);
      log.info("Created OrdineCliente [ID: {} - Identificativo: {} - NumeroOrdine: {}] di modifica: {}", ordineCliente.getId(),
               ordineCliente.getIdentificativo(), ordineCliente.getNumeroOrdine(), tipoOperazioneModifica);
      for (Richiesta richiesta : richieste) {
        richiesta.data(Instant.now());
        richiesta.setOrdineCliente(ordineCliente);
        TipoDispositivo tipoDispositivoRichiesta = richiesta.getTipoDispositivo();
        Optional<Contratto> contrattoOpt = getContrattoForRichiesta(tipoDispositivoRichiesta, codiceAzienda);
        if (contrattoOpt.isPresent()) {
          Contratto contratto = contrattoOpt.get();
          richiesta.setContratto(contratto);
        } else {
          log.warn("Not found Contratto for produttore {} and codice azienda {}", tipoDispositivoRichiesta.getProduttore(), codiceAzienda);
        }
        richiesta = save(richiesta);
        log.info("Created richiesta [ID: {} - Identificativo: {}]", richiesta.getId(), richiesta.getIdentificativo());
      }

      ordineCliente = ordineClienteServiceExt.save(ordineCliente);
    }
    return richieste;
  }

  private List<Richiesta> createRichiestaVariazioneTargaOrMezzoRitargatoViaCard(String uuidVeicolo, String uuidNewVeicolo, String targa,
                                                                                String nazione, String codiceAzienda,
                                                                                List<String> dispositivi,
                                                                                TipoRichiesta tipoRichiesta) throws Exception {

    List<Richiesta> richieste = new ArrayList<>();
    Richiesta r = new Richiesta().data(Instant.now())
                                 .stato(StatoRichiesta.EVASO_CON_RICONSEGNA)
                                 .tipo(tipoRichiesta);

    if (StringUtils.isNotBlank(targa) && StringUtils.isNotBlank(nazione)) {
      r.setAssociazione(targa);
      r.setCountry(nazione);
    }

    List<AssociazioneDV> assDvs = assDispVeicoloRepository.findByUuidVeicoloAndCodiceClienteFai(uuidVeicolo, codiceAzienda);

    Optional<AssociazioneDV> associazioneDvOpt = assDvs.stream()
                                                       .filter(asdv -> asdv.getDispositivo()
                                                                           .getStato()
                                                                           .isGreen())
                                                       .filter(asdv -> asdv.getDispositivo()
                                                                           .getTipoDispositivo()
                                                                           .getNome() == TipoDispositivoEnum.VIACARD)
                                                       .findFirst();
    if (!associazioneDvOpt.isPresent()) {
      throw new Exception("Not found associazione Dispositivo veicolo per il veicolo " + uuidVeicolo + " con Tipo Dispositivo VIACARD");
    }
    AssociazioneDV associazioneDV = associazioneDvOpt.get();
    log.info("Found associazione DV: {}", associazioneDV);
    if (StringUtils.isNotBlank(uuidNewVeicolo)) {
      log.info("Set new identificativo veicolo: {} on Associazione DV: {}", uuidNewVeicolo, associazioneDV);
      associazioneDV.setUuidVeicolo(uuidNewVeicolo);
      associazioneDV.setData(Instant.now());
      associazioneDV = assDispVeicoloRepository.save(associazioneDV);
      log.info("Saved Associazione DV: {}", associazioneDV);
    }
    Dispositivo dispositivo = associazioneDV.getDispositivo();
    TipoDispositivo tipoDispositivo = dispositivo.getTipoDispositivo();
    log.info("Tipo dispositivo: {}", tipoDispositivo);

    r.setTipoDispositivo(tipoDispositivo);

    Set<Dispositivo> dispositivos = new HashSet<>();
    dispositivos.add(dispositivo);
    r.setDispositivos(dispositivos);

    richieste.add(r);
    return richieste;
  }

  private Optional<Contratto> getContrattoForRichiesta(TipoDispositivo tipoDispositivo, String codiceAzienda) {
    log.debug("Search contratto by Produttore {} and Codice Azienda {}", tipoDispositivo.getProduttore(), codiceAzienda);
    return contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(tipoDispositivo.getProduttore(), codiceAzienda);
  }

  @Override
  public List<Long> executeFsmForRichiesteDiModifica(List<Richiesta> richieste) throws FsmExecuteCommandException {
    List<Long> resultSet = new ArrayList<>();
    if (richieste != null) {
      for (Richiesta richiesta : richieste) {

        Long idRichiesta = richiesta.getId();
        String identificativo = richiesta.getIdentificativo();
        TipoRichiesta tipoRichiesta = richiesta.getTipo();
        TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
        TipoDispositivoEnum nomeTipoDispositivo = tipoDispositivo.getNome();

        if ((tipoRichiesta == TipoRichiesta.VARIAZIONE_TARGA || tipoRichiesta == TipoRichiesta.MEZZO_RITARGATO) && tipoDispositivo != null
            && tipoDispositivo.getNome() == TipoDispositivoEnum.VIACARD) {
          log.info("Skip manage Richiesta with FSM: Fsm for {} does not exists for {}...", tipoRichiesta, tipoDispositivo.getNome());
          resultSet.add(idRichiesta);
          continue;
        }

        log.info("Manage Fsm for Richiesta [ID: {} - Identificativo: {} - Tipo: {}] with tipo Dispositivo: {}", idRichiesta, identificativo,
                 tipoRichiesta, nomeTipoDispositivo);
        StatoRichiesta fromState = richiesta.getStato();
        AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
        if (fsm == null) {
          log.warn("Not found FSM for Richiesta {}", richiesta);
          continue;
        }
        richiesta = fsmRichiestaUtils.executeFsmAndPersist(fsm, richiesta, RichiestaEvent.INITIAL);
        log.info("Richiesta [ID: {} - Identificativo: {}] changed status FROM: [{}] TO: [{}] ", idRichiesta, identificativo, fromState,
                 richiesta.getStato());

        resultSet.add(idRichiesta);
      }
    }

    log.debug("Managed {} richieste => Ids: {}", (resultSet != null) ? resultSet.size() : 0, resultSet);
    return resultSet;
  }

  @Override
  @Transactional
  public RichiestaDTO findRichiestaDTOByIdentificativo(String identificativo, boolean fetchEager) {
    Richiesta richiesta = findRichiestaByIdentificativo(identificativo, fetchEager);
    return richiestaMapper.toDto(richiesta);
  }

  @Override
  @Transactional
  public List<OrdineDTO> generateOrdineDTOByRichieste(List<String> identifivativoRichieste) throws Exception {
    Map<String, OrdineDTO> map = new HashMap<>();
    for (String identificativoRichiesta : identifivativoRichieste) {
      log.debug("Identificativo Richiesta: {}", identificativoRichiesta);
      Richiesta richiesta = findRichiestaByIdentificativo(identificativoRichiesta, true);
      log.debug("Richiesta: {}", richiesta);
      OrdineCliente ordineCliente = richiesta.getOrdineCliente();
      String identificativoOrdine = ordineCliente.getIdentificativo();
      log.debug("Identificativo OrdineCliente {}", identificativoOrdine);
      final OrdineDTO ordineDTO = Optional.ofNullable(map.get(identificativoOrdine)).orElse(new OrdineDTO());
      ordineDTO.setId(ordineCliente.getId());
      ordineDTO.setIdentificativo(identificativoOrdine);
      Long contrattoId = null;
      if (richiesta.getContratto() != null) {
        ordineDTO.setContrattoId(richiesta.getContratto()
                                          .getId());
        ordineDTO.setContrattoCodContrattoCliente(richiesta.getContratto()
                                                           .getCodContrattoCliente());
      }
      log.debug("Setting ContrattoId {} on OrdineDTO", contrattoId);

      IndirizzoSpedizioneOrdini indirizzoDiTransito = ordineCliente.getIndirizzoDiTransito();
      log.debug("Indirizzo di transito {} on OrdineCliente", indirizzoDiTransito);
      IndirizzoSpedizioneOrdiniDTO dtoIndirizzoTransito = indirizzoSpedizioniMapper.toDto(indirizzoDiTransito);
      ordineDTO.setIndirizzoDiTransito(dtoIndirizzoTransito);
      log.debug("Setting IndirizzoTransitoDTO {} on OrdineDTO", dtoIndirizzoTransito);

      IndirizzoSpedizioneOrdini indirizzoDestinazioneFinale = ordineCliente.getIndirizzoDestinazioneFinale();
      log.debug("Indirizzo di Destinazione Finale {} on OrdineCliente", indirizzoDestinazioneFinale);
      IndirizzoSpedizioneOrdiniDTO dtoIndirizzoDestFinale = indirizzoSpedizioniMapper.toDto(indirizzoDestinazioneFinale);
      ordineDTO.setIndirizzoDestinazioneFinale(dtoIndirizzoDestFinale);

      log.debug("Setting IndirizzoDestFinaleDTO {} on OrdineDTO", dtoIndirizzoDestFinale);
      ordineDTO.setClienteFai(clienteFaiMapper.toDto(ordineCliente.getClienteAssegnatario()));


      final Set<DispositivoDTO> dispositivos = Optional.ofNullable(ordineDTO.getDispositivos()).orElse(new HashSet<>());

      Optional<Dispositivo> optDispositivo = richiesta.getDispositivos()
                                         .stream()
                                         .findFirst();

      optDispositivo.ifPresent(dispositivo -> {
        DispositivoDTO                   dispositiviDTO            = dispositivoServiceExt.getDispositiviDTO(dispositivo);
        Set<StatoDispositivoServizioDTO> statoDispositivoServizios = dispositiviDTO.getStatoDispositivoServizios();
        for (StatoDispositivoServizioDTO statoDispositivoServizioDTO : statoDispositivoServizios) {
          Long            tipoServizioId  = statoDispositivoServizioDTO.getTipoServizioId();
          TipoServizioDTO tipoServizioDTO = tipoServizioService.findOne(tipoServizioId);
          String          nome            = tipoServizioDTO.getNome();
          statoDispositivoServizioDTO.setTipoServizioNome(nome);
        }
        dispositiviDTO.setStatoDispositivoServizios(statoDispositivoServizios);
        dispositiviDTO.setRequestUuid(identificativoRichiesta);
        dispositivos.add(dispositiviDTO);
        Map<String, String> mapDispositivoRIchiesta = ordineDTO.getMapDispositivoRIchiesta();
        mapDispositivoRIchiesta.put(dispositiviDTO.getIdentificativo(), richiesta.getIdentificativo());
        ordineDTO.setMapDispositivoRIchiesta(mapDispositivoRIchiesta);
      });
      TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
      HashSet<TipoDispositivoDTO> tipiDispositivo = new HashSet<>();
      tipiDispositivo.add(tipoDispositivoServiceMapper.toDto(tipoDispositivo));
      ordineDTO.setTipoDispositivos(tipiDispositivo);
      ordineDTO.setDispositivos(dispositivos);




      log.debug("put ORDINE DTO {} with KEY {}", ordineDTO, identificativoOrdine);
      map.put(identificativoOrdine, ordineDTO);
    }

    Set<OrdineDTO> setOrdiniDTO = map.keySet()
                                     .stream()
                                     .map(key -> map.get(key))
                                     .collect(toSet());
    return new ArrayList<>(setOrdiniDTO);
  }

  @Override
  public void sendMessageForGiustificativoFurtoSmarrimento(RichiestaDTO richiesta) {
    String identificativo = richiesta.getIdentificativo();
    TelepassEuGiustificativoAggiuntoDTO telepassEuDocumentUploadedDTO = new TelepassEuGiustificativoAggiuntoDTO(identificativo);
    jmsSenderService.publishGiustificativoMessage(telepassEuDocumentUploadedDTO);
  }

  @Override
  public List<Richiesta> findByContrattoAndTargaNazioneAndStato(final String _contractCode, final String _licensePlate,
                                                                final String _country, final StatoRichiesta _stato) {
    return richiestaRepositoryExt.findByCodiceContrattoAndTargaAndStato(_contractCode, _licensePlate, _country, _stato);
  }

  private List<Richiesta> createAndPersistRichieste(List<Richiesta> richieste,
                                                    HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) throws Exception {
    List<Richiesta> richiestas = new ArrayList<>();
    for (Richiesta richiesta : richieste) {
      List<DatiAggiuntiviRichiesta> datiAggiuntivi = richiesta.getDatiAggiuntivi();
      Richiesta richiestaSaved = createAndPersistRichiesta(richiesta, mapDispositivoVeicolo);
      List<DatiAggiuntiviRichiesta> datiAggiuntiviSaved = datiAggiungitiviRichiestaService.saveAndFlush(datiAggiuntivi);
      log.info("Persisted dati aggiuntivi: {} to Richiesta: {}", (!datiAggiuntiviSaved.isEmpty()) ? datiAggiuntiviSaved.size() : null,
               richiestaSaved);
      richiestas.add(richiestaSaved);
    }

    return richiestas;
  }

  private Richiesta createAndPersistRichiesta(Richiesta richiesta, HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) throws Exception {
    if (!richiesta.getTipo()
                  .name()
                  .equals(TipoRichiesta.MALFUNZIONAMENTO.name())) {
      dispositivoServiceExt.prePersistRelatedEntity(richiesta, mapDispositivoVeicolo);
    }

    Richiesta richiestaCreated = saveAndFlush(richiesta);
    if (!richiesta.getTipo()
                  .name()
                  .equals(TipoRichiesta.MALFUNZIONAMENTO.name())) {
      dispositivoServiceExt.addRelationDispositivoToRichiesta(richiesta);
    }

    log.debug("PERSISTED Richieste [ID: {} - Identificativo: {} - Tipo: {} - Stato: {} - Data modifica stato: {}]",
              richiestaCreated.getId(), richiestaCreated.getIdentificativo(), richiestaCreated.getTipo(), richiestaCreated.getStato(),
              richiestaCreated.getDataModificaStato());
    return richiestaCreated;
  }

  private void assignContractOnDispositiviAndSave(Set<Dispositivo> dispositivos, Contratto contract) {
    dispositivos.stream()
                .map(device -> {
                  try {
                    return dispositivoServiceExt.save(device.contratto(contract));
                  } catch (Exception e) {
                    log.error("Dispositivo Exception:", e);
                    throw new RuntimeException(e);
                  }
                })
                .collect(toSet());

  }

  private Produttore getProduttoreByRichiesta(Richiesta orderRequest) {
    TipoDispositivo tipoDispositivo = orderRequest.getTipoDispositivo();
    if (tipoDispositivo != null) {
      return tipoDispositivo.getProduttore();
    }
    return null;
  }

  private ClienteFai getClienteFaiByRichiesta(Richiesta richiesta) {
    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    if (ordineCliente != null) {
      return ordineCliente.getClienteAssegnatario();
    }
    return null;
  }

  private List<Richiesta> saveContractDevicesAssociation(List<Richiesta> richieste, Contratto contract) {
    if (richieste == null) {
      return richieste;
    }

    return richieste.stream()
                    .map(r -> {
                      Set<Dispositivo> dispositivos = r.getDispositivos();
                      dispositivoServiceExt.associateContract(dispositivos, contract);
                      log.debug("Set contract {} to Richiesta {}", contract, r);
                      return r.contratto(contract);
                    })
                    .collect(toList());

  }

  private Contratto newDefaultContract(Produttore produttore, ClienteFai clienteFai) {
    String codiceCliente = clienteFai.getCodiceCliente();
    List<Contratto> contracts = contrattoServiceExt.findAllByProduttoreAndCodiceAzienda(produttore, codiceCliente);
    if (contracts != null && !contracts.isEmpty()) {
      contrattoServiceExt.setPrimarioFalse(contracts);
    }

    return contrattoServiceExt.createContrattoByProduttoreAndClienteFai(produttore, clienteFai);
  }

  private Optional<Contratto> checkExistsContractByProduttoreAndClienteFai(Produttore produttore, String codiceAzienda) {
    Optional<Contratto> contractOpt = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(produttore, codiceAzienda);
    return contractOpt;
  }

  private List<Richiesta> createRichiesteAttivazioneDisattivazioneServizio(List<AssociazioneDV> associazioniDv,
                                                                           TipoDispositivo tipoDispositivo, String codiceAzienda,
                                                                           HashMap<TipoRichiesta, Set<TipoServizio>> mapServiziGestibiliDaRemoto,
                                                                           OrdineCliente ordineCliente) throws Exception {

    Set<Dispositivo> dispositivi = dispositivoServiceExt.getDispositiviFilteredByAssociazioneDv(associazioniDv, tipoDispositivo,
                                                                                                codiceAzienda);
    if (dispositivi == null || (dispositivi != null && (dispositivi.isEmpty() || dispositivi.size() > 1))) {
      throw new ValidationException("It's not possible exists more dispositivi for one tipo dispositivo and a vehicle");
    }

    Dispositivo dispositivo = dispositivi.stream()
                                         .findFirst()
                                         .get();
    List<Richiesta> richiesteCreated = null;
    Set<TipoRichiesta> keySet = mapServiziGestibiliDaRemoto.keySet();
    for (TipoRichiesta tipo : keySet) {
      log.debug("Managed servizi to: {}", tipo);
      switch (tipo) {
      case ATTIVAZIONE_SERVIZIO:
      case DISATTIVAZIONE_SERVIZIO:
        Set<TipoServizio> tipiServizi = mapServiziGestibiliDaRemoto.get(tipo);
        List<Richiesta> richiesteGenerated = tipiServizi.stream()
                                                        .map(ts -> {
                                                          try {
                                                            dispositivoServiceExt.setStatoDispositivoServizio(dispositivo, ts, tipo);
                                                            Richiesta r = mapToNewRichiesta(tipo,
                                                                                             StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO,
                                                                                             tipoDispositivo, dispositivo, ordineCliente,
                                                                                             null, ts.getNome());
                                                            log.info("Created Richiesta {} to servizio: {}: {}", tipo, ts.getNome(), r);
                                                            return r;
                                                          } catch (Exception e) {
                                                            throw new RuntimeException(e);
                                                          }
                                                        })
                                                        .collect(toList());

        if (richiesteGenerated != null) {
          if (richiesteCreated == null) {
            richiesteCreated = new ArrayList<>();
          }

          richiesteCreated.addAll(richiesteGenerated);
          log.info("Richieste for servizi {}: {}", tipo, richiesteGenerated.size());
        }
        break;
      default:
        log.error("This operation {} is not permitted....", tipo.name());
        throw new Exception("This operation " + tipo + " is not permitted. Only permitted operations are ["
                            + TipoRichiesta.ATTIVAZIONE_SERVIZIO + " - " + TipoRichiesta.DISATTIVAZIONE_SERVIZIO + "]");
      }
    }

    if (richiesteCreated == null) {
      log.warn("Not created richieste....");
    }
    log.debug("Tot richieste ATTIVAZIONE/DISATTIVAZIONE servizi: {}", (richiesteCreated != null) ? richiesteCreated.size() : 0);
    return richiesteCreated;
  }

  private Richiesta mapToNewRichiesta(TipoRichiesta tipo, StatoRichiesta stato, TipoDispositivo tipoDispositivo, Dispositivo dispositivo,
                                       OrdineCliente ordineCliente, String operazioniPossibili, String nomeServizio) throws Exception {
    Richiesta richiesta = new Richiesta();
    richiesta.setTipo(tipo);
    richiesta.setStato(stato);
    richiesta.setData(Instant.now());
    richiesta.setDataModificaStato(Instant.now());
    richiesta.setOperazioniPossibili(operazioniPossibili);
    richiesta.setNomeServizio(nomeServizio);
    if (StringUtils.isNotBlank(nomeServizio)) {
      log.debug("Add dati aggiuntivi to insert nome servizio: {}", nomeServizio);
      DatiAggiuntiviRichiesta datiAggiuntiviRichiesta = new DatiAggiuntiviRichiesta();
      datiAggiuntiviRichiesta.setChiave(KeyDatiAggiuntivi.NOME_SERVIZIO);
      datiAggiuntiviRichiesta.setValore(nomeServizio);
      ArrayList<DatiAggiuntiviRichiesta> datiAggiuntiviList = new ArrayList<>();
      datiAggiuntiviList.add(datiAggiuntiviRichiesta);
      richiesta.setDatiAggiuntivi(datiAggiuntiviList);
    }
    richiesta.setDispositivos(new HashSet<>(Arrays.asList(dispositivo)));
    richiesta.setTipoDispositivo(tipoDispositivo);
    richiesta.setOrdineCliente(ordineCliente);
    return richiesta;
  }

  private OrdineCliente initAndSaveOrdineClienteByCarrelloDTO(CarrelloDTO carrelloDTO, ClienteFai clienteFai) {
    OrdineCliente ordineCliente = initOrdineClienteDefault();
    ordineCliente = manageDatiAggiuntiviRichiesta(ordineCliente, carrelloDTO.getDatiAggiuntivi());
    ordineCliente.setClienteAssegnatario(clienteFai);
    ordineCliente.setRichiedente(carrelloDTO.getRichiedente());
    ordineCliente.setEnabledDmlMessage(false);
    ordineCliente = ordineClienteServiceExt.save(ordineCliente);
    ordineCliente = ordineClienteServiceExt.updateNumeroOrdine(ordineCliente);
    return ordineCliente;
  }

  private OrdineCliente initOrdineClienteDefault() {
    return new OrdineCliente().stato(StatoOrdineCliente.DA_EVADERE)
                              .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                              .dataCreazione(Instant.now())
                              .dataModificaStato(Instant.now());
  }

  private OrdineCliente initOrdineClienteToRichiesta(String codiceAzienda) throws Exception {
    OrdineCliente ordineCliente = initOrdineClienteDefault();
    ClienteFai clienteFai = clienteFaiRepo.findOneByCodiceCliente(codiceAzienda);
    if (clienteFai == null) {
      throw new Exception("Not found Cliente Fai for codice cliente " + codiceAzienda);
    }
    ordineCliente.setClienteAssegnatario(clienteFai);
    ordineCliente = ordineClienteServiceExt.save(ordineCliente);
    return ordineCliente;
  }

  private List<Richiesta> generateRichiestaDispositiviDaRestituire(Set<Dispositivo> dispositiviDaRestuitire, String codiceAzienda,
                                                                   OrdineCliente ordineCliente) throws Exception {
    CarrelloModificaRichiestaDTO carrelloDTO = new CarrelloModificaRichiestaDTO();
    TipoDispositivoEnum tipologiaDispositivo = dispositiviDaRestuitire.stream()
                                                                      .findFirst()
                                                                      .get()
                                                                      .getTipoDispositivo()
                                                                      .getNome();
    List<String> identificativoDispositivi = dispositiviDaRestuitire.stream()
                                                                    .map(disp -> disp.getIdentificativo())
                                                                    .collect(toList());
    carrelloDTO.setDispositivi(identificativoDispositivi);
    carrelloDTO.setTipoDispositivo(tipologiaDispositivo);
    carrelloDTO.setCodiceAzienda(codiceAzienda);
    carrelloDTO.setTipoOperazioneModifica(TipoRichiesta.MALFUNZIONAMENTO);
    List<Richiesta> richieste = generateRichiestaModifica(carrelloDTO, false);
    log.info("Create RIENTRO/MALFUNZIONAMENTO richiesta {} ", (richieste != null && !richieste.isEmpty()) ? richieste.get(0)
                                                                                                                     .getIdentificativo()
                                                                                                          : null);

    if (richieste != null) {
      richieste = richieste.stream()
                           .map(r -> r.ordineCliente(ordineCliente))
                           .collect(toList());
    } else {
      // TO FIX null pointer;
      richieste = new ArrayList<>();
    }

    return richieste;
  }

  private Map<String, Set<Dispositivo>> generateDispositivi(String uuid, TipoDispositivo tipoDispositivo,
                                                            VeicoloCarrelloDTO veicoloCarrelloDTO,
                                                            HashMap<String, List<Dispositivo>> mapDispositivoVeicolo,
                                                            List<AssociazioneDV> associazioniDv) throws Exception {
    Map<String, Set<Dispositivo>> mapDispositivi = new HashMap<>();
    Set<Dispositivo> dispositivos = null;
    Dispositivo dispositivo = null;

    // Creo nuovo dispositivo e creo una lista di dispositivi da restituire.
    Set<Dispositivo> vehicleHasTipoDispositivo = vehicleHasTipoDispositivo(associazioniDv, tipoDispositivo);
    if (vehicleHasTipoDispositivo != null && !vehicleHasTipoDispositivo.isEmpty()) {
      log.debug("Put dispositivos {} in map with Key {}", vehicleHasTipoDispositivo, DISPOSITIVI_DA_RESTITUIRE);
      mapDispositivi.put(DISPOSITIVI_DA_RESTITUIRE, vehicleHasTipoDispositivo);
    }

    log.debug("Create dispositivo {} for richiesta...", tipoDispositivo.getNome());
    dispositivo = dispositivoServiceExt.createDispositivoServizi(tipoDispositivo, veicoloCarrelloDTO.getNomeTipiServizio());
    if (mapDispositivoVeicolo == null) {
      mapDispositivoVeicolo = new HashMap<>();
    }
    createMapForAssociazioneDV(dispositivo, uuid, mapDispositivoVeicolo);

    if (dispositivo != null) {
      dispositivos = new HashSet<>();
      dispositivos.add(dispositivo);
    }

    log.debug("Put dispositivos {} in map with Key {}", dispositivos, NUOVO_DISPOSITIVO);
    mapDispositivi.put(NUOVO_DISPOSITIVO, dispositivos);
    return mapDispositivi;
  }

  private void createMapForAssociazioneDV(Dispositivo dispositivo, String uuid, HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) {
    List<Dispositivo> list = mapDispositivoVeicolo.get(uuid);
    if (list == null) {
      list = new ArrayList<>();
    }
    list.add(dispositivo);
    log.debug("Put dispositivos: {} in map with Key: {}", list, uuid);
    mapDispositivoVeicolo.put(uuid, list);
  }

  private Set<Dispositivo> vehicleHasTipoDispositivo(List<AssociazioneDV> listAssDispVeicolo, TipoDispositivo tipoDispositivo) {
    Set<Dispositivo> dispositiviFound = new HashSet<>();
    TipoDispositivoEnum nomeTipoDispositivo = tipoDispositivo.getNome();
    if (!isDeviceHGVType(nomeTipoDispositivo)) {
      if (listAssDispVeicolo != null && !listAssDispVeicolo.isEmpty()) {
        dispositiviFound = listAssDispVeicolo.parallelStream()
                                             .map(assDv -> assDv.getDispositivo())
                                             .filter(d -> d.getStato()
                                                           .isGreen())
                                             .filter(d -> d.getTipoDispositivo()
                                                           .getNome()
                                                           .name() == nomeTipoDispositivo.name())
                                             .map(d -> d)
                                             .collect(toSet());
      }
    }

    log.debug("Vehicle has tipoDispositivo {}: {}", nomeTipoDispositivo, dispositiviFound);
    return dispositiviFound;
  }

  private boolean isDeviceHGVType(TipoDispositivoEnum deviceType) {
    if (TipoDispositivoEnum.HGV.equals(deviceType))
      return true;
    return false;
  }

  private List<Richiesta> mapToRichieste(Set<Dispositivo> dispositivos, TipoDispositivo tipoDispositivo, ClienteFai clienteFai,
                                          OrdineCliente ordineCliente, String operazioniPossibili,
                                          List<datiAggiuntiviCarrelloDTO> datiAggiuntiviCarrello) throws Exception {
    // Creo una richiesta per dispositivo
    List<Richiesta> newRichieste = new ArrayList<>();
    for (Dispositivo dispositivo : dispositivos) {

      Richiesta richiesta = mapToNewRichiesta(TipoRichiesta.NUOVO_ORDINE, StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, tipoDispositivo,
                                               dispositivo, ordineCliente, operazioniPossibili, null);
      if (datiAggiuntiviCarrello != null) {
        mapDatiAggiuntiviRichiesta(datiAggiuntiviCarrello, richiesta, dispositivo);
      }
      newRichieste.add(richiesta);
    }

    log.debug("Generated {} richieste to {} dispositivi", newRichieste.size(), dispositivos.size());
    return newRichieste;
  }

  private void mapDatiAggiuntiviRichiesta(List<datiAggiuntiviCarrelloDTO> datiAggiuntiviCarrello, Richiesta richiesta,
                                             Dispositivo dispositivo) throws IllegalAccessException {
    if (datiAggiuntiviCarrello == null)
      return;

    if (datiAggiuntiviCarrello.size() > 1) {
      throw new IllegalArgumentException("DatiAggiuntiviCarrelloDTO is more then one. This is not possible, because DatiAggiuntivi is a map retated by vehicle uuid.");
    }

    datiAggiuntiviCarrelloDTO dto = datiAggiuntiviCarrello.get(0);
    log.info("DatiAggiuntivi by UUID vehicle: {}: {}", dto.getIdentificativo(), dto.getDatiAggiuntivi());
    HashMap<String, Object> datiAggiuntivi = dto.getDatiAggiuntivi();
    Set<String> keySet = datiAggiuntivi.keySet();
    for (String key : keySet) {
      DatiAggiuntiviRichiesta datiAggiuntiviRichiesta = new DatiAggiuntiviRichiesta();
      try {
        KeyDatiAggiuntivi keyDatiAggiuntivi = KeyDatiAggiuntivi.valueOf(key);
        datiAggiuntiviRichiesta.setChiave(keyDatiAggiuntivi);
      } catch (Exception e) {
        throw new IllegalAccessException("Not parse key: " + key + " to " + KeyDatiAggiuntivi.class + ": " + e);
      }
      Object object = datiAggiuntivi.get(key);
      try {
        datiAggiuntiviRichiesta.setValore(object.toString());
      } catch (Exception e) {
        datiAggiuntiviRichiesta.setValore("");
      }
      datiAggiuntiviRichiesta.setType(TypeClassValue.getTypeClass(object));
      datiAggiuntiviRichiesta.setRichiesta(richiesta);
      richiesta.addDatiAggiuntivi(datiAggiuntiviRichiesta);
    }
  }

  private OrdineCliente manageDatiAggiuntiviRichiesta(OrdineCliente ordineCliente, HashMap<String, String> datiAggiuntivi) {
    ordineCliente.setPrevisioneSpesaMensile(0);
    if (datiAggiuntivi != null) {
      String costo = datiAggiuntivi.get("costo");
      if (StringUtils.isNotBlank(costo)) {
        ordineCliente.setPrevisioneSpesaMensile(Integer.parseInt(costo));
      } else {
        log.warn("Costo per i dati aggiuntivi is: {} - Set default value => 0", costo);
      }
    } else {
      log.warn("Non sono stati inseriti i dati aggiuntivi- Set default value => 0");
    }

    return ordineCliente;
  }

  private List<Richiesta> manageDispositivoAndRichiesta(TipoRichiesta tipoOperazioneModifica, TipoDispositivoEnum tipoDispositivo,
                                                        List<String> dispositivi, String codiceAzienda,
                                                        String uuidDocumento) throws Exception {

    Richiesta richiesta = new Richiesta();
    richiesta.setTipo(tipoOperazioneModifica);
    richiesta.setData(Instant.now());
    richiesta.setDataModificaStato(Instant.now());
    richiesta.setDispositivos(new HashSet<Dispositivo>());
    setClienteFaiToRichiestaByCodice(richiesta, codiceAzienda);
    if (StringUtils.isNoneBlank(uuidDocumento)) {
      richiesta.setUuidDocumento(uuidDocumento);
    }

    log.info("Manage Dispositivo of type: " + tipoDispositivo.name());
    if (dispositivi != null && !dispositivi.isEmpty()) {
      if (dispositivi.size() > 1) {
        log.debug("Api use only one dispositivo to manage richiesta for modify: " + tipoOperazioneModifica);
      }

      String identificativoDispositivo = dispositivi.get(0);
      log.info("Manage dispositivo with identificativo: " + identificativoDispositivo);
      Dispositivo dispositivo = dispositivoServiceExt.findOneByIdentificativo(identificativoDispositivo);
      if (dispositivo == null) {
        throw new Exception("Not found Dispositivo with identificativo: " + identificativoDispositivo);
      }

      StatoDispositivo statoDispositivo = dispositivo.getStato();
      if (statoDispositivo.isYellow()) {
        throw new RuntimeException("Stato Dispositivo [" + identificativoDispositivo + " is: " + statoDispositivo
                                   + " (YELLOW STATE), so not valid for operation");
      } else if (statoDispositivo.isBlue()) {
        log.warn("Skip generation Richiesta, because device is disable");
      } else {
        TipoDispositivo tipoDisp = dispositivo.getTipoDispositivo();
        if (tipoDisp.getNome() == null || (tipoDisp.getNome() != null && tipoDisp.getNome()
                                                                                 .name() != tipoDispositivo.name())) {
          log.error("Il tipo dispositivo {} non corrisponde al tipo dispositivo [ID: {} - Nome: {}]", tipoDispositivo.name(),
                    tipoDisp.getId(), tipoDisp.getNome());
          throw new Exception("Il tipo dispositivo non corrisponde a " + tipoDispositivo.name());
        }
        setStatoRichiestaByTipoDispositivo(tipoDispositivo, richiesta);
        richiesta.setTipoDispositivo(tipoDisp);

        Contratto contratto = dispositivo.getContratto();
        log.info("Contratto {} to set on Richiesta by Dispositivo {}", contratto, dispositivo);
        richiesta.setContratto(contratto);

        richiesta.addDispositivo(dispositivo);
        dispositivo.addRichiesta(richiesta);
        dispositivoServiceExt.save(dispositivo);

        List<Richiesta> richieste = new ArrayList<>();
        richieste.add(richiesta);
        return richieste;
      }
    }

    return null;
  }

  private List<Richiesta> manageDispositivoMezzoRitargatoAndContrattoRichiesta(String uuidVeicolo, String targa, String nazione,
                                                                               TipoRichiesta tipoOperazioneModifica, String codiceAzienda,
                                                                               String uuidDocumento,
                                                                               TipoDispositivoEnum tipoDispositivo) throws Exception {
    // MEZZO RITARGATO -> Messaggio Jms con la targa da modificare su un determinato uuid veicolo con la rispettiva
    // nazione;
    List<Richiesta> richieste = manageDispositiviByUuidVeicolo(uuidVeicolo, tipoOperazioneModifica, codiceAzienda, uuidDocumento, null,
                                                               tipoDispositivo);

    if (richieste != null) {
      richieste = richieste.stream()
                           .map(r -> assignNewTargaNazione(r, targa, nazione))
                           .collect(toList());

      sendMessageToChangeTargaNazione(uuidVeicolo, targa, nazione);
    }

    return richieste;
  }

  private void sendMessageToChangeTargaNazione(String uuidVeicolo, String targa, String nazione) throws Exception {
    // Send jms message to update targa on vehicle service;
    MezzoRitargatoDTO mezzoRitargatoDTO = new MezzoRitargatoDTO(uuidVeicolo, targa, nazione);
    jmsSenderService.publishMezzoRitargatoMessage(mezzoRitargatoDTO);
  }

  private Richiesta assignNewTargaNazione(Richiesta richiesta, String targa, String nazione) {
    log.info("Assign new Targa: {} - Nazione: {} on Richiesta {}", targa, nazione, richiesta);
    String targaNazione = targa + SEPARATOR_TARGA_NAZIONE + nazione;
    return richiesta.newTargaNazione(targaNazione);
  }

  private List<Richiesta> manageDispositivoVariazioneTargaAndContrattoRichiesta(String uuidVeicolo, String uuidNewVeicolo,
                                                                                TipoRichiesta tipoOperazioneModifica, String codiceAzienda,
                                                                                String uuidDocumento,
                                                                                List<String> dispositivi) throws Exception {
    // VARIAZIONE TARGA -> UUID veicolo per la nuova richiesta;
    List<Richiesta> richieste = manageDispositiviByUuidVeicolo(uuidVeicolo, tipoOperazioneModifica, codiceAzienda, uuidDocumento,
                                                               dispositivi, null);
    return richieste;
  }

  private List<Richiesta> manageDispositiviByUuidVeicolo(String uuidVeicolo, TipoRichiesta tipoOperazioneModifica, String codiceAzienda,
                                                         String uuidDocumento, List<String> dispositivi,
                                                         TipoDispositivoEnum tipoDispositivoToExclude) throws Exception {
    log.info("Manage dispositivi by uuidVeicolo: {} for operazione {} to codice cliente {}", uuidVeicolo, tipoOperazioneModifica,
             codiceAzienda);
    List<Richiesta> richieste = null;
    HashMap<TipoDispositivoEnum, List<Dispositivo>> mapDispositivo = new HashMap<>();
    List<AssociazioneDV> assDvs = assDispVeicoloRepository.findByUuidVeicoloAndCodiceClienteFai(uuidVeicolo, codiceAzienda);
    log.debug("Found associazione dispositivo veicolo for vehicle {} and CodiceCliente {} : {}", uuidVeicolo, codiceAzienda, assDvs.size());

    assDvs = excludeAssociazioneDvByTipoDispositivo(assDvs, tipoDispositivoToExclude);

    // creo la mappa dei dispositivi divisa per tipologia dispositivo;
    createMapDispositivi(assDvs, mapDispositivo, dispositivi);

    Set<TipoDispositivoEnum> keySet = mapDispositivo.keySet();
    log.debug("Manage {} tipi di dispositivo: {}", keySet.size(), keySet);
    for (TipoDispositivoEnum tipologiaDispositivo : keySet) {
      // Creo una nuova istanza di Richiesta per ogni tipo di dispositivo (chiave della mappa);

      // recupero la lista dei dispositivi tramite la chiave;
      // La lista dei dispositivi non dovrebbe mai essere null o vuota;
      List<Dispositivo> list = mapDispositivo.get(tipologiaDispositivo);
      for (Dispositivo dispositivo : list) {
        log.info("Create Richiesta for dispositivo {}", dispositivo);

        StatoDispositivo stato = dispositivo.getStato();
        if (stato.isYellow()) {
          throw new RuntimeException("Stato dispositivo is " + stato + " (YELLOW STATE) that not managed modified operations");
        } else if (stato.isBlue()) {
          log.warn("Skip generation richiesta because stato dispositivo is {} (BLUE STATE)", stato);
          continue;
        }

        Richiesta richiesta = new Richiesta();
        richiesta.setTipo(tipoOperazioneModifica);
        richiesta.setData(Instant.now());
        richiesta.setDispositivos(new HashSet<Dispositivo>());
        if (StringUtils.isNoneBlank(uuidDocumento)) {
          log.info("Set UUID Documento {} on Richiesta", uuidDocumento);
          richiesta.setUuidDocumento(uuidDocumento);
        }

        // Aggancio il dispositivo alla richiesta
        richiesta.addDispositivo(dispositivo);
        Set<Richiesta> richiestas = dispositivo.getRichiestas();
        if (richiestas == null) {
          richiestas = new HashSet<>();
        }
        richiestas.add(richiesta);
        dispositivo.setRichiestas(richiestas);
        Contratto contratto = dispositivo.getContratto();
        log.info("Contratto {} to set on Richiesta by Dispositivo {}", contratto, dispositivo);
        richiesta.setContratto(contratto);

        setStatoRichiestaByTipoDispositivo(tipologiaDispositivo, richiesta);
        TipoDispositivo tipoDispositivo = dispositivo.getTipoDispositivo();
        log.info("Setting tipoDispositivo {} on new Richiesta di Variazione....", tipoDispositivo);
        richiesta.setTipoDispositivo(tipoDispositivo);
        richiesta.addDispositivo(dispositivo);
        dispositivo.addRichiesta(richiesta);
        dispositivoServiceExt.save(dispositivo);

        if (richieste == null) {
          richieste = new ArrayList<>();
        }
        log.info("Created richiesta {} for tipo dispositivo {}", richiesta, tipologiaDispositivo.name());
        richieste.add(richiesta);
      }
    }

    return richieste;
  }

  private List<AssociazioneDV> excludeAssociazioneDvByTipoDispositivo(List<AssociazioneDV> assDvs,
                                                                      TipoDispositivoEnum tipoDispositivoToExclude) {
    if (tipoDispositivoToExclude == null) {
      return assDvs;
    }

    if (assDvs == null || assDvs.isEmpty()) {
      return assDvs;
    }

    assDvs = assDvs.stream()
                   .filter(assDv -> assDv.getDispositivo() != null && assDv.getDispositivo()
                                                                           .getTipoDispositivo() != null
                                    && assDv.getDispositivo()
                                            .getTipoDispositivo()
                                            .getNome() != tipoDispositivoToExclude)
                   .collect(toList());
    return assDvs;
  }

  private void createMapDispositivi(List<AssociazioneDV> assDvs, HashMap<TipoDispositivoEnum, List<Dispositivo>> mapDispositivo,
                                    List<String> dispositivi) {
    log.info("Create Dispositivi Map by Tipo Dispositivo Enum key");
    for (AssociazioneDV assdv : assDvs) {
      Dispositivo dispositivo = assdv.getDispositivo();
      String identificativo = dispositivo.getIdentificativo();
      if (dispositivi == null || (dispositivi != null && dispositivi.contains(identificativo))) {
        TipoDispositivo tipoDispositivo = dispositivo.getTipoDispositivo();
        TipoDispositivoEnum nomeTipoDispositivo = tipoDispositivo.getNome();

        List<Dispositivo> list = mapDispositivo.get(nomeTipoDispositivo);
        boolean mapContainsKey = mapDispositivo.containsKey(nomeTipoDispositivo);
        log.debug("Map dispositivo contains key " + nomeTipoDispositivo + ": " + mapContainsKey);
        log.debug("list dispositivi in map is null: " + ((list == null) ? true : false));
        if (!mapContainsKey || list == null) {
          log.debug("Init list dispositivi for map...");
          list = new ArrayList<>();
        }

        log.debug("Add dispositivo [Identifivativo: {}] on map for key: {}", identificativo, nomeTipoDispositivo);
        list.add(dispositivo);
        mapDispositivo.put(nomeTipoDispositivo, list);
      } else {
        log.info("Il dispositivo [Identificativo: {}] non è contenuto in questa lista di identificativi di dispositivi {}", identificativo,
                 dispositivi);
      }
    }
  }

  private void setClienteFaiToRichiestaByCodice(Richiesta richiesta, String codiceAzienda) throws Exception {
    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    if (ordineCliente == null) {
      ordineCliente = initOrdineClienteDefault();
      richiesta.setOrdineCliente(ordineCliente);
    }
    if (ordineCliente != null && ordineCliente.getClienteAssegnatario() == null) {
      // Find cliente fai by codiceAzienda;
      ClienteFai clienteFai = clienteFaiRepo.findOneByCodiceCliente(codiceAzienda);
      if (clienteFai == null) {
        throw new Exception("Not found Cliente Fai for codice Azienda/Cliente: " + codiceAzienda);
      }

      log.info("Found Cliente Fai: {}", clienteFai);
      richiesta.getOrdineCliente()
               .setClienteAssegnatario(clienteFai);
    } else {
      log.debug("Cliente Fai associato alla richiesta [{}] : {}", richiesta, ordineCliente.getClienteAssegnatario());
    }
  }

  private void setStatoRichiestaByTipoDispositivo(TipoDispositivoEnum tipoDispositivo, Richiesta richiesta) {
    StatoRichiesta stato = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TI;
      break;
    case VIACARD:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_VC;
      break;
    case TRACKYCARD:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY;
      break;
    case GO_BOX:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_GOBOX;
      break;
    case TOLL_COLLECT:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS;
      break;
    case TESSERA_CARONTE:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_TESSERA_CARONTE;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      stato = StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO;
      break;
    // TODO add dispositivi da gestire;
    default:
      // Stato di default;
      stato = StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE;
      log.warn("Per la richiesta [ Identificativo: {} ] è stato impostato lo stato {} per default, in quanto non rientra nell'attuale gestione.",
               richiesta.getIdentificativo(), stato.name());
      break;
    }

    log.info("Set stato richiesta to : {}", stato);
    richiesta.setStato(stato);
  }

  @Override
  public Richiesta findRichiestaByIdentificativo(String identificativo, boolean fetchEager) {
    Richiesta richiesta = null;
    if (fetchEager) {
      richiesta = richiestaRepositoryExt.findOneByIdentificativoWithEagerRelationships(identificativo);
    } else {
      richiesta = richiestaRepositoryExt.findOneByIdentificativo(identificativo);
    }
    return richiesta;
  }

  @Override
  public List<Richiesta> findByCodiceId(Long id) {
    return richiestaRepositoryExt.findByContrattoId(id);
  }

  @Override
  public void delete(Richiesta richiesta) {
    Long richiestaId = richiesta.getId();
    richiestaRepositoryExt.delete(richiestaId);
    log.info("Delete Richiesta [ID: {} - Identificativo: {}]", richiestaId, richiesta.getIdentificativo());
  }

  // https://exagespa.atlassian.net/browse/FAINP-2870
  // @Override
  // public RichiestaDTO attivaContratto(String uuid) {
  // log.debug("Request to active contract in request with uuid : {}", uuid);
  // Richiesta richiesta = richiestaRepositoryExt.findOneByIdentificativo(uuid);
  // if (richiesta != null) {
  // if (richiesta.getStato().equals(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
  // || richiesta.getStato().equals(StatoRichiesta.ANNULLATO_UTENTE)) {
  // richiesta.setStato(StatoRichiesta.ATTIVO);
  // richiesta = richiestaRepositoryExt.save(richiesta);
  // }
  // return richiestaMapper.toDto(richiesta);
  // } else {
  // log.warn("Unable to active contract : request with uuid '{}' not found", uuid);
  // return null;
  // }
  // }

}
