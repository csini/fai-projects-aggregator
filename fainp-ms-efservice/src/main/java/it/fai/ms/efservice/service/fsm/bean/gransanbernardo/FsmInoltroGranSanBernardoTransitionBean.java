package it.fai.ms.efservice.service.fsm.bean.gransanbernardo;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmInoltroGranSanBernardo;

@WithStateMachine(id = FsmInoltroGranSanBernardo.FSM_INOLTRO_GRAN_SAN_BERNARDO)
public class FsmInoltroGranSanBernardoTransitionBean extends AbstractFsmRichiestaTransition {
}
