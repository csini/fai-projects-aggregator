package it.fai.ms.efservice.service.fsm.config.caronte;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.config.FsmModificaSimpleConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTesseraCaronteConfig.MODIFICA_TESSERA_CARONTE_CONFIG)
public class FsmModificaTesseraCaronteConfig extends FsmModificaSimpleConfig {

  public static final String MODIFICA_TESSERA_CARONTE_CONFIG = "modificaTesseraCaronte";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaTesseraCaronteConfig(FsmSenderToQueue fsmSenderToQueue) {
    super(fsmSenderToQueue);
  }
  
  @Override
  protected String getMachineId() {
    return FsmType.MODIFICA_TESSERA_CARONTE.fsmName();
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaTesseraCaronte();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TESSERA_CARONTE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
