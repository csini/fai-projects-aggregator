package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardDeviceId implements Serializable {

  private static final long serialVersionUID = 5040470301373978667L;

  private String id;

  public WizardDeviceId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardDeviceId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardDeviceId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
