package it.fai.ms.efservice.service.fsm.config;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroToll2GoConfig.INOLTRO_TOLL2GO)
public class FsmInoltroToll2GoConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_TOLL2GO = "inoltroToll2Go";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue fsmSenderToQueue;

  public FsmInoltroToll2GoConfig(final FsmSenderToQueue _fsmSenderToQueue) {
    fsmSenderToQueue = _fsmSenderToQueue;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_TOLL2GO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroToll2Go();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INOLTRO_SERVIZI)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ACCETTATO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .event(RichiestaEvent.MU_INOLTRATO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.SPEDITO_DAL_FORNITORE));
  }

}
