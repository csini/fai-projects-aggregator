package it.fai.ms.efservice.wizard.model;

public enum ActivabilityState {

  ACTIVE, ACTIVABLE, ACTIVABLE_ALL, NOT_ACTIVABLE, ONGOING_REQUEST, ANOMALY, ACTIVE_ON_ALL, VEHICLE_ANOMALY;

  public boolean isKo() {
    return this.equals(NOT_ACTIVABLE) || this.equals(ANOMALY) || this.equals(VEHICLE_ANOMALY);
  }
}
