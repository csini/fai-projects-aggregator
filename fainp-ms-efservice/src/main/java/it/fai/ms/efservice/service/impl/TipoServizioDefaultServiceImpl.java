package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.TipoServizioDefaultService;
import it.fai.ms.efservice.domain.TipoServizioDefault;
import it.fai.ms.efservice.repository.TipoServizioDefaultRepository;
import it.fai.ms.efservice.service.dto.TipoServizioDefaultDTO;
import it.fai.ms.efservice.service.mapper.TipoServizioDefaultMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TipoServizioDefault.
 */
@Service
@Transactional
public class TipoServizioDefaultServiceImpl implements TipoServizioDefaultService{

    private final Logger log = LoggerFactory.getLogger(TipoServizioDefaultServiceImpl.class);

    private final TipoServizioDefaultRepository tipoServizioDefaultRepository;

    private final TipoServizioDefaultMapper tipoServizioDefaultMapper;

    public TipoServizioDefaultServiceImpl(TipoServizioDefaultRepository tipoServizioDefaultRepository, TipoServizioDefaultMapper tipoServizioDefaultMapper) {
        this.tipoServizioDefaultRepository = tipoServizioDefaultRepository;
        this.tipoServizioDefaultMapper = tipoServizioDefaultMapper;
    }

    /**
     * Save a tipoServizioDefault.
     *
     * @param tipoServizioDefaultDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TipoServizioDefaultDTO save(TipoServizioDefaultDTO tipoServizioDefaultDTO) {
        log.debug("Request to save TipoServizioDefault : {}", tipoServizioDefaultDTO);
        TipoServizioDefault tipoServizioDefault = tipoServizioDefaultMapper.toEntity(tipoServizioDefaultDTO);
        tipoServizioDefault = tipoServizioDefaultRepository.save(tipoServizioDefault);
        return tipoServizioDefaultMapper.toDto(tipoServizioDefault);
    }

    /**
     *  Get all the tipoServizioDefaults.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TipoServizioDefaultDTO> findAll() {
        log.debug("Request to get all TipoServizioDefaults");
        return tipoServizioDefaultRepository.findAll().stream()
            .map(tipoServizioDefaultMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one tipoServizioDefault by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TipoServizioDefaultDTO findOne(Long id) {
        log.debug("Request to get TipoServizioDefault : {}", id);
        TipoServizioDefault tipoServizioDefault = tipoServizioDefaultRepository.findOne(id);
        return tipoServizioDefaultMapper.toDto(tipoServizioDefault);
    }

    /**
     *  Delete the  tipoServizioDefault by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoServizioDefault : {}", id);
        tipoServizioDefaultRepository.delete(id);
    }
}
