package it.fai.ms.efservice.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.SpedizioneClienteServiceExt;
import it.fai.ms.efservice.service.SpedizioneDispositiviClienteServiceExt;
import it.fai.ms.efservice.service.dto.SearchSpedizioneClienteDTO;
import it.fai.ms.efservice.service.dto.SearchSpedizioneDispositiviClienteDTO;
import it.fai.ms.efservice.service.dto.SpedizioneClienteDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneClienteViewDTO;
import it.fai.ms.efservice.service.dto.SpedizioneDispositiviClienteViewDTO;
import it.fai.ms.efservice.web.rest.util.ResponsePage;

@RestController
@RequestMapping(SearchSpedizioneClienteResourceExt.BASE_URL)
public class SearchSpedizioneClienteResourceExt {
  static final String         BASE_URL                      = "/api";
  private static final String SEARCH_SPEDIZIONE_CLIENTE     = "/_search/spedizioneCliente";
  private static final String SEARCH_SPEDIZIONE_DISPOSITIVI = "/_search/spedizioneClienteDispositivi";

  private static final String SEARCH_SPEDIZIONE_DISPOSITIVI_NO_RICHIESTA = "/_search/spedizioneDispositiviNoRichiesta";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final SpedizioneClienteServiceExt            serviceExt;
  private final SpedizioneDispositiviClienteServiceExt serviceDeviceClient;
  private final DispositivoServiceExt                  dispositivoServiceExt;

  public SearchSpedizioneClienteResourceExt(SpedizioneClienteServiceExt serviceExt, DispositivoServiceExt dispositivoServiceExt,
                                            final SpedizioneDispositiviClienteServiceExt serviceDeviceClient) {
    this.serviceExt = serviceExt;
    this.dispositivoServiceExt = dispositivoServiceExt;
    this.serviceDeviceClient = serviceDeviceClient;
  }

  @PostMapping(SEARCH_SPEDIZIONE_CLIENTE)
  @Timed
  public ResponseEntity<ResponsePage> searchSpedizioneCliente(@RequestBody(required = false) SearchSpedizioneClienteDTO dto,
                                                              Pageable pageable) {
    log.debug("REST request to search searchSpedizioneCliente : {}", dto);
    if (dto == null) {
      dto = new SearchSpedizioneClienteDTO();
    }
    int pageNumber = 0;
    if (pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<SpedizioneClienteViewDTO> page = serviceExt.findAllWithFilter(dto, pageRequest);

    return ResponseEntity.ok(new ResponsePage<>(page));
  }

  @PostMapping(SEARCH_SPEDIZIONE_DISPOSITIVI)
  @Timed
  public ResponseEntity<ResponsePage> searchSpedizioneClienteDispositivi(@RequestBody(required = false) SearchSpedizioneClienteDTO dto,
                                                                         Pageable pageable) {
    log.debug("REST request to search spedizioneClienteDispositivi : {}", dto);
    if (dto == null) {
      dto = new SearchSpedizioneClienteDTO();
    }
    int pageNumber = 0;
    if (pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<SpedizioneClienteDispositivoDTO> page = dispositivoServiceExt.getDevicesByCodiceAzienda(dto.getCerca(), // codiceAzienda
                                                                                                 pageRequest);

    return ResponseEntity.ok(new ResponsePage<>(page));
  }

  @PostMapping(SEARCH_SPEDIZIONE_DISPOSITIVI_NO_RICHIESTA)
  @Timed
  public ResponseEntity<ResponsePage> searchSpedizioneDispositiviClienteNoRichiesta(@RequestBody(
                                                                                                 required = false) SearchSpedizioneDispositiviClienteDTO dto,
                                                                                    Pageable pageable) {
    log.debug("REST request to search spedizioneClienteDispositivi : {}", dto);
    if (dto == null) {
      dto = new SearchSpedizioneDispositiviClienteDTO();
    }
    int pageNumber = 0;
    if (pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<SpedizioneDispositiviClienteViewDTO> page = serviceDeviceClient.findAllWithFilter(dto, pageRequest);

    return ResponseEntity.ok(new ResponsePage<>(page));
  }
}
