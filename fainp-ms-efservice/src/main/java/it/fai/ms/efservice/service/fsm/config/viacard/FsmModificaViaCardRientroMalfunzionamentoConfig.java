package it.fai.ms.efservice.service.fsm.config.viacard;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardSendDeviceReturnMessage;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaViaCardRientroMalfunzionamentoConfig.MOD_VIACARD_RIENTRO_MALF)
public class FsmModificaViaCardRientroMalfunzionamentoConfig extends FsmRichiestaConfig {

  public static final String MOD_VIACARD_RIENTRO_MALF = "ModViaCardRientroMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmUtil;

  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  public FsmModificaViaCardRientroMalfunzionamentoConfig(final FsmSenderToQueue _senderFsmUtil,
                                                         final DeviceReturnMessageProducerService _deviceReturnMessageService) {
    senderFsmUtil = _senderFsmUtil;
    deviceReturnMessageService = _deviceReturnMessageService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_VC_RIENTRO_MALFUNZIONAMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_VC;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfViaCardRientroMalfunzionamento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_VC)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO)
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MALFUNZIONAMENTO)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.ANNULLATO_UTENTE)
               .event(RichiestaEvent.MU_ANNULLAMENTO_UTENTE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .guard(new GuardSendDeviceReturnMessage(deviceReturnMessageService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISATTIVAZIONE_SOSPESA)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISATTIVAZIONE_SOSPESA)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmUtil));
  }

}
