package it.fai.ms.efservice.service.fsm.type.gransanbernardo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.gransanbernardo.FsmModificaGranSanBernardoFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaGranSanBernardoFurtoSmarrimento.FSM_MODIFICA_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO)
public class FsmModificaGranSanBernardoFurtoSmarrimento extends FsmRichiestaGeneric {
  
  public static final String FSM_MODIFICA_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO = "fsmModificaGranSanBernardoFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaGranSanBernardoFurtoSmarrimento(@Qualifier(FsmModificaGranSanBernardoFurtoSmarrimentoConfig.MOD_TES_GRAN_SANBERNARDO_FURTO_SMARRIMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    this.fsmType = FsmType.MOD_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO };
  }

   @Override
   protected FsmType getOldFsm() {
     return null;
   }

   @Override
   protected Logger getLogger() {
     return log;
   }
  
}
