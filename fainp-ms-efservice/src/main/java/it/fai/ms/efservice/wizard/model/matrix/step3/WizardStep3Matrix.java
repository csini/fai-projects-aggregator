package it.fai.ms.efservice.wizard.model.matrix.step3;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;

public class WizardStep3Matrix implements Serializable {

  private static final long serialVersionUID = 8957461942610199859L;

  private Map<WizardVehicleId, WizardMatrixActivability> map = new LinkedHashMap<>();
  private String                                         serviceTypeId;

  public WizardStep3Matrix(final String _serviceTypeId) {
    serviceTypeId = _serviceTypeId;
  }

  public void add(final WizardVehicleId _wizardVehicleId, final WizardMatrixActivability _wizardMatrixActivability) {
    map.put(_wizardVehicleId, _wizardMatrixActivability);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep3Matrix) _obj).getServiceTypeId(), serviceTypeId)
                 && Objects.equals(((WizardStep3Matrix) _obj).getMap(), map);
    }
    return isEquals;
  }

  public Map<WizardVehicleId, WizardMatrixActivability> getMap() {
    return map;
  }

  public String getServiceTypeId() {
    return serviceTypeId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceTypeId, map);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardStep3Matrix [map=");
    builder.append(this.map);
    builder.append(", serviceTypeId=");
    builder.append(this.serviceTypeId);
    builder.append("]");
    return builder.toString();
  }

}
