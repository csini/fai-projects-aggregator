package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.AbstractDmlSender;
import it.fai.ms.common.dml.efservice.dto.DispositivoDMLDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;

public class DispositivoDmlSenderUtil extends AbstractDmlSender<DispositivoDMLDTO, Dispositivo> {

  static final JmsTopicNames TOPIC_SAVE   = JmsTopicNames.DML_DISPOSITIVI_SAVE;
  static final JmsTopicNames TOPIC_DELETE = JmsTopicNames.DML_DISPOSITIVI_DELETE;

  private final DispositivoJmsMapper dispositivoJmsMapper ;

  public DispositivoDmlSenderUtil(JmsProperties jmsProperties,DispositivoJmsMapper dispositivoJmsMapper) {
    super(jmsProperties);
    this.dispositivoJmsMapper = dispositivoJmsMapper;
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

  @Override
  protected DispositivoDMLDTO getSavingDmlDto(Dispositivo ord) {
    DispositivoDMLDTO dto = dispositivoJmsMapper.toDispositivoDMLDTO(ord);
    if(dto.getDmlUniqueIdentifier()==null || dto.getDmlUniqueIdentifier().isEmpty())
      throw new IllegalArgumentException("DmlUniqueIdentifier cannot be null");
    return dto;
  }

  @Override
  protected DispositivoDMLDTO getDeletingDmlDto(String identificativo) {
    DispositivoDMLDTO dto = dispositivoJmsMapper.toEmptyDispositivoDMLDTO(identificativo);
    if(dto.getDmlUniqueIdentifier()==null || dto.getDmlUniqueIdentifier().isEmpty())
      throw new IllegalArgumentException("DmlUniqueIdentifier cannot be null");
    return dto;
  }
}
