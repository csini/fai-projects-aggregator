package it.fai.ms.efservice.client.dto;

import java.io.Serializable;

public class Marca implements Serializable {

  private String nome;

  public String getNome() {
    return nome;
  }

  public Marca nome(String nome) {
    this.nome = nome;
    return this;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
}
