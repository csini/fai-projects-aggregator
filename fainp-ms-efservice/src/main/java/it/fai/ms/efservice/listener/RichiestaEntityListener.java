package it.fai.ms.efservice.listener;

import java.util.UUID;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.jms.dml.RichiestaDmlSenderUtil;
import it.fai.ms.efservice.util.BeanUtil;

public class RichiestaEntityListener {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  
  //no-injection please
  RichiestaDmlSenderUtil dmlService;

  @PrePersist
  public void setIdentificativo(final Richiesta richiesta) {

    String uuidIdentificativo = UUID.randomUUID()
                                    .toString();
    if (logger.isDebugEnabled()) {
      logger.debug("Set identificativo: " + uuidIdentificativo + " for RICHIESTA: " + richiesta);
    }

    richiesta.setIdentificativo(uuidIdentificativo);
  }
  
  @PostPersist
  @PostUpdate
  public void sendSaveNotificationDML(Richiesta richiesta){
    if (dmlService==null) {
      dmlService = new RichiestaDmlSenderUtil(BeanUtil.getBean(JmsProperties.class));
    }
    dmlService.sendSaveNotification(richiesta);
  }
  
  @PostRemove
  public void sendDeleteNotificationDML(Richiesta richiesta){
    if (dmlService==null) {
      dmlService = new RichiestaDmlSenderUtil(BeanUtil.getBean(JmsProperties.class));
     }
    dmlService.sendDeleteNotification(richiesta.getIdentificativo());
  }
  
}
