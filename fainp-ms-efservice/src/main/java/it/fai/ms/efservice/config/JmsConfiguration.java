package it.fai.ms.efservice.config;

import javax.annotation.Resource;
import javax.jms.MessageListener;

import it.fai.ms.integration.system.sock.core.factory.PluginFactory;
import it.fai.ms.integration.system.sock.api.interfaces.FaiJmsListenerEndpointRegistrar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;

import it.fai.ms.common.jms.AndesJmsListenerEndpointRegistrar;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.service.jms.listener.JmsDmlVeicoliSaveListener;
import it.fai.ms.efservice.service.jms.listener.JmsListenerClienteFaiDmlSaveService;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceLicensePlateChanged;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceLicensePlateNotChangeable;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceNoPlateInsert;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDevicePlate2Update;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceServiceActivated;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceServiceDeactivated;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceStateUpdate;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceStolen;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceTheftFailure;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceVehicleJoin;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceVehicleUnjoin;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerStatoDispositivoServizioUpdateMessage;
import it.fai.ms.efservice.service.jms.listener.orderrequest.JmsListenerTrackyCardDeviceNotOrderable;
import it.fai.ms.efservice.service.jms.listener.orderrequest.JmsListenerTransitoFai;

@Configuration
//@ConditionalOnClass({MessageSystemManager.class})
@EnableJms
public class JmsConfiguration implements JmsListenerConfigurer {

  private static final Logger log = LoggerFactory.getLogger(JmsConfiguration.class);

  public static String username = "admin";
  public static String password = "faOK6NJlmj4XKZ";
  public static String url      = "exage-fai-svil-02.northeurope.cloudapp.azure.com";
  public static String port     = "5675";

  @Autowired
  JmsProperties jmsProperties;

  @Autowired
  JmsListenerClienteFaiDmlSaveService jmsListenerClienteFaiDmlSaveService;

  @Autowired
  JmsListenerDeviceServiceActivated jmsListenerDeviceServiceActivated;

  @Autowired
  JmsListenerDeviceServiceDeactivated jmsListenerDeviceServiceDeactivated;

  @Autowired
  JmsDmlVeicoliSaveListener jmsDmlVeicoliSaveListener;

  @Autowired
  JmsListenerDeviceLicensePlateChanged jmsListenerDeviceLicensePlaceChanged;

  @Autowired
  JmsListenerDeviceLicensePlateNotChangeable jmsListenerDeviceLicensePlaceNotChangeable;

  @Autowired
  JmsListenerStatoDispositivoServizioUpdateMessage jmsListenerStatoDispositivoServizioUpdateMessage;

  @Autowired
  JmsListenerDevicePlate2Update jmsListenerDevicePlate2Update;

  @Autowired
  JmsListenerDeviceVehicleJoin jmsListenerDeviceVehicleJoin;

  @Autowired
  JmsListenerDeviceVehicleUnjoin jmsListenerDeviceVehicleUnjoin;

  @Autowired
  JmsListenerDeviceStateUpdate jmsListenerDeviceStateUpdate;

  @Autowired
  JmsListenerDeviceNoPlateInsert jmsListenerDeviceNoPlateInsert;

  @Autowired
  JmsListenerDeviceStolen jmmListenerDeviceStolen;

  @Autowired
  JmsListenerDeviceTheftFailure jmsListenerDeviceTheftFailure;

  @Autowired
  JmsListenerTransitoFai jmsListenerTransitoFai;

  //@Autowired
  //JmsListenerTrackyCardDeviceOrdered jmsListenerTrackyCardOrdered;

  @Autowired
  JmsListenerTrackyCardDeviceNotOrderable jmsListenerTrackyCardNotOrderable;


  @Override
  public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
/*
    registerDurableTopic(JmsTopicNames.DML_AZIENDE_SAVE, jmsListenerClienteFaiDmlSaveService, registrar);

    registerDurableTopic(JmsTopicNames.SERVICE_ACTIVATED, jmsListenerDeviceServiceActivated, registrar);

    registerDurableTopic(JmsTopicNames.SERVICE_DEACTIVATED, jmsListenerDeviceServiceDeactivated, registrar);

    registerDurableTopic(JmsTopicNames.DML_VEHICLE_SAVE, jmsDmlVeicoliSaveListener, registrar);

    registerDurableTopic(JmsTopicNames.DEVICE_LICENSEPLATE_CHANGED, jmsListenerDeviceLicensePlaceChanged, registrar);
    registerDurableTopic(JmsTopicNames.DEVICE_LICENSEPLATE_NOT_CHANGEABLE, jmsListenerDeviceLicensePlaceNotChangeable, registrar);

    registerDurableTopic(JmsTopicNames.DEVICE_STOLEN, jmmListenerDeviceStolen, registrar);
    registerDurableTopic(JmsTopicNames.DEVICE_THEFT_NOTIFICATION_FAILURE, jmsListenerDeviceTheftFailure, registrar);

    registerQueue(JmsQueueNames.STATODISPOSITIVOSERVIZIO_UPDATE, jmsListenerStatoDispositivoServizioUpdateMessage, registrar);

    registerQueue(JmsQueueNames.DEVICE_PLATE_2_UPDATE, jmsListenerDevicePlate2Update, registrar);

    registerQueue(JmsQueueNames.JOIN_DEVICE_VEHICLE, jmsListenerDeviceVehicleJoin, registrar);
    registerQueue(JmsQueueNames.UNJOIN_DEVICE_VEHICLE, jmsListenerDeviceVehicleUnjoin, registrar);

    registerQueue(JmsQueueNames.DEVICE_STATE_UPDATE, jmsListenerDeviceStateUpdate, registrar);

    registerQueue(JmsQueueNames.DEVICE_NOPLATE_INSERT, jmsListenerDeviceNoPlateInsert, registrar);


    registerQueue(JmsQueueNames.ORDER_TRANSITO, jmsListenerTransitoFai, registrar);
*/
    // move to new registry implemetation
 //   registerQueue(JmsQueueNames.TRACKYCARD_DEVICE_ORDERED, jmsListenerTrackyCardOrdered, registrar);
  //  registerQueue(JmsQueueNames.TRACKYCARD_DEVICE_NOT_ORDERABLE, jmsListenerTrackyCardNotOrderable, registrar);
  }

  private void registerQueue(JmsQueueNames queue, MessageListener listener, JmsListenerEndpointRegistrar registrar) {
    log.info("Register Queue: [Queue: {} - Listener: {} - ListenerEndpointRegistrar: {}]", queue, listener, registrar);
    AndesJmsListenerEndpointRegistrar.registerQueueConsumer(queue, listener, registrar, jmsProperties);
  }

  private void registerDurableTopic(JmsTopicNames topic, MessageListener listener, JmsListenerEndpointRegistrar register) {
    log.info("Register Durable topic: [Topic: {} - Listener: {} - ListenerEndpointRegistrar: {}]", topic, listener, register);
    try {
      jmsListenerFactory.getPlugin(FaiJmsListenerEndpointRegistrar.class)
        .registerDurableTopicSubscription(topic,listener,register,jmsProperties);
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    AndesJmsListenerEndpointRegistrar.registerDurableTopicSubscription(topic, listener, register, jmsProperties);

    //messageSystemManager.loadJmsListener().registerDurableTopicSubscription(topic, listener, register, jmsProperties);
  }

  @Autowired
    @Resource(name = "jmsListenerFactory")
  PluginFactory jmsListenerFactory;
}
