package it.fai.ms.efservice.service.fsm.config.vispro;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActiveDevice;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsVisproMissing;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsVisproPresent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroVisproConfig.INOLTRO_VISPRO)
public class FsmInoltroVisproConfig extends  FsmRichiestaConfig {
  
  public static final String INOLTRO_VISPRO = "inoltroVispro";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;
  private final DocumentService documentService;
  private final DeviceProducerService deviceProducerService;

  public FsmInoltroVisproConfig(final FsmSenderToQueue _senderFsmService, DocumentService documentService, DeviceProducerService deviceProducerService) {
    super();
    this.senderFsmService = _senderFsmService;
    this.documentService = documentService;
    this.deviceProducerService = deviceProducerService;
  }
  
  @Override
  protected Logger getLogger() {
    return log;
  }
  
  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_VISPRO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroVispro();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
    .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .target(StatoRichiesta.INCOMPLETO_TECNICO)
    .guard(new GuardDocumentsVisproMissing(documentService))
    .and()
    .withExternal()// MANUAL
    .source(StatoRichiesta.INCOMPLETO_TECNICO)
    .target(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .event(RichiestaEvent.MU_DOCUMENTO_CARICATO)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .target(StatoRichiesta.ATTIVO_EVASO)
    .guard(new GuardDocumentsVisproPresent(documentService))
    .action(new FsmActionActiveDevice(senderFsmService, deviceProducerService, DispositivoEvent.SPEDITO_DAL_FORNITORE));
  }

}
