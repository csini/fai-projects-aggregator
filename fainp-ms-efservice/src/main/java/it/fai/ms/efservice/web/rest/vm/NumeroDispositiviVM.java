package it.fai.ms.efservice.web.rest.vm;

public class NumeroDispositiviVM {
	
	public NumeroDispositiviVM(long count, String codiceAzienda) {
		super();
		this.count = count;
		this.codiceAzienda = codiceAzienda;
	}
	private final long count;
	private final String codiceAzienda;
	
	public long getCount() {
		return count;
	}
	public String getCodiceAzienda() {
		return codiceAzienda;
	}
	@Override
	public String toString() {
		return "NumeroDispositiviVM [count=" + count + ", codiceAzienda=" + codiceAzienda + "]";
	}
}
