package it.fai.ms.efservice.service.fsm.bean.viatoll;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmModificaViaTollFurtoSmarrimento;

@WithStateMachine(id = FsmModificaViaTollFurtoSmarrimento.FSM_MODIFICA_VIATOLL_FURTO_SMARRIMENTO)
public class FsmModificaViaTollFurtoSmarrimentoTransitionBean extends AbstractFsmRichiestaTransition {
}
