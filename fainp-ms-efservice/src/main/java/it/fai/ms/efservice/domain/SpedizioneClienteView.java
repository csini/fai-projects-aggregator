package it.fai.ms.efservice.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "view_spedizioni_clienti")
public class SpedizioneClienteView {
  @Id
  private Long id;

  @Column(name = "codice_cliente")
  private String codiceAzienda;

  @Column(name = "ragione_sociale")
  private String ragioneSociale;

  @Column(name = "numero_dispositivi_spedibili")
  private Long numeroDispositiviSpedibili = 0L;

  @Column(name = "numero_dispositivi_disponibili")
  private Long numeroDispositiviDisponibili = 0L;

  @Column(name = "numero_dispositivi_da_ricevere")
  private Long numeroDispositiviDaRicevere = 0L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  public Long getNumeroDispositiviDisponibili() {
    return numeroDispositiviDisponibili;
  }

  public void setNumeroDispositiviDisponibili(Long numeroDispositiviDisponibili) {
    this.numeroDispositiviDisponibili = numeroDispositiviDisponibili;
  }

  public Long getNumeroDispositiviDaRicevere() {
    return numeroDispositiviDaRicevere;
  }

  public void setNumeroDispositiviDaRicevere(Long numeroDispositiviDaRicevere) {
    this.numeroDispositiviDaRicevere = numeroDispositiviDaRicevere;
  }

  @Override
  public String toString() {
    return "SpedizioneClienteView{" +
      "id=" + id +
      ", codiceAzienda='" + codiceAzienda + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", numeroDispositiviSpedibili=" + numeroDispositiviSpedibili +
      ", numeroDispositiviDisponibili=" + numeroDispositiviDisponibili +
      ", numeroDispositiviDaRicevere=" + numeroDispositiviDaRicevere +
      '}';
  }
}
