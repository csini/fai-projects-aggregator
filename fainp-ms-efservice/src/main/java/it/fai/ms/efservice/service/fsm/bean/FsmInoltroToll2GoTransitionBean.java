package it.fai.ms.efservice.service.fsm.bean;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmInoltroToll2Go")
public class FsmInoltroToll2GoTransitionBean extends AbstractFsmRichiestaTransition {
}
