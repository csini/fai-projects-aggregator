package it.fai.ms.efservice.service.fsm.exception;

public class FsmActionException extends Exception {

  private static final long serialVersionUID = 3454274494550690325L;

  public FsmActionException() {
    this(FsmActionException.class.toString());
  }

  public FsmActionException(String message, Throwable cause) {
    super(message, cause);
  }

  public FsmActionException(String message) {
    super(message);
  }

  public FsmActionException(Throwable cause) {
    super(cause);
  }

}
