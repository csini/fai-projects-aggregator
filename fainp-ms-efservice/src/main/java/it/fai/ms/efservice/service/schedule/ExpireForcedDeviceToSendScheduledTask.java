package it.fai.ms.efservice.service.schedule;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.config.ApplicationProperties;
import it.fai.ms.efservice.service.DispositiviDaSpedireCacheService;

@Component
public class ExpireForcedDeviceToSendScheduledTask {

  private final Logger log = LoggerFactory.getLogger(ExpireForcedDeviceToSendScheduledTask.class);

  private final DispositiviDaSpedireCacheService deviceToSendCache;

  private final ApplicationProperties applicationProperties;

  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private Boolean enabledSchedule;

  public ExpireForcedDeviceToSendScheduledTask(final DispositiviDaSpedireCacheService _deviceToSendCache,
                                               final ApplicationProperties _applicationProperties) {
    deviceToSendCache = _deviceToSendCache;
    applicationProperties = _applicationProperties;
  }

  @PostConstruct
  public void init() {
    if (applicationProperties != null) {
      enabledSchedule = applicationProperties.getScheduler()
                                             .isEnable();
    } else {
      log.error("Not initialize boolean value: enabledScheduler");
    }
  }

  // @Scheduled(cron = "*/60 * * * * MON-FRI")
  @Scheduled(cron = "${application.scheduler.forcedevicetosend}")
  public void run() throws Exception {
    long tStart = System.currentTimeMillis();
    String simpleNameClass = ExpireForcedDeviceToSendScheduledTask.class.getSimpleName();
    if (enabledSchedule != null && enabledSchedule) {
      log.info("-> Starting {} at {}", simpleNameClass, formattedLocalDate());

      deviceToSendCache.expireAllKeys();

      long tFinish = System.currentTimeMillis();
      log.info("-> Ending {} at {} in {} ms", simpleNameClass, formattedLocalDate(), (tFinish - tStart));
    } else {
      log.warn("Scheduler for change stato Ordine Cliente is enabled ? {}", enabledSchedule);
    }
  }

  private String formattedLocalDate() {
    LocalDateTime date = LocalDateTime.now();
    String dateFormatted = date.format(formatter);
    return dateFormatted;
  }

}
