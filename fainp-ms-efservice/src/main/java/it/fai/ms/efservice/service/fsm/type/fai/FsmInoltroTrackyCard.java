package it.fai.ms.efservice.service.fsm.type.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroTrackyCard.FSM_INOLTRO_TRACKYCARD)
public class FsmInoltroTrackyCard extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_TRACKYCARD = "fsmInoltroTrackyCard";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroTrackyCard(@Qualifier("inoltroTrackyCard") StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                              FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_TRACKYCARD;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TRACKYCARD };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
