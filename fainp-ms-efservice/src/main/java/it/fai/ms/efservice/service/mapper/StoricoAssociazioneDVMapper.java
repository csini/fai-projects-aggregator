package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.StoricoAssociazioneDVDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity StoricoAssociazioneDV and its DTO StoricoAssociazioneDVDTO.
 */
@Mapper(componentModel = "spring", uses = {DispositivoMapper.class, })
public interface StoricoAssociazioneDVMapper extends EntityMapper <StoricoAssociazioneDVDTO, StoricoAssociazioneDV> {

    @Mapping(source = "dispositivo.id", target = "dispositivoId")
    @Mapping(source = "dispositivo.seriale", target = "dispositivoSeriale")
    StoricoAssociazioneDVDTO toDto(StoricoAssociazioneDV storicoAssociazioneDV); 

    @Mapping(source = "dispositivoId", target = "dispositivo")
    StoricoAssociazioneDV toEntity(StoricoAssociazioneDVDTO storicoAssociazioneDVDTO); 
    default StoricoAssociazioneDV fromId(Long id) {
        if (id == null) {
            return null;
        }
        StoricoAssociazioneDV storicoAssociazioneDV = new StoricoAssociazioneDV();
        storicoAssociazioneDV.setId(id);
        return storicoAssociazioneDV;
    }
}
