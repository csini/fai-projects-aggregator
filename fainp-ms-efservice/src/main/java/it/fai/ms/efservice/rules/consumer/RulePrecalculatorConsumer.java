package it.fai.ms.efservice.rules.consumer;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

public interface RulePrecalculatorConsumer extends VehicleConsumer {

  @Override
  void consume(VehicleMessage newMessage);

}
