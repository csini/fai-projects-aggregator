package it.fai.ms.efservice.service.fsm.bean.dartfordcrossing;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmInoltroDartFordCrossing")
public class FsmInoltroDartFordCrossingTransitionBean extends AbstractFsmRichiestaTransition {

}
