package it.fai.ms.efservice.service.fsm.config.telepass.sat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardDispositivoDaSpedireTE;
import it.fai.ms.efservice.service.fsm.guard.GuardGestioneRientro;
import it.fai.ms.efservice.service.fsm.guard.GuardSendDeviceReturnMessage;
import it.fai.ms.efservice.service.fsm.guard.GuardTipoRichiesta;
import it.fai.ms.efservice.service.fsm.guard.GuardMalfunzionamentoSostituzione;
import it.fai.ms.efservice.service.fsm.guard.GuardMezzoRitargatoTE;
import it.fai.ms.efservice.service.fsm.guard.GuardVariazioneTargaTE;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = "ModTSatVarTarga")
public class FsmModificaTSatVarTargaConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTSatVarTargaConfig.class);

  private final FsmSenderToQueue senderFsmUtil;

  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  public FsmModificaTSatVarTargaConfig(FsmSenderToQueue senderFsmUtil, DeviceReturnMessageProducerService _deviceReturnMessageService) {
    this.senderFsmUtil = senderFsmUtil;
    deviceReturnMessageService = _deviceReturnMessageService;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmModificaTSatVarTarga");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT, ctx -> log.trace("Initial: " + ctx.getTarget()
                                                                                              .getIds()))
          .states(StatoRichiestaUtil.getStateOfTSatVarTargaRitargatoMalfSostituzione());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmUtil)) 
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .guard(new GuardTipoRichiesta(TipoRichiesta.VARIAZIONE_TARGA))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MEZZO_RITARGATO))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro())
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// MANUALGUARD
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .guard(new GuardDispositivoDaSpedireTE())
               .action(new FsmActionVerificaRientroDispositivo(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO)
               .guard(new GuardSendDeviceReturnMessage(deviceReturnMessageService))
               .action(new FsmActionGeneric()) 
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmUtil));
  }

}
