package it.fai.ms.efservice.wizard.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.service.WizardVehicleService;

@Service
public class WizardVehicleConsumerImpl implements WizardVehicleConsumer {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private WizardVehicleService wizardVehicleService;

  public WizardVehicleConsumerImpl(final WizardVehicleService _wizardVehicleService) {
    wizardVehicleService = _wizardVehicleService;
  }

  @Override
  @Transactional
  public void consume(final VehicleMessage _newMessage) {
    _log.info("Consuming message {}...", _newMessage);
    process(_newMessage);
    _log.info("...Message with uuid {} consumed", _newMessage.getUuid());
  }

  private void process(final VehicleMessage _newMessage) {
    wizardVehicleService.addVehicle(mapToWizardVehicle(_newMessage));
  }

  private WizardVehicle mapToWizardVehicle(final VehicleMessage _newMessage) {
    final WizardVehicle wizardVehicle = new WizardVehicle(new WizardVehicleId(_newMessage.getUuid()));
    wizardVehicle.setEuroClass(_newMessage.getEuroClass());
    wizardVehicle.setLicensePlate(_newMessage.getLicensePlate());
    wizardVehicle.setState(_newMessage.getState());
    wizardVehicle.setType(_newMessage.getType());
    wizardVehicle.setLibretto(_newMessage.getLibretto());
    wizardVehicle.setCountry(_newMessage.getCountry());
    
    return wizardVehicle;
  }

}