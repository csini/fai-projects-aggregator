package it.fai.ms.efservice.listener;

import java.util.UUID;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.jms.dml.ContrattoMessageSenderService;
import it.fai.ms.efservice.service.jms.mapper.ContrattoDmlSenderMapper;
import it.fai.ms.efservice.util.BeanUtil;

public class ContrattoEntityListener {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private ContrattoMessageSenderService contrattoMessageSenderService;

  @PrePersist
  public void setIdentificativo(final Contratto contract) {
    if (contract != null && StringUtils.isBlank(contract.getIdentificativo())) {
      String uuidIdentificativo = UUID.randomUUID()
                                      .toString();
      if (log.isDebugEnabled()) {
        log.debug("Set identificativo: " + uuidIdentificativo + " for CONTRATTO: " + contract);
      }

      contract.setIdentificativo(uuidIdentificativo);
    } else {
      log.error("Contract [{}] is null or identificativo is empty", contract);
    }
  }

  @PostPersist
  @PostUpdate
  public void sendSaveDmlContratto(Contratto contract) {
    checkAndInstanceSenderService();
    contrattoMessageSenderService.sendSaveNotification(contract);
  }

  @PostRemove
  public void sendDeleteDmlContratto(Contratto contract) {
    checkAndInstanceSenderService();
    contrattoMessageSenderService.sendDeleteNotification(contract);
  }

  private void checkAndInstanceSenderService() {
    if (contrattoMessageSenderService == null) {
      contrattoMessageSenderService = new ContrattoMessageSenderService(BeanUtil.getBean(JmsProperties.class),
                                                                        new ContrattoDmlSenderMapper());
    }
  }

}
