package it.fai.ms.efservice.wizard.repository;

import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

public interface WizardDeviceTypesForVehicleRepository {

  Set<WizardDeviceType> findByWizardVehicleId(WizardVehicleId wizardVehicleId);

}
