package it.fai.ms.efservice.domain.enumeration;

public enum TipologiaAssociazione {
  NOTARGA, SCORTA;
}
