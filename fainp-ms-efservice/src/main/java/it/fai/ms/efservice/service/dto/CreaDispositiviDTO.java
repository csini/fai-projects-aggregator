package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;

public class CreaDispositiviDTO implements Serializable {

  private static final long serialVersionUID = 7430532630023890045L;

  private String codiceCliente, codiceContratto;

  private List<CreaDispositivo> dispositivi;

  private TipoDispositivoEnum tipoDispositivo;

  private String hardwareType;

  public static class CreaDispositivo {
    private String seriale;

    private String            uuidVeicolo;
    private Instant           dataAssociazioneTarga;
    private StatoDispositivo  stato                   = StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE;
    private Instant           dataUltimaModificaStato = Instant.now();
    private Instant           dataSpedizione;
    private String            note;
    private List<CreaServizi> servizi;
    private Instant           dataPrimaAttivazione;

    public String getSeriale() {
      return seriale;
    }

    public void setSeriale(String seriale) {
      this.seriale = seriale;
    }

    public String getUuidVeicolo() {
      return uuidVeicolo;
    }

    public void setUuidVeicolo(String uuidVeicolo) {
      this.uuidVeicolo = uuidVeicolo;
    }

    public Instant getDataAssociazioneTarga() {
      return dataAssociazioneTarga;
    }

    public void setDataAssociazioneTarga(Instant dataAssociazioneTarga) {
      this.dataAssociazioneTarga = dataAssociazioneTarga;
    }

    public StatoDispositivo getStato() {
      return stato;
    }

    public void setStato(StatoDispositivo stato) {
      this.stato = stato;
    }

    public Instant getDataUltimaModificaStato() {
      return dataUltimaModificaStato;
    }

    public void setDataUltimaModificaStato(Instant dataUltimaModificaStato) {
      this.dataUltimaModificaStato = dataUltimaModificaStato;
    }

    public Instant getDataSpedizione() {
      return dataSpedizione;
    }

    public void setDataSpedizione(Instant dataSpedizione) {
      this.dataSpedizione = dataSpedizione;
    }

    public String getNote() {
      return note;
    }

    public void setNote(String note) {
      this.note = note;
    }

    public List<CreaServizi> getServizi() {
      return servizi;
    }

    public void setServizi(List<CreaServizi> servizi) {
      this.servizi = servizi;
    }

    public Instant getDataPrimaAttivazione() {
      return dataPrimaAttivazione;
    }

    public void setDataPrimaAttivazione(Instant dataPrimaAttivazione) {
      this.dataPrimaAttivazione = dataPrimaAttivazione;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("CreaDispositivo [seriale=");
      builder.append(seriale);
      builder.append(", uuidVeicolo=");
      builder.append(uuidVeicolo);
      builder.append(", dataAssociazioneTarga=");
      builder.append(dataAssociazioneTarga);
      builder.append(", stato=");
      builder.append(stato);
      builder.append(", dataUltimaModificaStato=");
      builder.append(dataUltimaModificaStato);
      builder.append(", dataSpedizione=");
      builder.append(dataSpedizione);
      builder.append(", note=");
      builder.append(note);
      builder.append(", servizi=");
      builder.append(servizi);
      builder.append(", dataPrimaAttivazione=");
      builder.append(dataPrimaAttivazione);
      builder.append("]");
      return builder.toString();
    }

  }

  public static class CreaServizi {
    private TipoServizioEnum tipoServizio;
    private Instant          dataAttivazione;
    private Instant          dataDisattivazione;
    private String           pan;

    public String getPan() {
      return pan;
    }

    public void setPan(String pan) {
      this.pan = pan;
    }

    public TipoServizioEnum getTipoServizio() {
      return tipoServizio;
    }

    public void setTipoServizio(TipoServizioEnum tipoServizio) {
      this.tipoServizio = tipoServizio;
    }

    public Instant getDataAttivazione() {
      return dataAttivazione;
    }

    public void setDataAttivazione(Instant dataAttivazione) {
      this.dataAttivazione = dataAttivazione;
    }

    public Instant getDataDisattivazione() {
      return dataDisattivazione;
    }

    public void setDataDisattivazione(Instant dataDisattivazione) {
      this.dataDisattivazione = dataDisattivazione;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("CreaServizi [tipoServizio=");
      builder.append(tipoServizio);
      builder.append(", dataAttivazione=");
      builder.append(dataAttivazione);
      builder.append(", dataDisattivazione=");
      builder.append(dataDisattivazione);
      builder.append(", pan=");
      builder.append(pan);
      builder.append("]");
      return builder.toString();
    }

  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public List<CreaDispositivo> getDispositivi() {
    return dispositivi;
  }

  public void setDispositivi(List<CreaDispositivo> dispositivi) {
    this.dispositivi = dispositivi;
  }

  public String getHardwareType() {
    return hardwareType;
  }

  public void setHardwareType(String hardwareType) {
    this.hardwareType = hardwareType;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("CreaDispositiviDTO [codiceCliente=");
    builder.append(codiceCliente);
    builder.append(", codiceContratto=");
    builder.append(codiceContratto);
    builder.append(", dispositivi=");
    builder.append(dispositivi);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", hardwareType=");
    builder.append(hardwareType);
    builder.append("]");
    return builder.toString();
  }

}
