package it.fai.ms.efservice.service.jms.listener;

import it.fai.ms.common.dml.AbstractJmsDmlListener;
import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.jms.mapper.ClienteFaiJmsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class JmsListenerClienteFaiDmlSaveService
  extends AbstractJmsDmlListener<AziendaDMLDTO, ClienteFai> {

  private final ClienteFaiJmsMapper clienteFaiMapper = new ClienteFaiJmsMapper();

  public JmsListenerClienteFaiDmlSaveService(ClienteFaiRepository repository) throws Exception {
    super(AziendaDMLDTO.class, repository);
  }

  @Override
  public ClienteFai getEntityToPersist(AziendaDMLDTO dto, ClienteFai clienteFai) {

    clienteFai = clienteFaiMapper.toClienteFai(dto, clienteFai);

    return clienteFai;
  }

}
