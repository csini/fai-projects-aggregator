package it.fai.ms.efservice.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.dto.ServizioUtenteDTO;
import it.fai.ms.efservice.service.dto.VistaListaServiziUtentiAppoggioDTO;
import it.fai.ms.efservice.service.mapper.CategoriaServizioMapper;

@Service
@Transactional
public class TipoServizioServiceExt {
  private static Logger                  log = LoggerFactory.getLogger(TipoServizioServiceExt.class);
  private TipoServizioRepositoryExt      tipoServizioRepositoryExt;
  private CategoriaServizioMapper        categoriaServizioMapper;
  private  Collection<TipoServizio> tipoServiziosKMaster;
  private TipoDispositivoServiceExt tipoDispositivoService;

  @Autowired
  public void setTipoServizioRepositoryExt(TipoServizioRepositoryExt tipoServizioRepositoryExt) {
    this.tipoServizioRepositoryExt = tipoServizioRepositoryExt;
  }

  @Autowired
  public void setCategoriaServizioMapper(CategoriaServizioMapper categoriaServizioMapper) {
    this.categoriaServizioMapper = categoriaServizioMapper;
  }


  @Autowired
  public void setTipoDispositiviService(TipoDispositivoServiceExt tipoServizioService) {
    this.tipoDispositivoService = tipoServizioService;


  }
  @PostConstruct
  void setup() {
    TipoServizioEnum[] KMASTER_SERVICES = new TipoServizioEnum[] { TipoServizioEnum.KMASTER_BASE,
                                                                   TipoServizioEnum.KMASTER_CONNECTED,
                                                                   TipoServizioEnum.KMASTER_TRAILER,
                                                                   TipoServizioEnum.KMASTER_SAFETY_SECURITY,
                                                                   TipoServizioEnum.KMASTER_OPZIONALE };
    Collection<TipoServizio> tipoServiziosTmp = new ArrayList<>(KMASTER_SERVICES.length);
    for (TipoServizioEnum tipoServizio : KMASTER_SERVICES) {
      tipoServiziosTmp.add(this.findByNome(tipoServizio.name()));
    }
    tipoServiziosKMaster = Collections.unmodifiableCollection(tipoServiziosTmp);
  }

  public List<ServizioUtenteDTO> getListaServiziUtente(String userFai, CategoriaServizioDTO categoriaServizioDTO) throws Exception {

    List<VistaListaServiziUtentiAppoggioDTO> lista                   = tipoServizioRepositoryExt
      .vistaListaServiziUtenti(categoriaServizioDTO.getId(), userFai);
    List<String>                             tipoServizioList        = tipoServizioRepositoryExt
      .findByCategoriaServizioOrderByOrdinamento(categoriaServizioMapper.toEntity(categoriaServizioDTO))
      .stream()
      .map(TipoServizio::getNome)
      .collect(Collectors.toList());
    List<String>                             tipoServizioActivedList = lista.stream()
      .map(VistaListaServiziUtentiAppoggioDTO::getNome)
      .collect(Collectors.toList());

    tipoServizioList.removeIf(tipoServizioActivedList::remove);

    List<ServizioUtenteDTO> collect = new ArrayList<>();

    for (int i = 0; i < lista.size(); i++) {
      ServizioUtenteDTO servizioUtente = new ServizioUtenteDTO();
      servizioUtente.setNumero(lista.get(i)
        .getCount());
      servizioUtente.setNomeServizio(lista.get(i)
        .getNome());
      if (lista.get(i)
        .getStato()
        .equals("ATTIVO")) {
        servizioUtente.setActive(true);
      } else {
        servizioUtente.setActive(false);
      }

      collect.add(servizioUtente);

    }

    for (String tipoServizio : tipoServizioList) {
      ServizioUtenteDTO servizioUtente = new ServizioUtenteDTO();
      servizioUtente.setNomeServizio(tipoServizio);
      servizioUtente.setActive(false);
      collect.add(servizioUtente);
    }

    return collect;
  }

  public TipoServizio findByNome(String nome) {
    return tipoServizioRepositoryExt.findByNome(nome);
  }

  public List<TipoServizio> findByNames(List<String> nomeTipiServizio) {
    return tipoServizioRepositoryExt.findByNames(nomeTipiServizio);
  }

  public Set<TipoServizio> getAvaiableByTipoDispositivo(TipoDispositivo tipoDispositivo, String tipoHardware) {

    Set<TipoServizio> tipoServizios1 = new HashSet<>(tipoDispositivo.getTipoServizios());
    if (TipoDispositivoEnum.TELEPASS_ITALIANO == tipoDispositivo.getNome() && "AS".equalsIgnoreCase(tipoHardware)) {
      for (TipoServizio servizioKmaster : tipoServiziosKMaster) {
        if (!tipoDispositivo.getTipoServizios().stream()
          .anyMatch(ts1 -> servizioKmaster.getNome()
            .equals(ts1.getNome())))
          tipoServizios1.add(servizioKmaster);
      }
    }
    return tipoServizios1;
  }
}
