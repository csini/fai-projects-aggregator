package it.fai.ms.efservice.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.web.rest.errors.CustomException;

/**
 * Service Interface for managing Richiesta.
 */
@Service
@Transactional
public interface RichiestaModificaServiceExt {

  /**
   * @param carrelloDTO
   * @return
   * @throws FsmExecuteCommandException
   * @throws Exception
   */
  List<Long> createRichiestaModifica(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException, FsmExecuteCommandException;

  List<Long> associaTarga(TipoDispositivoEnum tipoDispositivo, List<String> dispositivi, String newVeicolo);

  List<Long> richiestaPin(TipoDispositivoEnum tipoDispositivo, String codiceAzienda, List<String> dispositivi);
}
