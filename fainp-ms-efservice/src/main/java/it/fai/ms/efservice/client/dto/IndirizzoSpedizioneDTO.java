package it.fai.ms.efservice.client.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class IndirizzoSpedizioneDTO {
  @JsonProperty("destinatario")
  private String destinatario = null;

  @JsonProperty("indirizzoFai")
  private Boolean indirizzoFai = null;

  @JsonProperty("via")
  private String via = null;

  @JsonProperty("citta")
  private String citta = null;

  @JsonProperty("provincia")
  private String provincia = null;

  @JsonProperty("cap")
  private String cap = null;

  @JsonProperty("paese")
  private String paese = null;

  @JsonProperty("fulladdress")
  private String fulladdress = null;

  public IndirizzoSpedizioneDTO destinatario(String destinatario) {
    this.destinatario = destinatario;
    return this;
  }

  /**
   * Get destinatario
   * 
   * @return destinatario
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getDestinatario() {
    return destinatario;
  }

  public void setDestinatario(String destinatario) {
    this.destinatario = destinatario;
  }

  public IndirizzoSpedizioneDTO indirizzoFai(Boolean indirizzoFai) {
    this.indirizzoFai = indirizzoFai;
    return this;
  }

  /**
   * Get indirizzoFai
   * 
   * @return indirizzoFai
   **/
  @ApiModelProperty(value = "")

  public Boolean getIndirizzoFai() {
    return indirizzoFai;
  }

  public void setIndirizzoFai(Boolean indirizzoFai) {
    this.indirizzoFai = indirizzoFai;
  }

  public IndirizzoSpedizioneDTO via(String via) {
    this.via = via;
    return this;
  }

  /**
   * Get via
   * 
   * @return via
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getVia() {
    return via;
  }

  public void setVia(String via) {
    this.via = via;
  }

  public IndirizzoSpedizioneDTO citta(String citta) {
    this.citta = citta;
    return this;
  }

  /**
   * Get citta
   * 
   * @return citta
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getCitta() {
    return citta;
  }

  public void setCitta(String citta) {
    this.citta = citta;
  }

  public IndirizzoSpedizioneDTO provincia(String provincia) {
    this.provincia = provincia;
    return this;
  }

  /**
   * Get provincia
   * 
   * @return provincia
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public IndirizzoSpedizioneDTO cap(String cap) {
    this.cap = cap;
    return this;
  }

  /**
   * Get cap
   * 
   * @return cap
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  public String getCap() {
    return cap;
  }

  public void setCap(String cap) {
    this.cap = cap;
  }

  public IndirizzoSpedizioneDTO paese(String paese) {
    this.paese = paese;
    return this;
  }

  /**
   * Get paese
   * 
   * @return paese
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Size(min = 2, max = 2)
  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public IndirizzoSpedizioneDTO fulladdress(String fulladdress) {
    this.fulladdress = fulladdress;
    return this;
  }

  /**
   * Get fulladdress
   * 
   * @return fulladdress
   **/
  @ApiModelProperty(value = "")

  public String getFulladdress() {
    return fulladdress;
  }

  public void setFulladdress(String fulladdress) {
    this.fulladdress = fulladdress;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IndirizzoSpedizioneDTO indirizzoDTO = (IndirizzoSpedizioneDTO) o;
    return Objects.equals(this.destinatario, indirizzoDTO.destinatario) && Objects.equals(this.indirizzoFai, indirizzoDTO.indirizzoFai)
           && Objects.equals(this.via, indirizzoDTO.via) && Objects.equals(this.citta, indirizzoDTO.citta)
           && Objects.equals(this.provincia, indirizzoDTO.provincia) && Objects.equals(this.cap, indirizzoDTO.cap)
           && Objects.equals(this.paese, indirizzoDTO.paese) && Objects.equals(this.fulladdress, indirizzoDTO.fulladdress);
  }

  @Override
  public int hashCode() {
    return Objects.hash(destinatario, indirizzoFai, via, citta, provincia, cap, paese, fulladdress);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class IndirizzoDTO {\n");

    sb.append("    destinatario: ")
      .append(toIndentedString(destinatario))
      .append("\n");
    sb.append("    indirizzoFai: ")
      .append(toIndentedString(indirizzoFai))
      .append("\n");
    sb.append("    via: ")
      .append(toIndentedString(via))
      .append("\n");
    sb.append("    citta: ")
      .append(toIndentedString(citta))
      .append("\n");
    sb.append("    provincia: ")
      .append(toIndentedString(provincia))
      .append("\n");
    sb.append("    cap: ")
      .append(toIndentedString(cap))
      .append("\n");
    sb.append("    paese: ")
      .append(toIndentedString(paese))
      .append("\n");
    sb.append("    fulladdress: ")
      .append(toIndentedString(fulladdress))
      .append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString()
            .replace("\n", "\n    ");
  }
}
