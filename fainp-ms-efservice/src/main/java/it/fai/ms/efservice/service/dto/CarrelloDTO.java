package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import it.fai.common.enumeration.TipoDispositivoEnum;

public class CarrelloDTO implements Serializable {

  private static final long serialVersionUID = -7313928328948026239L;

  private String tipoServizio;

  private List<DispositivoCarrelloDTO> dispositivi;

  private HashMap<String, String> datiAggiuntivi;

  private List<String> uuidVeicoli;

  private String codiceAzienda;

  private String richiedente;

  private int numViaCard;

  private HashMap<TipoDispositivoEnum, List<datiAggiuntiviCarrelloDTO>> reservations;

  public String getTipoServizio() {
    return tipoServizio;
  }

  public void setTipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public List<DispositivoCarrelloDTO> getDispositivi() {
    return dispositivi;
  }

  public void setDispositivi(List<DispositivoCarrelloDTO> dispositivi) {
    this.dispositivi = dispositivi;
  }

  public List<String> getUuidVeicoli() {
    return uuidVeicoli;
  }

  public void setUuidVeicoli(List<String> uuidVeicoli) {
    this.uuidVeicoli = uuidVeicoli;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public HashMap<String, String> getDatiAggiuntivi() {
    return datiAggiuntivi;
  }

  public void setDatiAggiuntivi(HashMap<String, String> datiAggiuntivi) {
    this.datiAggiuntivi = datiAggiuntivi;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }

  public int getNumViaCard() {
    return numViaCard;
  }

  public void setNumViaCard(int numViaCard) {
    this.numViaCard = numViaCard;
  }

  public HashMap<TipoDispositivoEnum, List<datiAggiuntiviCarrelloDTO>> getReservations() {
    return reservations;
  }

  public void setReservations(HashMap<TipoDispositivoEnum, List<datiAggiuntiviCarrelloDTO>> reservations) {
    this.reservations = reservations;
  }

  @Override
  public String toString() {
    return "CarrelloDTO [tipoServizio=" + tipoServizio + ", dispositivi=" + dispositivi + ", datiAggiuntivi=" + datiAggiuntivi
           + ", uuidVeicoli=" + uuidVeicoli + ", codiceAzienda=" + codiceAzienda + ", richiedente=" + richiedente + ", numViaCard="
           + numViaCard + ", reservations=" + reservations + "]";
  }

}
