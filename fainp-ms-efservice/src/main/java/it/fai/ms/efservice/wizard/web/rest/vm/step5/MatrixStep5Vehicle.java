package it.fai.ms.efservice.wizard.web.rest.vm.step5;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixServiceType;

public class MatrixStep5Vehicle implements Serializable, Comparable<MatrixStep5Vehicle> {

  private static final long serialVersionUID = 7482708009359072308L;

  private String                         euro;
  private String                         identificativo;
  private Map<String, MatrixServiceType> items = new LinkedHashMap<>();
  private String                         targa;
  private String                         tipo;
  private String                         targaNazione;

  public MatrixStep5Vehicle(final String _uuid) {
    identificativo = _uuid;
  }

  @Override
  public int compareTo(final MatrixStep5Vehicle _obj) {
    return targa.compareTo(_obj.getTarga());
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((MatrixStep5Vehicle) _obj).getTarga(), targa);
    }
    return isEquals;
  }

  public String getEuro() {
    return euro;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public Map<String, MatrixServiceType> getItems() {
    return items;
  }

  public String getTarga() {
    return targa;
  }

  public String getTipo() {
    return tipo;
  }

  @Override
  public int hashCode() {
    return Objects.hash(targa);
  }

  public void setEuro(final String _euro) {
    euro = _euro;
  }

  public void setItems(final Map<String, MatrixServiceType> _serviceTypes) {
    items = _serviceTypes;
  }

  public void setTarga(final String _targa) {
    targa = _targa.toUpperCase(Locale.ROOT);
  }

  public void setTipo(final String _tipo) {
    tipo = _tipo;
  }

  public String getTargaNazione() {
    return targaNazione;
  }

  public void setTargaNazione(String targaNazione) {
    this.targaNazione = targaNazione;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixStep5Vehicle [euro=");
    builder.append(this.euro);
    builder.append(",uuid=");
    builder.append(this.identificativo);
    builder.append(",items=");
    builder.append(this.items);
    builder.append(",targa=");
    builder.append(this.targa);
    builder.append(",tipo=");
    builder.append(this.tipo);
    builder.append(",targaNazione=");
    builder.append(this.targaNazione);
    builder.append("]");
    return builder.toString();
  }

}
