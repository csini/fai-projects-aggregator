package it.fai.ms.efservice.client;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.client.anagazienda.ValidazioniAziende;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;

@Component(AnagaziendeClientService.QUALIFIER)
public class AnagaziendeClientService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  public final static String QUALIFIER = "anagaziendeClientService";

  private AnagaziendeClient anagaziendeClient;

  @Value("${efservice.authorizationHeader}") 
  private String authorizationToken;

  public AnagaziendeClientService(final AnagaziendeClient _anagaziendeClient) {
    anagaziendeClient = _anagaziendeClient;
  }

  public IndirizziDiSpedizioneDTO getIndirizzoSpedizioneClienteFai(final String _codiceCliente) {
    _log.debug("Feign request IndirizzoSpedizioneClienteFai : {}", _codiceCliente);
    try {
      return anagaziendeClient.getIndirizzoSpedizioneClienteFai(authorizationToken, _codiceCliente);
    } catch (Exception e) {
      _log.error("getIndirizzoSpedizioneClienteFai::feign error : {}", e.getMessage());
      return null;
    }
  }

  public ValidazioniAziende validaAzienda(List<String> _codiciAzienda) {
    _log.debug("Feign request validaAzienda : {}", _codiciAzienda);
    try {
      return anagaziendeClient.validaAzienda(authorizationToken, _codiciAzienda);
    } catch (Exception e) {
      _log.error("validaAzienda::feign error : {}", e.getMessage());
      return null;
    }
  }

}
