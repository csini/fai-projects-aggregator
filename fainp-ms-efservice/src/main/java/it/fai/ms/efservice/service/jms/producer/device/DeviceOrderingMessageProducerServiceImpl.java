package it.fai.ms.efservice.service.jms.producer.device;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessageFactory;
import it.fai.ms.common.jms.efservice.DeviceSerialNumber;
import it.fai.ms.common.jms.efservice.DeviceType;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderingMessage;
import it.fai.ms.common.jms.efservice.message.device.SimpleDeviceInfo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional
public class DeviceOrderingMessageProducerServiceImpl
  implements DeviceOrderingMessageProducerService {

  private final Logger                       log = LoggerFactory.getLogger(getClass());
  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public DeviceOrderingMessageProducerServiceImpl(final JmsSenderQueueMessageService _jmsSenderQueueService) {
    jmsSenderQueueService = _jmsSenderQueueService;
  }

  private DeviceOrderingMessage buildMessage(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    DeviceOrderingMessage dto = new DeviceOrderingMessage(orderRequestUtil.getContractUuid(), orderRequestUtil.getOrderRequestUuid(),
                                                          orderRequestUtil.getObuType());
    return dto;
  }

  private DeviceEventMessage buildDeviceEventMessage(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    OrderRequestDecorator orderRequest = new OrderRequestDecorator(richiesta);
    DeviceEventMessage deviceEventMessage = DeviceEventMessageFactory.createDeviceOrderMessage(
      new DeviceDataDTO()
        .contractCode(orderRequest.findNumeroContract())
        .requestId(orderRequest.getIdentificativo())
        .deviceCode(orderRequest.getDeviceSerialId())
        .deviceType(orderRequestUtil.getObuType().getId().name())
    );

    return deviceEventMessage;
  }

  @Override
  public void deviceOrdering(Richiesta richiesta) {
    DeviceOrderingMessage dto = null;
    switch (richiesta.getTipoDispositivo()
                     .getNome()) {

    //FIXME Tepeass EU è un refuso? Da verificare con i test
    //qui non dovrebbero MAI arrivare dispositivi TE EU, in ogni caso quegli ordini dovrebbero usare il topic non la coda...
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      dto = buildMessage(richiesta);
      jmsSenderQueueService.sendOrderingMessage(dto);
      break;
    case TELEPASS_ITALIANO:
    case VIACARD:
      DeviceEventMessage eventMessage = buildDeviceEventMessage(richiesta);
      jmsSenderQueueService.sendOrderingMessage(eventMessage);
      break;
    case TRACKYCARD:
      dto = buildMessageWithLicensePlate(richiesta);
      jmsSenderQueueService.sendTrackyCardOrderingMessage(dto);
      break;

    default:
      log.warn("Not manage device to send ordering: {}", richiesta.getTipoDispositivo()
                                                                  .getNome());
      break;
    }
  }

  private DeviceOrderingMessage buildMessageWithLicensePlate(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    DeviceOrderingMessage dto = new DeviceOrderingMessage(orderRequestUtil.getContractUuid(), orderRequestUtil.getOrderRequestUuid(),
                                                          getSimpleDeviceInfoByRichiesta(richiesta), orderRequestUtil.getLicensePlate());
    return dto;
  }

  private SimpleDeviceInfo getSimpleDeviceInfoByRichiesta(Richiesta richiesta) {
    TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
    Dispositivo device = getDispositivoRichiesta(richiesta);

    String seriale = "";
    String oldSeriale = "";
    if (device != null) {
      seriale = (StringUtils.isNotBlank(device.getSeriale()) ? device.getSeriale() : "");
      oldSeriale = (StringUtils.isNotBlank(device.getSerialeSostituito()) ? device.getSerialeSostituito() : "");
    }
    SimpleDeviceInfo sdi = new SimpleDeviceInfo(new DeviceSerialNumber(seriale), new DeviceSerialNumber(oldSeriale),
                                                new DeviceType(tipoDispositivo.getNome()
                                                                              .name()));
    log.info("Simple Device Info generate: {}", sdi);
    return sdi;
  }

  private Dispositivo getDispositivoRichiesta(Richiesta richiesta) {
    Optional<Dispositivo> deviceOpt = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst();
    if (deviceOpt.isPresent()) {
      return deviceOpt.get();
    }
    return null;
  }

}
