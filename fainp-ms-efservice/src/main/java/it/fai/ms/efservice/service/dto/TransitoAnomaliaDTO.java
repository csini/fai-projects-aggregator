package it.fai.ms.efservice.service.dto;

import it.fai.ms.efservice.enumeration.TipiAnomalieTransitoEnum;

import java.io.Serializable;

public class TransitoAnomaliaDTO implements Serializable {

  private String serialeDispositivo;
  private TipiAnomalieTransitoEnum tipoAnomalia;
  private String codiceAzienda;
  private String ragioneSociale;
  private String tipoDispositivo;
  private String nomeFileCaricato;
  private String targaVeicolo;
  private String classeEuro;
  private String messaggioErrore;

  public String getSerialeDispositivo() {
    return serialeDispositivo;
  }

  public void setSerialeDispositivo(String serialeDispositivo) {
    this.serialeDispositivo = serialeDispositivo;
  }

  public TipiAnomalieTransitoEnum getTipoAnomalia() {
    return tipoAnomalia;
  }

  public void setTipoAnomalia(TipiAnomalieTransitoEnum tipoAnomalia) {
    this.tipoAnomalia = tipoAnomalia;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getNomeFileCaricato() {
    return nomeFileCaricato;
  }

  public void setNomeFileCaricato(String nomeFileCaricato) {
    this.nomeFileCaricato = nomeFileCaricato;
  }

  public String getTargaVeicolo() {
    return targaVeicolo;
  }

  public void setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
  }

  public String getClasseEuro() {
    return classeEuro;
  }

  public void setClasseEuro(String classeEuro) {
    this.classeEuro = classeEuro;
  }

  public String getMessaggioErrore() {
    return messaggioErrore;
  }

  public void setMessaggioErrore(String messaggioErrore) {
    this.messaggioErrore = messaggioErrore;
  }

  @Override
  public String toString() {
    return "TransitoAnomaliaDTO{" +
      "serialeDispositivo='" + serialeDispositivo + '\'' +
      ", tipoAnomalia=" + tipoAnomalia +
      ", codiceAzienda='" + codiceAzienda + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", tipoDispositivo='" + tipoDispositivo + '\'' +
      ", nomeFileCaricato='" + nomeFileCaricato + '\'' +
      ", targaVeicolo='" + targaVeicolo + '\'' +
      ", classeEuro='" + classeEuro + '\'' +
      ", messaggioErrore='" + messaggioErrore + '\'' +
      '}';
  }
}
