package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.annotation.WithStateMachine;

@WithStateMachine(id = "fsmContratto")
public class FsmContrattoTransitionBean extends AbstractFsmContrattoTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

}
