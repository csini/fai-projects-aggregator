package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceAssociationMessageProducerService {

  void associationDeviceTelepassIta(Richiesta richiesta);

}
