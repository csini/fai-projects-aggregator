package it.fai.ms.efservice.vehicle.ownership.web.rest.vm;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AvailableVehicle implements Serializable {

  private static final long serialVersionUID = -7498903464518700435L;

  private String licensePlate;
  private String uuid;

  public AvailableVehicle() {
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((AvailableVehicle) _obj).getUuid(), uuid);
    }
    return isEquals;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  @JsonProperty(value = "vehicleUuid")
  public String getUuid() {
    return uuid;
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid);
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public void setUuid(final String _uuid) {
    uuid = _uuid;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AvailableVehicle [uuid=");
    builder.append(this.uuid);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append("]");
    return builder.toString();
  }

}
