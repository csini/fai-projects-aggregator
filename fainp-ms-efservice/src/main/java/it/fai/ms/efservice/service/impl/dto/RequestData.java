package it.fai.ms.efservice.service.impl.dto;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;

public class RequestData {

  public ClienteFai clienteFai;
  public List<AssociazioneDV> assDvs;
  public CarrelloModificaRichiestaDTO carrelloDto;
  public Map<String,Optional<AssociazioneDV>> reqDispositivi;
  public List<AssociazioneDV> targetAssDvs;
  public TipoRichiesta getTipoOperazioneModifica() {
    return carrelloDto.getTipoOperazioneModifica();
  }
  public String getNewVeicolo() {
    return carrelloDto.getNewVeicolo();
  }

}
