package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.RaggruppamentoRichiesteFornitoreView;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.RaggruppamentoRichiesteFornitoreViewRepositoryExt;
import it.fai.ms.efservice.repository.RaggruppamentoRichiesteOrdineFornitoreRepositoryExt;
import it.fai.ms.efservice.service.dto.SearchRaggruppamentoDTO;
import it.fai.ms.efservice.service.specifications.SearchRaggruppamentoOrdineSpecification;

@Service
@Transactional
public class RaggruppamentoRichiesteOrdineFornitoreServiceExt {

  private final Logger log = LoggerFactory.getLogger(RaggruppamentoRichiesteOrdineFornitoreServiceExt.class);

  private final RaggruppamentoRichiesteOrdineFornitoreRepositoryExt repositoryExt;
  private final RaggruppamentoRichiesteFornitoreViewRepositoryExt   viewRepositoryExt;

  public RaggruppamentoRichiesteOrdineFornitoreServiceExt(RaggruppamentoRichiesteOrdineFornitoreRepositoryExt repositoryExt,
                                                          RaggruppamentoRichiesteFornitoreViewRepositoryExt viewRepositoryExt) {
    this.repositoryExt = repositoryExt;
    this.viewRepositoryExt = viewRepositoryExt;
  }

  @Transactional(readOnly = true)
  public List<RaggruppamentoRichiesteFornitoreView> findAllByTipoDispositivo(String tipoDispositivoNome) {
    try {
      return viewRepositoryExt.findAllByTipoDispositivoOrderByDataGenerazioneFileDesc(tipoDispositivoNome);
//    	return viewRepositoryExt.findAll(RaggruppamentoRichiesteFornitoreViewRepositoryExt.allOpenByTipoDispositivo(tipoDispositivoNome));
    } catch (IllegalArgumentException e) {
      log.debug("No TipoDispositivoEnum found for name {}", tipoDispositivoNome);
      return Collections.emptyList();
    }
  }

  public Page<RaggruppamentoRichiesteFornitoreView> findAllWithFilter(SearchRaggruppamentoDTO dto, Pageable page) {

    SearchRaggruppamentoOrdineSpecification searchSpeficification = new SearchRaggruppamentoOrdineSpecification(dto);

    Page<RaggruppamentoRichiesteFornitoreView> result = viewRepositoryExt.findAll(searchSpeficification, page);

    return new PageImpl<>(result.getContent(), page, result.getTotalElements());
  }

  @Transactional(readOnly = true)
  public Optional<RaggruppamentoRichiesteOrdineFornitore> findByNomeFileDataAcquisizioneTipoDispositivo(String nomeFileRicevuto,
                                                                                                        Instant dataGenerazioneFile,
                                                                                                        TipoDispositivo tipoDispositivo) {

    log.debug("Ricerca RaggruppamentoRichiesteOrdineFornitore con nomeFile:=[{}],  dataGenerazioneFile:=[{}], tipoDispositivo:=[{}]", nomeFileRicevuto, dataGenerazioneFile, tipoDispositivo);

    //findByNomeFileRicevutoAndDataGenerazioneFileAndTipoDispositivo ha problemi con l'Instant: vedi DeviceResponseOkConsumer.saveRaggruppamentoRichiesteFornitore
    Optional<RaggruppamentoRichiesteOrdineFornitore> optRaggruppamento = repositoryExt.findByNomeFileRicevutoAndTipoDispositivo(nomeFileRicevuto, tipoDispositivo)
                                                                                      .stream()
                                                                                      .peek(r -> log.debug("trovato RaggruppamentoRichiesteOrdineFornitore con id:=[{}], nomeFile:=[{}], dataGenerazioneFile:=[{}], tipoDispositivo:=[{}]", r.getId(), r.getNomeFileRicevuto(), r.getDataGenerazioneFile(), r.getTipoDispositivo()))
                                                                                      .filter(r -> {
                                                                                        final Instant dataGenerationTruncated = dataGenerazioneFile.truncatedTo(ChronoUnit.SECONDS);
                                                                                        final Instant savedDataGenerazioneFile = r.getDataGenerazioneFile();
                                                                                        final Instant savedDataDiGenerazioneTruncated = savedDataGenerazioneFile.truncatedTo(ChronoUnit.SECONDS);
                                                                                        log.debug("dataGenerazioneFile:=[{}]", dataGenerazioneFile);
                                                                                        log.debug("dataGenerationTruncated:=[{}]", dataGenerationTruncated);
                                                                                        log.debug("r.getDataGenerazioneFile():=[{}]", savedDataGenerazioneFile);
                                                                                        log.debug("r.getDataGenerazioneFile():=[{}]", savedDataDiGenerazioneTruncated);
                                                                                        final boolean areDatesEquals = dataGenerazioneFile.equals(savedDataGenerazioneFile);
                                                                                        final boolean areDatesTruncatedEquals = dataGenerationTruncated.equals(savedDataDiGenerazioneTruncated);
                                                                                        log.debug("areDatesEquals = dataGenerazioneFile.equals(savedDataGenerazioneFile):=[{}]", areDatesEquals);
                                                                                        log.debug("areDatesTruncatedEquals = dataGenerationTruncated.equals(savedDataDiGenerazioneTruncated):=[{}]", areDatesTruncatedEquals);
                                                                                        return areDatesEquals;
                                                                                      }).findFirst();
    return optRaggruppamento;
  }

  public RaggruppamentoRichiesteOrdineFornitore save(RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore) {

    if (raggruppamentoRichiesteOrdineFornitore.getId() == null) {
      log.info("Persist {}", raggruppamentoRichiesteOrdineFornitore);
    } else {
      log.info("Update {}", raggruppamentoRichiesteOrdineFornitore);
    }

    log.debug("salvataggio RaggruppamentoRichiesteOrdineFornitore con nomeFile:=[{}],  dataGenerazioneFile:=[{}]", raggruppamentoRichiesteOrdineFornitore.getNomeFileRicevuto(), raggruppamentoRichiesteOrdineFornitore.getDataGenerazioneFile());
    final RaggruppamentoRichiesteOrdineFornitore save = repositoryExt.save(raggruppamentoRichiesteOrdineFornitore);
    log.debug("salvato RaggruppamentoRichiesteOrdineFornitore con id:=[{}], nomeFile:=[{}],  dataGenerazioneFile:=[{}]", raggruppamentoRichiesteOrdineFornitore.getId(), raggruppamentoRichiesteOrdineFornitore.getNomeFileRicevuto(), raggruppamentoRichiesteOrdineFornitore.getDataGenerazioneFile());

    return save;
  }

  public RaggruppamentoRichiesteOrdineFornitore saveAnFlush(RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore) {
    RaggruppamentoRichiesteOrdineFornitore raggruppamento = save(raggruppamentoRichiesteOrdineFornitore);
    repositoryExt.flush();
    log.info("Saved and Flush: {}", raggruppamento);
    return raggruppamento;
  }

  public List<RaggruppamentoRichiesteOrdineFornitore> findAllByIdIn(List<Long> ids) {
    return repositoryExt.findAllByIdIn(ids);
  }

  public List<RaggruppamentoRichiesteOrdineFornitore> findAllByNomeFileAndTipoDispositivo(final String nomeFileRicevuto, final TipoDispositivo tipoDispositivo) {
    return repositoryExt.findByNomeFileRicevutoAndTipoDispositivo(nomeFileRicevuto, tipoDispositivo);
  }
}
