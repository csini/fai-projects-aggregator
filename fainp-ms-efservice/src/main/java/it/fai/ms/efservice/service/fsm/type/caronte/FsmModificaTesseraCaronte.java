package it.fai.ms.efservice.service.fsm.type.caronte;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.caronte.FsmModificaTesseraCaronteConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(FsmModificaTesseraCaronte.FSM_MODIFICA_TESSERA_CARONTE)
public class FsmModificaTesseraCaronte extends FsmRichiestaGeneric {

  public static final String FSM_MODIFICA_TESSERA_CARONTE = "fsmModificaTesseraCaronte";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaTesseraCaronte(@Qualifier(FsmModificaTesseraCaronteConfig.MODIFICA_TESSERA_CARONTE_CONFIG) StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory,
                                   FsmRichiestaCacheService cacheService) {
    this.stateMachineFactory = stateMachineFactory;
    this.cacheService = cacheService;
    fsmType = FsmType.MODIFICA_TESSERA_CARONTE;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TESSERA_CARONTE };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }
}
