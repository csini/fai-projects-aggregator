package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.DocumentService.TipologiaDocumento;

public class AssignDocumentRequestDTO implements Serializable {

  private static final long serialVersionUID = 8706288090275360625L;
  
  private String codiceAzienda;

  private String uuidRequest;

  private String uuidDocument;

  private TipoDispositivoEnum deviceType;

  private TipologiaDocumento documentType;

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getUuidRequest() {
    return uuidRequest;
  }

  public void setUuidRequest(String uuidRequest) {
    this.uuidRequest = uuidRequest;
  }

  public String getUuidDocument() {
    return uuidDocument;
  }

  public void setUuidDocument(String uuidDocument) {
    this.uuidDocument = uuidDocument;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public TipologiaDocumento getDocumentType() {
    return documentType;
  }

  public void setDocumentType(TipologiaDocumento documentType) {
    this.documentType = documentType;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AssignDocumentRequest [codiceAzienda=");
    builder.append(codiceAzienda);
    builder.append(", uuidRequest=");
    builder.append(uuidRequest);
    builder.append(", uuidDocument=");
    builder.append(uuidDocument);
    builder.append(", deviceType=");
    builder.append(deviceType);
    builder.append(", documentType=");
    builder.append(documentType);
    builder.append("]");
    return builder.toString();
  }

}
