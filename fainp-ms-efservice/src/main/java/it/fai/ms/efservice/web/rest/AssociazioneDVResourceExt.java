package it.fai.ms.efservice.web.rest;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import it.fai.ms.efservice.service.dto.AssociazioneVeicoliCounterDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.HeaderCustom;
import it.fai.ms.efservice.web.rest.vm.NumeroDispositiviVM;

@RestController
@RequestMapping(AssociazioneDVResourceExt.BASE_PATH)
public class AssociazioneDVResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  public static final String DISPOSITIVI_BY_VEHICLE     = "/dispositivi/veicolo";
  public static final String API_DISPOSITIVI_BY_VEHICLE = BASE_PATH + DISPOSITIVI_BY_VEHICLE;

  public static final String VEHICLES_BY_DISPOSITIVOID     = "/veicoli/dispositivo-id";
  public static final String API_VEHICLES_BY_DISPOSITIVOID = BASE_PATH + VEHICLES_BY_DISPOSITIVOID;

  public static final String DISPOSITIVI_BY_VEHICLES     = "/dispositivi/veicoli";
  public static final String API_DISPOSITIVI_BY_VEHICLES = BASE_PATH + DISPOSITIVI_BY_VEHICLES;

  public static final String DISPOSITIVI_VEHICLES_FOR_MATRIX     = "/dispositivi/veicoli";
  public static final String API_DISPOSITIVI_VEHICLES_FOR_MATRIX = BASE_PATH + DISPOSITIVI_VEHICLES_FOR_MATRIX;

  public static final String DISPOSITIVI_VEHICLES_COUNTER     = "/dispositivi/veicoli/counter";
  public static final String API_DISPOSITIVI_VEHICLES_COUNTER = BASE_PATH + DISPOSITIVI_VEHICLES_COUNTER;
  
  public static final String VEHICLE_DISPOSITIVI_COUNTER     = "/veicoli/dispositivi/counter";
  public static final String API_VEHICLE_DISPOSITIVI_COUNTER = BASE_PATH + VEHICLE_DISPOSITIVI_COUNTER;

  private final AssociazioneDVServiceExt associazioneDVServiceExt;

  public AssociazioneDVResourceExt(AssociazioneDVServiceExt _associazioneDVServiceExt) {
    associazioneDVServiceExt = _associazioneDVServiceExt;
  }

  @GetMapping(DISPOSITIVI_BY_VEHICLE + "/{uuid}/{codiceAzienda}")
  @Timed
  @ApiOperation(
                value = "Mostra tutti i dispositivi associati ad un determinato veicolo",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "uuid is empty") })
  public ResponseEntity<List<DispositivoDTO>> getDispositiviByVehicle(@ApiParam(
                                                                                value = "uuid",
                                                                                required = true) @PathVariable(required = true) String uuid,
                                                                      @ApiParam(
                                                                                value = "codiceAzienda",
                                                                                required = true) @PathVariable(
                                                                                                               required = true) String codiceAzienda) throws Exception {
    log.debug("REST request all devices by vehicle with id : {}", uuid);

    if (StringUtils.isBlank(uuid)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VEHICLE_ID_REQUIRED);
    }

    List<DispositivoDTO> result = associazioneDVServiceExt.findDispositiviByVehicle(uuid, codiceAzienda);
    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("X-size", result != null ? result.size() : 0)
                                              .add("X-uri", API_DISPOSITIVI_BY_VEHICLE + "/" + uuid)
                                              .headers())
                         .body(result);
  }
  
  
  /**
   * Metodo aggiunto per nekte
   */
  @GetMapping(DISPOSITIVI_BY_VEHICLES)
  @Timed
  @ApiOperation(
                value = "Mostra tutti i dispositivi associati ad un determinato veicolo in un determinato stato (blue, green, yellow)",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "uuids is empty, stato is empty") })
  public ResponseEntity<HashMap<String, List<DispositivoDTO>>> getDispositiviByVehiclesStato(@ApiParam(
                                                                                                       value = "uuids",
                                                                                                       required = true) @RequestParam(
                                                                                                                                      required = true) Set<String> uuids,
                                                                                             @ApiParam(
                                                                                                       value = "stato",
                                                                                                       allowableValues = "blue,green,yellow",
                                                                                                       required = true) @RequestParam(
                                                                                                                                      required = true) String stato) throws Exception {
    log.debug("REST request all devices by vehicle with ids : {}, stato: {}", uuids, stato);

    if (uuids == null || uuids.size() < 1) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VEHICLE_IDS_REQUIRED);
    }
    if (StringUtils.isBlank(stato)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.STATO_DISPOSITIVO_REQUIRED);
    }

    HashMap<String, List<DispositivoDTO>> result = associazioneDVServiceExt.allVehicleDevicesByVehicleUuidsAndStato(uuids,
                                                                                                                    stato.toLowerCase());
    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .rows(result != null ? result.size() : 0)
                                              .uri(API_DISPOSITIVI_BY_VEHICLES)
                                              .headers())
                         .body(result);
  }

  @GetMapping(VEHICLES_BY_DISPOSITIVOID + "/{dispositivoId}")
  @Timed
  @ApiOperation(
                value = "Mostra tutte le associazioni con i veicoli dato l'id del dispositivo",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "dispositivoId is empty") })
  public ResponseEntity<List<AssociazioneDVDTO>> getVeicoliByIdDispositivo(@ApiParam(
                                                                                     value = "dispositivoId",
                                                                                     required = true) @PathVariable(
                                                                                                                    required = true) Long dispositivoId) throws Exception {
    log.debug("REST request all AssocazioneDV by vehicle dispositivoId : {}", dispositivoId);

    if (dispositivoId == null || dispositivoId <= 0) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.DISPOSITIVO_ID_REQUIRED);
    }

    List<AssociazioneDVDTO> result = associazioneDVServiceExt.findAssociazioneDVbyIdDispositivo(dispositivoId);
    return ResponseEntity.ok()
                         .headers(HeaderCustom.build()
                                              .add("X-size", result != null ? result.size() : -1)
                                              .add("X-uri", API_VEHICLES_BY_DISPOSITIVOID + "/" + dispositivoId)
                                              .headers())
                         .body(result);
  }

  @PostMapping(DISPOSITIVI_VEHICLES_COUNTER)
  @Timed
  @ApiOperation(
                value = "Mostra tutte le associazioni con i veicoli dato l'id del dispositivo",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "uuids is empty") })
  public ResponseEntity<List<AssociazioneVeicoliCounterDTO>> getAssociazioneVeicoliCounter(@RequestBody List<String> uuids) throws CustomException {
    log.debug("REST request all devices by vehicle counter");
    
    if (uuids == null || uuids.size() < 1) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED).add(Errno.PARAMETERS_REQUIRED);      
    }
    
    List<AssociazioneVeicoliCounterDTO> result = associazioneDVServiceExt.getAssociazioneVeicoliCounter(uuids);
    
    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                             .add("X-size", result != null ? result.size() : -1)
                             .add("X-uri", API_DISPOSITIVI_VEHICLES_COUNTER)
                             .headers())
        .body(result);
  }
  
  @GetMapping(VEHICLE_DISPOSITIVI_COUNTER + "/{uuidVehicle}/{uuidContratto}")
  @Timed
  @ApiOperation(
                value = "Conta dispositivi per Veicolo",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = "") })
  public ResponseEntity<NumeroDispositiviVM> countDispositiviAttiviPerVeicolo(
		  @ApiParam(value = "uuidVehicle", required = true) @PathVariable(required = true) String uuidVehicle,
		  @ApiParam(value = "uuidContratto", required = true) @PathVariable(required = true) String uuidContratto) throws CustomException {
    log.debug("REST count all devices by vehicle {} and contract {}",uuidVehicle,uuidContratto);
    
    Collection<StatoDispositivo> stati = new HashSet<>(StatoDispositivo.getGreenState());
    stati.addAll(StatoDispositivo.getYellowState());
    
    NumeroDispositiviVM count = associazioneDVServiceExt.countDispositiviPerVeicolo(uuidVehicle,uuidContratto, stati);
    
    return ResponseEntity.ok().body(count);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
