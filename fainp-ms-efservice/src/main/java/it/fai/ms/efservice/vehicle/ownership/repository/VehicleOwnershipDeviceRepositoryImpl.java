package it.fai.ms.efservice.vehicle.ownership.repository;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipDevice;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipDeviceTypeId;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipServiceTypeId;

@Service
public class VehicleOwnershipDeviceRepositoryImpl implements VehicleOwnershipDeviceRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private AssociazioneDVRepository devicesForVehicleRepository;

  @Autowired
  public VehicleOwnershipDeviceRepositoryImpl(final AssociazioneDVRepository _devicesForVehicleRepository) {
    devicesForVehicleRepository = _devicesForVehicleRepository;
  }
  
  
  @Override
  @Transactional(readOnly = true)
  public Set<VehicleOwnershipDevice> findByVehicleOwnershipId(final VehicleOwnershipId _vehicleOwnershipId, Set<TipoDispositivoEnum> tipiDispositivo) {
	  
	 final List<Dispositivo> devicesOnVehicle = devicesForVehicleRepository.findByUuidVeicoloAndTipoDispositivoNomeInAndStatoDispositivoNotIn(_vehicleOwnershipId.getId(),
			  tipiDispositivo,
	          StatoDispositivo.getBlueState());
	  
	 final Set<VehicleOwnershipDevice> vehicleOwnershipDevices = devicesOnVehicle.stream()
	          .map(device -> mapToVehicleOwnershipDeviceType(device))
	          .collect(toSet());
	_log.info("Found filtered by {}: {}", _vehicleOwnershipId, vehicleOwnershipDevices);
	return vehicleOwnershipDevices;
}

  @Override
  @Transactional(readOnly = true)
  public Set<VehicleOwnershipDevice> findByVehicleOwnershipId(final VehicleOwnershipId _vehicleOwnershipId) {
    final List<AssociazioneDV> byUuidVeicolo = devicesForVehicleRepository.findByUuidVeicolo(_vehicleOwnershipId.getId());
    final Set<VehicleOwnershipDevice> vehicleOwnershipDevices = byUuidVeicolo.stream()
                                                                             .map(device -> mapToVehicleOwnershipDeviceType(device))
                                                                             .collect(toSet());
    _log.info("Found filtered by {}: {}", _vehicleOwnershipId, vehicleOwnershipDevices);
    return vehicleOwnershipDevices;
  }
  
  private VehicleOwnershipDevice mapToVehicleOwnershipDeviceType(final AssociazioneDV _device) {
	  return mapToVehicleOwnershipDeviceType(_device.getDispositivo());
  }

  private VehicleOwnershipDevice mapToVehicleOwnershipDeviceType(final Dispositivo _device) {
    final VehicleOwnershipDevice vehicleOwnershipDevice = new VehicleOwnershipDevice(new VehicleOwnershipDeviceTypeId(_device
                                                                                                                             .getTipoDispositivo()
                                                                                                                             .getNome()
                                                                                                                             .name()));
    String state = _device
                          .getStato()
                          .name();
    boolean hasOnGoingRequest = state != null && (state.trim()
                                                       .toLowerCase()
                                                       .equals("attivo_spedito_a_fai")
                                                  || state.trim()
                                                          .toLowerCase()
                                                          .equals("attivo_in_attesa_spedizione")
                                                  || state.trim()
                                                          .toLowerCase()
                                                          .equals("accettato_da_fornitore")
                                                  || state.trim()
                                                          .toLowerCase()
                                                          .equals("iniziale"));
    vehicleOwnershipDevice.setOngoingRequest(hasOnGoingRequest);
    vehicleOwnershipDevice.getServiceTypeIds()
                          .addAll(_device
                                         .getStatoDispositivoServizios()
                                         .stream()
                                         .map(statoDispositivoServizio -> new VehicleOwnershipServiceTypeId(statoDispositivoServizio.getTipoServizio()
                                                                                                                                    .getNome()))
                                         .collect(toSet()));
    return vehicleOwnershipDevice;
  }

}
