package it.fai.ms.efservice.service;

import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import java.util.List;

/**
 * Service Interface for managing StatoDispositivoServizio.
 */
public interface StatoDispositivoServizioService {

  /**
   * Save a statoDispositivoServizio.
   *
   * @param statoDispositivoServizioDTO
   *          the entity to save
   * @return the persisted entity
   */
  StatoDispositivoServizioDTO save(StatoDispositivoServizioDTO statoDispositivoServizioDTO);

  /**
   * Get all the statoDispositivoServizios.
   *
   * @return the list of entities
   */
  List<StatoDispositivoServizioDTO> findAll();

  /**
   * Get the "id" statoDispositivoServizio.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  StatoDispositivoServizioDTO findOne(Long id);

  /**
   * Delete the "id" statoDispositivoServizio.
   *
   * @param id
   *          the id of the entity
   */
  void delete(Long id);

  StatoDispositivoServizio save(StatoDispositivoServizio sds);
}
