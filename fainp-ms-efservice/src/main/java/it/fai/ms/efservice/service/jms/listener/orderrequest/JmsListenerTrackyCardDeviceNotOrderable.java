package it.fai.ms.efservice.service.jms.listener.orderrequest;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceNotOrderableMessage;
import it.fai.ms.efservice.service.jms.consumer.DeviceNotOrderableConsumer;

@Service
@Transactional
public class JmsListenerTrackyCardDeviceNotOrderable implements MessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceNotOrderableConsumer consumer;

  @Autowired
  public JmsListenerTrackyCardDeviceNotOrderable(DeviceNotOrderableConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  public void onMessage(final Message _message) {
    DeviceNotOrderableMessage deviceNotOrderableMessage = null;
    try {
      try {
        deviceNotOrderableMessage = (DeviceNotOrderableMessage) ((ObjectMessage) _message).getObject();
        _log.info("Received jms message {}", deviceNotOrderableMessage);
        consumer.consume(deviceNotOrderableMessage);
      } catch (@SuppressWarnings("unused") final ClassCastException _e) {
        _log.warn("JMS ObjectMessage isn't {}", DeviceNotOrderableMessage.class.getSimpleName());
      }
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

}