package it.fai.ms.efservice.service.jms.listener.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceLicensePlaceChangedConsumer;

@Service
@Transactional
public class JmsQueueListenerDevicePlateChanged
  extends JmsObjectMessageListenerTemplate<DeviceEventMessage>
  implements JmsQueueListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceLicensePlaceChangedConsumer consumer;

  @Autowired
  public JmsQueueListenerDevicePlateChanged(DeviceLicensePlaceChangedConsumer _consumer) {
    consumer = _consumer;
  }

  @Override
  protected void consumeMessage(DeviceEventMessage deviceEventMessage) {
    _log.info("Received jms message {}", deviceEventMessage);
    try {
    consumer.consume(deviceEventMessage);
    } catch (Exception e) {
      _log.error("Exception on consume message DeviceLicensePlateChanged: ", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_PLATE_CHANGED;
  }
}
