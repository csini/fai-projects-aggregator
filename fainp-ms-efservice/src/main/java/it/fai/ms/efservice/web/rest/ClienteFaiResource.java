package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.ClienteFaiService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClienteFai.
 */
@RestController
@RequestMapping("/api")
public class ClienteFaiResource {

    private final Logger log = LoggerFactory.getLogger(ClienteFaiResource.class);

    private static final String ENTITY_NAME = "clienteFai";

    private final ClienteFaiService clienteFaiService;

    public ClienteFaiResource(ClienteFaiService clienteFaiService) {
        this.clienteFaiService = clienteFaiService;
    }

    /**
     * POST  /cliente-fais : Create a new clienteFai.
     *
     * @param clienteFaiDTO the clienteFaiDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clienteFaiDTO, or with status 400 (Bad Request) if the clienteFai has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cliente-fais")
    @Timed
    public ResponseEntity<ClienteFaiDTO> createClienteFai(@Valid @RequestBody ClienteFaiDTO clienteFaiDTO) throws URISyntaxException {
        log.debug("REST request to save ClienteFai : {}", clienteFaiDTO);
        if (clienteFaiDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new clienteFai cannot already have an ID")).body(null);
        }
        ClienteFaiDTO result = clienteFaiService.save(clienteFaiDTO);
        return ResponseEntity.created(new URI("/api/cliente-fais/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /cliente-fais : Updates an existing clienteFai.
     *
     * @param clienteFaiDTO the clienteFaiDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clienteFaiDTO,
     * or with status 400 (Bad Request) if the clienteFaiDTO is not valid,
     * or with status 500 (Internal Server Error) if the clienteFaiDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cliente-fais")
    @Timed
    public ResponseEntity<ClienteFaiDTO> updateClienteFai(@Valid @RequestBody ClienteFaiDTO clienteFaiDTO) throws URISyntaxException {
        log.debug("REST request to update ClienteFai : {}", clienteFaiDTO);
        if (clienteFaiDTO.getId() == null) {
            return createClienteFai(clienteFaiDTO);
        }
        ClienteFaiDTO result = clienteFaiService.save(clienteFaiDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clienteFaiDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /cliente-fais : get all the clienteFais.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clienteFais in body
     */
    @GetMapping("/cliente-fais")
    @Timed
    public List<ClienteFaiDTO> getAllClienteFais() {
        log.debug("REST request to get all ClienteFais");
        return clienteFaiService.findAll();
        }

    /**
     * GET  /cliente-fais/:id : get the "id" clienteFai.
     *
     * @param id the id of the clienteFaiDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clienteFaiDTO, or with status 404 (Not Found)
     */
    @GetMapping("/cliente-fais/{id}")
    @Timed
    public ResponseEntity<ClienteFaiDTO> getClienteFai(@PathVariable Long id) {
        log.debug("REST request to get ClienteFai : {}", id);
        ClienteFaiDTO clienteFaiDTO = clienteFaiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clienteFaiDTO));
    }

    /**
     * DELETE  /cliente-fais/:id : delete the "id" clienteFai.
     *
     * @param id the id of the clienteFaiDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cliente-fais/{id}")
    @Timed
    public ResponseEntity<Void> deleteClienteFai(@PathVariable Long id) {
        log.debug("REST request to delete ClienteFai : {}", id);
        clienteFaiService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
