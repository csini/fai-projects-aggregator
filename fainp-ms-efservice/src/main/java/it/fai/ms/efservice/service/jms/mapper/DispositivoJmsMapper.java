package it.fai.ms.efservice.service.jms.mapper;

import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.dml.efservice.dto.DispositivoDMLDTO;
import it.fai.ms.common.dml.efservice.dto.DispositivoServizioDMLDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.service.TipoServizioServiceExt;

@Service
public class DispositivoJmsMapper {
  private TipoServizioServiceExt tipoDispositiviService;

  public DispositivoJmsMapper(TipoServizioServiceExt tipoDispositiviService) {
    this.tipoDispositiviService = tipoDispositiviService;
  }

  private static Logger log = LoggerFactory.getLogger(DispositivoJmsMapper.class);

  public DispositivoDMLDTO toDispositivoDMLDTO(Dispositivo device) {

    return toDispositivoDMLDTO(device, null);
  }

  private DispositivoDMLDTO toDispositivoDMLDTO(Dispositivo device, DispositivoDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyDispositivoDMLDTO(device.getIdentificativo());
    }

    dml.setContrattoDmlUniqueIdentifier(device.getContratto() != null && device.getContratto()
                                                                               .getIdentificativo() != null ? device.getContratto()
                                                                                                                    .getIdentificativo()
                                                                                                            : null);
    dml.setDataModificaStato(device.getDataModificaStato());
    dml.setDataScadenza(device.getDataScadenza());
    dml.setDataSpedizione(device.getDataSpedizione());
    dml.setDeviceServiceDmlDTO(getStatoDispositivoServizioDmlDTO(device));

    dml.setOperazioniDisponibili(device.getOperazioniPossibili());
    dml.setSeriale(device.getSeriale());
    dml.setStato(device.getStato()
                       .name());
    dml.setStatoColore(device.getStato()
                             .isGreen() ? DispositivoDMLDTO.StatoColore.ATTIVO
                                        : device.getStato()
                                                .isYellow() ? DispositivoDMLDTO.StatoColore.IN_ATTIVAZIONE
                                                            : DispositivoDMLDTO.StatoColore.NON_ATTIVO);

    dml.setTarga2(device.getTarga2());
    dml.setTarga3(device.getTarga3());
    dml.setTargaDataValidita(device.getTargaDataValidita());
    dml.setTipoDispositivo(device.getTipoDispositivo() != null && device.getTipoDispositivo()
                                                                        .getNome() != null ? device.getTipoDispositivo()
                                                                                                   .getNome()
                                                                                                   .name()
                                                                                           : null);
    dml.setTipoHardware(device.getTipoHardware());
    dml.setVehicleUuid(getVehicleUuid(device));
    dml.setSerialeSostituito(device.getSerialeSostituito());
    
    dml.setNoteOperatore(device.getNoteOperatore());
    dml.setNoteTecniche(device.getNoteTecniche());
    dml.setTipoHardware(device.getTipoHardware());
    
    dml.setDataPrimaAttivazione(device.getDataPrimaAttivazione());

    if (log.isTraceEnabled())
      log.trace("DML: {}", dml);
    return dml;
  }

  private String getVehicleUuid(Dispositivo device) {
    String uuidVehicle = null;
    Set<AssociazioneDV> associazioneDispositivoVeicolos = device.getAssociazioneDispositivoVeicolos();
    if (associazioneDispositivoVeicolos == null || associazioneDispositivoVeicolos.isEmpty()) {
      log.warn("Not found AssociazioneDV to device: {}", device.getIdentificativo());
    } else {
      Optional<AssociazioneDV> optAssociazioneDV = associazioneDispositivoVeicolos.stream()
                                                                                  .findFirst();
      if (optAssociazioneDV.isPresent()) {
        AssociazioneDV associazioneDV = optAssociazioneDV.get();
        log.debug("AssociazioneDV: {}", associazioneDV);
        uuidVehicle = associazioneDV.getUuidVeicolo();
      } else {
        log.warn("Not found AssociazioneDV to device: {}", device.getIdentificativo());
      }
    }
    return uuidVehicle;
  }

  public DispositivoDMLDTO toEmptyDispositivoDMLDTO(String identificativo) {
    DispositivoDMLDTO dml = new DispositivoDMLDTO();

    dml.setDmlUniqueIdentifier(identificativo);
    dml.setDmlRevisionTimestamp(Instant.now());

    return dml;
  }

  private Set<DispositivoServizioDMLDTO> getStatoDispositivoServizioDmlDTO(Dispositivo device) {
    Set<StatoDispositivoServizio> statoDispositivoServizios = device.getStatoDispositivoServizios();
    if (statoDispositivoServizios == null) {
      log.warn("Not found StatoDispositivoServizio from device: {}", device);
      // Impossibile che non ci sia almeno uno stato dispositivo servizio.
      return null;
    }

    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    if (tipoDispositivo == null) {
      log.warn("Not found TipoDispositivo from device: {}", device);
      return null;
    }

    Set<TipoServizio> avaiableTipoServizios = tipoDispositiviService.getAvaiableByTipoDispositivo(device.getTipoDispositivo(),
                                                                                          device.getTipoHardware());

    if (avaiableTipoServizios == null) {
      log.warn("Not found TipoServizios from device: {} and device type: {}", device, tipoDispositivo);
      return null;
    }

    if (avaiableTipoServizios.isEmpty()) {
      log.warn("TipoServizios is EMPTY from device: {} and device type: {}", device, tipoDispositivo);
      return null;
    }

    Set<DispositivoServizioDMLDTO> deviceServiceDml = new HashSet<>();
    Set<TipoServizio> tipiServiziPresent = new HashSet<>();
    Set<DispositivoServizioDMLDTO> serviceDeviceActiveAndPending = statoDispositivoServizios.stream()
                                                                                            .map(sds -> {
                                                                                              TipoServizio ts = sds.getTipoServizio();
                                                                                              boolean containsService = avaiableTipoServizios.contains(ts);
                                                                                              if (containsService) {
                                                                                                log.debug("TipoServizio {} add to Service type List already present on device {}",
                                                                                                          ts, device);
                                                                                                tipiServiziPresent.add(ts);
                                                                                              }

                                                                                              return createDeviceServiceDmlDTO(device, sds,
                                                                                                                               ts);

                                                                                            })
                                                                                            .collect(toSet());
    if (log.isTraceEnabled())
      log.trace("Found service device ACTIVE and PENDING: {}",
                (serviceDeviceActiveAndPending != null) ? serviceDeviceActiveAndPending.size() : 0);
    deviceServiceDml.addAll(serviceDeviceActiveAndPending);

    Set<DispositivoServizioDMLDTO> serviceDeviceNotActive = avaiableTipoServizios.stream()
                                                                         .filter(ts -> !tipiServiziPresent.contains(ts))
                                                                         .map(ts -> {
                                                                           return createDeviceServiceDmlDTO(device, ts);
                                                                         })
                                                                         .collect(toSet());
    if (log.isTraceEnabled())
      log.trace("Found service device NOT ACTIVE: {}", (serviceDeviceNotActive != null) ? serviceDeviceNotActive.size() : 0);
    deviceServiceDml.addAll(serviceDeviceNotActive);
    if (log.isTraceEnabled())
      log.trace("DeviceServiceDmlDTO: {}", deviceServiceDml);
    return deviceServiceDml;
  }

  private DispositivoServizioDMLDTO createDeviceServiceDmlDTO(Dispositivo device, StatoDispositivoServizio sds, TipoServizio ts) {
    DispositivoServizioDMLDTO dto = new DispositivoServizioDMLDTO();
    dto.setTipoServizio(ts.getNome());
    dto.setDispositivoUuid(device.getIdentificativo());
    dto.setDmlRevisionTimestamp(Instant.now());
    StatoDS stato = sds.getStato();
    setOperazioniPossibili(dto, stato);
    dto.setStato(stato.name());
    dto.setDataInizio(sds.getDataAttivazione());
    dto.setDataFine(sds.getDataDisattivazione());
    dto.setPan(sds.getPan());
    if (log.isTraceEnabled())
      log.trace("DispositivoServizioDMLDTO: {}", dto);
    return dto;
  }

  private void setOperazioniPossibili(DispositivoServizioDMLDTO dto, StatoDS stato) {
    if (stato != null) {
      switch (stato) {
      case ATTIVO:
        dto.setOperazioniPossibili("DISATTIVA");
        break;
      case NON_ATTIVO:
        dto.setOperazioniPossibili("ATTIVA");
        break;

      default:
        log.debug("Stato [{}] is pending...", stato);
        break;
      }
    } else {
      dto.setOperazioniPossibili("ATTIVA");
    }
  }

  private DispositivoServizioDMLDTO createDeviceServiceDmlDTO(Dispositivo device, TipoServizio ts) {
    DispositivoServizioDMLDTO dto = new DispositivoServizioDMLDTO();
    dto.setTipoServizio(ts.getNome());
    dto.setDispositivoUuid(device.getIdentificativo());
    dto.setDmlRevisionTimestamp(Instant.now());
    setOperazioniPossibili(dto, null);
    dto.setStato(StatoDS.NON_ATTIVO.name());

    if (log.isTraceEnabled())
      log.trace("DispositivoServizioDMLDTO: {}", dto);
    return dto;
  }

}
