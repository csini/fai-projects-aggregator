package it.fai.ms.efservice.service.jms;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.DispositiviDaSpedireDTO;
import it.fai.ms.common.jms.dto.DispositivoFSMChangeStatusDTO;
import it.fai.ms.common.jms.dto.MezzoRitargatoDTO;
import it.fai.ms.common.jms.dto.RichiestaNewDispositivoDTO;
import it.fai.ms.common.jms.dto.notification.LogOperazioniPublishDTO;
import it.fai.ms.common.jms.dto.telepass.*;
import it.fai.ms.common.jms.efservice.CompanyCodeUuid;
import it.fai.ms.common.jms.efservice.message.contract.AbstractCreateContractMessage;
import it.fai.ms.common.jms.efservice.message.contract.ContractCreationMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderingMessage;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.fsm.bean.telepass.TelepassEuEventiOutboundDTO;
import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import it.fai.ms.integration.system.sock.api.spi.FaiMessageSystemProvider;
import it.fai.ms.integration.system.sock.config.FaiMessageSystemLoader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.io.Serializable;

@Service
@Transactional
public class JmsTopicSenderService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties           jmsProperties;

  public void setOrdineClienteServiceExt(OrdineClienteServiceExt ordineClienteServiceExt) {
    this.ordineClienteServiceExt = ordineClienteServiceExt;
  }

  private OrdineClienteServiceExt ordineClienteServiceExt;

  public JmsTopicSenderService(final JmsProperties applicationProperties,
                               final OrdineClienteServiceExt ordineClienteServiceExt) throws Exception {
    this.jmsProperties = applicationProperties;
     this.ordineClienteServiceExt = ordineClienteServiceExt;

     FaiMessageSystemLoader fml = new FaiMessageSystemLoader();
     faiMessageSystem = fml.provider();

  }

    public JmsTopicSenderService() {
    jmsProperties = null;
    ordineClienteServiceExt = null;
    FaiMessageSystemLoader fml = new FaiMessageSystemLoader();
    faiMessageSystem = fml.provider();
    }

  public void publishLogTopic(LogOperazioniPublishDTO logOperazioniPublishDTO) throws Exception {
    publishMessage(JmsTopicNames.LOG_OPERAZIONI_TOPIC, logOperazioniPublishDTO);
  }

  public void publishTelepassEuEventiOutboundDTO(TelepassEuEventiOutboundDTO telepassEuEventiOutDTO) throws Exception {

    if (StringUtils.isBlank(telepassEuEventiOutDTO.getCodiceClienteFai())) {
      final ClienteFai customer = getClienteFaiByIdentificativoOrdineCliente(telepassEuEventiOutDTO.getIdentificativoOrdineCliente());
      telepassEuEventiOutDTO.setCodiceClienteFai(customer.getCodiceCliente());
    }

    if (telepassEuEventiOutDTO.getTipoEventoOutbound() == TipoEventoOutbound.INOLTRO_RICHIEDI_CONTRATTO) {
      final AbstractCreateContractMessage contractCreationMessage = new ContractCreationMessage(new CompanyCodeUuid(telepassEuEventiOutDTO.getCodiceClienteFai()));
      publishMessage(JmsTopicNames.CONTRACT_CREATION, contractCreationMessage);
    } else {
      final DeviceOrderingMessage deviceOrderingMessage = new DeviceOrderingMessage(new OrderRequestUuid(telepassEuEventiOutDTO.getIdentificativoRichiesta()));
      publishMessage(JmsTopicNames.DEVICE_ORDERING, deviceOrderingMessage);
    }
  }

  public void publishRipetiInvioMessage(RichiestaDTO richiestaDTO) throws Exception {
    TelepassEuRipetiInvioInboundDTO dto = null;
    ClienteFai clienteFai = getClienteFaiByRichiestaDTO(richiestaDTO);
    dto = new TelepassEuRipetiInvioInboundDTO().codiceCliente(clienteFai.getCodiceCliente());
    publishMessage(JmsTopicNames.TELEPASS_EU_EVENTI_INBOUND, dto);
  }

  public void publishFsmCommandDispositivoMessage(DispositivoFSMChangeStatusDTO dispositivoFsmDTO) throws Exception {
    publishMessage(JmsTopicNames.FSM_DISPOSITIVI, dispositivoFsmDTO);
  }

  @Deprecated // Use publishRequestNewDispositivo
  public void publishTelepassEuNuovoDispositivoMessage(TelepassEuNuovoDispositivoDTO telepassEuNuovoDispositivoDTO) throws Exception {
    publishMessage(JmsTopicNames.TELEPASS_EU_NUOVO_DISPOSITIVO, telepassEuNuovoDispositivoDTO);
  }

  public void publishMezzoRitargatoMessage(MezzoRitargatoDTO mezzoRitargatoDTO) throws Exception {
    publishMessage(JmsTopicNames.VEHICLE, mezzoRitargatoDTO);
  }

  public void publishDenunciaAllineamentoMessage(TelepassEuDenunciaAllegatoDTO telepassEuDenunciaAllegatoDTO) throws Exception {
    publishMessage(JmsQueueNames.TELEPASS_EU_EVENTI_ALLEGATO_DENUNCIA, telepassEuDenunciaAllegatoDTO);
  }

  public void publishGiustificativoMessage(TelepassEuGiustificativoAggiuntoDTO telepassEuGiustificativoAggiuntoDTO) {
    publishMessage(JmsQueueNames.TELEPASS_EU_EVENTI_ALLEGATO_GIUSTIFICATIVO, telepassEuGiustificativoAggiuntoDTO);
  }

  public void publishDispositiviDaSpedireMessage(DispositiviDaSpedireDTO dispositiviDaSpedireDTO) throws Exception {
    publishMessage(JmsTopicNames.SPEDIZIONE_DISPOSITIVI, dispositiviDaSpedireDTO);
  }

  public void publishRequestNewDispositivoMessage(RichiestaNewDispositivoDTO richiestaNewDispositivoDTO) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
    publishMessage(JmsTopicNames.REQUEST_NEW_DEVICE, richiestaNewDispositivoDTO);
  }

  private ClienteFai getClienteFaiByRichiestaDTO(RichiestaDTO richiestaDTO) {
    Long ordineClienteId = richiestaDTO.getOrdineClienteId();
    OrdineCliente ordineCliente = ordineClienteServiceExt.loadClienteAssegnatarioOnOrdineCliente(ordineClienteId);
    ClienteFai clienteFai = ordineCliente.getClienteAssegnatario();
    return clienteFai;
  }

  private ClienteFai getClienteFaiByIdentificativoOrdineCliente(String identificativoOrdineCliente) {
    OrdineCliente ordineCliente = ordineClienteServiceExt.findByIdentificativoOrdineCliente(identificativoOrdineCliente);
    ClienteFai clienteFai = ordineCliente.getClienteAssegnatario();
    return clienteFai;
  }

  public void setFaiMessageSystem(FaiMessageSystem faiMessageSystem) {
    this.faiMessageSystem = faiMessageSystem;
  }

  //@Inject
  FaiMessageSystem faiMessageSystem;
  private void publishMessage(JmsTopicNames topic, Serializable message) throws IllegalAccessException, ClassNotFoundException, InstantiationException {
    log.debug("I'll send on {} the following {}: {}", topic.name(), message.getClass(), message.toString());
    JmsTopicSenderUtil.publish(jmsProperties, topic, message);
    //noinspection NamedArgsPositionMismatch
    /*
    FaiMessageSystemLoader fml = new FaiMessageSystemLoader();
    faiMessageSystem = fml.provider();
    Assert.notNull(faiMessageSystem);
    faiMessageSystem.publishMessage(topic.name(), message);
    */
    log.info("JMS message for {} was sent!", message.getClass());


  }

  private void publishMessage(JmsQueueNames queue, Serializable message) {
	    log.debug("I'll send on {} the following {}: {}", queue.name(), message.getClass(), message.toString());
	    //JmsQueueSenderUtil.publish(jmsProperties, queue, message);
	    log.info("JMS message for {} was sent!", message.getClass());
	  }

}
