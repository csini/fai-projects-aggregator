package it.fai.ms.efservice.wizard.model.matrix.step5;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixVehicle;

public class WizardStep5MatrixVehicle extends WizardMatrixVehicle {

  private static final long serialVersionUID = -4081082925871998036L;

  private Map<String, WizardStep5MatrixServiceType> serviceTypes = new HashMap<>();
  private String                                    type;

  public WizardStep5MatrixVehicle(final String _id) {
    super(_id);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep5MatrixVehicle) _obj).getServiceTypes(), serviceTypes)
                 && Objects.equals(((WizardStep5MatrixVehicle) _obj).getEuroClass(), getEuroClass())
                 && Objects.equals(((WizardStep5MatrixVehicle) _obj).getLicensePlate(), getLicensePlate())
                 && Objects.equals(((WizardStep5MatrixVehicle) _obj).getUuid(), getUuid())
                 && Objects.equals(((WizardStep5MatrixVehicle) _obj).getType(), type);
    }
    return isEquals;
  }

  public Map<String, WizardStep5MatrixServiceType> getServiceTypes() {
    return this.serviceTypes;
  }

  public String getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceTypes, type, super.hashCode());
  }

  public void setServiceTypes(final Map<String, WizardStep5MatrixServiceType> _serviceTypes) {
    serviceTypes = _serviceTypes;
  }

  public void setType(final String _type) {
    type = _type;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Step5WizardMatrixVehicle [serviceTypes=");
    builder.append(this.serviceTypes);
    builder.append(", type=");
    builder.append(this.type);
    builder.append(", getServiceTypes()=");
    builder.append(this.getServiceTypes());
    builder.append(", getType()=");
    builder.append(this.getType());
    builder.append("]");
    return builder.toString();
  }

}
