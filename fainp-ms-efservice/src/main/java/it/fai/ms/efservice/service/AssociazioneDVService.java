package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import java.util.List;

/**
 * Service Interface for managing AssociazioneDV.
 */
public interface AssociazioneDVService {

    /**
     * Save a associazioneDV.
     *
     * @param associazioneDVDTO the entity to save
     * @return the persisted entity
     */
    AssociazioneDVDTO save(AssociazioneDVDTO associazioneDVDTO);

    /**
     *  Get all the associazioneDVS.
     *
     *  @return the list of entities
     */
    List<AssociazioneDVDTO> findAll();

    /**
     *  Get the "id" associazioneDV.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AssociazioneDVDTO findOne(Long id);

    /**
     *  Delete the "id" associazioneDV.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
