package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.efservice.dto.ContrattoDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlSender;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.jms.mapper.ContrattoDmlSenderMapper;

public class ContrattoDmlSenderUtil
  extends AbstractMappableDmlSender<ContrattoDMLDTO, Contratto> {

  static final JmsTopicNames TOPIC_SAVE = JmsTopicNames.DML_CONTRATTI_SAVE;
  static final JmsTopicNames TOPIC_DELETE = JmsTopicNames.DML_CONTRATTI_DELETE;

  public ContrattoDmlSenderUtil(JmsProperties jmsProperties, ContrattoDmlSenderMapper contrattoJmsMapper) {
    super(jmsProperties, contrattoJmsMapper);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

}
