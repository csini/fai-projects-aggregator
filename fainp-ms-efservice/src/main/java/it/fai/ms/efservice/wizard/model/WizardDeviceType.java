package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardDeviceType implements Serializable, Comparable<WizardDeviceType> {

  private static final long serialVersionUID = 3379254720752869872L;

  private WizardDeviceTypeId id;
  private boolean            multiService;
  private Integer            order  = 0;
  private boolean            hidden = false;
  private Boolean            isToBeSent;

  public WizardDeviceType(final WizardDeviceTypeId _id) {
    Objects.requireNonNull(_id, "WizardDeviceTypeId is mandatory");
    id = _id;
  }

  public WizardDeviceType(final WizardDeviceTypeId _id, final boolean _multiService) {
    Objects.requireNonNull(_id, "WizardDeviceTypeId is mandatory");
    id = _id;
    multiService = _multiService;
  }

  @Override
  public int compareTo(final WizardDeviceType _wizardDeviceType) {
    int compareTo = 0;
    if (!this.equals(_wizardDeviceType)) {
      if (order.equals(_wizardDeviceType.getOrder())) {
        compareTo = id.getId()
                      .compareTo(_wizardDeviceType.getId()
                                                  .getId());
      } else {
        compareTo = order.compareTo(_wizardDeviceType.getOrder());
      }
    }
    return compareTo;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((WizardDeviceType) _obj).getId(), id) && Objects.equals(((WizardDeviceType) _obj).getOrder(), order);
    }
    return isEquals;
  }

  public WizardDeviceTypeId getId() {
    return id;
  }

  public Integer getOrder() {
    return order;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, order);
  }

  public boolean isMultiService() {
    return multiService;
  }

  public void setMultiService(final boolean _multiService) {
    multiService = _multiService;
  }

  public void setOrder(final Integer _order) {
    order = _order;
  }

  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean _hidden) {
    hidden = _hidden;
  }

  public Boolean isToBeSent() {
    return isToBeSent;
  }

  public void setIsToBeSent(Boolean _isToBeSent) {
    isToBeSent = _isToBeSent;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName())
           .append(" [id=")
           .append(id)
           .append(",multiService=")
           .append(multiService)
           .append(",order=")
           .append(order)
           .append(",hidden=")
           .append(hidden)
           .append(",isToBeSent=")
           .append(isToBeSent)
           .append("]");
    return builder.toString();
  }
}
