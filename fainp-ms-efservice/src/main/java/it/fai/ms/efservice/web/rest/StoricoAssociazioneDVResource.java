package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.StoricoAssociazioneDVService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.StoricoAssociazioneDVDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StoricoAssociazioneDV.
 */
@RestController
@RequestMapping("/api")
public class StoricoAssociazioneDVResource {

    private final Logger log = LoggerFactory.getLogger(StoricoAssociazioneDVResource.class);

    private static final String ENTITY_NAME = "storicoAssociazioneDV";

    private final StoricoAssociazioneDVService storicoAssociazioneDVService;

    public StoricoAssociazioneDVResource(StoricoAssociazioneDVService storicoAssociazioneDVService) {
        this.storicoAssociazioneDVService = storicoAssociazioneDVService;
    }

    /**
     * POST  /storico-associazione-dvs : Create a new storicoAssociazioneDV.
     *
     * @param storicoAssociazioneDVDTO the storicoAssociazioneDVDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new storicoAssociazioneDVDTO, or with status 400 (Bad Request) if the storicoAssociazioneDV has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/storico-associazione-dvs")
    @Timed
    public ResponseEntity<StoricoAssociazioneDVDTO> createStoricoAssociazioneDV(@RequestBody StoricoAssociazioneDVDTO storicoAssociazioneDVDTO) throws URISyntaxException {
        log.debug("REST request to save StoricoAssociazioneDV : {}", storicoAssociazioneDVDTO);
        if (storicoAssociazioneDVDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new storicoAssociazioneDV cannot already have an ID")).body(null);
        }
        StoricoAssociazioneDVDTO result = storicoAssociazioneDVService.save(storicoAssociazioneDVDTO);
        return ResponseEntity.created(new URI("/api/storico-associazione-dvs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /storico-associazione-dvs : Updates an existing storicoAssociazioneDV.
     *
     * @param storicoAssociazioneDVDTO the storicoAssociazioneDVDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated storicoAssociazioneDVDTO,
     * or with status 400 (Bad Request) if the storicoAssociazioneDVDTO is not valid,
     * or with status 500 (Internal Server Error) if the storicoAssociazioneDVDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/storico-associazione-dvs")
    @Timed
    public ResponseEntity<StoricoAssociazioneDVDTO> updateStoricoAssociazioneDV(@RequestBody StoricoAssociazioneDVDTO storicoAssociazioneDVDTO) throws URISyntaxException {
        log.debug("REST request to update StoricoAssociazioneDV : {}", storicoAssociazioneDVDTO);
        if (storicoAssociazioneDVDTO.getId() == null) {
            return createStoricoAssociazioneDV(storicoAssociazioneDVDTO);
        }
        StoricoAssociazioneDVDTO result = storicoAssociazioneDVService.save(storicoAssociazioneDVDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, storicoAssociazioneDVDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /storico-associazione-dvs : get all the storicoAssociazioneDVS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of storicoAssociazioneDVS in body
     */
    @GetMapping("/storico-associazione-dvs")
    @Timed
    public List<StoricoAssociazioneDVDTO> getAllStoricoAssociazioneDVS() {
        log.debug("REST request to get all StoricoAssociazioneDVS");
        return storicoAssociazioneDVService.findAll();
        }

    /**
     * GET  /storico-associazione-dvs/:id : get the "id" storicoAssociazioneDV.
     *
     * @param id the id of the storicoAssociazioneDVDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storicoAssociazioneDVDTO, or with status 404 (Not Found)
     */
    @GetMapping("/storico-associazione-dvs/{id}")
    @Timed
    public ResponseEntity<StoricoAssociazioneDVDTO> getStoricoAssociazioneDV(@PathVariable Long id) {
        log.debug("REST request to get StoricoAssociazioneDV : {}", id);
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = storicoAssociazioneDVService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storicoAssociazioneDVDTO));
    }

    /**
     * DELETE  /storico-associazione-dvs/:id : delete the "id" storicoAssociazioneDV.
     *
     * @param id the id of the storicoAssociazioneDVDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/storico-associazione-dvs/{id}")
    @Timed
    public ResponseEntity<Void> deleteStoricoAssociazioneDV(@PathVariable Long id) {
        log.debug("REST request to delete StoricoAssociazioneDV : {}", id);
        storicoAssociazioneDVService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
