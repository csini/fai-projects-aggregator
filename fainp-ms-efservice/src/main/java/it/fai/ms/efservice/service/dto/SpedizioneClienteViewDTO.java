package it.fai.ms.efservice.service.dto;

import it.fai.ms.efservice.web.rest.errors.PayloadError;

import java.util.ArrayList;
import java.util.List;

public class SpedizioneClienteViewDTO {

  private Long id;

  private String codiceAzienda;

  private String ragioneSociale;

  private Long numeroDispositiviSpedibili = 0L;

  private Long numeroDispositiviDisponibili = 0L;

  private Long numeroDispositiviDaRicevere = 0L;

  private List<PayloadError.Error> anomalieDispositivi = new ArrayList<>();

  private List<PayloadError.Error> anomalieCRM = new ArrayList<>();

  private Boolean disabled = false;

  private Boolean checked = true;

  public void addAnomaliaDispositivo(PayloadError.Error error){
    if(error == null) return;
    anomalieDispositivi.add(error);
  };

  public void addAllAnomalia(List<PayloadError.Error> error){
    if(error == null || error.isEmpty()) return;
    anomalieDispositivi.addAll(error);
  };

  public void addAllAnomaliaCRM(List<PayloadError.Error> error){
    if(error == null || error.isEmpty()) return;
    anomalieCRM.addAll(error);
  };

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  public Long getNumeroDispositiviDisponibili() {
    return numeroDispositiviDisponibili;
  }

  public void setNumeroDispositiviDisponibili(Long numeroDispositiviDisponibili) {
    this.numeroDispositiviDisponibili = numeroDispositiviDisponibili;
  }

  public Long getNumeroDispositiviDaRicevere() {
    return numeroDispositiviDaRicevere;
  }

  public void setNumeroDispositiviDaRicevere(Long numeroDispositiviDaRicevere) {
    this.numeroDispositiviDaRicevere = numeroDispositiviDaRicevere;
  }

  public List<PayloadError.Error> getAnomalieDispositivi() {
    return anomalieDispositivi;
  }

  public void setAnomalieDispositivi(List<PayloadError.Error> anomalieDispositivi) {
    this.anomalieDispositivi = anomalieDispositivi;
  }

  public List<PayloadError.Error> getAnomalieCRM() {
    return anomalieCRM;
  }

  public void setAnomalieCRM(List<PayloadError.Error> anomalieCRM) {
    this.anomalieCRM = anomalieCRM;
  }

  public Boolean getDisabled() {
    return disabled;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public Boolean getChecked() {
    return checked;
  }

  public void setChecked(Boolean checked) {
    this.checked = checked;
  }

  @Override
  public String toString() {
    return "SpedizioneClienteViewDTO{" +
      "id=" + id +
      ", codiceAzienda='" + codiceAzienda + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", numeroDispositiviSpedibili=" + numeroDispositiviSpedibili +
      ", numeroDispositiviDisponibili=" + numeroDispositiviDisponibili +
      ", numeroDispositiviDaRicevere=" + numeroDispositiviDaRicevere +
      ", anomalieDispositivi=" + anomalieDispositivi +
      ", anomalieCRM=" + anomalieCRM +
      ", disabled=" + disabled +
      ", checked=" + checked +
      '}';
  }
}
