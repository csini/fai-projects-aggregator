package it.fai.ms.efservice.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.ClienteFai_;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.OrdineCliente_;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

@Repository
public interface OrdineClienteRepositoryExt extends JpaRepository<OrdineCliente, Long>, JpaSpecificationExecutor<OrdineCliente> {

  @Query("select oc from OrdineCliente oc where oc.identificativo = :identificativo")
  OrdineCliente findByIdentificativoOrdine(@Param("identificativo") String identificativoOrdine);

  @Query("select oc from OrdineCliente oc where oc.clienteAssegnatario.codiceCliente = :identificativo")
  List<OrdineCliente> findByIdentificativoCliente(@Param("identificativo") String identificativoCliente);

  @Query("SELECT r FROM Richiesta r INNER JOIN r.ordineCliente oc WHERE oc.identificativo = :identificativo")
  Set<Richiesta> findRichieste(@Param("identificativo") String identificativo);

  @Query("select new it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO(oc.id, oc.identificativo) from OrdineCliente oc where oc.stato != :stato")
  Set<OrdineClienteCacheDTO> findFilteredByStatoNotIs(@Param("stato") StatoOrdineCliente stato);

  @Query("select new it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO(oc.id, oc.identificativo) from OrdineCliente oc where oc.stato != :stato and oc.id not in :ids")
  Set<OrdineClienteCacheDTO> findFilteredByStatoNotIsAndNotInCache(@Param("stato") StatoOrdineCliente stato, @Param("ids") List<Long> ids);

  static Specification<OrdineCliente> ofAzienda(String codiceAzienda) {
    return (root, query, cb) -> cb.equal(root.get(OrdineCliente_.clienteAssegnatario)
                                             .get(ClienteFai_.codiceCliente),
                                         codiceAzienda);
  }

  @Query("select new it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO(oc.id, oc.identificativo) from OrdineCliente oc where oc.stato != :stato")
  List<OrdineClienteCacheDTO> findByStatoNotIs(@Param("stato") StatoOrdineCliente stato);

}
