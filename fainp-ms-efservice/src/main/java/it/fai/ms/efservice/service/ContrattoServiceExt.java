package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

/**
 * Service Interface for managing Contratto.
 */
public interface ContrattoServiceExt {

  Contratto createContrattoByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai);

  Optional<Contratto> findByIdentificativo(String identificativo);

  List<Contratto> findAll();

  List<ContrattoDTO> findAllByVehicleUuid(String uuidVeicolo);

  List<Contratto> findByCodiceContratto(String codiceContratto);

  List<Contratto> findByCodiceCliente(String codiceCliente);

  Optional<Contratto> findByCodiceContrattoAndNomeProduttore(String codiceContratto, String nomeProduttore);

  Optional<Contratto> findContrattoByProduttoreAndCodiceAzienda(Produttore produttore, String codiceAzienda);

  Contratto findContrattoByServiceNameAndCodiceAzienda(String nomeServizio, String codiceAzienda);

  List<Contratto> findContrattoByCodiceAziendaAndTipoDispositivo(String codiceAzienda, TipoDispositivoEnum tipoDispositivoEnum);

  Contratto save(Contratto contratto);

  ContrattoDTO updateCodiceContratto(String Identifier, String newCode);

  boolean executeFsm(Map<String, String> mapCodContrattoProduttore, String operazione, String nota);

  Contratto fsmChangeStatus(Contratto contract, ContrattoEvent command) throws Exception;

  Contratto setOperazioniPossibiliOnContratto(Contratto contract);

  boolean checkContrattoByCodiceClienteAndTipoDispositivo(String codiceCliente, TipoDispositivoEnum tipoDispositivo);

  void delete(Contratto contratto);

  boolean isAvailableCommandFsm(Contratto contract, ContrattoEvent event);

//  Boolean existsByCodContrattoCliente(String newCode);

  boolean existsByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai);

  boolean existsCodContrattoCliente(String codContrattoCliente, Long produttoreId);

  boolean existsCodContrattoClienteOnDifferentClienteFai(Long myClienteFaiId, String codContrattoCliente, Long produttoreId);

  List<Contratto> contractsNotFound(List<String> contracts);

  List<Contratto> findAllByProduttoreAndCodiceAzienda(Produttore produttore, String codiceCliente);

  void setPrimarioFalse(List<Contratto> contracts);

  List<Contratto> findAllByProduttoreAndCodiceClienteAndStatusIn(String nomeProduttore, String codiceCliente,
                                                                 List<StatoContratto> statusContract);

}
