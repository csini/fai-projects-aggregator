package it.fai.ms.efservice.wizard.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5Matrix;

public interface Step5Service {

  List<WizardStep5Matrix> buildMatrixList(Set<Map<String, Set<String>>> deviceTypesMaps);

}
