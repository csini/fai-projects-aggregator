package it.fai.ms.efservice.web.rest;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.dml.AllineamentoDMLDTO.EntityName;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.jms.dml.DispositivoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;

/**
 * REST controller for managing bulk dml message.
 */
@RestController
@RequestMapping(DmlBulkAllineamentoWarmdataResource.BASE_PATH)
@Transactional
public class DmlBulkAllineamentoWarmdataResource {

  private final Logger log = LoggerFactory.getLogger(DmlBulkAllineamentoWarmdataResource.class);

  public static final String BASE_PATH = "/api/dmlbulkwarmdata";

  public static final String ALLINEADISPOSITIVISUWARMDATA     = "/allineaDispositivi";

  private final JmsProperties jmsProperties;

  private final DispositivoRepositoryExt               dispositivoRepository;

  private final DispositivoJmsMapper dispositivoJmsMapper;
  
  public DmlBulkAllineamentoWarmdataResource(JmsProperties jmsProperties, 
		  				 DispositivoRepositoryExt dispositivoRepository,
                         DispositivoJmsMapper dispositivoJmsMapper) {

    this.jmsProperties = jmsProperties;
    this.dispositivoRepository = dispositivoRepository;
    this.dispositivoJmsMapper = dispositivoJmsMapper;
  }

  @PostMapping("dispositivi/{id}/allineamento")
  @Timed
  @Async
  public void allineaDispostivo(@PathVariable Long id, @RequestParam(required = true, name = "uuid")String uuid) {
    
    Dispositivo dispositivo = this.dispositivoRepository.findOne(id);
    String noteTecniche = "ALIGN-" + uuid;
    dispositivo.setNoteTecniche(noteTecniche);
    log.info("Allineamento dispositivo {} - {}", dispositivo.getId(), dispositivo.getSeriale());
    this.dispositivoRepository.save(dispositivo);
                                                        
  }
  
  @PostMapping(ALLINEADISPOSITIVISUWARMDATA)
  @Timed
  @Async
  public void allineaDispostivoSuWarmdataSenzaStorico(@RequestParam(required = true, name = "pageStart", defaultValue = "0") int pageStart,
                                @RequestParam(required = true, name = "pageTo", defaultValue = "0") int pageTo,
                                @RequestParam(required = true, name = "pageSize", defaultValue = "1000") int pageSize,
                                @RequestParam(required = true, name = "skipStorico", defaultValue = "true") boolean skipStorico,
                      		  	@RequestParam(required = true, name = "statoDispositivo") List<StatoDispositivo> statiDispositivo) {
	  
	if(statiDispositivo==null || statiDispositivo.isEmpty()) {
		  throw new IllegalArgumentException("statiDispositivo cannot be empty");
	}

    log.info("REST request to send bulk dml message for Dispositivi: pageStart {}, pageTo={}, pageSize={}, statiDispositivo={}", pageStart, pageTo, pageSize, statiDispositivo);

    try (DispositivoDmlSenderUtil sender = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)) {

      Pageable pageable = new PageRequest(pageStart, pageSize);
      while (pageable != null) {

        // se pageTo è zero o è maggiore della paggina attuale -> exit
        if (pageTo > 0 && pageable.getPageNumber() >= pageTo) {
          log.info("Dispositivo DML Bulk finish for pageTo limit: currentPage {} pageStart {}, pageTo={}, pageSize={}",
                   pageable.getPageNumber(), pageStart, pageTo, pageSize);
          return;
        }

        log.info("Elaborazione pagina {} : pageStart {}, pageTo={}, pageSize={}", pageable.getPageNumber(), pageStart, pageTo, pageSize);
        Page<Dispositivo> page = dispositivoRepository.findByStateIn(statiDispositivo, pageable);
        log.info("Found {} dispositivi on {}", page.getSize(),page.getTotalElements());
        String data = Objects.toString(page.getContent().stream().map( d -> {
          return StringUtils.join(d.getId(), d.getSeriale(),'-');
        }).collect(Collectors.toList()));
        log.info("Update {}", data);
        for (Dispositivo dispositivo : page) { 
          if ( skipStorico ) {
            sender.sendAllineamentoWarmdataNotification(dispositivo, EntityName.DISPOSITIVO);
          }else {
            String noteTecniche = "ALIGN-ES";
            dispositivo.setNoteTecniche(noteTecniche);
            this.dispositivoRepository.save(dispositivo);
          }
         
        }

        pageable = page.hasNext() ? page.nextPageable() : null;
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
    log.info("Dispositivo DML Bulk finish: pageStart {}, pageTo={}, pageSize={}", pageStart, pageTo, pageSize);
  }
}
