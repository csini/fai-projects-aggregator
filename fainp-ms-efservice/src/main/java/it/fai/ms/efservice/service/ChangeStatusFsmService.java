package it.fai.ms.efservice.service;

import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.dto.FsmContrattoDTO;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.dto.FsmRichiesteDTO;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmResetService;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;

@Service
@Transactional
public class ChangeStatusFsmService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils                fsmRichiestaUtil;
  private final ChangeStatusOrdineClienteService changeStatusOrdineService;
  private final ContrattoServiceExt              contrattoServiceExt;
  private final DispositivoServiceExt            dispositivoServiceExt;
  private final FsmResetService                  fsmResetService;

  public ChangeStatusFsmService(final FsmRichiestaUtils _fsmRichiestaUtil,
                                final ChangeStatusOrdineClienteService _changeStatusOrdineService,
                                final ContrattoServiceExt _contrattoServiceExt, final DispositivoServiceExt _dispositivoServiceExt,
                                final FsmResetService _fsmResetService) {
    fsmRichiestaUtil = _fsmRichiestaUtil;
    changeStatusOrdineService = _changeStatusOrdineService;
    contrattoServiceExt = _contrattoServiceExt;
    dispositivoServiceExt = _dispositivoServiceExt;
    fsmResetService = _fsmResetService;
  }

  @Transactional
  public StateChangeStatusFsm changeStatusRichiesta(FsmRichiesteDTO fsmRichiesteDTO) {
    StateChangeStatusFsm state = StateChangeStatusFsm.PENDING;
    try {
      String operazione = fsmRichiesteDTO.getOperazione();
      Set<String> identificativoRichieste = fsmRichiesteDTO.getIdentificativi();
      String motivoSospensione = fsmRichiesteDTO.getMotivoSospensione();
      String nota = fsmRichiesteDTO.getNota();

      boolean esito = fsmRichiestaUtil.changeStatusRichieste(identificativoRichieste, operazione, motivoSospensione, nota);
      if (esito) {
        state = StateChangeStatusFsm.SUCCESS;
        String identificativoRichiesta = identificativoRichieste.stream()
                                                                .findFirst()
                                                                .get();
        Richiesta richiesta = fsmRichiestaUtil.findRichiesta(identificativoRichiesta);
        String identificativoOrdine = richiesta.getOrdineCliente()
                                               .getIdentificativo();
        log.debug("Call change status to Ordine Cliente {}", identificativoOrdine);
        try {
          changeStatusOrdineService.calculateAndChangeStatus(identificativoOrdine);
          log.info("Changes status on OrdineCliente: {}", identificativoOrdine);
        } catch (Exception e) {
          log.warn("Not change status on OrdineCliente: ", e);
          log.info("Lo stato dell'ordine [{}] verrà cambiato tramite il job schedulato", identificativoOrdine);
        }
      } else {
        state = StateChangeStatusFsm.ERROR;
      }
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
    return state;
  }

  @Transactional
  public StateChangeStatusFsm changeFsmContrattoStatus(FsmContrattoDTO fsmDTO) {
    try {
      Map<String, String> mapCodContrattoProduttore = fsmDTO.getMapCodContrattoProduttore();
      String operazione = fsmDTO.getOperazione();
      String nota = fsmDTO.getNota();

      boolean esito = contrattoServiceExt.executeFsm(mapCodContrattoProduttore, operazione, nota);
      log.debug("Esito cambio stato contratti is {}", esito);
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
    return StateChangeStatusFsm.SUCCESS;
  }

  @Transactional
  public StateChangeStatusFsm changeFsmDispositivoStatus(FsmDispositivoDTO fsmDTO) {
    try {
      Set<String> identificativoDispositivi = fsmDTO.getIdentificativi();
      String operazione = fsmDTO.getOperazione();
      String nota = fsmDTO.getNota();

      log.info("Operazione Dispositivo: {}", operazione);
      if (operazione.equals(DispositivoEvent.MU_DA_RESTITUIRE.name())) {
        for (String identificativoDevice : identificativoDispositivi) {
          Richiesta richiesta = null;
          Optional<Richiesta> optRichiesta = fsmRichiestaUtil.findRichiestaByStatoAndIdentificativoDispositivo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO,
                                                                                                               identificativoDevice);
          if (optRichiesta.isPresent()) {
            richiesta = optRichiesta.get();
          } else {
            Dispositivo device = dispositivoServiceExt.findOneByIdentificativo(identificativoDevice);
            TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
            log.info("Manage device type to operazione {}: {}", operazione, tipoDispositivo);
            if (tipoDispositivo != null && tipoDispositivo.getIndirizzoRientro() == null) {
              // Change stato on device to RIENTRATO_DISMESSO.
              operazione = DispositivoEvent.DISMESSO.name();
              boolean esito = dispositivoServiceExt.executeFsm(identificativoDispositivi, operazione, nota);
              log.debug("Changed stato to {} by command {}: {}", StatoDispositivo.RIENTRATO_DISMESSO, operazione, esito);
              return StateChangeStatusFsm.SUCCESS;
            } else {
              // Creo la richiesta...
              Set<Dispositivo> dispositivos = new HashSet<>();
              dispositivos.add(device);
              Contratto contratto = device.getContratto();
              ClienteFai cliente = contratto != null ? contratto.getClienteFai() : null;
              OrdineCliente ordineCliente = createAndPersistOrdineCliente(cliente);
              log.info("Create ordine cliente [Identificativo: {} - NumeroOrdine: {}]", ordineCliente.getIdentificativo(),
                       ordineCliente.getNumeroOrdine());
              if (tipoDispositivo != null && TipoDispositivoEnum.LIBER_T == tipoDispositivo.getNome()) {
                richiesta = createRichiestaDisattivazione(ordineCliente, tipoDispositivo, dispositivos, contratto);
              } else {
                richiesta = createRichiestaMalfunziamento(ordineCliente, tipoDispositivo, dispositivos, contratto);
              }
              log.info("Create richiesta {} of malfunzionamento to device: [identificativo: {} - Seriale: {}]",
                       richiesta.getIdentificativo(), device.getIdentificativo(), device.getSeriale());
              richiesta.setStato(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
              richiesta = fsmRichiestaUtil.saveAndFlushRichiesta(richiesta);
              initAndResetStateFSM(richiesta, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
              addRichiestaOnOrdineCliente(ordineCliente, richiesta);
              log.info("Added richiesta {} on OrdineCliente [identificativo: {} - NumeroOrdine: {}]", richiesta.getIdentificativo(),
                       ordineCliente.getIdentificativo(), ordineCliente.getNumeroOrdine());
            }

          }
          Set<String> identificativoRichieste = new HashSet<>();
          identificativoRichieste.add(richiesta.getIdentificativo());
          boolean changed = fsmRichiestaUtil.changeStatusRichieste(identificativoRichieste, RichiestaEvent.MU_DISPOSITIVO_RIENTRATO.name(),
                                                                   null, nota);
          if (changed) {
            log.info("Changed stato richiesta : {} by Identificativo dispositivo: {}", richiesta, identificativoDevice);
            // richiesta = fsmRichiestaUtil.saveAndFlushRichiesta(richiesta);
          }
        }
      } else {
        boolean esito = dispositivoServiceExt.executeFsm(identificativoDispositivi, operazione, nota);
        log.debug("Esito cambio stato Dispositivi is {}", esito);
      }
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
    return StateChangeStatusFsm.SUCCESS;
  }

  private void addRichiestaOnOrdineCliente(OrdineCliente ordineCliente, Richiesta richiesta) {
    Set<Richiesta> richiestas = ordineCliente.getRichiestas();
    if (richiestas == null) {
      richiestas = new HashSet<>();
    }
    richiestas.add(richiesta);
    fsmRichiestaUtil.saveOrdineCliente(ordineCliente.richiestas(richiestas));
  }

  private OrdineCliente createAndPersistOrdineCliente(ClienteFai cliente) {
    OrdineCliente ordineCliente = new OrdineCliente().stato(StatoOrdineCliente.DA_EVADERE)
                                                     .dataCreazione(Instant.now())
                                                     .dataModificaStato(Instant.now())
                                                     .destinazioneFai(true)
                                                     .clienteAssegnatario(cliente)
                                                     .tipo(TipoOrdineCliente.VARIAZIONE);
    ordineCliente = fsmRichiestaUtil.saveOrdineCliente(ordineCliente);
    return ordineCliente;
  }

  private void initAndResetStateFSM(Richiesta richiesta, StatoRichiesta statoToReset) {
    AbstractFsmRichiesta fsm = fsmRichiestaUtil.retrieveFsmByRichiesta(richiesta);
    if (fsm == null) {
      throw new RuntimeException("Not found FSM to richiesta: " + richiesta);
    }
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = fsm.getStateMachine();
    log.debug("State machine richiesta {} : {}", richiesta.getIdentificativo(), stateMachine);
    fsmResetService.putInCacheFsm(richiesta, stateMachine);
    fsmResetService.resetStateFsm(richiesta.getIdentificativo(), statoToReset);
  }

  private Richiesta createRichiestaDisattivazione(OrdineCliente ordineCliente, TipoDispositivo tipoDispositivo, Set<Dispositivo> devices,
                                                  Contratto contract) {
    return new Richiesta().tipo(TipoRichiesta.DISATTIVAZIONE)
                          .data(Instant.now())
                          .dataModificaStato(Instant.now())
                          .contratto(contract)
                          .dispositivos(devices)
                          .tipoDispositivo(tipoDispositivo)
                          .ordineCliente(ordineCliente)
                          .stato(StatoRichiestaUtil.getStatoInizialeByTipoDispositivo(tipoDispositivo.getNome()));
  }

  private Richiesta createRichiestaMalfunziamento(OrdineCliente ordineCliente, TipoDispositivo tipoDispositivo, Set<Dispositivo> devices,
                                                  Contratto contract) {
    return new Richiesta().tipo(TipoRichiesta.MALFUNZIONAMENTO)
                          .data(Instant.now())
                          .dataModificaStato(Instant.now())
                          .contratto(contract)
                          .dispositivos(devices)
                          .tipoDispositivo(tipoDispositivo)
                          .ordineCliente(ordineCliente)
                          .stato(StatoRichiestaUtil.getStatoInizialeByTipoDispositivo(tipoDispositivo.getNome()));
  }

}
