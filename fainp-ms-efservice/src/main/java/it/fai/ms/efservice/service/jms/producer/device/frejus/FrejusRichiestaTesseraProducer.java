package it.fai.ms.efservice.service.jms.producer.device.frejus;

import it.fai.ms.common.jms.publisher.JmsObjectMessageSenderTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsDestination;
//import it.fai.ms.common.jms.JmsObjectMessageSenderTemplate;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.frejus.RichiestaCartaFrejusMessage;


@Service()
@Transactional
public class FrejusRichiestaTesseraProducer
  extends JmsObjectMessageSenderTemplate<RichiestaCartaFrejusMessage>{


  public FrejusRichiestaTesseraProducer(JmsProperties jmsProperties) {
		super(jmsProperties);
	}

  @Override
	public JmsDestination getJmsDestination() {
    return JmsQueueNames.REQUEST_TESSERE_FREJUS;
	}

}
