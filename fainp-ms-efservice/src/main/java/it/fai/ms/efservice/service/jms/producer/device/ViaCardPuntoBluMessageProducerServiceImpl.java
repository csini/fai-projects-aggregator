package it.fai.ms.efservice.service.jms.producer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.fsm.ViaCardPuntoBluActivatedMessage;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional
public class ViaCardPuntoBluMessageProducerServiceImpl implements ViaCardPuntoBluMessageProducerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public ViaCardPuntoBluMessageProducerServiceImpl(final JmsSenderQueueMessageService _jmsSenderQueueService) {
    jmsSenderQueueService = _jmsSenderQueueService;
  }

  @Override
  public void notificationViaCardActivated(Richiesta richiesta) {
    String identificativo = richiesta.getIdentificativo();
    if (richiesta.getPuntoBlu()) {
      _log.info("Identificativo Richiesta to send notification activated ViaCard PuntoBlu: {}", identificativo);
      String numeroOrdine = richiesta.getOrdineCliente() != null ? richiesta.getOrdineCliente()
                                                                            .getNumeroOrdine()
                                                                 : null;
      String codiceCliente = richiesta.getContratto() != null && richiesta.getContratto()
                                                                          .getClienteFai() != null ? richiesta.getContratto()
                                                                                                              .getClienteFai()
                                                                                                              .getCodiceCliente()
                                                                                                   : null;

      ViaCardPuntoBluActivatedMessage dto = new ViaCardPuntoBluActivatedMessage(identificativo, numeroOrdine, codiceCliente);
      _log.info("Prepare sending message: {}", dto);
      jmsSenderQueueService.sendNotificationActivatedViaCardMessage(dto);
    } else {
      _log.info("Not send notification, because Richiesta {} not is PUNTO_BLU", identificativo);
    }
  }

}
