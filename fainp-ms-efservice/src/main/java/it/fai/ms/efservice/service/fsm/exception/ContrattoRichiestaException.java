package it.fai.ms.efservice.service.fsm.exception;

public class ContrattoRichiestaException extends RuntimeException {

  private static final long serialVersionUID = -5471954160198763487L;

  public ContrattoRichiestaException() {
    this(ContrattoRichiestaException.class.toString());
  }

  public ContrattoRichiestaException(String message, Throwable cause) {
    super(message, cause);
  }

  public ContrattoRichiestaException(String message) {
    super(message);
  }

  public ContrattoRichiestaException(Throwable cause) {
    super(cause);
  }

}
