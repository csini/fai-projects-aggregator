package it.fai.ms.efservice.service.jms.listener;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.anagazienda.TrasformazioneDTO;
import it.fai.ms.efservice.service.jms.consumer.NotificaTrasformazioneConsumer;

@Service
@Transactional
public class JmsNotificaTrasformazioneListener
  extends JmsObjectMessageListenerTemplate<TrasformazioneDTO>
  implements JmsTopicListener {

  private NotificaTrasformazioneConsumer notificaTrasformazioneConsumer;

  public JmsNotificaTrasformazioneListener(NotificaTrasformazioneConsumer notificaTrasformazioneConsumer) {
    this.notificaTrasformazioneConsumer = notificaTrasformazioneConsumer;
  }

  @Override
  protected void consumeMessage(TrasformazioneDTO notificaTrasformazione) {
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.NOTIFICA_TRASFORMAZIONE;
  }
}
