package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toSet;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.notification.NotificationAziendaDTO;
import it.fai.ms.common.jms.dto.notification.NotificationDispositivoDTO;
import it.fai.ms.common.jms.notification_v2.NotificationMessageClient;
import it.fai.ms.common.jms.notification_v2.VeicoloDispositivoConflittiMessage;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;

@Service
@Transactional
public class NotificationPreconditionVehicleAnomalyService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  private final DispositivoServiceExt deviceServiceExt;

  private final VehicleWizardCacheServiceExt vehicleWizardCacheService;

  public NotificationPreconditionVehicleAnomalyService(final JmsProperties _jmsProperties, final DispositivoServiceExt _deviceServiceExt,
                                                       final VehicleWizardCacheServiceExt _vehicleWizardCacheService) {
    jmsProperties = _jmsProperties;
    deviceServiceExt = _deviceServiceExt;
    vehicleWizardCacheService = _vehicleWizardCacheService;
  }

  public void sendNotification(RuleEntityKey key, VeicoloDispositivoConflittiMessage dto) {

    log.info("RuleEntityKey: {}", key);
    String vehicleUuid = key.getVehicleId();
    String deviceTypeId = key.getDeviceTypeId();
    String serviceTypeId = key.getServiceTypeId();
    if (vehicleUuid != null && !deviceTypeId.equals(RuleEngineDeviceTypeId.ofWildcard()
                                                                          .getId())) {

      TipoDispositivoEnum tipoDispositivo = TipoDispositivoEnum.valueOf(deviceTypeId);
      Set<Dispositivo> dispositivi = deviceServiceExt.findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(vehicleUuid,
                                                                                                                               tipoDispositivo);

      dispositivi.stream()
                 .filter(d -> d.getStato()
                               .isGreen())
                 .map(disp -> {
                   if (!serviceTypeId.equals(RuleEngineServiceTypeId.ofWildcard()
                                                                    .getId())) {
                     Optional<StatoDispositivoServizio> optSds = disp.getStatoDispositivoServizios()
                                                                     .stream()
                                                                     .filter(sds -> sds.getStato() == StatoDS.ATTIVO)
                                                                     .filter(sds -> sds.getTipoServizio() != null && sds.getTipoServizio()
                                                                                                                        .getNome()
                                                                                                                        .equals(serviceTypeId))
                                                                     .findFirst();
                     if (optSds.isPresent()) {
                       sendNotification(optSds.get(), disp, vehicleUuid, dto);
                     }
                   } else {
                     sendNotification(null, disp, vehicleUuid, dto);
                   }
                   return disp;
                 })
                 .collect(toSet());

    } else {
      log.warn("Vehicle UUID is {} or deviceTypeId is {}", vehicleUuid, deviceTypeId);
    }

  }

  private void sendNotification(StatoDispositivoServizio sds, Dispositivo device, String vehicleUuid,
                                VeicoloDispositivoConflittiMessage dto) {
    NotificationMessageClient notificationClient = new NotificationMessageClient(jmsProperties);

    NotificationDispositivoDTO notificationDispositivoDTO = new NotificationDispositivoDTO();
    String targa = vehicleWizardCacheService.getTargaByUuidVehicle(vehicleUuid);
    notificationDispositivoDTO.setUuidDispositivo(device.getIdentificativo());
    notificationDispositivoDTO.setSeriale(device.getSeriale());
    notificationDispositivoDTO.setTipoDispositivo(device.getTipoDispositivo()
                                                        .getNome());
    notificationDispositivoDTO.setAssociazione(targa);

    dto.setServizio((sds != null) ? sds.getTipoServizio()
                                       .getNome()
                                  : null);

    NotificationAziendaDTO notificationAziendaDTO = new NotificationAziendaDTO();
    Contratto contratto = device.getContratto();
    if (contratto != null) {
      ClienteFai clienteFai = contratto.getClienteFai();
      notificationAziendaDTO.setCodiceCliente(clienteFai != null ? clienteFai.getCodiceCliente() : null);
      notificationAziendaDTO.setRagioneSociale(clienteFai != null ? clienteFai.getRagioneSociale() : null);

//      notificationDispositivoDTO.setRagioneSociale(clienteFai != null ? clienteFai.getRagioneSociale() : null);
    }

    dto.setNotificationDispositivoDTO(notificationDispositivoDTO);
    dto.setNotificationAziendaDTO(notificationAziendaDTO);

    log.info("Send notification message: {}", dto);
    notificationClient.sendNotification(dto);
  }

}
