package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.List;

public class DataMapOrdineFornitoreDTO implements Serializable {

  private static final long serialVersionUID = -4704071627621838419L;

  private List<String> headers;

  private List<AnomaliaOrdineFornitoreDTO> data;

  public List<String> getHeaders() {
    return headers;
  }

  public void setHeaders(List<String> headers) {
    this.headers = headers;
  }

  public List<AnomaliaOrdineFornitoreDTO> getData() {
    return data;
  }

  public void setData(List<AnomaliaOrdineFornitoreDTO> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "DataMapOrdineFornitoreDTO [headers=" + headers + ", data=" + data + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((data == null) ? 0 : data.hashCode());
    result = prime * result + ((headers == null) ? 0 : headers.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DataMapOrdineFornitoreDTO other = (DataMapOrdineFornitoreDTO) obj;
    if (data == null) {
      if (other.data != null)
        return false;
    } else if (!data.equals(other.data))
      return false;
    if (headers == null) {
      if (other.headers != null)
        return false;
    } else if (!headers.equals(other.headers))
      return false;
    return true;
  }

}
