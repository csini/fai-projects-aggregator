package it.fai.ms.efservice.rules.engine;

import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

public interface RulePrecalculator {

  RuleOutcomeEntity process(RuleContext ruleContext, RuleBucket ruleId);

}
