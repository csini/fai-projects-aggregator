package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toSet;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableSet;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

@Service
@Transactional
public class TrackyCardGoBoxService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt deviceRepo;

  private final TipoServizioRepositoryExt deviceTypeRepo;

  private final StatoDispositivoServizioRepositoryExt sdsRepo;

  private final Map<String, WizardVehicleEntity> cacheVehicleWizard;

  public TrackyCardGoBoxService(final DispositivoRepositoryExt _deviceRepo, final TipoServizioRepositoryExt _deviceTypeRepo,
                                final StatoDispositivoServizioRepositoryExt _sdsRepo,
                                @Qualifier("wizard_vehicle") Map<String, WizardVehicleEntity> _cacheVehicleWizard) {
    deviceRepo = _deviceRepo;
    deviceTypeRepo = _deviceTypeRepo;
    sdsRepo = _sdsRepo;
    cacheVehicleWizard = _cacheVehicleWizard;
  }

  public Optional<Dispositivo> findActiveDeviceByTypeAndLicensePlateAndCountry(TipoDispositivoEnum tipoDispositivo,
                                                                               Set<StatoDispositivo> statiDispositiviAttivi, String targa,
                                                                               String nazione, String codiceCliente) {
    Dispositivo dispositivo = null;
    if (StringUtils.isNotBlank(codiceCliente)) {
      Set<Dispositivo> devicesPending = findDeviceByCodiceClienteAndTipoAndStato(codiceCliente, tipoDispositivo, statiDispositiviAttivi);
      dispositivo = filterDeviceByTargaNazione(devicesPending, targa, nazione);
    } else {
      log.warn("Codice cliente is null or empty");
    }
    return Optional.ofNullable(dispositivo);
  }

  public TipoServizio findByNomeServizio(String nomeServizio) {
    return deviceTypeRepo.findByNome(nomeServizio);
  }

  public Optional<Dispositivo> findPendingGoBoxRelatedByRichiesta(final Richiesta richiesta) {
    ClienteFai clienteFai = null;
    String targa = richiesta.getAssociazione();
    String nazione = richiesta.getCountry();
    Contratto contratto = richiesta.getContratto();
    if (contratto != null) {
      clienteFai = contratto.getClienteFai();
    }
    String codiceCliente = (clienteFai != null) ? clienteFai.getCodiceCliente() : null;
    return findPendingGoBoxByTargaNazioneAndStatoAndCodiceCliente(targa, nazione, StatoDispositivo.INIZIALE, codiceCliente);
  }

  public Optional<Dispositivo> findPendingGoBoxByTargaNazioneAndStatoAndCodiceCliente(String targa, String nazione, StatoDispositivo stato,
                                                                                      String codiceCliente) {
    Dispositivo device = null;
    if (StringUtils.isNotBlank(codiceCliente)) {
      Set<Dispositivo> devicesFound = findDeviceByCodiceClienteAndTipoAndStato(codiceCliente, TipoDispositivoEnum.GO_BOX,
                                                                               ImmutableSet.of(stato));
      device = filterDeviceByTargaNazione(devicesFound, targa, nazione);
    } else {
      log.warn("Codice cliente is null or empty");
    }
    return Optional.ofNullable(device);
  }

  public StatoDispositivoServizio updateStato(StatoDispositivoServizio sds, StatoDS stato) {
    return sdsRepo.save(sds.stato(stato));
  }

  private Set<Dispositivo> findDispositiviByCodiceClienteAndTipo(String codiceCliente, TipoDispositivoEnum tipoDispositivo) {
    return deviceRepo.findByCodiceClienteAndTipoDispositivo(codiceCliente, tipoDispositivo);
  }

  private Set<Dispositivo> findDeviceByCodiceClienteAndTipoAndStato(String codiceCliente, TipoDispositivoEnum tipoDispositivo,
                                                                    Set<StatoDispositivo> stati) {
    Set<Dispositivo> dispositiviGoBox = findDispositiviByCodiceClienteAndTipo(codiceCliente, tipoDispositivo);
    log.info("Devices found by [{} - {}]: {}", codiceCliente, tipoDispositivo, dispositiviGoBox);
    Set<Dispositivo> devicesFiltered = dispositiviGoBox.stream()
                                                       .filter(d -> stati.contains(d.getStato()))
                                                       .collect(toSet());
    log.info("Devices found by filter stati {} : {}", devicesFiltered);
    return devicesFiltered;
  }

  private Dispositivo filterDeviceByTargaNazione(Set<Dispositivo> devices, String targa, String nazione) {
    Dispositivo dispositivo = null;
    for (Dispositivo device : devices) {
      Set<AssociazioneDV> asdv = device.getAssociazioneDispositivoVeicolos();
      log.info("Associzioni DV found by Dispositivo {} : {}", device, asdv);
      Optional<AssociazioneDV> optAssDv = asdv.stream()
                                              .filter(adv -> cacheVehicleWizard.get(adv.getUuidVeicolo()) != null
                                                             && targa.equals(cacheVehicleWizard.get(adv.getUuidVeicolo())
                                                                                               .getLicensePlate())
                                                             && nazione.equals(cacheVehicleWizard.get(adv.getUuidVeicolo())
                                                                                                 .getCountry()))
                                              .findFirst();

      if (optAssDv.isPresent()) {
        dispositivo = device;
        break;
      }
    }
    log.info("Dispositivo found by targa nazione [{} - {}]: {}", targa, nazione, dispositivo);
    return dispositivo;
  }

}
