package it.fai.ms.efservice.web.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.service.StatoDispositivoServizioServiceExt;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.CreatedFsmChangeStatusAsyncDTO;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.dto.FsmRichiesteDTO;
import it.fai.ms.efservice.service.dto.FsmServizioDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

/**
 * REST controller for managing FSM change status.
 */
@RestController
@RequestMapping(FsmAsyncResourceExt.BASE_PATH)
public class FsmAsyncResourceExt {

  private final Logger log = LoggerFactory.getLogger(FsmAsyncResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  public static final String POLLING = "/polling";

  public static final String FSM_RICHIESTA = "/fsmrichiesta";

  public static final String FSM_POLLING_RICHIESTA = FSM_RICHIESTA + POLLING;

  public static final String FSM_DISPOSITIVO = "/fsmdispositivo";

  public static final String FSM_POLLING_DISPOSITIVO = FSM_DISPOSITIVO + POLLING;

  public static final String FSM_SERVIZIO = "/fsmservizio";

  public static final String FSM_POLLING_SERVIZIO = FSM_SERVIZIO + POLLING;

  private static final String KEY_JOB = "job";

  private static final String RICHIESTE_KEY_JOB   = "FSM-REQUEST";
  private static final String DISPOSITIVI_KEY_JOB = "FSM-DEVICE";
  private static final String SERVIZIO_KEY_JOB    = "FSM-SERVICE";

  // private static final String ENTITY_NAME = "fsm";

  private final FsmAsyncService                    fsmAsyncService;
  private final AsyncPollingJobServiceImpl         asyncPollingJobService;
  private final StatoDispositivoServizioServiceExt sdsServiceExt;

  public FsmAsyncResourceExt(final FsmAsyncService _fsmAsyncService, final AsyncPollingJobServiceImpl _asyncPollingJobService,
                             final StatoDispositivoServizioServiceExt _sdsServiceExt) {
    fsmAsyncService = _fsmAsyncService;
    asyncPollingJobService = _asyncPollingJobService;
    sdsServiceExt = _sdsServiceExt;
  }

  @PostMapping(FSM_RICHIESTA)
  @Timed
  public ResponseEntity<Map<String, String>> changeStatusToRichiesta(@Valid @RequestBody FsmRichiesteDTO fsmDTO) throws CustomException {
    long ti = System.currentTimeMillis();
    if (!checkValidationFsmRichiesteDTO(fsmDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identifierJob = getIdentifierJob(RICHIESTE_KEY_JOB);
    log.info("Create Async Job to change status by FSM with identifier {}", identifierJob);
    fsmAsyncService.changeAsyncFsmStatus(identifierJob, fsmDTO);

    long tf = System.currentTimeMillis();
    log.debug("FSM Resource 'changeStatusToRichieste' Request time: {} s", (tf - ti));
    return getResponse(identifierJob);
  }

  @GetMapping(FSM_POLLING_RICHIESTA + "/{identificativo}")
  @Timed
  public ResponseEntity<CreatedFsmChangeStatusAsyncDTO> getPollingFsmChangeStatus(@PathVariable String identificativo) {
    log.debug("Rest request for polling FSM change status {}", identificativo);
    return ResponseEntity.ok(getResponsePolling(identificativo));
  }

  @PostMapping(FSM_DISPOSITIVO)
  @Timed
  public ResponseEntity<Map<String, String>> changeStatusDispositivo(@Valid @RequestBody FsmDispositivoDTO fsmDTO) throws CustomException {
    long ti = System.currentTimeMillis();
    if (!checkValidationFsmDispositivoDTO(fsmDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identifierJob = getIdentifierJob(DISPOSITIVI_KEY_JOB);
    log.info("Create Async Job to change status by FSM-Dispositivo with identifier {}", identifierJob);
    fsmAsyncService.changeAsyncFsmDispositivo(identifierJob, fsmDTO);

    long tf = System.currentTimeMillis();
    log.debug("FSM Resource 'changeStatusDispositivo' Request time: {} s", (tf - ti));
    return getResponse(identifierJob);
  }

  @GetMapping(FSM_POLLING_DISPOSITIVO + "/{identificativo}")
  @Timed
  public ResponseEntity<CreatedFsmChangeStatusAsyncDTO> pollingFsmDispositivo(@PathVariable String identificativo) {
    log.debug("Rest request for polling FSM-Dispositivo change status {}", identificativo);
    return ResponseEntity.ok(getResponsePolling(identificativo));
  }

  @PostMapping(FSM_SERVIZIO)
  @Timed
  public ResponseEntity<Map<String, String>> changeStatusServizioDispositivo(@Valid @RequestBody FsmServizioDTO fsmDTO) throws CustomException {

    if (!checkValidationFsmServiziDTO(fsmDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identifierJob = getIdentifierJob(SERVIZIO_KEY_JOB);

    Set<String> identificativoServizi = fsmDTO.getIdentificativi();
    String dispositivoUuid = fsmDTO.getDispositivoUuid();
    String operazione = fsmDTO.getOperazione();

    Set<StatoDispositivoServizio> sdss = sdsServiceExt.changeStatusServizioDispositivo(identificativoServizi, dispositivoUuid, operazione);

    if (sdss == null || sdss.isEmpty()) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_FSM_SERVIZIO);
    }

    return getResponse(identifierJob);
  }

  @GetMapping(FSM_POLLING_SERVIZIO + "/{identificativo}")
  @Timed
  public ResponseEntity<CreatedFsmChangeStatusAsyncDTO> pollingFsmServizio(@PathVariable String identificativo) {
    log.debug("Rest request for polling FSM-Servizio change status {}", identificativo);
    CreatedFsmChangeStatusAsyncDTO responsePolling = getResponsePolling(identificativo);
    return ResponseEntity.ok(responsePolling.state(StateChangeStatusFsm.SUCCESS)); // TODO fix;
  }

  private boolean checkValidationFsmServiziDTO(FsmServizioDTO fsmDTO) {
    String operazione = fsmDTO.getOperazione();
    if (StringUtils.isBlank(operazione)) {
      log.error("FSM-Servizio => Operazione is BLANK");
      return false;
    }

    Set<String> identificativi = fsmDTO.getIdentificativi();
    if (identificativi == null || identificativi.isEmpty()) {
      log.error("FSM-Servizio => Identificativi servizi is NULL or EMPTY");
      return false;
    }

    String dispositivoUuid = fsmDTO.getDispositivoUuid();
    if (StringUtils.isBlank(dispositivoUuid)) {
      log.error("FSM-Servizio => Identificativo dispositivo is BLANK");
    }

    return true;
  }

  private boolean checkValidationFsmRichiesteDTO(FsmRichiesteDTO fsmDTO) {
    String operazione = fsmDTO.getOperazione();
    if (StringUtils.isBlank(operazione)) {
      log.error("Operazione for change status is {}", operazione);
      return false;
    }

    Set<String> identificativoRichieste = fsmDTO.getIdentificativi();
    if (identificativoRichieste == null || (identificativoRichieste != null && identificativoRichieste.isEmpty())) {
      log.error("Identificativo Richieste for change status is {}", identificativoRichieste);
      return false;
    }

    return true;
  }

  private boolean checkValidationFsmDispositivoDTO(FsmDispositivoDTO fsmDTO) {
    Set<String> identificativi = fsmDTO.getIdentificativi();
    if (identificativi == null || identificativi.isEmpty()) {
      log.error("FSM-Dispositivo => Identificativi dispositivo is NULL or EMPTY");
      return false;
    }

    String operazione = fsmDTO.getOperazione();
    if (StringUtils.isBlank(operazione)) {
      log.error("FSM-Dispositivo => Operazione is NULL");
      return false;
    }

    Set<DispositivoEvent> setEvents = new HashSet<DispositivoEvent>();
    Collections.addAll(setEvents, DispositivoEvent.values());
    boolean isPresent = setEvents.stream()
                                 .filter(ce -> ce.name()
                                                 .equalsIgnoreCase(operazione))
                                 .findFirst()
                                 .isPresent();
    if (!isPresent) {
      log.error("FSM-Dispositivo => L'operazione richiesta non rientra tra quelle possibili: {}", setEvents);
      return false;
    }

    return true;
  }

  private String getIdentifierJob(String prefixKey) {
    return fsmAsyncService.getKeyCacheAndPushDefaultValue(prefixKey);
  }

  private ResponseEntity<Map<String, String>> getResponse(String identifierJob) {
    Map<String, String> response = new HashMap<>();
    response.put(KEY_JOB, identifierJob);
    return ResponseEntity.ok(response);
  }

  private CreatedFsmChangeStatusAsyncDTO getResponsePolling(String identificativoJob) {
    StateChangeStatusFsm result = (StateChangeStatusFsm) asyncPollingJobService.getValue(identificativoJob);
    CreatedFsmChangeStatusAsyncDTO createdFsmChangeStatusAsyncDTO = new CreatedFsmChangeStatusAsyncDTO(result);
    log.info("Response polling is {}", createdFsmChangeStatusAsyncDTO);
    return createdFsmChangeStatusAsyncDTO;
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
