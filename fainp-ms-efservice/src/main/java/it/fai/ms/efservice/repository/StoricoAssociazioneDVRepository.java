package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.StoricoAssociazioneDV;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the StoricoAssociazioneDV entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoricoAssociazioneDVRepository extends JpaRepository<StoricoAssociazioneDV, Long> {

}
