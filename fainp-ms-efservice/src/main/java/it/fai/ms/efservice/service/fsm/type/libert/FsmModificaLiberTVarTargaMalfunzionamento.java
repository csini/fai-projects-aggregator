package it.fai.ms.efservice.service.fsm.type.libert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.libert.FsmModificaLiberTVarTargaMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaLiberTVarTargaMalfunzionamento.FSM_MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaLiberTVarTargaMalfunzionamento extends FsmRichiestaGeneric {
  
  public static final String FSM_MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO = "fsmModificaLiberTVarTargaMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaLiberTVarTargaMalfunzionamento(@Qualifier(FsmModificaLiberTVarTargaMalfunzionamentoConfig.MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.LIBER_T};
  }


  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
