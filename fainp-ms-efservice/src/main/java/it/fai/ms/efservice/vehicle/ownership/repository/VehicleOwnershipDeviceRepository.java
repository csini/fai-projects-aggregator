package it.fai.ms.efservice.vehicle.ownership.repository;

import java.util.Set;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipDevice;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;

public interface VehicleOwnershipDeviceRepository {

  Set<VehicleOwnershipDevice> findByVehicleOwnershipId(VehicleOwnershipId vehicleOwnershipId);

Set<VehicleOwnershipDevice> findByVehicleOwnershipId(VehicleOwnershipId _vehicleOwnershipId,
		Set<TipoDispositivoEnum> tipiDispositivo);

}
