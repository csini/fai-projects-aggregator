package it.fai.ms.efservice.service.fsm.config.assistenza.veicoli;


import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardTipoRichiesta;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaAssistenzaVeicoliVarTargaConfig.MOD_ASSISTENZA_VEICOLI_VARTARGA)
public class FsmModificaAssistenzaVeicoliVarTargaConfig extends FsmRichiestaConfig {

  public static final String MOD_ASSISTENZA_VEICOLI_VARTARGA = "ModAssistenzaVeicoliVarTarga";
  private final static Logger log = LoggerFactory.getLogger(FsmModificaAssistenzaVeicoliVarTargaConfig.class);
  private final FsmSenderToQueue senderFsmUtil;
  
  public FsmModificaAssistenzaVeicoliVarTargaConfig(final FsmSenderToQueue _senderFsmUtil,
                                                    final DeviceServiceActivationMessageProducerService _deviceServiceActivationService) {
    this.senderFsmUtil = _senderFsmUtil;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_ASSISTENZA_VEICOLI_VARIAZIONE_TARGA.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_ASSISTENZA_VEICOLI;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaAssistenzaVeicoliVarTarga();
  }
  
  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
 
    transitions.withExternal()// MANUAL
    .source(StatoRichiesta.ATTIVO_PER_MODIFICA_ASSISTENZA_VEICOLI)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()//AUTOGUARD
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.VARIAZIONE_TARGA)
    .guard(new GuardTipoRichiesta(TipoRichiesta.VARIAZIONE_TARGA))
    .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.VARIAZIONE_TARGA)
    .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
    .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.DISMESSO))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.DISATTIVAZIONE)
    .guard(new GuardTipoRichiesta(TipoRichiesta.DISATTIVAZIONE))
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.DISATTIVAZIONE)
    .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
    .action(new FsmActionChangeStatusDispositivo(senderFsmUtil, DispositivoEvent.DISMESSO))
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
    .target(StatoRichiesta.EVASO)
    .action(new FsmActionGeneric())
    ; 
                
  }
  
  
  

}
