package it.fai.ms.efservice.service.fsm.type;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.fsm.AbstractFsmDispositivo;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmDispositivoCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

@Service(value = "fsmDispositivo")
public class FsmDispositivo extends AbstractFsmDispositivo {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  // "{id:fsmDispositivo,childs:[],state:$state$,historyStates:{},event:null,eventHeaders:null,extendedState:{variables:{}}}";
  private static String JSON_DEFAULT = "{\"id\":\"fsmDispositivo\",\"childs\":[],\"state\":\"$STATE$\",\"historyStates\":{},\"event\":null,\"eventHeaders\":null,\"extendedState\":{\"variables\":{}}}";

  private StateMachineDispositivoBuilder fsmBuilder;

  private FsmDispositivoCacheService cacheService;

  /**
   * @param fsmBuilder
   * @param cacheService
   */
  public FsmDispositivo(StateMachineDispositivoBuilder _fsmBuilder, FsmDispositivoCacheService _cacheService) {
    this.fsmBuilder = _fsmBuilder;
    this.cacheService = _cacheService;
  }

  @Override
  public Dispositivo executeCommandToChangeState(DispositivoEvent command, Dispositivo dispositivo, String nota) throws Exception {
    Long deviceId = dispositivo.getId();
    String identificativo = dispositivo.getIdentificativo();
    logger.debug("Switch state by command {} for Dispositivo [ID: {} - Identificativo: {}]", command, deviceId, identificativo);

    StateMachine<StatoDispositivo, DispositivoEvent> stateMachine = null;

    // logger.info("Not available command {}. Check if not exists value in cache to restore state machine from not
    // INITIAL state of state machine.");
    String fsmKey = String.valueOf(deviceId);
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isBlank(jsonContext)) {
        logger.info("FsmKey {} is not stored in cache.");
        StatoDispositivo actualStateDispositivo = dispositivo.getStato();
        if (actualStateDispositivo != StatoDispositivo.INIZIALE) {
          String jsonReplacedState = StringUtils.replace(JSON_DEFAULT, "$STATE$", actualStateDispositivo.name());
          logger.info("Replaced defualt JSON to new JSON: {}", jsonReplacedState);
          logger.info("Inizializzo la macchina a partire da uno stato avanzato => StatoIniziale {} per il dispositivo [ID: {} - Identificativo: {}]",
                      actualStateDispositivo.name(), deviceId, identificativo);
          stateMachine = cacheService.restoreStateMachineFromJson(getStateMachine(), jsonReplacedState);
          logger.info("Restore stateMachine {} from JSONReplacedState: {}", stateMachine, jsonReplacedState);
        } else {
          logger.info("Lo stato del dispositivo {} è {}, quindi non bisogna inizializzare la macchina in uno stato avanzato.",
                      identificativo, actualStateDispositivo.name());
        }
      }
    }

    boolean availableCommand = isAvailableCommand(command, dispositivo);
    if (availableCommand) {

      logger.info("Command {} is available for dispositivo [ID: {} - Identificativo: {}]", command, deviceId, identificativo);
      if (stateMachine == null) {
        stateMachine = getStateMachine();
        stateMachine.start();
      }

      stateMachine = restoreStateMachine(stateMachine, fsmKey);

      if (command != null) {
        sendEventToFsm(stateMachine, command, dispositivo, nota);
        logger.info("Send command: {} to Dispositivo [ID: {} - Identificativo: {}]", command, deviceId, identificativo);
        cacheService.persist(stateMachine, fsmKey);
        // Set operazioni possibili da BO;
        dispositivo = setOperazioniPossibiliDispositivo(dispositivo);
      } else {
        logger.warn("Enum command (DispositivoEvent) is: {}", command);
      }

      logger.debug("=> FINISH switch state with command: {} to Dispositivo [ID: {} - Identificativo: {}]", command, deviceId,
                   identificativo);
    } else {
      logger.warn("Command: {} is not available for Dispositivo [ID: {} - Identificativo: {}] with stato {}", command, deviceId,
                  identificativo, dispositivo.getStato());
    }

    return dispositivo;
  }

  private StateMachine<StatoDispositivo, DispositivoEvent> restoreStateMachine(StateMachine<StatoDispositivo, DispositivoEvent> stateMachine,
                                                                               String fsmKey) {
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isNotBlank(jsonContext)) {
        stateMachine = cacheService.restore(stateMachine, fsmKey);
      }
    }

    return stateMachine;
  }

  @Override
  protected StateMachine<StatoDispositivo, DispositivoEvent> getStateMachine() {
    return fsmBuilder.build();
  }

}
