package it.fai.ms.efservice.service.fsm.type;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@Service(value = "fsmAccettazione")
public class FsmAccettazione extends AbstractFsmRichiesta {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory;

  private FsmFactory fsmFactory;

  private FsmRichiestaCacheService cacheService;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepo;

  /**
   * @param _stateMachineFactory
   * @param _fsmFactory
   * @param _cacheService
   */
  public FsmAccettazione(@Qualifier("accettazione") StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                         FsmFactory _fsmFactory, FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.fsmFactory = _fsmFactory;
    this.cacheService = _cacheService;
  }

  @Override
  public StateMachine<StatoRichiesta, RichiestaEvent> getStateMachine() {
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = stateMachineFactory.getStateMachine(FsmType.ACCETTAZIONE.fsmName());
    log.debug("Get state machine {}", stateMachine.getId());
    return stateMachine;
  }

  @Override
  public Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta, String nota) {
    long tStart = System.currentTimeMillis();
    Long richiestaId = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    StatoRichiesta statoAttuale = richiesta.getStato();
    log.debug("Switch state by command {} to richiesta [ID: {} - Identificativo: {}] with actual state: {}", command, richiestaId,
              identificativo, statoAttuale);

    if (!isEqualType(richiesta, richiesta.getTipoDispositivo()
                                         .getNome())) {
      log.warn("Richiesta: {} non gestibile tramite questa FSM...", identificativo);
      return richiesta;
    }

    boolean availableCommand = isAvailableCommand(command, richiesta);
    if (availableCommand) {

      StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = null;
      stateMachine = getStateMachine();
      stateMachine.start();
      String fsmKey = String.valueOf(richiestaId);
      stateMachine = getStoredStateMachine(stateMachine, fsmKey);

      if (command != null) {

        sendEventToFsm(stateMachine, command, richiesta, nota);
        log.info("Send command: {} to Richiesta [ID: {} - Identificativo: {}]", command, richiestaId, identificativo);

        cacheService.persist(stateMachine, fsmKey);
        if (richiesta != null) {
          richiesta = checkExistsContrattoToRichiesta(richiesta); // Potrebbe essere tolto; Viene mantenuto solo per log
                                                                  // nel caso ci siano dei problemi nel set del
                                                                  // contratto sulla richiesta;
          if (richiesta.getStato() == StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO) {
            try {
              FsmCommand commandInoltro = getCommandFsmInoltro(richiesta);
              richiesta = switchStateWithFsmInoltro(richiesta, commandInoltro, RichiestaEvent.INITIAL);
              AbstractFsmRichiesta fsmInoltro = getFsmByCommand(commandInoltro);
              // Recupero e Setto le nuove operazioni possibili da parte dell'utente;
              richiesta = retrieveUserCommandAndSetOperazioniPossibiliRichiesta(fsmInoltro, richiesta);
            } catch (Exception e) {
              log.error("FSM Inoltro Exception", e);
              throw new RuntimeException("FSM Inoltro: " + e.getMessage());
            }
          } else {
            // Setto le nuove operazioni possibili da parte dell'utente;
            richiesta = getUserAvailableCommandAndSetOnRichiesta(richiesta);
          }
        }
      } else {
        log.warn("Enum command (RichiestaEvent) is {} ", command);
      }
    } else {
      String messageError = String.format("Command %s is not available because Richiesta [ID: %s - Identificativo: %s] state is %s",
                                          command, richiestaId, identificativo, statoAttuale);
      log.error(messageError);
      throw new RuntimeException(messageError);
    }

    log.debug("=> FINISH switch state with command: {} to Richiesta [ID: {} - Identificativo: {}] in {} ms", command, richiestaId,
              identificativo, (System.currentTimeMillis() - tStart));
    return richiesta;
  }

  private Richiesta checkExistsContrattoToRichiesta(Richiesta richiesta) {
    String identificativoRichiesta = richiesta.getIdentificativo();

    Contratto contrattoFromRichiesta = richiesta.getContratto();
    if (contrattoFromRichiesta != null && contrattoFromRichiesta.getId() > 0) {
      log.debug("Already set a contract {} to richiesta {}", contrattoFromRichiesta.getCodContrattoCliente(), identificativoRichiesta);
      return richiesta;
    } else {
      log.warn("There was a problem on the Richiesta [ID: {} - Identificativo: {}] creation.", richiesta.getId(), identificativoRichiesta);
    }
    return richiesta;
  }

  private StateMachine<StatoRichiesta, RichiestaEvent> getStoredStateMachine(StateMachine<StatoRichiesta, RichiestaEvent> sm,
                                                                             String fsmKey) {
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isNotBlank(jsonContext)) {
        sm = cacheService.restore(getStateMachine(), fsmKey);
      }
    }

    return sm;
  }

  private Richiesta retrieveUserCommandAndSetOperazioniPossibiliRichiesta(AbstractFsmRichiesta fsmInoltro, Richiesta richiesta) {
    List<String> userAvailableCommand = fsmInoltro.getUserAvailableCommand(richiesta);
    return setOperazioniPossibiliRichiesta(userAvailableCommand, richiesta);
  }

  private FsmCommand getCommandFsmInoltro(Richiesta richiesta) {
    FsmCommand commandFsmInoltro = null;
    TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
    if (tipoDispositivo.getNome() == null) {
      Long id = tipoDispositivo.getId();
      tipoDispositivo = tipoDispositivoRepo.findOne(id);
    }

    switch (tipoDispositivo.getNome()) {
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE;
      break;
    case TELEPASS_ITALIANO:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TI;
      break;
    case VIACARD:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VC;
      break;
    case TRACKYCARD:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TRACKY;
      break;

    case GO_BOX:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_GOBOX;
      break;

    case TOLL_COLLECT:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TOLLCOLLECT;
      if (richiesta.getTipo()
                   .equals(TipoRichiesta.ATTIVAZIONE_SERVIZIO)
          || richiesta.getTipo()
                      .equals(TipoRichiesta.DISATTIVAZIONE_SERVIZIO)) {
        commandFsmInoltro = FsmCommand.CMD_ATTIVAZIONE_SERVIZIO_TOLLCOLLECT;
      }
      break;

    case TES_TRA_FREJUS_MONTE_BIANCO:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_FREJUS;
      break;
    case DARTFORD_CROSSING:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_DARTFORD_CROSSING;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_GRAN_SAN_BERNARDO;
      break;
    case VISPRO:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VISPRO;
      break;
    case VIA_TOLL:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VIA_TOLL;
      break;
    case LIBER_T:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_LIBER_T;
      break;
    case DARS_GO:
    case SATELLIC:
    case TES_CONV_CARBURANTI:
    case TESSERA_CARONTE:
    case VIA_T:
      log.warn("Tipo dispositivo {} is not supported at this moment", tipoDispositivo.getNome());
      break;
    case ASSISTENZA_VEICOLI:
      commandFsmInoltro = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_ASSISTENZA_VEICOLI;
      break;
    default:
      log.error("Tipo dispositivo {} not found in enumarations", tipoDispositivo.getNome());
      break;
    }

    log.info("FsmCommand for Inoltro: {}", commandFsmInoltro);
    return commandFsmInoltro;
  }

  private AbstractFsmRichiesta getFsmByCommand(FsmCommand command) {
    return fsmFactory.getFsm(command);
  }

  private Richiesta switchStateWithFsmInoltro(Richiesta richiesta, FsmCommand command, RichiestaEvent event) throws Exception {
    AbstractFsmRichiesta fsm = getFsmByCommand(command);
    richiesta = fsm.executeCommandToChangeState(event, richiesta);
    return richiesta;
  }

  @Override
  public boolean isEqualType(Richiesta richiesta, TipoDispositivoEnum tipoDispositivoFsm) {
    log.info("The FSM Accettazione is equal to all devices");
    return true;
  }

}
