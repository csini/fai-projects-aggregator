package it.fai.ms.efservice.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.dto.AssignDocumentRequestDTO;

@Service
@Transactional
public class AssignDocumentToRequestService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaRepositoryExt richiestaRepo;

  private final DocumentService documentService;

  public AssignDocumentToRequestService(final RichiestaRepositoryExt richiestaRepo, final DocumentService documentService) {
    this.richiestaRepo = richiestaRepo;
    this.documentService = documentService;
  }

  public void assignDocument(AssignDocumentRequestDTO dto) {
    String documentType = dto.getDocumentType()
                             .name();
    String documents = dto.getUuidDocument();
    String identificativoRichiesta = dto.getUuidRequest();
    Richiesta request = richiestaRepo.findOneByIdentificativo(identificativoRichiesta);
    List<String> uuidDocuments = request.getUuidDocuments();
    log.info("Found uuidDocuments {} on Requests: {}", uuidDocuments, identificativoRichiesta);
    if (uuidDocuments == null || uuidDocuments.isEmpty()) {
      log.info("Set documents {}", documents);
    } else {

      String uuidVehicle = null;
      for (String uuid : uuidDocuments) {
        DocumentoDTO documentoDTO = null;
        try {
          documentoDTO = documentService.findByIdentifierAndDocumentType(uuid, documentType)
                                        .get();
          log.info("Foudndocument: {} by identifier: {} - TypeDocument: {}", documentoDTO, uuid, documentType);
        } catch (Exception e) {
          log.error("Exception on call document client with identifier {}", dto, e);
        }
        if (documentoDTO != null) {
          uuidVehicle = uuid;
          break;
        }
      }

      if (StringUtils.isNotBlank(uuidVehicle) && !dto.getUuidDocument()
                                                     .equals(uuidVehicle)) {
        uuidDocuments.remove(uuidVehicle);
      }
      uuidDocuments.add(dto.getUuidDocument());
      documents = String.join(",", uuidDocuments);
    }
    request.setUuidDocumento(documents);
    richiestaRepo.save(request);
    log.info("Assign documents {} on request: {}", documents, identificativoRichiesta);
  }

}
