package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class RuleEngineVehicleId implements Serializable {

  private static final long serialVersionUID = 1882571494067693966L;

  private String id;

  public RuleEngineVehicleId(final String _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineVehicle Id is mandatory"));
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleEngineVehicleId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineVehicleId [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
