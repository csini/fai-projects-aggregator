package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;

import it.fai.common.enumeration.StatoDispositivo;

public class ModificaDispositivoDTO implements Serializable {

  private static final long serialVersionUID = -742897527920963449L;

  private String dmlUniqueIdentifier;

  private StatoDispositivo stato;

  private Instant dataUltimaModificaStato;

  private Instant dataSpedizione;

  private String note;
  
  private String tipoHardware;
  
  private Instant dataPrimaAttivazione;

  public String getDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }

  public void setDmlUniqueIdentifier(String dmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = dmlUniqueIdentifier;
  }

  public Instant getDataUltimaModificaStato() {
    return dataUltimaModificaStato;
  }

  public void setDataUltimaModificaStato(Instant dataUltimaModificaStato) {
    this.dataUltimaModificaStato = dataUltimaModificaStato;
  }

  public StatoDispositivo getStato() {
    return stato;
  }

  public void setStato(StatoDispositivo stato) {
    this.stato = stato;
  }

  public Instant getDataSpedizione() {
    return dataSpedizione;
  }

  public void setDataSpedizione(Instant dataSpedizione) {
    this.dataSpedizione = dataSpedizione;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public String getTipoHardware() {
    return tipoHardware;
  }

  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }

  public Instant getDataPrimaAttivazione() {
    return dataPrimaAttivazione;
  }

  public void setDataPrimaAttivazione(Instant dataPrimaAttivazione) {
    this.dataPrimaAttivazione = dataPrimaAttivazione;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ModificaDispositivoDTO [dmlUniqueIdentifier=");
    builder.append(dmlUniqueIdentifier);
    builder.append(", stato=");
    builder.append(stato);
    builder.append(", dataUltimaModificaStato=");
    builder.append(dataUltimaModificaStato);
    builder.append(", dataSpedizione=");
    builder.append(dataSpedizione);
    builder.append(", note=");
    builder.append(note);
    builder.append(", tipoHardware=");
    builder.append(tipoHardware);
    builder.append(", dataPrimaAttivazione=");
    builder.append(dataPrimaAttivazione);
    builder.append("]");
    return builder.toString();
  }

}