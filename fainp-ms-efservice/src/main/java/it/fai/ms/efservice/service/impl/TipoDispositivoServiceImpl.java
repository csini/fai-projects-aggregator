package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.TipoDispositivoService;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import it.fai.ms.efservice.service.mapper.TipoDispositivoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TipoDispositivo.
 */
@Service
@Transactional
public class TipoDispositivoServiceImpl implements TipoDispositivoService{

    private final Logger log = LoggerFactory.getLogger(TipoDispositivoServiceImpl.class);

    private final TipoDispositivoRepository tipoDispositivoRepository;

    private final TipoDispositivoMapper tipoDispositivoMapper;

    public TipoDispositivoServiceImpl(TipoDispositivoRepository tipoDispositivoRepository, TipoDispositivoMapper tipoDispositivoMapper) {
        this.tipoDispositivoRepository = tipoDispositivoRepository;
        this.tipoDispositivoMapper = tipoDispositivoMapper;
    }

    /**
     * Save a tipoDispositivo.
     *
     * @param tipoDispositivoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TipoDispositivoDTO save(TipoDispositivoDTO tipoDispositivoDTO) {
        log.debug("Request to save TipoDispositivo : {}", tipoDispositivoDTO);
        TipoDispositivo tipoDispositivo = tipoDispositivoMapper.toEntity(tipoDispositivoDTO);
        tipoDispositivo = tipoDispositivoRepository.save(tipoDispositivo);
        return tipoDispositivoMapper.toDto(tipoDispositivo);
    }

    /**
     *  Get all the tipoDispositivos.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TipoDispositivoDTO> findAll() {
        log.debug("Request to get all TipoDispositivos");
        return tipoDispositivoRepository.findAllWithEagerRelationships().stream()
            .map(tipoDispositivoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one tipoDispositivo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TipoDispositivoDTO findOne(Long id) {
        log.debug("Request to get TipoDispositivo : {}", id);
        TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOneWithEagerRelationships(id);
        return tipoDispositivoMapper.toDto(tipoDispositivo);
    }

    /**
     *  Delete the  tipoDispositivo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoDispositivo : {}", id);
        tipoDispositivoRepository.delete(id);
    }
}
