package it.fai.ms.efservice.vehicle.ownership.model;

import java.util.Objects;

public class VehicleOwnershipCompanyCode {

  private String id;

  public VehicleOwnershipCompanyCode(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((VehicleOwnershipCompanyCode) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnershipCompanyCode [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
