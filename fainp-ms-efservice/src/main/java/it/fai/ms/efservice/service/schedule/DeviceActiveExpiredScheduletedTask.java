package it.fai.ms.efservice.service.schedule;

import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.config.ApplicationProperties;
import it.fai.ms.efservice.service.DispositivoServiceExt;

@Component
public class DeviceActiveExpiredScheduletedTask {

  private final Logger log = LoggerFactory.getLogger(ExpireForcedDeviceToSendScheduledTask.class);

  private final DispositivoServiceExt deviceService;

  private final ApplicationProperties applicationProperties;

  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private Boolean enabledSchedule;

  public DeviceActiveExpiredScheduletedTask(final DispositivoServiceExt _deviceService, final ApplicationProperties _applicationProperties) {
    deviceService = _deviceService;
    applicationProperties = _applicationProperties;
  }

  @PostConstruct
  public void init() {
    if (applicationProperties != null) {
      enabledSchedule = applicationProperties.getScheduler()
                                             .isEnable();
    } else {
      log.error("Not initialize boolean value: enabledScheduler");
    }
  }

  // @Scheduled(cron = "*/60 * * * * MON-FRI")
  @Scheduled(cron = "${application.scheduler.deviceexpired}")
  public void run() throws Exception {
    if (enabledSchedule != null && enabledSchedule) {
      log.info("Start {} at {}", DeviceActiveExpiredScheduletedTask.class.getSimpleName(), Instant.now());
      int numDevicesUpdated = deviceService.findActiveDeviceExpiredAndUpdateStateToExpired();
      log.info("Update {} devices with state {}", numDevicesUpdated, StatoDispositivo.SCADUTO);
      log.info("Finish {} at {}", DeviceActiveExpiredScheduletedTask.class.getSimpleName(), Instant.now());
    } else {
      log.info("Device expired scheduleted task is not enabled.");
    }
  }

}
