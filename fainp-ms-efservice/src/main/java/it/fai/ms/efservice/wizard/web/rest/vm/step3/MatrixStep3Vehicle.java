package it.fai.ms.efservice.wizard.web.rest.vm.step3;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixActivabilityFailure;

public class MatrixStep3Vehicle implements Serializable, Comparable<MatrixStep3Vehicle> {

  private static final long serialVersionUID = 5510192375069821504L;

  private List<MatrixActivabilityFailure> errors;
  private String                          state;

  public MatrixStep3Vehicle() {
  }

  @Override
  public int compareTo(final MatrixStep3Vehicle _obj) {
    return state.compareTo(_obj.getState());
  }

  @JsonInclude(Include.NON_NULL)
  public List<MatrixActivabilityFailure> getErrors() {
    return errors;
  }

  @JsonProperty(value = "stato")
  public String getState() {
    return state;
  }

  public void setErrors(final List<MatrixActivabilityFailure> _errors) {
    errors = _errors;
  }

  public void setState(final String _state) {
    state = _state;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixStep3Vehicle [errors=");
    builder.append(this.errors);
    builder.append(", state=");
    builder.append(this.state);
    builder.append("]");
    return builder.toString();
  }

}
