package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.RichiestaDTO;
import java.util.List;

/**
 * Service Interface for managing Richiesta.
 */
public interface RichiestaService {

    /**
     * Save a richiesta.
     *
     * @param richiestaDTO the entity to save
     * @return the persisted entity
     */
    RichiestaDTO save(RichiestaDTO richiestaDTO);

    /**
     *  Get all the richiestas.
     *
     *  @return the list of entities
     */
    List<RichiestaDTO> findAll();

    /**
     *  Get the "id" richiesta.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RichiestaDTO findOne(Long id);

    /**
     *  Delete the "id" richiesta.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
