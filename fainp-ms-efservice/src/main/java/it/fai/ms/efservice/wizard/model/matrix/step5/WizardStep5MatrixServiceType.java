package it.fai.ms.efservice.wizard.model.matrix.step5;

import java.io.Serializable;
import java.util.Objects;

import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;

public class WizardStep5MatrixServiceType implements Serializable, Comparable<WizardStep5MatrixServiceType> {

  private static final long serialVersionUID = -4081371123861673376L;

  private WizardMatrixActivability activability;
  private String                   id;
  private long ordering;

  public WizardStep5MatrixServiceType(final String _id) {
    id = _id;
  }
  
  public WizardStep5MatrixServiceType(final String _id, final WizardMatrixActivability _wizardMatrixActivability, final long _ordering) {
    id = _id;
    activability = _wizardMatrixActivability;
    this.setOrdering(_ordering);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep5MatrixServiceType) _obj).getId(), id)
                 && Objects.equals(((WizardStep5MatrixServiceType) _obj).getActivability(), activability);
    }
    return isEquals;
  }

  public WizardMatrixActivability getActivability() {
    return activability;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, getActivability());
  }

  public boolean isNotActivable() {
    return activability.getActivabilityState()
                       .isKo();
  }

  public void setActivability(final WizardMatrixActivability _activability) {
    activability = _activability;
  }

  @Override
  public int compareTo(final WizardStep5MatrixServiceType _obj) {
    return id.compareTo(_obj.getId());
  }

  public long getOrdering() {
    return ordering;
  }

  public void setOrdering(long ordering) {
    this.ordering = ordering;
  }

  @Override
  public String toString() {
    return "WizardStep5MatrixServiceType [activability=" + activability + ", id=" + id + ", ordering=" + ordering + "]";
  }
}
