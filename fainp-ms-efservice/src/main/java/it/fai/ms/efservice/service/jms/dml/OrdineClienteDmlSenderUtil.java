package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.AbstractDmlSender;
import it.fai.ms.common.dml.efservice.dto.OrdiniClienteDMLDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.service.jms.mapper.OrdiniClienteJmsMapper;

public class OrdineClienteDmlSenderUtil extends AbstractDmlSender<OrdiniClienteDMLDTO,OrdineCliente> {

  static final JmsTopicNames TOPIC_SAVE = JmsTopicNames.DML_ORDINICLIENTE_SAVE;
  static final JmsTopicNames TOPIC_DELETE = JmsTopicNames.DML_ORDINICLIENTE_DELETE;

  private final OrdiniClienteJmsMapper ordiniClienteJmsMapper = new OrdiniClienteJmsMapper();

  public OrdineClienteDmlSenderUtil(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

  @Override
  protected OrdiniClienteDMLDTO getSavingDmlDto(OrdineCliente ord) {
    return ordiniClienteJmsMapper.toOrdineClienteDMLDTO(ord);
  }

  @Override
  protected OrdiniClienteDMLDTO getDeletingDmlDto(String dmlUniqueIdentifieri){
    return ordiniClienteJmsMapper.toEmptyOrdiniClienteDMLDTO(dmlUniqueIdentifieri);
  }

//  public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
//  {
//      Map<Object, Boolean> map = new ConcurrentHashMap<>();
//      return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
//  }

}
