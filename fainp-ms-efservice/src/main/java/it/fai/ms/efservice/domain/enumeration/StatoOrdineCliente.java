package it.fai.ms.efservice.domain.enumeration;

/**
 * The StatoOrdineCliente enumeration.
 */
public enum StatoOrdineCliente {
    DA_EVADERE, LAVORATO_PARZIALMENTE, ACCETTATO, ACCETTATO_PARZIALE, RIFIUTATO, SOSPESO, SOSPESO_E_LAVORATO_PARZIALMENTE, COMPLETATO
}
