/**
 * 
 */
package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

public class FsmActionContrattoGeneric implements Action<StatoContratto, ContrattoEvent> {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void execute(StateContext<StatoContratto, ContrattoEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Contratto) {
      Contratto contratto = (Contratto) obj;
      logger.debug("Generic FSM Action to Contratto [ID: {} - Codice: {}]", (contratto != null) ? contratto.getId() : null,
                   (contratto != null) ? contratto.getCodContrattoCliente() : null);
    }
  }

}
