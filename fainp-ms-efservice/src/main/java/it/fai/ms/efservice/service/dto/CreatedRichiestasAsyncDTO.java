package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import it.fai.ms.efservice.enumeration.StateCreationRichiesta;

public class CreatedRichiestasAsyncDTO implements Serializable {

  private static final long serialVersionUID = 2501684277868736241L;

  private StateCreationRichiesta state;

  private String numeroOrdineCliente;

  private WarningRichiestaDTO warningRichiestaDTO;

  public CreatedRichiestasAsyncDTO(StateCreationRichiesta state) {
    this.state = state;
  }

  public CreatedRichiestasAsyncDTO(StateCreationRichiesta state, String numeroOrdine) {
    this.state = state;
    this.numeroOrdineCliente = numeroOrdine;
  }

  public StateCreationRichiesta getState() {
    return state;
  }

  public CreatedRichiestasAsyncDTO state(StateCreationRichiesta state) {
    this.state = state;
    return this;
  }

  public void setState(StateCreationRichiesta state) {
    this.state = state;
  }

  public WarningRichiestaDTO getWarningRichiestaDTO() {
    return warningRichiestaDTO;
  }

  public CreatedRichiestasAsyncDTO warningRichiestaDTO(WarningRichiestaDTO warningRichiestaDTO) {
    this.warningRichiestaDTO = warningRichiestaDTO;
    return this;
  }

  public void setWarningRichiestaDTO(WarningRichiestaDTO warningRichiestaDTO) {
    this.warningRichiestaDTO = warningRichiestaDTO;
  }

  public String getNumeroOrdineCliente() {
    return numeroOrdineCliente;
  }

  public CreatedRichiestasAsyncDTO numeroOrdineCliente(String numeroOrdineCliente) {
    this.numeroOrdineCliente = numeroOrdineCliente;
    return this;
  }

  public void setNumeroOrdineCliente(String numeroOrdineCliente) {
    this.numeroOrdineCliente = numeroOrdineCliente;
  }

}
