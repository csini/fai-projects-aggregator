package it.fai.ms.efservice.service.fsm.config.viatoll;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceAssociationMessageProducerService;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroViaTollConfig.INOLTRO_VIATOLL)
public class FsmInoltroViaTollConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_VIATOLL = "inoltroViaToll";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  private final ManageDevicesInStorageService deviceInStorageServiceExt;

  private final DeviceProducerService deviceProducerService;

  private final DeviceAssociationMessageProducerService deviceAssociationProduceService;

  private final ApplicationEventPublisher applicationEventPublisher;

  private final ManageDeviceSentModeService manageDeviceSentModeService;

  private final GeneratorContractCodeService generatorContractCodeService;

  public FsmInoltroViaTollConfig(final FsmSenderToQueue _senderFsmService, final ManageDevicesInStorageService _deviceInStorageServiceExt,
                                 final DeviceProducerService _deviceProducerService,
                                 final DeviceAssociationMessageProducerService _deviceAssociationProduceService,
                                 final ApplicationEventPublisher _applicationEventPublisher,
                                 final ManageDeviceSentModeService _manageDeviceSentModeService,
                                 final GeneratorContractCodeService generatorContractCodeService) {
    senderFsmService = _senderFsmService;
    deviceInStorageServiceExt = _deviceInStorageServiceExt;
    deviceAssociationProduceService = _deviceAssociationProduceService;
    applicationEventPublisher = _applicationEventPublisher;
    manageDeviceSentModeService = _manageDeviceSentModeService;
    deviceProducerService = _deviceProducerService;
    this.generatorContractCodeService = generatorContractCodeService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_VIATOLL.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroViaToll();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    // transitions.withExternal()
    // .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
    // .target(StatoRichiesta.ACCETTATO)
    // .event(RichiestaEvent.INITIAL)
    // .action(new FsmActionGeneric())
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.ACCETTATO)
    // .target(StatoRichiesta.IN_ATTESA_PICKUPCODE)
    // .event(RichiestaEvent.MU_PICK_UP_CODE_REQUEST)
    // .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ATTESA_PICKUPCODE))
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.IN_ATTESA_PICKUPCODE)
    // .target(StatoRichiesta.COMPLETO)
    // .event(RichiestaEvent.MU_PICK_UP_CODE_OK)
    // .action(new FsmActionGenerateContratto(generatorContractCodeService))
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.COMPLETO)
    // .target(StatoRichiesta.IN_ATTESA_RITIRO_NON_RITIRATO)
    // .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ATTIVO))
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.IN_ATTESA_RITIRO_NON_RITIRATO)
    // .target(StatoRichiesta.ANNULLATO_UTENTE)
    // .event(RichiestaEvent.MU_ANNULLAMENTO_UTENTE)
    // .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ANNULLAMENTO))
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.IN_ATTESA_RITIRO_NON_RITIRATO)
    // .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
    // .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
    // .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ANNULLAMENTO))
    // .and()
    // .withExternal()
    // .source(StatoRichiesta.IN_ATTESA_RITIRO_NON_RITIRATO)
    // .target(StatoRichiesta.ATTIVO_EVASO)
    // .event(RichiestaEvent.MU_FILE_CSV)
    // .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ATTIVO));
    transitions.withExternal()// MANUAL
               .source(getStatoRichiestaInitial())
               .target(StatoRichiesta.INIZIALE)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.MU_SOSPENDI)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO);
  }

}
