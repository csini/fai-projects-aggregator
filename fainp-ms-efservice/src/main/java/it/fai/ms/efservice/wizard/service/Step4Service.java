package it.fai.ms.efservice.wizard.service;

import java.util.List;
import java.util.Set;

import it.fai.ms.efservice.dto.WizardVehicleDTO;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4Matrix;

public interface Step4Service {

  WizardStep4Matrix buildMatrix(WizardServiceTypeId _serviceTypeId, Set<WizardVehicleId> _wizardVehicleIds, String codiceCliente);

  WizardStep4Matrix buildCompleteMatrix(List<WizardVehicleDTO> wizardVehiclesDTO, String codiceCliente);

}
