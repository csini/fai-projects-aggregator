package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

/**
 * A DTO for the Dispositivo entity.
 */
public class DispositivoDTO implements Serializable {

  private static final long serialVersionUID = -5629809548246308865L;

  public enum StatoFrontend {
    GREEN, GREY, YELLOW
  }

  private Long id;

  @NotNull
  private String seriale;

  private String identificativo;

  @NotNull
  private StatoDispositivo stato;

  private Instant dataSpedizione;

  private Instant dataPrimaAttivazione;

  private String trackingNumber;

  private String shipmentReference;

  private ModalitaSpedizione modalitaSpedizione;

  private Long contrattoId;

  private String contrattoCodContrattoCliente;

  private Long tipoDispositivoId;

  private String tipoDispositivoNome;

  private Boolean isOperazionePending;

  private Long idOrdineOperazioneModifica;

  private TipoRichiesta tipoOperazioneModifica;

  private List<String> operazioniModificaPossibili;

  private Boolean isFileAllegato;

  private Set<StatoDispositivoServizioDTO> statoDispositivoServizios;

  private Set<AssociazioneDVDTO> associazioneDVs;

  private TipoMagazzino tipoMagazzino;

  private String operazioniPossibili;

  private Instant dataModificaStato;

  private ZonedDateTime dataScadenza;

  private String targa2;

  private String targa3;

  private ZonedDateTime targaDataValidita;

  private String tipoHardware;

  private  String requestUuid;

  /**
   * marco.murdocca Only in DTO
   */
  private StatoFrontend statoFrontend;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public StatoDispositivo getStato() {
    return stato;
  }

  public void setStato(StatoDispositivo stato) {
    this.stato = stato;
  }

  public Instant getDataSpedizione() {
    return dataSpedizione;
  }

  public void setDataSpedizione(Instant dataSpedizione) {
    this.dataSpedizione = dataSpedizione;
  }

  public Instant getDataPrimaAttivazione() {
    return dataPrimaAttivazione;
  }

  public void setDataPrimaAttivazione(Instant dataPrimaAttivazione) {
    this.dataPrimaAttivazione = dataPrimaAttivazione;
  }

  public String getTrackingNumber() {
    return trackingNumber;
  }

  public void setTrackingNumber(String trackingNumber) {
    this.trackingNumber = trackingNumber;
  }

  public String getShipmentReference() {
    return shipmentReference;
  }

  public void setShipmentReference(String shipmentReference) {
    this.shipmentReference = shipmentReference;
  }

  public ModalitaSpedizione getModalitaSpedizione() {
    return modalitaSpedizione;
  }

  public void setModalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
  }

  public Long getContrattoId() {
    return contrattoId;
  }

  public void setContrattoId(Long contrattoId) {
    this.contrattoId = contrattoId;
  }

  public String getContrattoCodContrattoCliente() {
    return contrattoCodContrattoCliente;
  }

  public void setContrattoCodContrattoCliente(String contrattoCodContrattoCliente) {
    this.contrattoCodContrattoCliente = contrattoCodContrattoCliente;
  }

  public Long getTipoDispositivoId() {
    return tipoDispositivoId;
  }

  public void setTipoDispositivoId(Long tipoDispositivoId) {
    this.tipoDispositivoId = tipoDispositivoId;
  }

  public String getTipoDispositivoNome() {
    return tipoDispositivoNome;
  }

  public void setTipoDispositivoNome(String tipoDispositivoNome) {
    this.tipoDispositivoNome = tipoDispositivoNome;
  }

  public Set<StatoDispositivoServizioDTO> getStatoDispositivoServizios() {
    return statoDispositivoServizios;
  }

  public void setStatoDispositivoServizios(Set<StatoDispositivoServizioDTO> statoDispositivoServizios) {
    this.statoDispositivoServizios = statoDispositivoServizios;
  }

  public Set<AssociazioneDVDTO> getAssociazioneDVs() {
    return associazioneDVs;
  }

  public void setAssociazioneDVs(Set<AssociazioneDVDTO> associazioneDVs) {
    this.associazioneDVs = associazioneDVs;
  }

  public Boolean getOperazionePending() {
    return isOperazionePending;
  }

  public void setOperazionePending(Boolean operazionePending) {
    isOperazionePending = operazionePending;
  }

  public Long getIdOrdineOperazioneModifica() {
    return idOrdineOperazioneModifica;
  }

  public void setIdOrdineOperazioneModifica(Long idOrdineOperazioneModifica) {
    this.idOrdineOperazioneModifica = idOrdineOperazioneModifica;
  }

  public TipoRichiesta getTipoOperazioneModifica() {
    return tipoOperazioneModifica;
  }

  public void setTipoOperazioneModifica(TipoRichiesta tipoOperazioneModifica) {
    this.tipoOperazioneModifica = tipoOperazioneModifica;
  }

  public List<String> getOperazioniModificaPossibili() {
    return operazioniModificaPossibili;
  }

  public void setOperazioniModificaPossibili(List<String> operazioniModificaPossibili) {
    if(this.operazioniModificaPossibili == null) {
      this.operazioniModificaPossibili = new ArrayList<>();
    }
    this.operazioniModificaPossibili = operazioniModificaPossibili;
  }

  public Boolean getFileAllegato() {
    return isFileAllegato;
  }

  public void setFileAllegato(Boolean fileAllegato) {
    isFileAllegato = fileAllegato;
  }

  /**
   * marco.murdocca Only in DTO
   */
  public StatoFrontend getStatoFrontend() {
    setStatoFrontend(StatoFrontend.GREY);
    if (this.stato != null) {
      if(this.stato.isYellow()) {
        setStatoFrontend(StatoFrontend.YELLOW);
      } else if (this.stato.isGreen()) {
        setStatoFrontend(StatoFrontend.GREEN);
      }
    }

    return this.statoFrontend;
  }

  /**
   * marco.murdocca Only in DTO
   */
  public void setStatoFrontend(StatoFrontend statoFrontend) {
    this.statoFrontend = statoFrontend;
  }

  public TipoMagazzino getTipoMagazzino() {
    return tipoMagazzino;
  }

  public void setTipoMagazzino(TipoMagazzino tipoMagazzino) {
    this.tipoMagazzino = tipoMagazzino;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public ZonedDateTime getDataScadenza() {
    return dataScadenza;
  }

  public void setDataScadenza(ZonedDateTime dataScadenza) {
    this.dataScadenza = dataScadenza;
  }

  public String getTarga2() {
    return targa2;
  }

  public void setTarga2(String targa2) {
    this.targa2 = targa2;
  }

  public String getTarga3() {
    return targa3;
  }

  public void setTarga3(String targa3) {
    this.targa3 = targa3;
  }

  public ZonedDateTime getTargaDataValidita() {
    return targaDataValidita;
  }

  public void setTargaDataValidita(ZonedDateTime targaDataValidita) {
    this.targaDataValidita = targaDataValidita;
  }

  public String getTipoHardware() {
    return tipoHardware;
  }

  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    DispositivoDTO dispositivoDTO = (DispositivoDTO) o;
    if (dispositivoDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), dispositivoDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DispositivoDTO [id=");
    builder.append(id);
    builder.append(", seriale=");
    builder.append(seriale);
    builder.append(", identificativo=");
    builder.append(identificativo);
    builder.append(", stato=");
    builder.append(stato);
    builder.append(", dataSpedizione=");
    builder.append(dataSpedizione);
    builder.append(", dataPrimaAttivazione=");
    builder.append(dataPrimaAttivazione);
    builder.append(", trackingNumber=");
    builder.append(trackingNumber);
    builder.append(", shipmentReference=");
    builder.append(shipmentReference);
    builder.append(", modalitaSpedizione=");
    builder.append(modalitaSpedizione);
    builder.append(", contrattoId=");
    builder.append(contrattoId);
    builder.append(", contrattoCodContrattoCliente=");
    builder.append(contrattoCodContrattoCliente);
    builder.append(", tipoDispositivoId=");
    builder.append(tipoDispositivoId);
    builder.append(", tipoDispositivoNome=");
    builder.append(tipoDispositivoNome);
    builder.append(", isOperazionePending=");
    builder.append(isOperazionePending);
    builder.append(", idOrdineOperazioneModifica=");
    builder.append(idOrdineOperazioneModifica);
    builder.append(", tipoOperazioneModifica=");
    builder.append(tipoOperazioneModifica);
    builder.append(", isFileAllegato=");
    builder.append(isFileAllegato);
    builder.append(", statoDispositivoServizios=");
    builder.append(statoDispositivoServizios);
    builder.append(", associazioneDVs=");
    builder.append(associazioneDVs);
    builder.append(", statoFrontend=");
    builder.append(statoFrontend);
    builder.append(", tipoMagazzino=");
    builder.append(tipoMagazzino);
    builder.append(", operazioniPossibili=");
    builder.append(tipoMagazzino);
    builder.append("]");
    return builder.toString();
  }

  public String getRequestUuid() {
    return requestUuid;
  }

  public void setRequestUuid(String requestUuid) {
    this.requestUuid = requestUuid;
  }

}
