package it.fai.ms.efservice.service.jms.listener.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.device.DeviceUpdateMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceUpdateConsumer;

@Service
@Transactional
public class JmsDeviceUpdateMessageListener
  extends JmsObjectMessageListenerTemplate<DeviceUpdateMessage>
  implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceUpdateConsumer deviceUpdateConsumer;

  public JmsDeviceUpdateMessageListener(DeviceUpdateConsumer deviceUpdateConsumer) {
    this.deviceUpdateConsumer = deviceUpdateConsumer;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_UPDATES;
  }

  @Override
  protected void consumeMessage(DeviceUpdateMessage typedMessage) {
    log.debug("Received [{}]", typedMessage);

    try {
      deviceUpdateConsumer.consume(typedMessage);
    } catch (RuntimeException e){
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    log.debug("TrackyCardOrder consumed");
  }

}
