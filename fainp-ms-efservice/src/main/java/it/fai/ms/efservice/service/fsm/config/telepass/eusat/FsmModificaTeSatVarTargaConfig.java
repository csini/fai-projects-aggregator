package it.fai.ms.efservice.service.fsm.config.telepass.eusat;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDeviceLicensePlate;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceLicensePlateChangeMessageProducerService;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTeSatVarTargaConfig.MOD_TE_SAT_VARTARGA)
public class FsmModificaTeSatVarTargaConfig extends FsmRichiestaConfig {

  public static final String MOD_TE_SAT_VARTARGA = "ModTeSatVarTarga";

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTeSatVarTargaConfig.class);

  private final DeviceLicensePlateChangeMessageProducerService deviceLicensePlateProducer;

  public FsmModificaTeSatVarTargaConfig(final DeviceLicensePlateChangeMessageProducerService _deviceLicensePlateProducer) {
    deviceLicensePlateProducer = _deviceLicensePlateProducer;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TE_SAT_VARIAZIONE_TARGA.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfTeSatVarTarga();
  }

  // Use Action to send TARRC;
  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(new FsmActionDeviceLicensePlate(deviceLicensePlateProducer))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(new FsmActionDeviceLicensePlate(deviceLicensePlateProducer))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(new FsmActionDeviceLicensePlate(deviceLicensePlateProducer))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.RESPONSE_OK);
  }

}
