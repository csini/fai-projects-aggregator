package it.fai.ms.efservice.client;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.common.jms.dto.TransitDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

@FeignClient(name = "faitrackycard")
public interface TrackyCardClient {

  String BASE_PATH = "/api";
  String TRACKY_CARD_FILE = BASE_PATH + "/transit-serial-number/file";
  String TRACKY_CARD_RESOURCE = BASE_PATH + "/tracky-cards/pin";


  @GetMapping(TRACKY_CARD_FILE + "/{uuid}")
  @Timed
  List<TransitDTO> trackyCardFileUpdate(
    @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
    @PathVariable("uuid") String uuid
  );

  @GetMapping(TRACKY_CARD_RESOURCE + "/{deviceCode}")
  @Timed
  String trackcardPin(
    @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
    @RequestParam("deviceCode") String deviceCode
  );


  }
