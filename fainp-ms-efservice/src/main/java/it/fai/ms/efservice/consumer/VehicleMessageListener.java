package it.fai.ms.efservice.consumer;

import java.util.Map;

import it.fai.ms.efservice.consumer.model.VehicleMessage;

public interface VehicleMessageListener {

  void receive(VehicleMessage vehicleMessage);
  
  void persistVehicleNoTargaScorta(Map<String, String> mapVehicle);

}
