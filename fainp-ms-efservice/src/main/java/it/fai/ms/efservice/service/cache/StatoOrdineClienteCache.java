package it.fai.ms.efservice.service.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StatoOrdineClienteCache {

  private Logger log = LoggerFactory.getLogger(getClass());

  // private Cache<String, String> cache;

  private Map<String, String> cache;

  public StatoOrdineClienteCache() {
  }

  // public StatoOrdineClienteCache(@Qualifier("stato_ordine_cliente") Cache<String, String> _cache) {
  // cache = _cache;
  // }

  @PostConstruct
  public void init() {
    if (cache == null) {
      log.info("Init Cache OrdiniCliente on MEMORY with HashMap");
      cache = new HashMap<>();
    }
  }

  public Set<String> getAllKeys() {
    Set<String> keys = cache.keySet();
    return keys;
  }

  public void putObject(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    if (cache.containsKey(key)) {
      log.trace("Skip insert keyValue {} on cache", key);
    } else {
      cache.put(key, json);
    }
  }

  public String getObject(String key) {
    log.debug("Get object from Map with Key: {}", key);
    String json = cache.get(key);
    return json;
  }

  public void clearCache() {
    cache.clear();
  }

  public void expireValueByKeys(Set<String> allKeys) {
    for (String key : allKeys) {
      if (key != null && cache.containsKey(key)) {
        String removed = cache.remove(key);
        log.debug("Removed {} from cache", removed);
      }
    }
  }

}
