/**
 *
 */
package it.fai.ms.efservice.service.dto;

import java.util.List;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

/**
 * @author Luca Vassallo
 */
public class CarrelloModificaRichiestaDTO {

  private TipoDispositivoEnum tipoDispositivo;

  private String codiceAzienda;

  private List<String> dispositivi;

  private TipoRichiesta tipoOperazioneModifica;

  private String documento;

  private String veicolo;

  private String newVeicolo;

  private String targa;

  private String nazione;

  private String richiedente;

  private Long idRichiestaOperazioneModifica;

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public CarrelloModificaRichiestaDTO tipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public CarrelloModificaRichiestaDTO codiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
    return this;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public List<String> getDispositivi() {
    return dispositivi;
  }

  public CarrelloModificaRichiestaDTO dispositivi(List<String> dispositivi) {
    this.dispositivi = dispositivi;
    return this;
  }

  public CarrelloModificaRichiestaDTO addDispositivo(String dispositivo) {
    this.dispositivi.add(dispositivo);
    return this;
  }

  public CarrelloModificaRichiestaDTO removeDispositivo(String dispositivo) {
    this.dispositivi.remove(dispositivo);
    return this;
  }

  public void setDispositivi(List<String> dispositivi) {
    this.dispositivi = dispositivi;
  }

  public TipoRichiesta getTipoOperazioneModifica() {
    return tipoOperazioneModifica;
  }

  public CarrelloModificaRichiestaDTO tipoOperazioneModifica(TipoRichiesta tipoOperazioneModifica) {
    this.tipoOperazioneModifica = tipoOperazioneModifica;
    return this;
  }

  public void setTipoOperazioneModifica(TipoRichiesta tipoOperazioneModifica) {
    this.tipoOperazioneModifica = tipoOperazioneModifica;
  }

  public String getDocumento() {
    return documento;
  }

  public CarrelloModificaRichiestaDTO documento(String documento) {
    this.documento = documento;
    return this;
  }

  public void setDocumento(String documento) {
    this.documento = documento;
  }

  public String getVeicolo() {
    return veicolo;
  }

  public CarrelloModificaRichiestaDTO veicolo(String veicolo) {
    this.veicolo = veicolo;
    return this;
  }

  public void setVeicolo(String veicolo) {
    this.veicolo = veicolo;
  }

  public String getNewVeicolo() {
    return newVeicolo;
  }

  public CarrelloModificaRichiestaDTO newVeicolo(String newVeicolo) {
    this.newVeicolo = newVeicolo;
    return this;
  }

  public void setNewVeicolo(String newVeicolo) {
    this.newVeicolo = newVeicolo;
  }

  public String getTarga() {
    return targa;
  }

  public CarrelloModificaRichiestaDTO targa(String targa) {
    this.targa = targa;
    return this;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getNazione() {
    return nazione;
  }

  public CarrelloModificaRichiestaDTO nazione(String nazione) {
    this.nazione = nazione;
    return this;
  }

  public void setNazione(String nazione) {
    this.nazione = nazione;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public CarrelloModificaRichiestaDTO richiedente(String richiedente) {
    this.richiedente = richiedente;
    return this;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }

  public Long getIdRichiestaOperazioneModifica() {
    return idRichiestaOperazioneModifica;
  }

  public void setIdRichiestaOperazioneModifica(Long idRichiestaOperazioneModifica) {
    this.idRichiestaOperazioneModifica = idRichiestaOperazioneModifica;
  }

  @Override
  public String toString() {
    return "CarrelloModificaRichiestaDTO [tipoDispositivo=" + tipoDispositivo + ", codiceAzienda=" + codiceAzienda + ", dispositivi="
           + dispositivi + ", tipoOperazioneModifica=" + tipoOperazioneModifica + ", documento=" + documento + ", veicolo=" + veicolo
           + ", newVeicolo=" + newVeicolo + ", targa=" + targa + ", nazione=" + nazione + ", richiedente=" + richiedente
           + ", idRichiestaOperazioneModifica=" + idRichiestaOperazioneModifica + "]";
  }

}
