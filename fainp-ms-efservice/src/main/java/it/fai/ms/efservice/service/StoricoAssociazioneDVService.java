package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.StoricoAssociazioneDVDTO;
import java.util.List;

/**
 * Service Interface for managing StoricoAssociazioneDV.
 */
public interface StoricoAssociazioneDVService {

    /**
     * Save a storicoAssociazioneDV.
     *
     * @param storicoAssociazioneDVDTO the entity to save
     * @return the persisted entity
     */
    StoricoAssociazioneDVDTO save(StoricoAssociazioneDVDTO storicoAssociazioneDVDTO);

    /**
     *  Get all the storicoAssociazioneDVS.
     *
     *  @return the list of entities
     */
    List<StoricoAssociazioneDVDTO> findAll();

    /**
     *  Get the "id" storicoAssociazioneDV.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    StoricoAssociazioneDVDTO findOne(Long id);

    /**
     *  Delete the "id" storicoAssociazioneDV.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
