package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.ContrattoService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Contratto.
 */
@RestController
@RequestMapping("/api")
public class ContrattoResource {

    private final Logger log = LoggerFactory.getLogger(ContrattoResource.class);

    private static final String ENTITY_NAME = "contratto";

    private final ContrattoService contrattoService;

    public ContrattoResource(ContrattoService contrattoService) {
        this.contrattoService = contrattoService;
    }

    /**
     * POST  /contrattos : Create a new contratto.
     *
     * @param contrattoDTO the contrattoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new contrattoDTO, or with status 400 (Bad Request) if the contratto has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/contrattos")
    @Timed
    public ResponseEntity<ContrattoDTO> createContratto(@RequestBody ContrattoDTO contrattoDTO) throws URISyntaxException {
        log.debug("REST request to save Contratto : {}", contrattoDTO);
        if (contrattoDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new contratto cannot already have an ID")).body(null);
        }
        ContrattoDTO result = contrattoService.save(contrattoDTO);
        return ResponseEntity.created(new URI("/api/contrattos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /contrattos : Updates an existing contratto.
     *
     * @param contrattoDTO the contrattoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated contrattoDTO,
     * or with status 400 (Bad Request) if the contrattoDTO is not valid,
     * or with status 500 (Internal Server Error) if the contrattoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/contrattos")
    @Timed
    public ResponseEntity<ContrattoDTO> updateContratto(@RequestBody ContrattoDTO contrattoDTO) throws URISyntaxException {
        log.debug("REST request to update Contratto : {}", contrattoDTO);
        if (contrattoDTO.getId() == null) {
            return createContratto(contrattoDTO);
        }
        ContrattoDTO result = contrattoService.save(contrattoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, contrattoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /contrattos : get all the contrattos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of contrattos in body
     */
    @GetMapping("/contrattos")
    @Timed
    public List<ContrattoDTO> getAllContrattos() {
        log.debug("REST request to get all Contrattos");
        return contrattoService.findAll();
        }

    /**
     * GET  /contrattos/:id : get the "id" contratto.
     *
     * @param id the id of the contrattoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the contrattoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/contrattos/{id}")
    @Timed
    public ResponseEntity<ContrattoDTO> getContratto(@PathVariable Long id) {
        log.debug("REST request to get Contratto : {}", id);
        ContrattoDTO contrattoDTO = contrattoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(contrattoDTO));
    }

    /**
     * DELETE  /contrattos/:id : delete the "id" contratto.
     *
     * @param id the id of the contrattoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/contrattos/{id}")
    @Timed
    public ResponseEntity<Void> deleteContratto(@PathVariable Long id) {
        log.debug("REST request to delete Contratto : {}", id);
        contrattoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
