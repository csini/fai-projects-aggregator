package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.SpedizioneClienteView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface SpedizioneClienteViewRepositoryExt extends
  JpaRepository<SpedizioneClienteView, Long>,
  JpaSpecificationExecutor<SpedizioneClienteView> {

}
