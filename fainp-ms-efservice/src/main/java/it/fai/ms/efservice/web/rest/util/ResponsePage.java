package it.fai.ms.efservice.web.rest.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;

import java.util.List;

public class ResponsePage<T> {

  @JsonProperty
  private List<T> content;

  @JsonProperty
  private PageMetadata pageMetadata;

  public ResponsePage(Page<T> page) {
    this.content = page.getContent();
    this.pageMetadata = new PageMetadata(
      page.getNumber(),
      page.getSize(),
      page.getTotalElements(),
      page.getTotalPages(),
      page.getNumberOfElements()
    );
  }

  public List<T> getContent() {
    return content;
  }

  public void setContent(List<T> content) {
    this.content = content;
  }

  public PageMetadata getPageMetadata() {
    return pageMetadata;
  }

  public void setPageMetadata(PageMetadata pageMetadata) {
    this.pageMetadata = pageMetadata;
  }

  class PageMetadata {
    PageMetadata(
      int number,
      int size,
      Long totalElements,
      int totalPages,
      int numberOfElements
    ) {
      this.number = number;
      this.size = String.valueOf(size);
      this.totalElements = String.valueOf(totalElements);
      this.totalPages = String.valueOf(totalPages);
      this.numberOfElements = String.valueOf(numberOfElements);
    }

    @JsonProperty("currentPage")
    private int number;
    @JsonProperty("itemsPerPage")
    private String size;
    @JsonProperty("totalItems")
    private String totalElements;
    @JsonProperty("totalPages")
    private String totalPages;
    @JsonProperty("currentPageItems")
    private String numberOfElements;

    public String getNumber() {
      // PageableHandlerMethodArgumentResolver bean will only parse the page number but will not convert it back.
      return String.valueOf(number + 1);
    }

    public void setNumber(int number) {
      this.number = number;
    }

    public String getSize() {
      return size;
    }

    public void setSize(String size) {
      this.size = size;
    }

    public String getTotalElements() {
      return totalElements;
    }

    public void setTotalElements(String totalElements) {
      this.totalElements = totalElements;
    }

    public String getTotalPages() {
      return this.totalPages;
    }

    public String getNumberOfElements() {
      return numberOfElements;
    }
  }

}
