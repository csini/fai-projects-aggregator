package it.fai.ms.efservice.service.async.impl;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.enumeration.StateCreationRichiesta;
import it.fai.ms.efservice.service.async.CreationOrdineAsyncService;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CreatedRichiestasAsyncDTO;

@Service
public class CreationOrdineAsyncServiceImpl implements CreationOrdineAsyncService {

  private final static String PREFIX_ORDINE = "ORD";

  private final Logger log = LoggerFactory.getLogger(getClass().getName());

  final AsyncJobServiceImpl                  asyncJobService;
  final ManageCreationOrdineAsyncServiceImpl manageOrdineAsync;

  public CreationOrdineAsyncServiceImpl(ManageCreationOrdineAsyncServiceImpl _manageOrdineAsync, AsyncJobServiceImpl _asyncJobService) {
    manageOrdineAsync = _manageOrdineAsync;
    asyncJobService = _asyncJobService;
  }

  @Override
  public String getKeyCacheAndPushDefaultValue() {
    CreatedRichiestasAsyncDTO createdRichiestasAsyncDTO = new CreatedRichiestasAsyncDTO(StateCreationRichiesta.PENDING);
    return asyncJobService.generateKeyCacheAndPushValue(PREFIX_ORDINE, createdRichiestasAsyncDTO);
  }

  @Override
  public void generateOrdineAsyncByCarrelloDTO(String identificativoJob, CarrelloDTO carrelloDTO) {
    log.info("Start async generate Ordine for Identificativo JOB {}", identificativoJob);
    CompletableFuture.supplyAsync(() -> {
      return manageOrdineAsync.generateOrdineAsync(carrelloDTO);
    })
                     .exceptionally(e -> {
                       log.error("Exception in generate ordine ASYNC by carrello DTO: ", e);
                       return new CreatedRichiestasAsyncDTO(StateCreationRichiesta.ERROR);
                     })
                     .thenAccept(dto -> {
                       log.info("Update value: {} for KEY-JOB: {}", dto, identificativoJob);
                       asyncJobService.save(identificativoJob, dto);
                     });
  }

}
