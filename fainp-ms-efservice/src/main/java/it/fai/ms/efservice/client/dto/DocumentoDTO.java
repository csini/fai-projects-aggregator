package it.fai.ms.efservice.client.dto;



import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

public class DocumentoDTO implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long id;

  private LocalDate dataDocumento;
  private String cliente;
  private String ragioneSociale;
  private LocalDate dataCompetenza;
  private Boolean archiviato;
  private LocalDate data_archiviazione;
  private String nome;
  private Instant dataBatch;
  private String identificativo;


  public DocumentoDTO() {
  }

  public DocumentoDTO(Long id, LocalDate dataDocumento, String cliente, String ragioneSociale, LocalDate dataCompetenza, Boolean archiviato, LocalDate data_archiviazione, String nome, Instant dataBatch, String identificativo) {
    this.id = id;
    this.dataDocumento = dataDocumento;
    this.cliente = cliente;
    this.ragioneSociale = ragioneSociale;
    this.dataCompetenza = dataCompetenza;
    this.archiviato = archiviato;
    this.data_archiviazione = data_archiviazione;
    this.nome = nome;
    this.dataBatch = dataBatch;
    this.identificativo = identificativo;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDate getDataDocumento() {
    return dataDocumento;
  }

  public void setDataDocumento(LocalDate dataDocumento) {
    this.dataDocumento = dataDocumento;
  }

  public String getCliente() {
    return cliente;
  }

  public void setCliente(String cliente) {
    this.cliente = cliente;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public LocalDate getDataCompetenza() {
    return dataCompetenza;
  }

  public void setDataCompetenza(LocalDate dataCompetenza) {
    this.dataCompetenza = dataCompetenza;
  }

  public Boolean getArchiviato() {
    return archiviato;
  }

  public void setArchiviato(Boolean archiviato) {
    this.archiviato = archiviato;
  }

  public LocalDate getData_archiviazione() {
    return data_archiviazione;
  }

  public void setData_archiviazione(LocalDate data_archiviazione) {
    this.data_archiviazione = data_archiviazione;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Instant getDataBatch() {
    return dataBatch;
  }

  public void setDataBatch(Instant dataBatch) {
    this.dataBatch = dataBatch;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }


  public DocumentoDTO id(Long id) {
    this.id = id;
    return this;
  }

  public DocumentoDTO dataDocumento(LocalDate dataDocumento) {
    this.dataDocumento = dataDocumento;
    return this;
  }

  public DocumentoDTO cliente(String cliente) {
    this.cliente = cliente;
    return this;
  }

  public DocumentoDTO ragioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
    return this;
  }

  public DocumentoDTO dataCompetenza(LocalDate dataCompetenza) {
    this.dataCompetenza = dataCompetenza;
    return this;
  }

  public DocumentoDTO archiviato(Boolean archiviato) {
    this.archiviato = archiviato;
    return this;
  }

  public DocumentoDTO data_archiviazione(LocalDate data_archiviazione) {
    this.data_archiviazione = data_archiviazione;
    return this;
  }

  public DocumentoDTO nome(String nome) {
    this.nome = nome;
    return this;
  }

  public DocumentoDTO dataBatch(Instant dataBatch) {
    this.dataBatch = dataBatch;
    return this;
  }

  public DocumentoDTO identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DocumentoDTO that = (DocumentoDTO) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {

    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "DocumentoDTO{" +
      "id=" + id +
      ", dataDocumento=" + dataDocumento +
      ", cliente='" + cliente + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", dataCompetenza=" + dataCompetenza +
      ", archiviato=" + archiviato +
      ", data_archiviazione=" + data_archiviazione +
      ", nome='" + nome + '\'' +
      ", dataBatch=" + dataBatch +
      ", identificativo='" + identificativo + '\'' +
      '}';
  }
}
