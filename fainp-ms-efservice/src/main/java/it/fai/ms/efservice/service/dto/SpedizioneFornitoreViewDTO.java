package it.fai.ms.efservice.service.dto;

public class SpedizioneFornitoreViewDTO {

  private Long id;

  private String produttore;

  private Long numeroDispositiviSpedibili = 0L;

  private Long numeroDispositiviDisponibili = 0L;

  private Long numeroDispositiviDaRicevere = 0L;

  private String tipoDispositivo;

  private Boolean notcheckable = false;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  public Long getNumeroDispositiviDisponibili() {
    return numeroDispositiviDisponibili;
  }

  public void setNumeroDispositiviDisponibili(Long numeroDispositiviDisponibili) {
    this.numeroDispositiviDisponibili = numeroDispositiviDisponibili;
  }

  public Long getNumeroDispositiviDaRicevere() {
    return numeroDispositiviDaRicevere;
  }

  public void setNumeroDispositiviDaRicevere(Long numeroDispositiviDaRicevere) {
    this.numeroDispositiviDaRicevere = numeroDispositiviDaRicevere;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Boolean getNotcheckable() {
    return notcheckable;
  }

  public void setNotcheckable(Boolean notcheckable) {
    this.notcheckable = notcheckable;
  }

  @Override
  public String toString() {
    return "SpedizioneFornitoreViewDTO{" +
      "id=" + id +
      ", produttore='" + produttore + '\'' +
      ", numeroDispositiviSpedibili=" + numeroDispositiviSpedibili +
      ", numeroDispositiviDisponibili=" + numeroDispositiviDisponibili +
      ", numeroDispositiviDaRicevere=" + numeroDispositiviDaRicevere +
      ", tipoDispositivo='" + tipoDispositivo + '\'' +
      ", notcheckable=" + notcheckable +
      '}';
  }
}
