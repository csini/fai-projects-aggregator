package it.fai.ms.efservice.repository.dispositivo;

import it.fai.common.enumeration.TipoDispositivoEnum;

public enum TipoDispositivoAssegnabile {
  BUONI_FREJUS_MONTE_BIANCO_FR(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR),
  BUONI_FREJUS_MONTE_BIANCO_IT(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT),
  BUONI_GRAN_SAN_BERNARDO(TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO),
  TELEPASS_ARIANNA_1(TipoDispositivoEnum.TELEPASS_ITALIANO,"AS"),

  ;


  private TipoDispositivoEnum tipoDispositivo;
  private String tipoHardware;

  private TipoDispositivoAssegnabile(TipoDispositivoEnum tipo) {
    this(tipo,null);
  }
  private TipoDispositivoAssegnabile(TipoDispositivoEnum tipo,String tipoHardware) {
    this.tipoDispositivo = tipo;
    this.tipoHardware = tipoHardware;
  }
  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }
  public String getTipoHardware() {
    return tipoHardware;
  }
}
