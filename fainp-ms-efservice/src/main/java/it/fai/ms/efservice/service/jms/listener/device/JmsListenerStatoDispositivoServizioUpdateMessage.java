package it.fai.ms.efservice.service.jms.listener.device;

import static it.fai.ms.common.jms.efservice.message_v2.device.StatoDispositivoServizioUpdateMessage.StatoDS.*;

import java.time.Instant;
import java.util.List;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsUtils;
import it.fai.ms.common.jms.efservice.message_v2.device.StatoDispositivoServizioUpdateMessage;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;

@Service
@Transactional
public class JmsListenerStatoDispositivoServizioUpdateMessage implements MessageListener {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private final StatoDispositivoServizioRepositoryExt repository;

  private final TipoServizioRepositoryExt tsRepository;

  private final DispositivoRepositoryExt dispRepository;

  public JmsListenerStatoDispositivoServizioUpdateMessage(StatoDispositivoServizioRepositoryExt repository,
                                                          TipoServizioRepositoryExt tsRepository, DispositivoRepositoryExt dispRepository) {
    this.repository = repository;
    this.tsRepository = tsRepository;
    this.dispRepository = dispRepository;
  }

  @Override
  public void onMessage(Message message) {
    try {
      StatoDispositivoServizioUpdateMessage dto = (StatoDispositivoServizioUpdateMessage) JmsUtils.getMessageObject(message, StatoDispositivoServizioUpdateMessage.class);
      log.debug("Received message : {}", dto);

      TipoServizio ts = tsRepository.findByNome(dto.getTipoServizio());
      if(ts==null){
        log.error("TipoServizio {} non esiste",dto.getTipoServizio());
        throw new RuntimeException("TipoServizio "+dto.getTipoServizio()+" non esiste");
      }

      List<StatoDispositivoServizio> list = repository.findByDispositivoUuidAndTipoServizio(dto.getUuidDispositivo().getUuid(), dto.getTipoServizio());
      StatoDispositivoServizio sds = null;

      if (list.isEmpty()) {
        sds = newStatoDispositivoServizio(dto, ts);
      }else{
        sds = list.stream().findFirst().get();
      }

      sds.setStato(StatoDS.valueOf(dto.getStato().name()));
      if(dto.getDataAttivazione()!=null)
        sds.setDataAttivazione(dto.getDataAttivazione());
      if(dto.getDataDisattivazione()!=null)
        sds.setDataDisattivazione(dto.getDataDisattivazione());
      if(dto.getDataScadenza()!=null)
        sds.setDataScadenza(dto.getDataScadenza());
      if(dto.getPan()!=null)
        sds.setPan(dto.getPan());

      // Se il servizio non è attivo deve avere sempre la data disattivazione
      if(dto.getStato() == NON_ATTIVO){
        if(sds.getDataDisattivazione()==null)
          sds.setDataDisattivazione(Instant.now());
      }else{
        sds.setDataDisattivazione(null);
      }

      // La data attivazione deve sempre essere presente
      if(sds.getDataAttivazione()==null && dto.getStato() != IN_ATTIVAZIONE)
        sds.setDataAttivazione(Instant.now());
      repository.save(sds);
    } catch (Exception e) {
      log.error("Error to read jms message", e);
      throw new RuntimeException(e);
    }
  }

  private StatoDispositivoServizio newStatoDispositivoServizio(StatoDispositivoServizioUpdateMessage dto, TipoServizio ts) {
    Dispositivo dispositivo = dispRepository.findOneByIdentificativo(dto.getUuidDispositivo()
                                                                     .getUuid());
    StatoDispositivoServizio sds = new StatoDispositivoServizio();
    sds.setDispositivo(dispositivo);
    sds.setTipoServizio(ts);
    return sds;
  }

}