package it.fai.ms.efservice.wizard.model.matrix.step4;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class WizardStep4Matrix implements Serializable {

  private static final long serialVersionUID = -1429002588489174020L;

  private String                        serviceTypeId;
  private Set<WizardStep4MatrixVehicle> vehicles = new LinkedHashSet<>();

  public WizardStep4Matrix(final String _serviceTypeId) {
    serviceTypeId = _serviceTypeId;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep4Matrix) _obj).getServiceTypeId(), serviceTypeId)
                 && Objects.equals(((WizardStep4Matrix) _obj).getVehicles(), vehicles);
    }
    return isEquals;
  }

  public String getServiceTypeId() {
    return serviceTypeId;
  }

  public Set<WizardStep4MatrixVehicle> getVehicles() {
    return vehicles;
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceTypeId, vehicles);
  }

  public void setVehicles(final Set<WizardStep4MatrixVehicle> _vehicles) {
    vehicles = _vehicles;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Step4WizardMatrix [serviceTypeId=");
    builder.append(this.serviceTypeId);
    builder.append(", vehicles=");
    builder.append(this.vehicles);
    builder.append("]");
    return builder.toString();
  }

}
