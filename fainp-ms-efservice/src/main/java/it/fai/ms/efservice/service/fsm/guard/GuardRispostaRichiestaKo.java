/**
 * 
 */
package it.fai.ms.efservice.service.fsm.guard;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

/**
 * @author Luca Vassallo
 */
public class GuardRispostaRichiestaKo implements Guard<StatoRichiesta, RichiestaEvent> {

  private Logger logger = LoggerFactory.getLogger(getClass());
  
  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.guard.Guard#evaluate(org.springframework.statemachine.StateContext)
   */
  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        
        Set<Dispositivo> dispositivos = richiesta.getDispositivos();
        
        boolean isNotAllActive = !checkAllDispositiviAttivi(dispositivos);
        logger.info("Is not all active: " + isNotAllActive);
        return isNotAllActive;
      }
    }
    
    return false;
  }

  private boolean checkAllDispositiviAttivi(Set<Dispositivo> dispositivi) {
    
    if(dispositivi != null) {
      Iterator<Dispositivo> iterator = dispositivi.iterator();
      while(iterator != null && iterator.hasNext()) {
        Dispositivo dispositivo = iterator.next();
        StatoDispositivo stato = dispositivo.getStato();
        if(!stato.equals(StatoDispositivo.ACCETTATO_DA_FORNITORE)) {
          return false;
        }
      }
    }
    
    return true;
  }

}
