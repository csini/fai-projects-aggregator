package it.fai.ms.efservice.service.jms.consumer.device;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableSet;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceResponseKOConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private static final String LOG_STRING = "Changed status for Richiesta";

  private static final Set<TipoRichiesta> requestTypes = ImmutableSet.of(TipoRichiesta.MALFUNZIONAMENTO,
                                                                         TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE,
                                                                         TipoRichiesta.MEZZO_RITARGATO, TipoRichiesta.VARIAZIONE_TARGA);

  private final FsmRichiestaUtils fsmRichiestaUtil;

  public DeviceResponseKOConsumer(final FsmRichiestaUtils _fsmRichiestaUtil) {
    fsmRichiestaUtil = _fsmRichiestaUtil;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class )
  public void consume(String identificativoRichiesta, String errorMsg) {
    Set<String> identificativoRichieste = new HashSet<>();
    log.info("Manage change status for Richiesta [Identificativo: {}] with error message: {}", identificativoRichiesta, errorMsg);
    identificativoRichieste.add(identificativoRichiesta);
    String operazione = RichiestaEvent.RESPONSE_KO.name();
    log.info("Call change status for Richiesta {} with operation {}", identificativoRichiesta, operazione);
    try {
    	boolean changeStatusRichieste = fsmRichiestaUtil.changeStatusRichieste(identificativoRichieste, operazione, errorMsg, null);
    	if (changeStatusRichieste) {
    		log.info("{}: {}", LOG_STRING, identificativoRichiesta);
    	} else {
    		log.error("NOT {}: {}", LOG_STRING, identificativoRichiesta);
    	}
    }catch (FsmExecuteCommandException e) {
		if(e.getMessage().contains("DISATTIVAZIONE_SOSPESA")) {
			log.warn("La richieste è già in stato sospeso: {}",e.getMessage());
		}else if(e.getMessage().contains("ATTIVO_EVASO") || e.getMessage().contains("EVASO_CON_RICONSEGNA")) {
			log.warn("La richieste è già stata evasa: {}",e.getMessage());
		}else{
			throw new IllegalStateException(e);
		}
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class )
  public void consume(String contractCode, String serialNumberDevice, String errorMsg) {
    log.info("Retrieve Richiesta by {} - {}", contractCode, serialNumberDevice);
    List<Richiesta> optRichiesta = fsmRichiestaUtil.findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(contractCode,
                                                                                                                 serialNumberDevice,
                                                                                                                 StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO,
                                                                                                                 requestTypes);

    if (!optRichiesta.isEmpty()) {
      Richiesta richiesta = optRichiesta.get(0);
      consume(richiesta.getIdentificativo(), errorMsg);
    } else {
      log.error("Not found Richiesta....");
    }

  }

}
