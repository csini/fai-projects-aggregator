package it.fai.ms.efservice.service.fsm.bean.telepass.eu;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaTeVarTarga")
public class FsmModificaTeVarTargaTransitionBean extends AbstractFsmRichiestaTransition {
}
