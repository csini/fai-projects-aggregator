package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.orderrequest.OrderNotificaTransitoMessage;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;

@Service
@Transactional
public class OrdineClienteTransitoFaiConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final OrdineClienteRepositoryExt repo;

  public OrdineClienteTransitoFaiConsumer(final OrdineClienteRepositoryExt _repo) {
    repo = _repo;
  }

  public void consume(OrderNotificaTransitoMessage message) {
    OrderRequestUuid orderRequestUuid = message.getOrderRequestUuid();
    
    OrdineCliente oc = repo.findByIdentificativoOrdine(orderRequestUuid.getUuid());
    oc.setDestinazioneFai(true);
    repo.save(oc);
    
    if(log.isDebugEnabled())
      log.debug("update destinazioneFai=true on ordine {}",oc);
  }

}
