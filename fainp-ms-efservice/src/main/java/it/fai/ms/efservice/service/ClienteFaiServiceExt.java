package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Set;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;

/**
 * Service Interface for managing ClienteFai.
 */
public interface ClienteFaiServiceExt {

  /**
   * @param codiceAzienda
   * @param tipoDispositivo
   * @return
   */
  public Set<Dispositivo> findDispositivoByClienteAndTipoDispositivo(String codiceAzienda, TipoDispositivoEnum tipoDispositivo);

  public ClienteFai findById(Long id);

  public ClienteFai save(ClienteFai clienteFai);

  public ClienteFaiDTO findDTOByCodiceCliente(String codiceCliente);

  public ClienteFai findByCodiceCliente(String codiceCliente);

  public ClienteFaiDTO findDTOByCodiceClienteFatturazione(String codiceClienteFatturazione);

  public ClienteFai findByCodiceClienteFatturazione(String codiceClienteFatturazione);

  public List<ClienteFai> findByRaggruppamentoImpresa(String raggruppamentoImpresa);

}
