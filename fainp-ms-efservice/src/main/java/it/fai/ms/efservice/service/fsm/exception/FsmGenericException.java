package it.fai.ms.efservice.service.fsm.exception;

public class FsmGenericException extends Exception {

  private static final long serialVersionUID = 1679092596206207046L;
  
  public FsmGenericException() {
    this(FsmGenericException.class.toString());
  }

  public FsmGenericException(String msgId, Throwable cause) {
    super(msgId, cause);
  }

  public FsmGenericException(Throwable cause) {
    super(cause);
  }

  public FsmGenericException(String message) {
    super(message);
  }

}