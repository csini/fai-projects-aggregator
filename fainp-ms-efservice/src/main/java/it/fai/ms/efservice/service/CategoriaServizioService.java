package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import java.util.List;

/**
 * Service Interface for managing CategoriaServizio.
 */
public interface CategoriaServizioService {

    /**
     * Save a categoriaServizio.
     *
     * @param categoriaServizioDTO the entity to save
     * @return the persisted entity
     */
    CategoriaServizioDTO save(CategoriaServizioDTO categoriaServizioDTO);

    /**
     *  Get all the categoriaServizios.
     *
     *  @return the list of entities
     */
    List<CategoriaServizioDTO> findAll();

    /**
     *  Get the "id" categoriaServizio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CategoriaServizioDTO findOne(Long id);

    /**
     *  Delete the "id" categoriaServizio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
