package it.fai.ms.efservice.service.fsm.guard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;

public class GuardSendDeviceReturnMessage implements Guard<StatoRichiesta, RichiestaEvent> {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  public GuardSendDeviceReturnMessage(DeviceReturnMessageProducerService _deviceReturnMessageService) {
    deviceReturnMessageService = _deviceReturnMessageService;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Richiesta richiesta = null;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        richiesta = (Richiesta) object;
        sendMessage(richiesta);
        // disassociazioneDispositivoVeicolo(richiesta);
      }
    }

    return true;
  }

  private void sendMessage(final Richiesta richiesta) {
    TipoDispositivoEnum deviceType = richiesta.getTipoDispositivo()
                                              .getNome();
    log.info("Send DEVICE RETURN message to DeviceType: {}", deviceType);
    switch (deviceType) {
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      deviceReturnMessageService.deactivateDevice(richiesta);
      break;

    case TELEPASS_ITALIANO:
    case VIACARD:
      deviceReturnMessageService.deactivateDeviceTelepassIta(richiesta);
      break;

    default:
      log.warn("Device Type {} does not managed", deviceType);
      break;
    }

  }

  // private void disassociazioneDispositivoVeicolo(Richiesta richiesta) {
  // Set<Dispositivo> dispositivos = richiesta.getDispositivos();
  // dispositivos.parallelStream()
  // .map(dispositivo -> {
  // try {
  // senderFsmService.sendMessageToDisassociazioneDispositivo(richiesta, dispositivo);
  // } catch (Exception e) {
  // throw new RuntimeException(e);
  // }
  // return dispositivo;
  // })
  // .collect(toSet());
  // }

}
