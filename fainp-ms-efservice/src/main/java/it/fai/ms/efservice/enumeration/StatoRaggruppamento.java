package it.fai.ms.efservice.enumeration;

public enum StatoRaggruppamento {
  APERTO, CHIUSO;
}
