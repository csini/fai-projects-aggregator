package it.fai.ms.efservice.service.jms.dml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.jms.mapper.ContrattoDmlSenderMapper;

@Service
@Transactional
public class ContrattoMessageSenderService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  // Not work as spring bean, really don't know why...
  private final ContrattoDmlSenderUtil contrattoDmlSenderUtil;

  public ContrattoMessageSenderService(JmsProperties jmsProperties, ContrattoDmlSenderMapper contrattoDmlSenderMapper) {
    this.contrattoDmlSenderUtil = new ContrattoDmlSenderUtil(jmsProperties, contrattoDmlSenderMapper);
  }

  public void sendSaveNotification(Contratto contract) {
    log.debug("Send SAVE contract: {}", (contract != null) ? contract.getIdentificativo() : null);
    contrattoDmlSenderUtil.sendSaveNotification(contract);
  }

  public void sendDeleteNotification(Contratto contract) {
    log.debug("Send DELETE contract: {}", (contract != null) ? contract.getIdentificativo() : null);
    contrattoDmlSenderUtil.sendDeleteNotification(String.valueOf(contract.getIdentificativo()));
  }
}
