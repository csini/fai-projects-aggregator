package it.fai.ms.efservice.vehicle.ownership.model;

import java.io.Serializable;
import java.util.Objects;

public class VehicleOwnership implements Serializable {

  private static final long serialVersionUID = 7666063529831037585L;

  private VehicleOwnershipCompanyCode companyCode;
  private String                      licensePlate;
  private String                      uuid;

  public VehicleOwnership(final String _uuid) {
    uuid = _uuid;
  }

  public VehicleOwnership(final String _uuid, final String _licensePlate, final VehicleOwnershipCompanyCode _companyCode) {
    uuid = _uuid;
    licensePlate = _licensePlate;
    companyCode = _companyCode;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((VehicleOwnership) _obj).getUuid(), uuid);
    }
    return isEquals;
  }

  public VehicleOwnershipCompanyCode getCompanyCode() {
    return companyCode;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public String getUuid() {
    return uuid;
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid);
  }

  public void setCompanyCode(final VehicleOwnershipCompanyCode _companyCode) {
    companyCode = _companyCode;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnership [companyCode=");
    builder.append(this.companyCode);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append(", uuid=");
    builder.append(this.uuid);
    builder.append("]");
    return builder.toString();
  }

}
