package it.fai.ms.efservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.enumeration.TipoDisposizione;
import it.fai.ms.efservice.service.dto.AnagraficaGiacenzaDTO;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;

/**
 * Service Implementation for managing DeviceDepositivoOrdineFornitore.
 */
@Service
@Transactional
public class OrdineFornitoreServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final OrdineFornitoreService    ordineFornitoreService;
  private final AnagraficaGiacenzaService anagraficaGiacenzaService;

  public OrdineFornitoreServiceExt(final OrdineFornitoreService ordineFornitoreService,
                                   final AnagraficaGiacenzaService anagraficaGiacenzaService) {
    this.ordineFornitoreService = ordineFornitoreService;
    this.anagraficaGiacenzaService = anagraficaGiacenzaService;
  }

  public String getTipoDispositivoByOrdineDispositivoId(Long ordineFornitoreId) {
    OrdineFornitoreDTO ordineFornitoreDTO = findOrdineFornitoreById(ordineFornitoreId);
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = findAnagraficaGiacenzaById(ordineFornitoreDTO.getAnagraficaGiacenzaId());
    String tipoDispositivo = anagraficaGiacenzaDTO.getTipoDispositivo();
    log.info("TipoDispositivo found: {}", tipoDispositivo);
    return tipoDispositivo;
  }

  public OrdineFornitoreDTO findOrdineFornitoreById(Long ordineFornitoreId) {
    OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreService.findOne(ordineFornitoreId);
    log.info("Found OrdineFornitoreDTO: {}", ordineFornitoreDTO);
    return ordineFornitoreDTO;
  }

  public AnagraficaGiacenzaDTO findAnagraficaGiacenzaById(Long anagraficaGiacenzaId) {
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaService.findOne(anagraficaGiacenzaId);
    log.info("Found AnangraficaGiacenzaDTO: {}", anagraficaGiacenzaDTO);
    return anagraficaGiacenzaDTO;
  }

  public OrdineFornitoreDTO updateDeviceAcquireQuantityAndState(OrdineFornitoreDTO ordineFornitoreDTO, int numDispositiviAcquisiti) {
    Integer oldNumDispositiviAcquisiti = ordineFornitoreDTO.getNumeroDispositiviAcquisiti();
    if (oldNumDispositiviAcquisiti == null) {
      oldNumDispositiviAcquisiti = 0;
    }

    int newQuantity = oldNumDispositiviAcquisiti + numDispositiviAcquisiti;
    ordineFornitoreDTO.setNumeroDispositiviAcquisiti(newQuantity);
    if (numDispositiviAcquisiti > 0) {
      ordineFornitoreDTO.setTipologiaDisposizione(TipoDisposizione.ACQUISITO);
    }

    return save(ordineFornitoreDTO);
  }

  public OrdineFornitoreDTO updateNoteOrdineFornitore(OrdineFornitoreDTO ordineFornitoreDTO, String note) {
    return save(ordineFornitoreDTO.note(note));

  }

  public OrdineFornitoreDTO save(OrdineFornitoreDTO dto) {
    return ordineFornitoreService.save(dto);
  }

}
