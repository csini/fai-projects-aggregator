package it.fai.ms.efservice.service.fsm.type.viatoll;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.viatoll.FsmModificaViaTollVarTargaMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaViaTollVarTargaMalfunzionamento.FSM_MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaViaTollVarTargaMalfunzionamento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO = "fsmModificaViaTollVarTargaMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaViaTollVarTargaMalfunzionamento(@Qualifier(FsmModificaViaTollVarTargaMalfunzionamentoConfig.MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.VIA_TOLL };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
