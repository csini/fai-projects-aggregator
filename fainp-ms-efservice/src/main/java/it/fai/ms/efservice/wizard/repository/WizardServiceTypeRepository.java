package it.fai.ms.efservice.wizard.repository;

import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceType;

public interface WizardServiceTypeRepository {

  Set<WizardServiceType> findAllByDeviceTypeId(WizardDeviceTypeId wizardDeviceTypeId);

}
