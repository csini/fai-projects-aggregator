package it.fai.ms.efservice.domain.enumeration;

public enum KeyDatiAggiuntivi {
  DATA_PRENOTAZIONE_DA, DATA_PRENOTAZIONE_A, NOME_SERVIZIO;
}
