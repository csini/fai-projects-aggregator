package it.fai.ms.efservice.service.fsm.cacheconfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.infinispan.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.region.Region;
import org.springframework.statemachine.state.AbstractState;
import org.springframework.statemachine.state.HistoryPseudoState;
import org.springframework.statemachine.state.PseudoState;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.AbstractStateMachine;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.serializer.FsmContrattoSerializer;

@Component
public class FsmContrattoCacheService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private FsmContrattoSerializer     serializer;
  private FsmInfinispanConfigFactory infinispanConfig;

  private Cache<String, String> cache;

  @Autowired
  public FsmContrattoCacheService(FsmContrattoSerializer _serializer, FsmInfinispanConfigFactory _infinispanConfig) {
    serializer = _serializer;
    infinispanConfig = _infinispanConfig;
  }

  @PostConstruct
  public void init() {
    log.debug("Init service FSM_ORDERS Cache");
    cache = infinispanConfig.buildCache("fsm_contratto");
  }

  public void putStateMachineContext(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    cache.put(key, json);
  }

  public String getStateMachineContext(String key) {
    String json = cache.get(key);
    log.debug("Get object from Map with Key {} Value {}", key, json);
    return json;
  }

  public String getIdStateMachine(String key) {
    String jsonSmContext = getStateMachineContext(key);

    return jsonSmContext;
  }

  public void clearCache(String key) {
    log.debug("Remove key {} from cache {}", key, ((cache != null) ? cache.getName() : ""));
    cache.remove(key);
  }

  public void clearCache() {
    log.debug("Clear cache {}", ((cache != null) ? cache.getName() : ""));
    cache.clear();
  }

  public void persist(StateMachine<StatoContratto, ContrattoEvent> stateMachine, String identifier) {
    StateMachineContext<StatoContratto, ContrattoEvent> buildStateMachineContext = buildStateMachineContext(stateMachine);
    String objSerialized = serializer.serialize(buildStateMachineContext);
    putStateMachineContext(identifier, objSerialized);
  }

  public void resetIdMachineFromAnotherIdMachineByContext(String json, String identifier, String idNewMachine) {
    StateMachineContext<StatoContratto, ContrattoEvent> smc = serializer.deSerialize(json, idNewMachine);
    String objSerialized = serializer.serialize(smc);
    putStateMachineContext(identifier, objSerialized);
  }

  public final StateMachine<StatoContratto, ContrattoEvent> restore(StateMachine<StatoContratto, ContrattoEvent> stateMachine,
                                                                    String identifier) {
    String json = getStateMachineContext(identifier);
    if (json == null) {
      return stateMachine;
    }

    StateMachineContext<StatoContratto, ContrattoEvent> smc = serializer.deSerialize(json);

    if (smc == null) {
      return stateMachine;
    }

    final StateMachineContext<StatoContratto, ContrattoEvent> context = smc;

    log.info("State Machine... {}", stateMachine);
    stateMachine.stop();

    stateMachine.getStateMachineAccessor()
                .doWithAllRegions(access -> {
                  access.resetStateMachine(context);
                });

    stateMachine.start();
    return stateMachine;
  }

  protected StateMachineContext<StatoContratto, ContrattoEvent> buildStateMachineContext(StateMachine<StatoContratto, ContrattoEvent> stateMachine) {
    ExtendedState extendedState = new DefaultExtendedState();
    extendedState.getVariables()
                 .putAll(stateMachine.getExtendedState()
                                     .getVariables());

    ArrayList<StateMachineContext<StatoContratto, ContrattoEvent>> childs = new ArrayList<>();
    StatoContratto id = null;
    State<StatoContratto, ContrattoEvent> state = stateMachine.getState();
    if (state.isSubmachineState()) {
      id = getDeepState(state);
    } else if (state.isOrthogonal()) {
      Collection<Region<StatoContratto, ContrattoEvent>> regions = ((AbstractState<StatoContratto, ContrattoEvent>) state).getRegions();
      for (Region<StatoContratto, ContrattoEvent> r : regions) {
        StateMachine<StatoContratto, ContrattoEvent> rsm = (StateMachine<StatoContratto, ContrattoEvent>) r;
        childs.add(buildStateMachineContext(rsm));
      }
      id = state.getId();
    } else {
      id = state.getId();
    }

    // building history state mappings
    Map<StatoContratto, StatoContratto> historyStates = new HashMap<>();
    PseudoState<StatoContratto, ContrattoEvent> historyState = ((AbstractStateMachine<StatoContratto, ContrattoEvent>) stateMachine).getHistoryState();
    if (historyState != null) {
      historyStates.put(null, ((HistoryPseudoState<StatoContratto, ContrattoEvent>) historyState).getState()
                                                                                                 .getId());
    }
    Collection<State<StatoContratto, ContrattoEvent>> states = stateMachine.getStates();
    for (State<StatoContratto, ContrattoEvent> ss : states) {
      if (ss.isSubmachineState()) {
        StateMachine<StatoContratto, ContrattoEvent> submachine = ((AbstractState<StatoContratto, ContrattoEvent>) ss).getSubmachine();
        PseudoState<StatoContratto, ContrattoEvent> ps = ((AbstractStateMachine<StatoContratto, ContrattoEvent>) submachine).getHistoryState();
        if (ps != null) {
          State<StatoContratto, ContrattoEvent> pss = ((HistoryPseudoState<StatoContratto, ContrattoEvent>) ps).getState();
          if (pss != null) {
            historyStates.put(ss.getId(), pss.getId());
          }
        }
      }
    }
    return new DefaultStateMachineContext<>(childs, id, null, null, extendedState, historyStates, stateMachine.getId());
  }

  private StatoContratto getDeepState(State<StatoContratto, ContrattoEvent> state) {
    Collection<StatoContratto> ids1 = state.getIds();
    StatoContratto[] ids2 = (StatoContratto[]) ids1.toArray();
    // TODO: can this be empty as then we'd get error?
    return ids2[ids2.length - 1];
  }

}
