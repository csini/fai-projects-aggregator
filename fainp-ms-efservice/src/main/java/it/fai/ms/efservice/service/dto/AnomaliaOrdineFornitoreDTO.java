package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public class AnomaliaOrdineFornitoreDTO implements Serializable {

  private static final long serialVersionUID = 4420917593167503261L;

  private String produttore;

  private String tipoDispositivo;

  private String seriale;

  private String statoDispositivo;

  private String codiceCliente;

  private String ragioneSociale;

  public AnomaliaOrdineFornitoreDTO(String _seriale) {
    seriale = _seriale;
  }

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public String getStatoDispositivo() {
    return statoDispositivo;
  }

  public void setStatoDispositivo(String statoDispositivo) {
    this.statoDispositivo = statoDispositivo;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codiceCliente == null) ? 0 : codiceCliente.hashCode());
    result = prime * result + ((produttore == null) ? 0 : produttore.hashCode());
    result = prime * result + ((ragioneSociale == null) ? 0 : ragioneSociale.hashCode());
    result = prime * result + ((seriale == null) ? 0 : seriale.hashCode());
    result = prime * result + ((statoDispositivo == null) ? 0 : statoDispositivo.hashCode());
    result = prime * result + ((tipoDispositivo == null) ? 0 : tipoDispositivo.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AnomaliaOrdineFornitoreDTO other = (AnomaliaOrdineFornitoreDTO) obj;
    if (codiceCliente == null) {
      if (other.codiceCliente != null)
        return false;
    } else if (!codiceCliente.equals(other.codiceCliente))
      return false;
    if (produttore == null) {
      if (other.produttore != null)
        return false;
    } else if (!produttore.equals(other.produttore))
      return false;
    if (ragioneSociale == null) {
      if (other.ragioneSociale != null)
        return false;
    } else if (!ragioneSociale.equals(other.ragioneSociale))
      return false;
    if (seriale == null) {
      if (other.seriale != null)
        return false;
    } else if (!seriale.equals(other.seriale))
      return false;
    if (statoDispositivo == null) {
      if (other.statoDispositivo != null)
        return false;
    } else if (!statoDispositivo.equals(other.statoDispositivo))
      return false;
    if (tipoDispositivo == null) {
      if (other.tipoDispositivo != null)
        return false;
    } else if (!tipoDispositivo.equals(other.tipoDispositivo))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "AnomaliaOrdineFornitoreDTO [produttore=" + produttore + ", tipoDispositivo=" + tipoDispositivo + ", seriale=" + seriale
           + ", statoDispositivo=" + statoDispositivo + ", codiceCliente=" + codiceCliente + ", ragioneSociale=" + ragioneSociale + "]";
  }

}
