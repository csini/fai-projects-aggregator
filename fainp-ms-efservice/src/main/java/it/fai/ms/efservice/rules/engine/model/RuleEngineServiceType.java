package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class RuleEngineServiceType implements Serializable {

  private static final long serialVersionUID = -5051747066857786897L;

  public static RuleEngineServiceType as(final String _id) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(_id));
  }

  private RuleEngineServiceTypeId id;

  public RuleEngineServiceType(final RuleEngineServiceTypeId _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineServiceType Id is mandatory"));
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((RuleEngineServiceType) _obj).getId(), id);
    }
    return isEquals;
  }

  public RuleEngineServiceTypeId getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineServiceType [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
