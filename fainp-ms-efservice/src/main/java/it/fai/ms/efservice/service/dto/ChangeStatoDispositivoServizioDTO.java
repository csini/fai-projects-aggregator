package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.efservice.domain.enumeration.StatoDS;

public class ChangeStatoDispositivoServizioDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String           uuid;
  private TipoServizioEnum tipoServizio;
  private String           pan;
  private Instant          dataAttivazione;
  private Instant          dataDisattivazione;
  private StatoDS          stato;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public TipoServizioEnum getTipoServizio() {
    return tipoServizio;
  }

  public void setTipoServizio(TipoServizioEnum tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public Instant getDataAttivazione() {
    return dataAttivazione;
  }

  public void setDataAttivazione(Instant dataAttivazione) {
    this.dataAttivazione = dataAttivazione;
  }

  public Instant getDataDisattivazione() {
    return dataDisattivazione;
  }

  public void setDataDisattivazione(Instant dataDisattivazione) {
    this.dataDisattivazione = dataDisattivazione;
  }

  public StatoDS getStato() {
    return stato;
  }

  public void setStato(StatoDS stato) {
    this.stato = stato;
  }

  public ChangeStatoDispositivoServizioDTO uuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

  public ChangeStatoDispositivoServizioDTO tipoServizio(TipoServizioEnum tipoServizio) {
    this.tipoServizio = tipoServizio;
    return this;
  }

  public ChangeStatoDispositivoServizioDTO pan(String pan) {
    this.pan = pan;
    return this;
  }

  public ChangeStatoDispositivoServizioDTO dataAttivazione(Instant dataAttivazione) {
    this.dataAttivazione = dataAttivazione;
    return this;
  }

  public ChangeStatoDispositivoServizioDTO dataDisattivazione(Instant dataDisattivazione) {
    this.dataDisattivazione = dataDisattivazione;
    return this;
  }

  public ChangeStatoDispositivoServizioDTO stato(StatoDS stato) {
    this.stato = stato;
    return this;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("ChangeStatoDispositivoServizioDTO [uuid=")
        .append(uuid)
        .append(", tipoServizio=")
        .append(tipoServizio)
        .append(", pan=")
        .append(pan)
        .append(", dataAttivazione=")
        .append(dataAttivazione)
        .append(", dataDisattivazione=")
        .append(dataDisattivazione)
        .append(", stato=")
        .append(stato)
        .append("]");
    return builder.toString();
  }

}
