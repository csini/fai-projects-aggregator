package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.DisassociazioneDispositivoDTO;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;

@Service
@Transactional
public class DisassociazioneDispositiviConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaRepositoryExt   ordineRepoExt;
  private final DispositivoServiceExt dispositivoServiceExt;

  /**
   * @param ordineRepo
   * @param dispositivoServiceExt
   */
  public DisassociazioneDispositiviConsumer(RichiestaRepositoryExt ordineRepoExt, DispositivoServiceExt dispositivoServiceExt) {
    this.ordineRepoExt = ordineRepoExt;
    this.dispositivoServiceExt = dispositivoServiceExt;
  }

  public Dispositivo disassociaDispositivo(DisassociazioneDispositivoDTO dto) throws Exception {
    String identificativoDispositivo = dto.getIdentificativoDispositivo();
    log.info("Disassociazione Dispositivo {}", identificativoDispositivo);
    Dispositivo dispositivo = dispositivoServiceExt.disassociaDispositivo(identificativoDispositivo);
    return dispositivo;
  }
}
