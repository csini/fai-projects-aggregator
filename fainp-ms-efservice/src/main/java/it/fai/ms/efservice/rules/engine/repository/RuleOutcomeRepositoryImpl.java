package it.fai.ms.efservice.rules.engine.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.notification_v2.VeicoloDispositivoConflittiMessage;
import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity.Failure;
import it.fai.ms.efservice.service.NotificationPreconditionVehicleAnomalyService;

@Service
public class RuleOutcomeRepositoryImpl implements RuleOutcomeRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final static String DEFAULT_VALUE = "n.a.";

  private Map<RuleEntityKey, RuleOutcomeEntity> persistentCache;

  private final NotificationPreconditionVehicleAnomalyService notificationPreconditionVehicleAnomalyService;


  @Value("${application.isIntilialLoading:}")
  private String isInitialLoading;

  @PostConstruct
  public void postConstruct(){
    _log.error(" ### isInitialLoading:" +  isInitialLoading + " ### ");
  }


  @Autowired
  public RuleOutcomeRepositoryImpl(@Qualifier("rule") final Map<RuleEntityKey, RuleOutcomeEntity> _cache,
                                   final NotificationPreconditionVehicleAnomalyService _notificationPreconditionVehicleAnomalyService) {
    persistentCache = _cache;
    notificationPreconditionVehicleAnomalyService = _notificationPreconditionVehicleAnomalyService;
  }

  @Override
  public Integer count() {
    int size = persistentCache.size();
    _log.info("Size: {}", size);
    return size;
  }

  @Override
  public void deleteAll() {
    persistentCache.clear();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> find(final RuleEngineDeviceTypeId _deviceTypeId, final RuleEngineVehicleId _vehicleId) {
    return find(RuleEngineServiceTypeId.ofWildcard(), _deviceTypeId, _vehicleId);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> find(final RuleEngineServiceTypeId _serviceTypeId, final RuleEngineDeviceTypeId _deviceTypeId,
                                    final RuleEngineVehicleId _vehicleId) {
    final RuleEntityKey ruleEntityKey = new RuleEntityKey();
    ruleEntityKey.setServiceTypeId(_serviceTypeId.getId());
    ruleEntityKey.setDeviceTypeId(_deviceTypeId.getId());
    ruleEntityKey.setVehicleId(_vehicleId.getId());
    _log.debug("Try to restore RuleOutcome from Infinispan Repository: {}", ruleEntityKey);
    final RuleOutcomeEntity ruleOutcomeEntity = restore(ruleEntityKey);
    RuleOutcome ruleOutcome = null;
    if (ruleOutcomeEntity != null) {
      _log.debug("Found {} for {}", ruleOutcomeEntity, ruleEntityKey);
      if (ruleOutcomeEntity.isOutcome()) {
        ruleOutcome = new RuleOutcome();
      } else {
        ruleOutcome = new RuleOutcome(mapToRuleFailure(ruleOutcomeEntity.getFailure()));
      }
    } else {
      _log.warn("Not found {} on Infinispan Repository", ruleEntityKey);
    }
    return Optional.ofNullable(ruleOutcome);
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> find(final RuleEngineVehicleId _vehicleId) {
    return find(RuleEngineServiceTypeId.ofWildcard(), RuleEngineDeviceTypeId.ofWildcard(), _vehicleId);
  }

  @Override
  @Transactional
  public RuleOutcomeEntity save(final RuleContext _ruleContext, final RuleOutcome _ruleOutcome) {
    _log.debug("Try to persist {} in Infinispan Repository for RuleContext {}", _ruleOutcome, _ruleContext);
    RuleEntityKey key = mapToRuleEntityKey(_ruleContext);
    RuleOutcomeEntity newRuleOutcome = mapToRuleOutcomeEntity(_ruleOutcome);

    _log.debug("Persisting {} in Infinispan Repository for RuleContext {}", _ruleOutcome, _ruleContext);
    RuleOutcomeEntity prevRuleOutCome = persist(key, newRuleOutcome);
    _log.debug("Persisted {} in Infinispan Repository for RuleContext {}", _ruleOutcome, _ruleContext);

    //short circuit return if is initial loading
    if ("true".equalsIgnoreCase(isInitialLoading)){
      if (_log.isDebugEnabled()) _log.debug("Is initial loading! returning from message elaboration...");
    }else {
      checkAndSendAnomaly(key, prevRuleOutCome, newRuleOutcome);
      _log.debug("CheckAndSendAnomaly key:=[{}], prevRuleOutCome:=[{}], newRuleOutcome:=[{}]", key, prevRuleOutCome, newRuleOutcome);
    }
    return newRuleOutcome;
  }

  private void checkAndSendAnomaly(RuleEntityKey key, RuleOutcomeEntity oldRule, RuleOutcomeEntity newRule) {
    if (oldRule == null) {
      _log.debug("Old rule outcome does not exist. Skip send anomaly...");
      return;
    }

    boolean outcomeOldRule = oldRule.isOutcome();
    boolean outcomeNewRule = newRule.isOutcome();
    _log.debug("Rule: {} - OLD outcome: {} - NEW outcome: {}", key, outcomeOldRule, outcomeNewRule);
    if (!outcomeOldRule && !outcomeNewRule) {

      // Check if failure rule is different;
      String failureCodeOld = oldRule.getFailure()
                                     .getFailureCode();
      String failureCodeNew = newRule.getFailure()
                                     .getFailureCode();
      _log.debug("FailureCode OLD: {} - FailureCode NEW: {}", failureCodeOld, failureCodeNew);
      if (!failureCodeOld.equals(failureCodeNew)) {
        // toDelete is FALSE;
        sendNotificationAnomalyOnVehicle(key, newRule, false);
      } else {
        _log.debug("The Failure code is equal between old and new RuleOutcome for Rule: {}", key);
      }
    } else if (!outcomeOldRule && outcomeNewRule) {
      // toDelete is TRUE
      sendNotificationAnomalyOnVehicle(key, newRule, true);
    } else if (outcomeOldRule && !outcomeNewRule) {
      // toDelete is FALSE;
      sendNotificationAnomalyOnVehicle(key, newRule, false);
    } else {
      _log.trace("Skip send anomaly, because NEW RuleOutcome is {} and OLD RuleOutcome is {} on Rule: {}", outcomeNewRule, outcomeOldRule,
                key);
    }

  }

  private void sendNotificationAnomalyOnVehicle(RuleEntityKey key, RuleOutcomeEntity newRule, boolean toDelete) {
    VeicoloDispositivoConflittiMessage dto = new VeicoloDispositivoConflittiMessage();
    Failure failure = newRule.getFailure();
    if (failure != null) {
      String failureCode = failure.getFailureCode();
      String failureMess = failure.getFailureMess();

      _log.info("New rule failure because: [ CODE: {} - MESSAGE: {} ]", failureCode, failureMess);
      dto.setAnomalie(failureMess);
    }

    dto.setToDelete(toDelete);
    dto.setDataUltimaVariazione(LocalDate.now());
    dto.setDatetime(Instant.now());

    notificationPreconditionVehicleAnomalyService.sendNotification(key, dto);
  }

  private RuleEntityKey mapToRuleEntityKey(final RuleContext _ruleContext) {
    RuleEntityKey ruleEntityKey = new RuleEntityKey();
    ruleEntityKey.setServiceTypeId(_ruleContext.getServiceTypeRuleContext()
                                               .getId()
                                               .getId());
    ruleEntityKey.setDeviceTypeId(_ruleContext.getDeviceTypeRuleContext()
                                              .getId()
                                              .getId());
    ruleEntityKey.setVehicleId(_ruleContext.getVehicleRuleContext()
                                           .getId()
                                           .getId());
    return ruleEntityKey;
  }

  private RuleFailure mapToRuleFailure(final Failure _failure) {
    return Optional.ofNullable(_failure)
                   .map(failure -> new RuleFailure(failure.getFailureCode(), failure.getFailureMess()))
                   .orElse(new RuleFailure("", DEFAULT_VALUE));
  }

  private RuleOutcomeEntity mapToRuleOutcomeEntity(final RuleOutcome _ruleOutcome) {
    RuleOutcomeEntity ruleOutcomeEntity = null;
    Boolean outcome = _ruleOutcome.getOutcome();
    if (!outcome) {
      Optional<RuleFailure> failure = _ruleOutcome.getFailure();
      if (failure.isPresent()) {
        RuleFailure ruleFailure = failure.get();
        ruleOutcomeEntity = new RuleOutcomeEntity(outcome, ruleFailure.getCode(), ruleFailure.getMess());
      } else {
        ruleOutcomeEntity = new RuleOutcomeEntity(outcome, DEFAULT_VALUE, DEFAULT_VALUE);
      }
    } else {
      ruleOutcomeEntity = new RuleOutcomeEntity(outcome);
    }

    return ruleOutcomeEntity;
  }

  private RuleOutcomeEntity persist(final RuleEntityKey _ruleEntityKey, final RuleOutcomeEntity _ruleOutcomeEntity) {
    RuleOutcomeEntity ruleOutcomeEntity = restore(_ruleEntityKey);
    RuleOutcomeEntity put = persistentCache.put(_ruleEntityKey, _ruleOutcomeEntity);
    if (put != null) {
      _log.debug("  Cache value updated for key {}", _ruleEntityKey);
    } else {
      _log.debug("  Cache value created for key {}", _ruleEntityKey);
    }

    _log.debug("Old ruleOutcomeEntity: {}", ruleOutcomeEntity);
    return ruleOutcomeEntity;
  }

  private RuleOutcomeEntity restore(final RuleEntityKey _ruleEntityKey) {
    //short circuit return if is initial loading
    if ("true".equalsIgnoreCase(isInitialLoading)){
      if (_log.isDebugEnabled()) _log.debug("Is initial loading! returning from message elaboration...");
      return null;
    }
    return persistentCache.get(_ruleEntityKey);
  }

}
