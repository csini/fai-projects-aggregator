package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the IndirizzoSpedizioneOrdini entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndirizzoSpedizioneOrdiniRepository extends JpaRepository<IndirizzoSpedizioneOrdini, Long>, JpaSpecificationExecutor<IndirizzoSpedizioneOrdini> {

}
