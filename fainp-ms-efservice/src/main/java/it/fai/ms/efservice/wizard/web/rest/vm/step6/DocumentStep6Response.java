package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;

public class DocumentStep6Response implements Serializable {

  private static final long serialVersionUID = 6916696558872880436L;

  private String       nome;
  private String       downloadUri;
  private boolean      upload            = false;
  private VehicleStep6 vehicle;
  private boolean      reservationFields = false;

  public DocumentStep6Response(String nome, String downloadUri) {
    super();
    this.nome = nome;
    this.downloadUri = downloadUri;
  }

  public DocumentStep6Response(String nome, String downloadUri, VehicleStep6 vehicle) {
    super();
    this.nome = nome;
    this.downloadUri = downloadUri;
    this.setVehicle(vehicle);
  }

  public DocumentStep6Response(String nome, String downloadUri, VehicleStep6 vehicle, boolean upload) {
    super();
    this.nome = nome;
    this.downloadUri = downloadUri;
    this.setVehicle(vehicle);
    this.setUpload(upload);
  }

  public DocumentStep6Response(String nome, String downloadUri, VehicleStep6 vehicle, boolean upload, boolean reservationFields) {
    super();
    this.nome = nome;
    this.downloadUri = downloadUri;
    this.setVehicle(vehicle);
    this.setUpload(upload);
    this.setReservationFields(reservationFields);
  }

  public DocumentStep6Response(String nome, String downloadUri, boolean upload) {
    super();
    this.nome = nome;
    this.downloadUri = downloadUri;
    this.setUpload(upload);
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDownloadUri() {
    return downloadUri;
  }

  public void setDownloadUri(String downloadUri) {
    this.downloadUri = downloadUri;
  }

  public boolean isUpload() {
    return upload;
  }

  public void setUpload(boolean upload) {
    this.upload = upload;
  }

  public VehicleStep6 getVehicle() {
    return vehicle;
  }

  public void setVehicle(VehicleStep6 vehicleStep6) {
    this.vehicle = vehicleStep6;
  }

  public boolean isReservationFields() {
    return reservationFields;
  }

  public void setReservationFields(boolean reservationFields) {
    this.reservationFields = reservationFields;
  }

  @Override
  public String toString() {
    return "DocumentStep6Response [nome=" + nome + ", downloadUri=" + downloadUri + ", upload=" + upload + ", vehicleStep6=" + vehicle
           + ", reservationFields=" + reservationFields + "]";
  }
}
