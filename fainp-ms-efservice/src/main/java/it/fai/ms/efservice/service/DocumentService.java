package it.fai.ms.efservice.service;

import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.dto.DocumentoDTO;

public interface DocumentService {

  enum TipologiaDocumento {
    RICHIESTA_DISPOSITIVO("RICHIESTA_DISPOSITIVO"), RICHIESTA_CONTRATTO("RICHIESTA_CONTRATTO");

    private String name;

    TipologiaDocumento(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }
  }

  Optional<DocumentoDTO> findLastDocument(String codiceAzienda, TipologiaDocumento tipologiaDocumento, TipoDispositivoEnum tipoDispositivo,
                                          String targa, String targaNazione);

  Optional<DocumentoDTO> findByIdentifierAndDocumentType(String uuid, String tipo);
}
