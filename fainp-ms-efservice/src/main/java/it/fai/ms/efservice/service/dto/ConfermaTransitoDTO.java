package it.fai.ms.efservice.service.dto;

import java.util.List;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.enumeration.ModalitaCarimentoTransitoEnum;

public class ConfermaTransitoDTO {

  @NotNull
  private TipoDispositivoEnum tipoDispositivo;

  @NotNull
  private ModalitaCarimentoTransitoEnum modalitaCaricamento;

  private String uuidDocumento;

  private List<Long> idSelezionati;

  private List<String> seriali;

  private String paese;

  @AssertTrue
  @JsonIgnore
  public boolean isValidForSelectedModalita() {
    switch (modalitaCaricamento) {
      case FILE_CSV:
      case FILE_GENERICO:
        return StringUtils.isNotEmpty(uuidDocumento);
      case MANUALE:
        return seriali != null && !seriali.isEmpty();
      case FILE_DA_FORNITORE:
        return idSelezionati != null && !idSelezionati.isEmpty();
      default:
        return false;
    }
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public ModalitaCarimentoTransitoEnum getModalitaCaricamento() {
    return modalitaCaricamento;
  }

  public void setModalitaCaricamento(ModalitaCarimentoTransitoEnum modalitaCaricamento) {
    this.modalitaCaricamento = modalitaCaricamento;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }

  public void setUuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
  }

  public List<Long> getIdSelezionati() {
    return idSelezionati;
  }

  public void setIdSelezionati(List<Long> idSelezionati) {
    this.idSelezionati = idSelezionati;
  }

  public List<String> getSeriali() {
    return seriali;
  }

  public void setSeriali(List<String> seriali) {
    this.seriali = seriali;
  }

  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  @Override
  public String toString() {
    return "ConfermaTransitoDTO [tipoDispositivo=" + tipoDispositivo + ", modalitaCaricamento=" + modalitaCaricamento + ", uuidDocumento="
           + uuidDocumento + ", idSelezionati=" + idSelezionati + ", seriali=" + seriali + ", paese=" + paese + "]";
  }


}
