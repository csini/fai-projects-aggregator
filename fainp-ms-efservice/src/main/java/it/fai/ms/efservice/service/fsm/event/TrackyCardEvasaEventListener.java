package it.fai.ms.efservice.service.fsm.event;

import static java.util.stream.Collectors.toSet;

import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.StatoDispositivoServizioServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class TrackyCardEvasaEventListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils fsmRichiestaUtils;

  private final StatoDispositivoServizioServiceExt sdsServiceExt;

  public TrackyCardEvasaEventListener(final FsmRichiestaUtils _fsmRichiestaUtils, final StatoDispositivoServizioServiceExt _sdsServiceExt) {
    fsmRichiestaUtils = _fsmRichiestaUtils;
    sdsServiceExt = _sdsServiceExt;
  }

  @EventListener
  public void consumeEvent(final TrackyCardEvasaEvent event) throws FsmExecuteCommandException {
    log.info("Receive Event Message: {}", event);
    Richiesta richiestaFromEvent = event.getRichiesta();
    log.info("Richiesta received: {}", richiestaFromEvent);
    Dispositivo dispositivo = event.getDispositivo();
    log.info("Dispositivo GoBox pending: {}", dispositivo);
    if (dispositivo != null) {
      Optional<Richiesta> optRichiesta = dispositivo.getRichiestas()
                                                    .stream()
                                                    .filter(r -> r.getTipo()
                                                                  .equals(TipoRichiesta.NUOVO_ORDINE))
                                                    .filter(r -> r.getStato()
                                                                  .equals(StatoRichiesta.ORDINE_SOSPESO))
                                                    .filter(r -> r.getTipoDispositivo() != null)
                                                    .filter(r -> r.getTipoDispositivo()
                                                                  .getNome()
                                                                  .equals(TipoDispositivoEnum.GO_BOX))
                                                    .findFirst();

      if (optRichiesta.isPresent()) {
        String nota = "TrackyCard rilasciata dalla Richiesta [" + richiestaFromEvent.getIdentificativo() + "]";
        Richiesta richiestaGoBox = optRichiesta.get();

        Richiesta richiestaTracky = getRichiestaTrackyCard(richiestaGoBox);
        String identificativoRequestTracky = richiestaTracky != null ? richiestaTracky.getIdentificativo() : null;
        String identificativoRequestEvent = richiestaFromEvent != null ? richiestaFromEvent.getIdentificativo() : null;
        log.info("Identificativo richiesta TrackyCard: {} - Identificativo Richiesta da evento: {}", identificativoRequestTracky,
                 identificativoRequestEvent);
        if (StringUtils.isNotBlank(identificativoRequestTracky) && StringUtils.isNotBlank(identificativoRequestEvent)
            && identificativoRequestTracky.equals(identificativoRequestEvent)) {
          Optional<Dispositivo> deviceTrackyOpt = richiestaTracky.getDispositivos()
                                                                 .stream()
                                                                 .filter(d -> d.getTipoDispositivo() != null)
                                                                 .filter(d -> d.getTipoDispositivo()
                                                                               .getNome()
                                                                               .equals(TipoDispositivoEnum.TRACKYCARD))
                                                                 .filter(d -> StringUtils.isNotBlank(d.getSeriale()))
                                                                 .findFirst();

          if (deviceTrackyOpt.isPresent()) {
            String serialeTrackyCard = deviceTrackyOpt.get()
                                                      .getSeriale();
            Set<StatoDispositivoServizio> sdsUpdated = dispositivo.getStatoDispositivoServizios()
                                                                  .stream()
                                                                  .map(sds -> {
                                                                    StatoDispositivoServizio sdsSaved = sdsServiceExt.save(sds.pan(serialeTrackyCard));
                                                                    return sdsSaved;
                                                                  })
                                                                  .collect(toSet());
            log.debug("Saved Sdss: {}", sdsUpdated);
          } else {
            log.error("Device trackycard related on device GoBox {} not found.", dispositivo);
          }
        } else {
          log.error("Impossible set PAN (Seriale) of TrackyCard on StatoDispositivoServizio on GoBox related");
        }

        fsmRichiestaUtils.changeStatusToRichiesta(richiestaGoBox, RichiestaEvent.MU_RIPETI_INVIO.name(), "", nota);
      } else {
        log.error("Not found richiesta GoBox related on device: {}", dispositivo);
      }
    } else {
      log.error("Dispositivo is null");
    }
  }

  private Richiesta getRichiestaTrackyCard(Richiesta richiestaGoBox) {
    String targa = richiestaGoBox.getAssociazione();
    String nazione = richiestaGoBox.getCountry();
    OrdineCliente ordineCliente = richiestaGoBox.getOrdineCliente();
    log.info("OrdineCliente of richiesta {}: {}", richiestaGoBox.getIdentificativo(), ordineCliente);
    Optional<Richiesta> optRichiestaTracky = ordineCliente != null ? ordineCliente.getRichiestas()
                                                                                  .stream()
                                                                                  .filter(r -> r.getTipo()
                                                                                                .equals(TipoRichiesta.NUOVO_ORDINE))
                                                                                  .filter(r -> r.getTipoDispositivo() != null)
                                                                                  .filter(r -> r.getTipoDispositivo()
                                                                                                .getNome()
                                                                                                .equals(TipoDispositivoEnum.TRACKYCARD))
                                                                                  .filter(r -> r.getAssociazione()
                                                                                                .equals(targa))
                                                                                  .filter(r -> r.getCountry()
                                                                                                .equals(nazione))
                                                                                  .findFirst()
                                                                   : Optional.empty();

    Richiesta richiesta = null;
    if (optRichiestaTracky.isPresent()) {
      richiesta = optRichiestaTracky.get();
      log.info("Find richiesta TrackyCard [ID: {} - Identificativo: {}]", richiesta.getId(), richiesta.getIdentificativo());
    }
    return richiesta;
  }

}
