package it.fai.ms.efservice.service.fsm.type.tollcollect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.FsmInoltroToll2GoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroToll2Go.FSM_INOLTRO_TOLL2GO)
public class FsmInoltroToll2Go extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_TOLL2GO = "fsmInoltroToll2Go";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroToll2Go(@Qualifier(FsmInoltroToll2GoConfig.INOLTRO_TOLL2GO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                           FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_TOLL2GO;
    deviceTypes = new TipoDispositivoEnum[] {  TipoDispositivoEnum.TOLL_COLLECT };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
