package it.fai.ms.efservice.security.jwt;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

  public static final String AUTHORIZATION_HEADER = "x-token";

  private TokenProvider tokenProvider;

  public JWTConfigurer(TokenProvider _tokenProvider) {
    tokenProvider = _tokenProvider;
  }

  @Override
  public void configure(final HttpSecurity _httpSecurity) throws Exception {
    _httpSecurity.addFilterBefore(new JWTFilter(tokenProvider), UsernamePasswordAuthenticationFilter.class);
  }
}
