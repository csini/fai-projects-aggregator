package it.fai.ms.efservice.service.impl;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.ViaCardServiceExt;

/**
 * Service Implementation for managing ViaCard Device.
 */
@Service
@Transactional
public class ViaCardServiceImplExt implements ViaCardServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ContrattoServiceExt       contrattoService;
  private final TipoDispositivoServiceExt tipoDispositivoService;

  private final DispositivoRepositoryExt deviceRepoExt;

  public ViaCardServiceImplExt(final DispositivoRepositoryExt _deviceRepoExt, ContrattoServiceExt contrattoService,
                               TipoDispositivoServiceExt tipoDispositivoService) {
    deviceRepoExt = _deviceRepoExt;
    this.contrattoService = contrattoService;
    this.tipoDispositivoService = tipoDispositivoService;
  }

  @Override
  public Set<Dispositivo> findByCodiceClienteAndTipoDispositivoAndStatoNotIn(String codiceCliente, TipoDispositivoEnum tipoDispositivo,
                                                                             Set<StatoDispositivo> states) {
    Set<Dispositivo> devices = deviceRepoExt.findByCodiceClienteAndTipoDispositivoAndStateNotIn(codiceCliente, tipoDispositivo, states);
    return devices;
  }

  @Override
  public long countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum tipoDispositivo, Contratto contratto) {
    Set<StatoDispositivo> filterState = StatoDispositivo.getBlueState();

    long numDevice = deviceRepoExt.count(DispositivoRepositoryExt.byContrattoAndTipoDispositivoAndStateNotIn(contratto, tipoDispositivo,
                                                                                                             filterState));

    log.info("Count of device type {} for Client-code {}, contract {} filtered by states not in {}: {}", tipoDispositivo,
             contratto.getClienteFai()
                      .getCodiceCliente(),
             contratto.getCodContrattoCliente(), filterState, numDevice);
    return numDevice;
  }

  @Override
  public long getNumberOfViaCardNecessaryForOrder(String codiceCliente, long numeroViaCard, long numeroTelepassIta) {
    long numOfViaCardNecesary = 0;
    log.info("Check necessary ViaCard by: CodiceCliente: {} , numero Via Card: {} , numero Telepass ITA: {}", codiceCliente, numeroViaCard,
             numeroTelepassIta);

    Produttore produttore = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.VIACARD)
                                                  .getProduttore();
    Optional<Contratto> contratto = contrattoService.findContrattoByProduttoreAndCodiceAzienda(produttore, codiceCliente);

    long numViaCard = 0;
    long numTelepassIta = 0;
    if (contratto.isPresent()) {
      numViaCard = countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum.VIACARD, contratto.get());
      numTelepassIta = countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum.TELEPASS_ITALIANO, contratto.get());
    }
    numViaCard += numeroViaCard;
    numTelepassIta += numeroTelepassIta;
    long diffViaCardTelepassIta = numViaCard - numTelepassIta;
    log.info("Diff to client code {} between numViaCard {} and numTelepassIta {} is : {}", codiceCliente, numViaCard, numTelepassIta,
             diffViaCardTelepassIta);
    if (diffViaCardTelepassIta < 0) {
      numOfViaCardNecesary = Math.abs(diffViaCardTelepassIta);
      log.info("La differenza tra num ViaCard e num Telepass ITA risulta essere minore di 0, procedo con la richiesta di {} VIACARD per il cliente: {}",
               numOfViaCardNecesary, codiceCliente);
    }

    if (numOfViaCardNecesary > numeroTelepassIta) {
      log.info("NumOfViaCardNecessary {} is more that numeroTelepassIta Ordered {} .... so fix number of VIACARD necessary for client code: {}",
               numOfViaCardNecesary, numeroTelepassIta, codiceCliente);
      numOfViaCardNecesary = numeroTelepassIta;
    }

    log.info("Number of VIACARD necessary for client code {} is: {}", codiceCliente, numOfViaCardNecesary);
    return numOfViaCardNecesary;
  }

}
