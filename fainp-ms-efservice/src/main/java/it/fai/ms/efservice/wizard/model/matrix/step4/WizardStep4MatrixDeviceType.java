package it.fai.ms.efservice.wizard.model.matrix.step4;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;

public class WizardStep4MatrixDeviceType implements Serializable {

  private static final long serialVersionUID = 9168734021580320365L;

  private WizardMatrixActivability activability;
  private String                   id;
  private Instant activationDate;
  private String seriale;

  public WizardStep4MatrixDeviceType(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep4MatrixDeviceType) _obj).getId(), id)
                 && Objects.equals(((WizardStep4MatrixDeviceType) _obj).getActivability(), activability);
    }
    return isEquals;
  }

  public boolean isNotActivable() {
    return activability.getActivabilityState()
                       .isKo();
  }

  public WizardMatrixActivability getActivability() {
    return activability;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, activability);
  }

  public void setActivability(final WizardMatrixActivability _activability) {
    activability = _activability;
  }

  @Override
  public String toString() {
    return "WizardStep4MatrixDeviceType{" +
      "activability=" + activability +
      ", id='" + id + '\'' +
      ", activationDate=" + activationDate +
      ", seriale='" + seriale + '\'' +
      '}';
  }

  public Instant getActivationDate() {
    return activationDate;
  }

  public void setActivationDate(Instant activationDate) {
    this.activationDate = activationDate;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }
}
