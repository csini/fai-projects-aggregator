package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toSet;

import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.web.util.ResponseUtil;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;
import it.fai.ms.efservice.service.dto.ChangeStatoDispositivoServizioDTO;

@Service
@Transactional
public class StatoDispositivoServizioServiceExt {

  private static final Logger log = LoggerFactory.getLogger(StatoDispositivoServizioServiceExt.class);

  @Resource
  private StatoDispositivoServizioRepositoryExt statoDispositivoServizioRepositoryExt;

  final private DispositivoServiceExt dispositivoServiceExt;

  final private TipoServizioServiceExt tipoServizioServiceExt;

  public StatoDispositivoServizioServiceExt(StatoDispositivoServizioRepositoryExt statoDispositivoServizioRepositoryExt,
                                            DispositivoServiceExt _dispositivoServiceExt, TipoServizioServiceExt _tipoServizioServiceExt) {
    this.statoDispositivoServizioRepositoryExt = statoDispositivoServizioRepositoryExt;
    dispositivoServiceExt = _dispositivoServiceExt;
    tipoServizioServiceExt = _tipoServizioServiceExt;
  }

  public ResponseEntity<List<StatoDispositivoServizio>> findAssociazioneDVbyDispositivoId(Dispositivo dispositivo) {
    // return ResponseUtil.wrapOrNotFound(Optional.ofNullable(
    // statoDispositivoServizioRepositoryExt.findStatoDispositivoServizibyDispositivoId(dispositivo)));
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(statoDispositivoServizioRepositoryExt.findAllByDispositivo(dispositivo)));
  }

  public ResponseEntity<Integer> findAnomaly(Dispositivo dispositivo) {
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(statoDispositivoServizioRepositoryExt.findAnomaly(dispositivo)));
  }

  public Page<StatoDispositivoServizio> getAllByDispostivoUuid(String uuidDispositivo, Pageable pageable) {
    return statoDispositivoServizioRepositoryExt.findAllByUuidDispositivo(uuidDispositivo, pageable);
  }

  public Set<StatoDispositivoServizio> changeStatusServizioDispositivo(Set<String> identificativoServizi, String identificativoDispositivo,
                                                                       String operazione) {
    Set<StatoDispositivoServizio> sdssUpdated = new HashSet<>();
    sdssUpdated = identificativoServizi.stream()
                                       .map(serviceType -> {
                                         log.info("Search StatoDispositivoServizio by Dispositivo {} and TipoServizio {} to update Stato",
                                                  identificativoDispositivo, serviceType);
                                         StatoDispositivoServizio sds = null;

                                         StatoDS stato = null;
                                         switch (operazione) {
                                         case "ATTIVA":
                                           stato = StatoDS.ATTIVO;
                                           sds = searchStatoDispositivoServizioOrCreate(identificativoDispositivo, serviceType,
                                                                                        StatoDS.NON_ATTIVO);
                                           break;

                                         case "DISATTIVA":
                                           stato = StatoDS.NON_ATTIVO;
                                           sds = searchStatoDispositivoServizioOrCreate(identificativoDispositivo, serviceType,
                                                                                        StatoDS.ATTIVO);
                                           break;

                                         default:
                                           break;
                                         }
                                         
                                         sds = updateStatoOnStatoDispositivoServizio(sds, stato);
                                         return sds;
                                       })
                                       .collect(toSet());

    return sdssUpdated;
  }

  private StatoDispositivoServizio updateStatoOnStatoDispositivoServizio(StatoDispositivoServizio sds, StatoDS stato) {
    if (sds != null && stato != null) {
      // TODO finire l'implementazione......inviare
      // le
      // informazioni al produttore corretto;
      sds = save(sds.stato(stato));
      log.info("update stato on sds: {}", sds);
    } else {
      log.warn("StatoDispositivoServizio is {} - StatoDS is {}", sds, stato);
    }
    return sds;
  }

  private StatoDispositivoServizio searchStatoDispositivoServizioOrCreate(String identificativoDispositivo, String serviceType,
                                                                          StatoDS stato) {
    StatoDispositivoServizio sds = statoDispositivoServizioRepositoryExt.findByDispositivoUuidAndTipoServizioAndStato(identificativoDispositivo,
                                                                                                                      serviceType, stato);
    if (sds == null) {
      log.debug("Create Stato Dispositivo Servizio by: Device Identifier: {} - Service type: {}", identificativoDispositivo, serviceType);
      Dispositivo dispositivo = dispositivoServiceExt.findOneByIdentificativo(identificativoDispositivo);
      TipoServizio tipoServizio = tipoServizioServiceExt.findByNome(serviceType);
      sds = new StatoDispositivoServizio().dispositivo(dispositivo)
                                          .tipoServizio(tipoServizio);
    }

    return sds;
  }

  public StatoDispositivoServizio save(StatoDispositivoServizio sds) {
    return statoDispositivoServizioRepositoryExt.save(sds);
  }

  public ChangeStatoDispositivoServizioDTO changeStatoDispositivoServizio(final ChangeStatoDispositivoServizioDTO dto) {
    log.debug("Request to change StatoDispositivoServizio by dto : {}", dto);

    Dispositivo device = dispositivoServiceExt.findOneByIdentificativo(dto.getUuid());
    if (device == null) {
      log.warn("Device with uuid '{}' not found", dto.getUuid());
      return null;
    }

    TipoServizio tipoServizio = tipoServizioServiceExt.findByNome(dto.getTipoServizio()
                                                                     .name());
    if (tipoServizio == null) {
      log.warn("Service type '{}' not found", dto.getTipoServizio()
                                                 .name());
      return null;
    }

    StatoDispositivoServizio sds = statoDispositivoServizioRepositoryExt.findFirstByDispositivoAndTipoServizio(device, tipoServizio);
    if (sds == null) {
      log.info("Not found state device service with device '{}' and service type '{}'", dto.getUuid(), dto.getTipoServizio()
                                                                                                          .name());
      sds = new StatoDispositivoServizio();
      sds.setDispositivo(device);
      sds.setTipoServizio(tipoServizio);
    }
    sds.setDataAttivazione(dto.getDataAttivazione() != null ? dto.getDataAttivazione()
                                                                 .truncatedTo(ChronoUnit.MILLIS)
                                                            : null);
    sds.setDataDisattivazione(dto.getDataDisattivazione() != null ? dto.getDataDisattivazione()
                                                                       .truncatedTo(ChronoUnit.MILLIS)
                                                                  : null);
    sds.setPan(dto.getPan() != null ? dto.getPan() : null);
    sds.setStato(dto.getStato());
    if (dto.getStato()
           .equals(StatoDS.ATTIVO)) {
      sds.setDataDisattivazione(null);
    }

    statoDispositivoServizioRepositoryExt.save(sds);

    return dto;
  }

  public List<StatoDispositivoServizio> findbyDispositivo(Dispositivo device) {
    log.debug("Find services by device : {}", device);
    return statoDispositivoServizioRepositoryExt.findByDispositivo(device);
  }

}
