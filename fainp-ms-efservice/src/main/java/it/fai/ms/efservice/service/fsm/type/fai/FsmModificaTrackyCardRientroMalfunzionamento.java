package it.fai.ms.efservice.service.fsm.type.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.fai.FsmModificaTrackyCardRientroMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTrackyCardRientroMalfunzionamento.FSM_MOD_TRACKYCARD_RIENTRO_MALF)
public class FsmModificaTrackyCardRientroMalfunzionamento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TRACKYCARD_RIENTRO_MALF = "fsmModificaTrackyCardRientroMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTrackyCardRientroMalfunzionamento(@Qualifier(FsmModificaTrackyCardRientroMalfunzionamentoConfig.MOD_TRACKYCARD_RIENTRO_MALF) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                                      FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TRACKY_RIENTRO_MALFUNZIONAMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TRACKYCARD };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
