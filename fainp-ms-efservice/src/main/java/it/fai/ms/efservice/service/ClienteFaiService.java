package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import java.util.List;

/**
 * Service Interface for managing ClienteFai.
 */
public interface ClienteFaiService {

    /**
     * Save a clienteFai.
     *
     * @param clienteFaiDTO the entity to save
     * @return the persisted entity
     */
    ClienteFaiDTO save(ClienteFaiDTO clienteFaiDTO);

    /**
     * Get all the clienteFais.
     *
     * @return the list of entities
     */
    List<ClienteFaiDTO> findAll();

    /**
     * Get the "id" clienteFai.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ClienteFaiDTO findOne(Long id);

    /**
     * Delete the "id" clienteFai.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
