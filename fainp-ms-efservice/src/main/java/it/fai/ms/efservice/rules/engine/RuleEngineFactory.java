package it.fai.ms.efservice.rules.engine;

import static java.text.MessageFormat.format;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.rules.engine.impl.RuleEngine_All_Devices;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_DartfordCrossing;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_Frejus;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_GoBox;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_TelepassEU;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_TelepassITA;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_TesseraCaronte;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_TollCollect;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_TrackyCard;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_ViaToll;
import it.fai.ms.efservice.rules.engine.impl.RuleEngine_Device_Vispro;
import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

@Component
public class RuleEngineFactory {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  public RuleEngineFactory() {
  }

  public RuleEngine newRuleEngine(final RuleContext _ruleContext, final RuleBucket _ruleId) {
    Objects.requireNonNull(_ruleContext, "RuleContext is mandatory");
    Objects.requireNonNull(_ruleId, "RuleId is mandatory");
    final RuleEngine ruleEngine = build(_ruleContext, _ruleId);
    _log.debug("Rule engine {} created: {}", _ruleId, ruleEngine);
    return ruleEngine;
  }

  private RuleEngine build(final RuleContext _ruleContext, final RuleBucket _ruleId) {
    RuleEngine ruleEngine = null;
    switch (_ruleId) {
    case DEVICE_TELEPASS_EU:
    case DEVICE_TELEPASS_EU_SAT:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_TelepassEU(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                      _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
    case DEVICE_TELEPASS_ITA:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_TelepassITA(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                       _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;

    case DEVICE_TRACKYCARD:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_TrackyCard(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                      _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;

    case DEVICE_GOBOX:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_GoBox(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                 _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
    case DEVICE_TOLL_COLLECT:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_TollCollect(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                       _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
    case DEVICE_FREJUS:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_Frejus(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                  _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
    case DEVICE_TESSERA_CARONTE:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_TesseraCaronte(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;

    case DEVICE_DARTFORD_CROSSING:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_DartfordCrossing(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                            _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;

    case DEVICE_VIA_TOLL:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_ViaToll(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                   _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
      
    case DEVICE_VISPRO:
      if (_ruleContext.getDeviceTypeRuleContext()
                      .getId()
                      .getId()
                      .equals(_ruleId.deviceType())) {
        ruleEngine = new RuleEngine_Device_Vispro(_ruleContext.getDeviceTypeRuleContext(), _ruleContext.getVehicleRuleContext(),
                                                            _ruleContext.getServiceTypeRuleContext());
      } else {
        ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      }
      break;
    case ALL_DEVICE:
      ruleEngine = new RuleEngine_All_Devices(_ruleContext.getVehicleRuleContext());
      break;
    default:
      throw new IllegalArgumentException(format("Rule {0} not implemented", _ruleId));
    }

    return ruleEngine;
  }

}
