package it.fai.ms.efservice.config;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import io.github.jhipster.config.JHipsterConstants;
import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

  private static final String PACKAGE_DOMAIN = "it.fai.ms.efservice.domain.*";

  private static final String INSTANCE_CACHE_NAME = "faiefservice";

  private final Logger log = LoggerFactory.getLogger(CacheConfiguration.class);

  private final Environment env;

  private final ServerProperties serverProperties;

  private final ApplicationProperties applicationProperties;

  private final DiscoveryClient discoveryClient;

  private Registration registration;

  public CacheConfiguration(Environment env, ServerProperties serverProperties, DiscoveryClient discoveryClient,
                            ApplicationProperties applicationProperties) {
    this.env = env;
    this.serverProperties = serverProperties;
    this.discoveryClient = discoveryClient;
    this.applicationProperties = applicationProperties;
  }

  @Autowired(required = false)
  public void setRegistration(Registration registration) {
    this.registration = registration;
  }

  @PreDestroy
  public void destroy() {
    log.info("Closing Cache Manager");
    Hazelcast.shutdownAll();
  }

  @Bean
  public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
    log.debug("Starting HazelcastCacheManager");
    CacheManager cacheManager = new com.hazelcast.spring.cache.HazelcastCacheManager(hazelcastInstance);
    return cacheManager;
  }

  @Bean
  public HazelcastInstance hazelcastInstance(JHipsterProperties jHipsterProperties) {
    log.debug("Configuring Hazelcast");
    HazelcastInstance hazelCastInstance = Hazelcast.getHazelcastInstanceByName(INSTANCE_CACHE_NAME);
    if (hazelCastInstance != null) {
      log.debug("Hazelcast already initialized");
      return hazelCastInstance;
    }
    Config config = new Config();
    config.setInstanceName(INSTANCE_CACHE_NAME);
    config.getNetworkConfig()
          .getJoin()
          .getMulticastConfig()
          .setEnabled(false);
    if (this.registration == null) {
      log.warn("No discovery service is set up, Hazelcast cannot create a cluster.");
    } else {
      // The serviceId is by default the application's name, see Spring Boot's eureka.instance.appname property
      String serviceId = registration.getServiceId();
      log.debug("Configuring Hazelcast clustering for instanceId: {}", serviceId);

      config.getNetworkConfig()
            .setPort(serverProperties.getPort() + 5701);
      config.getNetworkConfig()
            .getJoin()
            .getTcpIpConfig()
            .setEnabled(true);

      // In development, everything goes through 127.0.0.1, with a different port
      if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
        String urlManagementCenter = applicationProperties.getHostManagementHazelcast();
        if (StringUtils.isNotBlank(urlManagementCenter)) {
          config.getManagementCenterConfig()
                .setEnabled(true)
                .setUrl(urlManagementCenter);
        }

        for (ServiceInstance instance : discoveryClient.getInstances(serviceId)) {
          String clusterMember = instance.getHost() + ":" + (serverProperties.getPort() + 5701);
          log.debug("Adding Hazelcast (prod) cluster member " + clusterMember);
          config.getNetworkConfig()
                .getJoin()
                .getTcpIpConfig()
                .addMember(clusterMember);
        }
      } else {
        log.debug("Application is running with the {} profile, Hazelcast " + "cluster will only work with localhost instances", env);

        System.setProperty("hazelcast.local.localAddress", "127.0.0.1");
        for (ServiceInstance instance : discoveryClient.getInstances(serviceId)) {
          String clusterMember = "127.0.0.1:" + (instance.getPort() + 5701);
          log.debug("Adding Hazelcast cluster member " + clusterMember);
          config.getNetworkConfig()
                .getJoin()
                .getTcpIpConfig()
                .addMember(clusterMember);
        }
      }
    }
    config.getMapConfigs()
          .put("default", initializeDefaultMapConfig());
    config.getMapConfigs()
          .put(PACKAGE_DOMAIN, initializeDomainMapConfig(jHipsterProperties));
    return Hazelcast.newHazelcastInstance(config);
  }

  private MapConfig initializeDefaultMapConfig() {
    MapConfig mapConfig = new MapConfig();

    /*
     * Number of backups. If 1 is set as the backup-count for example, then all entries of the map will be copied to
     * another JVM for fail-safety. Valid numbers are 0 (no backup), 1, 2, 3.
     */
    mapConfig.setBackupCount(0);

    /*
     * Valid values are: NONE (no eviction), LRU (Least Recently Used), LFU (Least Frequently Used). NONE is the
     * default.
     */
    mapConfig.setEvictionPolicy(EvictionPolicy.LRU);

    /*
     * Maximum size of the map. When max size is reached, map is evicted based on the policy defined. Any integer
     * between 0 and Integer.MAX_VALUE. 0 means Integer.MAX_VALUE. Default is 0.
     */
    mapConfig.setMaxSizeConfig(new MaxSizeConfig(0, MaxSizeConfig.MaxSizePolicy.USED_HEAP_SIZE));

    return mapConfig;
  }

  private MapConfig initializeDomainMapConfig(JHipsterProperties jHipsterProperties) {
    MapConfig mapConfig = new MapConfig();
    mapConfig.setTimeToLiveSeconds(jHipsterProperties.getCache()
                                                     .getHazelcast()
                                                     .getTimeToLiveSeconds());
    return mapConfig;
  }
}
