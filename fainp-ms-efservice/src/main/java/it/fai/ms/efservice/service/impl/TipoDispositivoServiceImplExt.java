package it.fai.ms.efservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.mapper.TipoDispositivoMapper;

/**
 * Service Implementation for managing TipoDispositivo.
 */
@Service
@Transactional(readOnly = true)
public class TipoDispositivoServiceImplExt implements TipoDispositivoServiceExt {

  private final Logger log = LoggerFactory.getLogger(TipoDispositivoServiceImplExt.class);

  private final TipoDispositivoRepository tipoDispositivoRepository;

  private final TipoDispositivoMapper tipoDispositivoMapper;

  public TipoDispositivoServiceImplExt(TipoDispositivoRepository tipoDispositivoRepository, TipoDispositivoMapper tipoDispositivoMapper) {
    this.tipoDispositivoRepository = tipoDispositivoRepository;
    this.tipoDispositivoMapper = tipoDispositivoMapper;
  }

  @Override
  public TipoDispositivo findOneByNome(TipoDispositivoEnum nome) {
    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOneByNome(nome);
    return tipoDispositivo;
  }
  
  public TipoDispositivo findTipoDispositivoWithRelationship(Long idTipoDispositivo) {
    return tipoDispositivoRepository.findOneWithEagerRelationships(idTipoDispositivo);
  }
  
  @Override
  public TipoDispositivo findOneWithRelationshipByNome(TipoDispositivoEnum nome) {
    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOneByNome(nome);
    tipoDispositivo = findTipoDispositivoWithRelationship(tipoDispositivo.getId());
    return tipoDispositivo;
  }
  
  @Override
  public TipoDispositivo findOneById(Long id) {
    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOneById(id);
    return tipoDispositivo;
  }

}
