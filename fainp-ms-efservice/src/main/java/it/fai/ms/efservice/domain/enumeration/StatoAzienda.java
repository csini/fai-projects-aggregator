package it.fai.ms.efservice.domain.enumeration;

/**
 * The StatoAzienda enumeration.
 */
public enum StatoAzienda {
    ATTIVO, SOSPESO, BLOCCATO
}
