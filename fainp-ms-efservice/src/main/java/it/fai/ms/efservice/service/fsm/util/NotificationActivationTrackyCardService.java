package it.fai.ms.efservice.service.fsm.util;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.MailActivatedDeviceService;
import it.fai.ms.efservice.service.TrackyCardGoBoxService;
import it.fai.ms.efservice.service.fsm.event.TrackyCardEvasaEvent;

@Service
public class NotificationActivationTrackyCardService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ApplicationEventPublisher eventPublisher;

  private final TrackyCardGoBoxService trackyGoBoxService;

  private final MailActivatedDeviceService mailActivatedDeviceService;

  public NotificationActivationTrackyCardService(final ApplicationEventPublisher _eventPublisher,
                                                 final TrackyCardGoBoxService _trackyGoBoxService,
                                                 final MailActivatedDeviceService _mailActivatedDeviceService) {
    eventPublisher = _eventPublisher;
    trackyGoBoxService = _trackyGoBoxService;
    mailActivatedDeviceService = _mailActivatedDeviceService;
  }

  public void notifyActivation(final Richiesta richiesta) {
    log.info("Notify activation from request: {}", richiesta);
    Optional<Dispositivo> optDevice = trackyGoBoxService.findPendingGoBoxRelatedByRichiesta(richiesta);
    if (optDevice.isPresent()) {
      log.info("Send event to notify activated TrackyCard");
      eventPublisher.publishEvent(new TrackyCardEvasaEvent(richiesta, optDevice.get()));

      sendMailActivationTrackyCardToPendingGoBox(richiesta);
    } else {
      log.info("Device GoBox pending not present to Richiesta: {}", richiesta);
    }

  }

  private void sendMailActivationTrackyCardToPendingGoBox(final Richiesta richiesta) {
    Contratto contratto = richiesta.getContratto();
    if (contratto != null && contratto.getClienteFai() != null && contratto.getClienteFai()
                                                                           .getCodiceCliente() != null) {
      mailActivatedDeviceService.sendActivationTrackyCard(contratto.getClienteFai()
                                                                   .getCodiceCliente());
    } else {
      log.error("Not send notification activation TrackyCard because codice cliente is null or empty [Contratto: {} of the Richiesta: {}]",
                contratto, richiesta);
    }

  }

}
