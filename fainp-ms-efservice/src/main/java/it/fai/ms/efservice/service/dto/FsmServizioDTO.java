package it.fai.ms.efservice.service.dto;

public class FsmServizioDTO extends FsmDTO {

  private static final long serialVersionUID = -2552933652739265674L;
  
  private String dispositivoUuid;

  public String getDispositivoUuid() {
    return dispositivoUuid;
  }
  
  public FsmServizioDTO dispositivoUuid(String dispositivoUuid) {
    this.dispositivoUuid = dispositivoUuid;
    return this;
  }

  public void setDispositivoUuid(String dispositivoUuid) {
    this.dispositivoUuid = dispositivoUuid;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("FsmServizioDTO ");
    sb.append("[");
    sb.append("operazione: ");
    sb.append(getOperazione());
    sb.append(", identificativi (Nome Servizi): ");
    sb.append(getStringIdentificativi());
    sb.append(", dispositivoUUID: ");
    sb.append(dispositivoUuid);
    sb.append(", nota");
    sb.append(getNota());
    sb.append("]");
    return sb.toString();
  }
  
  

}
