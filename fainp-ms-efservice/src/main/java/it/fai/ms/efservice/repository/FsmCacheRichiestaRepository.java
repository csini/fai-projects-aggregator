package it.fai.ms.efservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.FsmCacheRichiesta;

@Repository
public interface FsmCacheRichiestaRepository extends JpaRepository<FsmCacheRichiesta, Long> {

}
