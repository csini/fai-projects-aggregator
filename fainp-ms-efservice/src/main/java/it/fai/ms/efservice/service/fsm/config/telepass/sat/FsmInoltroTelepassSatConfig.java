package it.fai.ms.efservice.service.fsm.config.telepass.sat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiestaViaCard;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoTsat;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionWsAbbinamento;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardAttesaTransito;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtClient;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtFai;
import it.fai.ms.efservice.service.fsm.guard.GuardTelepassItaInDepositoKo;
import it.fai.ms.efservice.service.fsm.guard.GuardTelepassItaInDepositoOk;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = "inoltroTSAT")
public class FsmInoltroTelepassSatConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  // FIXME - macchina a stati da rivedere completamente....

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue              senderFsmService;
  private final ManageDevicesInStorageService deviceInStorageService;
  private final ApplicationEventPublisher     applicationEventPublisher;
  private final ManageDeviceSentModeService   manageDeviceSentModeService;

  public FsmInoltroTelepassSatConfig(final FsmSenderToQueue _senderFsmService, final ManageDevicesInStorageService _deviceInStorageService,
                                     final ApplicationEventPublisher _applicationEventPublisher,
                                     final ManageDeviceSentModeService _manageDeviceSentModeService) {
    senderFsmService = _senderFsmService;
    deviceInStorageService = _deviceInStorageService;
    applicationEventPublisher = _applicationEventPublisher;
    manageDeviceSentModeService = _manageDeviceSentModeService;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmInoltroTSAT");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, ctx -> log.trace("Target: " + ctx.getTarget()
                                                                                                 .getIds()))
          .states(StatoRichiestaUtil.getStateOfInoltroTSAT());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.VERIFICA_ACCETTAZIONE)
               .event(RichiestaEvent.MU_VERIFICA_VC)
               .action(new FsmActionDispositivoTsat(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VERIFICA_ACCETTAZIONE)
               .target(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC)
               .guard(new GuardTelepassItaInDepositoKo(deviceInStorageService))
               .action(new FsmActionCreateNewRichiestaViaCard(senderFsmService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC)
               .target(StatoRichiesta.VERIFICA_ACCETTAZIONE)
               .event(RichiestaEvent.MS_VC_PRESENTI)
               .action(new FsmActionDispositivoTsat(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VERIFICA_ACCETTAZIONE)
               .target(StatoRichiesta.ABBINATO)
               .guard(new GuardTelepassItaInDepositoOk(deviceInStorageService, applicationEventPublisher))
               .action(new FsmActionDispositivoTsat(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ABBINATO)
               .target(StatoRichiesta.INOLTRATO)
               .action(new FsmActionWsAbbinamento())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA)
               .event(RichiestaEvent.MS_WS_KO)
               .action(new FsmActionGeneric()) // Sulla action del KO bisogna capire cosa fare.
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ACCETTATO_FORNITORE)
               .event(RichiestaEvent.MS_WS_OK)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE) // Removed because in the file: 'Processo Ordine
                                                                // Unificato v1.5-Rev-INT' is an auto-transition;
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.MU_PLATE_FREE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO_FORNITORE)
               .target(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.IN_SPEDIZIONE))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .guard(new GuardIsToBeSentAtFai(manageDeviceSentModeService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .guard(new GuardAttesaTransito(manageDeviceSentModeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .event(RichiestaEvent.MU_SPEDIZIONE_FAI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .guard(new GuardIsToBeSentAtClient(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_DAL_FORNITORE))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_SPEDIZIONE_COMPLETA)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_DA_FAI))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.CONCLUSO_NON_SPEDITO)
               .event(RichiestaEvent.MU_CHIUSURA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.NON_SPEDITO));
  }

}
