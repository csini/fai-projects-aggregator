package it.fai.ms.efservice.service;

import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;

public interface RequestNotificationService {

  void sendNotification(DeviceEventMessage deviceEventMessage);

}
