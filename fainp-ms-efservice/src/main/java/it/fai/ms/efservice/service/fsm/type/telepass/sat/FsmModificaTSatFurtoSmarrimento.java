/**
 * 
 */
package it.fai.ms.efservice.service.fsm.type.telepass.sat;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

/**
 * @author Luca Vassallo
 */
@Service(value = "fsmModificaTSatFurtoSmarrimento")
public class FsmModificaTSatFurtoSmarrimento extends AbstractFsmRichiesta {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory;

  private FsmRichiestaCacheService cacheService;

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaTSatFurtoSmarrimento(@Qualifier("ModTSatFurtoSmarrimento") StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                         FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
  }

  @Override
  public StateMachine<StatoRichiesta, RichiestaEvent> getStateMachine() {
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = stateMachineFactory.getStateMachine(FsmType.MOD_TSAT_FURTO_SMARRIMENTO.fsmName());
    log.debug("Get state machine {}", stateMachine.getId());
    return stateMachine;
  }

  @Override
  public Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta, String nota) {
    Long richiestaId = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    StatoRichiesta statoAttuale = richiesta.getStato();
    log.debug("Switch state by command {} to Richiesta [ID: {} - Identificativo: {}] with actual state: {}", command, richiestaId,
              identificativo, statoAttuale);

    boolean availableCommand = isAvailableCommand(command, richiesta);
    if (availableCommand) {
      StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = getStateMachine();
      stateMachine.start();

      String fsmKey = String.valueOf(richiestaId);
      if (StringUtils.isNotBlank(fsmKey)) {
        String jsonContext = cacheService.getStateMachineContext(fsmKey);
        if (StringUtils.isNotBlank(jsonContext)) {
          if (jsonContext.contains(stateMachine.getId())) {
            log.debug("Restore state machine {} for FsmKey {}", stateMachine.getId(), fsmKey);
            stateMachine = cacheService.restore(stateMachine, fsmKey);
          }
        }
      }

      if (command != null) {

        sendEventToFsm(stateMachine, command, richiesta, nota);
        log.info("Send command: {} to Richiesta [ID: {} - Identificativo: {}]", command, richiestaId, identificativo);

        cacheService.persist(stateMachine, fsmKey);
        richiesta = getUserAvailableCommandAndSetOnRichiesta(richiesta);
      } else {
        log.warn("Enum command (RichiestaEvent) is {} ", command);
      }
    } else {
      String messageError = String.format("Command %s is not available because Richiesta [ID: %s - Identificativo: %s] state is %s",
                                          command, richiestaId, identificativo, statoAttuale);
      log.error(messageError);
      throw new RuntimeException(messageError);
    }

    log.debug("=> FINISH switch state with command: {} to Richiesta [ID: {} - Identificativo: {}]", command, richiestaId, identificativo);
    return richiesta;
  }

}
