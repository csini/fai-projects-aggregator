package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceReturnMessageProducerService {

  void deactivateDevice(Richiesta orderRequestEntity);

  void deactivateDeviceTelepassIta(Richiesta richiesta);

}
