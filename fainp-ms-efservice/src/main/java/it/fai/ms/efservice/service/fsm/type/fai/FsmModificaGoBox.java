package it.fai.ms.efservice.service.fsm.type.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.fai.FsmModificaGoBoxConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaGoBox.FSM_MODIFICA_GO_BOX)
public class FsmModificaGoBox extends FsmRichiestaGeneric {

  public static final String FSM_MODIFICA_GO_BOX = "fsmModificaGoBox";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaGoBox(@Qualifier(FsmModificaGoBoxConfig.MODIFICA_GO_BOX) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                          FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.MODIFICA_GOBOX;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.GO_BOX };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.INOLTRO_GOBOX;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
