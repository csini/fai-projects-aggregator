package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.AssociazioneDVService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AssociazioneDV.
 */
@RestController
@RequestMapping("/api")
public class AssociazioneDVResource {

    private final Logger log = LoggerFactory.getLogger(AssociazioneDVResource.class);

    private static final String ENTITY_NAME = "associazioneDV";

    private final AssociazioneDVService associazioneDVService;

    public AssociazioneDVResource(AssociazioneDVService associazioneDVService) {
        this.associazioneDVService = associazioneDVService;
    }

    /**
     * POST  /associazione-dvs : Create a new associazioneDV.
     *
     * @param associazioneDVDTO the associazioneDVDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new associazioneDVDTO, or with status 400 (Bad Request) if the associazioneDV has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/associazione-dvs")
    @Timed
    public ResponseEntity<AssociazioneDVDTO> createAssociazioneDV(@RequestBody AssociazioneDVDTO associazioneDVDTO) throws URISyntaxException {
        log.debug("REST request to save AssociazioneDV : {}", associazioneDVDTO);
        if (associazioneDVDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new associazioneDV cannot already have an ID")).body(null);
        }
        AssociazioneDVDTO result = associazioneDVService.save(associazioneDVDTO);
        return ResponseEntity.created(new URI("/api/associazione-dvs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /associazione-dvs : Updates an existing associazioneDV.
     *
     * @param associazioneDVDTO the associazioneDVDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated associazioneDVDTO,
     * or with status 400 (Bad Request) if the associazioneDVDTO is not valid,
     * or with status 500 (Internal Server Error) if the associazioneDVDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/associazione-dvs")
    @Timed
    public ResponseEntity<AssociazioneDVDTO> updateAssociazioneDV(@RequestBody AssociazioneDVDTO associazioneDVDTO) throws URISyntaxException {
        log.debug("REST request to update AssociazioneDV : {}", associazioneDVDTO);
        if (associazioneDVDTO.getId() == null) {
            return createAssociazioneDV(associazioneDVDTO);
        }
        AssociazioneDVDTO result = associazioneDVService.save(associazioneDVDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, associazioneDVDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /associazione-dvs : get all the associazioneDVS.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of associazioneDVS in body
     */
    @GetMapping("/associazione-dvs")
    @Timed
    public List<AssociazioneDVDTO> getAllAssociazioneDVS() {
        log.debug("REST request to get all AssociazioneDVS");
        return associazioneDVService.findAll();
        }

    /**
     * GET  /associazione-dvs/:id : get the "id" associazioneDV.
     *
     * @param id the id of the associazioneDVDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the associazioneDVDTO, or with status 404 (Not Found)
     */
    @GetMapping("/associazione-dvs/{id}")
    @Timed
    public ResponseEntity<AssociazioneDVDTO> getAssociazioneDV(@PathVariable Long id) {
        log.debug("REST request to get AssociazioneDV : {}", id);
        AssociazioneDVDTO associazioneDVDTO = associazioneDVService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(associazioneDVDTO));
    }

    /**
     * DELETE  /associazione-dvs/:id : delete the "id" associazioneDV.
     *
     * @param id the id of the associazioneDVDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/associazione-dvs/{id}")
    @Timed
    public ResponseEntity<Void> deleteAssociazioneDV(@PathVariable Long id) {
        log.debug("REST request to delete AssociazioneDV : {}", id);
        associazioneDVService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
