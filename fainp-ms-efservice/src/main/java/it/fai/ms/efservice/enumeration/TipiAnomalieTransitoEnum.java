package it.fai.ms.efservice.enumeration;

public enum TipiAnomalieTransitoEnum {
  SERIALE_NON_PRESENTE_IN_SISTEMA,
  SERIALE_CON_STATO_INVALIDO,
  ERRORE_GENERICO;
}
