package it.fai.ms.efservice.service.fsm.config.viacard;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.MailActivatedDeviceService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionNotificationActivatedViaCardAndSpedizioneDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionRichiestaContratto;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendMail;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendMessageToGenerateFileViaCard;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardAttesaTransito;
import it.fai.ms.efservice.service.fsm.guard.GuardContractCodeUser;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtClient;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtFai;
import it.fai.ms.efservice.service.fsm.guard.GuardNotContractCodeUser;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroViaCardConfig.INOLTRO_VIACARD)
public class FsmInoltroViaCardConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_VIACARD = "inoltroViaCard";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  private final JmsProperties jmsProperties;

  private final DeviceProducerService deviceProducerService;

  private final MailActivatedDeviceService mailActivatedDeviceService;

  private final ManageDeviceSentModeService manageDeviceSentModeService;

  public FsmInoltroViaCardConfig(final FsmSenderToQueue _senderFsmService, final JmsProperties _jmsProperties,
                                 final DeviceProducerService _deviceProducerService,
                                 final MailActivatedDeviceService _mailActivatedDeviceService,
                                 final ManageDeviceSentModeService _manageDeviceSentModeService) {
    senderFsmService = _senderFsmService;
    jmsProperties = _jmsProperties;
    deviceProducerService = _deviceProducerService;
    mailActivatedDeviceService = _mailActivatedDeviceService;
    manageDeviceSentModeService = _manageDeviceSentModeService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_VIACARD.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroViaCard();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.INCOMPLETO_TECNICO)
               .guard(new GuardNotContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.COMPLETO)
               .guard(new GuardContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.INCOMPLETO_TECNICO)
               .target(StatoRichiesta.ATTESA_RISPOSTA)
               .action(new FsmActionRichiestaContratto(senderFsmService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_RISPOSTA)
               .target(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .event(RichiestaEvent.MS_CTRRI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .target(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .guard(new GuardNotContractCodeUser())
               .action(new FsmActionSendMail(jmsProperties))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .target(StatoRichiesta.COMPLETO)
               .guard(new GuardContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .target(StatoRichiesta.INCOMPLETO_TECNICO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.COMPLETO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.INOLTRATO))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .action(new FsmActionSendMessageToGenerateFileViaCard(deviceProducerService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ACCETTATO_FORNITORE)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ACCETTATO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .event(RichiestaEvent.MU_RIPETI_INVIO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO_FORNITORE)
               .target(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.IN_SPEDIZIONE))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .guard(new GuardIsToBeSentAtFai(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .guard(new GuardAttesaTransito(manageDeviceSentModeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .event(RichiestaEvent.MU_SPEDIZIONE_FAI)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .guard(new GuardIsToBeSentAtClient(manageDeviceSentModeService))
               .action(new FsmActionNotificationActivatedViaCardAndSpedizioneDispositivo(senderFsmService, mailActivatedDeviceService,
                                                                                         deviceProducerService,
                                                                                         DispositivoEvent.SPEDITO_DAL_FORNITORE))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_SPEDIZIONE_COMPLETA)
               .action(new FsmActionNotificationActivatedViaCardAndSpedizioneDispositivo(senderFsmService, mailActivatedDeviceService,
                                                                                         deviceProducerService,
                                                                                         DispositivoEvent.SPEDITO_DA_FAI))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.CONCLUSO_NON_SPEDITO)
               .event(RichiestaEvent.MU_CHIUSURA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.REVOCA));
  }

}
