package it.fai.ms.efservice.vehicle.ownership.model;

import java.io.Serializable;
import java.util.Objects;

public class VehicleOwnershipServiceTypeId implements Serializable {

  private static final long serialVersionUID = 8785412118071252695L;

  private String id;

  public VehicleOwnershipServiceTypeId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((VehicleOwnershipServiceTypeId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnershipServiceTypeId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
