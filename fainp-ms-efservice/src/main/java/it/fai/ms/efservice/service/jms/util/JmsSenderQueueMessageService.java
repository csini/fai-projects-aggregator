package it.fai.ms.efservice.service.jms.util;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderingMessage;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.efservice.dto.FsmCommandDTO;

@Service
@Transactional
public class JmsSenderQueueMessageService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  public JmsSenderQueueMessageService(final JmsProperties _jmsProperties) {
    jmsProperties = _jmsProperties;
  }

  public void sendAssociationMessage(Serializable dto) {
    publisher(JmsQueueNames.DEVICE_ASSOCIATION, dto);
  }

  public void sendLossMessage(Serializable dto) {
    publisher(JmsQueueNames.DEVICE_LOSS, dto);
  }

  public void sendDeviceReturnMessage(Serializable dto) {
    publisher(JmsQueueNames.DEVICE_RETURN, dto);
  }

  public void sendOrderingMessage(Serializable dto) {
    publisher(JmsQueueNames.DEVICE_ORDERING, dto);
  }

  public void sendTrackyCardOrderingMessage(DeviceOrderingMessage dto) {
    publisher(JmsQueueNames.TRACKYCARD_DEVICE_ORDERING, dto);

  }

  private void publisher(JmsQueueNames queue, Serializable dto) {
    log.info("Send {} message with DTO [{}]: {}", queue.name(), dto.getClass(), dto);
    JmsQueueSenderUtil.publish(jmsProperties, queue, dto);
    log.info("Sended {} message", queue.name());
  }

  public void sendNotificationActivatedViaCardMessage(Serializable dto) {
    log.info("Send VIACARD_PUNTOBLU_ACTIVATED message with DTO [{}]: {}", dto.getClass(), dto);
    JmsQueueNames queue = JmsQueueNames.VIACARD_PUNTOBLU_ACTIVATED;
    JmsQueueSenderUtil.publish(jmsProperties, queue, dto);
    log.info("Sended VIACARD_PUNTOBLU_ACTIVATED message on {}", queue.name());
  }

  public void sendCommandToFsm(String identificativo, String error, String operazione) {
    
    FsmCommandDTO dto = new FsmCommandDTO().identificativo(identificativo)
                                           .errorMessage(error)
                                           .operazione(operazione);
    
    log.info("Send CommandToFSM message with DTO [{}]: {}", dto.getClass(), dto);
    JmsQueueNames queue = JmsQueueNames.FSM_COMMAND_EVENT;
    JmsQueueSenderUtil.publish(jmsProperties, queue, dto);
    log.info("Sended CommandToFSM message on {}", queue.name());

  }

}
