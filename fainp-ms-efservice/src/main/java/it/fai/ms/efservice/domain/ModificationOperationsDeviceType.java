package it.fai.ms.efservice.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Entity
@Table(name = "operazioni_modifica_dispositivo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ModificationOperationsDeviceType {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "tipo_dispositivo")
  @Enumerated(EnumType.STRING)
  private TipoDispositivoEnum deviceType;

  @Column(name = "operations")
  private String operations;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public ModificationOperationsDeviceType deviceType(TipoDispositivoEnum deviceType) {
    setDeviceType(deviceType);
    return this;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public List<String> getOperations() {
    List<String> modificationOperations = new ArrayList<>();
    String[] splittedString = StringUtils.split(this.operations, ",");
    for (String operation : splittedString) {
      modificationOperations.add(operation);
    }
    return modificationOperations;
  }

  public ModificationOperationsDeviceType operations(String... operations) {
    setOperations(operations);
    return this;
  }

  public void setOperations(String... operations) {
    this.operations = String.join(",", operations);
  }

  @Override
  public String toString() {
    return "ModificationOperationsDeviceType [id=" + id + ", deviceType=" + deviceType + ", operations=" + operations + "]";
  }

}
