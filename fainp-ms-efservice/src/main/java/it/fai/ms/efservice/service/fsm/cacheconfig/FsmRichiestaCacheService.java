package it.fai.ms.efservice.service.fsm.cacheconfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.region.Region;
import org.springframework.statemachine.state.AbstractState;
import org.springframework.statemachine.state.HistoryPseudoState;
import org.springframework.statemachine.state.PseudoState;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.AbstractStateMachine;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.FsmCacheRichiestaService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.serializer.FsmRichiestaSerializer;

@Component
public class FsmRichiestaCacheService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private FsmRichiestaSerializer     serializer;
  private final FsmCacheRichiestaService fsmCacheRichiestaService;

  public FsmRichiestaCacheService(FsmRichiestaSerializer _serializer, final FsmCacheRichiestaService _fsmCacheRichiestaService) {
    serializer = _serializer;
    fsmCacheRichiestaService = _fsmCacheRichiestaService;
  }

  @PostConstruct
  public void init() {
    log.debug("Init service Cache Richiesta on DB");
  }

  public void putStateMachineContext(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    fsmCacheRichiestaService.put(Long.valueOf(key), json);
  }

  public String getStateMachineContext(String key) {
    String json = fsmCacheRichiestaService.get(Long.valueOf(key));
    log.debug("Get object from Map with Key {} Value {}", key, json);
    return json;
  }

  public String getIdStateMachine(String key) {
    String jsonSmContext = getStateMachineContext(key);

    return jsonSmContext;
  }

  public void clearCache(String key) {
    log.debug("Remove key {} from DB", key);
    fsmCacheRichiestaService.remove(Long.valueOf(key));
  }

  public void clearCache() {
    log.debug("Clear all from DB");
    fsmCacheRichiestaService.removeAll();
  }

  public void persist(StateMachine<StatoRichiesta, RichiestaEvent> stateMachine, String identifier) {
    StateMachineContext<StatoRichiesta, RichiestaEvent> buildStateMachineContext = buildStateMachineContext(stateMachine);
    String objSerialized = serializer.serialize(buildStateMachineContext);
    putStateMachineContext(identifier, objSerialized);
  }

  public void resetIdMachineFromAnotherIdMachineByContext(String json, String identifier, String idNewMachine) {
    StateMachineContext<StatoRichiesta, RichiestaEvent> smc = serializer.deSerialize(json, idNewMachine);
    String objSerialized = serializer.serialize(smc);
    putStateMachineContext(identifier, objSerialized);
  }

  public final StateMachine<StatoRichiesta, RichiestaEvent> restore(StateMachine<StatoRichiesta, RichiestaEvent> stateMachine,
                                                                    String identifier) {
    String json = getStateMachineContext(identifier);
    if (json == null) {
      return stateMachine;
    }

    StateMachineContext<StatoRichiesta, RichiestaEvent> smc = serializer.deSerialize(json);

    if (smc == null) {
      return stateMachine;
    }

    final StateMachineContext<StatoRichiesta, RichiestaEvent> context = smc;

    log.info("State Machine... {}", stateMachine);
    stateMachine.stop();

    stateMachine.getStateMachineAccessor()
                .doWithAllRegions(access -> {
                  access.resetStateMachine(context);
                });

    stateMachine.start();
    return stateMachine;
  }

  protected StateMachineContext<StatoRichiesta, RichiestaEvent> buildStateMachineContext(StateMachine<StatoRichiesta, RichiestaEvent> stateMachine) {
    ExtendedState extendedState = new DefaultExtendedState();
    extendedState.getVariables()
                 .putAll(stateMachine.getExtendedState()
                                     .getVariables());

    ArrayList<StateMachineContext<StatoRichiesta, RichiestaEvent>> childs = new ArrayList<>();
    StatoRichiesta id = null;
    State<StatoRichiesta, RichiestaEvent> state = stateMachine.getState();
    if (state.isSubmachineState()) {
      id = getDeepState(state);
    } else if (state.isOrthogonal()) {
      Collection<Region<StatoRichiesta, RichiestaEvent>> regions = ((AbstractState<StatoRichiesta, RichiestaEvent>) state).getRegions();
      for (Region<StatoRichiesta, RichiestaEvent> r : regions) {
        StateMachine<StatoRichiesta, RichiestaEvent> rsm = (StateMachine<StatoRichiesta, RichiestaEvent>) r;
        childs.add(buildStateMachineContext(rsm));
      }
      id = state.getId();
    } else {
      id = state.getId();
    }

    // building history state mappings
    Map<StatoRichiesta, StatoRichiesta> historyStates = new HashMap<>();
    PseudoState<StatoRichiesta, RichiestaEvent> historyState = ((AbstractStateMachine<StatoRichiesta, RichiestaEvent>) stateMachine).getHistoryState();
    if (historyState != null) {
      historyStates.put(null, ((HistoryPseudoState<StatoRichiesta, RichiestaEvent>) historyState).getState()
                                                                                                 .getId());
    }
    Collection<State<StatoRichiesta, RichiestaEvent>> states = stateMachine.getStates();
    for (State<StatoRichiesta, RichiestaEvent> ss : states) {
      if (ss.isSubmachineState()) {
        StateMachine<StatoRichiesta, RichiestaEvent> submachine = ((AbstractState<StatoRichiesta, RichiestaEvent>) ss).getSubmachine();
        PseudoState<StatoRichiesta, RichiestaEvent> ps = ((AbstractStateMachine<StatoRichiesta, RichiestaEvent>) submachine).getHistoryState();
        if (ps != null) {
          State<StatoRichiesta, RichiestaEvent> pss = ((HistoryPseudoState<StatoRichiesta, RichiestaEvent>) ps).getState();
          if (pss != null) {
            historyStates.put(ss.getId(), pss.getId());
          }
        }
      }
    }
    return new DefaultStateMachineContext<>(childs, id, null, null, extendedState, historyStates, stateMachine.getId());
  }

  private StatoRichiesta getDeepState(State<StatoRichiesta, RichiestaEvent> state) {
    Collection<StatoRichiesta> ids1 = state.getIds();
    StatoRichiesta[] ids2 = (StatoRichiesta[]) ids1.toArray();
    // TODO: can this be empty as then we'd get error?
    return ids2[ids2.length - 1];
  }

  public String resetStateAndMachineId(String identifierCache, StatoRichiesta stato, String nameFsm) {
    String oldContextFsm = getStateMachineContext(identifierCache);
    StateMachineContext<StatoRichiesta, RichiestaEvent> stateMachine = serializer.deSerialize(oldContextFsm, null);
    StatoRichiesta actualState = stateMachine.getState();
    String newContextFsm = oldContextFsm.replace(actualState.name(), stato.name());
    if (StringUtils.isNotBlank(nameFsm)) {
      String idActualStateMachine = stateMachine.getId();
      newContextFsm = newContextFsm.replace(idActualStateMachine, nameFsm);
    }
    log.info("OldContext FSM: {} - NewContext FSM: {}", oldContextFsm, newContextFsm);
    putStateMachineContext(identifierCache, newContextFsm);
    return newContextFsm;
  }

  public String resetStateFsm(String identifierCache, StatoRichiesta stato) {
    return resetStateAndMachineId(identifierCache, stato, null);
  }

}
