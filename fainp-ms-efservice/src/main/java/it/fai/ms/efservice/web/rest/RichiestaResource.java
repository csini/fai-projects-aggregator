package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Richiesta.
 */
@RestController
@RequestMapping("/api")
public class RichiestaResource {

    private final Logger log = LoggerFactory.getLogger(RichiestaResource.class);

    private static final String ENTITY_NAME = "richiesta";

    private final RichiestaService richiestaService;

    public RichiestaResource(RichiestaService richiestaService) {
        this.richiestaService = richiestaService;
    }

    /**
     * POST  /richiestas : Create a new richiesta.
     *
     * @param richiestaDTO the richiestaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new richiestaDTO, or with status 400 (Bad Request) if the richiesta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/richiestas")
    @Timed
    public ResponseEntity<RichiestaDTO> createRichiesta(@Valid @RequestBody RichiestaDTO richiestaDTO) throws URISyntaxException {
        log.debug("REST request to save Richiesta : {}", richiestaDTO);
        if (richiestaDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new richiesta cannot already have an ID")).body(null);
        }
        RichiestaDTO result = richiestaService.save(richiestaDTO);
        return ResponseEntity.created(new URI("/api/richiestas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /richiestas : Updates an existing richiesta.
     *
     * @param richiestaDTO the richiestaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated richiestaDTO,
     * or with status 400 (Bad Request) if the richiestaDTO is not valid,
     * or with status 500 (Internal Server Error) if the richiestaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/richiestas")
    @Timed
    public ResponseEntity<RichiestaDTO> updateRichiesta(@Valid @RequestBody RichiestaDTO richiestaDTO) throws URISyntaxException {
        log.debug("REST request to update Richiesta : {}", richiestaDTO);
        if (richiestaDTO.getId() == null) {
            return createRichiesta(richiestaDTO);
        }
        RichiestaDTO result = richiestaService.save(richiestaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, richiestaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /richiestas : get all the richiestas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of richiestas in body
     */
    @GetMapping("/richiestas")
    @Timed
    public List<RichiestaDTO> getAllRichiestas() {
        log.debug("REST request to get all Richiestas");
        return richiestaService.findAll();
        }

    /**
     * GET  /richiestas/:id : get the "id" richiesta.
     *
     * @param id the id of the richiestaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the richiestaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/richiestas/{id}")
    @Timed
    public ResponseEntity<RichiestaDTO> getRichiesta(@PathVariable Long id) {
        log.debug("REST request to get Richiesta : {}", id);
        RichiestaDTO richiestaDTO = richiestaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(richiestaDTO));
    }

    /**
     * DELETE  /richiestas/:id : delete the "id" richiesta.
     *
     * @param id the id of the richiestaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/richiestas/{id}")
    @Timed
    public ResponseEntity<Void> deleteRichiesta(@PathVariable Long id) {
        log.debug("REST request to delete Richiesta : {}", id);
        richiestaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
