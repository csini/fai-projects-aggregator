package it.fai.ms.efservice.wizard.repository;

import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDevice;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

public interface WizardDeviceRepository {

  Set<WizardDevice> findByWizardVehicleId(WizardVehicleId wizardVehicleId);

  Set<WizardDevice> findByWizardVehicleIdFilteredByDevice(WizardVehicleId _wizardVehicleId, Set<WizardDeviceTypeId> deviceTypeIds);

}
