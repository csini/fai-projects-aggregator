package it.fai.ms.efservice.service;

public class OrdineClienteNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 7766879935482422727L;

  public OrdineClienteNotFoundException() {
  }

  public OrdineClienteNotFoundException(final String _message) {
    super(_message);
  }

  public OrdineClienteNotFoundException(final Throwable _cause) {
    super(_cause);
  }

  public OrdineClienteNotFoundException(final String _message, final Throwable _cause) {
    super(_message, _cause);
  }

}
