package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.efservice.domain.StatoContratto;

/**
 * A DTO for the Contratto entity.
 */
public class ContrattoDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long id;

  private String codContrattoCliente;

  private Long produttoreId;

  private String produttoreNome;

  private Long clienteFaiId;

  private String clienteFaiRagioneSociale;

  private String operazioniPossibili;

  private StatoContratto stato;

  private Instant dataModificaStato;

  private String paeseRiferimentoIva;
  
  private Boolean primario;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodContrattoCliente() {
    return codContrattoCliente;
  }

  public void setCodContrattoCliente(String codContrattoCliente) {
    this.codContrattoCliente = codContrattoCliente;
  }

  public Long getProduttoreId() {
    return produttoreId;
  }

  public void setProduttoreId(Long produttoreId) {
    this.produttoreId = produttoreId;
  }

  public String getProduttoreNome() {
    return produttoreNome;
  }

  public void setProduttoreNome(String produttoreNome) {
    this.produttoreNome = produttoreNome;
  }

  public Long getClienteFaiId() {
    return clienteFaiId;
  }

  public void setClienteFaiId(Long clienteFaiId) {
    this.clienteFaiId = clienteFaiId;
  }

  public String getClienteFaiRagioneSociale() {
    return clienteFaiRagioneSociale;
  }

  public void setClienteFaiRagioneSociale(String clienteFaiRagioneSociale) {
    this.clienteFaiRagioneSociale = clienteFaiRagioneSociale;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public StatoContratto getStato() {
    return stato;
  }

  public void setStato(StatoContratto stato) {
    this.stato = stato;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public String getPaeseRiferimentoIva() {
    return paeseRiferimentoIva;
  }

  public void setPaeseRiferimentoIva(String paeseRiferimentoIva) {
    this.paeseRiferimentoIva = paeseRiferimentoIva;
  }

  @Deprecated
  public String getNazioneFatturazione() {
    return paeseRiferimentoIva;
  }

  @Deprecated
  public void setNazioneFatturazione(String nazioneFatturazione) {
    this.paeseRiferimentoIva = nazioneFatturazione;
  }

  public Boolean isPrimario() {
    return primario;
  }

  public void setPrimario(Boolean primario) {
    this.primario = primario;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ContrattoDTO contrattoDTO = (ContrattoDTO) o;
    if (contrattoDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), contrattoDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("ContrattoDTO [id=")
        .append(id)
        .append(", codContrattoCliente=")
        .append(codContrattoCliente)
        .append(", produttoreId=")
        .append(produttoreId)
        .append(", produttoreNome=")
        .append(produttoreNome)
        .append(", clienteFaiId=")
        .append(clienteFaiId)
        .append(", clienteFaiRagioneSociale=")
        .append(clienteFaiRagioneSociale)
        .append(", operazioniPossibili=")
        .append(operazioniPossibili)
        .append(", stato=")
        .append(stato)
        .append(", dataModificaStato=")
        .append(dataModificaStato)
        .append(", paeseRiferimentoIva=")
        .append(paeseRiferimentoIva)
        .append(", primario=")
        .append(primario)
        .append("]");
    return builder.toString();
  }

}
