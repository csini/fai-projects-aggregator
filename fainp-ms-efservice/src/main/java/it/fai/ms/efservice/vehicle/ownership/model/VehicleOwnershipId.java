package it.fai.ms.efservice.vehicle.ownership.model;

import java.io.Serializable;
import java.util.Objects;

public class VehicleOwnershipId implements Serializable {

  private static final long serialVersionUID = -1776181766113729840L;

  private String id;

  public VehicleOwnershipId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((VehicleOwnershipId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnershipId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
