package it.fai.ms.efservice.service.fsm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;

@Service
public class FsmFactory {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private FsmRichiestaMappingService fsmMap;

  public AbstractFsmRichiesta getFsm(FsmCommand command) {
    AbstractFsmRichiesta fsm = null;

    if (command == null) {
      log.warn("Command to get FSM from map is null");
      return null;
    }

    log.info("Retrieve FSM by command {}", command);
    switch (command) {
      case CMD_IN_ACCETTAZIONE:
        fsm = fsmMap.getFSM(FsmType.ACCETTAZIONE.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_TE:
      case CMD_ACCETTATO_PER_INOLTRO_TE_SAT:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TE.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_TSAT:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TSAT.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_TI:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TI.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_VC:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_VIACARD.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_TRACKY:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TRACKYCARD.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_FREJUS:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_FEJUS.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_DARTFORD_CROSSING:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_DARTFORD_CROSSING.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_GRAN_SAN_BERNARDO:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_GRAN_SAN_BERNARDO.fsmName());
        break;
      case CMD_FURTO_NO_SOST_GRAN_SAN_BERNARDO:
      case CMD_SMARRIMENTO_NO_SOST_GRAN_SAN_BERNARDO:
      case CMD_FURTO_SOST_GRAN_SAN_BERNARDO:
      case CMD_SMARRIMENTO_SOST_GRAN_SAN_BERNARDO:
        fsm = fsmMap.getFSM(FsmType.MOD_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_SOST_GRAN_SANBERNARDO:
      case CMD_MALFUNZIONAMENTO_NO_SOST_GRAN_SANBERNARDO:
      case CMD_MEZZO_RITARGATO_GRAN_SANBERNARDO:
      case CMD_VARIAZIONE_TARGA_GRAN_SANBERNARDO:
        fsm = fsmMap.getFSM(FsmType.MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_GOBOX:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_GOBOX.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_TOLLCOLLECT:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TOLL_COLLECT.fsmName());
        break;
      case CMD_ATTIVAZIONE_SERVIZIO_TOLLCOLLECT:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_TOLL2GO.fsmName());
        break;
      case CMD_FURTO_NO_SOST_TE:
      case CMD_SMARRIMENTO_NO_SOST_TE:
      case CMD_FURTO_SOST_TE:
      case CMD_SMARRIMENTO_SOST_TE:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_FURTO_SMARRIMENTO.fsmName());
        break;
      case CMD_FURTO_NO_SOST_TE_SAT:
      case CMD_SMARRIMENTO_NO_SOST_TE_SAT:
      case CMD_FURTO_SOST_TE_SAT:
      case CMD_SMARRIMENTO_SOST_TE_SAT:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_SAT_FURTO_SMARRIMENTO.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_SOST_TE:
      case CMD_MEZZO_RITARGATO_TE:
      case CMD_VARIAZIONE_TARGA_TE:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_VARIAZIONE_TARGA.fsmName());
        break;
      case CMD_MEZZO_RITARGATO_TE_SAT:
      case CMD_VARIAZIONE_TARGA_TE_SAT:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_SAT_VARIAZIONE_TARGA.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_NO_SOST_TE:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_RIENTRO_MALFUNZIONAMENTO.fsmName());
        break;

      case CMD_MALFUNZIONAMENTO_NO_SOST_TE_SAT:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_SAT_RIENTRO_MALFUNZIONAMENTO.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_SOST_TE_SAT:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_SAT_MALFUNZIONAMENTO_SOST.fsmName());
        break;
      case CMD_RESC_RIATT_SOSPENSIONE_TE:
        fsm = fsmMap.getFSM(FsmType.MOD_TE_RESC_RIATT_SOSPENSIONE.fsmName());
        break;

      // TELEPASS ITA
      case CMD_FURTO_NO_SOST_TI:
      case CMD_FURTO_SOST_TI:
      case CMD_SMARRIMENTO_NO_SOST_TI:
      case CMD_SMARRIMENTO_SOST_TI:
        fsm = fsmMap.getFSM(FsmType.MOD_TI_FURTO_SMARRIMENTO.fsmName());
        break;

      case CMD_VARIAZIONE_TARGA_TI:
      case CMD_MEZZO_RITARGATO_TI:
      case CMD_MALFUNZIONAMENTO_SOST_TI:
        fsm = fsmMap.getFSM(FsmType.MOD_TI_VARIAZIONE_TARGA.fsmName());
        break;

      case CMD_MALFUNZIONAMENTO_NO_SOST_TI:
        fsm = fsmMap.getFSM(FsmType.MOD_TI_RIENTRO_MALFUNZIONAMENTO.fsmName());
        break;

      // VIACARD
      case CMD_FURTO_NO_SOST_VC:
      case CMD_FURTO_SOST_VC:
      case CMD_SMARRIMENTO_NO_SOST_VC:
      case CMD_SMARRIMENTO_SOST_VC:
        fsm = fsmMap.getFSM(FsmType.MOD_VC_FURTO_SMARRIMENTO.fsmName());
        break;

      case CMD_VARIAZIONE_TARGA_VC:
      case CMD_MEZZO_RITARGATO_VC:
      case CMD_MALFUNZIONAMENTO_SOST_VC:
        fsm = fsmMap.getFSM(FsmType.MOD_VC_VARIAZIONE_TARGA.fsmName());
        break;

      case CMD_MALFUNZIONAMENTO_NO_SOST_VC:
        fsm = fsmMap.getFSM(FsmType.MOD_VC_RIENTRO_MALFUNZIONAMENTO.fsmName());
        break;

      // TRACKYCARD
      case CMD_FURTO_NO_SOST_TRACKY:
      case CMD_FURTO_SOST_TRACKY:
      case CMD_SMARRIMENTO_NO_SOST_TRACKY:
      case CMD_SMARRIMENTO_SOST_TRACKY:
        fsm = fsmMap.getFSM(FsmType.MOD_TRACKY_FURTO_SMARRIMENTO.fsmName());
        break;

      case CMD_VARIAZIONE_TARGA_TRACKY:
      case CMD_MEZZO_RITARGATO_TRACKY:
      case CMD_MALFUNZIONAMENTO_SOST_TRACKY:
        fsm = fsmMap.getFSM(FsmType.MOD_TRACKY_VARIAZIONE_TARGA.fsmName());
        break;

      case CMD_MALFUNZIONAMENTO_NO_SOST_TRACKY:
        fsm = fsmMap.getFSM(FsmType.MOD_TRACKY_RIENTRO_MALFUNZIONAMENTO.fsmName());
        break;
      case CMD_MODIFICA_FREJUS:
        fsm = fsmMap.getFSM(FsmType.MODIFICA_FREJUS.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_FREJUS:
        fsm = fsmMap.getFSM(FsmType.MALFUNZIONAMENTO_FREJUS.fsmName());
        break;
      case CMD_MODIFICA_BO_BOX:
        fsm = fsmMap.getFSM(FsmType.MODIFICA_GOBOX.fsmName());
        break;
      case CMD_MODIFICA_TOLLCOLLECT:
        fsm = fsmMap.getFSM(FsmType.MODIFICA_TOLL_COLLECT.fsmName());
        break;

      case CMD_MODIFICA_TESSERA_CARONTE:
        fsm = fsmMap.getFSM(FsmType.MODIFICA_TESSERA_CARONTE.fsmName());
        break;

      case CMD_VARIAZIONE_TARGA_DARTFORD_CROSSING:
        fsm = fsmMap.getFSM(FsmType.MOD_DARTFORD_CROSSING_VARAZIONE_TARGA.fsmName());
        break;

      case CMD_ACCETTATO_PER_INOLTRO_HGV:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_HGV.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_LIBER_T:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_LIBER_T.fsmName());
        break;
      case CMD_FURTO_NO_SOST_LIBER_T:
      case CMD_SMARRIMENTO_NO_SOST_LIBER_T:
      case CMD_FURTO_SOST_LIBER_T:
      case CMD_SMARRIMENTO_SOST_LIBER_T:
        fsm = fsmMap.getFSM(FsmType.MOD_LIBER_T_FURTO_SMARRIMENTO.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_SOST_LIBER_T:
      case CMD_MALFUNZIONAMENTO_NO_SOST_LIBER_T:
      case CMD_MEZZO_RITARGATO_LIBER_T:
      case CMD_VARIAZIONE_TARGA_LIBER_T:
        fsm = fsmMap.getFSM(FsmType.MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO.fsmName());
        break;
      case CMD_RIENTRO_LIBER_T:
        fsm = fsmMap.getFSM(FsmType.MOD_LIBER_T_RIENTRO.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_VIA_TOLL:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_VIATOLL.fsmName());
        break;
      case CMD_ACCETTATO_PER_INOLTRO_VISPRO:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_VISPRO.fsmName());
        break;

      // VIA TOLL
      case CMD_FURTO_NO_SOST_VIATOLL:
      case CMD_FURTO_SOST_VIATOLL:
      case CMD_SMARRIMENTO_NO_SOST_VIATOLL:
      case CMD_SMARRIMENTO_SOST_VIATOLL:
        fsm = fsmMap.getFSM(FsmType.MOD_VIATOLL_FURTO_SMARRIMENTO.fsmName());
        break;
      case CMD_MALFUNZIONAMENTO_SOST_VIATOLL:
      case CMD_MALFUNZIONAMENTO_NO_SOST_VIATOLL:
      case CMD_MEZZO_RITARGATO_VIATOLL:
      case CMD_VARIAZIONE_TARGA_VIATOLL:
        fsm = fsmMap.getFSM(FsmType.MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO.fsmName());
        break;

      // assistenza veicoli
      case CMD_ACCETTATO_PER_INOLTRO_ASSISTENZA_VEICOLI:
        fsm = fsmMap.getFSM(FsmType.INOLTRO_ASSISTENZA_VEICOLI.fsmName());
        break;
      case CMD_VARIAZIONE_TARGA_ASSISTENZA_VEICOLI:
        fsm = fsmMap.getFSM(FsmType.MOD_ASSISTENZA_VEICOLI_VARIAZIONE_TARGA.fsmName());
        break;

      case CMD_MALFUNZIONAMENTO_NO_SOST_LOCALIZZATORE_SAT:
        fsm = fsmMap.getFSM(FsmType.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO.fsmName());
        break;

      default:
        log.error("Not found mapped COMMAND {} to retrieve FSM", command);
        break;
    }

    log.debug("Retrieve Abstract FSM {}", (fsm != null) ? fsm.getStateMachine().getId() : null);
    return fsm;
  }

}
