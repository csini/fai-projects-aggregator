/**
 * 
 */
package it.fai.ms.efservice.service.fsm.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.monitor.AbstractStateMachineMonitor;
import org.springframework.statemachine.transition.Transition;

/**
 * @author Luca Vassallo
 */
public class StateMachineMonitor<S, E> extends AbstractStateMachineMonitor<S, E> {

  private final static Logger log = LoggerFactory.getLogger(StateMachineMonitor.class);

  /*
   * (non-Javadoc)
   * @see
   * org.springframework.statemachine.monitor.AbstractStateMachineMonitor#transition(org.springframework.statemachine.
   * StateMachine, org.springframework.statemachine.transition.Transition, long)
   */
  @Override
  public void transition(StateMachine<S, E> stateMachine, Transition<S, E> transition, long duration) {
    super.transition(stateMachine, transition, duration);
    if (log.isDebugEnabled()) {
      log.debug("Transition: " + transition + " duration: " + duration);
    }
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.monitor.AbstractStateMachineMonitor#action(org.springframework.statemachine.
   * StateMachine, org.springframework.statemachine.action.Action, long)
   */
  @Override
  public void action(StateMachine<S, E> stateMachine, Action<S, E> action, long duration) {
    super.action(stateMachine, action, duration);
    if (log.isDebugEnabled()) {
      log.debug("Action: " + action + " duration: " + duration);
    }
  }

}
