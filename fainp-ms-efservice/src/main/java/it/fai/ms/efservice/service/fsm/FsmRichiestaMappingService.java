package it.fai.ms.efservice.service.fsm;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FsmRichiestaMappingService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private Map<String, AbstractFsmRichiesta> mapStateMachine;

  public AbstractFsmRichiesta getFSM(String machineId) {
    log.info("Getting state machine from map with key: {}", machineId);
    AbstractFsmRichiesta abstractFsmRichiesta = mapStateMachine.get(machineId);
    return abstractFsmRichiesta;
  }

  public int countStateMachine() {
    int sizeSetStateMachine = mapStateMachine.keySet()
                                             .size();
    log.debug("Numero di macchine a stati in mappa: {}", sizeSetStateMachine);
    return sizeSetStateMachine;
  }

}
