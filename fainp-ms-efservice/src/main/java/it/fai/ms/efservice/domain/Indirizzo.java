package it.fai.ms.efservice.domain;


import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Indirizzo.
 */
@Entity
@Table(name = "indirizzo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Indirizzo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "via")
    private String via;

    @Column(name = "citta")
    private String citta;

    @Column(name = "cap")
    private String cap;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "paese")
    private String paese;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVia() {
        return via;
    }

    public Indirizzo via(String via) {
        this.via = via;
        return this;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getCitta() {
        return citta;
    }

    public Indirizzo citta(String citta) {
        this.citta = citta;
        return this;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCap() {
        return cap;
    }

    public Indirizzo cap(String cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getProvincia() {
        return provincia;
    }

    public Indirizzo provincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPaese() {
        return paese;
    }

    public Indirizzo paese(String paese) {
        this.paese = paese;
        return this;
    }

    public void setPaese(String paese) {
        this.paese = paese;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Indirizzo indirizzo = (Indirizzo) o;
        if (indirizzo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), indirizzo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Indirizzo{" +
            "id=" + getId() +
            ", via='" + getVia() + "'" +
            ", citta='" + getCitta() + "'" +
            ", cap='" + getCap() + "'" +
            ", provincia='" + getProvincia() + "'" +
            ", paese='" + getPaese() + "'" +
            "}";
    }
}
