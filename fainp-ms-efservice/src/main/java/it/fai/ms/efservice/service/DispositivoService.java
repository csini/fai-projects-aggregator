package it.fai.ms.efservice.service;

import java.util.List;

import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.service.dto.DispositivoDTO;

/**
 * Service Interface for managing Dispositivo.
 */
public interface DispositivoService {

    /**
     * Save a dispositivo.
     *
     * @param dispositivoDTO the entity to save
     * @return the persisted entity
     */
    DispositivoDTO save(DispositivoDTO dispositivoDTO);

    /**
     *  Get all the dispositivos.
     *
     *  @return the list of entities
     */
    List<DispositivoDTO> findAll();
    
    /**
     *  Get the "id" dispositivo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DispositivoDTO findOne(Long id);

    /**
     *  Delete the "id" dispositivo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
    
    /**
     *  Get all the DispositivoDTO by Ordine identificativo .
     *
     *  @return the list of entities
     */
    List<DispositivoDTO> findAllByOrdine(String identificativo);

    /**
     *  Get all the DispositivoDTO by codice azienda and tipo magazzino .
     *
     *  @return the list of entities
     */
    List<DispositivoDTO> findAllByCodiceAziendaAndTipoMagazzino(String codiceAzienda, TipoMagazzino tipoMagazzino);   
    
}
