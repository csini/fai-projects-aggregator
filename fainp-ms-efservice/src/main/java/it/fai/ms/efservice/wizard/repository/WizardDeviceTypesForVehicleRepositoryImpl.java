package it.fai.ms.efservice.wizard.repository;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

@Service
public class WizardDeviceTypesForVehicleRepositoryImpl implements WizardDeviceTypesForVehicleRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private AssociazioneDVRepository devicesForVehicleRepository;

  @Autowired
  public WizardDeviceTypesForVehicleRepositoryImpl(final AssociazioneDVRepository _devicesForVehicleRepository) {
    devicesForVehicleRepository = _devicesForVehicleRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDeviceType> findByWizardVehicleId(final WizardVehicleId _wizardVehicleId) {
    final List<AssociazioneDV> byUuidVeicolo = devicesForVehicleRepository.findByUuidVeicolo(_wizardVehicleId.getId());
    final Set<WizardDeviceType> wizardDeviceTypes = byUuidVeicolo.stream()
                                                                 .map(device -> mapToWizardDeviceType(device))
                                                                 .collect(toSet());
    _log.info("Found filtered by {}: {}", _wizardVehicleId, wizardDeviceTypes);
    return wizardDeviceTypes;
  }

  private WizardDeviceType mapToWizardDeviceType(final AssociazioneDV _device) {
    final WizardDeviceType wizardDeviceType = new WizardDeviceType(new WizardDeviceTypeId(_device.getDispositivo()
                                                                                                 .getTipoDispositivo()
                                                                                                 .getNome()
                                                                                                 .name()));
    return wizardDeviceType;
  }

}
