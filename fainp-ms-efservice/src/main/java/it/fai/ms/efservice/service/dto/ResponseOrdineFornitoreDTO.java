package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import it.fai.ms.efservice.enumeration.StateCreationRichiesta;

/**
 * ResponseOrdineFornitoreDTO
 */
public class ResponseOrdineFornitoreDTO implements Serializable {

  private static final long serialVersionUID = 8546642059157872202L;

  private StateCreationRichiesta state;

  private HashMap<String, List<AnomaliaOrdineFornitoreDTO>> warnings;

  public ResponseOrdineFornitoreDTO(StateCreationRichiesta _state) {
    state = _state;
  }

  public StateCreationRichiesta getState() {
    return state;
  }

  public ResponseOrdineFornitoreDTO state(StateCreationRichiesta _state) {
    state = _state;
    return this;
  }

  public void setState(StateCreationRichiesta state) {
    this.state = state;
  }

  public HashMap<String, List<AnomaliaOrdineFornitoreDTO>> getWarnings() {
    return warnings;
  }

  public void setWarnings(HashMap<String, List<AnomaliaOrdineFornitoreDTO>> warnings) {
    this.warnings = warnings;
  }

  @Override
  public String toString() {
    return "ResponseOrdineFornitoreDTO [state=" + state + ", warnings=" + warnings + "]";
  }

}
