package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.service.ManageStatoOrdineClienteService;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(ManageStatoOrdineResourceExt.BASE_PATH)
public class ManageStatoOrdineResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  public static final String MANAGE_STATO_ORDINE = "/managestatoordine";

  public static final String PUT_ORDINI_IN_CACHE = "/putallordineincache";

  public static final String EXPIRE_ORDINI_IN_CACHE = "/expireordineincache";

  private final ManageStatoOrdineClienteService manageStatoOrdineService;

  private final OrdineClienteServiceExt ordineClienteService;

  public ManageStatoOrdineResourceExt(ManageStatoOrdineClienteService _manageStatoOrdineService,
                                      OrdineClienteServiceExt _ordineClienteService) {
    manageStatoOrdineService = _manageStatoOrdineService;
    ordineClienteService = _ordineClienteService;
  }

  @PostMapping(MANAGE_STATO_ORDINE + "/{identificativo}")
  @Timed
  public ResponseEntity<Void> changeStatusOrdineCliente(@PathVariable(required = true) String identificativo) throws Exception {
    log.debug("Post request to change status for Ordine Cliente : [Identificativo: {}]", identificativo);

    if (StringUtils.isBlank(identificativo)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.IDENTIFICATIVO_IS_NULL);
    }
    OrdineCliente ordineCliente = ordineClienteService.findByIdentificativoOrdineCliente(identificativo);
    if (ordineCliente == null) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.NOT_FOUND_ORDINE_CLIENTE);
    }

    Long idOrdineCliente = ordineCliente.getId();
    manageStatoOrdineService.manageOrdineAndRemoveFromCache(String.valueOf(idOrdineCliente), identificativo);

    return ResponseEntity.ok()
                         .build();
  }

  @PostMapping(MANAGE_STATO_ORDINE)
  @Timed
  public ResponseEntity<Void> changeStatusOrdiniCliente() throws Exception {
    log.debug("Post request to change status for Ordine Cliente");

    manageStatoOrdineService.manageOrdiniInCache();

    return ResponseEntity.ok()
                         .build();
  }

  @PostMapping(PUT_ORDINI_IN_CACHE)
  @Timed
  public ResponseEntity<Void> forcePutAllOrdineClienteInCache() throws Exception {
    log.debug("Put all Ordine in cache...");

    List<OrdineClienteCacheDTO> ordiniCliente = ordineClienteService.findByStatoNotIs(StatoOrdineCliente.COMPLETATO);
    if (ordiniCliente == null) {
      log.warn("Not found ALL OrdineCliente");
    } else {
      log.info("Put {} Ordine Cliente", ordiniCliente.size());
      manageStatoOrdineService.putOrdiniNotCompletedInCache(ordiniCliente);
    }

    return ResponseEntity.ok()
                         .build();
  }

  @PostMapping(EXPIRE_ORDINI_IN_CACHE + "/{identificativo}")
  @Timed
  public ResponseEntity<Void> expireOrdineClienteInCache(@PathVariable(required = true) String identificativo) throws Exception {
    log.debug("Expire Ordine in cache by identificativo {}", identificativo);

    if (StringUtils.isBlank(identificativo)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.IDENTIFICATIVO_IS_NULL);
    }

    boolean expired = true;
    OrdineCliente ordineCliente = ordineClienteService.findByIdentificativoOrdineCliente(identificativo);
    if (ordineCliente == null) {
      log.warn("Not found OrdineCliente {}", identificativo);
    } else {
      log.info("Expire Ordine Cliente from cache....");
      expired = manageStatoOrdineService.expireOrdineInCache(ordineCliente.getId());
      log.info("Expire {} Ordine Cliente {} from cache", expired, identificativo);
    }

    if (!expired) {
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                           .build();
    }
    return ResponseEntity.ok()
                         .build();
  }

  @PostMapping(EXPIRE_ORDINI_IN_CACHE)
  @Timed
  public ResponseEntity<Void> expireOrdiniClienteInCache() throws Exception {
    log.debug("Expire All Ordini Cliente in cache...");

    Set<String> allKeyInCache = manageStatoOrdineService.getAllKeyInCache();
    log.info("Keys in cache to expire: {}", allKeyInCache);

    Set<Boolean> removedItems = allKeyInCache.parallelStream()
                                             .map(k -> {
                                               log.info("expire key: {}", k);
                                               boolean expired = manageStatoOrdineService.expireOrdineInCache(Long.valueOf(k));
                                               log.info("Expired {} key {}", expired, k);
                                               return expired;
                                             })
                                             .collect(toSet());

    Optional<Boolean> falseItem = removedItems.stream()
                                              .filter(b -> b == false)
                                              .findFirst();
    if (falseItem.isPresent()) {
      log.warn("Not all item in cache were removed");
    }

    return ResponseEntity.ok()
                         .build();
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
