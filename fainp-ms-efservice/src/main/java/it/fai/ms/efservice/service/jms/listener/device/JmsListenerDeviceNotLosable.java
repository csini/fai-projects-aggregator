package it.fai.ms.efservice.service.jms.listener.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceNotLosableConsumer;

@Service
@Transactional
public class JmsListenerDeviceNotLosable
  extends JmsObjectMessageListenerTemplate<DeviceEventMessage>
  implements JmsQueueListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceNotLosableConsumer consumer;

  @Autowired
  public JmsListenerDeviceNotLosable(DeviceNotLosableConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  protected void consumeMessage(DeviceEventMessage eventMessage) {
    _log.info("Received jms message {}", eventMessage);
    consumer.consume(eventMessage);
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_NOT_LOSABLE;
  }

}
