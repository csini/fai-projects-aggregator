/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmSwitchStateException;
import it.fai.ms.efservice.service.fsm.util.FsmDispositivoUtilService;

/**
 * @author Luca Vassallo
 */
public class AbstractFsmDispositivoTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private FsmDispositivoUtilService fsmServiceUtil;

  @OnTransition
  public void anyTransition(StateContext<StatoDispositivo, DispositivoEvent> stateContext) throws FsmSwitchStateException {
    State<StatoDispositivo, DispositivoEvent> source = stateContext.getSource();
    if (source != null) {
      Transition<StatoDispositivo, DispositivoEvent> transition = stateContext.getTransition();
      State<StatoDispositivo, DispositivoEvent> target = stateContext.getTarget();
      StatoDispositivo toState = target.getId();
      Message<DispositivoEvent> message = stateContext.getMessage();

      logTransitionFromTo(source, target);
      fsmServiceUtil.switchToState(message, toState, transition);
    }
  }

  public void logTransitionFromTo(State<StatoDispositivo, DispositivoEvent> source, State<StatoDispositivo, DispositivoEvent> target) {
      log.info("Transition From [" + ((source != null) ? source.getId() : null) + "] To [" + ((target != null) ? target.getId() : null)
               + "].");
  }

}
