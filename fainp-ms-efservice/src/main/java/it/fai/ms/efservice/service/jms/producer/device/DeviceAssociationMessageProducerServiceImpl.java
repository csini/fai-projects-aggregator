package it.fai.ms.efservice.service.jms.producer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessageFactory;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional
public class DeviceAssociationMessageProducerServiceImpl
  implements DeviceAssociationMessageProducerService {

  private final Logger                       log = LoggerFactory.getLogger(getClass());
  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public DeviceAssociationMessageProducerServiceImpl(final JmsSenderQueueMessageService _jmsSenderQueueService) {
    jmsSenderQueueService = _jmsSenderQueueService;
  }

  @Override
  public void associationDeviceTelepassIta(final Richiesta richiesta) {
    log.debug("Association TELEPASS ITA...");
    DeviceEventMessage eventMessage = buildDeviceEventMessage(richiesta);
    jmsSenderQueueService.sendAssociationMessage(eventMessage);
  }

  private DeviceEventMessage buildDeviceEventMessage(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    OrderRequestDecorator orderRequest = new OrderRequestDecorator(richiesta);
    DeviceEventMessage deviceEventMessage = DeviceEventMessageFactory
      .createDeviceAssociationMessage(
        new DeviceDataDTO()
          .contractCode(orderRequest.findNumeroContract())
          .requestId(orderRequest.getIdentificativo())
          .deviceCode(orderRequest.getDeviceSerialId())
          .deviceType(orderRequestUtil.getObuType().getId().name())
          .licensePlateCode(orderRequest.getLicensePlate())
          .licensePlateCountry(orderRequest.getCountry())
      );
    return deviceEventMessage;
  }
}
