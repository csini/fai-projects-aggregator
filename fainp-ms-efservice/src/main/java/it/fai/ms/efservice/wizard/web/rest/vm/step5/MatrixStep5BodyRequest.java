package it.fai.ms.efservice.wizard.web.rest.vm.step5;

import java.io.Serializable;
import java.util.Set;

public class MatrixStep5BodyRequest implements Serializable, Comparable<MatrixStep5BodyRequest> {

  private static final long serialVersionUID = -4931486511383516054L;

  private Set<String> deviceTypeIds;
  private String      vehicleUUID;

  public MatrixStep5BodyRequest() {
  }

  @Override
  public int compareTo(final MatrixStep5BodyRequest _obj) {
    return _obj.getVehicleUUID()
               .compareTo(vehicleUUID);
  }

  public Set<String> getDeviceTypeIds() {
    return deviceTypeIds;
  }

  public String getVehicleUUID() {
    return this.vehicleUUID;
  }

  public void setDeviceTypeIds(final Set<String> _deviceTypeIds) {
    deviceTypeIds = _deviceTypeIds;
  }

  public void setVehicleUUID(final String _vehicleUUID) {
    vehicleUUID = _vehicleUUID;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixStep5BodyRequest [deviceTypeIds=");
    builder.append(this.deviceTypeIds);
    builder.append(", vehicleUUID=");
    builder.append(this.vehicleUUID);
    builder.append("]");
    return builder.toString();
  }

}
