/**
 * 
 */
package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.state.State;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

/**
 * @author Luca Vassallo
 */
public class FsmActionDispositivoGeneric implements Action<StatoDispositivo, DispositivoEvent> {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void execute(StateContext<StatoDispositivo, DispositivoEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Dispositivo) {
      Dispositivo dispositivo = (Dispositivo) obj;

      if (logger.isDebugEnabled()) {
        logger.debug("Generic FSM Action to Dispositivo [ID: " + ((dispositivo != null) ? dispositivo.getId() : null) + "]");
      }
    }

    if (logger.isDebugEnabled()) {

      StringBuilder sb = new StringBuilder("Transition for Dispositivo ");
      StatoDispositivo statoPartenza = null;
      StatoDispositivo statoDestinazione = null;
      State<StatoDispositivo, DispositivoEvent> source = context.getSource();
      if (source != null) {
        statoPartenza = source.getId();
        sb.append(" FROM '");
        sb.append((statoPartenza != null) ? statoPartenza.name() : null);
        sb.append("' ");
      }
      State<StatoDispositivo, DispositivoEvent> target = context.getTarget();
      if (target != null) {
        statoDestinazione = target.getId();
        sb.append(" TO '");
        sb.append((statoDestinazione != null) ? statoDestinazione.name() : null);
        sb.append("' ");
      }

      boolean isAuto = false;
      DispositivoEvent event = context.getEvent();
      if (event == null) {
        isAuto = true;
      }
      sb.append((isAuto) ? " AUTO " : " with command " + event.name());
      logger.debug(sb.toString());
    }
  }

}
