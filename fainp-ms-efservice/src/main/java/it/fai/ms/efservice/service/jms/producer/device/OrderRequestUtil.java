package it.fai.ms.efservice.service.jms.producer.device;

import org.apache.commons.lang3.StringUtils;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.LicensePlate;
import it.fai.ms.common.jms.efservice.ObuId;
import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ObuType;
import it.fai.ms.common.jms.efservice.ObuTypeId;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.domain.Richiesta;

public class OrderRequestUtil {

  private OrderRequestDecorator orderRequest;

  public OrderRequestUtil(final Richiesta richiesta) {
    orderRequest = new OrderRequestDecorator(richiesta);
  }
  
  public ContractOrderRequest getContractUuid() {
    return new ContractOrderRequest(orderRequest.findNumeroContract(),orderRequest.findCodiceCliente());
  }

  public OrderRequestUuid getOrderRequestUuid() {
    return new OrderRequestUuid(orderRequest.getIdentificativo());
  }

  public String getVehicleUuid() {
    return orderRequest.getVehicleUuid();
  }

  public ObuType getObuType() {
    String deviceType = orderRequest.getHardwareDeviceType();
    if (StringUtils.isBlank(deviceType)) {
      TipoDispositivoEnum tipoDispositivo = orderRequest.getDeviceType();
      return mapObuTypeFromDeviceTypeName(tipoDispositivo);
    }
    return ObuType.fromString(deviceType);
  }

  public ObuId getObuId() {
    return new ObuId(orderRequest.getDeviceSerialId());
  }

  public ObuNumber getObuNumber() {
    return new ObuNumber(orderRequest.getDeviceSerialId());
  }

  public LicensePlate getLicensePlate() {
    return new LicensePlate(orderRequest.getLicensePlate(), orderRequest.getCountry());
  }

  public LicensePlate getOldLicensePlate() {
    return new LicensePlate(orderRequest.getOldLicensePlate(), orderRequest.getOldCountry());
  }

  private ObuType mapObuTypeFromDeviceTypeName(TipoDispositivoEnum _deviceTypeName) {
    ObuType obuType;
    switch (_deviceTypeName) {
    case TELEPASS_ITALIANO:
      obuType = new ObuType(ObuTypeId.AT);
      break;
    case VIACARD:
      obuType = new ObuType(ObuTypeId.TV);
      break;
    case TRACKYCARD:
      obuType = new ObuType(ObuTypeId.FP);
      break;
    case TELEPASS_EUROPEO_SAT:
      obuType = new ObuType(ObuTypeId.AA);
      break;
    default:
      throw new RuntimeException("Not manage device type " + _deviceTypeName + " to retrieve ObuType");
    }

    return obuType;
  }

}
