package it.fai.ms.efservice.service;

public class RichiestaHasContractException extends RuntimeException {

  private static final long serialVersionUID = 7766879935482422727L;

  public RichiestaHasContractException() {
  }

  public RichiestaHasContractException(final String _message) {
    super(_message);
  }

  public RichiestaHasContractException(final Throwable _cause) {
    super(_cause);
  }

  public RichiestaHasContractException(final String _message, final Throwable _cause) {
    super(_message, _cause);
  }

}
