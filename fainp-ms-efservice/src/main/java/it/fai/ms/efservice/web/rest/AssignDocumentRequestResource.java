package it.fai.ms.efservice.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.service.AssignDocumentToRequestService;
import it.fai.ms.efservice.service.dto.AssignDocumentRequestDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

@RestController
@RequestMapping(AssignDocumentRequestResource.BASE_PATH)
public class AssignDocumentRequestResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  public static final String ASSIGN_DOCUMENT_RICHIESTA = "/assigndocument";

  private final AssignDocumentToRequestService assignDocToRequestService;
  
  public AssignDocumentRequestResource(final AssignDocumentToRequestService assignDocToRequestService) {
    this.assignDocToRequestService = assignDocToRequestService;
  }

  @PostMapping(ASSIGN_DOCUMENT_RICHIESTA)
  @Timed
  public ResponseEntity<Void> assignDocumentToRequest(@RequestBody AssignDocumentRequestDTO dto) throws CustomException {
    log.debug("Request to assign document on request by DTO: {}", dto);
    
    assignDocToRequestService.assignDocument(dto);
    
    if (dto == null) {
      log.error("Not generate ORDINI DTO for richieste {}", dto);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_ON_GENERATE_ORDER_DTO);
    }

    return ResponseEntity.ok().build();
  }
  
}
