package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;

/**
 * Service Interface for managing ConfigurazioneDispositiviServizi.
 */
public interface ConfigurazioneDispositiviServiziService {

  /**
   * Save a configurazioneDispositiviServizi.
   *
   * @param configurazioneDispositiviServizi
   *          the entity to save
   * @return the persisted entity
   */
  ConfigurazioneDispositiviServizi save(ConfigurazioneDispositiviServizi configurazioneDispositiviServizi);

  /**
   * Get all the configurazioneDispositiviServizis.
   *
   * @param pageable
   *          the pagination information
   * @return the list of entities
   */
  Page<ConfigurazioneDispositiviServizi> findAll(Pageable pageable);

  /**
   * Get the "id" configurazioneDispositiviServizi.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  ConfigurazioneDispositiviServizi findOne(Long id);

  /**
   * Delete the "id" configurazioneDispositiviServizi.
   *
   * @param id
   *          the id of the entity
   */
  void delete(Long id);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @return
   */
  Optional<ConfigurazioneDispositiviServizi> findConfigurazione(TipoDispositivoEnum tipoDispositivo, String servizio);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @param modalitaGestione
   * @return
   */
  Optional<ConfigurazioneDispositiviServizi> findConfigurazione(TipoDispositivoEnum tipoDispositivo, String servizio,
                                                                ModalitaGestione modalitaGestione);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @return
   */
  Optional<ConfigurazioneDispositiviServizi> findConfigurazioneDaRemoto(TipoDispositivoEnum tipoDispositivo, String servizio);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @return
   */
  boolean isGestibileDaRemoto(TipoDispositivoEnum tipoDispositivo, String servizio);

  /**
   * @param tipoDispositivo
   * @param servizi
   * @return
   */
  boolean isGestibiliDaRemoto(TipoDispositivoEnum tipoDispositivo, List<String> servizi);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @return
   */
  Optional<ConfigurazioneDispositiviServizi> findConfigurazioneOffline(TipoDispositivoEnum tipoDispositivo, String servizio);

  /**
   * @param tipoDispositivo
   * @param servizio
   * @return
   */
  boolean isGestibileOffline(TipoDispositivoEnum tipoDispositivo, String servizio);

  /**
   * @param tipoDispositivo
   * @param servizi
   * @return
   */
  boolean isGestibiliOffline(TipoDispositivoEnum tipoDispositivo, List<String> servizi);

  /**
   * @param nomeTipoDispositivo
   * @param collect
   * @return
   */
  boolean isGestibili(TipoDispositivoEnum nomeTipoDispositivo, List<String> collect);

  List<ConfigurazioneDispositiviServizi> findRemoteConfigurations(TipoDispositivoEnum deviceType);

}
