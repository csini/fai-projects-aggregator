package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.RichiestaModificaGenerationService;
import it.fai.ms.efservice.service.RichiestaModificaServiceExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.web.rest.errors.CustomException;

@Service
@Transactional
public class RichiestaModificaServiceImplExt implements RichiestaModificaServiceExt {

  private final static String SEPARATOR_TARGA_NAZIONE = "§";

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaModificaGenerationService richiestaModificaGenerator;

  private final RichiestaServiceExt richiestaServiceExt;

  private final VehicleClient vehicleClient;

  private final OrdineClienteServiceExt ordineClienteService;

  public RichiestaModificaServiceImplExt(final RichiestaServiceExt _richiestaServiceExt, final VehicleClient _vehicleClient,
                                         final RichiestaModificaGenerationService _richiestaModificaGenerator,
                                         final OrdineClienteServiceExt _ordineClienteService,
                                         final DispositivoServiceExt _dispositivoService) {
    richiestaServiceExt = _richiestaServiceExt;
    vehicleClient = _vehicleClient;
    richiestaModificaGenerator = _richiestaModificaGenerator;
    ordineClienteService = _ordineClienteService;
  }

  @Override
  public List<Long> createRichiestaModifica(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException, FsmExecuteCommandException {
    log.info("Richieste di {} to device type {}", carrelloDTO.getTipoOperazioneModifica(), carrelloDTO.getTipoDispositivo());
    boolean isVariazioneTarga = isOperationsVariazioneTarga(carrelloDTO);
    if (isVariazioneTarga) {
      setTargaAndNazioneOnCartDTO(carrelloDTO);
    }

    List<Richiesta> richieste = richiestaModificaGenerator.generateRichieste(carrelloDTO);
    richieste = populateTargaNazioneIfRequestIsNotMezzoRitargatoOrVariazioneTarga(richieste, carrelloDTO);

    ordineClienteService.createOrdineClienteByRichieste(richieste, carrelloDTO);
    log.info("Modification request generated: {}", mappingRequestoToModificationRequestDTO(richieste));

    richieste = assignTargaOnRichiesteIfViaCardAndMezzoRitargato(richieste);

    richieste = setTargaNazioneOnParticularDeviceTypesAndOperationsRequests(richieste, carrelloDTO);

    populateAssociazioneIfNotSet(richieste, carrelloDTO.getVeicolo());

    richieste = richiestaServiceExt.save(richieste);
    log.info("Richieste saved: {}", mappingRequestoToModificationRequestDTO(richieste));
    List<Long> resultSet = richiestaServiceExt.executeFsmForRichiesteDiModifica(richieste);
    log.debug("Ids Richieste di modifica: {}", resultSet);
    return resultSet;
  }

  private void populateAssociazioneIfNotSet(List<Richiesta> richieste, String uuidVehicle) {
    richieste.forEach(r -> {
      if (r != null && StringUtils.isBlank(r.getAssociazione())) {
        log.debug("Not set Associazione field [{}] on richiesta [{}]", r.getAssociazione(), r.getIdentificativo());
        VehicleDTO vehicle = vehicleClient.findVehicleByUUID(jwt, uuidVehicle);
        if (vehicle != null) {
          String targa = vehicle.getTarga();
          String nazione = vehicle.getNazione();
          log.debug("Set Targa {} nazione {} on request: {}", targa, nazione, r.getIdentificativo());
          r.setAssociazione(targa);
          r.setCountry(nazione);
        } else {
          log.warn("Not found vehicle : {}", uuidVehicle);
        }
      } else {
        log.trace("Skip to set targa and nazione because already set... on request: {}", r.getIdentificativo());
      }
    });
  }

  private List<Richiesta> populateTargaNazioneIfRequestIsNotMezzoRitargatoOrVariazioneTarga(List<Richiesta> richieste,
                                                                                            CarrelloModificaRichiestaDTO carrelloDTO) {
    List<Richiesta> requests = new ArrayList<>();
    Optional<VehicleDTO> optVehicle = Optional.empty();
    optVehicle = getVehicle(carrelloDTO.getVeicolo());
    for (Richiesta richiesta : richieste) {
      switch (richiesta.getTipo()) {
      case FURTO:
      case SMARRIMENTO:
      case MALFUNZIONAMENTO:
        if (optVehicle.isPresent()) {
          richiesta = populateTargaNazioneOnRichiesta(richiesta, optVehicle.get());
        } else {

        }
        break;

      case FURTO_CON_SOSTITUZIONE:
      case SMARRIMENTO_CON_SOSTITUZIONE:
      case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
        if (optVehicle.isPresent()) {
          richiesta = populateTargaNazioneAndNewTargaNazioneOnRichiesta(richiesta, optVehicle.get());
        } else {
          log.warn("Not found vehicle to populate Targa e Nazione on request con sostituzione {}",
                   richiesta != null ? richiesta.getIdentificativo() : null);
        }
        break;

      default:
        break;
      }
      requests.add(richiesta);
    }
    return requests;
  }

  private Richiesta populateTargaNazioneAndNewTargaNazioneOnRichiesta(Richiesta richiesta, VehicleDTO v) {
    richiesta = populateTargaNazioneOnRichiesta(richiesta, v);
    String targa = v.getTarga();
    String nazione = v.getNazione();
    String newTargaNazione = targa + SEPARATOR_TARGA_NAZIONE + nazione;
    richiesta.setNewTargaNazione(newTargaNazione);
    log.debug("NewTargaNazione for request type: {} - ID: {}", richiesta.getTipo(), richiesta.getIdentificativo());
    return richiesta;
  }

  private Richiesta populateTargaNazioneOnRichiesta(Richiesta richiesta, VehicleDTO v) {
    String targa = v.getTarga();
    richiesta.setAssociazione(targa);
    String nazione = v.getNazione();
    richiesta.setCountry(nazione);
    log.debug("Set associazione: {} and country: {}", targa, nazione);
    return richiesta;
  }

  private Optional<VehicleDTO> getVehicle(String veicolo) {
    VehicleDTO vehicle = null;
    if (StringUtils.isNotBlank(veicolo)) {
      vehicle = vehicleClient.findVehicleByUUID(jwt, veicolo);
    }
    return Optional.ofNullable(vehicle);
  }

  private List<Richiesta> assignTargaOnRichiesteIfViaCardAndMezzoRitargato(List<Richiesta> richieste) {
    return richieste.stream()
                    .map(r -> {
                      if (r.getTipo() == TipoRichiesta.MEZZO_RITARGATO) {
                        if (r.getTipoDispositivo() != null && r.getTipoDispositivo()
                                                               .getNome() == TipoDispositivoEnum.VIACARD) {
                          log.info("Assign targaNazione on requests: {}", richieste);
                          r = assignTargaOnRichiesta(r);
                        }
                      }
                      return r;
                    })
                    .collect(toList());
  }

  private Richiesta assignTargaOnRichiesta(Richiesta richiesta) {
    log.debug("Assign Targa on richiesta by Vehicle...");

    // Per regola la corrispondenza tra richiesta e dispositivo deve essere uno ad uno, quindi posso prendere il primo
    // elemento della lista dei dispositivi;
    // Deve per forza esserci un dispositivo legato ad una richiesta, se no è un errore;
    Dispositivo dispositivo = richiesta.getDispositivos()
                                       .parallelStream()
                                       .findFirst()
                                       .get();
    if (dispositivo != null) {
      // Di regola dovrebbe esserci solo un veicolo associato ad un dispositivo;
      Optional<AssociazioneDV> associazioneDvOpt = dispositivo.getAssociazioneDispositivoVeicolos()
                                                              .stream()
                                                              .findFirst();

      AssociazioneDV associazioneDV = null;
      if (associazioneDvOpt.isPresent()) {
        associazioneDV = associazioneDvOpt.get();
      } else {
        log.error("Not found associazione DV for device: {}", dispositivo);
      }
      if (associazioneDV != null) {
        String uuidVeicolo = associazioneDV.getUuidVeicolo();
        log.debug("Retrieve targa from VehicleClient by UUID Vehicle {}", uuidVeicolo);
        VehicleDTO vehicleDTO = vehicleClient.findVehicleByUUID(jwt, uuidVeicolo);
        if (vehicleDTO != null) {
          String licensePlate = vehicleDTO.getTarga();
          log.debug("Set targa {} for identificativo veicolo {} on richiesta {}", licensePlate, uuidVeicolo, richiesta.getIdentificativo());
          richiesta.setAssociazione(licensePlate);
          String country = vehicleDTO.getNazione();
          log.debug("Country of targa {} : {}", licensePlate, country);
          richiesta.setCountry(country);
        } else {
          log.warn("Not found vehicle for uuidVehicle {}", uuidVeicolo);
        }
      } else {
        log.error("Not found associazioneDV for dispositivo {} for richiesta {}", dispositivo.getIdentificativo(),
                  richiesta.getIdentificativo());
      }
    } else {
      String msgErrorExp = "Not found dispositivo related on richiesta: " + richiesta.getIdentificativo();
      log.error(msgErrorExp);
    }

    return richiesta;
  }

  private List<Richiesta> setTargaNazioneOnParticularDeviceTypesAndOperationsRequests(List<Richiesta> richieste,
                                                                                      CarrelloModificaRichiestaDTO carrelloDTO) {
    List<Richiesta> requests = new ArrayList<>();

    boolean isMezzoRitargato = isOperationsMezzoRitargato(carrelloDTO);

    for (Richiesta r : richieste) {
      boolean isVarTargaAndParticularDeviceType = isVariazioneTargaOperationsAndDeviceTypeTelepassItaOrGoBox(r, carrelloDTO);
      log.debug("For request {} isVariazioneTargaTelepassITorGoBocx: {} or isMezzoRitargato: {}", r, isVarTargaAndParticularDeviceType,
                isMezzoRitargato);

      if (isVarTargaAndParticularDeviceType || isMezzoRitargato) {
        List<Richiesta> requestList = new ArrayList<>();
        requestList.add(r);
        filterRequestByDeviceTypeAndAssignTarga(requestList, new ArrayList<>(Arrays.asList(TipoDispositivoEnum.TELEPASS_ITALIANO)));

        List<Richiesta> richiesteNotIs = getFilteredRequestWhereDeviceTypeNotIs(requestList,
                                                                                new ArrayList<>(Arrays.asList(TipoDispositivoEnum.TELEPASS_ITALIANO,
                                                                                                              TipoDispositivoEnum.GO_BOX)));

        List<Richiesta> richiesteIs = getFilteredRequestWhereDeviceTypeIs(requestList,
                                                                          new ArrayList<>(Arrays.asList(TipoDispositivoEnum.TELEPASS_ITALIANO,
                                                                                                        TipoDispositivoEnum.GO_BOX)));
        retrieveAndSetTargaNazione(richiesteIs);

        requests.addAll(richiesteNotIs);
        requests.addAll(richiesteIs);
      } else {
        log.warn("Not assign targa on associazione richiesta to particular device types....");
        requests.add(r);
      }
    }

    return requests;
  }

  private boolean isVariazioneTargaOperationsAndDeviceTypeTelepassItaOrGoBox(Richiesta request, CarrelloModificaRichiestaDTO carrelloDTO) {
    boolean isVariazioneTarga = isOperationsVariazioneTarga(carrelloDTO);
    if (isVariazioneTarga) {
      if (request != null) {
        TipoDispositivo tipoDispositivo = request.getTipoDispositivo();
        if (tipoDispositivo != null) {
          TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
          boolean isTelepassIta = isDeviceType(deviceType, TipoDispositivoEnum.TELEPASS_ITALIANO);
          boolean isGoBox = isDeviceType(deviceType, TipoDispositivoEnum.GO_BOX);
          log.info("isTelepassIta: {} - isGoBox: {}, isVariazioneTarga: {}", isTelepassIta, isGoBox, isVariazioneTarga);
          if (isTelepassIta || isGoBox) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private void retrieveAndSetTargaNazione(List<Richiesta> richieste) {
    richieste = richieste.stream()
                         .map(r -> {
                           String newTargaNazione = r.getNewTargaNazione();
                           String associazione = r.getAssociazione();
                           String country = r.getCountry();
                           log.info("NewTargaNazione: {} - oldLicensePlate: {} - OldCountry: {}", newTargaNazione, associazione, country);
                           String[] split = newTargaNazione.split(SEPARATOR_TARGA_NAZIONE);
                           if (split.length > 1) {
                             String targaNazione = associazione + SEPARATOR_TARGA_NAZIONE + country;
                             r.newTargaNazione(targaNazione);
                             associazione = split[0];
                             country = split[1];
                             log.info("NewTargaNazione: {} - oldLicensePlate: {} - oldCountry: {}", targaNazione, associazione, country);
                             r.associazione(associazione)
                              .country(country);
                           }
                           return r;
                         })
                         .collect(toList());
  }

  private List<Richiesta> getFilteredRequestWhereDeviceTypeIs(List<Richiesta> richieste, List<TipoDispositivoEnum> deviceTypes) {
    return richieste.stream()
                    .filter(r -> isRequestDeviceTypeIn(r, deviceTypes))
                    .collect(toList());
  }

  private List<Richiesta> getFilteredRequestWhereDeviceTypeNotIs(List<Richiesta> richieste, List<TipoDispositivoEnum> deviceTypes) {
    return richieste.stream()
                    .filter(r -> !isRequestDeviceTypeIn(r, deviceTypes))
                    .collect(toList());
  }

  private void filterRequestByDeviceTypeAndAssignTarga(List<Richiesta> richieste, List<TipoDispositivoEnum> deviceTypes) {
    for (Richiesta request : richieste) {
      if (isRequestDeviceTypeIn(request, deviceTypes)) {
        log.debug("Assign targa on filtered request...");
        assignTargaOnRichiesta(request);
      }
    }
  }

  private boolean isRequestDeviceTypeIn(Richiesta request, List<TipoDispositivoEnum> deviceTypes) {
    if (request.getTipoDispositivo() != null && deviceTypes.contains(request.getTipoDispositivo()
                                                                            .getNome()))
      return true;
    return false;
  }

  private List<RichiestaModificaDTO> mappingRequestoToModificationRequestDTO(List<Richiesta> richieste) {
    return richieste.stream()
                    .map(r -> mappingRequestoToModificationRequestDTO(r))
                    .collect(toList());
  }

  private RichiestaModificaDTO mappingRequestoToModificationRequestDTO(Richiesta request) {
    TipoDispositivoEnum deviceType = (request.getTipoDispositivo() != null) ? request.getTipoDispositivo()
                                                                                     .getNome()
                                                                            : null;
    return new RichiestaModificaDTO(request.getIdentificativo(), request.getTipo(), deviceType, request.getStato(),
                                    request.getAssociazione(), request.getCountry());
  }

  private boolean isDeviceType(TipoDispositivoEnum deviceTypeFromCart, TipoDispositivoEnum deviceType) {
    if (deviceTypeFromCart != null && deviceType != null && deviceTypeFromCart.equals(deviceType))
      return true;
    return false;
  }

  private boolean isOperationsMezzoRitargato(CarrelloModificaRichiestaDTO carrelloDTO) {
    TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
    if (tipoOperazioneModifica.equals(TipoRichiesta.MEZZO_RITARGATO))
      return true;
    return false;
  }

  private boolean isOperationsVariazioneTarga(CarrelloModificaRichiestaDTO carrelloDTO) {
    TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
    if (tipoOperazioneModifica.equals(TipoRichiesta.VARIAZIONE_TARGA))
      return true;
    return false;
  }

  private boolean isViaCardDevice(CarrelloModificaRichiestaDTO carrelloDTO) {
    TipoDispositivoEnum deviceType = carrelloDTO.getTipoDispositivo();
    if (deviceType != null && deviceType.equals(TipoDispositivoEnum.VIACARD))
      return true;
    return false;
  }

  private void setTargaAndNazioneOnCartDTO(CarrelloModificaRichiestaDTO carrelloDTO) {
    String uuidNewVeicolo = carrelloDTO.getNewVeicolo();
    VehicleDTO vehicle = vehicleClient.findVehicleByUUID(jwt, uuidNewVeicolo);
    if (vehicle != null) {
      String targa = vehicle.getTarga();
      carrelloDTO.setTarga(targa);
      String nazione = vehicle.getNazione();
      carrelloDTO.setNazione(nazione);
      log.info("Add targa {} and nazione {} on carrelloDTO: {}", targa, nazione, carrelloDTO);
    } else {
      log.error("Not found vehicle by UUID: {}", uuidNewVeicolo);
      throw new IllegalArgumentException("Not found vehicle");
    }
  }

  public class RichiestaModificaDTO {

    private String              identificativo;
    private TipoRichiesta       requestType;
    private TipoDispositivoEnum deviceType;
    private StatoRichiesta      stato;
    private String              targa;
    private String              nazione;

    public RichiestaModificaDTO(String identificativo, TipoRichiesta requestType, TipoDispositivoEnum deviceType, StatoRichiesta stato,
                                String targa, String nazione) {
      this.identificativo = identificativo;
      this.requestType = requestType;
      this.deviceType = deviceType;
      this.stato = stato;
      this.targa = targa;
      this.nazione = nazione;
    }

    @Override
    public String toString() {
      return "RichiestaModificaDTO [identificativo=" + identificativo + ", requestType=" + requestType + ", deviceType=" + deviceType
             + ", stato=" + stato + ", targa=" + targa + ", nazione=" + nazione + "]";
    };
  }

  @Override
  public List<Long> associaTarga(TipoDispositivoEnum tipoDispositivo, List<String> dispositivi, String vehicle) {

    Optional<String> deviceIdentifierOpt = dispositivi.stream()
                                                      .findFirst();
    if (deviceIdentifierOpt.isPresent()) {
      String deviceIdentifier = deviceIdentifierOpt.get();
      return richiestaModificaGenerator.associaTarga(tipoDispositivo, deviceIdentifier, vehicle);
    }
    return new ArrayList<>();
  }

  @Override
  public List<Long> richiestaPin(TipoDispositivoEnum tipoDispositivo, String codiceAzienda, List<String> dispositivi) {

    Optional<String> deviceIdentifierOpt = dispositivi.stream()
                                                      .findFirst();
    if (deviceIdentifierOpt.isPresent()) {
      String deviceIdentifier = deviceIdentifierOpt.get();
      return richiestaModificaGenerator.richiestaPin(tipoDispositivo, codiceAzienda, deviceIdentifier);
    }
    return Collections.emptyList();
  }
}
