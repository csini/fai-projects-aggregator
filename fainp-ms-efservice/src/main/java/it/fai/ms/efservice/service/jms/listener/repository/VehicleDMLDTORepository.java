package it.fai.ms.efservice.service.jms.listener.repository;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.cache.AbstractCacheRepositoryImpl;
import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;

@Repository
public class VehicleDMLDTORepository
  extends AbstractCacheRepositoryImpl<VehicleDMLDTO, String>
  implements InterfaceDmlRepository<VehicleDMLDTO> {

  public VehicleDMLDTORepository(final Map<String, VehicleDMLDTO> vehicleDMLDTOCache) {
    super(vehicleDMLDTOCache);
  }

  @Override
  public VehicleDMLDTO save(VehicleDMLDTO o) {
    return super.save(o);
  }

  @Override
  public VehicleDMLDTO findByDmlUniqueIdentifier(String dmlUniqueIdentifier) {
    return this.findOne(dmlUniqueIdentifier);
  }

}
