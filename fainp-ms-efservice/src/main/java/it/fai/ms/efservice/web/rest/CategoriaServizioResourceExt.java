package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.CategoriaServizioServiceExt;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTOLight;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;

@RestController
@RequestMapping(CategoriaServizioResourceExt.BASE_PATH)
public class CategoriaServizioResourceExt {

  private final Logger log = LoggerFactory.getLogger(CategoriaServizioResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  private final CategoriaServizioResource categoriaServizioResource;

  private final CategoriaServizioServiceExt categoriaServizioService;

  public CategoriaServizioResourceExt(CategoriaServizioResource categoriaServizioResource,
                                      CategoriaServizioServiceExt categoriaServizioService) {
    this.categoriaServizioResource = categoriaServizioResource;
    this.categoriaServizioService = categoriaServizioService;
  }

  @GetMapping("/getListaCategoriaServizio")
  @Timed
  @ApiOperation(value = "Recupera la lista con tutte le Categorie servizi", response = CategoriaServizioResourceExt.class)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = "listaCategoriaServizio retrieved"),
                          @ApiResponse(code = 400, message = "getListaCategoriaServizio error") })
  public ResponseEntity<List<CategoriaServizioDTO>> getListaCategoriaServizio() {
    List<CategoriaServizioDTO> lista = categoriaServizioResource.getAllCategoriaServizios();

    // Sorting
    Collections.sort(lista, new FieldOrdinamentoComparator());

    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lista));

  }

  @GetMapping("/categoria/{nomeCategoria}/tipiservizi")
  @Timed
  @ApiOperation(value = "Recupera la lista di tutti i tipi dispositivo di una categoria", response = CategoriaServizioResourceExt.class)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = "TipiServiziByCategoria retrieved"),
                          @ApiResponse(code = 400, message = "getTipiServiziByCategoria error") })
  public ResponseEntity<List<TipoServizioDTO>> getTipiServiziByCategoria(@PathVariable("nomeCategoria") String nomeCategoria) {
    List<TipoServizioDTO> lista = categoriaServizioService.findServiziByNomeCategoria(nomeCategoria);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lista));

  }

  @GetMapping("/categoria/{nomeCategoria}/tipidispositivi")
  @Timed
  @ApiOperation(value = "Recupera la lista di tutti i tipi dispositivo di una categoria", response = CategoriaServizioResourceExt.class)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = "TipiDispositivoByCategoria retrieved"),
                          @ApiResponse(code = 400, message = "getTipiDispositivoByCategoria error") })
  public ResponseEntity<List<TipoDispositivoDTOLight>> getTipiDispositivoByCategoria(@PathVariable("nomeCategoria") String nomeCategoria) {
    List<TipoDispositivoDTOLight> lista = categoriaServizioService.findTipoDispositivoByNomeCategoria(nomeCategoria);
    log.debug("Device found: {} by categoria: {}", lista, nomeCategoria);
    List<TipoDispositivoEnum> deviceToExclude = Arrays.asList(TipoDispositivoEnum.KMASTER);
    List<TipoDispositivoDTOLight> listaFiltered = filterToExcludeDeviceType(lista, deviceToExclude);
    log.debug("Device filtered: {} by are not {}", lista, deviceToExclude);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(listaFiltered));

  }

  private List<TipoDispositivoDTOLight> filterToExcludeDeviceType(List<TipoDispositivoDTOLight> lista,
                                                                  List<TipoDispositivoEnum> deviceTypes) {
    if (lista == null || lista.isEmpty()) {
      return lista;
    }

    if (deviceTypes == null || deviceTypes.isEmpty()) {
      return lista;
    }

    return lista.parallelStream()
                .filter(dto -> deviceTypeNotInList(dto.getNome(), deviceTypes))
                .collect(toList());
  }

  private boolean deviceTypeNotInList(TipoDispositivoEnum device, List<TipoDispositivoEnum> deviceTypes) {
    return !deviceTypeInList(device, deviceTypes);
  }

  private boolean deviceTypeInList(TipoDispositivoEnum device, List<TipoDispositivoEnum> deviceTypes) {
    return deviceTypes.contains(device);
  }

}