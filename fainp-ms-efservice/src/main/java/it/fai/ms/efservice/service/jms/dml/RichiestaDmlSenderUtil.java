package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.AbstractDmlSender;
import it.fai.ms.common.dml.efservice.dto.RichiestaDMLDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.jms.mapper.RichiestaJmsMapper;

public class RichiestaDmlSenderUtil extends AbstractDmlSender<RichiestaDMLDTO,Richiesta> {

  static final JmsTopicNames TOPIC_SAVE = JmsTopicNames.DML_RICHIESTE_SAVE;
  static final JmsTopicNames TOPIC_DELETE = JmsTopicNames.DML_RICHIESTE_DELETE;

  private final RichiestaJmsMapper richiestaJmsMapper = new RichiestaJmsMapper();

  public RichiestaDmlSenderUtil(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

  @Override
  protected RichiestaDMLDTO getSavingDmlDto(Richiesta ord) {
    return richiestaJmsMapper.toRichiestaDMLDTO(ord);
  }

  @Override
  protected RichiestaDMLDTO getDeletingDmlDto(String dmlUniqueIdentifier){
    return richiestaJmsMapper.toEmptyRichiestaDMLDTO(dmlUniqueIdentifier);
  }
}
