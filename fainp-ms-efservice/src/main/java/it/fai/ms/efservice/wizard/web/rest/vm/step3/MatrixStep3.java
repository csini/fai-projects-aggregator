package it.fai.ms.efservice.wizard.web.rest.vm.step3;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixAbstract;

@JsonInclude(Include.NON_EMPTY)
public class MatrixStep3 extends MatrixAbstract {

  private static final long serialVersionUID = -5238218633259284880L;

  private String                          deviceTypeId;
  private Map<String, MatrixStep3Vehicle> vehicles = new LinkedHashMap<>();

  public MatrixStep3(final String _deviceTypeId) {
    deviceTypeId = _deviceTypeId;
  }

  @JsonProperty(value = "tipoDispositivo")
  public String getDeviceTypeId() {
    return deviceTypeId;
  }

  public Map<String, MatrixStep3Vehicle> getVehicles() {
    return vehicles;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName() + " [deviceTypeId=");
    builder.append(deviceTypeId);
    builder.append(",vehicles=");
    builder.append(vehicles);
    builder.append(",header=");
    builder.append(this.getHeader());
    builder.append("]");
    return builder.toString();
  }

}
