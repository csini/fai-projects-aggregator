package it.fai.ms.efservice.service.fsm.bean.vispro;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.vispro.FsmInoltroVispro;

@WithStateMachine(id = FsmInoltroVispro.FSM_INOLTRO_VISPRO)
public class FsmInoltroVisproTransitionBean extends AbstractFsmRichiestaTransition {
}
