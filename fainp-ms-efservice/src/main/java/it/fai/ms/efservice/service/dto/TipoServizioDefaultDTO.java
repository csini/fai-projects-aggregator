package it.fai.ms.efservice.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the TipoServizioDefault entity.
 */
public class TipoServizioDefaultDTO implements Serializable {

    private Long id;

    private Long tipoDispositivoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipoDispositivoId() {
        return tipoDispositivoId;
    }

    public void setTipoDispositivoId(Long tipoDispositivoId) {
        this.tipoDispositivoId = tipoDispositivoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TipoServizioDefaultDTO tipoServizioDefaultDTO = (TipoServizioDefaultDTO) o;
        if(tipoServizioDefaultDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoServizioDefaultDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoServizioDefaultDTO{" +
            "id=" + getId() +
            "}";
    }
}
