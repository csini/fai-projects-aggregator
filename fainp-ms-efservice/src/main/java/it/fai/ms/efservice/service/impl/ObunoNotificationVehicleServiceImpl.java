package it.fai.ms.efservice.service.impl;

import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;

@Service
@Transactional
public class ObunoNotificationVehicleServiceImpl implements ObunoNotificationVehicleService {

  private Logger _log = LoggerFactory.getLogger(getClass());

  private final ContrattoRepositoryExt contractRepository;

  private static String NOME_PRODUTTORE = "TELEPASS";

  @Autowired
  public ObunoNotificationVehicleServiceImpl(final ContrattoRepositoryExt _contractRepository) {
    contractRepository = _contractRepository;
  }

  @Override
  public Optional<String> findCompanyCodeByContractUuid(final String _contractUuid) {
    String companyCode = null;
    final Optional<Contratto> optionalContract = findContract(_contractUuid);
    if (optionalContract.isPresent()) {
      companyCode = extractCompanyCode(optionalContract.get());
    }
    return Optional.ofNullable(companyCode);
  }

  private String extractCompanyCode(final Contratto _contract) {
    final String companyCodeUuid = _contract.getClienteFai()
                                            .getCodiceCliente();
    _log.info("Company code uuid {} for contract {}", companyCodeUuid, _contract.getCodContrattoCliente());
    return companyCodeUuid;
  }

  private Optional<Contratto> findContract(final String _contractUuid) {
    
    final Optional<Contratto> optionalContract = contractRepository.findFirstByCodContrattoClienteAndProduttore_nome(_contractUuid,
                                                                                                                NOME_PRODUTTORE);
    if (optionalContract.isPresent()) {
      _log.info("Contract found for contract uuid {} : {}", _contractUuid, optionalContract.get());
    } else {
      _log.warn("Contract not found for contract uuid {}", _contractUuid);
    }
    return optionalContract;
  }

}
