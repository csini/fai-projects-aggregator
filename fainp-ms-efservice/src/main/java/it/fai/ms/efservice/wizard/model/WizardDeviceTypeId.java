package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardDeviceTypeId implements Serializable {

  private static final long serialVersionUID = 7440596298473592807L;

  private int    activableServicesNumber = 0;
  private String id;

  public WizardDeviceTypeId(final String _id) {
    id = _id;
  }

  public WizardDeviceTypeId(final String _id, final int _activableServicesNumber) {
    id = _id;
    activableServicesNumber = _activableServicesNumber;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardDeviceTypeId) _obj).getId(), id);
    }
    return isEquals;
  }

  public int getActivableServicesNumber() {
    return activableServicesNumber;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  public void setActivableServicesNumber(final int _activableServicesNumber) {
    activableServicesNumber = _activableServicesNumber;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName() + " [id=");
    builder.append(id);
    builder.append(",activableServicesNumber=");
    builder.append(activableServicesNumber);
    builder.append("]");
    return builder.toString();
  }

}
