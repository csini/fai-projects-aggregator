package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.jms.JmsDeviceErrorService;

@Service
@Transactional
public class DeviceNotLosableConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseKOConsumer deviceResponseKoConsumer;

  private final JmsDeviceErrorService jmsDeviceErrorService;

  public DeviceNotLosableConsumer(final DeviceResponseKOConsumer _deviceResponseKoConsumer,
                                  final JmsDeviceErrorService _jmsDeviceErrorService) {
    deviceResponseKoConsumer = _deviceResponseKoConsumer;
    jmsDeviceErrorService = _jmsDeviceErrorService;
  }

  public void consume(DeviceEventMessage eventMessage) {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();

    String identificativoRichiesta = deviceDataDTO.getRequestId();

    String errorMsg = jmsDeviceErrorService.buildErrorMessage(eventMessage);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

}
