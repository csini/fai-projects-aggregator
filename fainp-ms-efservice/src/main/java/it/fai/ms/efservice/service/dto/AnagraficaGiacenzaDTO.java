package it.fai.ms.efservice.service.dto;


import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the AnagraficaGiacenza entity.
 */
public class AnagraficaGiacenzaDTO implements Serializable {

    private Long id;

    private String tipoDispositivo;

    private String produttoreNome;

    private Integer giacenzaMinima;

    private Integer alertRiordino;

    private Integer quantitaUltimoRiordino;

    private Boolean notificationSend;

    private Long totaleDispositiviInGiacenza;

    private Instant dataUltimoRiordino;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoDispositivo() {
        return tipoDispositivo;
    }

    public void setTipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public String getProduttoreNome() {
        return produttoreNome;
    }

    public void setProduttoreNome(String produttoreNome) {
        this.produttoreNome = produttoreNome;
    }

    public Integer getGiacenzaMinima() {
        return giacenzaMinima;
    }

    public void setGiacenzaMinima(Integer giacenzaMinima) {
        this.giacenzaMinima = giacenzaMinima;
    }

    public Integer getAlertRiordino() {
        return alertRiordino;
    }

    public void setAlertRiordino(Integer alertRiordino) {
        this.alertRiordino = alertRiordino;
    }

    public Integer getQuantitaUltimoRiordino() {
        return quantitaUltimoRiordino;
    }

    public void setQuantitaUltimoRiordino(Integer quantitaUltimoRiordino) {
        this.quantitaUltimoRiordino = quantitaUltimoRiordino;
    }

    public Boolean isNotificationSend() {
        return notificationSend;
    }

    public void setNotificationSend(Boolean notificationSend) {
        this.notificationSend = notificationSend;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = (AnagraficaGiacenzaDTO) o;
        if(anagraficaGiacenzaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), anagraficaGiacenzaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AnagraficaGiacenzaDTO{" +
            "id=" + getId() +
            ", tipoDispositivo='" + getTipoDispositivo() + "'" +
            ", produttoreNome='" + getProduttoreNome() + "'" +
            ", giacenzaMinima=" + getGiacenzaMinima() +
            ", alertRiordino=" + getAlertRiordino() +
            ", quantitaUltimoRiordino=" + getQuantitaUltimoRiordino() +
            ", notificationSend='" + isNotificationSend() + "'" +
            "}";
    }

  public Long getTotaleDispositiviInGiacenza() {
    return totaleDispositiviInGiacenza;
  }

  public void setTotaleDispositiviInGiacenza(Long totaleDispositiviInGiacenza) {
    this.totaleDispositiviInGiacenza = totaleDispositiviInGiacenza;
  }

  public Instant getDataUltimoRiordino() {
    return dataUltimoRiordino;
  }

  public void setDataUltimoRiordino(Instant dataUltimoRiordino) {
    this.dataUltimoRiordino = dataUltimoRiordino;
  }
}
