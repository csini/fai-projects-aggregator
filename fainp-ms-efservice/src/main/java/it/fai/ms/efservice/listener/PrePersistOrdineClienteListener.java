package it.fai.ms.efservice.listener;

import java.util.UUID;

import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.efservice.domain.OrdineCliente;

public class PrePersistOrdineClienteListener {

  private final Logger log = LoggerFactory.getLogger(this.getClass());
  
  //no-injection please

  @PrePersist
  public void setIdentificativo(final OrdineCliente ordineCliente) {

    String uuidIdentificativo = UUID.randomUUID()
                                    .toString();
    if (log.isDebugEnabled()) {
      log.debug("Set identificativo: " + uuidIdentificativo + " for Ordine Cliente: " + ordineCliente);
    }

    ordineCliente.setIdentificativo(uuidIdentificativo);
  }
}
