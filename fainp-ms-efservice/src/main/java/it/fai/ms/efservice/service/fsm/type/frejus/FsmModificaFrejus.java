package it.fai.ms.efservice.service.fsm.type.frejus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.frejus.FsmModificaFrejusCollectConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(FsmModificaFrejus.FSM_MODIFICA_FREJUS)
public class FsmModificaFrejus extends FsmRichiestaGeneric {

  public static final String FSM_MODIFICA_FREJUS = "fsmModificaFrejus";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaFrejus(@Qualifier(FsmModificaFrejusCollectConfig.MODIFICA_FREJUS) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                           FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MODIFICA_FREJUS;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.INOLTRO_FEJUS;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }
}
