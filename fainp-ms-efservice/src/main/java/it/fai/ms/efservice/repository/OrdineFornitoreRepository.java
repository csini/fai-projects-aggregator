package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.OrdineFornitore;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the OrdineFornitore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdineFornitoreRepository extends JpaRepository<OrdineFornitore, Long> {

}
