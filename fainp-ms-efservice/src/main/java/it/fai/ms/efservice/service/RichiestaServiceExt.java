package it.fai.ms.efservice.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.OrdineDTO;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

/**
 * Service Interface for managing Richiesta.
 */
@Service
@Transactional
public interface RichiestaServiceExt {

  /**
   * @param carrelloDTO
   * @return List<Richiesta>
   * @throws Exception
   */
  List<Richiesta> generateAndSaveOrdineAndRichiesteByCarrelloDTO(CarrelloDTO carrelloDTO) throws Exception;

  /**
   * @param identificativoRichiesta
   * @return
   * @throws Exception
   */
  RichiestaDTO getRichiestaByIdentificativo(String identificativoRichiesta, boolean fetchEager) throws Exception;

  /**
   * @param carrelloDTO
   * @return
   * @throws Exception
   */
  List<Richiesta> generateRichiestaModifica(CarrelloModificaRichiestaDTO carrelloDTO, boolean newOrder) throws Exception;

  /**
   * @param identificativoRichiesta
   * @return
   * @throws Exception
   */
  Richiesta createRichiestaDiSostituzioneFromIdentificativo(String identificativoRichiesta) throws Exception;

  /**
   * @param identificativo
   * @param fetchEager
   * @return
   */
  Richiesta findRichiestaByIdentificativo(String identificativo, boolean fetchEager);

  /**
   * @param identificativo
   * @param fetchEager
   * @return
   */
  RichiestaDTO findRichiestaDTOByIdentificativo(String identificativo, boolean fetchEager);

  /**
   * @param richieste
   * @return
   * @throws Exception
   */
  List<OrdineDTO> generateOrdineDTOByRichieste(List<String> richieste) throws Exception;

  /**
   * @param codiceContratto
   * @param identificativoRichiesta
   * @return
   * @throws Exception
   */
  Richiesta associateContrattoToRichiesta(String codiceContratto, String identificativoRichiesta) throws Exception;

  /**
   * @param richieste
   */
  List<Richiesta> save(List<Richiesta> richieste);

  /**
   * @param richiesta
   */
  Richiesta save(Richiesta richiesta);

  /**
   *
   */
  Richiesta saveAndFlush(Richiesta richiesta);

  /**
   * @param richieste
   * @return
   * @throws FsmExecuteCommandException
   * @throws Exception
   */
  List<Long> executeFsmForRichiesteDiModifica(List<Richiesta> richieste) throws FsmExecuteCommandException ;

  /**
   * @param richiesta
   * @throws Exception
   */
  void sendMessageForGiustificativoFurtoSmarrimento(RichiestaDTO richiesta) ;

  /**
   * @param _contractUuid
   * @param _licensePlate
   */
  List<Richiesta> findByContrattoAndTargaNazioneAndStato(String _contractUuid, String _licensePlate, String _country, StatoRichiesta stato);

  /**
   * @param id
   */
  List<Richiesta> findByCodiceId(Long id);

  void delete(Richiesta richiesta);

}
