package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.AbstractDmlSender;
import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.service.jms.mapper.ClienteFaiJmsMapper;

public class ClienteFaiDmlSenderUtil extends AbstractDmlSender<AziendaDMLDTO,ClienteFai> {

  static final JmsTopicNames TOPIC_SAVE = JmsTopicNames.DML_AZIENDE_SAVE;
  
  static final ClienteFaiJmsMapper mapper = new ClienteFaiJmsMapper();

  public ClienteFaiDmlSenderUtil(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return null;
  }

  @Override
  protected AziendaDMLDTO getSavingDmlDto( ClienteFai ord) {
    return mapper.toAziendaDMLDTO(ord, null);
  }

  @Override
  protected AziendaDMLDTO getDeletingDmlDto(String dmlUniqueIdentifier){
    throw new UnsupportedOperationException("not implemented");
  }
}
