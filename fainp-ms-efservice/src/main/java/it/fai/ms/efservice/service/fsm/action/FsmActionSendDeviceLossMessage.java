package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceLossMessageProducerService;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional
public class FsmActionSendDeviceLossMessage implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceLossMessageProducerService deviceLossMessageService;
  private final RichiestaRepository              richiestaRepository;
  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public FsmActionSendDeviceLossMessage(final DeviceLossMessageProducerService _deviceLossMessageService,
                                        final RichiestaRepository _richiestaRepository, final JmsSenderQueueMessageService jmsSenderQueueService) {
    deviceLossMessageService = _deviceLossMessageService;
    richiestaRepository = _richiestaRepository;
    this.jmsSenderQueueService = jmsSenderQueueService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
      Message<RichiestaEvent> message = context.getMessage();
      if (message != null) {
        MessageHeaders headers = message.getHeaders();
        Object object = headers.get("object");
        if (object instanceof Richiesta) {
          Richiesta request = (Richiesta) object;
          try {
          Richiesta richiesta = richiestaRepository.findOne(request.getId());
          TipoRichiesta tipoRichiesta = richiesta.getTipo();
          log.info("Send EVTNO for TipoRichiesta: {} and Device Type: {}?", tipoRichiesta, richiesta.getTipoDispositivo()
                                                                                                    .getNome());
          switch (tipoRichiesta) {
          case FURTO:
          case FURTO_CON_SOSTITUZIONE:
          case SMARRIMENTO:
          case SMARRIMENTO_CON_SOSTITUZIONE:
            sendMessage(richiesta);
            break;

          default:
            log.warn("For type {} not send EVTNO", tipoRichiesta);
            break;
          }
        } catch (Exception e) {
          long currentTimeMillis = System.currentTimeMillis();
          String identifier = "DEVICE_LOSS" + currentTimeMillis;
          String messageError = String.format("Exception [%s] when send message to DEVICE LOSS", identifier);
          log.error(messageError, e);
          String error = identifier + " - " + e.getMessage();
          String operazione = RichiestaEvent.MS_ORDINE_SOSPESO_EXCEPTION.name();
          jmsSenderQueueService.sendCommandToFsm(request.getIdentificativo(), error, operazione);
        }
      }
    } 

  }

  private void sendMessage(Richiesta richiesta) {

    log.info("Send DeviceLossMessage for richiesta {}", richiesta);
    TipoDispositivoEnum deviceType = richiesta.getTipoDispositivo()
                                              .getNome();
    switch (deviceType) {
    case TELEPASS_EUROPEO:
    case VIA_TOLL:
    case TELEPASS_EUROPEO_SAT:
      deviceLossMessageService.deactivateDevice(richiesta);
      break;

    case TELEPASS_ITALIANO:
    case VIACARD:
      deviceLossMessageService.deactivatedDeviceTelepassIta(richiesta);
      break;

    default:
      log.warn("Device type {} does not managed...");
      break;
    }

  }

}
