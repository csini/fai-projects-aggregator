package it.fai.ms.efservice.rules.engine;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;

@Service
public class PrecalculatedRuleServiceImpl implements PrecalculatedRuleService {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleOutcomeRepository ruleOutcomeRepository;

  public PrecalculatedRuleServiceImpl(final RuleOutcomeRepository _ruleOutcomeRepository) {
    ruleOutcomeRepository = _ruleOutcomeRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> execute(final RuleEngineDeviceTypeId _deviceTypeId, final RuleEngineVehicleId _vehicleId) {
    _log.debug("Loading rule outcome for {} {}", _deviceTypeId, _vehicleId);
    Optional<RuleOutcome> ruleOutcome = ruleOutcomeRepository.find(_deviceTypeId, _vehicleId);
    _log.debug("Precalculated rule for {} {}: {}", _deviceTypeId, _vehicleId, ruleOutcome);
    return ruleOutcome;
  }
  
  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> execute(final RuleEngineServiceTypeId _serviceTypeId, final RuleEngineDeviceTypeId _deviceTypeId,
                                       final RuleEngineVehicleId _vehicleId) {
    _log.debug("Loading rule outcome for {} {} {}", _serviceTypeId, _deviceTypeId, _vehicleId);
    Optional<RuleOutcome> ruleOutcome = ruleOutcomeRepository.find(_serviceTypeId, _deviceTypeId, _vehicleId);
    _log.debug("Precalculated rule for {} {} {}: {}", _serviceTypeId, _deviceTypeId, _vehicleId, ruleOutcome);
    return ruleOutcome;
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<RuleOutcome> execute(final RuleEngineVehicleId _vehicleId) {
    _log.debug("Loading rule outcome for {}", _vehicleId);
    Optional<RuleOutcome> ruleOutcome = ruleOutcomeRepository.find(_vehicleId);
    _log.debug("Precalculated rule for {}: {}", _vehicleId, ruleOutcome);
    return ruleOutcome;
  }

}
