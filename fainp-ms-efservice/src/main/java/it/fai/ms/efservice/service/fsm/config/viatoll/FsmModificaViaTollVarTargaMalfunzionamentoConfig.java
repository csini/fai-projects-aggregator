package it.fai.ms.efservice.service.fsm.config.viatoll;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardMalfunzionamento;
import it.fai.ms.efservice.service.fsm.guard.GuardMezzoRitargatoViaToll;
import it.fai.ms.efservice.service.fsm.guard.GuardVariazioneTargaViaToll;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaViaTollVarTargaMalfunzionamentoConfig.MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaViaTollVarTargaMalfunzionamentoConfig extends FsmRichiestaConfig {

  public static final String MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO = "modViaTollVarTargaMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public FsmModificaViaTollVarTargaMalfunzionamentoConfig(final FsmSenderToQueue _senderFsmService) {
    super();
    this.senderFsmService = _senderFsmService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaViaTollVarTargaMalfunzionamento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .guard(new GuardVariazioneTargaViaToll())
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .guard(new GuardMezzoRitargatoViaToll())
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.MALFUNZIONAMENTO)
               .guard(new GuardMalfunzionamento())
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MALFUNZIONAMENTO)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionRientratoDispositivo(senderFsmService));
  }

}
