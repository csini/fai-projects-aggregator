package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public class OrdineClienteCacheDTO implements Serializable {

  private static final long serialVersionUID = 2537159775463746057L;

  private Long id;

  private String identificativo;

  public OrdineClienteCacheDTO() {
  }

  public OrdineClienteCacheDTO(Long id, String identificativo) {
    this.id = id;
    this.identificativo = identificativo;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("OrdineClienteCacheDTO [id=");
    builder.append(id);
    builder.append(", identificativo=");
    builder.append(identificativo);
    builder.append("]");
    return builder.toString();
  }

}
