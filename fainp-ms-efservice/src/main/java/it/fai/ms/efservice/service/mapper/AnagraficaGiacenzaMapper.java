package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.AnagraficaGiacenzaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity AnagraficaGiacenza and its DTO AnagraficaGiacenzaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AnagraficaGiacenzaMapper extends EntityMapper<AnagraficaGiacenzaDTO, AnagraficaGiacenza> {



    default AnagraficaGiacenza fromId(Long id) {
        if (id == null) {
            return null;
        }
        AnagraficaGiacenza anagraficaGiacenza = new AnagraficaGiacenza();
        anagraficaGiacenza.setId(id);
        return anagraficaGiacenza;
    }
}
