package it.fai.ms.efservice.service.fsm.config.frejus;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.config.FsmModificaSimpleConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaFrejusCollectConfig.MODIFICA_FREJUS)
public class FsmModificaFrejusCollectConfig extends FsmModificaSimpleConfig {

  public static final String MODIFICA_FREJUS = "modificaFrejus";

  private final Logger log = LoggerFactory.getLogger(getClass());
  
  public FsmModificaFrejusCollectConfig(FsmSenderToQueue fsmSenderToQueue) {
    super(fsmSenderToQueue);
  }

  @Override
  protected String getMachineId() {
    return FsmType.MODIFICA_FREJUS.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaFrejus();
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
