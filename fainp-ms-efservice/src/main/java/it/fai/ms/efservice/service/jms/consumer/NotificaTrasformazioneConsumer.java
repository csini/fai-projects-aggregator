package it.fai.ms.efservice.service.jms.consumer;

import it.fai.ms.common.jms.dto.anagazienda.TrasformazioneDTO;

public interface NotificaTrasformazioneConsumer {

  void consume(TrasformazioneDTO notificaTrasformazione);
}
