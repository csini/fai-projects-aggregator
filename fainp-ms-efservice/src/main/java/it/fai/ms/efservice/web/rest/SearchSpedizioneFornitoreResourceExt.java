package it.fai.ms.efservice.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.SpedizioneFornitoreServiceExt;
import it.fai.ms.efservice.service.dto.SearchSpedizioneFornitoreDTO;
import it.fai.ms.efservice.service.dto.SpedizioneFornitoreDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneFornitoreViewDTO;
import it.fai.ms.efservice.web.rest.util.ResponsePage;

@RestController
@RequestMapping(SearchSpedizioneFornitoreResourceExt.BASE_URL)
public class SearchSpedizioneFornitoreResourceExt {
  static final String         BASE_URL                      = "/api";
  private static final String SEARCH_SPEDIZIONE_PRODUTTORE     = "/_search/spedizioneProduttore";
  private static final String SEARCH_SPEDIZIONE_PRODUTTORE_DISPOSITIVI = "/_search/spedizioneProduttoreDispositivi";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final SpedizioneFornitoreServiceExt serviceExt;
  private final DispositivoServiceExt         dispositivoServiceExt;

  public SearchSpedizioneFornitoreResourceExt(SpedizioneFornitoreServiceExt serviceExt, DispositivoServiceExt dispositivoServiceExt) {
    this.serviceExt = serviceExt;
    this.dispositivoServiceExt = dispositivoServiceExt;
  }

  @PostMapping(SEARCH_SPEDIZIONE_PRODUTTORE)
  @Timed
  public ResponseEntity<ResponsePage> searchSpedizioneFornitore(
    @RequestBody(required = false) SearchSpedizioneFornitoreDTO dto,
    Pageable pageable
  ) {
    log.debug("REST request to search searchSpedizioneFornitore : {}", dto);
    if (dto == null) {
      dto = new SearchSpedizioneFornitoreDTO();
    }
    int pageNumber = 0;
    if (pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<SpedizioneFornitoreViewDTO> page = serviceExt.findAllWithFilter(dto, pageRequest);

    return ResponseEntity.ok(new ResponsePage<>(page));
  }

  @PostMapping(SEARCH_SPEDIZIONE_PRODUTTORE_DISPOSITIVI)
  @Timed
  public ResponseEntity<ResponsePage> searchSpedizioneFornitoreDispositivi(
    @RequestBody(required = false) List<TipoDispositivoEnum> dto,
    Pageable pageable
  ) {
    log.debug("REST request to search List<TipoDispositivoEnum> : {}", dto);
    
    int pageNumber = 0;
    if (pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<SpedizioneFornitoreDispositivoDTO> page = dispositivoServiceExt.getDevicesByTipiDispositivo(dto, // codiceAzienda
                                                                                               pageRequest);

    return ResponseEntity.ok(new ResponsePage<>(page));
  }
}
