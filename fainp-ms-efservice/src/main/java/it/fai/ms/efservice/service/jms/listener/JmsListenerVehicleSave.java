package it.fai.ms.efservice.service.jms.listener;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;
import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

// @Service(JmsListenerVehicleSave.QUALIFIER)
// @Transactional
@Deprecated
public class JmsListenerVehicleSave extends JmsObjectMessageListenerTemplate<VehicleDMLDTO> implements JmsTopicListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public final static String QUALIFIER = "jmsListenerVehicleSave";

  private List<VehicleConsumer> consumers = new LinkedList<>();

  public JmsListenerVehicleSave(final List<VehicleConsumer> consumers) throws Exception {
    this.consumers = consumers;
    log.info("{}", this.consumers);
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_VEHICLE_SAVE;
  }

  @Override
  protected void consumeMessage(VehicleDMLDTO dto) {
    try {
      if (dto.getIdentificativo() == null) {
        return;
      }

      VehicleMessage vehicleMessage = new VehicleMessage();
      vehicleMessage.setAxes(dto.getNumeroAssi());
      vehicleMessage.setCategory(dto.getCategoria());
      vehicleMessage.setCountry(dto.getNazione());
      vehicleMessage.setDieselParticulateFilterClass(dto.getClasseFiltroAntiparticolato());
      vehicleMessage.setEuroClass(dto.getEuro());
      vehicleMessage.setLibretto(dto.getUuidDocumento());
      vehicleMessage.setLicensePlate(dto.getTarga());
      vehicleMessage.setLoneVehicleGrossWeight(dto.getMassa());
      vehicleMessage.setLoneVehicleTareWeight(dto.getTara());
      vehicleMessage.setState(dto.getStato());
      vehicleMessage.setTrainsetGrossWeight(dto.getMassaConvoglio());
      vehicleMessage.setType(dto.getTipo());
      vehicleMessage.setUuid(dto.getIdentificativo());
      vehicleMessage.setMake(dto.getMarca());
      vehicleMessage.setPayload(dto.getPortata());

      log.info("Received message: {}", vehicleMessage);
      consumers.stream()
               .forEach(consumer -> {
                 log.debug("Dispatching Message {} to consumer {}", vehicleMessage, consumer);
                 consumer.consume(vehicleMessage);
               });
      log.info("Message queued: {}", vehicleMessage);
    } catch (Exception e) {
      log.error("Error", e);
    }
  }

}
