package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.DispositiviDaSpedireDTO;
import it.fai.ms.efservice.client.AnagaziendeClient;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.anagazienda.ValidazioneCliente;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipologiaAssociazione;
import it.fai.ms.efservice.dto.CheckDispositiviDTO;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.service.DispositiviDaSpedireCacheService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.TipoHardwareDispositivoService;
import it.fai.ms.efservice.service.TipoServizioServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.CreaDispositiviDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.dto.ModificaDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneClienteDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneFornitoreDispositivoDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.mapper.AssociazioneDVMapper;
import it.fai.ms.efservice.service.mapper.DispositivoMapper;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

/**
 * Service Implementation for managing Dispositivo.
 */
@Service
@Transactional
public class DispositivoServiceImplExt implements DispositivoServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final static String SERVIZIO_DEFAULT_VIA_CARD = "PEDAGGI_ITALIA";

  @Autowired
  private FsmDispositivo fsmDispositivo;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepositoryExt;

  @Autowired
  private DispositivoMapper dispositivoMapper;

  @Autowired
  private StatoDispositivoServizioRepository sdsRepository;

  @Autowired
  private AssociazioneDVRepository assDispVeicoloRepository;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoServiceExt;

  @Autowired
  private TipoServizioServiceExt tipoServizioServiceExt;

  @Autowired
  private ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService;

  @Autowired
  private JmsTopicSenderService jsmTopicSender;

  private final Map<String, WizardVehicleEntity> cacheVehicleWizard;

  @Autowired
  private AssociazioneDVRepository associazioneDVRepository;

  @Autowired
  private AssociazioneDVMapper associazioneDVMapper;

  @Autowired
  private VehicleClient vehicleClient;

  @Autowired
  private DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService;

  @Autowired
  private StatoDispositivoServizioRepositoryExt statoDispositivoServizioRepositoryExt;

  @Autowired
  private TipoHardwareDispositivoService tipoHardwareService;

  private final AnagaziendeClient anagaziendeClient;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public DispositivoServiceImplExt(@Qualifier("wizard_vehicle") Map<String, WizardVehicleEntity> cacheVehicleWizard,
                                   AnagaziendeClient anagaziendeClient) {
    this.cacheVehicleWizard = cacheVehicleWizard;
    this.anagaziendeClient = anagaziendeClient;
  }

  @Override
  public void prePersistRelatedEntity(Richiesta richiesta, HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) throws Exception {
    Set<Dispositivo> dispositivos = richiesta.getDispositivos();
    for (Dispositivo dispositivo : dispositivos) {
      Set<StatoDispositivoServizio> statoDispositivoServizios = dispositivo.getStatoDispositivoServizios();
      sdsRepository.save(statoDispositivoServizios);
    }

    List<Dispositivo> dispositiviSaved = save(dispositivos);

    persistAssociazioneDV(new HashSet<Dispositivo>(dispositiviSaved), mapDispositivoVeicolo);
  }

  @Override
  public void addRelationDispositivoToRichiesta(Richiesta richiesta) {

    Set<Dispositivo> dispositivos = richiesta.getDispositivos();
    for (Dispositivo dispositivo : dispositivos) {
      Set<Richiesta> richiestas = dispositivo.getRichiestas();
      if (richiestas == null) {
        richiestas = new HashSet<>();
      }

      richiestas.add(richiesta);
    }

    saveAndFlush(dispositivos);
  }

  @Override
  public TipoDispositivo searchTipoDispositivoByNome(String nomeTipoDispositivo) {
    TipoDispositivo tipoDispositivo = tipoDispositivoServiceExt.findOneWithRelationshipByNome(TipoDispositivoEnum.valueOf(nomeTipoDispositivo));
    if (tipoDispositivo == null) {
      log.error("Not found tipo dispositivo with name: " + nomeTipoDispositivo);
    }
    return tipoDispositivo;
  }

  private void persistAssociazioneDV(Set<Dispositivo> dispositivos,
                                     HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) throws Exception {
    Iterator<Dispositivo> iterator = dispositivos.iterator();
    while (iterator != null && iterator.hasNext()) {
      Dispositivo dispositivo = iterator.next();
      String identificativo = dispositivo.getIdentificativo();
      Set<String> keySet = mapDispositivoVeicolo.keySet();
      Iterator<String> keyIterator = keySet.iterator();
      while (keyIterator != null && keyIterator.hasNext()) {
        String key = keyIterator.next();
        List<Dispositivo> dispositivi = mapDispositivoVeicolo.get(key);
        for (Dispositivo disp : dispositivi) {
          if (identificativo == disp.getIdentificativo()) {
            AssociazioneDV assDv = new AssociazioneDV();
            assDv.setData(Instant.now());
            assDv.setDispositivo(disp);
            assDv.setUuidVeicolo(key);
            AssociazioneDV assDvUpdated = assDispVeicoloRepository.save(assDv);
            Dispositivo dispReload = dispositivoRepositoryExt.findOne(disp.getId());
            dispReload.addAssociazioneDispositivoVeicolo(assDvUpdated);
            save(dispReload);
          }
        }
      }

    }

  }

  @Override
  public List<AssociazioneDV> getAssociazioneDvByUuidVeicoloAndCodiceClienteFai(String uuid, String codiceAzienda) {
    List<AssociazioneDV> assDv = assDispVeicoloRepository.findByUuidVeicoloAndCodiceClienteFai(uuid, codiceAzienda);
    return assDv;
  }

  @Override
  public Dispositivo addServizioOnDispositivo(List<AssociazioneDV> listAssDispVeicolo, TipoDispositivo tipoDispositivo,
                                              VeicoloCarrelloDTO veicoloCarrelloDTO) {
    // TODO Add check to already exists TipoServizio request
    Iterator<AssociazioneDV> iterator = listAssDispVeicolo.iterator();
    while (iterator != null && iterator.hasNext()) {
      AssociazioneDV assDv = iterator.next();
      Dispositivo dispositivo = assDv.getDispositivo();
      if (dispositivo.getTipoDispositivo() == tipoDispositivo) {
        List<String> nomeTipiServizio = veicoloCarrelloDTO.getNomeTipiServizio();

        for (String nomeServizio : nomeTipiServizio) {

          TipoServizio tipoServizio = findByNomeServizio(nomeServizio);
          StatoDispositivoServizio sds = new StatoDispositivoServizio();
          sds.setStato(StatoDS.ATTIVO);
          sds.setTipoServizio(tipoServizio);
          sds.setDispositivo(dispositivo);

        }

        return dispositivo;
      }

    }

    return null;
  }

  @Override
  public Dispositivo createDispositivoDefault(TipoDispositivo tipoDispositivo) {
    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setTipoDispositivo(tipoDispositivo);
    dispositivo.setStato((isHgvDevice(tipoDispositivo.getNome())) ? StatoDispositivo.ANNULLATO : StatoDispositivo.INIZIALE);
    dispositivo.setSeriale(Dispositivo.NO_SERIALE_DISPOSITIVO);
    dispositivo.setDataModificaStato(Instant.now());
    return dispositivo;
  }

  @Override
  public boolean isHgvDevice(TipoDispositivo deviceType) {
    return isHgvDevice(deviceType.getNome());
  }

  private boolean isHgvDevice(TipoDispositivoEnum deviceType) {
    if (TipoDispositivoEnum.HGV.equals(deviceType))
      return true;
    return false;
  }

  @Override
  public Dispositivo createDispositivoServizi(TipoDispositivo tipoDispositivo, List<String> nomeTipiServizio,
                                              List<String> serviziAggiuntivi) throws Exception {
    Dispositivo dispositivo = createDispositivoDefault(tipoDispositivo);

    log.debug("Find tipoHardware by TipoDispositivo: {}", tipoDispositivo);
    List<TipoHardwareDispositivo> hardwareTypes = tipoHardwareService.findActiveByDeviceTypeId(tipoDispositivo.getId());
    setTipoHardwareDefault(dispositivo, hardwareTypes);

    Set<StatoDispositivoServizio> setSds = new HashSet<>();

    for (String nomeServizio : nomeTipiServizio) {

      TipoServizio tipoServizio = findByNomeServizio(nomeServizio);
      if (tipoServizio != null && tipoServizio.getTipoDispositivos() != null && (tipoServizio.getTipoDispositivos()
                                                                                             .contains(tipoDispositivo)
                                                                                 || serviziAggiuntivi.contains(nomeServizio))) {
        StatoDispositivoServizio sds = new StatoDispositivoServizio();
        sds.setStato((isHgvDevice(tipoDispositivo.getNome())) ? StatoDS.NON_ATTIVO : StatoDS.IN_ATTIVAZIONE);
        sds.setTipoServizio(tipoServizio);
        sds.setDispositivo(dispositivo);
        setSds.add(sds);
      } else {
        String nameServiceType = (tipoServizio != null) ? tipoServizio.getNome() : null;
        throw new Exception("Tipo Dispositivo " + tipoDispositivo + " non disponibile per il tipo Servizio [" + nameServiceType
                            + "] richiesto");
      }
    }

    dispositivo.setStatoDispositivoServizios(setSds);
    Set<StatoDispositivoServizio> statoDispositivoServizios = dispositivo.getStatoDispositivoServizios();
    sdsRepository.save(statoDispositivoServizios);

    dispositivo = save(dispositivo);
    return dispositivo;
  }

  private void setTipoHardwareDefault(Dispositivo dispositivo, List<TipoHardwareDispositivo> hardwareTypes) {
    if (hardwareTypes != null && !hardwareTypes.isEmpty()) {
      for (TipoHardwareDispositivo tipoHardwareDispositivo : hardwareTypes) {
        if (tipoHardwareDispositivo.isDefaultHardwareType()) {
          log.debug("Found default hardware type: {}", tipoHardwareDispositivo);
          dispositivo.setTipoHardware(tipoHardwareDispositivo.getHardwareType());
          break;
        }
      }
    } else {
      log.warn("Not found Hardware types");
    }
  }

  @Override
  public Dispositivo createDispositivoServizi(TipoDispositivo tipoDispositivo, List<String> nomeTipiServizio) throws Exception {
    return createDispositivoServizi(tipoDispositivo, nomeTipiServizio, new ArrayList<>());
  }

  @Override
  public TipoServizio findByNomeServizio(String nomeServizio) {
    return tipoServizioServiceExt.findByNome(nomeServizio);
  }

  @Override
  public Set<Dispositivo> cloneDispositivi(Set<Dispositivo> dispositivos, Richiesta richiesta, TipoDispositivo tipoDisp,
                                           String uuidVeicolo) {

    // TODO rivedere con qualcuno (Vins - Roberto - Marco)
    Set<Dispositivo> newDispositivos = new HashSet<>();
    for (Dispositivo dispositivo : dispositivos) {
      Dispositivo dispReloaded = dispositivoRepositoryExt.findOne(dispositivo.getId());
      log.info("Dispositivo to clone: id:=[{}], Identificativo:=[{}], Seriale:=[{}], Stato:=[{}], DataPrimaAttivazione:=[{}]",
               dispReloaded.getId(), dispReloaded.getIdentificativo(), dispReloaded.getSeriale(), dispReloaded.getStato(),
               dispReloaded.getDataPrimaAttivazione());

      Dispositivo dispClone = dispReloaded.cloneAndResetFileds();

      if (tipoDisp == null) {
        tipoDisp = dispReloaded.getTipoDispositivo();
      }

      Set<Richiesta> richiestas = new HashSet<>();
      richiestas.add(richiesta);
      String seriale = dispositivo.getSeriale();
      log.info("Set SerialeSostituito with old device serial {}", seriale);
      dispClone.setSerialeSostituito(seriale);
      dispClone.setRichiestas(richiestas);

      Set<AssociazioneDV> associazioneDispositivoVeicolos = dispReloaded.getAssociazioneDispositivoVeicolos();
      log.info("AssociazioneDV size: [" + associazioneDispositivoVeicolos.size() + "] ");
      Set<AssociazioneDV> assDvs = new HashSet<>();
      for (AssociazioneDV associazioneDV : associazioneDispositivoVeicolos) {
        log.info("AssociazioneDV cloned: [" + associazioneDV.getId() + "] ");
        AssociazioneDV newAssDv = cloneAssociazioneDV(associazioneDV, uuidVeicolo);
        newAssDv.setDispositivo(dispClone);
        assDvs.add(newAssDv);
      }
      List<AssociazioneDV> savedAssDvs = assDispVeicoloRepository.save(assDvs);
      assDvs = new HashSet<AssociazioneDV>(savedAssDvs);
      dispClone.setAssociazioneDispositivoVeicolos(assDvs);

      Set<StatoDispositivoServizio> statoDispositivoServizios = dispReloaded.getStatoDispositivoServizios();
      log.info("StatoDispositivoServizio size: [" + statoDispositivoServizios.size() + "] ");
      Set<StatoDispositivoServizio> sdsList = new HashSet<>();
      for (StatoDispositivoServizio statoDispositivoServizio : statoDispositivoServizios) {
        StatoDispositivoServizio sds = cloneStatoDispositivoServizio(statoDispositivoServizio);
        sds.setDispositivo(dispClone);
        sdsList.add(sds);
      }
      List<StatoDispositivoServizio> savedSds = sdsRepository.save(sdsList);
      sdsList = new HashSet<StatoDispositivoServizio>(savedSds);
      dispClone.setStatoDispositivoServizios(sdsList);

      // Non mantengo lo storico perchè è la richiesta di un nuovo dispositivo;
      dispClone.setStoricoAssociazioneDVs(new HashSet<>());
      newDispositivos.add(dispClone);
    }

    List<Dispositivo> dispositivi = save(newDispositivos);
    dispositivi.forEach(dispClone -> {
      log.info("Dispositivo cloned and persisted: id:=[{}], Identificativo:=[{}], Seriale:=[{}], Stato:=[{}], DataPrimaAttivazione:=[{}]",
               dispClone.getId(), dispClone.getIdentificativo(), dispClone.getSeriale(), dispClone.getStato(),
               dispClone.getDataPrimaAttivazione());
    });
    return new HashSet<>(dispositivi);
  }

  private AssociazioneDV cloneAssociazioneDV(AssociazioneDV associazioneDV, String uuidVeicolo) {
    AssociazioneDV newAssDv = new AssociazioneDV();
    BeanUtils.copyProperties(associazioneDV, newAssDv);
    newAssDv.setId(null);
    if (StringUtils.isNotBlank(uuidVeicolo)) {
      log.info("Assigned on new AssociazioneDV {} the vehicle [Identificativo: {}]", newAssDv, uuidVeicolo);
      newAssDv.setUuidVeicolo(uuidVeicolo);
    }
    newAssDv.setData(Instant.now());
    return newAssDv;
  }

  private StatoDispositivoServizio cloneStatoDispositivoServizio(StatoDispositivoServizio sds) {
    StatoDispositivoServizio newSds = new StatoDispositivoServizio();
    BeanUtils.copyProperties(sds, newSds);
    newSds.setId(null);

    StatoDS stato = newSds.getStato();
    switch (stato) {
    case NON_ATTIVO:
      stato = StatoDS.IN_ATTIVAZIONE;
      break;
    case ATTIVO:
      stato = StatoDS.IN_DISATTIVAZIONE;
      break;
    default:
      break;
    }

    newSds.setStato(stato);
    newSds.setPan(null);
    return newSds;
  }

  @Override
  public Dispositivo findOneByIdentificativo(String identificativo) {
    return dispositivoRepositoryExt.findOneByIdentificativo(identificativo);
  }

  public Set<Dispositivo> findByIdentificativo(List<String> identificativoDispositivi) {
    return dispositivoRepositoryExt.findByIdentificativo(identificativoDispositivi);
  }

  @Override
  public List<String> generateMapAndSendInQueue(List<String> identificativoDispositivi) throws Exception {
    final Set<Dispositivo> devices = findByIdentificativo(identificativoDispositivi);

    Set<String> devicesIdentificativo = new HashSet<>();
    Map<String, Set<String>> mapRichiestaDispositivi = devices.stream()
                                                              .map(device -> device)
                                                              .collect(toMap(k -> {
                                                                String uuid = k.getRichiestas()
                                                                               .stream()
                                                                               .findFirst()
                                                                               .get()
                                                                               .getIdentificativo();
                                                                return uuid;
                                                              }, v -> {
                                                                Set<String> deviceIds = new HashSet<>();
                                                                String identificativo = v.getIdentificativo();
                                                                devicesIdentificativo.add(identificativo);
                                                                deviceIds.add(identificativo);
                                                                return deviceIds;
                                                              }, (s, devicesIds) -> {
                                                                devicesIds.addAll(s);
                                                                return devicesIds;
                                                              }));

    DispositiviDaSpedireDTO dto = new DispositiviDaSpedireDTO(mapRichiestaDispositivi);
    jsmTopicSender.publishDispositiviDaSpedireMessage(dto);

    return new ArrayList<>(devicesIdentificativo);
  }

  @Override
  public AssociazioneDV associazioneDispositivoVeicolo(TipologiaAssociazione tipologiaAssociazione,
                                                       CarrelloModificaRichiestaDTO carrelloDTO) throws Exception {

    // FIXME if necessary insert switch to tipologia associazione {NOTARGA / SCORTA}

    // TODO check if necessary implements specific FSM to manage this case SCORTA/NOTARGA;

    AssociazioneDV assDv = null;
    List<String> dispositivi = carrelloDTO.getDispositivi();
    Set<Dispositivo> dispositivos = dispositivoRepositoryExt.findByIdentificativo(dispositivi);
    Optional<Dispositivo> dispositivoOpt = dispositivos != null ? dispositivos.stream()
                                                                              .findFirst()
                                                                : Optional.empty();
    if (dispositivoOpt.isPresent()) {
      String newVehicle = carrelloDTO.getNewVeicolo();
      Dispositivo dispositivo = dispositivoOpt.get();
      log.info("Manage associazione DispositivoVeicolo for Dispositivo [ Identificativo: {} ] and Veicolo [ Identificativo: {} ]",
               dispositivo.getIdentificativo(), newVehicle);
      Set<AssociazioneDV> associazioneDispositivoVeicolos = dispositivo.getAssociazioneDispositivoVeicolos();

      if (CollectionUtils.isEmpty(associazioneDispositivoVeicolos)) {
        assDv = new AssociazioneDV().dispositivo(dispositivo);
      } else if (associazioneDispositivoVeicolos.size() == 1) {
        assDv = associazioneDispositivoVeicolos.stream()
                                               .findFirst()
                                               .get();
        log.info("Il Veicolo [ Identificativo: {} ] verrà aggiornato per l'associazione DispositivoVeicolo [ ID: {} ]", newVehicle,
                 assDv.getId());
      } else {
        log.error("Il dispositivo {} possiede un numero di associazione DispositivoVeicolo è maggiore di 1");
        return null;
      }

      assDv.uuidVeicolo(newVehicle)
           .data(Instant.now());
      assDv = assDispVeicoloRepository.save(assDv);
      dispositivo.setTipoMagazzino(null);
      save(dispositivo);
    }

    return assDv;
  }

  @Override
  public Dispositivo disassociaDispositivo(String identificativoDispositivo) throws Exception {
    // FIXME : Raffinare, disassociando solo i veicoli di un determinato codice cliente
    // Fare lo stesso per il contratto;
    Dispositivo dispositivo = findOneByIdentificativo(identificativoDispositivo);
    Set<AssociazioneDV> associazioneDispositivoVeicolos = dispositivo.getAssociazioneDispositivoVeicolos();
    if (associazioneDispositivoVeicolos != null && !associazioneDispositivoVeicolos.isEmpty()) {
      for (AssociazioneDV assDv : associazioneDispositivoVeicolos) {
        String uuidVeicolo = assDv.getUuidVeicolo();
        assDv.setDispositivo(null);
        assDv.setUuidVeicolo(null);

        assDv = assDispVeicoloRepository.save(assDv);
        log.debug("De-Association Dispositivo to Vehicles");
        assDispVeicoloRepository.delete(assDv);
        log.debug("Removed association from vehicle {}", uuidVeicolo);
      }
    }

    Contratto contratto = dispositivo.getContratto();
    if (contratto != null) {
      dispositivo.setContratto(null);
      log.debug("Disassociazione Dispositivo-Contratto [ ID: {} - CodiceContratto: {} ]", contratto.getId(),
                contratto.getCodContrattoCliente());
    }
    dispositivo.setAssociazioneDispositivoVeicolos(new HashSet<>());
    dispositivo = save(dispositivo);
    log.debug("Removed all association Device Vehicle from Dispositivo {}", dispositivo);
    return dispositivo;
  }

  @Override
  public List<Dispositivo> getDevicesByCodiceAzienda(String codiceAzienda) {
    return dispositivoRepositoryExt.findAllByCodiceAzienda(codiceAzienda);
  }

  @Override
  public Page<SpedizioneClienteDispositivoDTO> getDevicesByCodiceAzienda(String codiceAzienda, Pageable page) {
    Page<Dispositivo> dispositiviPage = dispositivoRepositoryExt.findAllByCodiceAzienda(codiceAzienda, page);

    ValidazioneCliente validazioneCliente = anagaziendeClient.validaAzienda(jwt, Collections.singletonList(codiceAzienda))
                                                             .getValidazioni()
                                                             .stream()
                                                             .findFirst()
                                                             .orElse(new ValidazioneCliente());

    return dispositiviPage.map(dispositivo -> {
      SpedizioneClienteDispositivoDTO dispDTO = new SpedizioneClienteDispositivoDTO();

      dispositivo.getAssociazioneDispositivoVeicolos()
                 .stream()
                 .findFirst()
                 .ifPresent(assDv -> {
                   String uuidVeicolo = assDv.getUuidVeicolo();
                   log.debug("Find vehicle by identificativo: {}", uuidVeicolo);
                   WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVeicolo);
                   if (wizardVehicleEntity == null) {
                     return;
                   }
                   String targa = wizardVehicleEntity.getLicensePlate();
                   log.debug("Found License plate vehicle {}: {}", uuidVeicolo, targa);
                   dispDTO.setTarga(targa);
                 });

      dispDTO.setIdentificativo(dispositivo.getIdentificativo());
      Richiesta richiesta = dispositivo.getRichiestas()
                                       .stream()
                                       .filter(r -> r.getStato()
                                                     .equals(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI))
                                       .findFirst()
                                       .orElse(new Richiesta());
      dispDTO.setSeriale(dispositivo.getSeriale());
      dispDTO.setTipoDispositivo(dispositivo.getTipoDispositivo()
                                            .getNome()
                                            .name());
      dispDTO.setDataAccettazioneRichiesta(richiesta.getDataModificaStato());
      dispDTO.setDataRichiestaDispositivo(richiesta.getData());
      dispDTO.setAnomalieDispositivi(controlloAnomalieDispositivi(dispositivo.getId()));
      dispDTO.setAnomalieCRM(controlloAnomalieAnagrafiche(dispositivo, validazioneCliente));
      dispDTO.setForzaturaDispositivo(dispositiviDaSpedireCacheService.get(dispositivo.getIdentificativo()));
      return dispDTO;
    });
  }

  private List<PayloadError.Error> controlloAnomalieDispositivi(Long dispostivoId) {

    List<PayloadError.Error> anomalie = new ArrayList<>();

    List<String> uuids = associazioneDVRepository.findAllWithRichiestaStatoAndDispositivo(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI,
                                                                                          dispostivoId)
                                                 .stream()
                                                 .map(AssociazioneDV::getUuidVeicolo)
                                                 .collect(Collectors.toList());

    if (uuids != null && !uuids.isEmpty()) {
      List<VehicleDTO> issues = vehicleClient.findVehicleWithoutBooklet(jwt, uuids);
      if (issues != null && !issues.isEmpty()) {
        anomalie.add(PayloadError.buildError(Errno.DELIVERY_VEICOLO_SENZA_LIBRETTO));
      }
    }
    return anomalie;
  }

  private List<PayloadError.Error> controlloAnomalieAnagrafiche(Dispositivo dispositivo, ValidazioneCliente validazioneCliente) {

    List<PayloadError.Error> anomalie = new ArrayList<>();

    boolean issueTrackyCard = TipoDispositivoEnum.TRACKYCARD.equals(dispositivo.getTipoDispositivo()
                                                                               .getNome())
                              && BooleanUtils.isNotTrue(validazioneCliente.isFidoMensile());

    if (issueTrackyCard) {
      anomalie.add(PayloadError.buildError(Errno.CRM_INVALID_FIDO));
    }

    return anomalie;
  }

  @Override
  public Boolean hasVehicleActivedDevices(String codiceAzienda, String identificativoVehicle) {
    return dispositivoRepositoryExt.hasVehicleActivedDevices(codiceAzienda, identificativoVehicle, StatoDS.ATTIVO);
  }

  @Override
  public DispositivoDTO getDispositiviDTO(Dispositivo dispositivo) {
    return dispositivoMapper.toDto(dispositivo);
  }

  @Override
  public Set<Dispositivo> getDispositiviFilteredByAssociazioneDv(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo,
                                                                 String codiceCliente) {

    final TipoDispositivoEnum nomeTipoDispositivo = tipoDispositivo.getNome();

    Set<Dispositivo> dispositiviFilteredByTipo = associazioniDv.parallelStream()
                                                               .map(AssociazioneDV::getDispositivo)
                                                               .filter(d -> d.getTipoDispositivo()
                                                                             .getNome()
                                                                             .name()
                                                                             .equals(nomeTipoDispositivo.name()))
                                                               .collect(toSet());

    log.debug("Dispositivi filtrati per tipo dispositivo {}: {}", nomeTipoDispositivo, dispositiviFilteredByTipo);

    Set<Dispositivo> dispositiviFilteredByAttivi = dispositiviFilteredByTipo.parallelStream()
                                                                            .filter(d -> !d.getStato()
                                                                                           .isBlue())
                                                                            .collect(toSet());

    log.debug("Dispositivi filtrati per stato attivo: {}", dispositiviFilteredByAttivi);
    Set<Dispositivo> dispositiviFilter = dispositiviFilteredByAttivi.parallelStream()
                                                                    .filter(d -> d.getContratto() != null)
                                                                    .filter(d -> d.getContratto()
                                                                                  .getClienteFai() != null)
                                                                    .filter(d -> d.getContratto()
                                                                                  .getClienteFai()
                                                                                  .getCodiceCliente() != null)
                                                                    .filter(d -> d.getContratto()
                                                                                  .getClienteFai()
                                                                                  .getCodiceCliente()
                                                                                  .equalsIgnoreCase(codiceCliente))
                                                                    .collect(toSet());

    log.debug("Dispositivi filtered by codice cliente {}: {}", codiceCliente, dispositiviFilter);
    return dispositiviFilter;
  }

  @Override
  public HashMap<TipoRichiesta, Set<TipoServizio>> getServiziGestibiliDaRemoto(Set<TipoServizio> serviziDaDisattivare,
                                                                               Set<TipoServizio> serviziDaAttivare,
                                                                               Dispositivo dispositivo) throws Exception {
    HashMap<TipoRichiesta, Set<TipoServizio>> mapServiziDaAttivareDisattivare = null;
    Set<TipoServizio> serviziDaAttivareDisattivare = Stream.concat(serviziDaDisattivare.stream(), serviziDaAttivare.stream())
                                                           .collect(toSet());
    TipoDispositivoEnum nomeTipoDispositivo = dispositivo.getTipoDispositivo()
                                                         .getNome();
    boolean isRemoteServices = configurazioneDispositiviServiziService.isGestibiliDaRemoto(nomeTipoDispositivo,
                                                                                           serviziDaAttivareDisattivare.parallelStream()
                                                                                                                       .map(s -> s.getNome())
                                                                                                                       .collect(toList()));
    if (!isRemoteServices) {
      log.info("I seguenti servizi {} non sono tutti attivabili da remoto per il tipo dispositivo {}", serviziDaAttivareDisattivare,
               nomeTipoDispositivo);
      return mapServiziDaAttivareDisattivare;
    }

    mapServiziDaAttivareDisattivare = new HashMap<>();
    mapServiziDaAttivareDisattivare.put(TipoRichiesta.ATTIVAZIONE_SERVIZIO, serviziDaAttivare);
    mapServiziDaAttivareDisattivare.put(TipoRichiesta.DISATTIVAZIONE_SERVIZIO, serviziDaDisattivare);
    log.info("Servizi da Attivare/Disattivare {}", mapServiziDaAttivareDisattivare);
    return mapServiziDaAttivareDisattivare;
  }

  @Override
  public Set<Dispositivo> findDispositiviByCodiceContratto(String codiceContratto) {
    log.debug("Request findDispositiviByCodiceContratto : {}", codiceContratto);
    return dispositivoRepositoryExt.findDispositiviByCodiceContratto(codiceContratto);
  }

  @Override
  public List<DispositivoDTO> findByCodiceContratto(String codiceContratto) {
    log.debug("Request findByCodiceContratto : {}", codiceContratto);
    return dispositivoRepositoryExt.findDispositiviByCodiceContratto(codiceContratto)
                                   .stream()
                                   .map(dispositivoMapper::toDto)
                                   .collect(toList());
  }

  @Override
  public void associateContract(Set<Dispositivo> dispositivos, Contratto contract) {
    Set<Dispositivo> dispositiviAssociateContract = dispositivos.stream()
                                                                .map(d -> {
                                                                  try {
                                                                    return save(d.contratto(contract));
                                                                  } catch (Exception e) {
                                                                    log.error("Exception on associate Contract on Dispositivi:", e);
                                                                    throw new RuntimeException(e);
                                                                  }
                                                                })
                                                                .collect(toSet());
    log.debug("Dispositivi a cui è stato associato il contratto [{}] : {}", contract, dispositiviAssociateContract);
  }

  @Override
  public Dispositivo findDispositivoByAssociazioneDvTypeAndCliente(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo,
                                                                   String codiceCliente) throws Exception {
    Set<Dispositivo> dispositiviFiltered = getDispositiviFiltered(associazioniDv, tipoDispositivo, codiceCliente);
    if (dispositiviFiltered == null) {
      return null;
    }

    Dispositivo dispositivo = checkDispositivoManaged(dispositiviFiltered);
    return dispositivo;
  }

  @Override
  public boolean isServiziGestibili(TipoDispositivoEnum tipoDispositivo, Set<TipoServizio> serviziDaAttivareDisattivare) {
    log.info("Check if all service {} manageable for device type {}", serviziDaAttivareDisattivare, tipoDispositivo);
    boolean isServicesManage = configurazioneDispositiviServiziService.isGestibili(tipoDispositivo,
                                                                                   serviziDaAttivareDisattivare.parallelStream()
                                                                                                               .map(s -> s.getNome())
                                                                                                               .collect(toList()));

    return isServicesManage;
  }

  @Override
  public boolean isServiziGestibiliDaRemoto(TipoDispositivoEnum tipoDispositivo,
                                            Set<TipoServizio> serviziDaAttivareDisattivare) throws Exception {
    boolean isServicesRemoteManage = configurazioneDispositiviServiziService.isGestibiliDaRemoto(tipoDispositivo,
                                                                                                 serviziDaAttivareDisattivare.parallelStream()
                                                                                                                             .map(s -> s.getNome())
                                                                                                                             .collect(toList()));
    return isServicesRemoteManage;
  }

  @Override
  public boolean isServiziGestibiliOffline(TipoDispositivoEnum tipoDispositivo,
                                           Set<TipoServizio> serviziDaAttivareDisattivare) throws Exception {
    boolean isServicesOfflineManage = configurazioneDispositiviServiziService.isGestibiliOffline(tipoDispositivo,
                                                                                                 serviziDaAttivareDisattivare.parallelStream()
                                                                                                                             .map(s -> s.getNome())
                                                                                                                             .collect(toList()));
    return isServicesOfflineManage;
  }

  @Override
  public void manageAttivazioneDisattivazioneServiziOffline(Set<TipoServizio> serviziDaDisattivare, Set<TipoServizio> serviziDaAttivare,
                                                            Dispositivo dispositivo) throws Exception {

    Set<StatoDispositivoServizio> sdss = dispositivo.getStatoDispositivoServizios();
    log.debug("Stato Dispositivo Servizio {} on Dispositivo {}", (sdss != null) ? sdss.size() : 0, dispositivo);

    serviziDaAttivare.stream()
                     .map(service -> {
                       StatoDispositivoServizio sds = new StatoDispositivoServizio().stato(StatoDS.IN_ATTIVAZIONE)
                                                                                    .dispositivo(dispositivo)
                                                                                    .tipoServizio(service);

                       sds = sdsRepository.save(sds);
                       log.info("Create new StatoDispositivoServizio {}", sds);
                       return sds;
                     })
                     .collect(toSet());

    serviziDaDisattivare.stream()
                        .map(service -> {
                          StatoDispositivoServizio sds = null;
                          Set<StatoDispositivoServizio> sdsFiltered = sdss.stream()
                                                                          .filter(s -> s.getTipoServizio()
                                                                                        .getId() == service.getId())
                                                                          .collect(toSet());
                          if (sdsFiltered != null && sdsFiltered.size() == 1) {
                            sds = sdsFiltered.stream()
                                             .findFirst()
                                             .get();
                            sds = sdsRepository.save(sds.stato(StatoDS.IN_DISATTIVAZIONE));
                            log.info("Update SDS {}", sds);
                          } else {
                            log.error("SDS Not found or size set is more then 1");
                          }
                          return sds;
                        })
                        .collect(toSet());

    log.warn("Per i servizi gestibili offline non viene creata la richiesta, ma solo modificato lo stato dispositivo servizio");
  }

  @Override
  public Set<TipoServizio> getServiziAttivi(Set<StatoDispositivoServizio> sdss, Dispositivo dispositivo) {
    Set<TipoServizio> serviziAttivi = sdss.parallelStream()
                                          .filter(sds -> sds.getStato()
                                                            .name() == StatoDS.ATTIVO.name())
                                          .filter(sds -> !isDeviceTrackyCard(dispositivo)
                                                         || (isDeviceTrackyCard(dispositivo) && sds.getTipoServizio() != null
                                                             && !sds.getTipoServizio()
                                                                    .getNome()
                                                                    .equalsIgnoreCase("PEDAGGI_AUSTRIA")))
                                          .map(sds -> sds.getTipoServizio())
                                          .collect(toSet());
    log.info("Servizi Attivi {} on Dispositivo {}", (serviziAttivi != null) ? serviziAttivi.size() : 0, dispositivo);
    return serviziAttivi;
  }

  private boolean isDeviceTrackyCard(Dispositivo device) {
    return device.getTipoDispositivo() != null && device.getTipoDispositivo()
                                                        .getNome()
                                                        .equals(TipoDispositivoEnum.TRACKYCARD);
  }

  @Override
  public Set<TipoServizio> getServiziDaAttivare(Set<TipoServizio> serviziAttivi, List<String> nomeTipiServizio) {
    List<TipoServizio> tipiServizi = tipoServizioServiceExt.findByNames(nomeTipiServizio);
    log.debug("*** DA ATTIVARE => Servizi Attivi: {} - TipiServizi: {}", serviziAttivi, tipiServizi);
    Set<TipoServizio> serviziDaAttivare = tipiServizi.stream()
                                                     .filter(s -> !serviziAttivi.contains(s))
                                                     .collect(toSet());

    log.info("Servizi da ATTIVARE {}", (serviziDaAttivare != null) ? serviziDaAttivare.size() : 0);
    return serviziDaAttivare;
  }

  @Override
  public Set<TipoServizio> getServiziDaDisattivare(Set<TipoServizio> serviziAttivi, List<String> nomeTipiServizio) {
    List<TipoServizio> tipiServizi = tipoServizioServiceExt.findByNames(nomeTipiServizio);
    log.debug("*** Da DISATTIVARE => Servizi Attivi: {} - TipiServizi: {}", serviziAttivi, tipiServizi);
    Set<TipoServizio> serviziDaDisattivare = serviziAttivi.stream()
                                                          .filter(sa -> !tipiServizi.contains(sa))
                                                          .collect(toSet());

    log.info("Servizi da DISATTIVARE {}", (serviziDaDisattivare != null) ? serviziDaDisattivare.size() : 0);
    return serviziDaDisattivare;
  }

  private Dispositivo checkDispositivoManaged(Set<Dispositivo> dispositiviFiltered) throws Exception {
    Dispositivo dispositivo = dispositiviFiltered.stream()
                                                 .findFirst()
                                                 .get();
    log.info("Dispositivo {}", dispositivo);
    if (dispositivo.getStato()
                   .isYellow()) {
      throw new Exception("Unable to perform operations on a device is status pending");
    }
    return dispositivo;
  }

  private Set<Dispositivo> getDispositiviFiltered(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo,
                                                  String codiceCliente) throws Exception {
    if (associazioniDv == null || (associazioniDv != null && associazioniDv.isEmpty())) {
      log.debug("AssociazioneDVs is NULL or EMPTY");
      return null;
    }

    Set<Dispositivo> dispositiviFiltered = getDispositiviFilteredByAssociazioneDv(associazioniDv, tipoDispositivo, codiceCliente);
    if (dispositiviFiltered == null || (dispositiviFiltered != null && dispositiviFiltered.isEmpty())) {
      log.debug("Not found Dispositivo for AssociazioneDV {} , tipoDispositivo {}, codice cliente {}: {}", associazioniDv, tipoDispositivo,
                codiceCliente, dispositiviFiltered);
      return null;
    }

    if (dispositiviFiltered.size() > 1) {
      throw new Exception("Impossible have found more than 1 device");
    }

    return dispositiviFiltered;
  }

  @Override
  public void setStatoDispositivoServizio(Dispositivo dispositivo, TipoServizio tipoServizio, TipoRichiesta tipoRichiesta) {
    Set<StatoDispositivoServizio> statoDispositivoServizios = dispositivo.getStatoDispositivoServizios();
    String identificativo = dispositivo.getIdentificativo();
    String seriale = dispositivo.getSeriale();
    String nomeServizio = tipoServizio.getNome();
    log.info("Sds for dispositivo [identificativo: {} - Seriale: {}] and Servizio [{}]: {}", identificativo, seriale, tipoServizio,
             statoDispositivoServizios);

    Set<StatoDispositivoServizio> savedSds = statoDispositivoServizios.stream()
                                                                      .filter(sds -> sds.getTipoServizio() != null && sds.getTipoServizio()
                                                                                                                         .getNome()
                                                                                                                         .equalsIgnoreCase(tipoServizio.getNome()))
                                                                      .map(sds -> {
                                                                        log.info("Update stato on Sds {}", sds);
                                                                        List<StatoDispositivoServizio> sdsFound = statoDispositivoServizioRepositoryExt.findByDispositivoUuidAndTipoServizio(identificativo,
                                                                                                                                                                                             nomeServizio);
                                                                        log.info("Reload sds: {}", sdsFound);
                                                                        if (sdsFound != null && !sdsFound.isEmpty()) {
                                                                          sds = sdsFound.get(0);
                                                                        }
                                                                        return updateDeviceServiceStatus(sds, tipoRichiesta);
                                                                      })
                                                                      .collect(toSet());

    if (savedSds == null || savedSds.isEmpty()) {
      log.info("Create new Stato Dispositivo Servizio, because it doesn't exists on Device: {}", dispositivo);
      StatoDispositivoServizio sds = new StatoDispositivoServizio().tipoServizio(tipoServizio)
                                                                   .dispositivo(dispositivo);
      sds = updateDeviceServiceStatus(sds, tipoRichiesta);
      dispositivo.addStatoDispositivoServizio(sds);
      dispositivoRepositoryExt.save(dispositivo);

      savedSds = new HashSet<>();
      savedSds.add(sds);
    }

    log.info("Saved Stato Dispositivo Servizio: {}", savedSds);
  }

  @Override
  public Dispositivo save(Dispositivo dispositivo) {
    dispositivo = fsmDispositivo.setOperazioniPossibiliDispositivo(dispositivo);
    Dispositivo dispositivoSaved = dispositivoRepositoryExt.save(dispositivo);
    return dispositivoSaved;
  }

  @Override
  public Dispositivo saveWithChild(Dispositivo dispositivo, Set<AssociazioneDV> associazioneDispositivoVeicolos,
                                   Set<StatoDispositivoServizio> statoDispositivoServizios) throws Exception {
    associazioneDVRepository.save(associazioneDispositivoVeicolos);
    statoDispositivoServizioRepositoryExt.save(statoDispositivoServizios);
    return dispositivoRepositoryExt.save(dispositivo);
  }

  @Override
  public Dispositivo saveAndFlush(Dispositivo dispositivo) throws Exception {
    dispositivo = fsmDispositivo.setOperazioniPossibiliDispositivo(dispositivo);
    return dispositivoRepositoryExt.saveAndFlush(dispositivo);
  }

  @Override
  public List<Dispositivo> save(List<Dispositivo> dispositivi, boolean cascade) {
    dispositivi = setOperazioniPossibiliOnDispositivi(dispositivi);
    List<Dispositivo> dispositiviSaved = dispositivoRepositoryExt.save(dispositivi);

    Stream<Dispositivo> stream = dispositiviSaved.stream();
    if (cascade) {
      stream = stream.map(d -> {
        associazioneDVRepository.save(d.getAssociazioneDispositivoVeicolos());
        statoDispositivoServizioRepositoryExt.save(d.getStatoDispositivoServizios());
        return d;
      });
    }

    stream.map(d -> {
      log.debug("Saved Dispositivo [Identificativo: {} - Tipo: {} - Seriale: {}]", d.getIdentificativo(), d.getTipoDispositivo(),
                d.getSeriale());
      return d;
    })
          .collect(toSet());
    return dispositiviSaved;
  }

  @Override
  public List<Dispositivo> saveAndFlush(List<Dispositivo> dispositivi) {
    dispositivi = setOperazioniPossibiliOnDispositivi(dispositivi);
    Set<Dispositivo> dispositiviSaved = dispositivi.stream()
                                                   .map(d -> {
                                                     d = dispositivoRepositoryExt.saveAndFlush(d);
                                                     log.debug("Saved Dispositivo [Identificativo: {} - Tipo: {} - Seriale: {}]",
                                                               d.getIdentificativo(), d.getTipoDispositivo(), d.getSeriale());
                                                     return d;
                                                   })
                                                   .collect(toSet());
    List<Dispositivo> devicesSaved = new ArrayList<>();
    devicesSaved.addAll(dispositiviSaved);
    return devicesSaved;
  }

  private StatoDispositivoServizio updateDeviceServiceStatus(StatoDispositivoServizio sds, TipoRichiesta tipoRichiesta) {
    StatoDS stato = null;
    switch (tipoRichiesta) {
    case ATTIVAZIONE_SERVIZIO:
      stato = StatoDS.IN_ATTIVAZIONE;
      break;

    case DISATTIVAZIONE_SERVIZIO:
      stato = StatoDS.IN_DISATTIVAZIONE;
      break;

    default:
      log.warn("Tipo Richiesta per modifica stato SDS non possibile: {}", tipoRichiesta);
      break;
    }

    if (stato != null) {
      sds = sdsRepository.save(sds.stato(stato));
    }
    return sds;
  }

  private List<Dispositivo> save(Set<Dispositivo> devices) {
    List<Dispositivo> list = new ArrayList<>();
    list.addAll(devices);

    list = setOperazioniPossibiliOnDispositivi(list);
    return save(list, false);
  }

  private void saveAndFlush(Set<Dispositivo> dispositivos) {
    List<Dispositivo> list = new ArrayList<>();
    list.addAll(dispositivos);

    list = setOperazioniPossibiliOnDispositivi(list);
    saveAndFlush(list);
  }

  private List<Dispositivo> setOperazioniPossibiliOnDispositivi(List<Dispositivo> dispositivi) {
    return dispositivi.stream()
                      .map(d -> {
                        try {
                          return fsmDispositivo.setOperazioniPossibiliDispositivo(d);
                        } catch (Exception e) {
                          log.error("Exception:", e);
                          throw new RuntimeException(e);
                        }
                      })
                      .collect(toList());
  }

  @Override
  public boolean executeFsm(Set<String> identificativoDispositivi, String operazione, String nota) {

    identificativoDispositivi.stream()
                             .map(identifier -> {
                               Dispositivo device = findOneByIdentificativo(identifier);
                               log.debug("Found device {} for execute FSM...", device);
                               DispositivoEvent command = DispositivoEvent.valueOf(operazione);
                               log.debug("Command execute on device: {}", command);
                               try {
                                 device = fsmDispositivo.executeCommandToChangeState(command, device, nota);
                                 device = save(device);
                               } catch (Exception e) {
                                 log.error("Exception on change status FSM Dispositivo", e);
                                 throw new RuntimeException(e);
                               }
                               log.debug("Changed status on device: {}", device);
                               return device;
                             })
                             .collect(toSet());

    return true;
  }

  @Override
  public Set<Dispositivo> findDispositiviByCodiceClienteAndTipo(String codiceCliente, TipoDispositivoEnum tipoDispositivo) {
    return dispositivoRepositoryExt.findByCodiceClienteAndTipoDispositivo(codiceCliente, tipoDispositivo);
  }

  @Override
  public int countDispositiviAttiviByCodiceClienteAndTipo(String codiceCliente, TipoDispositivoEnum tipoDispositivo) {
    Set<Dispositivo> deviceActive = null;
    Set<Dispositivo> dispositiviFound = findDispositiviByCodiceClienteAndTipo(codiceCliente, tipoDispositivo);
    if (dispositiviFound != null) {
      deviceActive = dispositiviFound.stream()
                                     .filter(d -> d.getStato()
                                                   .isGreen())
                                     .collect(toSet());
    }
    return (deviceActive != null) ? deviceActive.size() : 0;
  }

  @Override
  public Set<Dispositivo> findDispositiviByStateAndType(StatoDispositivo stato, TipoDispositivoEnum tipoDispositivo) {
    Set<Dispositivo> devices = dispositivoRepositoryExt.findDispositiviByStateAndTipo(StatoDispositivo.IN_DEPOSITO, tipoDispositivo);
    log.info("Found Dispotivi [type: {}] {}: ", tipoDispositivo, stato, (devices != null) ? devices.size() : 0);
    return devices;
  }

  @Override
  public Dispositivo findOneForUpdateByStateAndType(StatoDispositivo stato, TipoDispositivoEnum tipoDispositivo) {
    Optional<Dispositivo> deviceOpt = dispositivoRepositoryExt.findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo.IN_DEPOSITO,
                                                                                                      tipoDispositivo);
    Dispositivo device = deviceOpt.isPresent() ? deviceOpt.get() : null;
    log.info("Device FOR UPDATE found: {}", device);
    return device;
  }

  @Override
  public Optional<Dispositivo> findByCodiceContrattoAndSeriale(String contractCode, String seriale) {
    log.debug("Find device with contract number : {} and serial number : {}");
    Optional<Dispositivo> dispositivoOpt = dispositivoRepositoryExt.findByCodiceContrattoAndSeriale(contractCode, seriale);
    return dispositivoOpt;
  }

  @Override
  public StatoDispositivoServizio updateStatoDeviceService(StatoDispositivoServizio sds, StatoDS stato) {
    log.info("Update new Stato {} on DeviceService {}", stato, sds);
    StatoDispositivoServizio sdsSaved = sdsRepository.save(sds.stato(stato));
    log.info("StatoDispositivoServizio saved {}", sdsSaved);
    return sdsSaved;
  }

  @Override
  public List<DispositivoDTO> findDispositiviByCodiceContrattoAndSeriale(String codiceContratto, String seriale) {
    log.debug("Find devices by contract number : {} and serial number : {}", codiceContratto, seriale);
    List<Dispositivo> list = this.dispositivoRepositoryExt.findDispositiviByCodiceContrattoAndSeriale(codiceContratto, seriale);

    return list.stream()
               .map(this::mapperWithFileAllegato)
               .collect(Collectors.toCollection(LinkedList::new));
  }

  @Override
  public List<DispositivoDTO> findDispositiviByCodiceContrattoAndVeicolo(String codiceContratto, String uuidVeicolo) {

    List<Dispositivo> list = this.dispositivoRepositoryExt.findDispositiviByCodiceContrattoAndVeicolo(codiceContratto, uuidVeicolo);

    return list.stream()
               .map(this::mapperWithFileAllegato)
               .collect(Collectors.toCollection(LinkedList::new));
  }

  @Override
  public DispositivoDTO mapperWithFileAllegato(Dispositivo src) {
    DispositivoDTO dto = dispositivoMapper.toDto(src);
    dto.setOperazionePending(src.getStato()
                                .isYellow());

    List<AssociazioneDV> associazioneDVs = associazioneDVRepository.findByDispositivoId(src.getId());
    if (associazioneDVs != null) {
      dto.setAssociazioneDVs(associazioneDVs.stream()
                                            .map(associazioneDVMapper::toDto)
                                            .collect(toSet()));
    }

    src.getRichiestas()
       .stream()
       .filter(richiesta -> TipoRichiesta.TIPORICHIESTE_MODIFICA.contains(richiesta.getTipo()))
       .filter(richiesta -> !StatoRichiestaUtil.getStatoRichiestaConcluso()
                                               .contains(richiesta.getStato()))
       .findFirst()
       .ifPresent(richiesta -> {
         dto.setIdOrdineOperazioneModifica(richiesta.getId());
         dto.setTipoOperazioneModifica(richiesta.getTipo());
         Optional.ofNullable(richiesta.getUuidDocumento())
                 .ifPresent(uuidDoc -> dto.setFileAllegato(!uuidDoc.isEmpty()));
       });

    return dto;
  }

  @Override
  public String modificaDispositivoByBO(ModificaDispositivoDTO modificaDispositivoDTO) throws Exception {
    log.debug("Modifica dispositivo from BackOffice by DTO: {}", modificaDispositivoDTO);
    String identificativoDispositivo = modificaDispositivoDTO.getDmlUniqueIdentifier();
    Dispositivo dispositivo = findOneByIdentificativo(identificativoDispositivo);

    if (dispositivo == null) {
      log.error("Not found dispositivo by identificativo: {}", identificativoDispositivo);
      return null;
    }

    StatoDispositivo stato = modificaDispositivoDTO.getStato();
    Instant dataVariazioneStato = modificaDispositivoDTO.getDataUltimaModificaStato();
    Instant dataSpedizione = modificaDispositivoDTO.getDataSpedizione();
    String note = modificaDispositivoDTO.getNote();
    String tipoHardware = modificaDispositivoDTO.getTipoHardware();
    Instant dataPrimaAttivazione = modificaDispositivoDTO.getDataPrimaAttivazione();

    StringBuilder sb = new StringBuilder("Il dispositivo [" + identificativoDispositivo + "] è stato aggiornato con ");
    if (stato != null) {
      sb.append("stato [" + stato + "] ");
      log.debug("Set STATO [ {} ] on Dispositivo {}", stato, identificativoDispositivo);
      dispositivo.setStato(stato);
    }

    if (dataVariazioneStato != null) {
      sb.append("dataUltimaModificaStato [" + dataVariazioneStato + "] ");
      log.debug("Set DATA VARIAZIONE [ {} ] on Dispositivo {}", dataVariazioneStato, identificativoDispositivo);
      dispositivo.setDataModificaStato(dataVariazioneStato);
    }

    if (dataSpedizione != null) {
      sb.append("dataSpedizione [" + dataSpedizione + "] ");
      log.debug("Set DATA SPEDIZIONE [ {} ] on Dispositivo {}", dataSpedizione, identificativoDispositivo);
      dispositivo.setDataSpedizione(dataSpedizione);
    }

    if (StringUtils.isNotBlank(note)) {
      sb.append("note [" + note + "] ");
      log.debug("Set NOTE OPERATORE [ {} ] on Dispositivo {}", note, identificativoDispositivo);
      dispositivo.setNoteOperatore(note);
    }

    if (StringUtils.isBlank(tipoHardware)) {
      log.warn("Not set tipoHardware for device: {}", identificativoDispositivo);
    } else {
      log.debug("Tipo Hardware selected: {} for device: {}", tipoHardware, identificativoDispositivo);
      dispositivo.setTipoHardware(tipoHardware);
    }

    if (dataPrimaAttivazione != null) {
      dispositivo.setDataPrimaAttivazione(dataPrimaAttivazione);
      log.debug("Set DataPrimaAttivazione: {} for device: {}", dataPrimaAttivazione, identificativoDispositivo);
    }

    try {
      dispositivo = save(dispositivo);
    } catch (Exception e) {
      log.error("Exception ", e);
      throw new RuntimeException(e);
    }

    log.info(sb.toString());
    return identificativoDispositivo;
  }

  @Override
  @Transactional
  public void creaDispositivi(CreaDispositiviDTO dto, Contratto contratto, TipoDispositivo tipoDispositivo) throws CustomException {
    tipoDispositivo = tipoDispositivoServiceExt.findOneByNome(dto.getTipoDispositivo());

    String tipoHardware = dto.getHardwareType();
    log.info("Creation device {} with HardwareType: {}", tipoDispositivo, tipoHardware);

    List<Dispositivo> listToSave = new ArrayList<>();

    List<VehicleDTO> vehicles = vehicleClient.findAllByCodiceAzienda(jwt, contratto.getClienteFai()
                                                                                   .getCodiceCliente());
    for (CreaDispositiviDTO.CreaDispositivo disp : dto.getDispositivi()) {
      log.debug("Crea Dispositivi from DTO: {}", dto);
      List<Dispositivo> list = dispositivoRepositoryExt.findBySerialeAndTipoDispositivo(disp.getSeriale(), tipoDispositivo);
      if (list.size() > 0)
        throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                             .add(Errno.VALIDATION_FAILED);

      Dispositivo entity = new Dispositivo();
      if (disp.getUuidVeicolo() != null && !disp.getUuidVeicolo()
                                                .isEmpty()) {
        if (!isValidVehicle(vehicles, disp.getUuidVeicolo())) {
          throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                               .add(Errno.INVALID_VEHICLE);
        }
        AssociazioneDV dv = new AssociazioneDV();
        dv.setData(disp.getDataAssociazioneTarga() != null ? disp.getDataAssociazioneTarga() : Instant.now());
        dv.setDispositivo(entity);
        dv.setUuidVeicolo(disp.getUuidVeicolo());
        entity.setAssociazioneDispositivoVeicolos(new HashSet<>());
        entity.getAssociazioneDispositivoVeicolos()
              .add(dv);
      } else {
        entity.setTipoMagazzino(TipoMagazzino.SCORTA);
      }

      if (disp.getServizi() != null && !disp.getServizi()
                                            .isEmpty()) {
        entity.setStatoDispositivoServizios(new HashSet<>(disp.getServizi()
                                                              .size()));
        disp.getServizi()
            .forEach(s -> {
              StatoDispositivoServizio ss = new StatoDispositivoServizio();
              ss.dataAttivazione(s.getDataAttivazione());
              ss.setDispositivo(entity);
              ss.setStato(StatoDS.ATTIVO);
              ss.setPan(s.getPan());
              TipoServizio tipoServizio = tipoServizioServiceExt.findByNome(s.getTipoServizio()
                                                                             .name());
              ss.setTipoServizio(tipoServizio);

              entity.addStatoDispositivoServizio(ss);
            });
      }

      entity.setContratto(contratto);
      entity.setDataModificaStato(disp.getDataUltimaModificaStato());
      // ent.setDataPrimaAttivazione(Instant.now());
      entity.setDataSpedizione(disp.getDataSpedizione());
      entity.setNoteTecniche(disp.getNote());
      entity.setSeriale(disp.getSeriale());
      entity.setStato(disp.getStato());
      entity.setTipoDispositivo(tipoDispositivo);
      entity.setTipoHardware(tipoHardware);

      Instant dataPrimaAttivazione = disp.getDataPrimaAttivazione();
      if (dataPrimaAttivazione != null) {
        entity.setDataPrimaAttivazione(disp.getDataPrimaAttivazione());
      } else {
        log.warn("Not set data Prima Attivazione on DTO: {}", disp);
      }

      listToSave.add(entity);
    }

    if (!listToSave.isEmpty())
      this.save(listToSave, true);
  }

  private boolean isValidVehicle(List<VehicleDTO> vehicles, String uuidVeicolo) {
    return vehicles.stream()
                   .anyMatch(v -> v.getIdentificativo()
                                   .equals(uuidVeicolo));
  }

  @Override
  public Optional<Dispositivo> findViaCardByIdentificativoVeicolo(String identificativoVeicolo) {
    Optional<Dispositivo> viaCardOpt = dispositivoRepositoryExt.findByTipoDispositivoAndIdentificativoVeicolo(identificativoVeicolo,
                                                                                                              TipoDispositivoEnum.VIACARD);
    return viaCardOpt;
  }

  @Override
  public Dispositivo createViaCardOnScorta() throws Exception {
    Dispositivo dispositivoCreated = createViaCardDefault();
    dispositivoCreated = dispositivoRepositoryExt.save(dispositivoCreated.tipoMagazzino(TipoMagazzino.SCORTA));
    log.info("Set dispositivo {}: {}", dispositivoCreated.getIdentificativo(), dispositivoCreated.getTipoMagazzino());
    return dispositivoCreated;
  }

  @Override
  public Dispositivo createViaCardOnVehicle(String identificativoVeicolo) throws Exception {
    Dispositivo dispositivoCreated = createViaCardDefault();
    AssociazioneDV adv = new AssociazioneDV().data(Instant.now())
                                             .dispositivo(dispositivoCreated)
                                             .uuidVeicolo(identificativoVeicolo);
    adv = assDispVeicoloRepository.save(adv);
    log.info("Save associazione Dispositivo Veicolo {}", adv);

    dispositivoCreated.addAssociazioneDispositivoVeicolo(adv);
    dispositivoCreated = dispositivoRepositoryExt.save(dispositivoCreated);
    log.info("Saved relation between Entity: Dispositivo and Associazione_DV");
    return dispositivoCreated;
  }

  private Dispositivo createViaCardDefault() throws Exception {
    VeicoloCarrelloDTO dto = new VeicoloCarrelloDTO();
    List<String> servizi = new ArrayList<>();
    servizi.add(SERVIZIO_DEFAULT_VIA_CARD);
    dto.setNomeTipiServizio(servizi);
    TipoDispositivo tipoDispositivo = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.VIACARD);
    Dispositivo dispositivoCreated = createDispositivoServizi(tipoDispositivo, dto.getNomeTipiServizio());
    log.info("Created dispositivo {} with StatoDispositivoServizio [{}]", dispositivoCreated, SERVIZIO_DEFAULT_VIA_CARD);
    return dispositivoCreated;
  }

  @Override
  public Optional<Dispositivo> findByTipoDispositivoAndSeriale(TipoDispositivoEnum tipoDispositivo, String seriale) {
    Optional<Dispositivo> dispositivoOpt = dispositivoRepositoryExt.findFirstByTipoDispositivo_nomeAndSeriale(tipoDispositivo, seriale);
    log.info("Found by Seriale {}: {}", seriale, dispositivoOpt);
    return dispositivoOpt;
  }

  @Override
  public Optional<Dispositivo> findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo stato, TipoDispositivoEnum tipoDispositivo) {
    return dispositivoRepositoryExt.findTop1ByStatoAndTipoDispositivo_nome(stato, tipoDispositivo);
  }

  @Override
  public void delete(Dispositivo device) {
    dispositivoRepositoryExt.delete(device);
  }

  @Override
  public void flush() {
    dispositivoRepositoryExt.flush();
  }

  @Override
  public void removeStatoDispositivoServizios(Dispositivo device) {
    Set<StatoDispositivoServizio> statoDispositivoServizios = device.getStatoDispositivoServizios();
    log.info("Find SdS: {}", statoDispositivoServizios != null ? statoDispositivoServizios.size() : 0);

    if (statoDispositivoServizios == null || statoDispositivoServizios.isEmpty()) {
      log.warn("Not remove Stato dispositivo servizio related on Device: {}", device);
      return;
    }

    List<StatoDispositivoServizio> sdss = statoDispositivoServizios.stream()
                                                                   .map(sds -> sds.dispositivo(null))
                                                                   .collect(toList());
    if (sdss != null) {
      sdsRepository.save(sdss);
    } else {
      log.warn("Not remove Stato dispositivo servizio related on Device: {}", device);
    }
  }

  @Override
  public void removeAssociazioneDvs(Dispositivo device) {
    Set<AssociazioneDV> associazioneDispositivoVeicolos = device.getAssociazioneDispositivoVeicolos();
    log.info("Find Associazione Dvs: {}", associazioneDispositivoVeicolos != null ? associazioneDispositivoVeicolos.size() : 0);

    if (associazioneDispositivoVeicolos == null || associazioneDispositivoVeicolos.isEmpty()) {
      log.warn("Not remove Associaizone Dvs related on Device: {}", device);
      return;
    }

    List<AssociazioneDV> asdvs = associazioneDispositivoVeicolos.stream()
                                                                .map(asdv -> asdv.dispositivo(null))
                                                                .collect(toList());
    if (asdvs != null) {
      assDispVeicoloRepository.save(asdvs);
    } else {
      log.warn("Not remove Associaizone Dvs related on Device: {}", device);
    }
  }

  @Override
  public void updateAssociazioneDv(AssociazioneDV associazioneDV) {
    assDispVeicoloRepository.saveAndFlush(associazioneDV);
  }

  @Override
  public WizardVehicleEntity findVehicleBySerialeDispositivo(String dispositivoSeriale) {
    WizardVehicleEntity vehicleEntity = null;
    Set<StatoDispositivo> statesIn = new HashSet<>();
    statesIn.add(StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE);
    Optional<Dispositivo> optDevice = dispositivoRepositoryExt.findBySerialeAndStatoIn(dispositivoSeriale, statesIn);
    log.info("Found optDevice by seriale {} and GreenStatus: {}", dispositivoSeriale, optDevice);
    if (optDevice.isPresent()) {
      Dispositivo device = optDevice.get();
      Set<AssociazioneDV> associazioneDispositivoVeicolos = device.getAssociazioneDispositivoVeicolos();
      log.debug("Found associazioniDV: {}", (associazioneDispositivoVeicolos != null) ? associazioneDispositivoVeicolos.size() : 0);
      Optional<AssociazioneDV> optAssDv = associazioneDispositivoVeicolos.stream()
                                                                         .findFirst();
      log.info("Found opt AssociazioneDV: {}", optAssDv);
      if (optAssDv.isPresent()) {
        AssociazioneDV associazioneDV = optAssDv.get();
        String uuidVeicolo = associazioneDV.getUuidVeicolo();
        log.info("Search UUID Vehicle {} in cache", uuidVeicolo);
        vehicleEntity = cacheVehicleWizard.get(uuidVeicolo);
        log.info("Found vehicle in cache: {}", vehicleEntity);
      }
    }

    log.info("Founded vehicle in cache: {}", vehicleEntity);
    return vehicleEntity;
  }

  @Override
  public Page<SpedizioneFornitoreDispositivoDTO> getDevicesByTipiDispositivo(List<TipoDispositivoEnum> tipiDispositivo, Pageable page) {
    StatoDispositivo stato = StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE;
    return dispositivoRepositoryExt.findAllByTipoDispositivo_nomeAndStato(tipiDispositivo, stato, page)
                                   .map(dispositivo -> {
                                     SpedizioneFornitoreDispositivoDTO dispDTO = new SpedizioneFornitoreDispositivoDTO();
                                     dispositivo.getAssociazioneDispositivoVeicolos()
                                                .stream()
                                                .findFirst()
                                                .ifPresent(assDv -> {
                                                  String uuidVeicolo = assDv.getUuidVeicolo();
                                                  log.debug("Find vehicle by identificativo: {}", uuidVeicolo);
                                                  WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVeicolo);
                                                  if (wizardVehicleEntity == null) {
                                                    return;
                                                  }
                                                  String targa = wizardVehicleEntity.getLicensePlate();
                                                  log.debug("Found License plate vehicle {}: {}", uuidVeicolo, targa);
                                                  dispDTO.setTarga(targa);
                                                });

                                     dispDTO.setIdentificativo(dispositivo.getIdentificativo());
                                     Richiesta richiesta = dispositivo.getRichiestas()
                                                                      .stream()
                                                                      .filter(r -> r.getStato()
                                                                                    .equals(StatoRichiesta.DISPOSITIVO_DISATTIVATO))
                                                                      .findFirst()
                                                                      .orElse(new Richiesta());
                                     dispDTO.setSeriale(dispositivo.getSeriale());
                                     dispDTO.setTipoDispositivo(dispositivo.getTipoDispositivo()
                                                                           .getNome()
                                                                           .name());
                                     dispDTO.setDataAccettazioneRichiesta(richiesta.getDataModificaStato());
                                     dispDTO.setDataRichiestaDispositivo(richiesta.getData());

                                     log.debug("Generate {}", dispDTO);
                                     return dispDTO;
                                   });
  }

  @Override
  public Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(String vehicleUuid,
                                                                                                   TipoDispositivoEnum tipoDispositivo) {
    return dispositivoRepositoryExt.findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(vehicleUuid, tipoDispositivo);
  }

  @Override
  public Optional<Dispositivo> findActiveDeviceByTypeAndLicensePlateAndCountry(TipoDispositivoEnum tipoDispositivo,
                                                                               Set<StatoDispositivo> statiDispositiviAttivi, String targa,
                                                                               String nazione, String codiceCliente) {

    Set<Dispositivo> devices = findDispositiviByCodiceClienteAndTipo(codiceCliente, tipoDispositivo);
    log.info("Devices found by [{} - {}]: {}", codiceCliente, tipoDispositivo, devices);
    Set<Dispositivo> devicesActived = devices.stream()
                                             .filter(d -> statiDispositiviAttivi.contains(d.getStato()))
                                             .collect(toSet());
    log.info("DevicesActived found by : {}", devicesActived);

    Dispositivo dispositivo = null;
    for (Dispositivo device : devicesActived) {
      Set<AssociazioneDV> asdv = device.getAssociazioneDispositivoVeicolos();
      log.info("Associzioni DV found by Dispositivo {} : {}", device, asdv);
      Optional<AssociazioneDV> optAssDv = asdv.stream()
                                              .filter(adv -> cacheVehicleWizard.get(adv.getUuidVeicolo()) != null
                                                             && targa.equals(cacheVehicleWizard.get(adv.getUuidVeicolo())
                                                                                               .getLicensePlate())
                                                             && nazione.equals(cacheVehicleWizard.get(adv.getUuidVeicolo())
                                                                                                 .getCountry()))
                                              .findFirst();

      if (optAssDv.isPresent()) {
        dispositivo = device;
        break;
      }
    }

    log.info("Dispositivo found: {}", dispositivo);
    return Optional.ofNullable(dispositivo);
  }

  @Override
  public List<DispositivoDTO> findAllByVehicleUuid(String uuidVeicolo) {
    log.debug("Request all devices by vehicle uuid : {}", uuidVeicolo);
    return dispositivoRepositoryExt.findAllByVehicleUuid(uuidVeicolo)
                                   .stream()
                                   .map(dispositivoMapper::toDto)
                                   .collect(Collectors.toList());
  }

  @Override
  public List<it.fai.ms.common.jms.dto.efservice.DispositivoDTO> findActiveByVehicleUuid(String uuidVeicolo, String codiceCliente) {
    log.debug("Request active devices by vehicle uuid : {} and cliente {}", uuidVeicolo, codiceCliente);
    return dispositivoRepositoryExt.findByVehicleUuidAndStato(uuidVeicolo, StatoDispositivo.getGreenState())
                                   .stream()
                                   .filter(d -> codiceCliente.equals(d.getContratto()
                                                                      .getClienteFai()
                                                                      .getCodiceCliente()))
                                   .map(dispositivoMapper::toJmsDto)
                                   .collect(Collectors.toList());
  }

  @Override
  public Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicolo(String uuid) {
    return dispositivoRepositoryExt.findByAssociazioneDispositivoVeicolos_uuidVeicolo(uuid);
  }

  @Override
  public Optional<Dispositivo> findGoBoxDeviceRelated(String serialeDeviceSostituito) {
    return dispositivoRepositoryExt.findByStatoDispositivoServizios_PanAndTipoDispositivo_nomeAndStatoDispositivoServizios_TipoServizio_nome(serialeDeviceSostituito,
                                                                                                                                             TipoDispositivoEnum.GO_BOX,
                                                                                                                                             "PEDAGGI_AUSTRIA");
  }

  @Override
  public StatoDispositivoServizio updateStatoDispositivoServizio(StatoDispositivoServizio sds) {
    StatoDispositivoServizio sdsSaved = sdsRepository.save(sds);
    log.info("Saved Sds: {}", sdsSaved);
    return sdsSaved;
  }

  @Override
  public Set<String> checkDispositivoExists(String codiceCliente, TipoDispositivoEnum deviceType, Set<String> vehicleUUIDs) {
    return vehicleUUIDs.parallelStream()
                       .map(uuid -> {
                         Set<Dispositivo> devices = findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(uuid,
                                                                                                                             deviceType);
                         if (devices == null || devices.isEmpty()) {
                           log.debug("Not found device by uuidVeicolo: {} and deviceType: {}", uuid, deviceType);
                         } else {
                           Optional<Dispositivo> optDispositivo = devices.stream()
                                                                         .filter(d -> !d.getStato()
                                                                                        .isBlue())
                                                                         .filter(d -> d.getContratto() != null && d.getContratto()
                                                                                                                   .getClienteFai() != null
                                                                                      && d.getContratto()
                                                                                          .getClienteFai()
                                                                                          .getCodiceCliente()
                                                                                          .equals(codiceCliente))
                                                                         .findFirst();

                           log.debug("Found device by uuidVeicolo: {} and deviceType: {} and codice cliente: {} ? {}", uuid, deviceType,
                                     codiceCliente, optDispositivo.isPresent());

                           if (optDispositivo.isPresent()) {
                             return null;
                           }
                         }

                         return uuid;
                       })
                       .filter(uuid -> StringUtils.isNotBlank(uuid))
                       .collect(toSet());
  }

  @Override
  public List<String> checkDispositivoExists(CheckDispositiviDTO checkDispositiviDto) {
    TipoDispositivoEnum deviceType = checkDispositiviDto.getTipoDispositivo();
    String codiceCliente = checkDispositiviDto.getCodiceCliente();
    Map<String, String> veicoli = checkDispositiviDto.getVeicoli();
    Set<String> vehicleUuid = veicoli.keySet();
    Set<String> vehicleUuidWithoutDeviceType = checkDispositivoExists(codiceCliente, deviceType, vehicleUuid);

    if (vehicleUuidWithoutDeviceType == null) {
      return new ArrayList<>();
    }
    return vehicleUuidWithoutDeviceType.stream()
                                       .map(uuid -> veicoli.get(uuid))
                                       .collect(toList());
  }

  @Override
  public List<DispositivoDTO> getDispositiviAttiviTarghe(final LocalDate dateStart) {
    log.debug("Request getDispositiviAttiviTarghe from {}", dateStart);

    List<StatoDispositivo> states = Arrays.asList(StatoDispositivo.ATTIVO);

    return dispositivoRepositoryExt.findByStatoIn(states)
                                   .stream()
                                   .map(dispositivoMapper::toDto)
                                   .collect(toList());
  }

  @Override
  public List<DispositivoDTO> checkDevicesNotFound(String tipo, List<String> serialList) {
    TipoDispositivo td = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.valueOf(tipo.toUpperCase()));

    List<StatoDispositivo> states = Arrays.asList(StatoDispositivo.ATTIVO, StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO,
                                                  StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.ATTIVO_SPEDITO_A_FAI,
                                                  StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                                                  StatoDispositivo.ATTIVO_EVASO);

    List<String> serials = serialList.stream()
                                     .filter(Objects::nonNull)
                                     .collect(Collectors.toList());

    List<DispositivoDTO> results = null;

    List<Dispositivo> devices = dispositivoRepositoryExt.findByStatoInAndTipoDispositivo(states, td);

    if (devices != null && !devices.isEmpty()) {

      results = devices.stream()
                       .filter(d -> {

                         return (StringUtils.isNotBlank(d.getSeriale()) && !serials.contains(d.getSeriale()));

                       })
                       .map(dispositivoMapper::toDto)
                       .collect(Collectors.toList());

    } else {

      results = new ArrayList<DispositivoDTO>();

    }

    return results;
  }

  @Override
  public void changeStatoServiceToNotActiveByDevice(Dispositivo dispositivo) {
    Long id = dispositivo.getId();
    Dispositivo dispositivoReloaded = dispositivoRepositoryExt.findOne(id);
    dispositivoReloaded.getStatoDispositivoServizios()
                       .forEach(sds -> {
                         StatoDispositivoServizio sdsSaved = statoDispositivoServizioRepositoryExt.save(sds.stato(StatoDS.NON_ATTIVO)
                                                                                                           .dataDisattivazione(Instant.now()));
                         log.debug("Updated {}", sdsSaved);
                       });
  }

  @Override
  public List<Dispositivo> findActiveDeviceExpired() {
    ZonedDateTime now = ZonedDateTime.now();
    Set<StatoDispositivo> activeStates = StatoDispositivo.getGreenState();
    List<Dispositivo> devicesExpired = dispositivoRepositoryExt.findActiveDeviceExpired(now, activeStates);
    log.info("Found {} active device with expired date is less than {}", (devicesExpired != null) ? devicesExpired.size() : 0, now);
    return devicesExpired;
  }

  @Override
  public int findActiveDeviceExpiredAndUpdateStateToExpired() {
    final List<Dispositivo> devicesUpdated = new ArrayList<>();
    List<Dispositivo> devicesExpired = findActiveDeviceExpired();
    if (devicesExpired != null && !devicesExpired.isEmpty()) {
      devicesExpired.forEach(device -> {
        try {
          Dispositivo deviceUpdated = save(device.stato(StatoDispositivo.SCADUTO));
          devicesUpdated.add(deviceUpdated);
        } catch (Exception e) {
          log.error("Not update device: {} because: ", device, e);
        }
      });
    }
    return devicesUpdated.size();
  }

}
