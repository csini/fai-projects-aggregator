package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;

@Service
@Transactional
public abstract class RequestUtilService {

  public final static String SEPARATOR_TARGA_NAZIONE = "§";

  private Logger log = LoggerFactory.getLogger(getClass());

  private final ClienteFaiServiceExt clienteFaiService;

  public RequestUtilService(final ClienteFaiServiceExt _clienteFaiService) {
    clienteFaiService = _clienteFaiService;
  }

  public Richiesta createDefaultRequestByType(TipoRichiesta requestType) {
    log.debug("Request type to generate: {}", requestType);
    return new Richiesta().tipo(requestType)
                          .data(Instant.now())
                          .dataModificaStato(Instant.now());
  }

  public Richiesta createDefaultRequestByTypeAndDeviceType(TipoRichiesta requestType, TipoDispositivo tipoDispositivo) {
    log.debug("Request type to generate: {} - {}", requestType, tipoDispositivo.getNome());
    return createDefaultRequestByType(requestType).tipoDispositivo(tipoDispositivo);
  }

  public StatoRichiesta getStatusRequestByDeviceType(TipoDispositivoEnum deviceType) {
    StatoRichiesta requestStatus = StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE;
    switch (deviceType) {
    case TELEPASS_EUROPEO:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TI;
      break;
    case VIACARD:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_VC;
      break;
    case TRACKYCARD:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY;
      break;
    case GO_BOX:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_GOBOX;
      break;
    case TOLL_COLLECT:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS;
      break;
    case TESSERA_CARONTE:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_TESSERA_CARONTE;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO;
      break;
    case ASSISTENZA_VEICOLI:
      requestStatus = StatoRichiesta.ATTIVO_PER_MODIFICA_ASSISTENZA_VEICOLI;
      break;
      
    // TODO add dispositivi da gestire;

    default:
      log.error("Device type {} not managed to retrieve request status, using default status {}.", deviceType, requestStatus);
      break;
    }

    return requestStatus;
  }

  public OrdineCliente createDefaultOrdineCliente(String codiceCliente) {
    ClienteFai clienteFai = retrieveClienteFai(codiceCliente);
    return createDefaultOrdineCliente(clienteFai);
  }

  public void assignTargaNazioneVehicleOnRequestsByCarrelloDTO(List<Richiesta> requests, CarrelloModificaRichiestaDTO carrelloDTO) {
    String targa = carrelloDTO.getTarga();
    String nazione = carrelloDTO.getNazione();
    log.info("Set New TargaNazione on requests");
    requests = requests.stream()
                       .map(r -> assignNewTargaNazione(r, targa, nazione))
                       .collect(toList());
  }

  public void assignClientOrderOnRequests(List<Richiesta> requests, OrdineCliente clientOrderForRequests) {
    log.debug("Set ClientOrder on requests");
    if (requests != null && !requests.isEmpty()) {
      requests.forEach(r -> r.setOrdineCliente(clientOrderForRequests));
    }
  }

  public Richiesta assignNewTargaNazione(Richiesta request, String licensePlate, String country) {
    log.info("Assign new Targa: {} - Nazione: {} on Richiesta {}", licensePlate, country, request);
    String licensePlateCountry = licensePlate + SEPARATOR_TARGA_NAZIONE + country;
    return request.newTargaNazione(licensePlateCountry);
  }

  protected ClienteFai retrieveClienteFai(String codiceCliente) {
    return clienteFaiService.findByCodiceCliente(codiceCliente);
  }

  private OrdineCliente createDefaultOrdineCliente(ClienteFai clienteFai) {
    return createDefaultOrdineCliente().clienteAssegnatario(clienteFai);
  }

  private OrdineCliente createDefaultOrdineCliente() {
    return new OrdineCliente().stato(StatoOrdineCliente.DA_EVADERE)
                              .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                              .dataCreazione(Instant.now())
                              .dataModificaStato(Instant.now());
  }

}
