package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;

import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the OrdineCliente entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdineClienteRepository extends JpaRepository<OrdineCliente, Long> {

  @Query("select distinct c from ClienteFai a, OrdineCliente c " + " where a.id = c.clienteAssegnatario.id"
      + " and a.codiceCliente = :codiceAzienda")
  List<OrdineCliente> findAllByCodiceAzienda(@Param("codiceAzienda") String codiceAzienda);

}
