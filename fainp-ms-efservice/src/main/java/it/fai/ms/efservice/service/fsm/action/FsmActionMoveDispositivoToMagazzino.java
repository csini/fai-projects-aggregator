package it.fai.ms.efservice.service.fsm.action;

import java.time.Instant;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class FsmActionMoveDispositivoToMagazzino implements Action<StatoRichiesta, RichiestaEvent> {

  private static final Logger log = LoggerFactory.getLogger(FsmActionMoveDispositivoToMagazzino.class);
  private ManageDevicesInStorageService deviceInStorageService;

  public FsmActionMoveDispositivoToMagazzino(final ManageDevicesInStorageService deviceInStorageService) {
    this.deviceInStorageService = deviceInStorageService;

  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    log.info("move dispositivo to deposito {}", context);
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object         object  = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        Set<Dispositivo> dispositivos = richiesta.getDispositivos();
        dispositivos.forEach(reqDisp->{
          reqDisp.setStato(StatoDispositivo.RIENTRATO_DISMESSO);
          String seriale = reqDisp.getSeriale();
          reqDisp.setSeriale(seriale + "-dismesso"+reqDisp.getId());

          Dispositivo dispDeposito = createDispositivoDefault(reqDisp.getTipoDispositivo());
          dispDeposito.setStato(StatoDispositivo.IN_DEPOSITO);
          dispDeposito.setContratto(null);
          dispDeposito.setSeriale(seriale);
          deviceInStorageService.save(dispDeposito);
          deviceInStorageService.moveStatoDispositivoServizios(reqDisp, dispDeposito);
          deviceInStorageService.removeAssociazioneDvs(dispDeposito);

          deviceInStorageService.save(dispDeposito);
          deviceInStorageService.save(reqDisp);
          deviceInStorageService.flushDevice();
        });
      }
    }
  }
  public Dispositivo createDispositivoDefault(TipoDispositivo tipoDispositivo) {
    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setTipoDispositivo(tipoDispositivo);
    dispositivo.setStato(StatoDispositivo.INIZIALE);
    dispositivo.setSeriale("NO_SERIALE");
    dispositivo.setDataModificaStato(Instant.now());
    return dispositivo;
  }
}
