package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.List;

public class TransitoRispostaDTO implements Serializable {

  private Long numDispositiviDaAbbinare;
  private Long numDispositiviAbbinati;
  private List<TransitoAnomaliaDTO> anomalie;

  public Long getNumDispositiviDaAbbinare() {
    return numDispositiviDaAbbinare;
  }

  public void setNumDispositiviDaAbbinare(Long numDispositiviDaAbbinare) {
    this.numDispositiviDaAbbinare = numDispositiviDaAbbinare;
  }

  public Long getNumDispositiviAbbinati() {
    return numDispositiviAbbinati;
  }

  public void setNumDispositiviAbbinati(Long numDispositiviAbbinati) {
    this.numDispositiviAbbinati = numDispositiviAbbinati;
  }

  public List<TransitoAnomaliaDTO> getAnomalie() {
    return anomalie;
  }

  public void setAnomalie(List<TransitoAnomaliaDTO> anomalie) {
    this.anomalie = anomalie;
  }

  @Override
  public String toString() {
    return "TransitoRispostaDTO{" +
      "numDispositiviDaAbbinare=" + numDispositiviDaAbbinare +
      ", numDispositiviAbbinati=" + numDispositiviAbbinati +
      ", anomalie=" + anomalie +
      '}';
  }
}
