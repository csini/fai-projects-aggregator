package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;

/**
 * Spring Data JPA repository for the Richiesta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RichiestaRepository extends JpaRepository<Richiesta, Long> {

  @Query("select distinct richiesta from Richiesta richiesta left join fetch richiesta.dispositivos")
  List<Richiesta> findAllWithEagerRelationships();

  @Query("select richiesta from Richiesta richiesta left join fetch richiesta.dispositivos where richiesta.id =:id")
  Richiesta findOneWithEagerRelationships(@Param("id") Long id);
  
  
  @Query("select distinct d from ClienteFai a, OrdineCliente c, Richiesta d " + " where a.id = c.clienteAssegnatario.id" + " and c.id = d.ordineCliente.id"
      + " and a.codiceCliente = :codiceAzienda")
  List<Richiesta> findAllByCodiceAzienda(@Param("codiceAzienda") String codiceAzienda);

}
