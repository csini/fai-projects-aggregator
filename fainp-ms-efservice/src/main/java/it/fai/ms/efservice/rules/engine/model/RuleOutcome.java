package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import it.fai.common.enumeration.WizardPreconditionCode;

public class RuleOutcome implements Serializable {

  public static class RuleFailure implements Serializable {

    private static final long serialVersionUID = -8088348769901969950L;

    private String code;
    private String mess;

    public RuleFailure(final String _failureCode, final String _failureMess) {
      code = _failureCode;
      mess = _failureMess;
    }

    @Override
    public boolean equals(final Object _obj) {
      return _obj instanceof RuleFailure && Objects.equals(((RuleFailure) _obj).getCode(), code)
             && Objects.equals(((RuleFailure) _obj).getMess(), mess);
    }

    public String getCode() {
      return code;
    }

    public String getMess() {
      return mess;
    }

    public Number decode() {
      WizardPreconditionCode preconditionCode = WizardPreconditionCode.DEFAULT;
      if (StringUtils.isNotBlank(code)) {
        preconditionCode = WizardPreconditionCode.get(code);
      }
      return preconditionCode.code();
    }

    public boolean isVehicleAnomaly() {
      if (this.decode() != null) {
        int errorCode = this.decode()
                            .intValue();
        if (WizardPreconditionCode.getVehicleAnomalyCode().contains(errorCode)) {
          return true;
        }
      }
      return false;
    }
    
    public boolean isTesseraCaronteNotActivable() {
      if (this.decode() != null) {
        int errorCode = this.decode()
                            .intValue();
        if (errorCode == WizardPreconditionCode.TESSERA_CARONTE_NOT_ACTIVABLE.code().intValue()) {
          return true;
        }
      }
      return false;
    }

    @Override
    public int hashCode() {
      return Objects.hash(code, mess);
    }

    public void setCode(final String _code) {
      code = _code;
    }

    public void setMess(final String _mess) {
      mess = _mess;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("RuleFailure [code=")
             .append(this.code)
             .append(", mess=")
             .append(this.mess)
             .append(", decode=")
             .append(decode())
             .append(", isVehicleAnomaly=")
             .append(isVehicleAnomaly())
             .append("]");
      return builder.toString();
    }

  }

  private static final long serialVersionUID = -123368976569319622L;

  private Instant     createDate = Instant.now();
  private RuleFailure failure;
  private Boolean     outcome    = false;

  public RuleOutcome() {
    outcome = true;
  }

  public RuleOutcome(final RuleFailure _ruleFailure) {
    failure = _ruleFailure;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleOutcome) _obj).getOutcome(), outcome)
                 && Objects.equals(((RuleOutcome) _obj).getFailure(), getFailure());
    }
    return isEquals;
  }

  public Instant getCreateDate() {
    return createDate;
  }

  public Optional<RuleFailure> getFailure() {
    return Optional.ofNullable(failure);
  }

  public Boolean getOutcome() {
    return outcome;
  }

  @Override
  public int hashCode() {
    return Objects.hash(outcome, getFailure());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleOutcome [");
    builder.append("createDate=");
    builder.append(createDate);
    builder.append(", ");
    if (failure != null) {
      builder.append("failure=");
      builder.append(failure);
      builder.append(", ");
    }
    builder.append("outcome=");
    builder.append(outcome);
    builder.append("]");
    return builder.toString();
  }

}
