package it.fai.ms.efservice.domain.enumeration;

/**
 * The ClassificazioneTipoDisp enumeration.
 */
public enum ClassificazioneTipoDisp {
    TESSERA, TESSERA_VIRTUALE, APPARATO
}
