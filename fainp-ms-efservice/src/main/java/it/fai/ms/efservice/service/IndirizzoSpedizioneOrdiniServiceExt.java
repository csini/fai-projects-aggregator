package it.fai.ms.efservice.service;

import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.client.dto.IndirizzoSpedizioneDTO;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;

public interface IndirizzoSpedizioneOrdiniServiceExt {

  OrdineCliente setIndirizziOnOrdineCliente(OrdineCliente ordineCliente);
  
  IndirizziDiSpedizioneDTO getIndirizziDiSpedizioneByNav(String codiceCliente);

  IndirizzoSpedizioneOrdini mapAndpersistIndirizzoSpedizioneOrdini(IndirizzoSpedizioneDTO indirizzoSpedizione);

}
