package it.fai.ms.efservice.service.fsm.config.dartfordcrossing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGenerateContratto;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = "inoltroDartFordCrossing")
public class FsmInoltroDartFordCrossingConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue fsmSenderToQueue;

  @SuppressWarnings("unused")
  private final ManageDeviceSentModeService manageDeviceSentModeService;

  @SuppressWarnings("unused")
  private final DeviceProducerService deviceProducerService;

  private final GeneratorContractCodeService generatorContractCodeService;

  public FsmInoltroDartFordCrossingConfig(final GeneratorContractCodeService generatorContractCodeService,
                                          FsmSenderToQueue fsmSenderToQueue, ManageDeviceSentModeService manageDeviceSentModeService,
                                          DeviceProducerService deviceProducerService) {
    super();
    this.generatorContractCodeService = generatorContractCodeService;
    this.fsmSenderToQueue = fsmSenderToQueue;
    this.manageDeviceSentModeService = manageDeviceSentModeService;
    this.deviceProducerService = deviceProducerService;
  }

  @SuppressWarnings("unused")
  private Richiesta getRichiesta(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Object obj = context.getMessageHeader("object");
    Richiesta richiesta = null;
    if (obj instanceof Richiesta) {
      richiesta = (Richiesta) obj;
      if (log.isDebugEnabled()) {
        log.debug("Change Stato Richiesta [ID: " + ((richiesta != null) ? richiesta.getId() : null) + "] per spedizioni Dispositivi...");
      }

    } else {
      throw new RuntimeException("Object " + obj + " is not a Reuest");
    }
    return richiesta;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmInoltroDartFordCrossing");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());

  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, ctx -> log.trace("Target: " + ctx.getTarget()
                                                                                                 .getIds()))
          .states(StatoRichiestaUtil.getStateOfInoltroDartFordCrossing());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionGenerateContratto(generatorContractCodeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_INOLTRATO)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ATTIVO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.DISATTIVO));
  }

}
