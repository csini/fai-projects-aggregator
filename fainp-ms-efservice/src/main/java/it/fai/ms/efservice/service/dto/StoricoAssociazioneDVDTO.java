package it.fai.ms.efservice.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the StoricoAssociazioneDV entity.
 */
public class StoricoAssociazioneDVDTO implements Serializable {

    private Long id;

    private Instant dataInizio;

    private Instant dataFine;

    private String uuidVeicolo;

    private Long dispositivoId;

    private String dispositivoSeriale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Instant dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Instant getDataFine() {
        return dataFine;
    }

    public void setDataFine(Instant dataFine) {
        this.dataFine = dataFine;
    }

    public String getUuidVeicolo() {
        return uuidVeicolo;
    }

    public void setUuidVeicolo(String uuidVeicolo) {
        this.uuidVeicolo = uuidVeicolo;
    }

    public Long getDispositivoId() {
        return dispositivoId;
    }

    public void setDispositivoId(Long dispositivoId) {
        this.dispositivoId = dispositivoId;
    }

    public String getDispositivoSeriale() {
        return dispositivoSeriale;
    }

    public void setDispositivoSeriale(String dispositivoSeriale) {
        this.dispositivoSeriale = dispositivoSeriale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = (StoricoAssociazioneDVDTO) o;
        if(storicoAssociazioneDVDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storicoAssociazioneDVDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoricoAssociazioneDVDTO{" +
            "id=" + getId() +
            ", dataInizio='" + getDataInizio() + "'" +
            ", dataFine='" + getDataFine() + "'" +
            ", uuidVeicolo='" + getUuidVeicolo() + "'" +
            "}";
    }
}
