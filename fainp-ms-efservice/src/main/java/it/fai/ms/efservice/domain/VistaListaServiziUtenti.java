package it.fai.ms.efservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vista_lista_servizi_utenti")
public class VistaListaServiziUtenti implements Serializable {
	
    private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "chiave_farlocca")
	private String chiaveFarlocca;
	
    @Column(name = "codice_cliente")
	private String codicecliente;
	
    @Column(name = "categoria_servizio_id")
	private Long categoriaServizioId;
	
    @Column(name = "nome")
	private String nome;
	
    @Column(name = "stato")
	private String stato;
	
	public String getChiaveFarlocca() {
		return chiaveFarlocca;
	}
	public void setChiaveFarlocca(String chiaveFarlocca) {
		this.chiaveFarlocca = chiaveFarlocca;
	}
	public String getCodicecliente() {
		return codicecliente;
	}
	public void setCodicecliente(String codicecliente) {
		this.codicecliente = codicecliente;
	}
	public Long getCategoriaServizioId() {
		return categoriaServizioId;
	}
	public void setCategoriaServizioId(Long categoriaServizioId) {
		this.categoriaServizioId = categoriaServizioId;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	
	

}
