/**
 * 
 */
package it.fai.ms.efservice.service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;

/**
 * @author Luca Vassallo
 */
public interface TipoDispositivoServiceExt {
  
  TipoDispositivo findOneById(Long id);

  TipoDispositivo findOneByNome(TipoDispositivoEnum nome);
  
  TipoDispositivo findTipoDispositivoWithRelationship(Long id);

  TipoDispositivo findOneWithRelationshipByNome(TipoDispositivoEnum nome);

}