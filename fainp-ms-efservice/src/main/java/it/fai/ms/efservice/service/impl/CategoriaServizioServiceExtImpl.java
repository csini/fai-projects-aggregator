package it.fai.ms.efservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.CategoriaServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.CategoriaServizioRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;
import it.fai.ms.efservice.service.CategoriaServizioServiceExt;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTOLight;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import it.fai.ms.efservice.service.mapper.CategoriaServizioMapper;
import it.fai.ms.efservice.service.mapper.TipoDispositivoLightMapper;
import it.fai.ms.efservice.service.mapper.TipoServizioMapper;

@Service
@Transactional
public class CategoriaServizioServiceExtImpl implements CategoriaServizioServiceExt {

  private CategoriaServizioRepositoryExt categoriaServizioRepository;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  @Autowired
  private TipoServizioRepositoryExt tipoServizioRepositoryExt;

  @Autowired
  TipoDispositivoLightMapper tipoDispositivoMapper;

  @Autowired
  TipoServizioMapper tipoServizioMapper;

  private CategoriaServizioMapper categoriaServizioMapper;

  @Override
  @Transactional(readOnly = true)
  public CategoriaServizioDTO findByNome(String nomeCategoriaServizi) {
    CategoriaServizio categoriaServizio = categoriaServizioRepository.findByNome(nomeCategoriaServizi);
    return categoriaServizioMapper.toDto(categoriaServizio);
  }

  @Autowired
  public void setCategoriaServizioRepository(CategoriaServizioRepositoryExt categoriaServizioRepository) {
    this.categoriaServizioRepository = categoriaServizioRepository;
  }

  @Autowired
  public void setCategoriaServizioMapper(CategoriaServizioMapper categoriaServizioMapper) {
    this.categoriaServizioMapper = categoriaServizioMapper;
  }

  @Override
  @Transactional(readOnly = true)
  public List<TipoServizioDTO> findServiziByNomeCategoria(String nomeCategoria) {
    CategoriaServizio categoria = categoriaServizioRepository.findByNome(nomeCategoria);
    List<TipoServizio> list = tipoServizioRepositoryExt.findByCategoriaServizioOrderByNome(categoria);
    return tipoServizioMapper.toDto(list);
  }

  @Override
  @Transactional(readOnly = true)
  public List<TipoDispositivoDTOLight> findTipoDispositivoByNomeCategoria(String nomeCategoria) {
    List<TipoDispositivo> list = tipoDispositivoRepository.findByNomeCategoriaServizioOrderByNome(nomeCategoria);
    return tipoDispositivoMapper.toDto(list);
  }
}
