package it.fai.ms.efservice.wizard.web.rest.vm;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MatrixAbstract implements Serializable {

  private static final long serialVersionUID = -8373976879373956826L;

  private List<String> header = new LinkedList<>();

  public MatrixAbstract() {
  }

  public List<String> getHeader() {
    return header;
  }

}
