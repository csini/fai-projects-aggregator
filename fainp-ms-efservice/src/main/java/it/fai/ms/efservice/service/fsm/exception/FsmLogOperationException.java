/**
 * 
 */
package it.fai.ms.efservice.service.fsm.exception;

/**
 * @author Luca Vassallo
 */
public class FsmLogOperationException extends Exception {

  private static final long serialVersionUID = -6889375548233788015L;

  public FsmLogOperationException() {
    this(FsmLogOperationException.class.toString());
  }

  public FsmLogOperationException(String msgId, Throwable cause) {
    super(msgId, cause);
  }

  public FsmLogOperationException(Throwable cause) {
    super(cause);
  }

  public FsmLogOperationException(String message) {
    super(message);
  }
}
