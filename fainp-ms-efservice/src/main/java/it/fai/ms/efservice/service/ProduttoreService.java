package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.ProduttoreDTO;
import java.util.List;

/**
 * Service Interface for managing Produttore.
 */
public interface ProduttoreService {

    /**
     * Save a produttore.
     *
     * @param produttoreDTO the entity to save
     * @return the persisted entity
     */
    ProduttoreDTO save(ProduttoreDTO produttoreDTO);

    /**
     *  Get all the produttores.
     *
     *  @return the list of entities
     */
    List<ProduttoreDTO> findAll();

    /**
     *  Get the "id" produttore.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ProduttoreDTO findOne(Long id);

    /**
     *  Delete the "id" produttore.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
