package it.fai.ms.efservice.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.dto.RichiestaOrdineClienteDTO;

/**
 * Spring Data JPA repository for the Richiesta entity.
 */
@Repository
public interface RichiestaRepositoryExt extends JpaRepository<Richiesta, Long> {

  Richiesta findOneByIdentificativo(String identificativo);

  /**
   * Return last record
   *
   * @return Richiesta
   */
  Richiesta findFirstByOrderByIdDesc();

  @Query("select r from Richiesta r inner join r.ordineCliente left join fetch r.dispositivos d left join d.associazioneDispositivoVeicolos left join fetch r.tipoDispositivo where r.identificativo =:identificativo")
  Richiesta findOneByIdentificativoWithEagerRelationships(@Param("identificativo") String identificativo);

  @Query("select r from Richiesta r inner join r.ordineCliente oc inner join oc.clienteAssegnatario cf where r.stato =:statoRichiesta and cf.codiceCliente =:codiceCliente")
  List<Richiesta> findAllByCodiceClienteFaiAndStatoRichiesta(@Param("statoRichiesta") StatoRichiesta statoRichiesta,
                                                                    @Param("codiceCliente") String codiceCliente);

  @Query("select r from Richiesta r inner join r.ordineCliente oc inner join oc.clienteAssegnatario cf inner join r.dispositivos where r.identificativo =:identificativo")
  Richiesta findOneByIdentificativoWithEagerOrdineClienteClienteFaiDispositivos(@Param("identificativo") String identificativo);

  @Query("select r from Richiesta r inner join r.ordineCliente oc inner join oc.clienteAssegnatario cf inner join r.dispositivos where r.identificativo in :identificativi")
  List<Richiesta> findAllByIdentificativoWithEagerOrdineClienteClienteFaiDispositivos(@Param("identificativi") List<String> identificativoRichieste);

  @Query("select r from Richiesta r inner join r.contratto c where r.associazione = :licensePlate and c.codContrattoCliente = :codiceContratto")
  List<Richiesta> findByCodiceContrattoAndTarga(@Param("codiceContratto") String _contractCode,
                                                @Param("licensePlate") String _licensePlate);

  @Query("select r from Richiesta r inner join r.contratto c where c.id = :idContratto")
  List<Richiesta> findByContrattoId(@Param("idContratto") Long id);

  @Query("select r from Richiesta r inner join r.contratto c where c.codContrattoCliente = :codiceContratto AND r.associazione = :targa AND r.country = :nazione AND r.stato = :stato")
  List<Richiesta> findByCodiceContrattoAndTargaAndStato(@Param("codiceContratto") String codiceContratto, @Param("targa") String targa,
                                                        @Param("nazione") String country, @Param("stato") StatoRichiesta stato);

  @Query("SELECT r FROM Richiesta r inner join r.dispositivos d WHERE r.stato = :statoRichiesta AND d.identificativo = :identificativo")
  Optional<Richiesta> findByStatoAndDispositivo(@Param("statoRichiesta") StatoRichiesta stato,
                                                @Param("identificativo") String identificativoDevice);

  List<Richiesta> findAllByStato(StatoRichiesta stato);

  @Query("SELECT r FROM Richiesta r inner join r.dispositivos d inner join r.contratto c WHERE r.stato = :statoRichiesta AND d.seriale = :serialNumber AND c.codContrattoCliente = :contratto AND r.stato = :statoRichiesta AND r.tipo IN :tipiRichiesta")
  List<Richiesta> findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(@Param("contratto") String codiceContratto,
                                                                               @Param("serialNumber") String serialNumberDevice,
                                                                               @Param("statoRichiesta") StatoRichiesta stato,
                                                                               @Param("tipiRichiesta") Set<TipoRichiesta> requestTypes);
  
  List<Richiesta> findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(String targa, String nazione,
                                                                              TipoDispositivoEnum tipoDispositivo,
                                                                              StatoRichiesta statoRichiesta);
  @Query("select r from Richiesta r inner join r.tipoDispositivo td where r.stato = :statoRichiesta and td.nome = :deviceType")
  List<Richiesta> findAllByTipoDispositivo_nomeAndStatoRichiesta(@Param("statoRichiesta") StatoRichiesta dispositivoDisattivato, @Param("deviceType") TipoDispositivoEnum deviceType);

  @Query("select new it.fai.ms.efservice.service.dto.RichiestaOrdineClienteDTO(r.id, r.stato) from Richiesta r inner join r.ordineCliente oc where oc.identificativo = :identificativoOrdine")
  Set<RichiestaOrdineClienteDTO> findStatoRichiestaByIdentificativoOrdine(@Param("identificativoOrdine") String identificativoOrdineCliente);
}
