package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniServiceExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;

@RestController
@RequestMapping("/api")
public class IndirizzoSpedizioneOrdiniResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private static final String INDIRIZZO_SPEDIZIONE_CLIENTE_NAV = "/indirizzospedizionecliente";

  private final IndirizzoSpedizioneOrdiniServiceExt indirizzoSpedizioneOrdiniServiceExt;

  private final OrdineClienteServiceExt ordineClienteServiceExt;

  public IndirizzoSpedizioneOrdiniResourceExt(IndirizzoSpedizioneOrdiniServiceExt indirizzoSpedizioneOrdiniServiceExt,
                                              final OrdineClienteServiceExt ordineClienteServiceExt) {
    this.indirizzoSpedizioneOrdiniServiceExt = indirizzoSpedizioneOrdiniServiceExt;
    this.ordineClienteServiceExt = ordineClienteServiceExt;
  }

  @GetMapping(INDIRIZZO_SPEDIZIONE_CLIENTE_NAV + "/{codiceCliente}")
  @Timed
  public ResponseEntity<IndirizziDiSpedizioneDTO> getIndirizzoSpedizioneOrdini(@PathVariable(name = "codiceCliente") String codiceCliente)
                                                                                                                                           throws URISyntaxException {
    log.debug("REST request to get IndirizziDiSpedizione by nav to codice cliente : {}", codiceCliente);
    IndirizziDiSpedizioneDTO indirizzi = indirizzoSpedizioneOrdiniServiceExt.getIndirizziDiSpedizioneByNav(codiceCliente);
    return ResponseEntity.ok()
                         .body(indirizzi);
  }

  @PostMapping(INDIRIZZO_SPEDIZIONE_CLIENTE_NAV + "/{codiceCliente}/{ordineClienteId}")
  @Timed
  public ResponseEntity<Map<String, IndirizzoSpedizioneOrdini>> saveIndirizziSpedizioneOrdini(@PathVariable(
                                                                                                           name = "codiceCliente") String codiceCliente,
                                                                                             @PathVariable(
                                                                                                           name = "ordineClienteId") Long idOrdineCliente) throws URISyntaxException {
    log.debug("REST request to save IndirizziDiSpedizione by nav to codice cliente : {} and OrdineClienteId: {}", codiceCliente,
              idOrdineCliente);

    OrdineCliente ordineCliente = ordineClienteServiceExt.findById(idOrdineCliente, true);

    ordineCliente = indirizzoSpedizioneOrdiniServiceExt.setIndirizziOnOrdineCliente(ordineCliente);
    Map<String, IndirizzoSpedizioneOrdini> mapIndirizzi = new HashMap<>();
    mapIndirizzi.put("TRANSITO", ordineCliente.getIndirizzoDiTransito());
    mapIndirizzi.put("FINALE", ordineCliente.getIndirizzoDestinazioneFinale());
    return ResponseEntity.ok()
                         .body(mapIndirizzi);
  }

}
