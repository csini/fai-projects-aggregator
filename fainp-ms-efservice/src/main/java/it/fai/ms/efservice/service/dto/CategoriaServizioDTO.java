package it.fai.ms.efservice.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the CategoriaServizio entity.
 */
public class CategoriaServizioDTO implements Serializable {

    private Long id;

    @NotNull
    private String nome;

    private String nomeBusiness;

    private String descrizione;

    private String testoPromo;

    private Long ordinamento;

    @NotNull
    private Boolean disabilitato;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeBusiness() {
        return nomeBusiness;
    }

    public void setNomeBusiness(String nomeBusiness) {
        this.nomeBusiness = nomeBusiness;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getTestoPromo() {
        return testoPromo;
    }

    public void setTestoPromo(String testoPromo) {
        this.testoPromo = testoPromo;
    }

    public Long getOrdinamento() {
        return ordinamento;
    }

    public void setOrdinamento(Long ordinamento) {
        this.ordinamento = ordinamento;
    }

    public Boolean isDisabilitato() {
        return disabilitato;
    }

    public void setDisabilitato(Boolean disabilitato) {
        this.disabilitato = disabilitato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CategoriaServizioDTO categoriaServizioDTO = (CategoriaServizioDTO) o;
        if(categoriaServizioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), categoriaServizioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CategoriaServizioDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", nomeBusiness='" + getNomeBusiness() + "'" +
            ", descrizione='" + getDescrizione() + "'" +
            ", testoPromo='" + getTestoPromo() + "'" +
            ", ordinamento='" + getOrdinamento() + "'" +
            ", disabilitato='" + isDisabilitato() + "'" +
            "}";
    }
}
