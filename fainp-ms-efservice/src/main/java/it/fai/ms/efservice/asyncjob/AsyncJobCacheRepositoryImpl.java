package it.fai.ms.efservice.asyncjob;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

@Service
public class AsyncJobCacheRepositoryImpl {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private Cache persistentCache;

  @Autowired
  public AsyncJobCacheRepositoryImpl(@Qualifier("async_job") final Cache _persistentCache) {
    persistentCache = _persistentCache;
  }

  public void persist(final String uuid, final Serializable value) {
    persistentCache.put(uuid, value);
    _log.debug("  Cache value [{}] updated for key {}", value, uuid);
  }

  public Serializable restore(final String uuid) {
    return persistentCache.get(uuid) != null ? (Serializable) persistentCache.get(uuid).get() : null;
  }

}
