package it.fai.ms.efservice.consumer.jms;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.consumer.VehicleMessageListener;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

@Deprecated
@Service
public class VehicleMessageListenerJms implements VehicleMessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private List<VehicleConsumer> consumers;

  @Autowired
  public VehicleMessageListenerJms(final List<VehicleConsumer> _consumers) {
    consumers = _consumers;
    _log.info("Consumers: {}", _consumers);
  }

  @Override
  public void receive(final VehicleMessage _vehicleMessage) {
    _log.info("Received message: {}", _vehicleMessage);
    consumers.stream()
             .forEach(consumer -> {
               consumer.consume(_vehicleMessage);
               _log.info("Message {} dispatched to consumer {}", _vehicleMessage, consumer);
             });
  }

  @Override
  public void persistVehicleNoTargaScorta(Map<String, String> mapVehicle) {
    // TODO Auto-generated method stub

  }

}
