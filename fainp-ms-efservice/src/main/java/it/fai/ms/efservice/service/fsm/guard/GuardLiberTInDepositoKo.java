package it.fai.ms.efservice.service.fsm.guard;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardLiberTInDepositoKo implements Guard<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(GuardLiberTInDepositoKo.class);

  private final ManageDevicesInStorageService deviceInStorageServiceExt;

  public GuardLiberTInDepositoKo(final ManageDevicesInStorageService _deviceInStorageServiceExt) {
    deviceInStorageServiceExt = _deviceInStorageServiceExt;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    boolean success = false;
    Set<Dispositivo> devicesInDeposito = deviceInStorageServiceExt.findDispositiviByStateAndType(StatoDispositivo.IN_DEPOSITO,
                                                                                                 TipoDispositivoEnum.LIBER_T);
    if (devicesInDeposito == null || devicesInDeposito.isEmpty()) {
      log.info("Not found Dispositivi IN_DEPOSITO");
      success = true;
    }
    return success;
  }

}
