package it.fai.ms.efservice.consumer.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;

public class VehicleMessage implements Serializable {

  private static final long serialVersionUID = -4750523728875446310L;

  private Integer axes;
  private String  category;
  // private Set<String> companyCodes = new LinkedHashSet<>();
  private String  country;
  private String  dieselParticulateFilterClass;
  private String  euroClass;
  private String  licensePlate;
  private Integer loneVehicleGrossWeight;
  private Integer loneVehicleTareWeight;
  private String  make;
  private String  state;
  private Integer trainsetGrossWeight;
  private String  type;
  private String  uuid;
  private String  libretto;
  private String  fuelType;
  private Float   length;
  private Float   height;
  private Integer payload;
  private Float   width;
  private Integer numeroAssiPienoCarico;
  private Instant dataUltimaModifica;
  private String  numberChassis;
  private String  color;
  private String  model;

  public VehicleMessage() {
  }

  public Integer getAxes() {
    return axes;
  }

  public String getCategory() {
    return category;
  }

  // public Set<String> getCompanyCodes() {
  // return companyCodes;
  // }

  public String getCountry() {
    return country;
  }

  public String getDieselParticulateFilterClass() {
    return dieselParticulateFilterClass;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public Integer getLoneVehicleGrossWeight() {
    return loneVehicleGrossWeight;
  }

  public Integer getLoneVehicleTareWeight() {
    return loneVehicleTareWeight;
  }

  public String getMake() {
    return make;
  }

  public String getState() {
    return state;
  }

  public Integer getTrainsetGrossWeight() {
    return trainsetGrossWeight;
  }

  public String getType() {
    return type;
  }

  public String getUuid() {
    return uuid;
  }

  public String getLibretto() {
    return libretto;
  }

  public void setAxes(final Integer _axes) {
    axes = _axes;
  }

  public void setCategory(final String _category) {
    category = _category;
  }

  // public void setCompanyCodes(final Set<String> _companyCodes) {
  // companyCodes = _companyCodes;
  // }

  public void setCountry(final String _country) {
    country = _country;
  }

  public void setDieselParticulateFilterClass(final String _dieselParticulateFilterClass) {
    dieselParticulateFilterClass = _dieselParticulateFilterClass;
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public void setLoneVehicleGrossWeight(final Integer _loneVehicleGrossWeight) {
    loneVehicleGrossWeight = _loneVehicleGrossWeight;
  }

  public void setLoneVehicleTareWeight(final Integer _loneVehicleTareWeight) {
    loneVehicleTareWeight = _loneVehicleTareWeight;
  }

  public void setMake(final String _make) {
    this.make = _make;
  }

  public void setState(final String _state) {
    state = _state;
  }

  public void setTrainsetGrossWeight(final Integer _trainsetGrossWeight) {
    trainsetGrossWeight = _trainsetGrossWeight;
  }

  public void setType(final String _type) {
    type = _type;
  }

  public void setUuid(final String _uuid) {
    uuid = _uuid;
  }

  public void setLibretto(String _libretto) {
    libretto = _libretto;
  }

  public String getFuelType() {
    return fuelType;
  }

  public void setFuelType(String fuelType) {
    this.fuelType = fuelType;
  }

  public Float getLength() {
    return length;
  }

  public void setLength(Float length) {
    this.length = length;
  }

  public Float getHeight() {
    return height;
  }

  public void setHeight(Float height) {
    this.height = height;
  }

  public Integer getPayload() {
    return payload;
  }

  public void setPayload(Integer paylod) {
    this.payload = paylod;
  }

  public Float getWidth() {
    return width;
  }

  public void setWidth(Float width) {
    this.width = width;
  }

  public Integer getNumeroAssiPienoCarico() {
    return numeroAssiPienoCarico;
  }

  public void setNumeroAssiPienoCarico(Integer numeroAssiPienoCarico) {
    this.numeroAssiPienoCarico = numeroAssiPienoCarico;
  }

  public Instant getDataUltimaModifica() {
    return dataUltimaModifica;
  }

  public void setDataUltimaModifica(Instant dataUltimaModifica) {
    this.dataUltimaModifica = dataUltimaModifica;
  }

  public String getNumberChassis() {
    return numberChassis;
  }

  public void setNumberChassis(String numberChassis) {
    this.numberChassis = numberChassis;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleMessage [axes=");
    builder.append(this.axes);
    builder.append(", category=");
    builder.append(this.category);
    // builder.append(", companyCodes=");
    // builder.append(this.companyCodes);
    builder.append(", country=");
    builder.append(this.country);
    builder.append(", dieselParticulateFilterClass=");
    builder.append(this.dieselParticulateFilterClass);
    builder.append(", euroClass=");
    builder.append(this.euroClass);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append(", loneVehicleGrossWeight=");
    builder.append(this.loneVehicleGrossWeight);
    builder.append(", loneVehicleTareWeight=");
    builder.append(this.loneVehicleTareWeight);
    builder.append(", make=");
    builder.append(this.make);
    builder.append(", state=");
    builder.append(this.state);
    builder.append(", trainsetGrossWeight=");
    builder.append(this.trainsetGrossWeight);
    builder.append(", type=");
    builder.append(this.type);
    builder.append(", uuid=");
    builder.append(this.uuid);
    builder.append(", libretto=");
    builder.append(this.libretto);
    builder.append(", FuelType=");
    builder.append(this.fuelType);
    builder.append(", length=");
    builder.append(this.length);
    builder.append(", height=");
    builder.append(this.height);
    builder.append(", width=");
    builder.append(this.width);
    builder.append(", payload=");
    builder.append(this.payload);
    builder.append(", numAssiPienoCarico=");
    builder.append(this.numeroAssiPienoCarico);
    builder.append(", dataUltimaModifica=");
    builder.append(this.dataUltimaModifica);
    builder.append(", color=");
    builder.append(this.color);
    builder.append("]");
    return builder.toString();
  }

}
