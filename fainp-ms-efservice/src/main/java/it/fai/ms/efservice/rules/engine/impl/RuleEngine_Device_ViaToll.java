package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_ViaToll implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;

  public RuleEngine_Device_ViaToll(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome,
                                   final RuleEngineServiceType _serviceTypeRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
    serviceTypeRuleContext = _serviceTypeRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("devType", deviceTypeRuleContext);
    context.put("serType", serviceTypeRuleContext);
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    // rules.add("boolean taranotnull = (vehicle.weight != null && vehicle.weight.loneVehicleTareWeight != null);");
    // rules.add("boolean taragt3500 = (vehicle.weight != null && vehicle.weight.loneVehicleTareWeight > 3500);");

    rules.add("boolean trainsetnotnull = (vehicle.weight != null && vehicle.weight.trainsetGrossWeight != null);");
    rules.add("boolean trainsetgt3500 = (vehicle.weight != null && vehicle.weight.trainsetGrossWeight > 3500);");
    rules.add("boolean grossnotnull = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight != null);");
    rules.add("boolean grossgt3500 = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight > 3500);");

    // rules.add("if ( taranotnull && !taragt3500) return 'tara3500viatoll';");

    rules.add("if (trainsetnotnull  && !trainsetgt3500) return 'trainsetviatoll';");
    rules.add("if (!trainsetnotnull && grossnotnull && !grossgt3500) return 'gross3500viatoll';");
    rules.add("return ''");

    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);

    String res = null;
    switch (preconditionCode) {
    case TRAINSET_GROSS_3500_VIA_TOLL:
      res = "Vehicle trainset weight must be > 3500 Kg";
      break;
    case GROSS_3500_VIA_TOLL:
      res = "Vehicle gross weight must be > 3500 Kg";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
