package it.fai.ms.efservice.service.fsm.config.telepass.ita;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendDeviceLossMessage;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTItaFurtoSmarrimentoConfig.MOD_TITA_FURTO_SMARRIMENTO)
public class FsmModificaTItaFurtoSmarrimentoConfig extends FsmRichiestaConfig {

  public static final String MOD_TITA_FURTO_SMARRIMENTO = "ModTItaFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue               senderFsmService;
  private final FsmActionSendDeviceLossMessage fsmActionSendDeviceLoss;

  public FsmModificaTItaFurtoSmarrimentoConfig(final FsmSenderToQueue _senderFsmService,
                                               final FsmActionSendDeviceLossMessage _fsmActionSendDeviceLoss) {
    senderFsmService = _senderFsmService;
    fsmActionSendDeviceLoss = _fsmActionSendDeviceLoss;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TI_FURTO_SMARRIMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TI;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaTItaFurtoSmarrimento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TI)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.SMARRIMENTO)
               .guard(new GuardSmarrimento())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.FURTO)
               .guard(new GuardFurto())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.SMARRIMENTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(fsmActionSendDeviceLoss)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.FURTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(fsmActionSendDeviceLoss)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.EVASO)
               .event(RichiestaEvent.RESPONSE_OK)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(fsmActionSendDeviceLoss);
  }

}
