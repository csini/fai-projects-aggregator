package it.fai.ms.efservice.service.fsm.type.hgv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.hgv.FsmInoltroHGVConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroHGV.FSM_INOLTRO_HGV)
public class FsmInoltroHGV extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_HGV = "fsmInoltroHGV";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroHGV(@Qualifier(FsmInoltroHGVConfig.INOLTRO_HGV) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                       FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_HGV;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.HGV };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}