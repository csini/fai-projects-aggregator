package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceLicensePlateChangeMessageProducerService {

  void deviceLicensePlateChangeToTopic(Richiesta richiesta);

  void deviceLicensePlateChangeToQueue(Richiesta richiesta);

}
