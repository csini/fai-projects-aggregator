package it.fai.ms.efservice.service;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.service.dto.OrdineClienteDTO;
import java.util.List;

/**
 * Service Interface for managing OrdineCliente.
 */
public interface OrdineClienteService {

    /**
     * Save a ordineCliente.
     *
     * @param ordineClienteDTO the entity to save
     * @return the persisted entity
     */
    OrdineClienteDTO save(OrdineClienteDTO ordineClienteDTO);

    /**
     *  Get all the ordineClientes.
     *
     *  @return the list of entities
     */
    List<OrdineClienteDTO> findAll();

    /**
     *  Get the "id" ordineCliente.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    OrdineClienteDTO findOne(Long id);

    /**
     *  Delete the "id" ordineCliente.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

  OrdineCliente save(OrdineCliente ordineCliente);
}
