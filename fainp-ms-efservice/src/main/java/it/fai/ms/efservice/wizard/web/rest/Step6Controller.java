package it.fai.ms.efservice.wizard.web.rest;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import it.fai.ms.efservice.wizard.service.Step6Service;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DocumentStep6Response;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ServiceDocumentsStep6BodyRequest;

@RestController
@RequestMapping("/api/wizard/step6")
public class Step6Controller {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private Step6Service step6Service;

  @Autowired
  public Step6Controller(final Step6Service _step6Service) {
    step6Service = _step6Service;
  }

  @PostMapping("/documents/{codiceAzienda}/service")
  public ResponseEntity<List<DocumentStep6Response>> getServiceDocuments(
          @ApiParam(value = "Codice Azienda del richiedente", required = true) @PathVariable("codiceAzienda") String _codiceAzienda,
          final @RequestBody @NotNull ServiceDocumentsStep6BodyRequest _request) {

    _log.info("Creating DocumentStep6 for service request {}", _request);

    List<DocumentStep6Response> rs = step6Service.getDocuments(_request,_codiceAzienda);

    _log.info("Response created {}", rs);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(rs);
  }
  
  @PostMapping("/documents/{codiceAzienda}/contract")
  public ResponseEntity<List<DocumentStep6Response>> getContractDocuments(
          @ApiParam(value = "Codice Azienda del richiedente", required = true) @PathVariable("codiceAzienda") String _codiceAzienda,
          final @RequestBody @NotNull ContractDocumentsStep6BodyRequest _request) {

    _log.info("Creating DocumentStep6 for contract request {}", _request);

    List<DocumentStep6Response> rs = step6Service.getDocuments(_request,_codiceAzienda);

    _log.info("Response created {}", rs);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(rs);
  }
  
  @PostMapping("/documents/{codiceAzienda}/device")
  public ResponseEntity<List<DocumentStep6Response>> getDeviceDocuments(
                                                                  @ApiParam(value = "Codice Azienda del richiedente", required = true) @PathVariable("codiceAzienda") String _codiceAzienda,
                                                                  final @RequestBody @NotNull DeviceDocumentsStep6BodyRequest _request) {
    _log.info("Creating DocumentStep6 for device request {}", _request);

    List<DocumentStep6Response> rs = step6Service.getDocuments(_request,_codiceAzienda);

    _log.info("Response created {}", rs);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(rs);
  }
}
