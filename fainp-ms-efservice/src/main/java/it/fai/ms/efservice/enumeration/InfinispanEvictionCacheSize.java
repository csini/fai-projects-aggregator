package it.fai.ms.efservice.enumeration;

public enum InfinispanEvictionCacheSize {

  SIZE_CACHE_PRECONDITION_WIZARD(50000),

  SIZE_CACHE_STANDARD(32768);
  
  private long size;

  private InfinispanEvictionCacheSize(long _size) {
    size = _size;
  }

  public long getSize() {
    return size;
  }

}
