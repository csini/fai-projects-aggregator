package it.fai.ms.efservice.service.fsm.type.tollcollect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.tollcollect.FsmModificaTollCollectConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTollCollect.FSM_MODIFICA_TOLLCOLLECT)
public class FsmModificaTollCollect extends FsmRichiestaGeneric {

  public static final String FSM_MODIFICA_TOLLCOLLECT = "fsmModificaTollCollect";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaTollCollect(@Qualifier(FsmModificaTollCollectConfig.MODIFICA_TOLLCOLLECT) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MODIFICA_TOLL_COLLECT;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TOLL_COLLECT };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.INOLTRO_TOLL_COLLECT;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }
}
