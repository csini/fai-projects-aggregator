package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceServiceNotActivableMessage;
import it.fai.ms.efservice.service.ServiziManageService;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceServiceNotActivatedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ServiziManageService serviziManageService;

  public DeviceServiceNotActivatedConsumer(final ServiziManageService _serviziManageService) {
    serviziManageService = _serviziManageService;
  }

  public void consume(DeviceServiceNotActivableMessage message) throws FsmExecuteCommandException {
    log.debug("Manage message: {}", message);
    serviziManageService.notActivableService(message);
  }

}
