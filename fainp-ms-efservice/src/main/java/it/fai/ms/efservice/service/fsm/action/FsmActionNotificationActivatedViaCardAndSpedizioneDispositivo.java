package it.fai.ms.efservice.service.fsm.action;

import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.MailActivatedDeviceService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionNotificationActivatedViaCardAndSpedizioneDispositivo implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue           senderFsmService;
  private final MailActivatedDeviceService mailActivationDeviceService;
  private final DeviceProducerService      deviceProducerService;
  private final DispositivoEvent           deviceEvent;

  public FsmActionNotificationActivatedViaCardAndSpedizioneDispositivo(final FsmSenderToQueue _senderFsmService,
                                                                       final MailActivatedDeviceService _mailActivationDeviceService,
                                                                       final DeviceProducerService _deviceProducerService,
                                                                       final DispositivoEvent _deviceEvent) {
    senderFsmService = _senderFsmService;
    mailActivationDeviceService = _mailActivationDeviceService;
    deviceProducerService = _deviceProducerService;
    deviceEvent = _deviceEvent;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;
      if (log.isDebugEnabled()) {
        log.debug("Change Stato Richiesta [ID: " + ((richiesta != null) ? richiesta.getId() : null) + "] per spedizioni Dispositivi...");
      }

      sendNotificationActivationByMail(richiesta);

      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      log.info("Change status with command: {} for dispositivi: {}", deviceEvent, dispositivi);
      executeCommandDispositivi(deviceEvent, dispositivi);

      changeDeviceServiceStatus(richiesta);
    }
  }

  private void sendNotificationActivationByMail(Richiesta richiesta) {
    Contratto contratto = richiesta.getContratto();
    if (contratto != null) {
      ClienteFai clienteFai = contratto.getClienteFai();
      if (clienteFai != null) {
        String codiceCliente = clienteFai.getCodiceCliente();
        if (StringUtils.isNotBlank(codiceCliente)) {
          if (mailActivationDeviceService.sendActivationViaCard(codiceCliente)) {
            log.info("Notification activation device sent");
          } else {
            log.error("Not send notification activation device");
          }
        } else {
          log.error("Code Cliente is null or empty for ClienteFai: {}", clienteFai);
        }
      } else {
        log.error("ClienteFai is null for contratto: {}", contratto);
      }
    } else {
      log.error("Contratto is null for Richiesta: {}", richiesta);
    }

  }

  private void executeCommandDispositivi(DispositivoEvent command, Set<Dispositivo> dispositivi) {
    if (dispositivi != null) {
      Iterator<Dispositivo> iterator = dispositivi.iterator();
      while (iterator != null && iterator.hasNext()) {
        Dispositivo dispositivo = iterator.next();
        try {
          senderFsmService.sendMessageForChangeStatoDispositivo(dispositivo, command);
        } catch (Exception e) {
          log.error("Error: " + e.getMessage(), e);
        }

      }
    }

  }

  private void changeDeviceServiceStatus(Richiesta richiesta) {
    log.info("Active/DeActive service on Device to request: {}", richiesta.getIdentificativo());
    deviceProducerService.updateDeviceServiceStatus(richiesta);
    log.info("Finish update device services status to Richiesta: {}", richiesta);
  }

}
