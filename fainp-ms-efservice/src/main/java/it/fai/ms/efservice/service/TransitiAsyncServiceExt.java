package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.ConfermaTransitoDTO;

public interface TransitiAsyncServiceExt {
  void processAsyncTransiti(String identificativoJob, ConfermaTransitoDTO dto);
  String getKeyCacheAndPushDefaultValue(String key);
}
