package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceServiceActivationMessageProducerService {

  void activateDevice(Richiesta orderRequestEntity);

  void deactivateDevice(Richiesta orderRequestEntity);

  void updateDeviceServiceStatus(Richiesta richiesta);

  void updateAllServiceDeviceStatus(Richiesta richiesta);

  void disableAllServiceDeviceStatus(Richiesta richiesta);

}
