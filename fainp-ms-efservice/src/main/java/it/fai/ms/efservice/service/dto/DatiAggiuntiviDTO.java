package it.fai.ms.efservice.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the DatiAggiuntivi entity.
 */
public class DatiAggiuntiviDTO implements Serializable {

    private Long id;

    private Integer previsioneSpesaMensile;

    private String uuidVeicolo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrevisioneSpesaMensile() {
        return previsioneSpesaMensile;
    }

    public void setPrevisioneSpesaMensile(Integer previsioneSpesaMensile) {
        this.previsioneSpesaMensile = previsioneSpesaMensile;
    }

    public String getUuidVeicolo() {
        return uuidVeicolo;
    }

    public void setUuidVeicolo(String uuidVeicolo) {
        this.uuidVeicolo = uuidVeicolo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DatiAggiuntiviDTO datiAggiuntiviDTO = (DatiAggiuntiviDTO) o;
        if(datiAggiuntiviDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), datiAggiuntiviDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DatiAggiuntiviDTO{" +
            "id=" + getId() +
            ", previsioneSpesaMensile='" + getPrevisioneSpesaMensile() + "'" +
            ", uuidVeicolo='" + getUuidVeicolo() + "'" +
            "}";
    }
}
