package it.fai.ms.efservice.service.jms.listener.device;

import java.time.Instant;
import java.util.Date;
import java.util.Set;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.dto.DispositivoStatoDTO;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.StatoDispositivoServizioServiceExt;

@Service(JmsListenerDeviceStateUpdate.QUALIFIER)
@Transactional
public class JmsListenerDeviceStateUpdate implements MessageListener {

  public final static String QUALIFIER = "jmsListenerDeviceStateUpdate";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DispositivoServiceExt dispositivoService;
  private final StatoDispositivoServizioServiceExt statoDispositivoServizioService;

  public JmsListenerDeviceStateUpdate(DispositivoServiceExt _dispositivoService,
                                      StatoDispositivoServizioServiceExt _statoDispositivoServizioService) {
    dispositivoService = _dispositivoService;
    statoDispositivoServizioService = _statoDispositivoServizioService;
  }

  @Override
  public void onMessage(Message message) {
    try {      
      // messaggio da allineamento telepass
      DispositivoStatoDTO dto = (DispositivoStatoDTO) ((ObjectMessage)message).getObject();
      _log.debug("Received message : {}", dto);
      Dispositivo device = dispositivoService.findOneByIdentificativo(dto.getUuidDispositivo());      
      device.setStato(StatoDispositivo.valueOf(dto.getStato()));
      device.setDataModificaStato(dto.getDataModificaStato());
      device.setDataSpedizione(dto.getDataSpedizione());
      device.setNoteTecniche(String.format("Aggiornato ALTIT %s", FastDateFormat.getInstance("dd/MM/yyyy HH:mm").format(new Date())));
      Set<StatoDispositivoServizio> sds =  device.getStatoDispositivoServizios();
      if (sds != null) {
        sds.stream() 
        .forEach(s -> {
          if (!s.getStato().equals(StatoDS.NON_ATTIVO)) {
            s.setStato(StatoDS.NON_ATTIVO);
            s.setDataDisattivazione(Instant.now());
            statoDispositivoServizioService.save(s);
          }
        });
      }
      device = dispositivoService.saveAndFlush(device);
    } catch (Exception e) {
      _log.error("JmsListenerDeviceStateUpdate", e);
    }    
  }

}
