package it.fai.ms.efservice.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.DispositivoService;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.mapper.DispositivoMapper;

/**
 * Service Implementation for managing Dispositivo.
 */
@Service
@Transactional
public class DispositivoServiceImpl implements DispositivoService {

  private final Logger log = LoggerFactory.getLogger(DispositivoServiceImpl.class);

  private final DispositivoRepository dispositivoRepository;

  private final DispositivoMapper dispositivoMapper;

  @Autowired 
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired 
  private DispositivoRepositoryExt dispositivoRepositoryExt;

  public DispositivoServiceImpl(DispositivoRepository dispositivoRepository, DispositivoMapper dispositivoMapper) {
    this.dispositivoRepository = dispositivoRepository;
    this.dispositivoMapper = dispositivoMapper;
  }

  public List<Dispositivo>findAllRientrati(Instant start ) {
    TipoDispositivo tipoDispositivo = new TipoDispositivo();
    tipoDispositivo.setId(2l);
    
    List<StatoDispositivo> stati =  Arrays.asList(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                                                  StatoDispositivo.RIENTRATO_DISMESSO,
                                                  StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE);
    List<Dispositivo> results = new ArrayList<>();
  
    results.addAll(dispositivoRepositoryExt.findByStatoInAndTipoDispositivoAndDataModificaStatoBetween( stati, tipoDispositivo, start, Instant.now()).stream() 
    .collect(Collectors.toCollection(LinkedList::new)));
    
    tipoDispositivo.setId(19l);
    results.addAll(dispositivoRepositoryExt.findByStatoInAndTipoDispositivoAndDataModificaStatoBetween( stati, tipoDispositivo, start, Instant.now()).stream() 
                   .collect(Collectors.toCollection(LinkedList::new)));
    
    results.forEach( d -> {
      d.getRichiestas().forEach( c->{
        
        c.getDispositivos().forEach( di -> {
          
        });
        
      });
    });
    this.log.debug("Found " + results.size());
    return results;
                   
  }
  
  /**
   * Save a dispositivo.
   *
   * @param dispositivoDTO
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public DispositivoDTO save(DispositivoDTO dispositivoDTO) {
    log.debug("Request to save Dispositivo : {}", dispositivoDTO);
    Dispositivo dispositivo = dispositivoMapper.toEntity(dispositivoDTO);
    dispositivo = dispositivoRepository.save(dispositivo);
    return dispositivoMapper.toDto(dispositivo);
  }

  /**
   * Get all the dispositivos.
   *
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public List<DispositivoDTO> findAll() {
    log.debug("Request to get all Dispositivos");
    return dispositivoRepository.findAll()
        .stream()
        .map(dispositivoMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get one dispositivo by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Override
  @Transactional(readOnly = true)
  public DispositivoDTO findOne(Long id) {
    log.debug("Request to get Dispositivo : {}", id);
    Dispositivo dispositivo = dispositivoRepository.findOne(id);
    return dispositivoMapper.toDto(dispositivo);
  }

  /**
   * Delete the dispositivo by id.
   *
   * @param id
   *          the id of the entity
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete Dispositivo : {}", id);
    dispositivoRepository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public List<DispositivoDTO> findAllByOrdine(String identificativo) {
    log.debug("Request all devices by richiesta with identificativo : {}", identificativo);
    Richiesta result = richiestaRepositoryExt.findOneByIdentificativo(identificativo);
    List<DispositivoDTO> arr = new ArrayList<DispositivoDTO>();
    for (Dispositivo dispositivo : result.getDispositivos()) {
      arr.add(dispositivoMapper.toDto(dispositivo));
    }
    return arr;
  }

  @Override
  @Transactional(readOnly = true)
  public List<DispositivoDTO> findAllByCodiceAziendaAndTipoMagazzino(String codiceAzienda, TipoMagazzino tipoMagazzino) {
    log.debug("Request all devices by codice azienda : {} and tipo magazzino : {}", codiceAzienda, tipoMagazzino);

//    Predicate<Dispositivo> predicate = p -> p.getTipoMagazzino() == null ? tipoMagazzino.equals(TipoMagazzino.SCORTA) : p.getTipoMagazzino().equals(tipoMagazzino);
//    Predicate<Dispositivo> predicate = p -> p.getTipoMagazzino() == null ? false : p.getTipoMagazzino().equals(tipoMagazzino);

    return dispositivoRepositoryExt.findAllByCodiceAziendaAndTipoMagazzino(codiceAzienda, tipoMagazzino)
        .parallelStream()
//        .filter(predicate)
        .map(dispositivoMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
  }
}
