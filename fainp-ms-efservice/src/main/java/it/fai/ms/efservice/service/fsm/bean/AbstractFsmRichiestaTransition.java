package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmLogOperationException;
import it.fai.ms.efservice.service.fsm.exception.FsmSwitchStateException;
import it.fai.ms.efservice.service.fsm.util.FsmRichiestaUtilService;

public class AbstractFsmRichiestaTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private FsmRichiestaUtilService fsmServiceUtil;

  @OnTransition
  public void anyTransition(StateContext<StatoRichiesta, RichiestaEvent> stateContext) throws FsmSwitchStateException,
                                                                                       FsmLogOperationException {
    State<StatoRichiesta, RichiestaEvent> source = stateContext.getSource();
    if (source != null) {
      Transition<StatoRichiesta, RichiestaEvent> transition = stateContext.getTransition();
      State<StatoRichiesta, RichiestaEvent> target = stateContext.getTarget();
      StatoRichiesta toState = target.getId();
      Message<RichiestaEvent> message = stateContext.getMessage();

      MessageHeaders headers = message.getHeaders();

      Object object = headers.get("object");
      boolean isValidEntity = validateEntity(object);
      if (isValidEntity) {
        if (object instanceof Richiesta) {
          Richiesta richiesta = (Richiesta) object;
          String nota = getNotaFromMessage(headers);
          if (nota != null) {
            richiesta.setUltimaNota(nota);
          }
          logTransitionFromTo(source, target);
          fsmServiceUtil.switchToState(richiesta, toState, transition);
        }
      }
    }
  }

  private String getNotaFromMessage(MessageHeaders headers) {
    String nota = null;
    Object object = headers.get("nota");
    if (object != null) {
      if (object instanceof String) {
        nota = (String) object;
      }
    } else {
      log.warn("Nota is null");
    }
    return nota;
  }

  private boolean validateEntity(Object obj) {
    if (obj == null) {
      log.error("Entity to change status is null");
      return false;
    }
    return true;
  }

  private void logTransitionFromTo(State<StatoRichiesta, RichiestaEvent> source, State<StatoRichiesta, RichiestaEvent> target) {
    log.debug("Transition From [" + ((source != null) ? source.getId() : null) + "] To [" + ((target != null) ? target.getId() : null)
              + "].");
  }

}
