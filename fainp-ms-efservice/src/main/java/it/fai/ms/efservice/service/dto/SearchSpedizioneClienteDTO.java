package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;

public class SearchSpedizioneClienteDTO implements Serializable {

  private String codiceAzienda;

  private String ragioneSociale;

  private String cerca;

  private String targa;

  private String seriale;

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCerca() {
    return cerca;
  }

  public void setCerca(String cerca) {
    this.cerca = cerca;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  @Override
  public String toString() {
    return "SearchSpedizioneClienteDTO{" +
      "codiceAzienda='" + codiceAzienda + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", cerca='" + cerca + '\'' +
      ", targa='" + targa + '\'' +
      ", seriale='" + seriale + '\'' +
      '}';
  }
}
