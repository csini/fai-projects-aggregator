package it.fai.ms.efservice.wizard.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.service.TipoServizioServiceExt;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractStep6;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceStep6;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DocumentStep6Response;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ServiceDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.VehicleStep6;

@Service
public class Step6ServiceImpl implements Step6Service {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private static final String DOWNLOAD = "DOWNLOAD";
  // private static final String INLINE = "INLINE";

  private final String BASE_URL_DOWNLOAD_CONTRACT     = "/servicedocuments/download/precompiled-contract/";
  private final String NEW_BASE_URL_DOWNLOAD_CONTRACT = "/servicedocuments/download/new-contract/precompiled/";

  private final TipoServizioServiceExt tpService;

  public Step6ServiceImpl(TipoServizioServiceExt tpService) {
    this.tpService = tpService;
  }

  @Override
  public List<DocumentStep6Response> getDocuments(ServiceDocumentsStep6BodyRequest _request, final String codiceAzienda) {
    List<TipoServizio> list = tpService.findByNames(_request.getServicetypes()
                                                    .stream()
                                                    .distinct()
                                                    .collect(Collectors.toList()));
    return list.stream()
        .filter(ts -> ts.getDocumenti() != null)
        .map(ts -> mapper(ts, codiceAzienda))
        .collect(Collectors.toList());
  }

  @Override
  public List<DocumentStep6Response> getDocuments(ContractDocumentsStep6BodyRequest _request, String _codiceAzienda) {

    List<DocumentStep6Response> resList = new ArrayList<>();

    // Per ora è gestito solo il caso TOLL_COLLECT: valutare se inserire una configurazione a catalogo

    for (ContractStep6 item : _request.getContracts()) {
      if (hasItemDeviceType(item, TipoDispositivoEnum.TOLL_COLLECT) && item.getContrattoAltroFornitore() != null) {
        createAndAddDocumentSte6(item, _codiceAzienda, resList);
      } 
      if (hasItemDeviceType(item, TipoDispositivoEnum.HGV)) {
        createAndAddDocumentSte6(item, _codiceAzienda, resList);
      }
      if (hasItemDeviceType(item, TipoDispositivoEnum.VISPRO)) {
        createAndAddDocumentSte6(item, _codiceAzienda, resList);
      }
//      if (hasItemDeviceType(item, TipoDispositivoEnum.ASSISTENZA_VEICOLI)) {
//        createAndAddDocumentSte6(item, _codiceAzienda, resList);
//      }
    }

    return resList;
  }

  private void createAndAddDocumentSte6(ContractStep6 item, String _codiceAzienda, List<DocumentStep6Response> resList) {
    log.debug("Create and add document step6 to device {} and company {} ", item.getTipoDispositivo(), _codiceAzienda);
    String uri = item.getContrattoAltroFornitore() != null && item.getContrattoAltroFornitore()
        .booleanValue() ? BASE_URL_DOWNLOAD_CONTRACT
                        : NEW_BASE_URL_DOWNLOAD_CONTRACT;

    uri += item.getTipoDispositivo() + "/" + _codiceAzienda;
    resList.add(new DocumentStep6Response(item.getTipoDispositivo(), uri, true));
  }

  private boolean hasItemDeviceType(ContractStep6 item, TipoDispositivoEnum deviceType) {
    return item.getTipoDispositivo().equals(deviceType.name());
  }

  @Override
  public List<DocumentStep6Response> getDocuments(DeviceDocumentsStep6BodyRequest _request, String _codiceAzienda) {
    List<DocumentStep6Response> resList = new ArrayList<>();

    // Per ora è gestito solo il caso TOLL_COLLECT: valutare se inserire una configurazione a catalogo

    for (DeviceStep6 item : _request.getDevices()) {
      String tipoDispositivo = item.getTipoDispositivo();
      TipoDispositivoEnum deviceType = null;
      try {
        deviceType = TipoDispositivoEnum.valueOf(tipoDispositivo);
      } catch (Exception e) {
        throw new IllegalArgumentException("Device type [ " + tipoDispositivo + " ] cannot be cast to "
            + TipoDispositivoEnum.class.getSimpleName());
      }

      String uri;
      for (VehicleStep6 vehicle : item.getVehicles()) {
        switch (deviceType) {
        case TOLL_COLLECT:
          uri = "/servicedocuments/download/precompiled/TOLL_COLLECT/" + _codiceAzienda + "?uuid=" + vehicle.getIdentificativo();
          resList.add(new DocumentStep6Response(tipoDispositivo, uri, vehicle, true));
          break;

        case ASSISTENZA_VEICOLI:
          uri = NEW_BASE_URL_DOWNLOAD_CONTRACT + "ASSISTENZA_VEICOLI/" + _codiceAzienda + "?property1=" + vehicle.getIdentificativo();
          resList.add(new DocumentStep6Response(tipoDispositivo, uri, vehicle, true));
          break;

        case HGV:
          resList.add(new DocumentStep6Response(tipoDispositivo, null, vehicle, false, true));
          break;

        default:
          log.warn("This device '{}' is not managed to retrieve documents...", deviceType);
          break;
        }
      }
    }

    return resList;
  }

  private DocumentStep6Response mapper(TipoServizio ts, String codiceAzienda) {
    switch (ts.getDocumenti()) {
    case DOWNLOAD:
      return new DocumentStep6Response(ts.getNome(), "/servicedocuments/download/precompiled/" + ts.getNome() + "/" + codiceAzienda);
    }
    return new DocumentStep6Response(ts.getNome(), null);
  }
}
