package it.fai.ms.efservice.service.fsm.config.telepass.ita;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActiveDevice;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendMessageToWsTelepassIta;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardAttesaTransito;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtClient;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtFai;
import it.fai.ms.efservice.service.fsm.guard.GuardTelepassItaInDepositoKo;
import it.fai.ms.efservice.service.fsm.guard.GuardTelepassItaInDepositoOk;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceAssociationMessageProducerService;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroTelepassItaConfig.INOLTRO_TI)
public class FsmInoltroTelepassItaConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_TI = "inoltroTI";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  private final ManageDevicesInStorageService deviceInStorageServiceExt;

  private final DeviceProducerService deviceProducerService;

  private final DeviceAssociationMessageProducerService deviceAssociationProduceService;

  private final ApplicationEventPublisher applicationEventPublisher;

  private final ManageDeviceSentModeService manageDeviceSentModeService;

  public FsmInoltroTelepassItaConfig(final FsmSenderToQueue _senderFsmService,
                                     final ManageDevicesInStorageService _deviceInStorageServiceExt,
                                     final DeviceProducerService _deviceProducerService,
                                     final DeviceAssociationMessageProducerService _deviceAssociationProduceService,
                                     final ApplicationEventPublisher _applicationEventPublisher,
                                     final ManageDeviceSentModeService _manageDeviceSentModeService) {
    senderFsmService = _senderFsmService;
    deviceInStorageServiceExt = _deviceInStorageServiceExt;
    deviceAssociationProduceService = _deviceAssociationProduceService;
    applicationEventPublisher = _applicationEventPublisher;
    manageDeviceSentModeService = _manageDeviceSentModeService;
    deviceProducerService = _deviceProducerService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_TI.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroTI();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.VERIFICA_TI_IN_DEPOSITO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VERIFICA_TI_IN_DEPOSITO)
               .target(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO)
               .guard(new GuardTelepassItaInDepositoKo(deviceInStorageServiceExt))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO)
               .target(StatoRichiesta.VERIFICA_TI_IN_DEPOSITO)
               .event(RichiestaEvent.MU_NEW_TELEPASS_ITA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VERIFICA_TI_IN_DEPOSITO)
               .target(StatoRichiesta.IN_ATTESA_INOLTRO)
               .guard(new GuardTelepassItaInDepositoOk(deviceInStorageServiceExt, applicationEventPublisher))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.INOLTRATO))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.IN_ATTESA_INOLTRO)
               .target(StatoRichiesta.INOLTRATO)
               .action(new FsmActionSendMessageToWsTelepassIta(deviceAssociationProduceService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.IN_ATTESA_INOLTRO)
               .event(RichiestaEvent.MS_REINOLTRA_A_WS)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ACCETTATO_FORNITORE)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ACCETTATO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.IN_ATTESA_INOLTRO)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO_FORNITORE)
               .target(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.IN_SPEDIZIONE))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .guard(new GuardIsToBeSentAtFai(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .guard(new GuardAttesaTransito(manageDeviceSentModeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .event(RichiestaEvent.MU_SPEDIZIONE_FAI)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .guard(new GuardIsToBeSentAtClient(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_DAL_FORNITORE))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_SPEDIZIONE_COMPLETA)
               .action(new FsmActionActiveDevice(senderFsmService, deviceProducerService, DispositivoEvent.SPEDITO_DA_FAI))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.CONCLUSO_NON_SPEDITO)
               .event(RichiestaEvent.MU_CHIUSURA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.REVOCA));
  }

}
