package it.fai.ms.efservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.efservice.domain.enumeration.KeyDatiAggiuntivi;
import it.fai.ms.efservice.domain.enumeration.TypeClassValue;

@Entity
// @EntityListeners({ RichiestaEntityListener.class, })
@Table(name = "dati_aggiuntivi_richiesta")
public class DatiAggiuntiviRichiesta implements Serializable {

  private static final long serialVersionUID = -2075289975352049479L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(name = "chiave", nullable = false)
  private KeyDatiAggiuntivi chiave;

  @Column(name = "valore", nullable = false)
  private String valore;

  @Enumerated(EnumType.STRING)
  @Column(name = "type_class", nullable = false)
  private TypeClassValue type; // Per ora c'è solo instant;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  private Richiesta richiesta;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public KeyDatiAggiuntivi getChiave() {
    return chiave;
  }

  public DatiAggiuntiviRichiesta chiave(KeyDatiAggiuntivi chiave) {
    setChiave(chiave);
    return this;
  }

  public void setChiave(KeyDatiAggiuntivi chiave) {
    this.chiave = chiave;
  }

  public String getValore() {
    return valore;
  }

  public DatiAggiuntiviRichiesta valore(String valore) {
    setValore(valore);
    return this;
  }

  public void setValore(String valore) {
    this.valore = valore;
  }

  public TypeClassValue getType() {
    return type;
  }

  public DatiAggiuntiviRichiesta type(TypeClassValue type) {
    setType(type);
    return this;
  }

  public void setType(TypeClassValue type) {
    this.type = type;
  }

  public Richiesta getRichiesta() {
    return richiesta;
  }

  public DatiAggiuntiviRichiesta richiesta(Richiesta richiesta) {
    setRichiesta(richiesta);
    return this;
  }

  public void setRichiesta(Richiesta richiesta) {
    this.richiesta = richiesta;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("DatiAggiuntiviRichiesta [");
    sb.append("id=")
      .append(id);
    sb.append(", chiave=")
      .append(chiave);
    sb.append(", value=")
      .append(valore);
    sb.append(", type=")
      .append(type);
    sb.append(", richiesta=")
      .append(richiesta);
    sb.append("]");
    return sb.toString();
  }

}
