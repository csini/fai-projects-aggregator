package it.fai.ms.efservice.service.fsm.guard;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardWsOK implements Guard<StatoRichiesta, RichiestaEvent> {

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    
    // Chiamata ad un web server....
    
    return false;
  }

}
