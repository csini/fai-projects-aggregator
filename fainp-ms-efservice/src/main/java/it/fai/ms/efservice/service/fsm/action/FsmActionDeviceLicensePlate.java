package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceLicensePlateChangeMessageProducerService;

public class FsmActionDeviceLicensePlate implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceLicensePlateChangeMessageProducerService deviceLicensePlateProducer;

  public FsmActionDeviceLicensePlate(final DeviceLicensePlateChangeMessageProducerService _deviceLicensePlateProducer) {
    deviceLicensePlateProducer = _deviceLicensePlateProducer;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    log.info("Execute deviceLicensePlateProducer: {}", context);

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      log.info("Richiesta: {}", richiesta);
      TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
      if (tipoDispositivo != null) {
        TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
        switch (deviceType) {
        case TELEPASS_EUROPEO_SAT:
          deviceLicensePlateProducer.deviceLicensePlateChangeToTopic(richiesta);
          break;

        case TELEPASS_ITALIANO:
          deviceLicensePlateProducer.deviceLicensePlateChangeToQueue(richiesta);
          break;
          
        case TELEPASS_EUROPEO:
          log.warn("Device {} managed return device to productor", deviceType);
          break;

        default:
          log.warn("Device type {} not managed...", deviceType);
          break;
        }
      } else {
        log.error("Not found device type for richiesta: {}", richiesta);
      }

    }

  }

}
