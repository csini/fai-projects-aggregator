package it.fai.ms.efservice.service.fsm.bean.viacard;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaViaCardFurtoSmarrimento")
public class FsmModificaViaCardFurtoSmarrimentoTransitionBean extends AbstractFsmRichiestaTransition {
}
