package it.fai.ms.efservice.asyncjob;

import java.io.Serializable;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AsyncJobServiceImpl {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());

  final AsyncJobCacheRepositoryImpl fsmAsyncRepo;

  public AsyncJobServiceImpl(AsyncJobCacheRepositoryImpl _fsmAsyncRepo) {
    fsmAsyncRepo = _fsmAsyncRepo;
  }

  public void save(final String keyCache, final Serializable value) {
    log.info("Persist for Key {} value {}", keyCache, value);
    fsmAsyncRepo.persist(keyCache, value);
  }

  public Serializable getValue(final String keyCache) {
    Serializable value = fsmAsyncRepo.restore(keyCache);
    log.debug("Restore value {} from key {}", value, keyCache);
    return value;
  }

  public String getKeyCache(String prefix, String identificativoJob) {
    String keyCache = new String(prefix + "-" + identificativoJob);
    log.debug("Retrieved key {}", keyCache);
    return keyCache;
  }

  public String generateKeyCacheAndPushValue(String prefixKey, Serializable value) {
    String keyCache = getKeyCache(prefixKey, UUID.randomUUID()
                                                 .toString());
    save(keyCache, value);
    log.debug("Generated key cache {} and push value {}", keyCache, value);
    return keyCache;
  }

}
