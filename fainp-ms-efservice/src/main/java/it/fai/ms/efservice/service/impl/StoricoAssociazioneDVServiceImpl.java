package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.StoricoAssociazioneDVService;
import it.fai.ms.efservice.domain.StoricoAssociazioneDV;
import it.fai.ms.efservice.repository.StoricoAssociazioneDVRepository;
import it.fai.ms.efservice.service.dto.StoricoAssociazioneDVDTO;
import it.fai.ms.efservice.service.mapper.StoricoAssociazioneDVMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing StoricoAssociazioneDV.
 */
@Service
@Transactional
public class StoricoAssociazioneDVServiceImpl implements StoricoAssociazioneDVService{

    private final Logger log = LoggerFactory.getLogger(StoricoAssociazioneDVServiceImpl.class);

    private final StoricoAssociazioneDVRepository storicoAssociazioneDVRepository;

    private final StoricoAssociazioneDVMapper storicoAssociazioneDVMapper;

    public StoricoAssociazioneDVServiceImpl(StoricoAssociazioneDVRepository storicoAssociazioneDVRepository, StoricoAssociazioneDVMapper storicoAssociazioneDVMapper) {
        this.storicoAssociazioneDVRepository = storicoAssociazioneDVRepository;
        this.storicoAssociazioneDVMapper = storicoAssociazioneDVMapper;
    }

    /**
     * Save a storicoAssociazioneDV.
     *
     * @param storicoAssociazioneDVDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public StoricoAssociazioneDVDTO save(StoricoAssociazioneDVDTO storicoAssociazioneDVDTO) {
        log.debug("Request to save StoricoAssociazioneDV : {}", storicoAssociazioneDVDTO);
        StoricoAssociazioneDV storicoAssociazioneDV = storicoAssociazioneDVMapper.toEntity(storicoAssociazioneDVDTO);
        storicoAssociazioneDV = storicoAssociazioneDVRepository.save(storicoAssociazioneDV);
        return storicoAssociazioneDVMapper.toDto(storicoAssociazioneDV);
    }

    /**
     *  Get all the storicoAssociazioneDVS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<StoricoAssociazioneDVDTO> findAll() {
        log.debug("Request to get all StoricoAssociazioneDVS");
        return storicoAssociazioneDVRepository.findAll().stream()
            .map(storicoAssociazioneDVMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one storicoAssociazioneDV by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public StoricoAssociazioneDVDTO findOne(Long id) {
        log.debug("Request to get StoricoAssociazioneDV : {}", id);
        StoricoAssociazioneDV storicoAssociazioneDV = storicoAssociazioneDVRepository.findOne(id);
        return storicoAssociazioneDVMapper.toDto(storicoAssociazioneDV);
    }

    /**
     *  Delete the  storicoAssociazioneDV by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StoricoAssociazioneDV : {}", id);
        storicoAssociazioneDVRepository.delete(id);
    }
}
