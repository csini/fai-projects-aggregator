package it.fai.ms.efservice.web.rest.errors;

/**
 * https://exagespa.atlassian.net/wiki/spaces/FAIMYS/pages/2555909/Convenzioni+sul+codice
 * https://exagespa.atlassian.net/browse/FAINP-541
 */
public enum Errno {
  //@formatter:off
  UNKNOWN(4000, "unknown", "unknown"),
  EXCEPTION_AT_CREATE_ORDERS(4001, "Order not created", "Order not created"),
  PARAMETERS_REQUIRED(4002, "Parameters required", "The parameters are required"),
  INVALID_PARAMETER(4003, "Invalid parameter", "The parameter is invalid"),
  CONTRACT_FOUND_PRODUCER_CUSTOMER(4004, "Contract found with producer and customer code", "Contract found with producer and customer code"),
  CONTRACT_FOUND_PRODUCER_CUSTOMER_PRIMARIO_TRUE(4007, "Contract found with producer and customer code and flag primario true", "Contract found with producer and customer code and flag primario true"),
  CONTRACT_FOUND_PRODUCER_CUSTOMER_WITH_STATUS_NOT_REVOKED(4008, "Contracts found with producer and customer code and status ACTIVED or SUSPENDED", "Contracts found with producer and customer code and status ACTIVED or SUSPENDED"),
  CONTRACT_FOUND_PRODUCER_CODE(4005, "Contract found with same producer and contract code", "Contract found with same producer and contract code"),
  INVALID_CUSTOMER(4006, "Supplied customer code not found", "Supplied customer code not found"),
  VEHICLE_ID_REQUIRED(4010, "vehicle id is required, must be greater than or equal to 1", "vehicle id is required, must be greater than or equal to 1"),
  DISPOSITIVO_ID_REQUIRED(4011, "Dispositivo id is required, must be greater than or equal to 1", "vehicle id is required, must be greater than or equal to 1"),
  VALIDATION_FAILED(4012, "Validation failed", "Validation failed"),
  INVALID_CATEGORY_TYPE(4013, "Invalid Service Category", "Invalid Service Category"),
  EXCEPTION_AT_CREATE_CONTRACT(4014, "Contract not created", "Contract not created"),
  NOT_FOUND_ORDER(4015, "Order not found", "Order not found"),
  ORDER_NULL(4016, "Order is null", "Order is null"),
  UPLOAD_DOCUMENT_ON_ORDER(4017, "Document not uploaded on richiesta", "Document not uploaded on richiesta"),
  STATO_DISPOSITIVO_REQUIRED(4018, "Stato dispositivo is required", "Stato dispositivo is required. Allowable values are blue,green,yellow"),
  VEHICLE_IDS_REQUIRED(4019, "vehicle ids are required, must be greater than or equal to 1", "vehicle ids are required, must be greater than or equal to 1"),
  FAILED_SENT_MESSAGE_TO_QUEUE(4020, "Message didn't send to queue", "Message didn't send to queue"),
  FAILED_ASSOCIATION(4021, "Failed association vehicle to device", "Failed association vehicle to device"),
  LIST_RICHIESTE_NULL(4022,"List of Richieste is null","List of Richieste is null"),
  EXCEPTION_ON_GENERATE_ORDER_DTO(2023, "Exception on generate ORDER_DTO", "Exception on generate ORDER_DTO"),
  NOT_CHANGE_RICHIESTA_STATUS(2024, "Exception on change status Richieste", "Exception on change status Richieste"),
  INVALID_VEHICLE(4025, "Invalid Vehicle for client", "Invalid Vehicle for client"),
  FAILED_TRACKYCARD_PIN_REQUEST(4021, "Failed send Trackycard pin request", "Failed send Trackycard pin request"),
  NOT_FOUND_CLIENTE_FAI(2025, "Not found Cliente FAI", "Not found Cliente FAI"),
  IDENTIFICATIVO_IS_NULL(2026, "Identificativo (param of request) is null", "Identificativo (param of request) is null"),
  NOT_FOUND_ORDINE_CLIENTE(2027, "Not found Ordine Cliente", "Not found Ordine Cliente"),
  NOT_FOUND_CONFIGURATION_DISPOSITIVO_SERVIZIO(2028, "Not found configuration tipoDispositivo and servizio", "Not found configuration tipoDispositivo and servizio"),
  EXCEPTION_FSM_SERVIZIO(2029, "Exception on change status on StatoDispositivoServizio", "Exception on change status on StatoDispositivoServizio"),
  FAILED_UPDATE_DISPOSITIVI_BY_BO(2030, "Failed update dispositivo by BackOffice", "Failed update dispositivo by BackOffice"),
  ERROR_CREATION_DEVICE(2031, "Failed creation device by BackOffice", "Failed creation device by BackOffice"),
  VALIDATION_NUM_DEVICE_TO_LOAD(2032, "Validation of number device to load", "Validation of number device to load"),
  MAX_RANGE_EXCEEDED(2033, "Maximum limit exceeded", "Maximum limit exceeded"),
  NOT_FOUND_CONTRATTO_CLIENTEFAI(2034, "Not found Contratto ClienteFAI", "Not found Contratto ClienteFAI"),
  RANGE_NOT_VALID(2035, "Range start is greater than range finish", "Range start is greater than range finish"),
  EXCEPTION_RESET_STATE_FSM(2036, "Exception on reset FSM state", "Exception on reset FSM state"),
  EXCEPTION_ON_PROCESSING_TRANSIT(2037, "Exception processing TRANSIT", "general exception"),
  TRANSIT_NO_SERIALS_FOUND_ON_INPUT(2038, "No serials found on INPUT", "No serials found on INPUT"),
  DELIVERY_VEICOLO_SENZA_LIBRETTO(2039, "Veicoli senza libretto", "Veicoli senza libretto"),
  VALUE_NOT_ENTERED_IN_CACHE(2040, "Device does not forced to send", "Device does not forced to send"),
  EXCEPTION_ON_PROCESSING_PRINT(2041, "Exception processing Print request", "Exception processing Print request"),
  CRM_INVALID_MODALITA_PAGAMENTO(2042, "NO Modalità di pagamento presente", "NO Modalità di pagamento presente"),
  CRM_INVALID_FIDIUSSIONE(2043, "Fidejussione non presente", "Fidejussione non presente"),
  CRM_INVALID_DOCUMENTO(2044, "Documento di riconoscimento no valido", "Documento di riconoscimento no valido"),
  CRM_INVALID_LICENZA(2045, "Tipo di licenza non in regola", "Tipo di licenza non in regola"),
  CONTRACT_CODE_EXISTS(2046, "Codice contratto già esistente", "Codice contratto già esistente"),
  EXCEPTION_ON_PROCESSING_CHANGE_STATUS(2047, "Eccezzione durante il cambio stato", "Eccezzione durante il cambio stato"),
  CRM_INVALID_FIDO(2048, "Fido non presente", "Fido non presente"),
  STATO_DISPOSITIVO_IS_YELLOW(2053,"Sono presenti dispositivi in stato pending","Sono presenti dispositivi in stato pending"),
  CANNOT_PROCESS_MULTIPLE_DEVICE(2050,"Operazione non consentita per piu dispositivi","Operazione non consentita per piu dispositivi"),
  DEVICE_NOTFOUND(2051,"Dispositivo non trovato","Dispositivo non trovato"),
  DUPLICATE_DEVICE(2052,"Dispositivo duplicato","Dispositivo duplicato"), 
  NOT_FOUND_CONTRATTO(4009, "Contract not found", "Contract not found"), 
  SELECT_MORE_CONTRACT(4023, "It is not possible select more contract for this action", "It is not possible select more contract for this action"), 
  NOT_FOUND_DEVICE_TYPE(4024, "Not found device type", "Not found device type"),
  ;
  //@formatter:on

  private final Integer code;
  private final String  internalMessage;
  private final String  description;

  Errno(Integer code, String internalMessage, String description) {
    this.code = code;
    this.internalMessage = internalMessage;
    this.description = description;
  }

  public Integer code() {
    return code;
  }

  public String internalMessage() {
    return internalMessage;
  }

  public String description() {
    return description;
  }

}
