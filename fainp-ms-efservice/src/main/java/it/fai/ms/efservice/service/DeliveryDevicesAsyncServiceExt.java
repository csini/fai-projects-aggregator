package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;

public interface DeliveryDevicesAsyncServiceExt {

  public String getKeyCacheAndPushDefaultValue(String key);
  
  public void processAsyncDeliveryDevices(String identifierJob, DeliveryPrintDTO dto);
  
}
