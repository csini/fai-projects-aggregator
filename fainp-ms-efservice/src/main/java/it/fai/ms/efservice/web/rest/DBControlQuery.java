package it.fai.ms.efservice.web.rest;


import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;


@RestController
@RequestMapping(DBControlQuery.BASE_PATH)
public class DBControlQuery {
  public static final String BASE_PATH = "/api/control";

  private final DispositivoRepositoryExt dispositivi;
  private final ContrattoRepositoryExt contratti;
  private final OrdineClienteRepositoryExt ordini;
  private final RichiestaRepositoryExt richieste;
  private final ClienteFaiRepository aziende;
  public DBControlQuery(DispositivoRepositoryExt dispositivi, ContrattoRepositoryExt contratti, OrdineClienteRepositoryExt ordini,
                        RichiestaRepositoryExt richieste, ClienteFaiRepository aziende) {
    super();
    this.dispositivi = dispositivi;
    this.contratti   = contratti;
    this.ordini      = ordini;
    this.richieste   = richieste;
    this.aziende     = aziende;
  }


  @GetMapping("count/dispositivi/{codiceAzienda}")
  public Long countDispositiviAzienda(@PathVariable("codiceAzienda") String codiceAzienda) throws UnknownHostException {
    return dispositivi.count(DispositivoRepositoryExt.contrattoOfAzienda(codiceAzienda));
  }


  @GetMapping("count/dispositivi")
  public Long countDispositivi() throws UnknownHostException {
    return dispositivi.count();
  }

//  @GetMapping("count/veicoli")
//  public Long countVeicoli() throws UnknownHostException {
//    return countQuery("veicolo", QueryBuilders.matchAllQuery());
//  }

  @GetMapping("count/contratti")
  public Long countContratti() throws UnknownHostException {
    return contratti.count();
  }

  @GetMapping("count/contratti/{codiceAzienda}")
  public Long countContrattiAzienda(@PathVariable("codiceAzienda") String codiceAzienda) throws UnknownHostException {
   return contratti.count(ContrattoRepositoryExt.ofAzienda(codiceAzienda));
  }
  @GetMapping("count/ordini")
  public Long countOrdini() throws UnknownHostException {
    return ordini.count();
  }

  @GetMapping("count/ordini/{codiceAzienda}")
  public Long countOrdiniAzienda(@PathVariable("codiceAzienda") String codiceAzienda) throws UnknownHostException {
    return ordini.count(OrdineClienteRepositoryExt.ofAzienda(codiceAzienda));
  }
  @GetMapping("count/richieste")
  public Long countRichieste() throws UnknownHostException {
    return richieste.count();
  }

  @GetMapping("count/aziende")
  public Long countAziende() throws UnknownHostException {
    return aziende.count();
  }
}
