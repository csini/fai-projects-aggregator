package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import it.fai.common.enumeration.TipoDispositivoEnum;

public class DeviceAssignationDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private TipoDispositivoEnum tipoDispositivo;
  private String              contratto;
  private String              codiceCliente;
  private Long                quantita;

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getContratto() {
    return contratto;
  }

  public void setContratto(String contratto) {
    this.contratto = contratto;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public Long getQuantita() {
    return quantita;
  }

  public void setQuantita(Long quantita) {
    this.quantita = quantita;
  }

  public DeviceAssignationDTO tipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public DeviceAssignationDTO contratto(String contratto) {
    this.contratto = contratto;
    return this;
  }

  public DeviceAssignationDTO codiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
    return this;
  }

  public DeviceAssignationDTO quantita(Long quantita) {
    this.quantita = quantita;
    return this;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("DeviceAssignationDTO [tipoDispositivo=")
        .append(tipoDispositivo)
        .append(", contratto=")
        .append(contratto)
        .append(", codiceCliente=")
        .append(codiceCliente)
        .append(", quantita=")
        .append(quantita)
        .append("]");
    return builder.toString();
  }

}
