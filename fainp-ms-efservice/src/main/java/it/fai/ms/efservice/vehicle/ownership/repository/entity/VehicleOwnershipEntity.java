package it.fai.ms.efservice.vehicle.ownership.repository.entity;

import java.io.Serializable;
import java.util.Objects;

public class VehicleOwnershipEntity implements Serializable {

  private static final long serialVersionUID = 4721169051554198698L;

  private String companyCodeId;
  private String licensePlate;
  private String vehicleUuid;

  public VehicleOwnershipEntity(final String _vehicleUuid) {
    vehicleUuid = _vehicleUuid;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((VehicleOwnershipEntity) _obj).getVehicleUuid(), vehicleUuid)
                 && Objects.equals(((VehicleOwnershipEntity) _obj).getLicensePlate(), licensePlate)
                 && Objects.equals(((VehicleOwnershipEntity) _obj).getCompanyCodeId(), companyCodeId);
    }
    return isEquals;
  }

  public String getCompanyCodeId() {
    return companyCodeId;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public String getVehicleUuid() {
    return vehicleUuid;
  }

  @Override
  public int hashCode() {
    return Objects.hash(vehicleUuid, licensePlate, companyCodeId);
  }

  public void setCompanyCode(final String _companyCode) {
    companyCodeId = _companyCode;
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnershipEntity [companyCodeId=");
    builder.append(this.companyCodeId);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append(", vehicleUuid=");
    builder.append(this.vehicleUuid);
    builder.append("]");
    return builder.toString();
  }

}
