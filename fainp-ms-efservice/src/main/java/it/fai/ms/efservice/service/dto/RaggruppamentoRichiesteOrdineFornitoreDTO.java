package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.efservice.enumeration.StatoRaggruppamento;

/**
 * A DTO for the RaggruppamentoRichiesteOrdineFornitore entity.
 */
public class RaggruppamentoRichiesteOrdineFornitoreDTO implements Serializable {

  private Long id;

  private String nomeFileRicevuto;

  private Instant dataGenerazioneFile;

  private Instant dataUltimaAcquisizione;

  private Long tipoDispositivoId;

  private String tipoDispositivoNome;

  private Long numeroDispositiviOrdinati;

  private Long numeroDispositiviAcquisiti;

  private StatoRaggruppamento statoRaggruppamento;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomeFileRicevuto() {
    return nomeFileRicevuto;
  }

  public void setNomeFileRicevuto(String nomeFileRicevuto) {
    this.nomeFileRicevuto = nomeFileRicevuto;
  }

  public Instant getDataGenerazioneFile() {
    return dataGenerazioneFile;
  }

  public void setDataGenerazioneFile(Instant dataGenerazioneFile) {
    this.dataGenerazioneFile = dataGenerazioneFile;
  }

  public Instant getDataUltimaAcquisizione() {
    return dataUltimaAcquisizione;
  }

  public void setDataUltimaAcquisizione(Instant dataUltimaAcquisizione) {
    this.dataUltimaAcquisizione = dataUltimaAcquisizione;
  }

  public Long getTipoDispositivoId() {
    return tipoDispositivoId;
  }

  public void setTipoDispositivoId(Long tipoDispositivoId) {
    this.tipoDispositivoId = tipoDispositivoId;
  }

  public String getTipoDispositivoNome() {
    return tipoDispositivoNome;
  }

  public void setTipoDispositivoNome(String tipoDispositivoNome) {
    this.tipoDispositivoNome = tipoDispositivoNome;
  }

  public Long getNumeroDispositiviOrdinati() {
    return numeroDispositiviOrdinati;
  }

  public void setNumeroDispositiviOrdinati(Long numeroDispositiviOrdinati) {
    this.numeroDispositiviOrdinati = numeroDispositiviOrdinati;
  }

  public Long getNumeroDispositiviAcquisiti() {
    return numeroDispositiviAcquisiti;
  }

  public void setNumeroDispositiviAcquisiti(Long numeroDispositiviAcquisiti) {
    this.numeroDispositiviAcquisiti = numeroDispositiviAcquisiti;
  }

  public StatoRaggruppamento getStatoRaggruppamento() {
    return statoRaggruppamento;
  }

  public void setStatoRaggruppamento(StatoRaggruppamento statoRaggruppamento) {
    this.statoRaggruppamento = statoRaggruppamento;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RaggruppamentoRichiesteOrdineFornitoreDTO RaggruppamentoRichiesteOrdineFornitoreDTO = (RaggruppamentoRichiesteOrdineFornitoreDTO) o;
    if (RaggruppamentoRichiesteOrdineFornitoreDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), RaggruppamentoRichiesteOrdineFornitoreDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "RaggruppamentoRichiesteOrdineFornitoreDTO{" + "id=" + id + ", nomeFileRicevuto='" + nomeFileRicevuto + '\''
           + ", dataGenerazioneFile=" + dataGenerazioneFile + ", dataUltimaAcquisizione=" + dataUltimaAcquisizione + ", tipoDispositivoId="
           + tipoDispositivoId + ", tipoDispositivoNome='" + tipoDispositivoNome + '\'' + ", numeroDispositiviOrdinati="
           + numeroDispositiviOrdinati + ", numeroDispositiviAcquisiti=" + numeroDispositiviAcquisiti + ", statoRaggrupamento='"
           + statoRaggruppamento + '\'' + '}';
  }
}
