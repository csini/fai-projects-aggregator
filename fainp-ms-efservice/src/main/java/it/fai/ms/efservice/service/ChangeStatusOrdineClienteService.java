package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Set;

import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

public interface ChangeStatusOrdineClienteService {

  String calculateAndChangeStatus(String identificativoOrdineCliente);

  Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIs(StatoOrdineCliente stato);

  Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIsAndAlreadyInCache(StatoOrdineCliente completato, List<String> keysId);

}
