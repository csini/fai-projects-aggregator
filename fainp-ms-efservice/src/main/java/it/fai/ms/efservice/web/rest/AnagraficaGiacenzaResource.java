package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.AnagraficaGiacenzaService;
import it.fai.ms.efservice.web.rest.errors.BadRequestAlertException;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.AnagraficaGiacenzaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnagraficaGiacenza.
 */
@RestController
@RequestMapping("/api")
public class AnagraficaGiacenzaResource {

    private final Logger log = LoggerFactory.getLogger(AnagraficaGiacenzaResource.class);

    private static final String ENTITY_NAME = "anagraficaGiacenza";

    private final AnagraficaGiacenzaService anagraficaGiacenzaService;

    public AnagraficaGiacenzaResource(AnagraficaGiacenzaService anagraficaGiacenzaService) {
        this.anagraficaGiacenzaService = anagraficaGiacenzaService;
    }

    /**
     * POST  /anagrafica-giacenzas : Create a new anagraficaGiacenza.
     *
     * @param anagraficaGiacenzaDTO the anagraficaGiacenzaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new anagraficaGiacenzaDTO, or with status 400 (Bad Request) if the anagraficaGiacenza has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/anagrafica-giacenzas")
    @Timed
    public ResponseEntity<AnagraficaGiacenzaDTO> createAnagraficaGiacenza(@RequestBody AnagraficaGiacenzaDTO anagraficaGiacenzaDTO) throws URISyntaxException {
        log.debug("REST request to save AnagraficaGiacenza : {}", anagraficaGiacenzaDTO);
        if (anagraficaGiacenzaDTO.getId() != null) {
            throw new BadRequestAlertException("A new anagraficaGiacenza cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnagraficaGiacenzaDTO result = anagraficaGiacenzaService.save(anagraficaGiacenzaDTO);
        return ResponseEntity.created(new URI("/api/anagrafica-giacenzas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /anagrafica-giacenzas : Updates an existing anagraficaGiacenza.
     *
     * @param anagraficaGiacenzaDTO the anagraficaGiacenzaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated anagraficaGiacenzaDTO,
     * or with status 400 (Bad Request) if the anagraficaGiacenzaDTO is not valid,
     * or with status 500 (Internal Server Error) if the anagraficaGiacenzaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/anagrafica-giacenzas")
    @Timed
    public ResponseEntity<AnagraficaGiacenzaDTO> updateAnagraficaGiacenza(@RequestBody AnagraficaGiacenzaDTO anagraficaGiacenzaDTO) throws URISyntaxException {
        log.debug("REST request to update AnagraficaGiacenza : {}", anagraficaGiacenzaDTO);
        if (anagraficaGiacenzaDTO.getId() == null) {
            return createAnagraficaGiacenza(anagraficaGiacenzaDTO);
        }
        AnagraficaGiacenzaDTO result = anagraficaGiacenzaService.save(anagraficaGiacenzaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, anagraficaGiacenzaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /anagrafica-giacenzas : get all the anagraficaGiacenzas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of anagraficaGiacenzas in body
     */
    @GetMapping("/anagrafica-giacenzas")
    @Timed
    public List<AnagraficaGiacenzaDTO> getAllAnagraficaGiacenzas() {
        log.debug("REST request to get all AnagraficaGiacenzas");
        return anagraficaGiacenzaService.findAll();
        }

    /**
     * GET  /anagrafica-giacenzas/:id : get the "id" anagraficaGiacenza.
     *
     * @param id the id of the anagraficaGiacenzaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the anagraficaGiacenzaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/anagrafica-giacenzas/{id}")
    @Timed
    public ResponseEntity<AnagraficaGiacenzaDTO> getAnagraficaGiacenza(@PathVariable Long id) {
        log.debug("REST request to get AnagraficaGiacenza : {}", id);
        AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(anagraficaGiacenzaDTO));
    }

    /**
     * DELETE  /anagrafica-giacenzas/:id : delete the "id" anagraficaGiacenza.
     *
     * @param id the id of the anagraficaGiacenzaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/anagrafica-giacenzas/{id}")
    @Timed
    public ResponseEntity<Void> deleteAnagraficaGiacenza(@PathVariable Long id) {
        log.debug("REST request to delete AnagraficaGiacenza : {}", id);
        anagraficaGiacenzaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
