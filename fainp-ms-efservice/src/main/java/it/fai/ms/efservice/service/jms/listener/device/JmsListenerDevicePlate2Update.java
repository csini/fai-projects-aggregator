package it.fai.ms.efservice.service.jms.listener.device;

import java.util.Optional;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceLicensePlateMessage;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;

@Service(JmsListenerDevicePlate2Update.QUALIFIER)
@Transactional
public class JmsListenerDevicePlate2Update implements MessageListener {

  public final static String QUALIFIER = "jmsListenerDevicePlate2Update";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt dispositivoRepository;

  public JmsListenerDevicePlate2Update(DispositivoRepositoryExt _dispositivoRepository) {
    dispositivoRepository = _dispositivoRepository;
  }

  @Override
  public void onMessage(Message message) {
    try {
      DeviceLicensePlateMessage dto = (DeviceLicensePlateMessage) ((ObjectMessage)message).getObject();
      _log.debug("Received message : {}", dto);

      synchronized (dispositivoRepository) {
        Optional<Dispositivo> opt = dispositivoRepository.findByCodiceContrattoAndSeriale(dto.getContractUuid().getUuid(), dto.getSupportCode());
        if (opt.isPresent()) {
          Dispositivo device = opt.get();
          if (dto.getLicensePosition() == 2) {
            device.setTarga2(dto.getLicensePlate() != null ? dto.getLicensePlate().getLicenseId() : null);
            dispositivoRepository.saveAndFlush(device);
            return;
          }
          if (dto.getLicensePosition() == 3) {
            device.setTarga3(dto.getLicensePlate() != null ? dto.getLicensePlate().getLicenseId() : null);
            dispositivoRepository.saveAndFlush(device);
            return;
          }
        }
      }

    } catch (Exception e) {
      _log.error("JmsListenerDevicePlate2Update", e);
    }    
  }

}
