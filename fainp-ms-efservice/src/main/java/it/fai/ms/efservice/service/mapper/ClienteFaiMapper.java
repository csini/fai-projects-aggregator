package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClienteFai and its DTO ClienteFaiDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClienteFaiMapper extends EntityMapper<ClienteFaiDTO, ClienteFai> {


    @Mapping(target = "contrattos", ignore = true)
    ClienteFai toEntity(ClienteFaiDTO clienteFaiDTO);

    default ClienteFai fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClienteFai clienteFai = new ClienteFai();
        clienteFai.setId(id);
        return clienteFai;
    }
}
