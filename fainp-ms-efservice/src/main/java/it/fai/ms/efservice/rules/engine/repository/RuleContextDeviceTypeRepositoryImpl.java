package it.fai.ms.efservice.rules.engine.repository;

import static java.util.stream.Collectors.toSet;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;

@Service
public class RuleContextDeviceTypeRepositoryImpl implements RuleContextDeviceTypeRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final String CACHE_KEY = "TIPO_DISPOSITIVO";

  private HashMap<String, Set<RuleEngineDeviceType>> cache = null;

  private final TipoDispositivoRepository deviceTypeRepository;

  public RuleContextDeviceTypeRepositoryImpl(final TipoDispositivoRepository _deviceTypeRepository) {
    deviceTypeRepository = _deviceTypeRepository;
    cache = new HashMap<>();
  }

  @Override
  @Transactional(readOnly = true)
  public Set<RuleEngineDeviceType> findAll() {

    if (!cache.containsKey(CACHE_KEY)) {
      _log.info("Search....");
      cache.put(CACHE_KEY, deviceTypeRepository.findAll()
                                               .stream()
                                               .filter(td -> !td.isHidden())
                                               .map(deviceType -> mapToRuleContextDeviceType(deviceType))
                                               .collect(toSet()));
    }

    final Set<RuleEngineDeviceType> ruleContextDeviceTypes = cache.get(CACHE_KEY);
    _log.info("Found {} elements", (ruleContextDeviceTypes != null) ? ruleContextDeviceTypes.size() : 0);
    return ruleContextDeviceTypes;
  }

  @Override
  @Transactional(readOnly = true)
  public RuleEngineDeviceType newStarContextDeviceType() {
    return new RuleEngineDeviceType(RuleEngineDeviceTypeId.ofWildcard());
  }

  private RuleEngineDeviceType mapToRuleContextDeviceType(final TipoDispositivo _deviceType) {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId(_deviceType.getNome()
                                                                          .name()));
  }

}
