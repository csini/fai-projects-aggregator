/**
 *
 */
package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;



public class FsmActionGeneric implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      if (logger.isDebugEnabled()) {
        logger.debug("Generic FSM Action to Richiesta [ID: " + ((richiesta != null) ? richiesta.getId() : null) + "]");
      }
    }
  }

}
