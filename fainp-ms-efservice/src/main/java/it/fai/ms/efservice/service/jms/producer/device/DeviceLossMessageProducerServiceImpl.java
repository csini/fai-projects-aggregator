package it.fai.ms.efservice.service.jms.producer.device;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessageFactory;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ObuType;
import it.fai.ms.common.jms.efservice.message.device.AbstractDeviceMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceLossMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceTheftMessage;
import it.fai.ms.common.jms.efservice.message.device.ObuDeviceInfo;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class DeviceLossMessageProducerServiceImpl implements DeviceLossMessageProducerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public DeviceLossMessageProducerServiceImpl(final JmsProperties _jmsProperties,
                                              final JmsSenderQueueMessageService _jmsSenderQueueService) {
    jmsProperties         = _jmsProperties;
    jmsSenderQueueService = _jmsSenderQueueService;
  }

  @Override
  public void deactivateDevice(final Richiesta _orderRequestEntity) {
    TipoRichiesta requestType = _orderRequestEntity.getTipo();
    JmsTopicNames jmsTopic    = null;
    _log.info("Request type: {}", requestType);
    try {
      switch (requestType) {
        case FURTO:
        case FURTO_CON_SOSTITUZIONE:
          jmsTopic = JmsTopicNames.DEVICE_THEFT;
          break;

        case SMARRIMENTO:
        case SMARRIMENTO_CON_SOSTITUZIONE:
          jmsTopic = JmsTopicNames.DEVICE_LOSS;
          break;

        default:
          _log.warn("Not send message because Request type is {}", requestType);
          break;
      }
      if (jmsTopic == null) {
        return;
      }
      publishMessage(_orderRequestEntity, jmsTopic);
    } catch (Exception _e) {
      String messageError = String.format("Message not published to richiesta [%s - Type: %s]", _orderRequestEntity.getIdentificativo(), requestType);
      _log.error(messageError, _e);
      throw new RuntimeException(_e);
    }
  }

  @Override
  public void deactivatedDeviceTelepassIta(final Richiesta richiesta) {
    DeviceEventMessage eventMessage = buildDeviceEventMessage(richiesta);
    jmsSenderQueueService.sendLossMessage(eventMessage);
  }

  private DeviceEventMessage buildDeviceEventMessage(Richiesta richiesta) {
    OrderRequestUtil      orderRequestUtil   = new OrderRequestUtil(richiesta);
    OrderRequestDecorator orderRequest       = new OrderRequestDecorator(richiesta);
    DeviceEventMessage    deviceEventMessage = DeviceEventMessageFactory
      .createDeviceLossMessage(new DeviceDataDTO().contractCode(orderRequest.findNumeroContract())
        .requestId(orderRequest.getIdentificativo())
        .deviceCode(orderRequest.getDeviceSerialId())
        .deviceType(orderRequestUtil.getObuType().getId().name()));
    return deviceEventMessage;
  }

  private AbstractDeviceMessage publishMessage(Richiesta _orderRequestEntity, JmsTopicNames jmsTopic) {
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(_orderRequestEntity);
    AbstractDeviceMessage       message      = null;
    switch (jmsTopic) {
      case DEVICE_LOSS:
        message = new DeviceLossMessage(new ContractOrderRequest(orderRequest.findNumeroContract(),
                                                                 orderRequest.findCodiceCliente()),
                                        new ObuDeviceInfo(new ObuNumber(orderRequest.getDeviceSerialId()),
                                                          ObuType.fromString(getHardwareType(orderRequest))));
        break;

      case DEVICE_THEFT:
        message = new DeviceTheftMessage(new ContractOrderRequest(orderRequest.findNumeroContract(),
                                                                  orderRequest.findCodiceCliente()),
                                         new ObuDeviceInfo(new ObuNumber(orderRequest.getDeviceSerialId()),
                                                           ObuType.fromString(getHardwareType(orderRequest))));
        break;
      default:
        _log.warn("NOT managed JMS TOPIC: {}", jmsTopic);
        break;
    }
    final AbstractDeviceMessage finalMessage = message;
    if (finalMessage != null)
      finalMessage.getServices().addAll(orderRequest.getServices());
      publishOnJms(_orderRequestEntity, jmsTopic, message);

    return finalMessage;
  }

  private String getHardwareType(final OrderRequestDecorator orderRequest) {
    String hardwareDeviceType = orderRequest.getHardwareDeviceType();
    if (StringUtils.isEmpty(hardwareDeviceType)) {
      _log.warn("hardware type not specified for device {}", orderRequest);
      switch (orderRequest.getDeviceType()) {
        case TELEPASS_EUROPEO:
          hardwareDeviceType = "AF";// AF=Q-free (Telepass EU)
          break;
        case TELEPASS_EUROPEO_SAT:
          hardwareDeviceType = "AA";// AA=Arianna2
          break;
        // AD=Dual (dm02)
        // AZ= Reets (dm03)
        default:
          throw new RuntimeException("Cannot determine tipo hardware for device type" + orderRequest.getDeviceType());
      }
    }
    return hardwareDeviceType;
  }

  private void publishOnJms(Richiesta _orderRequestEntity, JmsTopicNames jmsTopic, AbstractDeviceMessage message) {
    message.getServices().addAll(DeviceService.fromDeviceServiceNames(this.findActiveServicesNamesLocal(_orderRequestEntity)));
    _log.info("Message produced : {} ", message);
    JmsTopicSenderUtil.publish(jmsProperties, jmsTopic, message);
  }

  private List<String> findActiveServicesNamesLocal(final Richiesta _orderRequestEntity) {
    final List<String> activeServicesNames = _orderRequestEntity.getDispositivos()
      .stream()
      .findFirst()
      .get()
      .getStatoDispositivoServizios()
      .stream()
      .filter(deviceServiceState -> deviceServiceState.getStato().equals(StatoDS.ATTIVO))
      .map(deviceServiceState -> deviceServiceState.getTipoServizio())
      .map(serviceType -> serviceType.getNome())
      .collect(toList());
    _log.info("OrderRequestDecorator {} active services names : {}", _orderRequestEntity.getIdentificativo(),
              activeServicesNames);
    return activeServicesNames;
  }

}
