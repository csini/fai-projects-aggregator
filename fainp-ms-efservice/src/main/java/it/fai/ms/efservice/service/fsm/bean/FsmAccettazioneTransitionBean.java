/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

/**
 * @author Luca Vassallo
 */
@WithStateMachine(id = "fsmAccettazione")
public class FsmAccettazioneTransitionBean extends AbstractFsmRichiestaTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @StatesOnTransition(source = StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, target = StatoRichiesta.SOSPESA)
  public void switchFromPortaleToVerifica(StateContext<StatoRichiesta, RichiestaEvent> stateContext) {
    // Add implementation if necessary;
  }

}
