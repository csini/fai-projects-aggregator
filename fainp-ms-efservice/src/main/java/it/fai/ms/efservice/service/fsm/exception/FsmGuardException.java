package it.fai.ms.efservice.service.fsm.exception;

public class FsmGuardException extends Exception {

  private static final long serialVersionUID = -7451640233444268733L;

  public FsmGuardException() {
    this(FsmGuardException.class.toString());
  }

  public FsmGuardException(String message, Throwable cause) {
    super(message, cause);
  }

  public FsmGuardException(String message) {
    super(message);
  }

  public FsmGuardException(Throwable cause) {
    super(cause);
  }

}
