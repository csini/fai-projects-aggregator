package it.fai.ms.efservice.service.fsm;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.fsm.util.FsmByTipoDispositivoService;

@Service
@Transactional
public class FsmRichiestaUtils {

  private final Logger log = LoggerFactory.getLogger(FsmRichiestaUtils.class);

  private FsmByTipoDispositivoService fsmDeviceTypeService;
  private RichiestaRepository         richiestaRepo;
  private RichiestaRepositoryExt      richiestaRepoExt;
  private OrdineClienteServiceExt     ordineClienteServiceExt;

  public FsmRichiestaUtils(final FsmByTipoDispositivoService _fsmDeviceTypeService, final RichiestaRepository _richiestaRepo,
                           final RichiestaRepositoryExt _richiestaRepoExt, final OrdineClienteServiceExt _ordineClienteServiceExt) {
    fsmDeviceTypeService = _fsmDeviceTypeService;
    richiestaRepo = _richiestaRepo;
    richiestaRepoExt = _richiestaRepoExt;
    ordineClienteServiceExt = _ordineClienteServiceExt;
  }

  public boolean changeStatusRichieste(Set<String> identificativoRichieste, String operazione, String motivoSospensione,
                                       String nota) throws FsmExecuteCommandException {
    long tStart = System.currentTimeMillis();
    Set<Richiesta> richiesteWithStatusChanged = new HashSet<>();
    List<Richiesta> richieste = richiestaRepoExt.findAllByIdentificativoWithEagerOrdineClienteClienteFaiDispositivos(new ArrayList<>(identificativoRichieste));
    for (Richiesta request : richieste) {
      Richiesta richiestaChanged = changeStatusToRichiesta(request, operazione, motivoSospensione, nota);
      StatoRichiesta stato = richiestaChanged.getStato();
      log.info("=> Changed status Richiesta [identificativo: {} - Associazione: {} - TipoDispositivo {}] to {} by command: {}",
               request.getIdentificativo(), request.getAssociazione(), request.getTipoDispositivo(), stato, operazione);
      richiesteWithStatusChanged.add(richiestaChanged);
    }

    int numRichiesteElaborate = identificativoRichieste.size();
    richiestaRepo.save(richiesteWithStatusChanged);
    log.info("Change status of {} Richieste in {} ms", numRichiesteElaborate, (System.currentTimeMillis() - tStart));
    return true;
  }

  public Richiesta changeStatusToRichiesta(Richiesta richiesta, String operazione, String motivoSospensione,
                                           String nota) throws FsmExecuteCommandException {
    long ts = System.currentTimeMillis();
    nota = StringUtils.defaultString(nota);
    String identificativo = richiesta.getIdentificativo();
    log.debug("Actual status richiesta {}", richiesta.getStato());
    String anomalia = motivoSospensione + (StringUtils.isNotBlank(nota) ? " - " + nota : "");
    richiesta.setAnomalia(anomalia);
    richiesta.setMotivoSospensione(motivoSospensione);
    richiesta.setUltimaNota(nota);
    RichiestaEvent event = RichiestaEvent.valueOf(operazione);
    log.debug("Event {}", event);
    try {
      richiesta = executeFsm(null, richiesta, event);
    } catch (Exception e) {
      throw new FsmExecuteCommandException(e);
    }
    log.debug("Change status for Richiesta {} in {} ms", identificativo, (System.currentTimeMillis() - ts));
    return richiesta;
  }

  private Richiesta executeFsm(AbstractFsmRichiesta fsm, Richiesta richiesta, RichiestaEvent event) throws FsmExecuteCommandException {
    if (fsm == null) {
      fsm = retrieveFsmByRichiesta(richiesta);
    }

    if (fsm != null) {
      richiesta = fsm.executeCommandToChangeState(event, richiesta);
      log.debug("Executed change status richiesta {} with command {}", richiesta.getIdentificativo(), event.name());
    } else {
      log.warn("Dispositivo non gestito => FSM not found");
    }

    return richiesta;
  }

  public Richiesta resetStatusFsm(String identificativo, StatoRichiesta stato) {
    Richiesta richiesta = findRichiesta(identificativo);
    AbstractFsmRichiesta fsm = retrieveFsmByRichiesta(richiesta);

    fsm.resetStato(richiesta);

    return richiesta;
  }

  public Richiesta executeFsmAndPersist(AbstractFsmRichiesta fsm, Richiesta richiesta,
                                        RichiestaEvent event) throws FsmExecuteCommandException {
    richiesta = executeFsm(fsm, richiesta, event);
    richiesta = richiestaRepo.save(richiesta);
    log.info("Saved richiesta {}", richiesta);
    return richiesta;
  }

  public AbstractFsmRichiesta retrieveFsmByRichiesta(Richiesta richiesta) {

    Long idRichiesta = richiesta.getId();
    String identificativoRichiesta = richiesta.getIdentificativo();

    TipoRichiesta tipo = richiesta.getTipo();
    if (tipo == null) {
      log.error("Tipo Richiesta [ID: {} - Identificativo: {}] is {}", idRichiesta, identificativoRichiesta, tipo);
      throw new ValidationException("Not found Tipo for RIchiesta [ID: " + idRichiesta + " - Identificativo: " + identificativoRichiesta
                                    + "]");
    }
    log.debug("Tipo Richiesta {}", tipo);
    if (tipo == TipoRichiesta.NUOVO_ORDINE) {
      StatoRichiesta statoRichiesta = richiesta.getStato();
      switch (statoRichiesta) {
      case IN_ATTESA_DI_LAVORAZIONE:
      case SOSPESA:
        return fsmDeviceTypeService.getFsmAccettazione();
      default:
        log.debug("Not use FSM Accettazione");
        break;
      }
    }

    TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
    if (tipoDispositivo == null) {
      log.error("Tipo DISPOSITIVO for Richiesta [ID: {} - Identificativo: {}] is {}", idRichiesta, identificativoRichiesta,
                tipoDispositivo);
      throw new ValidationException("Not found Tipo DISPOSITIVO for Richiesta [ID: " + idRichiesta + " - Identificativo: "
                                    + identificativoRichiesta);
    }
    TipoDispositivoEnum tipoDispEnum = tipoDispositivo.getNome();
    if (tipoDispEnum == null) {
      log.error("Tipo DISPOSITIVO ENUM for Richiesta [ID: {} - Identificativo: {}] is {}", idRichiesta, identificativoRichiesta,
                tipoDispEnum);
      throw new ValidationException("Not found Tipo DISPOSITIVO Enum for Richiesta [ID: " + idRichiesta + " - Identificativo: "
                                    + identificativoRichiesta);
    }

    AbstractFsmRichiesta fsm = getFsmByTipoRichiestaAndTipoDispositivo(tipo, tipoDispEnum);
    return fsm;
  }

  private AbstractFsmRichiesta getFsmByTipoRichiestaAndTipoDispositivo(TipoRichiesta tipologiaRichiesta,
                                                                       TipoDispositivoEnum tipoDispositivo) {
    AbstractFsmRichiesta fsm = null;
    switch (tipologiaRichiesta) {
    case FURTO:
      fsm = fsmDeviceTypeService.getFsmFurtoByTipoDispositivo(tipoDispositivo);
      break;

    case FURTO_CON_SOSTITUZIONE:
      fsm = fsmDeviceTypeService.getFsmFurtoSostituzioneByTipoDispositivo(tipoDispositivo);
      break;

    case SMARRIMENTO:
      fsm = fsmDeviceTypeService.getFsmSmarrimentoByTipoDispositivo(tipoDispositivo);
      break;

    case SMARRIMENTO_CON_SOSTITUZIONE:
      fsm = fsmDeviceTypeService.getFsmSmarrimentoSostituzioneByTipoDispositivo(tipoDispositivo);
      break;

    case MEZZO_RITARGATO:
      fsm = fsmDeviceTypeService.getFsmMezzoRitargatoByTipoDispositivo(tipoDispositivo);
      break;

    case VARIAZIONE_TARGA:
      fsm = fsmDeviceTypeService.getFsmVariazioneTargaByTipoDispositivo(tipoDispositivo);
      break;

    case MALFUNZIONAMENTO:
      fsm = fsmDeviceTypeService.getFsmMalfunzionamentoByTipoDispositivo(tipoDispositivo);
      break;

    case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
      fsm = fsmDeviceTypeService.getFsmMalfunzionamentoSostituzioneByTipoDispositivo(tipoDispositivo);
      break;

    case RESCISSIONE:
    case RIATTIVAZIONE:
    case SOSPENSIONE:
      // TODO;
      log.error("Tipologia Richiesta [" + tipologiaRichiesta.name() + "] to do implements");
      break;

    case NUOVO_ORDINE:
    case ATTIVAZIONE_SERVIZIO:
    case DISATTIVAZIONE_SERVIZIO:
      fsm = fsmDeviceTypeService.getFsmNewOrderByTipoDispositivo(tipoDispositivo);
      if (tipologiaRichiesta.equals(TipoRichiesta.ATTIVAZIONE_SERVIZIO) && tipoDispositivo.equals(TipoDispositivoEnum.TOLL_COLLECT)) {
        log.info("FsmCommand to activation service on TOLL_COLLECT");
        fsm = fsmDeviceTypeService.getFsmActiveServiceByTipoDispositivo(tipoDispositivo);
      }
      break;

    case DISATTIVAZIONE:
      fsm = fsmDeviceTypeService.getFsmDisattivazione(tipoDispositivo);
      break;

    default:
      log.error("Tipologia Richiesta [" + tipologiaRichiesta.name() + "] is not managed");
      break;
    }

    log.info("Found FSM for tipo richiesta {} and tipo dispositivo {}: {}", tipologiaRichiesta, tipoDispositivo,
             (fsm != null) ? fsm.getClass()
                                .getName()
                           : null);
    return fsm;
  }

  public Richiesta findRichiesta(String identificativoRichiesta) {
    Richiesta richiesta = richiestaRepoExt.findOneByIdentificativoWithEagerOrdineClienteClienteFaiDispositivos(identificativoRichiesta);
    return richiesta;
  }

  public Richiesta findOnlyRichiesta(String identificativoRichiesta) {
    Richiesta richiesta = richiestaRepoExt.findOneByIdentificativo(identificativoRichiesta);
    return richiesta;
  }

  public List<String> getAvailableCommandByRichiestaId(Long id, boolean forUser) {
    Richiesta richiesta = richiestaRepo.findOne(id);
    if (richiesta == null) {
      return null;
    }
    return getAvailableCommandByRichiesta(richiesta, forUser);
  }

  public List<String> getAvailableCommandByIdentificativoRichiesta(String identificativo, boolean forUser) {
    Richiesta richiesta = richiestaRepoExt.findOneByIdentificativo(identificativo);
    if (richiesta == null) {
      return null;
    }
    return getAvailableCommandByRichiesta(richiesta, forUser);
  }

  public void setOperazioniPossibiliOnRichiesta(String identificativo) {
    Richiesta richiesta = findRichiesta(identificativo);
    AbstractFsmRichiesta fsm = retrieveFsmByRichiesta(richiesta);
    if (fsm != null) {
      richiesta = fsm.getUserAvailableCommandAndSetOnRichiesta(richiesta);
      richiestaRepoExt.save(richiesta);
    }
  }

  private List<String> getAvailableCommandByRichiesta(Richiesta richiesta, boolean forUser) {
    List<String> availableCommand = null;
    AbstractFsmRichiesta fsm = retrieveFsmByRichiesta(richiesta);
    if (fsm != null) {
      if (forUser) {
        availableCommand = fsm.getUserAvailableCommand(richiesta);
      } else {
        Collection<RichiestaEvent> allAvailableCommand = fsm.getAllAvailableCommand(richiesta);
        availableCommand = allAvailableCommand.stream()
                                              .map(event -> event.name())
                                              .collect(toList());
      }
    }
    log.debug("Found by fsm {} these commands available {}: {}", fsm, (forUser) ? "for user" : "", availableCommand);
    return availableCommand;
  }

  public List<Richiesta> findRichiestaByContrattoAndTargaNazioneAndStato(String codiceContratto, String targa, String country,
                                                                         StatoRichiesta stato) {
    return richiestaRepoExt.findByCodiceContrattoAndTargaAndStato(codiceContratto, targa, country, stato);
  }

  public Optional<Richiesta> findRichiestaByStatoAndIdentificativoDispositivo(StatoRichiesta stato, String identificativoDevice) {
    Optional<Richiesta> optRichiesta = richiestaRepoExt.findByStatoAndDispositivo(stato, identificativoDevice);
    log.debug("Found optional Richiesta: {}", optRichiesta);
    return optRichiesta;
  }

  public List<Richiesta> findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(String codiceContratto, String serialNumberDevice,
                                                                                  StatoRichiesta stato, Set<TipoRichiesta> requestTypes) {
    return richiestaRepoExt.findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(codiceContratto, serialNumberDevice, stato,
                                                                                     requestTypes);
  }

  public List<Richiesta> findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(String licensePlate, String countryLicensePlate,
                                                                                     TipoDispositivoEnum deviceType,
                                                                                     StatoRichiesta statoRichiesta) {
    List<Richiesta> richieste = richiestaRepoExt.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(licensePlate,
                                                                                                             countryLicensePlate,
                                                                                                             deviceType, statoRichiesta);
    log.debug("Found Richieste: {}", (richieste != null) ? richieste.size() : null);
    return richieste;
  }

  public OrdineCliente saveOrdineCliente(OrdineCliente ordineCliente) {
    ordineCliente = ordineClienteServiceExt.save(ordineCliente);
    return ordineCliente;
  }

  public Richiesta saveRichiesta(Richiesta richiesta) {
    return richiestaRepo.save(richiesta);
  }

  public Richiesta saveAndFlushRichiesta(Richiesta richiesta) {
    return richiestaRepo.saveAndFlush(richiesta);
  }

}
