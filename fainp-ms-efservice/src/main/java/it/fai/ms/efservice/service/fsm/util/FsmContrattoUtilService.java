package it.fai.ms.efservice.service.fsm.util;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

@Service
public class FsmContrattoUtilService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  final private JmsTopicSenderService senderJmsService;

  /**
   * @param _senderJmsService
   */
  public FsmContrattoUtilService(final JmsTopicSenderService _senderJmsService) {
    this.senderJmsService = _senderJmsService;
  }

  public boolean switchToState(Contratto contratto, StatoContratto toState, Transition<StatoContratto, ContrattoEvent> transition) {
    boolean isSwitch = switchToState(contratto, toState);
    if (isSwitch) {
      Trigger<StatoContratto, ContrattoEvent> trigger = transition.getTrigger();
      boolean isAutoTransition = false;
      if (trigger == null || (trigger != null && trigger.getEvent()
                                                        .name()
                                                        .startsWith("MS_"))) {
        isAutoTransition = true;
      }

      boolean isAutoTx = isAutoTransition;
      sendNotificationByTransitionType(isAutoTx, contratto, transition);

    } else {
      return false;
    }

    return true;
  }

  private boolean sendNotificationByTransitionType(boolean isAutoTx, Contratto contratto,
                                                   Transition<StatoContratto, ContrattoEvent> transition) {
    log.debug("Invio Notifica di cambio stato " + ((isAutoTx) ? "AUTOMATICO" : "MANUALE") + ".");
    StatoContratto stateFrom = null;
    StatoContratto stateTo = null;

    stateFrom = transition.getSource()
                          .getId();
    stateTo = transition.getTarget()
                        .getId();

    LocalDate now = LocalDate.now();
    boolean sent = false;
    if (isAutoTx) {
      sent = sendNotification(contratto, now, stateFrom, stateTo);
    } else {
      String faiOperator = SecurityUtils.getCurrentUserLogin();
      sent = sendNotification(contratto, now, stateFrom, stateTo, faiOperator);
    }
    return sent;
  }

  private boolean sendNotification(Contratto contratto, LocalDate now, StatoContratto stateFrom, StatoContratto stateTo) {
    return sendNotification(contratto, now, stateFrom, stateTo, null);
  }

  private boolean sendNotification(Contratto contratto, LocalDate now, StatoContratto stateFrom, StatoContratto stateTo,
                                   String operatoreFai) {
    if (contratto == null) {
      log.error("Not found Richiesta {} for change status from {} to {}", stateFrom, stateTo);
      return false;
    }
    String codContrattoCliente = contratto.getCodContrattoCliente();
    log.debug("INFO TRANSITION:\n\t- Contratto: {}\n\t- Data: {}\n\t- Transizione => Da: [{}] A: [{}]\n\t- UltimaNota: {}\n\t- OperatoreFai: {}",
              codContrattoCliente, new Date(), stateFrom, stateTo, null, operatoreFai);

    ClienteFai clienteFai = contratto.getClienteFai();
    if (clienteFai == null) {
      log.error("Error in codice cliente FAI: [Contratto: {}]", contratto);
      return false;
    }

    // Richiesta numero <ID_RICHIESTA> cambia stato in <STATO_DESTINAZIONE>. Autore della modifica:
    // <SISTEMA_OPPURE_OPERATORE_FAI>. Note: <NOTE>
    StringBuilder sb = new StringBuilder("Cambio stato da " + stateFrom.name() + " ");
    sb.append("a " + stateTo.name() + ". ");
    sb.append("Autore della modifica: " + ((StringUtils.isNotBlank(operatoreFai)) ? operatoreFai : "Sistema"));
    sb.append(". Contratto[" + codContrattoCliente + "]");
    String message = sb.toString();

    log.debug(message);

    return true;
  }

  private boolean switchToState(Contratto contratto, StatoContratto toState) {
    if (contratto != null) {
      contratto.setStato(toState);
      Instant dataModificaStato = Instant.now();
      contratto.setDataModificaStato(dataModificaStato);
    } else {
      return false;
    }

    return true;
  }

}
