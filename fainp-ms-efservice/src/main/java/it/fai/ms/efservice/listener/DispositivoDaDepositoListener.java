package it.fai.ms.efservice.listener;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.notification_v2.AlertGiacenzaNotificationMessage;
import it.fai.ms.common.jms.notification_v2.NotificationMessageClient;
import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.listener.events.DispositivoDepositoInEvent;
import it.fai.ms.efservice.listener.events.DispositivoDepositoOutEvent;
import it.fai.ms.efservice.repository.AnagraficaGiacenzaRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@Component
public class DispositivoDaDepositoListener {
  private final Logger log = LoggerFactory.getLogger(DispositivoDaDepositoListener.class);


  private final DispositivoRepositoryExt dispositivoRepositoryExt;
  private final AnagraficaGiacenzaRepository anagraficaGiacenzaRepository;
  private final JmsProperties jmsProperties;

  public DispositivoDaDepositoListener(
    DispositivoRepositoryExt dispositivoRepositoryExt,
    AnagraficaGiacenzaRepository anagraficaGiacenzaRepository,
    JmsProperties jmsProperties
  ) {
    this.dispositivoRepositoryExt = dispositivoRepositoryExt;
    this.anagraficaGiacenzaRepository = anagraficaGiacenzaRepository;
    this.jmsProperties = jmsProperties;
  }

  @PostConstruct
  public void init(){
    log.info("Resetting anagraficaGiacenza alert lock");
    anagraficaGiacenzaRepository.resetSendFlag();
  }

  @EventListener
  public void onDispositivoDepositoInEvent(DispositivoDepositoInEvent event) {
    log.debug("Event onDispositivoDepositoInEvent for {}", event);
    Optional<AnagraficaGiacenza> anagraficaGiacenzaOptional = anagraficaGiacenzaRepository.findByTipoDispositivo(
      event.getTipoDispositivo().getNome().name()
    );

    if(anagraficaGiacenzaOptional.isPresent()){

      AnagraficaGiacenza anagraficaGiacenza = anagraficaGiacenzaOptional.get();

      // if the flag notification send is null or false we don't need unlock the anagrafica gianceza
      if(BooleanUtils.isNotTrue(anagraficaGiacenza.isNotificationSend())) {
        log.debug("Anagrafica Giacenza is not locked... skipping unlock.");
        return;
      }

      List<Dispositivo> dispositiviInDeposito = dispositivoRepositoryExt.findAllByStatoAndTipoDispositivo(
        StatoDispositivo.IN_DEPOSITO,
        event.getTipoDispositivo()
      );

      double threshold = getThresHoldFromAnagraficaGiacenza(anagraficaGiacenza);
      Integer numDispositiviInDeposito = dispositiviInDeposito.size();

      log.debug("onDispositivoDepositoInEvent threshold={}, numDispositiviInDeposito={}", threshold, numDispositiviInDeposito);

      if(numDispositiviInDeposito > threshold || threshold == 0d) {
        log.debug(
          "Unlocking anagraficaGiacenza for {} in order to receive notifications",
          anagraficaGiacenza.getTipoDispositivo()
        );
        anagraficaGiacenza.setNotificationSend(false);
        anagraficaGiacenzaRepository.save(anagraficaGiacenza);
      }

    }
  }

  @EventListener
  public void onDispositivoDepositoOutEvent(DispositivoDepositoOutEvent event) {
    log.debug("Event DispositivoDepositoOutEvent for {}", event);
    Optional<AnagraficaGiacenza> anagraficaGiacenzaOptional = anagraficaGiacenzaRepository.findByTipoDispositivo(
      event.getTipoDispositivo().getNome().name()
    );

    if(anagraficaGiacenzaOptional.isPresent()){

      AnagraficaGiacenza anagraficaGiacenza = anagraficaGiacenzaOptional.get();

      // Notifica gia' inviata, quindi evitiamo di inviare altre notifiche
      if(BooleanUtils.isTrue(anagraficaGiacenza.isNotificationSend())) {
        log.debug("Skipping notification, reason: already sent");
        return;
      }

      List<Dispositivo> dispositiviInDeposito = dispositivoRepositoryExt.findAllByStatoAndTipoDispositivo(
        StatoDispositivo.IN_DEPOSITO,
        event.getTipoDispositivo()
      );


      double threshold = getThresHoldFromAnagraficaGiacenza(anagraficaGiacenza);
      Integer numDispositiviInDeposito = dispositiviInDeposito.size();

      log.debug("onDispositivoDepositoOutEvent threshold={}, numDispositiviInDeposito={}", threshold, numDispositiviInDeposito);

      if(threshold >= numDispositiviInDeposito) {
        log.debug(
          "Il numero di dispositivi presenti in deposito è inferiore all'alert riordinino della giacenza minima {}",
          anagraficaGiacenza.getAlertRiordino()
        );
        AlertGiacenzaNotificationMessage alertGiacenzaNotificationMessage = new AlertGiacenzaNotificationMessage(
          -1L, //not used
          anagraficaGiacenza.getId()
        );
        new NotificationMessageClient(jmsProperties).sendNotification(alertGiacenzaNotificationMessage);
        log.debug(
          "Notification send, locking anagraficaGiacenza for {} in order to not receive more notifications",
          anagraficaGiacenza.getTipoDispositivo()
        );
        anagraficaGiacenza.setNotificationSend(true);
        anagraficaGiacenzaRepository.save(anagraficaGiacenza);
        log.debug(
          "Inviando notifica alert deposito con AlertGiacenzaNotificationMessage {}",
          alertGiacenzaNotificationMessage
        );
      }

    }
  }

  private double getThresHoldFromAnagraficaGiacenza(AnagraficaGiacenza anagraficaGiacenza){
    Integer giacenzaMinima = anagraficaGiacenza.getGiacenzaMinima();
    double alertRiordino = (anagraficaGiacenza.getAlertRiordino().doubleValue() / 100) + 1;

    return Math.round(giacenzaMinima * alertRiordino);

  }
}
