package it.fai.ms.efservice.wizard.model.matrix;

import java.io.Serializable;
import java.util.Objects;

public class WizardMatrixActivabilityFailure implements Serializable {

  private static final long serialVersionUID = -5654520944242728268L;

  private Number code;
  private String mess;

  public WizardMatrixActivabilityFailure(final Number _code, final String _mess) {
    code = _code;
    mess = _mess;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardMatrixActivabilityFailure) _obj).getCode(), code)
                 && Objects.equals(((WizardMatrixActivabilityFailure) _obj).getMess(), mess);
    }
    return isEquals;
  }

  public Number getCode() {
    return code;
  }

  public String getMess() {
    return mess;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, mess);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Step4WizardMatrixActivabilityFailure [code=");
    builder.append(this.code);
    builder.append(", mess=");
    builder.append(this.mess);
    builder.append("]");
    return builder.toString();
  }

}
