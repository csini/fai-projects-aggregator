package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_All_Devices implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineVehicle vehicleRuleContext;

  public RuleEngine_All_Devices(final RuleEngineVehicle _vehicleRuleIncome) {
    vehicleRuleContext = _vehicleRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed {} {}: {}", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean c1 = (vehicle.licensePlate != null && vehicle.licensePlate != empty);");
    rules.add("boolean c2 = (vehicle.country != null && vehicle.country != empty);");
    rules.add("boolean c3 = (vehicle.type != null && vehicle.type != empty);");
    rules.add("boolean c4 = (vehicle.euroClass != null && vehicle.euroClass != empty);");
    rules.add("boolean c5 = (vehicle.make != null && vehicle.make != empty);");
    rules.add("if (!c1) return 'licensePlate';");
    rules.add("if (!c2) return 'country';");
    rules.add("if (!c3) return 'type';");
    rules.add("if (!c4) return 'euroClass';");
    rules.add("if (!c5) return 'make';");
    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {

    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);
    String res = null;
    switch (preconditionCode) {
    case LICENSE_PLATE:
      res = "licensePlate must be present";
      break;
    case COUNTRY:
      res = "country must be present";
      break;
    case TYPE:
      res = "type must be present";
      break;
    case EURO_CLASS:
      res = "euroClass must be present";
      break;
    case MAKE:
      res = "make must be present";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
