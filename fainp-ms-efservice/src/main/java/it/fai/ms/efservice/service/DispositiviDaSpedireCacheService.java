package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import org.infinispan.Cache;
import org.infinispan.CacheSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DispositiviDaSpedireCacheService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final Cache<String, Boolean> cache;

  public DispositiviDaSpedireCacheService(@Qualifier("forced_device_to_send") Cache<String, Boolean> _cache) {
    cache = _cache;
  }

  public Set<String> getAllKeys() {
    CacheSet<String> keys = cache.keySet();
    return keys.stream()
               .collect(toSet());
  }

  public void put(String key, Boolean boolValue) {
    log.debug("Put in map [key: {} - Boolean value: {}]", key, boolValue);
    cache.put(key, boolValue);
  }

  public Boolean get(String key) {
    log.debug("Get Boolean value from cache with Key: {}", key);
    Boolean boolValue = cache.get(key);
    return boolValue;
  }

  public void clearCache() {
    log.debug("Clear cache {}", ((cache != null) ? cache.getName() : ""));
    cache.clear();
  }

  public void expireValueByKeys(Set<String> allKeys) {
    allKeys.stream()
           .map(key -> {
             Boolean removed = cache.remove(key);
             log.debug("Removed {} from cache", removed);
             return removed;
           })
           .collect(toSet());
  }

  public List<String> putAllKeys(List<String> identificativi) {
    List<String> identificativiInCache = identificativi.stream()
                                                       .map(identificativo -> {
                                                         put(identificativo, true);
                                                         Boolean boolValue = get(identificativo);
                                                         if (boolValue != null && boolValue == true) {
                                                           log.debug("Identificativo Dispositivo {} put in cache to force send...",
                                                                     identificativo);
                                                           return identificativo;
                                                         }
                                                         return null;
                                                       })
                                                       .filter(key -> key != null)
                                                       .collect(toList());
    log.info("Identificativi in cache: {}", identificativiInCache);
    return identificativiInCache;
  }

  public void expireAllKeys() throws Exception {
    Set<String> allKeys = getAllKeys();
    log.info("Founded these keys in cache: {}", allKeys);

    expireValueByKeys(allKeys);

    allKeys = getAllKeys();
    if (allKeys == null || allKeys.isEmpty()) {
      log.info("Expired all value in cache...");
    } else {
      throw new Exception("Not expire all value in cache");
    }
  }

}
