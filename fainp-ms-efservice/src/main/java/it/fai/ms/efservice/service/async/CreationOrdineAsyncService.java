package it.fai.ms.efservice.service.async;

import it.fai.ms.efservice.service.dto.CarrelloDTO;

public interface CreationOrdineAsyncService {

  /**
   * @param carrelloDTO
   * @return
   */
  public void generateOrdineAsyncByCarrelloDTO(String identificativoJob, CarrelloDTO carrelloDTO);

  /**
   * @return
   */
  public String getKeyCacheAndPushDefaultValue();

}
