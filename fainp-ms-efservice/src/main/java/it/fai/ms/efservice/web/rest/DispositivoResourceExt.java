package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.jsonwebtoken.lang.Collections;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.message.device.DeviceReturnMessage;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipologiaAssociazione;
import it.fai.ms.efservice.dto.CheckDispositiviDTO;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.DispositiviDaSpedireCacheService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.ModificationOperationsDeviceTypeService;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.CreaDispositiviDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.dto.ModificaDispositivoDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.impl.DispositivoServiceImpl;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.HeaderCustom;

@RestController
@RequestMapping(DispositivoResourceExt.BASE_PATH)
public class DispositivoResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  public static final String DISPOSITIVI_BY_ORDINE     = "/dispositivi/ordine";
  public static final String API_DISPOSITIVI_BY_ORDINE = BASE_PATH + DISPOSITIVI_BY_ORDINE;

  public static final String DISPOSITIVI_SCORTA     = "/dispositivi/scorta";
  public static final String API_DISPOSITIVI_SCORTA = BASE_PATH + DISPOSITIVI_SCORTA;

  public static final String DISPOSITIVI_NO_TARGA     = "/dispositivi/notarga";
  public static final String API_DISPOSITIVI_NO_TARGA = BASE_PATH + DISPOSITIVI_NO_TARGA;

  public static final String DISPOSITIVI_DA_SPEDIRE     = "/dispositivi/daspedire";
  public static final String API_DISPOSITIVI_DA_SPEDIRE = BASE_PATH + DISPOSITIVI_DA_SPEDIRE;

  public static final String ASSOCIAZIONE_DV                 = "/dispositivi/associazione";
  public static final String API_DISPOSITIVI_ASSOCIAZIONE_DV = BASE_PATH + ASSOCIAZIONE_DV;

  public static final String DEVICES_BY_VEHICLE_UUID     = "/dispositivi/vehicle/uuid";
  public static final String API_DEVICES_BY_VEHICLE_UUID = BASE_PATH + DEVICES_BY_VEHICLE_UUID;

  public static final String GET_DISPOSITIVO_BY_SERIALE = "/dispositivi/getdispositivobyseriale";
  public static final String GET_DISPOSITIVO_BY_TARGA   = "/dispositivi/getdispositivobytarga";

  public static final String MODIFICA_DISPOSITIVO = "/dispositivi/modifica";
  public static final String CREA_DISPOSITIVO     = "/dispositivi/crea";
  public static final String RIENTRO_DISPOSITIVO     = "/dispositivi/rientro";
  

  public static final String FORCE_DISPOSITIVI_DA_SPEDIRE     = "/dispositivi/forzaspedizione/";
  public static final String API_FORCE_DISPOSITIVI_DA_SPEDIRE = BASE_PATH + FORCE_DISPOSITIVI_DA_SPEDIRE;

  public static final String CHECK_DEVICE_EXIST_ON_VEHICLE = "/checkDeviceExistOnVehicle";
  public static final String API_CHECK_DISPOSITIVI         = BASE_PATH + CHECK_DEVICE_EXIST_ON_VEHICLE;

  public static final String DEVICE_ASSIGNATION     = "/dispositivi/assegnazione";
  public static final String API_DEVICE_ASSIGNATION = BASE_PATH + DEVICE_ASSIGNATION;

  public static final String GET_DISPOSITIVI_BY_CONTRACT     = "/dispositivi/contratto";
  public static final String API_GET_DISPOSITIVI_BY_CONTRACT = BASE_PATH + GET_DISPOSITIVI_BY_CONTRACT;

  public static final String ACTIVE_DEVICES_WITH_PLATE     = "/dispositivi-attivi-targhe";
  public static final String API_ACTIVE_DEVICES_WITH_PLATE = BASE_PATH + ACTIVE_DEVICES_WITH_PLATE;

  public static final String DEVICES_NOT_FOUND = "/dispositivi/not-found";
  public static final String API_DEVICES_NOT_FOUND = BASE_PATH + DEVICES_NOT_FOUND;

  private final DispositivoServiceImpl dispositivoService;

  private final DispositivoServiceExt dispositivoServiceExt;

  private final ContrattoServiceExt contrattoService;

  private final TipoDispositivoServiceExt tipoDispositivoService;

  private final DispositiviDaSpedireCacheService deviceToSendCache;

  private final ModificationOperationsDeviceTypeService modificationOperationsDeviceTypeService;
  
  @Autowired
  private DeviceReturnMessageProducerService deviceReturnMessageProducerService;

  public DispositivoResourceExt(DispositivoServiceImpl dispositivoService, DispositivoServiceExt dispositivoServiceExt,
                                ContrattoServiceExt contrattoService, TipoDispositivoServiceExt tipoDispositivoService,
                                final DispositiviDaSpedireCacheService deviceToSendCache,
                                final ModificationOperationsDeviceTypeService modificationOperationsDeviceTypeService) {
    this.dispositivoService                      = dispositivoService;
    this.dispositivoServiceExt                   = dispositivoServiceExt;
    this.contrattoService                        = contrattoService;
    this.tipoDispositivoService                  = tipoDispositivoService;
    this.deviceToSendCache                       = deviceToSendCache;
    this.modificationOperationsDeviceTypeService = modificationOperationsDeviceTypeService;
  }

  @GetMapping(DISPOSITIVI_BY_ORDINE + "/{identificativo}")
  @Timed
  @ApiOperation(
                value = "Mostra tutti i dispositivi associati ad un determinato ordine con il loro stato attuale",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "identificativo is empty") })
  public ResponseEntity<List<DispositivoDTO>> getDispositivByOrdine(@ApiParam(
                                                                              value = "identificativo",
                                                                              required = true) @PathVariable(
                                                                                                             required = true) String identificativo) throws Exception {
    log.debug("REST request all devices by richiesta with identificativo : {}", identificativo);

    if (StringUtils.isBlank(identificativo)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<DispositivoDTO> result = dispositivoService.findAllByOrdine(identificativo);
    return ResponseEntity.ok()
      .headers(HeaderCustom.build()
        .rows(result != null ? result.size() : 0)
        .uri(API_DISPOSITIVI_BY_ORDINE + "/" + identificativo)
        .headers())
      .body(result);
  }

  /**
   * Codice azienda : 0046348
   *
   * @param codiceAzienda
   * @return
   * @throws Exception
   */

  @GetMapping(DISPOSITIVI_SCORTA + "/{codiceAzienda}")
  @Timed
  @ApiOperation(
                value = "Mostra tutti i dispositivi SCORTA associati ad un determinato codice azienda",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "codiceAzienda is empty") })
  public ResponseEntity<List<DispositivoDTO>> getDispositiviScorta(@ApiParam(
                                                                             value = "codiceAzienda",
                                                                             required = true) @PathVariable(
                                                                                                            required = true) String codiceAzienda) throws Exception {
    log.debug("REST request all devices (SCORTA) with codice azienda : {}", codiceAzienda);

    if (StringUtils.isBlank(codiceAzienda)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<DispositivoDTO> result = dispositivoService.findAllByCodiceAziendaAndTipoMagazzino(codiceAzienda, TipoMagazzino.SCORTA)
      .stream()
      .filter(dispositivoDTO -> dispositivoDTO.getStato()
        .isGreen())
      .collect(Collectors.toList());
    result = setModificationOperationsPossible(result);

    return ResponseEntity.ok()
      .headers(HeaderCustom.build()
        .rows(result != null ? result.size() : 0)
        .uri(API_DISPOSITIVI_SCORTA + "/" + codiceAzienda)
        .headers())
      .body(result);
  }

  /**
   * Codice azienda : 0046348
   *
   * @param codiceAzienda
   * @return
   * @throws Exception
   */

  @GetMapping(DISPOSITIVI_NO_TARGA + "/{codiceAzienda}")
  @Timed
  @ApiOperation(
                value = "Mostra tutti i dispositivi NOTARGA associati ad un determinato codice azienda",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 404, message = "codiceAzienda is empty") })
  public ResponseEntity<List<DispositivoDTO>> getDispositiviNoTarga(@ApiParam(
                                                                              value = "codiceAzienda",
                                                                              required = true) @PathVariable(
                                                                                                             required = true) String codiceAzienda) throws Exception {
    log.debug("REST request all devices (NOTARGA) with codice azienda : {}", codiceAzienda);

    if (StringUtils.isBlank(codiceAzienda)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<DispositivoDTO> result = dispositivoService.findAllByCodiceAziendaAndTipoMagazzino(codiceAzienda, TipoMagazzino.NOTARGA);
    result = setModificationOperationsPossible(result);

    return ResponseEntity.ok()
      .headers(HeaderCustom.build()
        .rows(result != null ? result.size() : 0)
        .uri(API_DISPOSITIVI_NO_TARGA + "/" + codiceAzienda)
        .headers())
      .body(result);
  }

  private List<DispositivoDTO> setModificationOperationsPossible(List<DispositivoDTO> result) {
    return result.stream()
      .map(deviceDto -> {
        List<String> operations    = new ArrayList<>();
        boolean    isToAddAssocaiTargaAction = isToAddAssociaTargaAction(deviceDto);
        if (isToAddAssocaiTargaAction) {
          operations.add(TipoRichiesta.ASSOCIA_TARGA.name());
        }
//        else {
//          TipoDispositivoEnum deviceType        = null;
//          String            tipoDispositivoNome = deviceDto.getTipoDispositivoNome();
//          try {
//            deviceType = TipoDispositivoEnum.valueOf(tipoDispositivoNome);
//          } catch (Exception e) {
//            log.error("Exception on cast tipo dispositivo {} to Enum", tipoDispositivoNome, e);
//            throw new RuntimeException(e);
//          }
//          operations = modificationOperationsDeviceTypeService.getModificationOperations(deviceType);
//
//        }
        deviceDto.setOperazioniModificaPossibili(operations);
        return deviceDto;
      })
      .collect(toList());
  }

  private boolean isToAddAssociaTargaAction(DispositivoDTO deviceDto) {

    TipoMagazzino tipoMagazzino = deviceDto.getTipoMagazzino();
    boolean isScortaOrNoTarga = tipoMagazzino.equals(TipoMagazzino.SCORTA) || tipoMagazzino.equals(TipoMagazzino.NOTARGA);

    String tipoDispositivoNome = deviceDto.getTipoDispositivoNome();
    boolean isViacardOrTelepassItaliano = TipoDispositivoEnum.VIACARD.name().equalsIgnoreCase(tipoDispositivoNome) ||
      TipoDispositivoEnum.TELEPASS_ITALIANO.name().equalsIgnoreCase(tipoDispositivoNome);

    return isScortaOrNoTarga && isViacardOrTelepassItaliano;
  }

  @GetMapping(DISPOSITIVI_DA_SPEDIRE + "/{dispositivi}")
  @Timed
  public ResponseEntity<List<String>> getDispositiviDaSpedire(@PathVariable(
                                                                            value = "dispositivi",
                                                                            required = true) List<String> identificativoDispositivi) throws Exception {
    log.debug("REST request all devices to send : {}", identificativoDispositivi);

    if (Collections.isEmpty(identificativoDispositivi)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<String> result = null;
    try {
      result = dispositivoServiceExt.generateMapAndSendInQueue(identificativoDispositivi);
    } catch (Exception e) {
      log.error("Exception {}", e);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.FAILED_SENT_MESSAGE_TO_QUEUE);
    }

    return ResponseEntity.ok(result);
  }

  @GetMapping("/dispositivo/{codiceAzienda}/{uuidVehicle}")
  public ResponseEntity<Boolean> hasVehicleActivedDevices(@PathVariable String codiceAzienda, @PathVariable String uuidVehicle) {
    return ResponseEntity.ok(dispositivoServiceExt.hasVehicleActivedDevices(codiceAzienda, uuidVehicle));
  }

  @PostMapping(ASSOCIAZIONE_DV + "/{tipologiaAssociazione}")
  @Timed
  public ResponseEntity<String> associationDispositivoVeicolo(@PathVariable TipologiaAssociazione tipologiaAssociazione,
                                                              @RequestBody CarrelloModificaRichiestaDTO carrelloDTO) throws Exception {
    if (!checkValidityParamsCarrelloAssociazioneDv(carrelloDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    AssociazioneDV assDv = dispositivoServiceExt.associazioneDispositivoVeicolo(tipologiaAssociazione, carrelloDTO);
    if (assDv == null) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.FAILED_ASSOCIATION);
    }

    return ResponseEntity.ok(null);
  }

  @PostMapping(MODIFICA_DISPOSITIVO)
  @Timed
  public ResponseEntity<Void> modificaDispositivi(@RequestBody ModificaDispositivoDTO modificaDispositivoDTO) throws Exception {
    log.debug("Request for modify Dispositivi by DTO: {}", modificaDispositivoDTO);

    if (modificaDispositivoDTO == null) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    String identificativoDaModificare = modificaDispositivoDTO.getDmlUniqueIdentifier();
    if (StringUtils.isBlank(identificativoDaModificare)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    String identificativo = null;
    try {
      identificativo = dispositivoServiceExt.modificaDispositivoByBO(modificaDispositivoDTO);
    } catch (Exception e) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.FAILED_UPDATE_DISPOSITIVI_BY_BO);
    }

    if (identificativo == null) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.FAILED_UPDATE_DISPOSITIVI_BY_BO);
    }

    return ResponseEntity.ok()
      .build();
  }

  @PostMapping(FORCE_DISPOSITIVI_DA_SPEDIRE)
  @Timed
  public ResponseEntity<List<String>> forceDeviceToSend(@RequestBody List<String> identificativi) throws CustomException {
    log.debug("Request to FORCE device to send by identificativi: {}", identificativi);
    if (identificativi == null || identificativi.isEmpty()) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<String> valueEnteredInCache = deviceToSendCache.putAllKeys(identificativi);

    if (valueEnteredInCache == null || valueEnteredInCache.isEmpty()) {
      log.error("Devices {} don't forced to send....", identificativi);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.VALUE_NOT_ENTERED_IN_CACHE);
    }
    return ResponseEntity.ok(valueEnteredInCache);
  }

  @PostMapping(CREA_DISPOSITIVO)
  @Timed
  public ResponseEntity<Void> creaDispositivi(@RequestBody CreaDispositiviDTO dto) throws Exception {
    log.debug("Request for creating Dispositivi by DTO: {}", dto);

    if (dto == null || dto.getTipoDispositivo() == null || dto.getDispositivi() == null || dto.getDispositivi()
      .isEmpty()) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(dto.getTipoDispositivo());
    if (tipoDispositivo == null) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    Contratto contratto = null;
    if (dto.getCodiceContratto() != null) {
      Produttore produttore     = tipoDispositivo.getProduttore();
      String     nomeProduttore = (produttore != null) ? produttore.getNome() : null;
      contratto = contrattoService.findByCodiceContrattoAndNomeProduttore(dto.getCodiceContratto(), nomeProduttore)
        .orElseThrow(() -> CustomException.builder(HttpStatus.PRECONDITION_FAILED)
          .add(Errno.NOT_FOUND_CONTRATTO_CLIENTEFAI));

      if (!contratto.getClienteFai()
        .getCodiceCliente()
        .equals(dto.getCodiceCliente())) {
        CustomException.builder(HttpStatus.PRECONDITION_FAILED)
          .add(Errno.NOT_FOUND_CONTRATTO_CLIENTEFAI);
      }
    }

    dispositivoServiceExt.creaDispositivi(dto, contratto, tipoDispositivo);
    return ResponseEntity.ok()
      .build();
  }

  private boolean checkValidityParamsCarrelloAssociazioneDv(CarrelloModificaRichiestaDTO carrelloDTO) {
    if (StringUtils.isBlank(carrelloDTO.getNewVeicolo())) {
      log.error("Not found identificativo vehicle...");
      return false;
    }

    List<String> dispositivi = carrelloDTO.getDispositivi();
    if ((dispositivi == null) || (dispositivi != null && (dispositivi.isEmpty() || dispositivi.size() > 1))) {
      log.error("Dispositivo not found");
      return false;
    }

    return true;
  }

  @GetMapping(GET_DISPOSITIVO_BY_SERIALE + "/{codiceContratto}/{seriale}")
  @Timed
  public ResponseEntity<DispositivoDTO> getDispositivoBySeriale(@PathVariable(required = true) String codiceContratto,
                                                                @PathVariable(required = true) String seriale) throws CustomException {

    log.debug("REST Request device by contract number : {} and serial number : {}", codiceContratto, seriale);
    List<DispositivoDTO> dtos = dispositivoServiceExt.findDispositiviByCodiceContrattoAndSeriale(codiceContratto, seriale);

    if (dtos == null || dtos.isEmpty())
      return ResponseEntity.notFound()
        .build();

    return ResponseEntity.ok(dtos.get(0));
  }

  @GetMapping(GET_DISPOSITIVI_BY_CONTRACT + "/{codiceContratto}")
  @Timed
  public ResponseEntity<DispositivoDTO> getDispositivoByContratto(@PathVariable(required = true) String codiceContratto) throws Exception {

    log.debug("REST Request devices by contract number : {}", codiceContratto);
    List<DispositivoDTO> dtos = dispositivoServiceExt.findByCodiceContratto(codiceContratto);

    if (dtos == null || dtos.isEmpty())
      return ResponseEntity.notFound()
        .build();

    return ResponseEntity.ok(dtos.get(0));
  }

  @GetMapping(GET_DISPOSITIVO_BY_TARGA + "/{codiceContratto}/{uuidVeicolo}")
  @Timed
  public ResponseEntity<DispositivoDTO> getDispositivoByTarga(@PathVariable(required = true) String codiceContratto,
                                                              @PathVariable(required = true) String uuidVeicolo) throws CustomException {

    List<DispositivoDTO> dtos = dispositivoServiceExt.findDispositiviByCodiceContrattoAndVeicolo(codiceContratto, uuidVeicolo);

    if (dtos == null || dtos.isEmpty())
      return ResponseEntity.notFound()
        .build();

    return ResponseEntity.ok(dtos.get(0));
  }

  @PostMapping("/dispositivi/vehicle/stato" + "/{codiceCliente}/{uuidVeicolo}")
  @Timed
  public ResponseEntity<List<it.fai.ms.common.jms.dto.efservice.DispositivoDTO>> getActiveDispositiviByVehicleUuid(@PathVariable("codiceCliente") String codiceCliente,
                                                                                @PathVariable("uuidVeicolo") String uuidVeicolo) throws Exception {
    log.debug("REST Request all devices by vehicle uuid : {} codCliente {}", uuidVeicolo, codiceCliente);
    List<it.fai.ms.common.jms.dto.efservice.DispositivoDTO> body = dispositivoServiceExt.findActiveByVehicleUuid(uuidVeicolo,codiceCliente);
    
    return ResponseEntity.ok()
      .body(body);
  }

  @GetMapping(DEVICES_BY_VEHICLE_UUID + "/{uuidVeicolo}")
  @Timed
  public ResponseEntity<List<DispositivoDTO>> getDispositiviByVehicleUuid(@PathVariable String uuidVeicolo) throws Exception {
    log.debug("REST Request all devices by vehicle uuid : {}", uuidVeicolo);

    if (StringUtils.isBlank(uuidVeicolo)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.VALIDATION_FAILED);
    }

    List<DispositivoDTO> body = dispositivoServiceExt.findAllByVehicleUuid(uuidVeicolo);

    return ResponseEntity.ok()
      .headers(HeaderCustom.build()
        .add("X-rows", body != null ? body.size() : -1)
        .add("X-uuid", uuidVeicolo)
        .headers())
      .body(body);
  }

  @PostMapping(CHECK_DEVICE_EXIST_ON_VEHICLE)
  @Timed
  public ResponseEntity<List<String>> checkDispositivi(@RequestBody CheckDispositiviDTO checkDispositiviDto) throws Exception {
    log.debug("REST Request check exists device by DTO: {}", checkDispositiviDto);

    if (checkDispositiviDto == null) {
      log.error("DTO is null or empty string.");
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.VALIDATION_FAILED);
    }

    List<String> licensePlateWithoutDeviceType = dispositivoServiceExt.checkDispositivoExists(checkDispositiviDto);
    return ResponseEntity.ok()
      .body(licensePlateWithoutDeviceType);
  }

  //
  // TODO : task https://exagespa.atlassian.net/browse/FAINP-2862 sospseso
  //
  @GetMapping(ACTIVE_DEVICES_WITH_PLATE)
  @Timed
  @ApiOperation(
                value = "Ritorna l'elenco dei dispositivi attivi e su quali targhe (compresa data immatricolazione) in un periodo specificato in ingresso",
                consumes = MediaType.APPLICATION_JSON,
                produces = MediaType.APPLICATION_JSON)
  @ApiResponses(value = { @ApiResponse(code = 200, message = ""), @ApiResponse(code = 400, message = "dateStart is empty") })
  public ResponseEntity<List<DispositivoDTO>> getDispositiviAttiviTarghe(@ApiParam(
                                                                                   value = "dateStart",
                                                                                   required = true) @RequestParam(
                                                                                                                  name = "dateStart",
                                                                                                                  required = true) @DateTimeFormat(
                                                                                                                                                   iso = ISO.DATE) LocalDate dateStart) throws Exception {
    log.info("REST Request getDispositiviAttiviTarghe from {}", dateStart);

    if (dateStart == null) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
        .add(Errno.INVALID_PARAMETER);
    }

    List<DispositivoDTO> results = dispositivoServiceExt.getDispositiviAttiviTarghe(dateStart);

    return ResponseEntity.ok()
      .headers(HeaderCustom.build()
        .uri(API_ACTIVE_DEVICES_WITH_PLATE)
        .rows(results != null ? results.size() : -1)
        .headers())
      .body(results);
  }

  @PostMapping(DEVICES_NOT_FOUND + "/{tipo}")
  @Timed
  public ResponseEntity<List<DispositivoDTO>> checkDevicesNotFound(@PathVariable("tipo") String tipo,
                                                           @RequestBody List<String> serials)
                                                               throws Exception {
    log.debug("REST Request check devices not found with type : {} and serials : {}", tipo, serials);

    List<DispositivoDTO> results = dispositivoServiceExt.checkDevicesNotFound(tipo, serials);

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(API_DEVICES_NOT_FOUND)
                 .rows(results != null ? results.size() : -1)
                 .headers())
        .body(results);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
      .body(ex.getPayloadError());
  }
  
  @PostMapping(RIENTRO_DISPOSITIVO)
  @Timed
  public ResponseEntity<?> rientroDispositivi(@RequestBody DispositivoDTO dispositivoDTO) throws Exception {
    
    log.debug("data " + dispositivoDTO.getDataModificaStato());
    List<Dispositivo> results = dispositivoService.findAllRientrati(dispositivoDTO.getDataModificaStato());
    results.stream().map( d -> {
      
      return d.getRichiestas().stream().max(  (a,b )-> {
        return a.getId().compareTo(b.getId());
      }).orElse(null);
      
    }).filter(Objects::nonNull).forEach( r -> { 
      
      try {
      
        deviceReturnMessageProducerService.deactivateDevice(r);
      
      }catch(Throwable e) {
        this.log.warn("Error {}", e);
      }
    }); 
    return ResponseEntity.status(HttpStatus.OK).body(String.format("Sent %s", "" + results.size()));
  }
  
}
