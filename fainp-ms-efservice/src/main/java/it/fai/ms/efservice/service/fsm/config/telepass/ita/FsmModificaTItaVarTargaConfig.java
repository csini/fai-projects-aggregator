package it.fai.ms.efservice.service.fsm.config.telepass.ita;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDeviceLicensePlate;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardGestioneRientro;
import it.fai.ms.efservice.service.fsm.guard.GuardMalfunzionamentoSostituzione;
import it.fai.ms.efservice.service.fsm.guard.GuardMezzoRitargatoTE;
import it.fai.ms.efservice.service.fsm.guard.GuardSendDeviceReturnMessage;
import it.fai.ms.efservice.service.fsm.guard.GuardTipoRichiesta;
import it.fai.ms.efservice.service.fsm.guard.GuardVariazioneTargaTE;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceLicensePlateChangeMessageProducerService;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTItaVarTargaConfig.MOD_TITA_VARTARGA)
public class FsmModificaTItaVarTargaConfig extends FsmRichiestaConfig {

  public static final String MOD_TITA_VARTARGA = "ModTItaVarTarga";

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTItaVarTargaConfig.class);

  private final FsmSenderToQueue senderFsmUtil;

  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  private final DeviceLicensePlateChangeMessageProducerService deviceLicensePlateProducer;

  public FsmModificaTItaVarTargaConfig(FsmSenderToQueue senderFsmUtil, DeviceReturnMessageProducerService _deviceReturnMessageService,
                                       final DeviceLicensePlateChangeMessageProducerService _deviceLicensePlateProducer) {
    this.senderFsmUtil = senderFsmUtil;
    deviceReturnMessageService = _deviceReturnMessageService;
    deviceLicensePlateProducer = _deviceLicensePlateProducer;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TI_VARIAZIONE_TARGA.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TI;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfTItaVarTargaRitargatoMalfSostituzione();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TI)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .guard(new GuardTipoRichiesta(TipoRichiesta.VARIAZIONE_TARGA))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MEZZO_RITARGATO))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE))
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .action(new FsmActionDeviceLicensePlate(deviceLicensePlateProducer))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .guard(new GuardGestioneRientro()) //FIXME-fsm - cosa succede se la guard fallisce?
               .action(new FsmActionCreateNewRichiesta(senderFsmUtil.getSenderJmsService()))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .guard(new GuardSendDeviceReturnMessage(deviceReturnMessageService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISATTIVAZIONE_SOSPESA)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISATTIVAZIONE_SOSPESA)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmUtil));
  }

}
