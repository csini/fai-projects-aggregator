package it.fai.ms.efservice.service.fsm.bean.fai;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.frejus.FsmMalfunzionamentoFrejusSost;

@WithStateMachine(id = FsmMalfunzionamentoFrejusSost.FSM_NAME)
public class FsmMalfunzionamentoFrejusTransitionBean extends AbstractFsmRichiestaTransition {

}
