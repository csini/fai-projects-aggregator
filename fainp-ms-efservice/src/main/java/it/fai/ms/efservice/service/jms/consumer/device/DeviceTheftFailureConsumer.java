package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceTheftNotificationFailureMessage;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

@Service
@Transactional
public class DeviceTheftFailureConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseKOConsumer deviceResponseKoConsumer;

  DeviceStolenConsumer deviceStolenConsumer;

  public DeviceTheftFailureConsumer(DeviceResponseKOConsumer _deviceResponseKoConsumer,
                                    DeviceStolenConsumer _deviceStolenConsumer) {
    deviceResponseKoConsumer = _deviceResponseKoConsumer;
    deviceStolenConsumer = _deviceStolenConsumer;
  }

  public void consume(DeviceTheftNotificationFailureMessage message) {
    log.info("Manage message: {}", message);
    String identificativoRichiesta = deviceStolenConsumer.getOrderRequestUuid(message,TipoRichiesta.FURTO, TipoRichiesta.FURTO_CON_SOSTITUZIONE);
    String errorMsg = null;
    // String errorMsg = jmsDeviceErrorService.buildErrorMessage(message);
    if(identificativoRichiesta!=null)
    	deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
    else
    	log.warn("Richiesta non trovata per il messaggio {}",message);
  }

}
