package it.fai.ms.efservice.service.fsm.action;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionChangeStatoDispositivo implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(FsmActionChangeStatoDispositivo.class);

  private final FsmSenderToQueue senderFsmService;

  private final DispositivoEvent command;


  public FsmActionChangeStatoDispositivo(FsmSenderToQueue senderFsmService,DispositivoEvent command) {
    this.senderFsmService = senderFsmService;
    this.command = command;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;


      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      try {
        changeStateWithCommand(dispositivi, command);
      } catch (Exception e) {
        log.error("Exception", e);
      }
    }
  }

  private void changeStateWithCommand(Set<Dispositivo> dispositivi, DispositivoEvent event) throws Exception {
    if (dispositivi != null) {
      Iterator<Dispositivo> iterator = dispositivi.iterator();
      while (iterator != null && iterator.hasNext()) {
        Dispositivo dispositivo = iterator.next();
        senderFsmService.sendMessageForChangeStatoDispositivo(dispositivo, event);
      }
    }

  }

}
