package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardServiceTypeId implements Serializable {

  private static final long serialVersionUID = -3814191350773625333L;

  private String id;

  public WizardServiceTypeId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardServiceTypeId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardServiceTypeId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }
}
