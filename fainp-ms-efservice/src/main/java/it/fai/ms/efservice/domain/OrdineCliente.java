package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.common.dml.AbstractDmlEntity;
import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.listener.PrePersistOrdineClienteListener;

/**
 * A OrdineCliente.
 */
@Entity
@EntityListeners({ PrePersistOrdineClienteListener.class, })
@Table(name = "ordine_cliente")
public class OrdineCliente implements AbstractDmlEntity,Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "previsione_spesa_mensile")
  private Integer previsioneSpesaMensile;

  @Column(name = "uuid_veicolo")
  private String uuidVeicolo;

  @Column(name = "destinazione_fai")
  private Boolean destinazioneFai;

  @Column(name = "numero_ordine")
  private String numeroOrdine;

  @Enumerated(EnumType.STRING)
  @Column(name = "stato")
  private StatoOrdineCliente stato;

  @Column(name = "data_modifica_stato")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataModificaStato;

  @Column(name = "data_creazione")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataCreazione;

  @Column(name = "richiedente")
  private String richiedente;

  @Enumerated(EnumType.STRING)
  @Column(name = "tipo")
  private TipoOrdineCliente tipo;

  @Column(name = "identificativo")
  private String identificativo;

  @OneToMany(mappedBy = "ordineCliente")
  @JsonIgnore
  private Set<Richiesta> richiestas = new HashSet<>();

  @OneToOne
  @JoinColumn
  private IndirizzoSpedizioneOrdini indirizzoDiTransito;

  @OneToOne
  @JoinColumn
  private IndirizzoSpedizioneOrdini indirizzoDestinazioneFinale;

  @ManyToOne
  private ClienteFai clienteAssegnatario;
  
  @Transient
  private boolean enabledDmlMessage = true;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getPrevisioneSpesaMensile() {
    return previsioneSpesaMensile;
  }

  public OrdineCliente previsioneSpesaMensile(Integer previsioneSpesaMensile) {
    this.previsioneSpesaMensile = previsioneSpesaMensile;
    return this;
  }

  public void setPrevisioneSpesaMensile(Integer previsioneSpesaMensile) {
    this.previsioneSpesaMensile = previsioneSpesaMensile;
  }

  public String getUuidVeicolo() {
    return uuidVeicolo;
  }

  public OrdineCliente uuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
    return this;
  }

  public void setUuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
  }

  public Boolean isDestinazioneFai() {
    return destinazioneFai;
  }

  public OrdineCliente destinazioneFai(Boolean destinazioneFai) {
    this.destinazioneFai = destinazioneFai;
    return this;
  }

  public void setDestinazioneFai(Boolean destinazioneFai) {
    this.destinazioneFai = destinazioneFai;
  }

  public String getNumeroOrdine() {
    return numeroOrdine;
  }

  public OrdineCliente numeroOrdine(String numeroOrdine) {
    this.numeroOrdine = numeroOrdine;
    return this;
  }

  public void setNumeroOrdine(String numeroOrdine) {
    this.numeroOrdine = numeroOrdine;
  }

  public StatoOrdineCliente getStato() {
    return stato;
  }

  public OrdineCliente stato(StatoOrdineCliente stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(StatoOrdineCliente stato) {
    this.stato = stato;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public OrdineCliente dataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
    return this;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public Instant getDataCreazione() {
    return dataCreazione;
  }

  public OrdineCliente dataCreazione(Instant dataCreazione) {
    this.dataCreazione = dataCreazione;
    return this;
  }

  public void setDataCreazione(Instant dataCreazione) {
    this.dataCreazione = dataCreazione;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public OrdineCliente richiedente(String richiedente) {
    this.richiedente = richiedente;
    return this;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }

  public TipoOrdineCliente getTipo() {
    return tipo;
  }

  public OrdineCliente tipo(TipoOrdineCliente tipo) {
    this.tipo = tipo;
    return this;
  }

  public void setTipo(TipoOrdineCliente tipo) {
    this.tipo = tipo;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public OrdineCliente identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Set<Richiesta> getRichiestas() {
    return richiestas;
  }

  public OrdineCliente richiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
    return this;
  }

  public OrdineCliente addRichiesta(Richiesta richiesta) {
    this.richiestas.add(richiesta);
    richiesta.setOrdineCliente(this);
    return this;
  }

  public OrdineCliente removeRichiesta(Richiesta richiesta) {
    this.richiestas.remove(richiesta);
    richiesta.setOrdineCliente(null);
    return this;
  }

  public void setRichiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
  }

  public IndirizzoSpedizioneOrdini getIndirizzoDiTransito() {
    return indirizzoDiTransito;
  }

  public OrdineCliente indirizzoDiTransito(IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini) {
    this.indirizzoDiTransito = indirizzoSpedizioneOrdini;
    return this;
  }

  public void setIndirizzoDiTransito(IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini) {
    this.indirizzoDiTransito = indirizzoSpedizioneOrdini;
  }

  public IndirizzoSpedizioneOrdini getIndirizzoDestinazioneFinale() {
    return indirizzoDestinazioneFinale;
  }

  public OrdineCliente indirizzoDestinazioneFinale(IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini) {
    this.indirizzoDestinazioneFinale = indirizzoSpedizioneOrdini;
    return this;
  }

  public void setIndirizzoDestinazioneFinale(IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini) {
    this.indirizzoDestinazioneFinale = indirizzoSpedizioneOrdini;
  }

  public ClienteFai getClienteAssegnatario() {
    return clienteAssegnatario;
  }

  public OrdineCliente clienteAssegnatario(ClienteFai clienteFai) {
    this.clienteAssegnatario = clienteFai;
    return this;
  }

  public void setClienteAssegnatario(ClienteFai clienteFai) {
    this.clienteAssegnatario = clienteFai;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrdineCliente ordineCliente = (OrdineCliente) o;
    if (ordineCliente.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), ordineCliente.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "OrdineCliente{" + "id=" + getId() + ", previsioneSpesaMensile='" + getPrevisioneSpesaMensile() + "'" + ", uuidVeicolo='"
           + getUuidVeicolo() + "'" + ", destinazioneFai='" + isDestinazioneFai() + "'" + ", numeroOrdine='" + getNumeroOrdine() + "'"
           + ", stato='" + getStato() + "'" + ", dataModificaStato='" + getDataModificaStato() + "'" + ", dataCreazione='"
           + getDataCreazione() + "'" + ", richiedente='" + getRichiedente() + "'" + ", tipo='" + getTipo() + "'" + ", identificativo='"
           + getIdentificativo() + "'" + "}";
  }

@Override
public boolean isEnabledDmlMessage() {
	return enabledDmlMessage;
}

@Override
public void setEnabledDmlMessage(boolean enabledDmlMessage) {
	this.enabledDmlMessage = enabledDmlMessage;
}
}
