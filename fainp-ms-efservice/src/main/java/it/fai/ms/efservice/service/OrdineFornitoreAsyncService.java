package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.enumeration.StateCreationRichiesta;
import it.fai.ms.efservice.enumeration.WarningCodeOrdineFornitore;
import it.fai.ms.efservice.service.dto.AnomaliaOrdineFornitoreDTO;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreByRangeDTO;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreBySerialeDTO.OrdineFornitoreDispositivo;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import it.fai.ms.efservice.service.dto.ResponseOrdineFornitoreDTO;

/**
 * Service Implementation for managing DeviceDepositivoOrdineFornitore.
 */
@Service
@Transactional
public class OrdineFornitoreAsyncService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AsyncJobServiceImpl               asyncJobService;
  private final DocumentClient                    documentClient;
  private final OrdineFornitoreServiceExt         ordineFornitoreServiceExt;
  private final DispositivoOrdineFornitoreService deviceOrdineFornitoreService;

  public OrdineFornitoreAsyncService(final AsyncJobServiceImpl _asyncJobService, final DocumentClient _documentClient,
                                     final OrdineFornitoreServiceExt _ordineFornitoreServiceExt,
                                     final DispositivoOrdineFornitoreService _deviceOrdineFornitoreService) {
    asyncJobService              = _asyncJobService;
    documentClient               = _documentClient;
    ordineFornitoreServiceExt    = _ordineFornitoreServiceExt;
    deviceOrdineFornitoreService = _deviceOrdineFornitoreService;
  }

  public String generateKey(String prefix_key) {
    ResponseOrdineFornitoreDTO responseDto  = new ResponseOrdineFornitoreDTO(StateCreationRichiesta.PENDING);
    String                     keyGenerated = asyncJobService.generateKeyCacheAndPushValue(prefix_key, responseDto);
    log.debug("Key generated: {}", keyGenerated);
    return keyGenerated;
  }

  public OrdineFornitoreDTO findOrdineFornitoreById(Long ordineFornitoreId) {
    return ordineFornitoreServiceExt.findOrdineFornitoreById(ordineFornitoreId);
  }

  public ResponseOrdineFornitoreDTO generateDeviceForOrdineFornitore(OrdineFornitoreDTO ordineFornitoreDTO,
                                                                     CaricamentoOrdineFornitoreByRangeDTO dto) {
    Long   ordineFornitoreId = ordineFornitoreDTO.getId();
    String tipoDispositivo   = ordineFornitoreServiceExt.getTipoDispositivoByOrdineDispositivoId(ordineFornitoreId);

    LongStream        range               = LongStream.rangeClosed(Long.valueOf(dto.getRangeStart()), Long.valueOf(dto.getRangeStop()));
    Set<String>       serialiToSkip       = new HashSet<>();
    final Set<String> serialiSkipToLength = range.mapToObj(serial -> {
                                            String seriale = null;
                                            seriale = String.valueOf(serial);
                                            return seriale;
                                          })
      .filter(s -> s.length() < 10 || s.length() > 10)
      .map(s -> {
        log.info("Seriale {} is greater or less than 10", s);
        serialiToSkip.add(s);
        return s;
      })
      .collect(toSet());

    range = LongStream.rangeClosed(Long.valueOf(dto.getRangeStart()), Long.valueOf(dto.getRangeStop()));
    Set<Dispositivo> serialiSkipToDeviceExists = range.mapToObj(serial -> {
      String seriale = String.valueOf(serial);
      return deviceOrdineFornitoreService.findDeviceByTipoAndSeriale(TipoDispositivoEnum.valueOf(tipoDispositivo), seriale);
    })
      .filter(opt -> opt.isPresent())
      .map(opt -> {
        Dispositivo device = opt.get();
        String    seriale = device.getSeriale();
        log.info("Device with seriale {} already present in DB", seriale);
        serialiToSkip.add(seriale);
        return device;
      })
      .collect(toSet());

    Set<Dispositivo> dispositiviGenerated = deviceOrdineFornitoreService
      .createDispositivoByRange(tipoDispositivo, dto.getPan(), dto.getRangeStart(), dto.getRangeStop(), serialiToSkip);

    int quantity = dispositiviGenerated.size();
    ordineFornitoreDTO = ordineFornitoreServiceExt.updateDeviceAcquireQuantityAndState(ordineFornitoreDTO, quantity);

    String note = dto.getNote();
    log.info("Update note [{}] on OrdineFornitore: {}", note, ordineFornitoreDTO);
    ordineFornitoreDTO = ordineFornitoreServiceExt.updateNoteOrdineFornitore(ordineFornitoreDTO, note);

    if (serialiToSkip != null && !serialiToSkip.isEmpty()) {
      return getResponse(StateCreationRichiesta.WARNING, serialiSkipToLength, serialiSkipToDeviceExists);
    }
    return getResponse(StateCreationRichiesta.SUCCESS);
  }

  public ResponseOrdineFornitoreDTO generateDeviceForOrdineFornitore(OrdineFornitoreDTO ordineFornitoreDTO, Collection<OrdineFornitoreDispositivo> rowList,
                                                                     String note) {
    Long   ordineFornitoreId = ordineFornitoreDTO.getId();
    String tipoDispositivo   = ordineFornitoreServiceExt.getTipoDispositivoByOrdineDispositivoId(ordineFornitoreId);

    Set<String>         serialiToSkipForValidation = Collections.<String> emptySet();
    TipoDispositivoEnum tipoDispositivoEnum        = TipoDispositivoEnum.valueOf(tipoDispositivo);
    log.info("generateDeviceForOrdineFornitore : {} for device : {}", ordineFornitoreDTO, tipoDispositivoEnum);
    switch (tipoDispositivoEnum) {
      case LIBER_T:
      case TES_TRAF_GRAN_SANBERNARDO:
        serialiToSkipForValidation = rowList.stream()
        .map(l -> l.getSeriale())
        .filter(k -> !k.matches("\\d{19}+"))
        .map(s -> {
          log.info("Seriale {} is greater or less than 19 or is not a digit sequence", s);
          return s;
        })
        .collect(toSet());
        break;
      case TELEPASS_ITALIANO:
        serialiToSkipForValidation = rowList.stream()
          .map(array -> {
            if (array.getSeriale().length() < 10) {
              log.info("Seriale {} is greater or less than 10", array.getSeriale());
            } else {
              array.setSeriale( "0" + array.getSeriale().substring(array.getSeriale().length() - 10));
            }
            return array;
          })
          .map(l -> l.getSeriale())
          .filter(k -> k.length() < 10)
          .collect(toSet());
        break;
      case BUONI_FREJUS_MONTE_BIANCO_FR:
      case BUONI_FREJUS_MONTE_BIANCO_IT:
        serialiToSkipForValidation = (rowList.stream()
          .filter(l -> {
            if (l.getDataScadenza()==null) {
              log.info("row has no data scadenza: {}", (Object) l);
              return true;
            }
            return false;

          })
          .map(l -> l.getSeriale())
          .collect(toSet()));
        break;
      case BUONI_GRAN_SAN_BERNARDO:
         //NO validation
          break;
      default:
        serialiToSkipForValidation = rowList.stream()
          .map(l -> l.getSeriale())
          .filter(k -> k.length() < 10 || k.length() > 10)
          .map(s -> {
            log.info("Seriale {} is greater or less than 10", s);
            return s;
          })
          .collect(toSet());
        break;
    }

    Set<Dispositivo> serialiSkipToDeviceExists = rowList.stream()
      .map(l -> l.getSeriale())
      .map(key -> deviceOrdineFornitoreService.findDeviceByTipoAndSeriale(tipoDispositivoEnum, key))
      .filter(opt -> opt.isPresent())
      .map(opt -> {
        Dispositivo dispositivo = opt.get();
        String    seriale     = dispositivo.getSeriale();
        log.info("Device with seriale {} already present in DB", seriale);
        return dispositivo;
      })
      .collect(toSet());

    final Set<String> serialiToSkip = new HashSet<>(serialiToSkipForValidation);
    serialiToSkip.addAll(serialiSkipToDeviceExists.stream()
      .map(d -> d.getSeriale())
      .collect(Collectors.toSet()));

    Set<Dispositivo> dispositiviGenerated = deviceOrdineFornitoreService.createDispositiviByList(tipoDispositivo, rowList, serialiToSkip);

    int quantity = dispositiviGenerated.size();
    ordineFornitoreDTO = ordineFornitoreServiceExt.updateDeviceAcquireQuantityAndState(ordineFornitoreDTO, quantity);
    log.info("Update note [{}] on OrdineFornitore: {}", note, ordineFornitoreDTO);
    ordineFornitoreDTO = ordineFornitoreServiceExt.updateNoteOrdineFornitore(ordineFornitoreDTO, note);

    if (serialiToSkip != null && !serialiToSkip.isEmpty()) {
      return getResponse(StateCreationRichiesta.WARNING, serialiToSkipForValidation, serialiSkipToDeviceExists);
    }

    return getResponse(StateCreationRichiesta.SUCCESS);
  }


  public void asyncGenerateDeviceForOrdineFornitore(String identificativoJob, OrdineFornitoreDTO ordineFornitoreDTO,
                                                    CaricamentoOrdineFornitoreByRangeDTO dtoCaricamento) {

    log.info("Start async generate Device to OrdineFornitore {} for Identificativo JOB {}", ordineFornitoreDTO, identificativoJob);
    CompletableFuture.supplyAsync(() -> {
      return generateDeviceForOrdineFornitore(ordineFornitoreDTO, dtoCaricamento);
    })
      .exceptionally(e -> {
        log.error("Exception in generate Device to OrdineFornitore {} ASYNC for identificativo job: {}", ordineFornitoreDTO,
                  identificativoJob, e);
        return new ResponseOrdineFornitoreDTO(StateCreationRichiesta.ERROR);
      })
      .thenAccept(dto -> asyncJobService.save(identificativoJob, dto));
  }

  public void asyncGenerateDeviceForOrdineFornitore(String identificativoJob, OrdineFornitoreDTO ordineFornitoreDTO, List<String[]> map,
                                                    String note) {

    log.info("Start async generate Device to OrdineFornitore {} for Identificativo JOB {}", ordineFornitoreDTO, identificativoJob);
    CompletableFuture.supplyAsync(() -> {
      return generateDeviceForOrdineFornitore(ordineFornitoreDTO, map.stream().map(this::toDevice).collect(Collectors.toList()), note);
    })
      .exceptionally(e -> {
        log.error("Exception in generate Device to OrdineFornitore {} ASYNC for identificativo job: {}", ordineFornitoreDTO,
                  identificativoJob, e);
        return new ResponseOrdineFornitoreDTO(StateCreationRichiesta.ERROR);
      })
      .thenAccept(dto -> asyncJobService.save(identificativoJob, dto));
  }

  public OrdineFornitoreDispositivo toDevice(String[] dati) {
    OrdineFornitoreDispositivo dispositivo = new OrdineFornitoreDispositivo();
    dispositivo.setSeriale(dati[0]);
    if(dati.length>1)
      dispositivo.setPan(dati[1]);
    if(dati.length>2)
      dispositivo.setTipoHardware(dati[2]);
    if(dati.length>3)
      dispositivo.setDataScadenza(deviceOrdineFornitoreService.parseDate(dati[3]));
    return dispositivo;
  }

  public void asyncGenerateDeviceForOrdineFornitore(String identificativoJob, OrdineFornitoreDTO ordineFornitoreDTO,
                                                    Collection<OrdineFornitoreDispositivo> dispositivi, String note) {

    log.info("Start async generate Device to OrdineFornitore {} for Identificativo JOB {}", ordineFornitoreDTO, identificativoJob);
    CompletableFuture.supplyAsync(() -> {
      try {
        return generateDeviceForOrdineFornitore(ordineFornitoreDTO, dispositivi, note);
      } catch (Exception ex) {
        log.error("Exception: ", ex);
        throw new RuntimeException(ex);
      }
    })
      .exceptionally(e -> {
        log.error("Exception in generate Device to OrdineFornitore {} ASYNC for identificativo job: {}", ordineFornitoreDTO,
                  identificativoJob, e);
        return new ResponseOrdineFornitoreDTO(StateCreationRichiesta.ERROR);
      })
      .thenAccept(dto -> asyncJobService.save(identificativoJob, dto));
  }

  public List<String[]> getRowListFromFile(String token, String identificativoDocumento) {
    return documentClient.getListSerialDeviceFromCSVSemicolon(token, identificativoDocumento);
  }

  public ResponseOrdineFornitoreDTO getResponseByIdentificativoJob(String identificativoJob) {
    ResponseOrdineFornitoreDTO value = (ResponseOrdineFornitoreDTO) asyncJobService.getValue(identificativoJob);
    log.info("Find in cache for Identificativo Job {} : {}", identificativoJob, value);
    return value;
  }

  public OrdineFornitoreDTO save(OrdineFornitoreDTO ordineFornitoreDTO) {
    return ordineFornitoreServiceExt.save(ordineFornitoreDTO);
  }

  private ResponseOrdineFornitoreDTO getResponse(StateCreationRichiesta state, Set<String> serialiToSkipForLength,
                                                 Set<Dispositivo> serialiToSkipDeviceExists) {
    HashMap<String, List<AnomaliaOrdineFornitoreDTO>> warnings = new HashMap<>();

    if (serialiToSkipForLength != null && !serialiToSkipForLength.isEmpty()) {
      List<AnomaliaOrdineFornitoreDTO> listSerialiLengthFailure = new ArrayList<>();
      listSerialiLengthFailure.addAll(serialiToSkipForLength.stream()
        .map(s -> buildAnomaliaOrdineFornitoreDTO(s))
        .collect(toList()));
      warnings.put(WarningCodeOrdineFornitore.SERIALE_NOT_VALID.name(), listSerialiLengthFailure);
    }

    if (serialiToSkipDeviceExists != null && !serialiToSkipDeviceExists.isEmpty()) {
      List<AnomaliaOrdineFornitoreDTO> listSerialiDeviceExists = new ArrayList<>();
      listSerialiDeviceExists.addAll(serialiToSkipDeviceExists.stream()
        .map(d -> buildAnomaliaOrdineFornitoreDTO(d))
        .collect(toList()));
      warnings.put(WarningCodeOrdineFornitore.SERIALE_ALREADY_EXISTS.name(), listSerialiDeviceExists);
    }

    return getResponse(state, warnings);
  }

  private ResponseOrdineFornitoreDTO getResponse(StateCreationRichiesta state, HashMap<String, List<AnomaliaOrdineFornitoreDTO>> warnings) {
    ResponseOrdineFornitoreDTO responseDto = new ResponseOrdineFornitoreDTO(state);
    responseDto.setWarnings(warnings);
    return responseDto;
  }

  private ResponseOrdineFornitoreDTO getResponse(StateCreationRichiesta state) {
    return new ResponseOrdineFornitoreDTO(state);
  }

  private AnomaliaOrdineFornitoreDTO buildAnomaliaOrdineFornitoreDTO(Dispositivo device) {
    String                     seriale = device.getSeriale();
    AnomaliaOrdineFornitoreDTO dto     = new AnomaliaOrdineFornitoreDTO(seriale);
    dto.setStatoDispositivo(device.getStato()
      .name());
    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    dto.setTipoDispositivo(tipoDispositivo != null ? tipoDispositivo.getNome() != null ? tipoDispositivo.getNome()
      .name() : null : null);

    dto.setProduttore(tipoDispositivo != null ? tipoDispositivo.getProduttore() != null ? tipoDispositivo.getProduttore()
      .getNome() : null : null);

    ClienteFai clienteFaiDispositivo = device.getContratto() != null ? device.getContratto()
      .getClienteFai() : null;
    dto.setCodiceCliente(clienteFaiDispositivo != null ? clienteFaiDispositivo.getCodiceCliente() : null);
    dto.setRagioneSociale(clienteFaiDispositivo != null ? clienteFaiDispositivo.getRagioneSociale() : null);
    return dto;
  }

  private AnomaliaOrdineFornitoreDTO buildAnomaliaOrdineFornitoreDTO(String seriale) {
    AnomaliaOrdineFornitoreDTO dto = new AnomaliaOrdineFornitoreDTO(seriale);
    return dto;
  }

}
