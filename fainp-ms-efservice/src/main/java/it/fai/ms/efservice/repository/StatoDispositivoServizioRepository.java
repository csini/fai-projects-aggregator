package it.fai.ms.efservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.StatoDispositivoServizio;


/**
 * Spring Data JPA repository for the StatoDispositivoServizio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StatoDispositivoServizioRepository extends JpaRepository<StatoDispositivoServizio, Long>, JpaSpecificationExecutor<StatoDispositivoServizio> {

}
