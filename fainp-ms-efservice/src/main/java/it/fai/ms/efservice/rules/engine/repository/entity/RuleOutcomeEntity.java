package it.fai.ms.efservice.rules.engine.repository.entity;

import java.io.Serializable;
import java.util.Objects;

public class RuleOutcomeEntity implements Serializable {

  public class Failure implements Serializable {

    private static final long serialVersionUID = 6601062532718102028L;

    private String failureCode;
    private String failureMess;

    public Failure(final String _code, final String _mess) {
      failureCode = _code;
      failureMess = _mess;
    }

    @Override
    public boolean equals(final Object _obj) {
      boolean isEquals = false;
      if (getClass().isInstance(_obj)) {
        isEquals = Objects.equals(((Failure) _obj).getFailureCode(), getFailureCode())
                   && Objects.equals(((Failure) _obj).getFailureMess(), getFailureMess());
      }
      return isEquals;
    }

    public String getFailureCode() {
      return failureCode;
    }

    public String getFailureMess() {
      return failureMess;
    }

    @Override
    public int hashCode() {
      return Objects.hash(failureCode, failureMess);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("Failure [");
      if (failureCode != null) {
        builder.append("failureCode=");
        builder.append(failureCode);
        builder.append(", ");
      }
      if (failureMess != null) {
        builder.append("failureMess=");
        builder.append(failureMess);
      }
      builder.append("]");
      return builder.toString();
    }

  }

  private static final long serialVersionUID = -7973628047018629696L;

  private Failure failure;
  private boolean outcome;

  public RuleOutcomeEntity(boolean _outcome) {
    outcome = _outcome;
  }

  public RuleOutcomeEntity(boolean _outcome, String _failureCode, String _failureMess) {
    outcome = _outcome;
    failure = new Failure(_failureCode, _failureMess);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleOutcomeEntity) _obj).isOutcome(), outcome)
                 && Objects.equals(((RuleOutcomeEntity) _obj).getFailure(), getFailure());
    }
    return isEquals;
  }

  public Failure getFailure() {
    return failure;
  }

  @Override
  public int hashCode() {
    return Objects.hash(outcome, failure);
  }

  public boolean isOutcome() {
    return outcome;
  }

  public void setOutcome(boolean _outcome) {
    outcome = _outcome;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleOutcomeEntity [");
    if (failure != null) {
      builder.append("failure=");
      builder.append(failure);
      builder.append(", ");
    }
    builder.append("outcome=");
    builder.append(outcome);
    builder.append("]");
    return builder.toString();
  }

}
