package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import it.fai.ms.efservice.service.dto.AssociazioneVeicoliCounterDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.mapper.AssociazioneDVMapper;
import it.fai.ms.efservice.service.mapper.DispositivoMapper;
import it.fai.ms.efservice.web.rest.vm.NumeroDispositiviVM;

@Service
@Transactional
public class AssociazioneDVServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AssociazioneDVRepository                associazioneDVRepository;
  private final DispositivoMapper                       dispositivoMapper;
  private final AssociazioneDVMapper                    associazioneDVMapper;
  private final DispositivoServiceExt                   dispositivoServiceExt;
  private final ContrattoRepositoryExt contrattoRepositoryExt;
  private final ModificationOperationsDeviceTypeService modificationOperationsDeviceTypeService;

  public AssociazioneDVServiceExt(AssociazioneDVRepository _associazioneDVRepository, DispositivoMapper _dispositivoMapper,
                                  AssociazioneDVMapper _associazioneDVMapper, DispositivoServiceExt _dispositivoServiceExt,
                                  ModificationOperationsDeviceTypeService _modificationOperationsDeviceTypeService,ContrattoRepositoryExt _contrattoRepositoryExt) {
    associazioneDVRepository = _associazioneDVRepository;
    dispositivoMapper = _dispositivoMapper;
    associazioneDVMapper = _associazioneDVMapper;
    dispositivoServiceExt = _dispositivoServiceExt;
    modificationOperationsDeviceTypeService = _modificationOperationsDeviceTypeService;
    contrattoRepositoryExt = _contrattoRepositoryExt;
  }

  public List<AssociazioneDVDTO> findAssociazioneDVbyIdDispositivo(final Long _deviceId) {
    return associazioneDVRepository.findByDispositivoId(_deviceId)
                                   .stream()
                                   .map(associazioneDVMapper::toDto)
                                   .collect(toList());
  }

  public List<DispositivoDTO> findDispositiviByVehicle(final String _vehicleUuid, String _codiceAzienda) {
    // Added filter on codiceAzienda; FAINP-1147
    List<Dispositivo> devicesByVehicle = allVehicleDevicesByVehicleUuid(_vehicleUuid);
    List<Dispositivo> deviceFiltered = filterDeviceByClientCode(devicesByVehicle, _codiceAzienda);
    List<DispositivoDTO> devicesDTO = convertToDtoWithFileAllegatoAndSetModificationOperations(deviceFiltered);
    return devicesDTO;
  }

  private List<DispositivoDTO> convertToDtoWithFileAllegatoAndSetModificationOperations(List<Dispositivo> deviceFiltered) {
    return deviceFiltered.stream()
                         .map(device -> {
                           List<String> operations = modificationOperationsDeviceTypeService.getModificationOperations(device.getTipoDispositivo()
                                                                                                                             .getNome());
                           DispositivoDTO deviceDTO = dispositivoServiceExt.mapperWithFileAllegato(device);
                           deviceDTO.setOperazioniModificaPossibili(operations);
                           return deviceDTO;
                         })
                         .collect(toList());
  }

  private List<Dispositivo> filterDeviceByClientCode(List<Dispositivo> devices, String codiceCliente) {
    return devices.stream()
                  .filter(d -> d.getContratto() != null)
                  .filter(d -> d.getContratto()
                                .getClienteFai() != null)
                  .filter(d -> d.getContratto()
                                .getClienteFai()
                                .getCodiceCliente()
                                .equalsIgnoreCase(codiceCliente))
                  .collect(toList());
  }

  private List<Dispositivo> allVehicleDevicesByVehicleUuid(final String _vehicleUuid) {
    return associazioneDVRepository.findByUuidVeicolo(_vehicleUuid)
                                   .stream()
                                   .map(AssociazioneDV::getDispositivo)
                                   .collect(toList());
  }

  public HashMap<String, List<DispositivoDTO>> allVehicleDevicesByVehicleUuidsAndStato(final Set<String> _vehicleUuids,
                                                                                       final String statoDispositivo) {
    log.debug("Request all devices by vehicle with ids : {}, stato: {}", _vehicleUuids, statoDispositivo);
    List<AssociazioneDV> assDeviceVehicle = findAssociazioneDeviceVehicle(_vehicleUuids);
    List<AssociazioneDV> assDeviceVehicleFilteredByStatus = filterByDeviceStatus(assDeviceVehicle, statoDispositivo);
    HashMap<String, List<DispositivoDTO>> mapVehicleDeviceDTO = createMapVehicleDeviceDTO(assDeviceVehicleFilteredByStatus);
    return mapVehicleDeviceDTO;
  }
  
  public NumeroDispositiviVM countDispositiviPerVeicolo(String vehicleUuid, String uuidContratto, Collection<StatoDispositivo> statiDispositivo ) {
	
	Optional<Contratto> contratto = contrattoRepositoryExt.findByIdentificativo(uuidContratto);
	if(contratto.isPresent()) {
		List<NumeroDispositiviVM> count = associazioneDVRepository.countDispositiviPerVeicolo(vehicleUuid,contratto.get().getClienteFai(),statiDispositivo);
		return count.isEmpty() ? new NumeroDispositiviVM(0, contratto.get().getClienteFai().getCodiceCliente()) : count.get(0);
	}else {
		log.warn("Contratto {} non found",uuidContratto);
		return null;
	}
  }

  private HashMap<String, List<DispositivoDTO>> createMapVehicleDeviceDTO(List<AssociazioneDV> assDeviceVehicleFilteredByStatus) {
    return assDeviceVehicleFilteredByStatus.stream()
                                           .collect(toMap(assDv -> assDv.getUuidVeicolo(),
                                                          assDv -> new ArrayList<DispositivoDTO>(Arrays.asList(dispositivoMapper.toDto(assDv.getDispositivo()))),
                                                          (list1, list2) -> {
                                                            list1.addAll(list2);
                                                            return list1;
                                                          }, HashMap::new));
  }

  private List<AssociazioneDV> filterByDeviceStatus(List<AssociazioneDV> assDeviceVehicle, String statoDispositivo) {
    return assDeviceVehicle.parallelStream()
                           .filter(device -> statoDispositivo.equals("blue") ? device.getDispositivo()
                                                                                     .getStato()
                                                                                     .isBlue()
                                                                             : (statoDispositivo.equals("yellow") ? device.getDispositivo()
                                                                                                                          .getStato()
                                                                                                                          .isYellow()
                                                                                                                  : device.getDispositivo()
                                                                                                                          .getStato()
                                                                                                                          .isGreen()))
                           .collect(toList());
  }

  private List<AssociazioneDV> findAssociazioneDeviceVehicle(Set<String> _vehicleUuids) {
    return associazioneDVRepository.findByUuidVeicoloIn(_vehicleUuids);
  }

  public List<AssociazioneVeicoliCounterDTO> getAssociazioneVeicoliCounter(List<String> uuids) {
    log.debug("Request all devices by vehicle counter");
    return associazioneDVRepository.getAssociazioneVeicoliCounter(uuids);
  }

  public List<AssociazioneDV> findByUuidVeicoloAndCodiceCliente(String uuidVehicle, String codiceCliente) {
    List<AssociazioneDV> associationDeviceVehicle = associazioneDVRepository.findByUuidVeicoloAndCodiceClienteFai(uuidVehicle,
                                                                                                                  codiceCliente);
    if (associationDeviceVehicle == null || associationDeviceVehicle.isEmpty()) {
      associationDeviceVehicle = new ArrayList<>();
    }
    return associationDeviceVehicle;
  }

  public AssociazioneDV save(AssociazioneDV associazioneDV) {
    return associazioneDVRepository.save(associazioneDV);
  }

  public List<AssociazioneDV> findAndFilterActiveStatusAndDeviceTypeByVehicleAndClientCode(String uuidVehicle, String codiceCliente,
                                                                                           List<TipoDispositivoEnum> deviceTypesFilter) {
    List<AssociazioneDV> assDvs = findByUuidVeicoloAndCodiceCliente(uuidVehicle, codiceCliente);
    List<AssociazioneDV> associazioneDvOpt = filterByActiveStatusDeviceAndDeviceTypes(assDvs, deviceTypesFilter);
    if (associazioneDvOpt.isEmpty()) {
      log.info("Not found associazione DeviceVehicle for uuidVehicle {} and deviceTypes {}", uuidVehicle, deviceTypesFilter);
      return null;
    }

    return associazioneDvOpt;
  }

  private List<AssociazioneDV> filterByActiveStatusDeviceAndDeviceTypes(List<AssociazioneDV> assDvs,
                                                                        List<TipoDispositivoEnum> deviceTypesFilter) {
    return assDvs.stream()
                 .filter(asdv -> asdv.getDispositivo()
                                     .getStato()
                                     .isGreen())
                 .filter(asdv -> deviceTypesFilter.contains(asdv.getDispositivo()
                                                                .getTipoDispositivo()
                                                                .getNome()))
                 .collect(Collectors.toList());
  }

  public AssociazioneDV assignNewUuidVehicle(AssociazioneDV assDv, String newUuidVehicle) {
    if (StringUtils.isNotBlank(newUuidVehicle)) {
      assDv.setUuidVeicolo(newUuidVehicle);
      assDv.setData(Instant.now());
      assDv = save(assDv);
      log.info("Saved new identificativo veicolo: {} on Associazione DV: {}", newUuidVehicle, assDv);
    }
    return assDv;
  }

}
