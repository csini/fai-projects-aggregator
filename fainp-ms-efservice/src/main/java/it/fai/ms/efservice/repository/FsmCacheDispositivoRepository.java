package it.fai.ms.efservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.FsmCacheDispositivo;

@Repository
public interface FsmCacheDispositivoRepository extends JpaRepository<FsmCacheDispositivo, Long> {

}
