package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_TelepassEU implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;

  public RuleEngine_Device_TelepassEU(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome,
                                      final RuleEngineServiceType _serviceTypeRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
    serviceTypeRuleContext = _serviceTypeRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;
    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("devType", deviceTypeRuleContext);
    context.put("serType", serviceTypeRuleContext);
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean careac = (serType.id.id == 'AREA_C');");
    rules.add("boolean germania = (serType.id.id == 'PEDAGGI_GERMANIA');");
    rules.add("if (!careac && vehicle.category == null) return 'category';");

    rules.add("boolean c1 = (vehicle.category != null && (vehicle.category == 'N1' || vehicle.category == 'M1'));");
    rules.add("if (!careac && !germania && c1) return 'categoryN1M1';");

    rules.add("if (!careac && !germania && vehicle.axes == null) return 'axesnotset';");

    rules.add("boolean c2 = (vehicle.axes != null && (vehicle.axes < 1 || vehicle.axes > 4));");
    rules.add("if (!careac && !germania && c2) return 'axesvalue';");

    rules.add("if (!careac && !germania && (vehicle.weight == null || vehicle.weight.loneVehicleTareWeight == null)) return 'taranotset';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.loneVehicleTareWeight <= 3500) return 'tara3500';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.loneVehicleTareWeight > 20000) return 'taramaxvalue';");

    rules.add("if (!careac && !germania && (vehicle.weight == null || vehicle.weight.loneVehicleGrossWeight == null)) return 'grossnotset';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight <= 3500) return 'gross3500';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight <= vehicle.weight.loneVehicleTareWeight) return 'grosslseqtare';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight > 32000) return 'grossmaxvalue';");

    rules.add("if (!careac && !germania && (vehicle.weight == null || vehicle.weight.trainsetGrossWeight == null)) return 'trainsetnotset';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.trainsetGrossWeight <= 3500) return 'trainset3500';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.trainsetGrossWeight < vehicle.weight.loneVehicleGrossWeight) return 'trainsetlsgross';");
    rules.add("if (!careac && !germania && vehicle.weight != null && vehicle.weight.trainsetGrossWeight > 150000) return 'trainsetmaxvalue';");

    rules.add("boolean c5 = (vehicle.dieselParticulateFilter == null || (vehicle.dieselParticulateFilter.filterClass != null && vehicle.dieselParticulateFilter.filterClass != empty));");
    rules.add("if (!careac && !germania && !c5) return 'dieselParticulateFilter';");

    rules.add("if (!careac && !germania && vehicle.height == null) return 'heightnotset';");

    rules.add("boolean c6 = (vehicle.height != null && vehicle.height > 3f);");
    rules.add("if (!careac && !germania && !c6) return 'height';");

    rules.add("boolean clength = ((vehicle.type == 'AUTOBUS') || (vehicle.type != 'AUTOBUS' && vehicle.length <= 7.5));");
    rules.add("if (careac && !clength) return 'typelength';");
    rules.add("boolean cdiesel1 = (vehicle.fuelType == 'DIESEL' && vehicle.euroVehicle < 4);");
    rules.add("if (careac && cdiesel1) return 'dieseleuro';");
    rules.add("boolean cdiesel2 = (vehicle.fuelType == 'DIESEL' && (vehicle.euroVehicle == 4)  && vehicle.dieselParticulateFilter == null);");
    rules.add("if (careac && cdiesel2) return 'dieselfap';");
    rules.add("boolean cbenzina = (vehicle.fuelType == 'BENZINA' && vehicle.euroVehicle < 1);");
    rules.add("if (careac && cbenzina) return 'benzina';");

    rules.add("if (germania && (vehicle.weight == null || vehicle.weight.loneVehicleGrossWeight == null)) return 'grossnotset';");
    rules.add("if (germania && vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight != null && vehicle.weight.loneVehicleGrossWeight <= 7500) return 'gross7500';");
    rules.add("boolean numberChassis = (vehicle.numberChassis != null);");
    rules.add("if (germania && vehicle.numberChassis == null) return 'numberChassis';");

    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);

    String res = null;
    switch (preconditionCode) {
    case AXES:
      res = "axes must be > 0 and < 5";
      break;
    case AXES_NOT_SET:
      res = "axes must be present";
      break;
    case WEIGHT:
      res = "weight must be present";
      break;
    case WEIGHT3500:
      res = "weight is less than 3500";
      break;
    case CATEGORY_N1M1:
      res = "category must not be N1 or M1";
      break;
    case CATEGORY:
      res = "category must be present";
      break;
    case COUNTRY:
      res = "country must be present";
      break;
    case LICENSE_PLATE:
      res = "license plate must be present";
      break;
    case EURO_CLASS:
      res = "euro class plate must be present";
      break;
    case MAKE:
      res = "make must be present";
      break;
    case TYPE:
      res = "type must be present";
      break;
    case DIESEL_PARTICULAR_FILTER:
      res = "diesel particulate filter class must be present";
      break;
    case HEIGHT_NOT_SET:
      res = "Height must be set";
      break;
    case HEIGHT:
      res = "height must be > 3 mt";
      break;
    case TYPE_LENGTH:
      res = "Vehicle type not AUTOBUS length must be <= 7,5mt";
      break;
    case DIESEL_EURO:
      res = "Vehicle diesel euro must be > 3";
      break;
    case DIESEL_FAP:
      res = "Vehicle diesel euro 4 must have FAP";
      break;
    case BENZINA:
      res = "Vehicle benzina euro must be >= 1";
      break;
    case TARA_NOT_SET:
      res = "Tara weight must be set";
      break;
    case TARA_3500:
      res = "Tara weight must be greater than 3500 kg";
      break;
    case TARA_MAX_VALUE_20000:
      res = "Max value of tara weight is 20000 kg";
      break;
    case GROSS_NOT_SET:
      res = "Gross weight must be set";
      break;
    case GROSS_3500:
      res = "Gross weight must be greater than 3500 kg";
      break;
    case GROSS_LS_EQ_TARA:
      res = "Gross weight must be greater than tara weight";
      break;
    case GROSS_MAX_VALUE_32000:
      res = "Max value of gross weight is 32000 kg";
      break;
    case TRAINSET_NOT_SET:
      res = "Trainset weight must be set";
      break;
    case TRAINSET_3500:
      res = "Trainset weight must be greater than 3500 kg";
      break;
    case TRAINSET_LS_GROSS:
      res = "Trainset weight must be greater equal than gross weight";
      break;
    case TRAINSET_MAX_VALUE_150000:
      res = "Max value of trainset weight is 150000 kg";
      break;
    case GROSS_7500:
      res = "Gross weight must be greater than 7500 kg";
      break;
    case NUMBER_CHASSIS_EMPTY:
      res = "Vehicle must be set Number Chassis";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
