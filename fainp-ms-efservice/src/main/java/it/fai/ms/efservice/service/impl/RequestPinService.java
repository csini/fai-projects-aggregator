package it.fai.ms.efservice.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.TrackyCardClient;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.MailActivatedDeviceService;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;

@Service
@Transactional
public class RequestPinService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String authorizationToken;
  private final TrackyCardClient trackyCardClient;
  private final MailActivatedDeviceService mailActivatedDeviceService;
  private final DispositivoServiceExt deviceService;
  private final VehicleWizardCacheServiceExt vehicleService;


  public RequestPinService(
   final DispositivoServiceExt    _deviceService,
    final MailActivatedDeviceService _mailActivatedDeviceService,
    final TrackyCardClient _trackyCardClient,
    VehicleWizardCacheServiceExt vehicleService,
    @Value("${efservice.authorizationHeader}") String _authorizationToken) {

    deviceService = _deviceService;
    mailActivatedDeviceService = _mailActivatedDeviceService;
    this.vehicleService = vehicleService;
    authorizationToken = _authorizationToken;
    trackyCardClient = _trackyCardClient;
  }

  //TODO chiamare trackycard per aver il pin e inviare mail tramite notification
  public List<Long> requestTrackycardPin(TipoDispositivoEnum tipoDispositivo, String codiceCliente, String identifierDevice) throws Exception {

    log.info("Load device: {}", identifierDevice);
    Dispositivo device = deviceService.findOneByIdentificativo(identifierDevice);

    String trackycardSeriale = Optional.ofNullable(device).map(dispositivo -> dispositivo.getSeriale()).orElse("NOT_FOUND");
    if ("NOT_FOUND".equalsIgnoreCase(trackycardSeriale)){
      throw new IllegalStateException("Cannot find device or seriale for this device id: "+identifierDevice);
    }
    String pin = trackyCardClient.trackcardPin(authorizationToken, trackycardSeriale);


    boolean res = mailActivatedDeviceService.sendTrackyCardPin(codiceCliente, vehicleService.getTargaByUuidVehicle(identifierDevice),trackycardSeriale, pin);

    return Arrays.asList(res ? 1l : 0l);
  }



}
