package it.fai.ms.efservice.service.fsm.config.gransanbernardo;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaGranSanBernardoFurtoSmarrimentoConfig.MOD_TES_GRAN_SANBERNARDO_FURTO_SMARRIMENTO)
public class FsmModificaGranSanBernardoFurtoSmarrimentoConfig extends  FsmRichiestaConfig {
  
  public static final String MOD_TES_GRAN_SANBERNARDO_FURTO_SMARRIMENTO = "modTesGranSanbernardoFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public FsmModificaGranSanBernardoFurtoSmarrimentoConfig(final FsmSenderToQueue _senderFsmService) {
    super();
    this.senderFsmService = _senderFsmService;
  }
  
  @Override
  protected Logger getLogger() {
    return log;
  }
  
  @Override
  protected String getMachineId() {
    return FsmType.MOD_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaGranSanBernardoFurtoSmarrimento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
    .source(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.SMARRIMENTO)
    .guard(new GuardSmarrimento())
    .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.FURTO)
    .guard(new GuardFurto())
    .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.SMARRIMENTO)
    .target(StatoRichiesta.EVASO)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.FURTO)
    .target(StatoRichiesta.EVASO)
    .action(new FsmActionGeneric());
  }

}
