package it.fai.ms.efservice.service.jms.listener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.DmlPostSaveEvent;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlListener;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;
import it.fai.ms.common.jms.FaiMessageListener;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;
import it.fai.ms.efservice.service.jms.listener.mapper.VeicoloDmlListenerMapper;
import it.fai.ms.efservice.service.jms.listener.repository.VehicleDMLDTORepository;

@Service
@Transactional
public class JmsDmlVeicoliSaveListener extends AbstractMappableDmlListener<VehicleDMLDTO, VehicleDMLDTO> implements FaiMessageListener {

  private final Logger log = LoggerFactory.getLogger(JmsDmlVeicoliSaveListener.class);

  @Value("${server.port}")
  private Long serverPort;

  private List<VehicleConsumer> consumers = new LinkedList<>();

  public JmsDmlVeicoliSaveListener(VehicleDMLDTORepository vehicleDMLDTORepositoryImpl, VeicoloDmlListenerMapper veicoloDmlListenerMapper,
                                   ApplicationEventPublisher publisher, final List<VehicleConsumer> consumers) {

    super(VehicleDMLDTO.class, vehicleDMLDTORepositoryImpl, veicoloDmlListenerMapper, publisher);
    this.consumers = consumers;
  }

  @EventListener
  public void handleVehicleDMLDTOPostSave(DmlPostSaveEvent<VehicleDMLDTO> event) {
    log.info("handleVeicoloPostSave received event : {}", event);
    try {

      VehicleDMLDTO persistedEntity = event.getPersistedEntity();

      log.info("Call consumers with dto : {}", persistedEntity);
      VehicleDMLDTO dto = persistedEntity;
      if (dto.getIdentificativo() == null) {
        return;
      }

      // if (dto.getPossessi() == null || dto.getPossessi()
      // .isEmpty()) {
      // log.warn("Skip consume message to process rule because vehicle {} is not related with almost client.",
      // dto.getIdentificativo());
      // return;
      // }

      VehicleMessage vehicleMessage = new VehicleMessage();
      vehicleMessage.setAxes(dto.getNumeroAssi());
      vehicleMessage.setCategory(dto.getCategoria());
      vehicleMessage.setCountry(dto.getNazione());
      vehicleMessage.setDieselParticulateFilterClass(dto.getClasseFiltroAntiparticolato());
      vehicleMessage.setEuroClass(dto.getEuro());
      vehicleMessage.setLibretto(dto.getUuidDocumento());
      vehicleMessage.setLicensePlate(dto.getTarga());
      vehicleMessage.setLoneVehicleGrossWeight(dto.getMassa());
      vehicleMessage.setLoneVehicleTareWeight(dto.getTara());
      vehicleMessage.setState(dto.getStato());
      vehicleMessage.setTrainsetGrossWeight(dto.getMassaConvoglio());
      vehicleMessage.setType(dto.getTipo());
      vehicleMessage.setUuid(dto.getIdentificativo());
      vehicleMessage.setMake(dto.getMarca());
      vehicleMessage.setFuelType(dto.getAlimentazione());
      vehicleMessage.setLength(dto.getLunghezza());
      vehicleMessage.setHeight(dto.getAltezza());
      vehicleMessage.setPayload(dto.getPortata());
      vehicleMessage.setWidth(dto.getLarghezza());
      vehicleMessage.setNumeroAssiPienoCarico(dto.getNumeroAssiPienoCarico());
      vehicleMessage.setDataUltimaModifica(dto.getDmlRevisionTimestamp());
      vehicleMessage.setNumberChassis(dto.getNumeroTelaio());
      vehicleMessage.setColor(dto.getColore());
      vehicleMessage.setModel(dto.getModello());

      log.info("Received message: {}, dispatching message to {}", vehicleMessage, consumers);
      consumers.stream()
               .forEach(consumer -> {
                 log.debug("Dispatching Message {} to consumer {}", vehicleMessage, consumer);
                 consumer.consume(vehicleMessage);
                 log.debug("Message consumed");
               });
      log.info("Done with message");
    } catch (Exception e) {
      log.error("Error", e);
    }
  }

  public boolean isValidToPersist(VehicleDMLDTO dto) {
    // if (dto.getPossessi() == null || dto.getPossessi()
    // .isEmpty()) {
    // log.warn("Skip JmsDmlVeicoliSaveListener because vehicle {} is not related with almost client.",
    // dto.getIdentificativo());
    // return false;
    // }
    return true;
  }

  @Override
  public String getSubcriptionName() {
    String className = getClass().getName();
    StringBuilder sb = new StringBuilder(className);

    String hostName = "localhost";
    try {
      InetAddress address = InetAddress.getLocalHost();
      if (address != null) {
        hostName = address.getHostName();
        sb.append("_")
          .append(hostName);
      }
    } catch (UnknownHostException e) {
      log.error("Not found Address information....");
    }

    if (serverPort != null) {
      sb.append("_")
        .append(serverPort);
    } else {
      log.error("Not load ServerPort properties...");
    }

    String subscriptionName = sb.toString();
    log.trace("SubscriptionaName listener: {}", subscriptionName);
    return subscriptionName;
  }
}
