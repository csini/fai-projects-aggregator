package it.fai.ms.efservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Produttore.
 */
@Entity
@Table(name = "produttore")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Produttore implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nome")
  private String nome;

  @Column(name = "codice_fornitore_nav")
  private String codiceFornitoreNav;

  @OneToMany(mappedBy = "produttore")
  @JsonIgnore
  private Set<Contratto> contrattos = new HashSet<>();

  @OneToMany(mappedBy = "produttore")
  private Set<TipoDispositivo> tipoDispositivos = new HashSet<>();

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public Produttore nome(String nome) {
    this.nome = nome;
    return this;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCodiceFornitoreNav() {
    return codiceFornitoreNav;
  }

  public Produttore codiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
    return this;
  }

  public void setCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
  }

  public Set<Contratto> getContrattos() {
    return contrattos;
  }

  public Produttore contrattos(Set<Contratto> contrattos) {
    this.contrattos = contrattos;
    return this;
  }

  public Produttore addContratto(Contratto contratto) {
    this.contrattos.add(contratto);
    contratto.setProduttore(this);
    return this;
  }

  public Produttore removeContratto(Contratto contratto) {
    this.contrattos.remove(contratto);
    contratto.setProduttore(null);
    return this;
  }

  public void setContrattos(Set<Contratto> contrattos) {
    this.contrattos = contrattos;
  }

  public Set<TipoDispositivo> getTipoDispositivos() {
    return tipoDispositivos;
  }

  public Produttore tipoDispositivos(Set<TipoDispositivo> tipoDispositivos) {
    this.tipoDispositivos = tipoDispositivos;
    return this;
  }

  public Produttore addTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivos.add(tipoDispositivo);
    tipoDispositivo.setProduttore(this);
    return this;
  }

  public Produttore removeTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivos.remove(tipoDispositivo);
    tipoDispositivo.setProduttore(null);
    return this;
  }

  public void setTipoDispositivos(Set<TipoDispositivo> tipoDispositivos) {
    this.tipoDispositivos = tipoDispositivos;
  }
  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Produttore produttore = (Produttore) o;
    if (produttore.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), produttore.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "Produttore{" + "id=" + getId() + ", nome='" + getNome() + "'" + ", codiceFornitoreNav='" + getCodiceFornitoreNav() + "'" + "}";
  }
}
