package it.fai.ms.efservice.web.rest.errors;

import org.springframework.http.HttpStatus;

public class CustomException extends Exception {

	private static final long serialVersionUID = 495004025947558797L;

	private String errorMessage = "";

	private PayloadError payloadError;
	
	private HttpStatus httpStatus = HttpStatus.OK;
 
	public CustomException(HttpStatus httpStatus, PayloadError payloadError) {
		super("Payload Error");
		this.payloadError = payloadError;
		this.httpStatus = httpStatus;
	}
	
//	public CustomException(HttpStatus httpStatus, PayloadError payloadError, String errorMessage) {
//		super(errorMessage);
//		this.payloadError = payloadError;
//		this.httpStatus = httpStatus;
//		this.errorMessage = errorMessage;
//	}
	
	public static CustomException builder(HttpStatus httpStatus) {
		return new CustomException(httpStatus, PayloadError.builder());
	}
	
	public CustomException add(Errno errno) {
		this.payloadError.add(errno);
		return this;
	}

	public CustomException() {
		super();
	}

	public PayloadError getPayloadError() {
		return payloadError;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
}
