package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.RichiestaDTO;

public interface RichiestaTemporaryBackofficeService {

  public RichiestaDTO saveRichiestaForTemporayBackoffice(RichiestaDTO richiestaDTO) throws Exception;
}
