package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import it.fai.ms.efservice.service.TipoDispositivoService;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;

/**
 * REST controller for managing TipoDispositivo.
 */
@RestController
@RequestMapping("/api")
public class TipoDispositivoResource {

  private final Logger log = LoggerFactory.getLogger(TipoDispositivoResource.class);

  private static final String ENTITY_NAME = "tipoDispositivo";

  private final TipoDispositivoService tipoDispositivoService;

  public TipoDispositivoResource(TipoDispositivoService tipoDispositivoService) {
    this.tipoDispositivoService = tipoDispositivoService;
  }

  /**
   * POST /tipo-dispositivos : Create a new tipoDispositivo.
   *
   * @param tipoDispositivoDTO
   *          the tipoDispositivoDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new tipoDispositivoDTO, or with status 400
   *         (Bad Request) if the tipoDispositivo has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping("/tipo-dispositivos")
  @Timed
  public ResponseEntity<TipoDispositivoDTO> createTipoDispositivo(@Valid @RequestBody TipoDispositivoDTO tipoDispositivoDTO) throws URISyntaxException {
    log.debug("REST request to save TipoDispositivo : {}", tipoDispositivoDTO);
    if (tipoDispositivoDTO.getId() != null) {
      return ResponseEntity.badRequest()
                           .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
                                                                  "A new tipoDispositivo cannot already have an ID"))
                           .body(null);
    }
    TipoDispositivoDTO result = tipoDispositivoService.save(tipoDispositivoDTO);
    return ResponseEntity.created(new URI("/api/tipo-dispositivos/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                          .toString()))
                         .body(result);
  }

  /**
   * PUT /tipo-dispositivos : Updates an existing tipoDispositivo.
   *
   * @param tipoDispositivoDTO
   *          the tipoDispositivoDTO to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated tipoDispositivoDTO, or with status 400
   *         (Bad Request) if the tipoDispositivoDTO is not valid, or with status 500 (Internal Server Error) if the
   *         tipoDispositivoDTO couldn't be updated
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PutMapping("/tipo-dispositivos")
  @Timed
  public ResponseEntity<TipoDispositivoDTO> updateTipoDispositivo(@Valid @RequestBody TipoDispositivoDTO tipoDispositivoDTO) throws URISyntaxException {
    log.debug("REST request to update TipoDispositivo : {}", tipoDispositivoDTO);
    if (tipoDispositivoDTO.getId() == null) {
      return createTipoDispositivo(tipoDispositivoDTO);
    }
    TipoDispositivoDTO result = tipoDispositivoService.save(tipoDispositivoDTO);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoDispositivoDTO.getId()
                                                                                                    .toString()))
                         .body(result);
  }

  /**
   * GET /tipo-dispositivos : get all the tipoDispositivos.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of tipoDispositivos in body
   */
  @GetMapping("/tipo-dispositivos")
  @Timed
  public List<TipoDispositivoDTO> getAllTipoDispositivos() {
    log.debug("REST request to get all TipoDispositivos");
    List<TipoDispositivoDTO> deviceTypes = tipoDispositivoService.findAll();
    return deviceTypes.stream()
                      .filter(td -> !td.isHidden())
                      .collect(toList());
  }

  /**
   * GET /tipo-dispositivos/:id : get the "id" tipoDispositivo.
   *
   * @param id
   *          the id of the tipoDispositivoDTO to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the tipoDispositivoDTO, or with status 404 (Not
   *         Found)
   */
  @GetMapping("/tipo-dispositivos/{id}")
  @Timed
  public ResponseEntity<TipoDispositivoDTO> getTipoDispositivo(@PathVariable Long id) {
    log.debug("REST request to get TipoDispositivo : {}", id);
    TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoService.findOne(id);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoDispositivoDTO));
  }

  /**
   * DELETE /tipo-dispositivos/:id : delete the "id" tipoDispositivo.
   *
   * @param id
   *          the id of the tipoDispositivoDTO to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/tipo-dispositivos/{id}")
  @Timed
  public ResponseEntity<Void> deleteTipoDispositivo(@PathVariable Long id) {
    log.debug("REST request to delete TipoDispositivo : {}", id);
    tipoDispositivoService.delete(id);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                         .build();
  }
}
