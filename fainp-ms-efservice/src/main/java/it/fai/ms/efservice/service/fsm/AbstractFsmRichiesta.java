package it.fai.ms.efservice.service.fsm;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public abstract class AbstractFsmRichiesta {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String USER_MANUAL_PREFIX = "MU_";

  private final String MANUAL_SYSTEM_PREFIX = "MS_";

  public abstract StateMachine<StatoRichiesta, RichiestaEvent> getStateMachine();

  public Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta) {
    return executeCommandToChangeState(command, richiesta, null);
  }

  public abstract Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta, String nota) ;

  public boolean isEqualType(Richiesta richiesta, TipoDispositivoEnum tipoDispositivoFsm) {
    if (richiesta != null && richiesta.getTipoDispositivo() != null && richiesta.getTipoDispositivo()
      .getNome()
      .equals(tipoDispositivoFsm)) {
      return true;
    }
    return false;
  }

  public boolean sendEventToFsm(StateMachine<StatoRichiesta, RichiestaEvent> sm, RichiestaEvent command, Richiesta richiesta, String nota) {
    final Message<RichiestaEvent> message = MessageBuilder.withPayload(command)
      .setHeader("object", richiesta)
      .setHeader("nota", nota)
      .build();
    
    boolean sended = sm.sendEvent(message);
    log.debug("Sended event to change status: {}", sended);
    if(!sended) {
      throw new RuntimeException("Not sended event...so not change status by command: " + command);
    }
    return sended;
  }

  public Collection<RichiestaEvent> getAllAvailableCommand(Richiesta richiesta) {
    Enum<StatoRichiesta> fsmStatus = richiesta.getStato();
    Collection<RichiestaEvent> events = new ArrayList<>();
    Collection<Transition<StatoRichiesta, RichiestaEvent>> transitions = getStateMachine().getTransitions();

    Set<RichiestaEvent> eventsSet = transitions.parallelStream()
      .map(tx -> tx)
      .filter(tx -> (tx != null && tx.getSource() != null && tx.getSource()
        .getId()
        .name()
        .equals(fsmStatus.name())))
      .map(tx -> tx)
      .filter(tx -> tx.getTrigger() != null)
      .map(tx -> tx.getTrigger()
        .getEvent())
      .collect(toSet());

    events.addAll(eventsSet);
    return events;
  }

  public List<String> getUserAvailableCommand(Richiesta obj) {
    Collection<RichiestaEvent> allAvailableCommand = getAllAvailableCommand(obj);
    List<String> userAvailableCommand = new ArrayList<>();

    for (RichiestaEvent event : allAvailableCommand) {
      String eventName = event.name();
      if (eventName.startsWith(USER_MANUAL_PREFIX)) {
        userAvailableCommand.add(eventName);
      }
    }

    return userAvailableCommand;
  }

  public boolean isAvailableCommand(Enum<?> command, Richiesta obj) {
    Collection<RichiestaEvent> allAvailableCommand = getAllAvailableCommand(obj);
    for (RichiestaEvent event : allAvailableCommand) {
      Enum<?> enumCmd = null;
      if (event instanceof Enum<?>) {
        enumCmd = (Enum<?>) event;

      }
      if (enumCmd.name()
        .equalsIgnoreCase(command.name())) {
        return true;
      }
    }
    return false;
  }

  public boolean isAutoTransition(Enum<?> command) {
    boolean foundCommand = true;

    Collection<Transition<StatoRichiesta, RichiestaEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoRichiesta, RichiestaEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {

      Transition<StatoRichiesta, RichiestaEvent> tx = iterator.next();
      Trigger<StatoRichiesta, RichiestaEvent> trigger = tx.getTrigger();
      if (trigger != null) {
        RichiestaEvent event = trigger.getEvent();
        if (event.name()
          .equalsIgnoreCase(command.name())) {
          foundCommand = false;
          break;
        }
      }
    }

    return foundCommand;
  }

  public boolean isStatoPossibile(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale) {
    Collection<Transition<StatoRichiesta, RichiestaEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoRichiesta, RichiestaEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StatoRichiesta, RichiestaEvent> tx = iterator.next();
      State<StatoRichiesta, RichiestaEvent> source = tx.getSource();
      if (source != null) {
        StatoRichiesta statoPartenza = source.getId();
        if (statoPartenza.name()
          .equals(statoPrecedente.name())) {

          State<StatoRichiesta, RichiestaEvent> target = tx.getTarget();
          if (target != null) {
            StatoRichiesta destinazione = target.getId();
            if (destinazione.name()
              .equals(statoAttuale.name())) {
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  public boolean isStatoManualeSistema(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale) {
    RichiestaEvent richiestaEvent = getRichiestaEvent(statoPrecedente, statoAttuale);
    if (richiestaEvent == null) {
      return false;
    }

    if (richiestaEvent.name()
      .startsWith(MANUAL_SYSTEM_PREFIX)) {
      return true;
    }
    return false;
  }

  public RichiestaEvent getRichiestaEvent(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale) {
    Collection<Transition<StatoRichiesta, RichiestaEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoRichiesta, RichiestaEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StatoRichiesta, RichiestaEvent> tx = iterator.next();
      State<StatoRichiesta, RichiestaEvent> source = tx.getSource();
      if (source != null) {
        StatoRichiesta statoPartenza = source.getId();
        if (statoPartenza.name()
          .equals(statoPrecedente.name())) {

          State<StatoRichiesta, RichiestaEvent> target = tx.getTarget();
          if (target != null) {
            StatoRichiesta destinazione = target.getId();
            if (destinazione.name()
              .equals(statoAttuale.name())) {
              Trigger<StatoRichiesta, RichiestaEvent> trigger = tx.getTrigger();
              if (trigger != null) {
                RichiestaEvent event = trigger.getEvent();
                return event;
              }
            }
          }
        }
      }
    }

    return null;
  }

  public Richiesta getUserAvailableCommandAndSetOnRichiesta(Richiesta richiesta) {
    List<String> userAvailableCommand = getUserAvailableCommand(richiesta);
    return setOperazioniPossibiliRichiesta(userAvailableCommand, richiesta);
  }

  public Richiesta setOperazioniPossibiliRichiesta(List<String> userAvailableCommand, Richiesta richiesta) {
    String operations = getOperazioniPossibiliByUserAvailableCommand(userAvailableCommand, richiesta);
    return richiesta.operazioniPossibili(operations);
  }

  public String getOperazioniPossibili(Richiesta richiesta) {
    List<String> userAvailableCommand = getUserAvailableCommand(richiesta);

    userAvailableCommand = filterAvailableCommandIfRichiestaIniziale(richiesta, userAvailableCommand);

    StringBuilder sb = new StringBuilder();
    for (String command : userAvailableCommand) {
      sb.append(command);
      sb.append(',');
    }

    String operations = sb.toString();
    if (StringUtils.isNotBlank(operations)) {
      operations = operations.substring(0, (operations.length() - 1));
    }
    return operations;
  }

  private List<String> filterAvailableCommandIfRichiestaIniziale(Richiesta richiesta, List<String> userAvailableCommand) {
    if (richiesta != null && richiesta.getStato() == StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE) {
      log.debug("Stato Richiesta: {}", richiesta.getStato());
      if (userAvailableCommand != null && !userAvailableCommand.isEmpty()) {
        log.debug("User available command before filter: {}", userAvailableCommand);
        userAvailableCommand = userAvailableCommand.parallelStream()
          .map(s -> s)
          .filter(s -> (!s.equalsIgnoreCase(RichiestaEvent.MU_ANNULLAMENTO_UTENTE.name())))
          .filter(s -> (!s.equalsIgnoreCase(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE.name())))
          .collect(toList());
        log.debug("User available command after filter: {}", userAvailableCommand);
      } else {
        log.debug("User available command is empty.");
      }
    }

    return userAvailableCommand;
  }

  public String getOperazioniPossibiliByUserAvailableCommand(List<String> userAvailableCommand, Richiesta richiesta) {
    StringBuilder sb = new StringBuilder();
    for (String command : userAvailableCommand) {
      sb.append(command);
      sb.append(',');
    }

    String operations = sb.toString();
    if (StringUtils.isNotBlank(operations)) {
      operations = operations.substring(0, (operations.length() - 1));
    }
    return operations;
  }

  public void resetStato(Richiesta richiesta) {

  }

}
