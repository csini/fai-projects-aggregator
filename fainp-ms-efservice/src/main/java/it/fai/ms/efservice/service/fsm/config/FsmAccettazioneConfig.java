/**
 * 
 */
package it.fai.ms.efservice.service.fsm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAnnullaDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

/**
 * @author Luca Vassallo
 */
@Configuration
@EnableStateMachineFactory(name = "accettazione")
public class FsmAccettazioneConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmAccettazioneConfig.class);

  private final FsmSenderToQueue fsmSenderToQueue;

  public FsmAccettazioneConfig(FsmSenderToQueue fsmSenderToQueue) {
    this.fsmSenderToQueue = fsmSenderToQueue;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.config.AbstractStateMachineConfigurerAdapter#configure(org.springframework.
   * statemachine.config.builders.StateMachineConfigurationConfigurer)
   */
  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {

    super.configure(config);
    config.withConfiguration()
          .machineId("fsmAccettazione");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, ctx -> log.trace("Target: " + ctx.getTarget()
                                                                                             .getIds()))
          .states(StatoRichiestaUtil.getStateOfAccettazione());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
               .target(StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE)
               .event(RichiestaEvent.MU_ANNULLAMENTO_UTENTE)
               .action(new FsmActionAnnullaDispositivo(fsmSenderToQueue))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
               .target(StatoRichiesta.SOSPESA)
               .event(RichiestaEvent.MU_VALUTAZIONE_PRECONDIZIONI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .action(new FsmActionAnnullaDispositivo(fsmSenderToQueue))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
               .target(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .event(RichiestaEvent.MU_ACCETTA_RICHIESTA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
               .target(StatoRichiesta.RICHIESTA_ANNULLATA)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionAnnullaDispositivo(fsmSenderToQueue))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SOSPESA)
               .target(StatoRichiesta.RICHIESTA_ANNULLATA)
               .event(RichiestaEvent.MU_ANNULLA_RICHIESTA)
               .action(new FsmActionAnnullaDispositivo(fsmSenderToQueue))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SOSPESA)
               .target(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .event(RichiestaEvent.MU_ACCETTA_RICHIESTA)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SOSPESA)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .action(new FsmActionAnnullaDispositivo(fsmSenderToQueue));
  }

}
