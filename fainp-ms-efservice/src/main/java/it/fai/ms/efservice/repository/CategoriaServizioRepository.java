package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.CategoriaServizio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CategoriaServizio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoriaServizioRepository extends JpaRepository<CategoriaServizio, Long> {

}
