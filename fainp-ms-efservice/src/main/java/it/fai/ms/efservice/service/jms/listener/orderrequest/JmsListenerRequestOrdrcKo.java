package it.fai.ms.efservice.service.jms.listener.orderrequest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.RequestOrdrcKoDTO;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
public class JmsListenerRequestOrdrcKo extends JmsObjectMessageListenerTemplate<RequestOrdrcKoDTO> implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils fsmRichiestaUtils;

  public JmsListenerRequestOrdrcKo(final FsmRichiestaUtils fsmRichiestaUtils) {
    this.fsmRichiestaUtils = fsmRichiestaUtils;
  }

  @Override
  protected void consumeMessage(RequestOrdrcKoDTO dto) {
    log.info("Received message on {} : {}", getClass(), dto);

    String identificativoRichiesta = dto.getIdentificativoRichiesta();
    String command = dto.getCommand();
    log.debug("Call Efservice to change status request {} by command {}", identificativoRichiesta, command);

    Set<String> identifiers = new HashSet<>(Arrays.asList(identificativoRichiesta));
    try {
      fsmRichiestaUtils.changeStatusRichieste(identifiers, command, dto.getMotivoSospensione(), dto.getNota());
    } catch (FsmExecuteCommandException e) {
      log.error("Not changed status for request: {} with command {}", identificativoRichiesta, command);
      log.error("FsmException: ", e);
      throw new RuntimeException(e);
    }

    log.info("Status changed for request: {}", identificativoRichiesta);
    log.debug("Message consumed...");
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.RICHIESTA_ORDRC_KO;
  }

}
