package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.mapper.RichiestaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Richiesta.
 */
@Service
@Transactional
public class RichiestaServiceImpl implements RichiestaService{

    private final Logger log = LoggerFactory.getLogger(RichiestaServiceImpl.class);

    private final RichiestaRepository richiestaRepository;

    private final RichiestaMapper richiestaMapper;

    public RichiestaServiceImpl(RichiestaRepository richiestaRepository, RichiestaMapper richiestaMapper) {
        this.richiestaRepository = richiestaRepository;
        this.richiestaMapper = richiestaMapper;
    }

    /**
     * Save a richiesta.
     *
     * @param richiestaDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RichiestaDTO save(RichiestaDTO richiestaDTO) {
        log.debug("Request to save Richiesta : {}", richiestaDTO);
        Richiesta richiesta = richiestaMapper.toEntity(richiestaDTO);
        richiesta = richiestaRepository.save(richiesta);
        return richiestaMapper.toDto(richiesta);
    }

    /**
     *  Get all the richiestas.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RichiestaDTO> findAll() {
        log.debug("Request to get all Richiestas");
        return richiestaRepository.findAllWithEagerRelationships().stream()
            .map(richiestaMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one richiesta by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RichiestaDTO findOne(Long id) {
        log.debug("Request to get Richiesta : {}", id);
        Richiesta richiesta = richiestaRepository.findOneWithEagerRelationships(id);
        return richiestaMapper.toDto(richiesta);
    }

    /**
     *  Delete the  richiesta by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Richiesta : {}", id);
        richiestaRepository.delete(id);
    }
}
