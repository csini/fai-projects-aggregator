package it.fai.ms.efservice.service.dto;

public class CaricamentoOrdineFornitoreByFileDTO extends CaricamentoOrdineFornitoreDTO {

  private static final long serialVersionUID = 5495423745270863662L;
  
  private String file;

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("CaricamentoOrdineFornitoreByFileDTO [");
    sb.append("OrdineFornitoreId=");
    sb.append(getId());
    sb.append(", FileUUID=");
    sb.append(file);
    sb.append(", SkipValidation=");
    sb.append(isSkipValidation());
    sb.append(", Note=");
    sb.append(getNote());
    sb.append("]");
    return sb.toString();
  }

}
