package it.fai.ms.efservice.service;

import it.fai.ms.common.jms.efservice.message.device.DeviceServiceNotActivableMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceNotDeactivableMessage;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

public interface ServiziManageService {

  void notDeActivableService(DeviceServiceNotDeactivableMessage message) throws FsmExecuteCommandException;

  void notActivableService(DeviceServiceNotActivableMessage message) throws FsmExecuteCommandException;
}
