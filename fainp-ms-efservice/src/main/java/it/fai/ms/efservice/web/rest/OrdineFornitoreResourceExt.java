package it.fai.ms.efservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.domain.enumeration.TipoDisposizione;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.AnagraficaGiacenzaService;
import it.fai.ms.efservice.service.OrdineFornitoreAsyncService;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreByFileDTO;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreBySerialeDTO;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTOExt;
import it.fai.ms.efservice.service.dto.ResponseOrdineFornitoreDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;

/**
 * REST controller for managing OrdineFornitore.
 */
@RestController
@RequestMapping(OrdineFornitoreResourceExt.BASE_PATH)
public class OrdineFornitoreResourceExt {

  private final Logger log = LoggerFactory.getLogger(OrdineFornitoreResourceExt.class);

  // private static final String ENTITY_NAME = "ordineFornitore";

  public static final String BASE_PATH = "/api/public";

  private static final String ORDINE_FORNITORE_DISPOSITIVO = "/ordinefornitoredispositivo";

  private static final String KEY_JOB = "job";

  private static final long RANGE_MAX = 1000;

  private static final String PREFIX_TOKEN = "Bearer ";

  private final OrdineFornitoreAsyncService asyncOrdineFornitoreService;
  private final AnagraficaGiacenzaService anagraficaGiacenzaService;

  public OrdineFornitoreResourceExt(final OrdineFornitoreAsyncService _asyncService, final AnagraficaGiacenzaService _giacenzaService) {
      asyncOrdineFornitoreService = _asyncService;
      anagraficaGiacenzaService = _giacenzaService;
  }

  @PostMapping(ORDINE_FORNITORE_DISPOSITIVO + "/seriale")
  @Timed
  public ResponseEntity<Map<String, String>> caricamentoOrdineFornitoreSeriale(@RequestBody @Valid CaricamentoOrdineFornitoreBySerialeDTO dto) throws URISyntaxException,
                                                                                                                                               CustomException {
    log.debug("REST request to generate Dispositivo from : {}", dto);
    Long ordineFornitoreId = dto.getId();
    if (ordineFornitoreId == null) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String generatedKey = asyncOrdineFornitoreService.generateKey("ORDFORNITORE");
    OrdineFornitoreDTO ordineFornitoreDTO = asyncOrdineFornitoreService.findOrdineFornitoreById(ordineFornitoreId);
    if (!dto.isSkipValidation()) {
      int numDispositiviDaCaricare = dto.getDispositivi()==null?0:dto.getDispositivi().size();
      if (!validationDiffNumDispositiviCaricati(ordineFornitoreDTO, numDispositiviDaCaricare)) {
        throw CustomException.builder(HttpStatus.BAD_REQUEST)
                             .add(Errno.VALIDATION_NUM_DEVICE_TO_LOAD);
      }
    }


    asyncOrdineFornitoreService.asyncGenerateDeviceForOrdineFornitore(generatedKey, ordineFornitoreDTO, dto.getDispositivi(),
                                                                      dto.getNote());
    return getResponse(generatedKey);
  }

  @PostMapping(ORDINE_FORNITORE_DISPOSITIVO + "/file")
  @Timed
  public ResponseEntity<Map<String, String>> caricamentoOrdineFornitoreByFile(@RequestBody @Valid CaricamentoOrdineFornitoreByFileDTO dto) throws URISyntaxException,
                                                                                                                                           CustomException {
    log.debug("REST request to generate Dispositivo from : {}", dto);
    Long ordineFornitoreId = dto.getId();
    if (ordineFornitoreId == null) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identificativoDocumento = dto.getFile();
    if (StringUtils.isBlank(identificativoDocumento)) {
      log.error("Identificativo documento is BLANK");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String generatedKey = asyncOrdineFornitoreService.generateKey("ORDFORNITORE");
    String token = PREFIX_TOKEN + SecurityUtils.getCurrentUserJWT();
    OrdineFornitoreDTO ordineFornitoreDTO = asyncOrdineFornitoreService.findOrdineFornitoreById(ordineFornitoreId);
    List<String[]> csvRow = asyncOrdineFornitoreService.getRowListFromFile(token, identificativoDocumento);
    if (!dto.isSkipValidation()) {
      long count = csvRow.size();
      long numDispositiviDaCaricare = count;
      if (!validationDiffNumDispositiviCaricati(ordineFornitoreDTO, numDispositiviDaCaricare)) {
        throw CustomException.builder(HttpStatus.BAD_REQUEST)
                             .add(Errno.VALIDATION_NUM_DEVICE_TO_LOAD);
      }
    }

    asyncOrdineFornitoreService.asyncGenerateDeviceForOrdineFornitore(generatedKey, ordineFornitoreDTO, csvRow, dto.getNote());
    return getResponse(generatedKey);
  }

  @GetMapping(ORDINE_FORNITORE_DISPOSITIVO + "/polling/{identificativo}")
  @Timed
  public ResponseEntity<ResponseOrdineFornitoreDTO> pollingOrdineFornitore(@PathVariable("identificativo") String identificativoJob) throws URISyntaxException,
                                                                                                                                     CustomException {
    log.debug("REST request to polling job: {}", identificativoJob);
    ResponseOrdineFornitoreDTO response = asyncOrdineFornitoreService.getResponseByIdentificativoJob(identificativoJob);
    return ResponseEntity.ok(response);
  }

  /**
   * POST /ordine-fornitores : Create a new ordineFornitore.
   *
   * @param ordineFornitoreDTO
   *          the ordineFornitoreDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new ordineFornitoreDTO, or with status 400
   *         (Bad Request) if the ordineFornitore has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping("/ordine-fornitores")
  @Timed
  public ResponseEntity<OrdineFornitoreDTO> createOrdineFornitore(@Valid @RequestBody OrdineFornitoreDTOExt dto) throws URISyntaxException {
    log.info("REST request to save OrdineFornitore : {}", dto);

    anagraficaGiacenzaService.updateRiordino(dto.getQuantita(), dto.getAnagraficaGiacenzaId());

    OrdineFornitoreDTO ordineFornitoreDTO = new OrdineFornitoreDTO();
    ordineFornitoreDTO.setAnagraficaGiacenzaId(dto.getAnagraficaGiacenzaId());
    ordineFornitoreDTO.setDataRiordino(Instant.now());
    ordineFornitoreDTO.setNumeroDispositiviAcquisiti(0);
    log.info("dto.getQuantita(): {}", dto.getQuantita());
    ordineFornitoreDTO.setQuantita(dto.getQuantita());
    ordineFornitoreDTO.setRichiedente(SecurityUtils.getCurrentUserLogin());
    ordineFornitoreDTO.setTipologiaDisposizione(TipoDisposizione.DA_ACQUISIRE);

    OrdineFornitoreDTO result = asyncOrdineFornitoreService.save(ordineFornitoreDTO);
    return ResponseEntity.created(new URI("/api/ordine-fornitores/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert("OrdineFornitore", result.getId()
                                                                                                .toString()))
                         .body(result);
  }

  private long getRange(String rangeStart, String rangeStop) {
    long startValue = Long.parseLong(rangeStart);
    long stopValue = Long.parseLong(rangeStop);
    long diff = stopValue - startValue + 1; // add plus one to include limit
    return diff;
  }

  private boolean validationDiffNumDispositiviCaricati(OrdineFornitoreDTO ordineFornitoreDTO, long numDispositiviDaCaricare) {
    boolean isValid = true;
    Integer quantita = ordineFornitoreDTO.getQuantita();
    if (quantita == null) {
      quantita = 0;
    }
    long quantitaLong = quantita.longValue();
    long diff = quantitaLong - numDispositiviDaCaricare;
    if (diff != 0) {
      isValid = false;
    }

    log.info("Validation -> Quantità {} - numDispositiviDaCaricare {} = {} : {}", quantita, numDispositiviDaCaricare, diff, isValid);
    return isValid;
  }

  private ResponseEntity<Map<String, String>> getResponse(String identifierJob) {
    Map<String, String> response = new HashMap<>();
    response.put(KEY_JOB, identifierJob);
    return ResponseEntity.ok(response);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
