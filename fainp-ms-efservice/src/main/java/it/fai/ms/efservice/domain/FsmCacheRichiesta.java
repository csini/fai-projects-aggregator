package it.fai.ms.efservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fsm_richiesta_cache")
public class FsmCacheRichiesta implements Serializable {
  private static final long serialVersionUID = 1008611923263326840L;

  @Id
  @Column(name = "id")
  private Long id;
  
  @Column(name = "data")
  private String data;
  
  
  public FsmCacheRichiesta() {}
  
  public FsmCacheRichiesta(Long id, String data) {
    this.id = id;
    this.data = data;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("FsmCacheRichiesta [id=");
    builder.append(id);
    builder.append(", data=");
    builder.append(data);
    builder.append("]");
    return builder.toString();
  }
  
}
