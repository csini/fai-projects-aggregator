package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

/**
 * A DTO for the Richiesta entity.
 */
public class RichiestaDTO implements Serializable {

  private Long id;

  private String identificativo;

  @NotNull
  private Instant data;

  @NotNull
  private TipoRichiesta tipo;

  @NotNull
  private StatoRichiesta stato;

  private String ultimaNota;

  private String uuidDocumento;

  private String anomalia;

  private String motivoSospensione;

  private String associazione;

  private String operazioniPossibili;

  private Instant dataModificaStato;

  private String nomeServizio;

  private Long contrattoId;

  private String contrattoCodContrattoCliente;

  private Set<DispositivoDTO> dispositivos = new HashSet<>();

  private Long ordineClienteId;

  private Long tipoDispositivoId;

  private String tipoDispositivoNome;

  private String country;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Instant getData() {
    return data;
  }

  public void setData(Instant data) {
    this.data = data;
  }

  public TipoRichiesta getTipo() {
    return tipo;
  }

  public void setTipo(TipoRichiesta tipo) {
    this.tipo = tipo;
  }

  public StatoRichiesta getStato() {
    return stato;
  }

  public void setStato(StatoRichiesta stato) {
    this.stato = stato;
  }

  public String getUltimaNota() {
    return ultimaNota;
  }

  public void setUltimaNota(String ultimaNota) {
    this.ultimaNota = ultimaNota;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }
  
  @JsonIgnore
  public List<String> getUuidDocuments() {
    List<String> uuidDocList = new ArrayList<>();
    if(this.uuidDocumento != null) {
      String[] splittedUuidDoc = StringUtils.split(this.uuidDocumento, ",");
      for (String uuidDoc : splittedUuidDoc) {
        uuidDocList.add(uuidDoc);
      }
    }
    return uuidDocList;
  }

  public void setUuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
  }
  
  public void setUuidDocuments(String... uuidDocuments) {
    this.uuidDocumento = "";
    this.uuidDocumento = String.join(",", uuidDocuments);
  }

  public String getAnomalia() {
    return anomalia;
  }

  public void setAnomalia(String anomalia) {
    this.anomalia = anomalia;
  }

  public String getMotivoSospensione() {
    return motivoSospensione;
  }

  public void setMotivoSospensione(String motivoSospensione) {
    this.motivoSospensione = motivoSospensione;
  }

  public String getAssociazione() {
    return associazione;
  }

  public void setAssociazione(String associazione) {
    this.associazione = associazione;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public String getNomeServizio() {
    return nomeServizio;
  }

  public void setNomeServizio(String nomeServizio) {
    this.nomeServizio = nomeServizio;
  }

  public Long getContrattoId() {
    return contrattoId;
  }

  public void setContrattoId(Long contrattoId) {
    this.contrattoId = contrattoId;
  }

  public String getContrattoCodContrattoCliente() {
    return contrattoCodContrattoCliente;
  }

  public void setContrattoCodContrattoCliente(String contrattoCodContrattoCliente) {
    this.contrattoCodContrattoCliente = contrattoCodContrattoCliente;
  }

  public Set<DispositivoDTO> getDispositivos() {
    return dispositivos;
  }

  public void setDispositivos(Set<DispositivoDTO> dispositivos) {
    this.dispositivos = dispositivos;
  }

  public Long getOrdineClienteId() {
    return ordineClienteId;
  }

  public void setOrdineClienteId(Long ordineClienteId) {
    this.ordineClienteId = ordineClienteId;
  }

  public Long getTipoDispositivoId() {
    return tipoDispositivoId;
  }

  public void setTipoDispositivoId(Long tipoDispositivoId) {
    this.tipoDispositivoId = tipoDispositivoId;
  }

  public String getTipoDispositivoNome() {
    return tipoDispositivoNome;
  }

  public void setTipoDispositivoNome(String tipoDispositivoNome) {
    this.tipoDispositivoNome = tipoDispositivoNome;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RichiestaDTO richiestaDTO = (RichiestaDTO) o;
    if (richiestaDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), richiestaDTO.getId());
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "RichiestaDTO [id=" + id + ", identificativo=" + identificativo + ", data=" + data + ", tipo=" + tipo + ", stato=" + stato
           + ", ultimaNota=" + ultimaNota + ", uuidDocumento=" + uuidDocumento + ", anomalia=" + anomalia + ", motivoSospensione="
           + motivoSospensione + ", associazione=" + associazione + ", operazioniPossibili=" + operazioniPossibili + ", dataModificaStato="
           + dataModificaStato + ", nomeServizio=" + nomeServizio + ", contrattoId=" + contrattoId + ", contrattoCodContrattoCliente="
           + contrattoCodContrattoCliente + ", dispositivos=" + dispositivos + ", ordineClienteId=" + ordineClienteId
           + ", tipoDispositivoId=" + tipoDispositivoId + ", tipoDispositivoNome=" + tipoDispositivoNome + ", country=" + country + "]";
  }

}
