package it.fai.ms.efservice.service.frejus;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoService;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.web.rest.DmlBulkResource;

@Service
public class ContrattiFrejusService {
	
	private final Logger log = LoggerFactory.getLogger(ContrattiFrejusService.class);

	private ContrattoServiceExt contrattoServiceExt;
	private TipoDispositivoServiceExt tipoDispositivoService;
	private ClienteFaiServiceExt clienteFaiService;

	public ContrattiFrejusService(ContrattoServiceExt contrattoServiceExt,TipoDispositivoServiceExt tipoDispositivoService,ClienteFaiServiceExt clienteFaiService) {
		this.contrattoServiceExt = contrattoServiceExt;
		this.tipoDispositivoService = tipoDispositivoService;
		this.clienteFaiService = clienteFaiService;
	}
	
	public Optional<String> saveNazioneFatturazione(String codiceAzienda, TipoDispositivoEnum tipoDispositivo,
			String nazioneFatturazione) {

		TipoDispositivo dispositivo = tipoDispositivoService.findOneByNome(tipoDispositivo);
		Produttore produttore = dispositivo.getProduttore();
		Optional<Contratto> contratto = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(produttore,codiceAzienda);
		if(contratto.isPresent())
			log.error("contratto giá presente");
		Optional<Contratto> contr = createContract(codiceAzienda, produttore,  nazioneFatturazione);
		return contr.flatMap(c->Optional.ofNullable(c.getPaeseRiferimentoIva()));
	}

	public Optional<String> changeNazioneFatturazione(String codiceAzienda, TipoDispositivoEnum tipoDispositivo, String nazioneFatturazione) {
		TipoDispositivo dispositivo = tipoDispositivoService.findOneByNome(tipoDispositivo);
		Produttore produttore = dispositivo.getProduttore();
		Optional<Contratto> contratto = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(produttore,codiceAzienda);
		Optional<Contratto> contr = createContractOrSavePaese(codiceAzienda, produttore, contratto, nazioneFatturazione);
		return contr.flatMap(c->Optional.ofNullable(c.getPaeseRiferimentoIva()));
	}

	public Optional<String> getNazioneFatturazione(String codiceAzienda, TipoDispositivoEnum tipoDispositivo) {
		
		TipoDispositivo dispositivo = tipoDispositivoService.findOneByNome(tipoDispositivo);
		Produttore produttore = dispositivo.getProduttore();
		Optional<Contratto> contratto = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(produttore,codiceAzienda);
		Optional<String> paese = contratto.flatMap(c->Optional.ofNullable(c.getPaeseRiferimentoIva()));
		
		if(!paese.isPresent()){
			ClienteFai clienteFai = clienteFaiService.findByCodiceCliente(codiceAzienda);
			paese = Optional.ofNullable(clienteFai.getPaese());
		}
		if(paese.isPresent()&&( "IT".equals(paese.get())||"FR".equals(paese.get()))) {
			createContractOrSavePaese(codiceAzienda, produttore, contratto, paese.get());
			return paese;
		}else
			return Optional.empty();
		
	}

	
	private Optional<Contratto> createContract(String codiceAzienda, Produttore produttore,String paese) {
		return createContractOrSavePaese(codiceAzienda, produttore, Optional.empty(), paese);
	}
	private Optional<Contratto> createContractOrSavePaese(String codiceAzienda, Produttore produttore, Optional<Contratto> contratto,
			String paese) {
		if(!contratto.isPresent()) {
			ClienteFai clienteFai = clienteFaiService.findByCodiceCliente(codiceAzienda);
			contratto =  Optional.of(contrattoServiceExt.createContrattoByProduttoreAndClienteFai(produttore, clienteFai));
		}
		String p=paese;
		contratto.ifPresent(c->{
			c.setPaeseRiferimentoIva(p);
			contrattoServiceExt.save(c);
		});
		return contratto;
	}

}
