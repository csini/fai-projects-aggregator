package it.fai.ms.efservice.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ServiziGestibiliDaRemoto entity.
 */
public class ServiziGestibiliDaRemotoDTO implements Serializable {

    private Long id;

    private Long tipoDispositivoId;

    private String tipoDispositivoNome;

    private Long tipoServizioId;

    private String tipoServizioNome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipoDispositivoId() {
        return tipoDispositivoId;
    }

    public void setTipoDispositivoId(Long tipoDispositivoId) {
        this.tipoDispositivoId = tipoDispositivoId;
    }

    public String getTipoDispositivoNome() {
        return tipoDispositivoNome;
    }

    public void setTipoDispositivoNome(String tipoDispositivoNome) {
        this.tipoDispositivoNome = tipoDispositivoNome;
    }

    public Long getTipoServizioId() {
        return tipoServizioId;
    }

    public void setTipoServizioId(Long tipoServizioId) {
        this.tipoServizioId = tipoServizioId;
    }

    public String getTipoServizioNome() {
        return tipoServizioNome;
    }

    public void setTipoServizioNome(String tipoServizioNome) {
        this.tipoServizioNome = tipoServizioNome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ServiziGestibiliDaRemotoDTO serviziGestibiliDaRemotoDTO = (ServiziGestibiliDaRemotoDTO) o;
        if(serviziGestibiliDaRemotoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), serviziGestibiliDaRemotoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ServiziGestibiliDaRemotoDTO{" +
            "id=" + getId() +
            "}";
    }
}
