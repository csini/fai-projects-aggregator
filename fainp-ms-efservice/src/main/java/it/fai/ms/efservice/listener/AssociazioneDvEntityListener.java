package it.fai.ms.efservice.listener;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.jms.dml.AssociazioneDvDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.DispositivoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;
import it.fai.ms.efservice.util.BeanUtil;

import java.time.temporal.ChronoUnit;

public class AssociazioneDvEntityListener {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  // no-injection please
  AssociazioneDvDmlSenderUtil dmlService;
  DispositivoDmlSenderUtil    dmlDeviceService;


  @PostLoad
  public void postLoad(AssociazioneDV assDv) {
    log.trace("Called PostLoad!");
    saveCurentAssociazioneDVStatus(assDv);
  }

  @PrePersist
  public void prePersist(AssociazioneDV assDv) {
    log.trace("Called PostLoad!");
  }

  @PreUpdate
  public void preUpdate(AssociazioneDV assDv) {
    log.trace("Called PostLoad!");
  }

  @PreRemove
  public void preRemove(AssociazioneDV assDv){
    log.trace("Called PostLoad!");
  }

  @PostPersist
  public void postPersist(AssociazioneDV assDv) {
    log.trace("Called PostLoad!");
    sendSaveNotificationDML(assDv);
  }

  @PostUpdate
  public void postUpdate(AssociazioneDV assDv) {
    log.trace("Called PostLoad!");
    sendSaveNotificationDML(assDv);
  }

  @PostRemove
  public void postRemove(AssociazioneDV assDv){
    log.trace("Called PostLoad!");
    sendDeleteNotificationDML(assDv);
  }

  private void saveCurentAssociazioneDVStatus(AssociazioneDV assDv) {
    String oldUuidVeicolo = assDv.getUuidVeicolo();
    log.trace("Assigning current vehicle uuid [{}] to oldUuidVeicolo for further uses", oldUuidVeicolo);
    assDv.setOldUuidVeicolo(oldUuidVeicolo);
  }

  private void sendSaveNotificationDML(AssociazioneDV assDv) {
    if (dmlService == null) {
      dmlService = new AssociazioneDvDmlSenderUtil(BeanUtil.getBean(JmsProperties.class));
    }
    log.info("Send persist/update AssociazioneDv: {}", assDv);

    dmlService.sendSaveNotification(assDv);

    sendDeviceNotification(assDv);

    log.info("Assign just saved vehicle uuid to oldUuidVeicolo for further uses");
    postLoad(assDv);
  }

  private void sendDeviceNotification(AssociazioneDV assDv) {
    if (dmlDeviceService == null) {
      dmlDeviceService = new DispositivoDmlSenderUtil(BeanUtil.getBean(JmsProperties.class), BeanUtil.getBean(DispositivoJmsMapper.class));
    }

    Dispositivo dispositivo = assDv.getDispositivo();
    log.info("Send Device {} after update associazione DV", dispositivo != null ? dispositivo.getIdentificativo() : null);
    if (dispositivo != null) {
      if (StringUtils.isNotBlank(dispositivo.getIdentificativo())) {
        dispositivo.addAssociazioneDispositivoVeicolo(assDv);
        dmlDeviceService.sendSaveNotification(dispositivo);
      } else {
        log.warn("Not send message because identifier dispositivo is not set");
      }
    } else {
      log.warn("Not send message because dispositivo related on associazioneDV is null");
    }
  }

  private void sendDeleteNotificationDML(AssociazioneDV assDv) {
    if (dmlService == null) {
      dmlService = new AssociazioneDvDmlSenderUtil(BeanUtil.getBean(JmsProperties.class));
    }
    log.info("Send delete AssociazioneDv: {}", assDv);
    dmlService.sendDeleteNotification(String.valueOf(assDv.getId()));
  }

}
