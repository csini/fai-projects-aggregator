package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceProducerService {

  void deviceOrdering(Richiesta richiesta);

  void activateDevice(Richiesta orderRequestEntity);

  void deactivateDevice(Richiesta orderRequestEntity);

  void updateDeviceServiceStatus(Richiesta richiesta);

}
