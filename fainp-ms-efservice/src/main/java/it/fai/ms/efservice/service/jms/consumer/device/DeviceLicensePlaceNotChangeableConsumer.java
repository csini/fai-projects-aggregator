package it.fai.ms.efservice.service.jms.consumer.device;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceLicensePlateNotChangeableMessage;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.jms.JmsDeviceErrorService;

@Service
@Transactional
public class DeviceLicensePlaceNotChangeableConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsDeviceErrorService jmsDeviceErrorService;

  private final DeviceResponseKOConsumer deviceResponseKoConsumer;

  private final RichiestaRepositoryExt repo;

  public DeviceLicensePlaceNotChangeableConsumer(final DeviceResponseKOConsumer _deviceResponseKoConsumer,
                                                 final JmsDeviceErrorService _jmsDeviceErrorService, final RichiestaRepositoryExt _repo) {
    deviceResponseKoConsumer = _deviceResponseKoConsumer;
    jmsDeviceErrorService = _jmsDeviceErrorService;
    repo = _repo;
  }

  public void consume(DeviceLicensePlateNotChangeableMessage message) {
    log.info("Manage message: {}", message);

    /*
     * DeviceLicensePlateNotChangeableMessage [ contractOrderRequest=ContractOrderRequest [numeroContratto= ,
     * codiceCliente=null], orderRequestUuid=null, obuNumber=ObuNumber [code=00049000000813800844], obuId=ObuId
     * [code=0813800844], obuType=ObuType [id=AA],
     * deviceInfo=it.fai.ms.common.jms.efservice.message.device.ObuDeviceInfo@3b549447, services=[],
     * errorDetails=ErrorDetails [code=XXmessage=GENERIC_ERROR], licensePlate=LicensePlate
     * [licenseId=FV707TA,countryId=IT]]
     */

    final String contractNumber = message.getContractOrderRequest()
                                         .getNumeroContratto();
    String seriale = message.getObuNumber()
                            .getCode();
    TipoDispositivoEnum[] tipiDispositivo = { TipoDispositivoEnum.TELEPASS_EUROPEO_SAT };
    Set<TipoRichiesta> tipiRichiesta = new HashSet<>(Arrays.asList(TipoRichiesta.VARIAZIONE_TARGA, TipoRichiesta.MEZZO_RITARGATO));
    // vehicle
    String plate = message.getLicensePlate()
                          .getLicenseId();
    String plateCountry = message.getLicensePlate()
                                 .getCountryId();

    List<Richiesta> list = repo.findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(contractNumber, seriale,
                                                                                         StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA,
                                                                                         tipiRichiesta);
    Richiesta richiesta = null;

    String newTargaNazione = plate + "§" + plateCountry;

    for (Richiesta optRichiesta : list) {
      if (plate.equals(optRichiesta.getAssociazione()) && plateCountry.equals(optRichiesta.getCountry())) {
        richiesta = optRichiesta;
        break;
      } else if (optRichiesta.getNewTargaNazione()
                             .equals(newTargaNazione)) {
        richiesta = optRichiesta;
        break;
      }
    }

    if (richiesta == null) {
      log.warn("Nessuna Richiesta trovata per {}", message);
      return;
    }

    String identificativoRichiesta = richiesta.getIdentificativo();

    String errorMsg = jmsDeviceErrorService.buildErrorMessage(message);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

  public void consume(DeviceEventMessage eventMessage) {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();
    String errorMsg = jmsDeviceErrorService.buildErrorMessage(eventMessage);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

}
