package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.ServiceUuid;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceNotActivableMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceNotDeactivableMessage;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.ServiziManageService;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class ServiziManageServiceImpl implements ServiziManageService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils     fsmRichiestaUtils;
  private final DispositivoServiceExt dispositivoServiceExt;

  public ServiziManageServiceImpl(final FsmRichiestaUtils _fsmRichiestaUtils, final DispositivoServiceExt _dispositivoServiceExt) {
    fsmRichiestaUtils = _fsmRichiestaUtils;
    dispositivoServiceExt = _dispositivoServiceExt;
  }

  @Override
  public void notActivableService(DeviceServiceNotActivableMessage message) throws FsmExecuteCommandException {
    log.info("Manage NOT ACTIVABLE service by message {}", message);

    String contractCode = message.getContractUuid()
                                 .getUuid();
    String seriale = message.getObuNumber()
                            .getCode();
    Set<DeviceService> services = message.getServices();
    logMessage(contractCode, seriale, services);
    changeStatusRichiesta(contractCode, seriale, services, TipoRichiesta.ATTIVAZIONE_SERVIZIO);
  }

  @Override
  public void notDeActivableService(DeviceServiceNotDeactivableMessage message) throws FsmExecuteCommandException {
    log.info("Manage NOT DE ACTIVABLE service by message {}", message);

    String contractCode = message.getContractUuid()
                                 .getUuid();
    String seriale = message.getObuNumber()
                            .getCode();
    Set<DeviceService> services = message.getServices();
    logMessage(contractCode, seriale, services);
    changeStatusRichiesta(contractCode, seriale, services, TipoRichiesta.DISATTIVAZIONE_SERVIZIO);
  }

  private void logMessage(String contractCode, String serialNumber, Set<DeviceService> services) {
    Set<ServiceUuid> servicesUuid = services.stream()
                                            .map(ds -> ds.getServiceUuid())
                                            .collect(toSet());

    log.info("Message: [ContractCode: {} - SerialeNumber Device: {} - Services: {}]", contractCode, serialNumber, servicesUuid);
  }

  // FIXME manage string code error from message
  private void changeStatusRichiesta(String contractCode, String seriale, Set<DeviceService> services, TipoRichiesta tipoRichiesta) throws FsmExecuteCommandException {
    Optional<Dispositivo> dispositivoOpt = dispositivoServiceExt.findByCodiceContrattoAndSeriale(contractCode, seriale);
    if (!dispositivoOpt.isPresent()) {
      throw new RuntimeException("Not found dispositivo with Contratto: " + contractCode + " and Seriale: " + seriale);
    }

    boolean isFirst = true;
    String str = "SERVICE ";
    switch (tipoRichiesta) {
    case ATTIVAZIONE_SERVIZIO:
      str += "NOT ACTIVATED: ";
      break;
    case DISATTIVAZIONE_SERVIZIO:
      str += "NOT DE-ACTIVATED: ";
      break;

    default:
      log.warn("TipoRichiesta {} not managed to NOT ACTIVATED OR NOT DEACTIVATED service....");
      break;
    }
    StringBuilder sb = new StringBuilder(str);
    for (DeviceService deviceService : services) {
      ServiceUuid serviceUuid = deviceService.getServiceUuid();
      if (!isFirst) {
        sb.append(", ");
      }
      sb.append(serviceUuid);
      isFirst = false;
    }

    final String serviceToString = sb.toString();
    Dispositivo dispositivo = dispositivoOpt.get();
    log.info("Found dispositivo {}", dispositivo);

    Set<Richiesta> richiestas = dispositivo.getRichiestas();
    
    Set<String> richiesteManaged = new HashSet<>();
    
    for (Richiesta r : richiestas) {
      if(r != null && r.getTipo() == tipoRichiesta) {
        r.setUltimaNota(serviceToString);
        log.info("Find Richiesta {} for type: {}", r, tipoRichiesta);
        Richiesta richiesta = fsmRichiestaUtils.changeStatusToRichiesta(r,
                                                                        RichiestaEvent.RESPONSE_KO.name(),
                                                                        null, null);
        richiesteManaged.add(richiesta.getIdentificativo());
      }
    }

    log.info("Richieste managed: {}", richiesteManaged);
  }

}
