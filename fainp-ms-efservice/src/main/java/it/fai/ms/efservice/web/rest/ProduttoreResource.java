package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.ProduttoreService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.ProduttoreDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static java.util.stream.Collectors.toList;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Produttore.
 */
@RestController
@RequestMapping("/api")
public class ProduttoreResource {

  private final Logger log = LoggerFactory.getLogger(ProduttoreResource.class);

  private static final String ENTITY_NAME = "produttore";

  private final ProduttoreService produttoreService;

  public ProduttoreResource(ProduttoreService produttoreService) {
    this.produttoreService = produttoreService;
  }

  /**
   * POST /produttores : Create a new produttore.
   *
   * @param produttoreDTO
   *          the produttoreDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new produttoreDTO, or with status 400 (Bad
   *         Request) if the produttore has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping("/produttores")
  @Timed
  public ResponseEntity<ProduttoreDTO> createProduttore(@RequestBody ProduttoreDTO produttoreDTO) throws URISyntaxException {
    log.debug("REST request to save Produttore : {}", produttoreDTO);
    if (produttoreDTO.getId() != null) {
      return ResponseEntity.badRequest()
                           .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new produttore cannot already have an ID"))
                           .body(null);
    }
    ProduttoreDTO result = produttoreService.save(produttoreDTO);
    return ResponseEntity.created(new URI("/api/produttores/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                          .toString()))
                         .body(result);
  }

  /**
   * PUT /produttores : Updates an existing produttore.
   *
   * @param produttoreDTO
   *          the produttoreDTO to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated produttoreDTO, or with status 400 (Bad
   *         Request) if the produttoreDTO is not valid, or with status 500 (Internal Server Error) if the produttoreDTO
   *         couldn't be updated
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PutMapping("/produttores")
  @Timed
  public ResponseEntity<ProduttoreDTO> updateProduttore(@RequestBody ProduttoreDTO produttoreDTO) throws URISyntaxException {
    log.debug("REST request to update Produttore : {}", produttoreDTO);
    if (produttoreDTO.getId() == null) {
      return createProduttore(produttoreDTO);
    }
    ProduttoreDTO result = produttoreService.save(produttoreDTO);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, produttoreDTO.getId()
                                                                                               .toString()))
                         .body(result);
  }

  /**
   * GET /produttores : get all the produttores.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of produttores in body
   */
  @GetMapping("/produttores")
  @Timed
  public List<ProduttoreDTO> getAllProduttores() {
    log.debug("REST request to get all Produttores");
    return produttoreService.findAll();
  }

  /**
   * GET /produttores/:id : get the "id" produttore.
   *
   * @param id
   *          the id of the produttoreDTO to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the produttoreDTO, or with status 404 (Not Found)
   */
  @GetMapping("/produttores/{id}")
  @Timed
  public ResponseEntity<ProduttoreDTO> getProduttore(@PathVariable Long id) {
    log.debug("REST request to get Produttore : {}", id);
    ProduttoreDTO produttoreDTO = produttoreService.findOne(id);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(produttoreDTO));
  }

  /**
   * DELETE /produttores/:id : delete the "id" produttore.
   *
   * @param id
   *          the id of the produttoreDTO to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/produttores/{id}")
  @Timed
  public ResponseEntity<Void> deleteProduttore(@PathVariable Long id) {
    log.debug("REST request to delete Produttore : {}", id);
    produttoreService.delete(id);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                         .build();
  }
}
