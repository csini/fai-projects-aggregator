package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CategoriaServizio and its DTO CategoriaServizioDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategoriaServizioMapper extends EntityMapper <CategoriaServizioDTO, CategoriaServizio> {
    
    @Mapping(target = "tipoServizios", ignore = true)
    CategoriaServizio toEntity(CategoriaServizioDTO categoriaServizioDTO); 
    default CategoriaServizio fromId(Long id) {
        if (id == null) {
            return null;
        }
        CategoriaServizio categoriaServizio = new CategoriaServizio();
        categoriaServizio.setId(id);
        return categoriaServizio;
    }
}
