package it.fai.ms.efservice.service.jms.listener.device;

import java.time.Instant;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceVehicleUnjoinMessage;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;

@Service(JmsListenerDeviceVehicleUnjoin.QUALIFIER)
@Transactional
@Deprecated
public class JmsListenerDeviceVehicleUnjoin implements MessageListener {

  public final static String QUALIFIER = "jmsListenerDeviceVehicleUnjoin";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final AssociazioneDVRepository associazioneDVRepository;
  private final DispositivoRepositoryExt dispositivoRepository;

  public JmsListenerDeviceVehicleUnjoin(AssociazioneDVRepository _associazioneDVRepository, DispositivoRepositoryExt _dispositivoRepository) {
    associazioneDVRepository = _associazioneDVRepository;
    dispositivoRepository = _dispositivoRepository;
  }

  @Override
  public void onMessage(Message message) {
	  DeviceVehicleUnjoinMessage dto = null;
    try {
      dto = (DeviceVehicleUnjoinMessage) ((ObjectMessage)message).getObject();

      Dispositivo dispositivo = null;
      if (dto.getDeviceUuid() != null && dto.getDeviceUuid().getUuid() != null) {
        dispositivo = dispositivoRepository.findOneByIdentificativo(dto.getDeviceUuid().getUuid());
      }
      
      if (dispositivo == null) {
        _log.warn("Device not found : {}", dto.getDeviceUuid().getUuid());
        return;
      }
      
      AssociazioneDV associazioneDV = associazioneDVRepository.findFirstByUuidVeicoloAndDispositivo(dto.getVehicleUuid().getUuid(), dispositivo);
      if (associazioneDV != null) {
        //associazioneDVRepository.delete(associazioneDV.getId());
    	  associazioneDV.setUuidVeicolo(null);
    	  associazioneDV.setData(dto.getTimestamp()!=null ? dto.getTimestamp() : Instant.now());
        associazioneDVRepository.save(associazioneDV);
        _log.debug("Unjoin device to vehicle : {}", associazioneDV);
      } else {
        _log.warn("Unjoin device to vehicle exists : {}", associazioneDV);
      }

      if (!dispositivo.getTipoMagazzino().equals(TipoMagazzino.NOTARGA)) {
        dispositivo.setTipoMagazzino(TipoMagazzino.NOTARGA);
        dispositivoRepository.saveAndFlush(dispositivo);
      }      
    } catch (Exception e) {
      _log.error("Error processing "+dto, e);
    }      
  }

}
