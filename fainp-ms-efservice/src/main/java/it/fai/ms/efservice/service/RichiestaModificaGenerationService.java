package it.fai.ms.efservice.service;

import java.util.List;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;

public interface RichiestaModificaGenerationService {

  List<Richiesta> generateRichieste(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException ;

  List<Long> associaTarga(TipoDispositivoEnum tipoDispositivo, String identifier, String newVeicolo);

  List<Long> richiestaPin(TipoDispositivoEnum tipoDispositivo, String codiceCliente, String identifierDevice);
}
