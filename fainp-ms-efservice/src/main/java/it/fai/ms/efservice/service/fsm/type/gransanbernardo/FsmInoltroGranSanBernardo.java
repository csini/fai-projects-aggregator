package it.fai.ms.efservice.service.fsm.type.gransanbernardo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.gransanbernardo.FsmInoltroGranSanBernardoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroGranSanBernardo.FSM_INOLTRO_GRAN_SAN_BERNARDO)
public class FsmInoltroGranSanBernardo extends FsmRichiestaGeneric {
  
  public static final String FSM_INOLTRO_GRAN_SAN_BERNARDO = "fsmInoltroGranSanBernardo";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmInoltroGranSanBernardo(@Qualifier(FsmInoltroGranSanBernardoConfig.INOLTRO_TES_GRAN_SANBERNARDO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_GRAN_SAN_BERNARDO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO };
  }


  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
