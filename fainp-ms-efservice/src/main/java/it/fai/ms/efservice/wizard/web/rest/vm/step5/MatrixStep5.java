package it.fai.ms.efservice.wizard.web.rest.vm.step5;

import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixAbstract;

@JsonInclude(Include.NON_EMPTY)
public class MatrixStep5 extends MatrixAbstract {

  private static final long serialVersionUID = -7926397316313367043L;

  private Set<MatrixStep5Vehicle> content = new TreeSet<>();
  private boolean                 multiService;
  private String                  tipoDispositivo;
  private Boolean                 isToBeSent;

  public MatrixStep5(final String _deviceType) {
    tipoDispositivo = _deviceType;
  }

  public Set<MatrixStep5Vehicle> getContent() {
    return content;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public boolean isMultiService() {
    return multiService;
  }

  public void setMultiService(final boolean _multiService) {
    multiService = _multiService;
  }

  public Boolean isToBeSent() {
    return isToBeSent;
  }

  public void setToBeSent(Boolean isToBeSent) {
    this.isToBeSent = isToBeSent;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName() + " [content=");
    builder.append(content);
    builder.append(",multiService=");
    builder.append(multiService);
    builder.append(",devicetype=");
    builder.append(tipoDispositivo);
    builder.append(",isTobeSent=");
    builder.append(isToBeSent);
    builder.append("]");
    return builder.toString();
  }

}
