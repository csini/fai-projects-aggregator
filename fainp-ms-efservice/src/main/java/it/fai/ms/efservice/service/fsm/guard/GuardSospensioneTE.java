package it.fai.ms.efservice.service.fsm.guard;

import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardSospensioneTE implements Guard<StatoRichiesta, RichiestaEvent> {

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        TipoRichiesta tipoRichiesta = richiesta.getTipo();
        if (tipoRichiesta.name() == TipoRichiesta.SOSPENSIONE.name()) {
          return true;
        }
      }
    }
    
    return false;
  }

}
