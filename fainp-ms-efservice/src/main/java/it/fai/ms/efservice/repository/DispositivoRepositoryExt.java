package it.fai.ms.efservice.repository;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai_;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Contratto_;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Dispositivo_;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo_;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;

/**
 * Spring Data JPA repository for the Dispositivo entity.
 */
@Repository
public interface DispositivoRepositoryExt extends JpaRepository<Dispositivo, Long>, JpaSpecificationExecutor<Dispositivo> {

  /**
   * Return last record
   *
   * @return Dispositivo
   */
  Dispositivo findFirstByOrderByIdDesc();

  /**
   * @param identificativo
   * @return Dispositivo
   */
  Dispositivo findOneByIdentificativo(String identificativo);

  /**
   * @param codiceAzienda
   * @return List<Dispositivo>
   */
  // @Query("select d from ClienteFai a, Contratto c, Dispositivo d "
  // + " where a.id = c.clienteFai.id"
  // + " and c.id = d.contratto.id"
  // + " and a.codiceCliente = :codiceAzienda")

  /*
   * select d.* from cliente_fai a, contratto c, dispositivo d where a.id = c.cliente_fai_id and c.id = d.contratto_id
   * and a.codice_cliente = 'JPBOPDj' and d.tipo_magazzino = 'NOTARGA'
   */
  @Query("select d from ClienteFai a, Contratto c, Dispositivo d " + " where a.id = c.clienteFai.id" + " and c.id = d.contratto.id"
         + " and a.codiceCliente = :codiceAzienda" + " and d.tipoMagazzino = :tipoMagazzino")
  List<Dispositivo> findAllByCodiceAziendaAndTipoMagazzino(@Param("codiceAzienda") String codiceAzienda,
                                                           @Param("tipoMagazzino") TipoMagazzino tipoMagazzino);

  Set<Dispositivo> findByIdentificativo(List<String> identificativoDispositivi);

  /**
   * select distinct dispositivo.* from cliente_fai, contratto, dispositivo where cliente_fai.id =
   * contratto.cliente_fai_id and contratto.id = dispositivo.contratto_id and cliente_fai.codice_cliente = '0046348'
   */
  @Query("select distinct d from ClienteFai a, Contratto c, Dispositivo d " + " where a.id = c.clienteFai.id" + " and c.id = d.contratto.id"
         + " and a.codiceCliente = :codiceAzienda")
  List<Dispositivo> findAllByCodiceAzienda(@Param("codiceAzienda") String codiceAzienda);

  @Query("select distinct d " + " from Dispositivo d" + " join d.contratto c" + " join ClienteFai a on a.id = c.clienteFai.id "
         + " join d.richiestas r" + " where a.codiceCliente = :codiceAzienda " + " and r.stato = 'DA_SPEDIRE_A_CURA_DI_FAI'")
  Page<Dispositivo> findAllByCodiceAzienda(@Param("codiceAzienda") String codiceAzienda, Pageable page);

  @Query("select distinct (case when (count(d.id) > 0)  then true else false end) from Dispositivo d "
         + " JOIN Contratto c ON d.contratto.id = c.id " + " JOIN AssociazioneDV a ON d.id = a.dispositivo.id "
         + " JOIN StatoDispositivoServizio s on d.id = s.dispositivo.id" + " WHERE c.clienteFai.codiceCliente = :codiceAzienda "
         + " AND s.stato = :stato " + " AND a.uuidVeicolo = :identificativoVehicle")
  Boolean hasVehicleActivedDevices(@Param("codiceAzienda") String codiceAzienda,
                                   @Param("identificativoVehicle") String identificativoVehicle, @Param("stato") StatoDS stato);

  @Query("select d from Dispositivo d inner join d.contratto c where c.codContrattoCliente = :codiceContratto")
  Set<Dispositivo> findDispositiviByCodiceContratto(@Param("codiceContratto") String codiceContratto);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.contratto.clienteFai cf INNER JOIN d.tipoDispositivo td WHERE td.nome = :tipoDispositivo AND cf.codiceCliente = :codiceAzienda")
  Set<Dispositivo> findByCodiceClienteAndTipoDispositivo(@Param("codiceAzienda") String codiceCliente,
                                                         @Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.tipoDispositivo td WHERE d.stato = :stato AND td.nome = :tipoDispositivo")
  Set<Dispositivo> findDispositiviByStateAndTipo(@Param("stato") StatoDispositivo statoDispositivo,
                                                 @Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  Optional<Dispositivo> findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo statoDispositivo, TipoDispositivoEnum tipoDispositivo);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.contratto c WHERE d.seriale = :seriale AND c.codContrattoCliente = :codiceContratto")
  Optional<Dispositivo> findByCodiceContrattoAndSeriale(@Param("codiceContratto") String contractCode, @Param("seriale") String seriale);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.contratto c WHERE d.seriale = :seriale AND c.codContrattoCliente = :codiceContratto")
  // @Query("select d from Dispositivo d, Contratto c, AssociazioneDV a " +
  // " where c.id = d.contratto.id " +
  // " and d.id = a.dispositivo.id " +
  // " and d.seriale = :seriale " +
  // " and c.codContrattoCliente = :codiceContratto ")
  List<Dispositivo> findDispositiviByCodiceContrattoAndSeriale(@Param("codiceContratto") String codiceContratto,
                                                               @Param("seriale") String seriale);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.contratto c INNER JOIN d.associazioneDispositivoVeicolos a WHERE a.uuidVeicolo = :uuidVeicolo AND c.codContrattoCliente = :codiceContratto")
  List<Dispositivo> findDispositiviByCodiceContrattoAndVeicolo(@Param("codiceContratto") String codiceContratto,
                                                               @Param("uuidVeicolo") String uuidVeicolo);

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.associazioneDispositivoVeicolos adv INNER JOIN d.tipoDispositivo td WHERE adv.uuidVeicolo = :identificativoVeicolo AND td.nome = :tipoDispositivo")
  Optional<Dispositivo> findByTipoDispositivoAndIdentificativoVeicolo(@Param("identificativoVeicolo") String identificativoVeicolo,
                                                                      @Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo);

  List<Dispositivo> findAllByStatoAndTipoDispositivo(StatoDispositivo statoDispositivo, TipoDispositivo tipoDispositivo);

  List<Dispositivo> findAllByStato(StatoDispositivo statoDispositivo);

  List<Dispositivo> findBySerialeAndTipoDispositivo(String seriale, TipoDispositivo tipoDispositivo);

  Optional<Dispositivo> findFirstByTipoDispositivo_nomeAndSeriale(TipoDispositivoEnum tipoDispositivo, String seriale);

  List<Dispositivo> findAllBySerialeInAndTipoDispositivo_nome(List<String> seriale, TipoDispositivoEnum tipoDispositivo);

  @Modifying
  @Query("update Dispositivo d set d.dataSpedizione = :deliveryDate  where d.identificativo in :identificativiDispositivi")
  void setDeliveryDate(@Param("deliveryDate") Instant deliveryDate,
                       @Param("identificativiDispositivi") List<String> identificativiDispositivi);

  Optional<Dispositivo> findBySerialeAndStatoIn(String dispositivoSeriale, Set<StatoDispositivo> state);

  Set<Dispositivo> findByTipoDispositivo_nome(TipoDispositivoEnum tipoDispositivo);

  Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(String vehicleUuid,
                                                                                            TipoDispositivoEnum tipoDispositivo);

  Page<Dispositivo> findAllByTipoDispositivo_nomeAndStato(List<TipoDispositivoEnum> tipiDispositivo, StatoDispositivo stato, Pageable page);

  /**
   * select distinct contratto.* from dispositivo inner join associazione_dv on
   * dispositivo.id=associazione_dv.dispositivo_id inner join contratto on dispositivo.contratto_id=contratto.id where
   * associazione_dv.uuid_veicolo = '9a4c1ac0-d075-11e7-8fab-cec278b6b50a'
   */
  @Query("select distinct d " + "from " + "Dispositivo d inner join AssociazioneDV ad on d.id=ad.dispositivo.id "
         + "inner join Contratto c on d.contratto.id=c.id " + "where " + "ad.uuidVeicolo = :uuidVeicolo ")
  List<Dispositivo> findAllByVehicleUuid(@Param("uuidVeicolo") String uuidVeicolo);

  Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicolo(String uuid);

  Optional<Dispositivo> findByStatoDispositivoServizios_PanAndTipoDispositivo_nomeAndStatoDispositivoServizios_TipoServizio_nome(String serialeDeviceSostituito,
                                                                                                                                 TipoDispositivoEnum deviceType,
                                                                                                                                 String nomeServizio);

  @Lock(LockModeType.PESSIMISTIC_WRITE)
  @Query("" + "SELECT d FROM Dispositivo d " + "JOIN d.tipoDispositivo t " + "WHERE d.stato = :statoDispositivo "
         + "AND t.nome = :tipoDispositivo " + "AND (d.tipoHardware <> :tipoHardwareNot OR d.tipoHardware IS NULL)")
  List<Dispositivo> findByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(@Param("statoDispositivo") StatoDispositivo statoDispositivo,
                                                                         @Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo,
                                                                         @Param("tipoHardwareNot") String tipoHardwareNot);

  List<Dispositivo> findByStatoIn(List<StatoDispositivo> states);

  List<Dispositivo> findByStatoInAndTipoDispositivo(List<StatoDispositivo> states, TipoDispositivo td);

  @Query("select distinct d " + "from " + "Dispositivo d inner join AssociazioneDV ad on d.id=ad.dispositivo.id "
         + "inner join Contratto c on d.contratto.id=c.id " + "where " + "ad.uuidVeicolo = :uuidVeicolo " + "and d.stato in :stato")
  Collection<Dispositivo> findByVehicleUuidAndStato(@Param("uuidVeicolo") String uuidVeicolo, @Param("stato") Set<StatoDispositivo> stato);

  Set<Dispositivo> findByRichiestasIsNullAndStatoIs(StatoDispositivo statoDispositivo);

  Set<Dispositivo> findByRichiestasIsNullAndStatoIsAndContratto_ClienteFai_codiceCliente(StatoDispositivo inPreparazioneSpedizione,
                                                                                         String codiceCliente);

  // @EntityGraph(value = "statoDispositivoServizios", type = EntityGraph.EntityGraphType.FETCH)
  @Override
  @EntityGraph(attributePaths = { "contratto", "contratto.clienteFai", "contratto.produttore" })
  // @Query("SELECT d FROM Dispositivo d")
  Page<Dispositivo> findAll(Pageable pageable);

  @Query("select d from Dispositivo d where d.dataScadenza is not null and d.dataScadenza < :actualDate and d.stato in (:activeStates)")
  List<Dispositivo> findActiveDeviceExpired(@Param("actualDate") ZonedDateTime now,
                                            @Param("activeStates") Set<StatoDispositivo> activeStates);

  public static Specification<Dispositivo> contrattoOfAzienda(String codiceAzienda) {
    return (root, query, cb) -> cb.equal(root.get(Dispositivo_.contratto)
                                             .get(Contratto_.clienteFai)
                                             .get(ClienteFai_.codiceCliente),
                                         codiceAzienda);
  }

  @Query("SELECT d FROM Dispositivo d INNER JOIN d.contratto.clienteFai cf INNER JOIN d.tipoDispositivo td WHERE td.nome = :tipoDispositivo AND cf.codiceCliente = :codiceAzienda AND d.stato NOT IN (:stati)")
  Set<Dispositivo> findByCodiceClienteAndTipoDispositivoAndStateNotIn(@Param("codiceAzienda") String codiceCliente,
                                                         @Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo,
                                                         @Param("stati") Set<StatoDispositivo> states);
  
  @Query("SELECT d FROM Dispositivo d WHERE d.stato IN (:stati)")
  Page<Dispositivo> findByStateIn(@Param("stati") Collection<StatoDispositivo> states,Pageable pageable);
  
  List<Dispositivo> findByStatoInAndTipoDispositivo_nome(List<StatoDispositivo> states, TipoDispositivoEnum tipoDispositivo);

  public static Specification<Dispositivo> byContrattoAndTipoDispositivoAndStateNotIn(Contratto contratto,
                                                                                      TipoDispositivoEnum tipoDispositivo,
                                                                                      Set<StatoDispositivo> states) {
    return (root, query, cb) -> cb
      .and(cb.equal(root.get(Dispositivo_.contratto), contratto),
           cb.equal(root.get(Dispositivo_.tipoDispositivo).get(TipoDispositivo_.nome), tipoDispositivo),
           cb.not(root.get(Dispositivo_.stato).in(states)));
  }
  
  List<Dispositivo> findByStatoInAndTipoDispositivoAndDataModificaStatoBetween(List<StatoDispositivo> states, TipoDispositivo td,Instant startDate, Instant endDate);

}
