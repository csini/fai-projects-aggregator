package it.fai.ms.efservice.service.fsm.guard;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.listener.events.DispositivoDepositoOutEvent;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardLiberTInDepositoOk implements Guard<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(GuardLiberTInDepositoOk.class);

  private final ManageDevicesInStorageService deviceInStorageService;

  private final ApplicationEventPublisher applicationEventPublisher;

  public GuardLiberTInDepositoOk(
    final ManageDevicesInStorageService _deviceInStorageService,
    final ApplicationEventPublisher applicationEventPublisher
  ) {
    deviceInStorageService = _deviceInStorageService;
    this.applicationEventPublisher = applicationEventPublisher;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    boolean success = false;
    Optional<Dispositivo> deviceLockOpt = deviceInStorageService.findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo.IN_DEPOSITO,
                                                                                                  TipoDispositivoEnum.LIBER_T);

    if (deviceLockOpt.isPresent()) {
      Dispositivo deviceLock = deviceLockOpt.get();
      log.info("FIND DISPOSITIVO IN DEPOSITO: {}", deviceLock);
      Dispositivo deviceInDeposito = deviceLock;

      String serialeDispositivoInDeposito = deviceInDeposito.getSeriale();
      Message<RichiestaEvent> message = context.getMessage();
      if (message != null) {
        MessageHeaders headers = message.getHeaders();
        Object object = headers.get("object");
        if (object instanceof Richiesta) {
          Richiesta richiesta = (Richiesta) object;
          try {
            String identificativoRichiesta = richiesta.getIdentificativo();
            log.info("Identificativo Richiesta: {}", identificativoRichiesta);
            Dispositivo deviceRichiesta = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst()
                                               .get();
            log.debug("Dispositivo Richiesta: {}", deviceRichiesta);
            String identificativoDevice = deviceRichiesta.getIdentificativo();

            log.debug("Move StatoDispositivoServizio");
            deviceInStorageService.removeStatoDispositivoServizios(deviceRichiesta);
            deviceInStorageService.moveStatoDispositivoServizios(deviceInDeposito,deviceRichiesta);
            deviceRichiesta.getStatoDispositivoServizios().forEach(sds->
              sds.setStato(StatoDS.ATTIVO)
            );
            log.debug("Remove AssociazioneDispositivoVeicolo");
            deviceInStorageService.removeAssociazioneDvs(deviceInDeposito);
            log.debug("Salvo il dispositivo che era in deposito......");
            deviceInDeposito = deviceInStorageService.save(deviceInDeposito.contratto(null));
            log.debug("Flush device");
            deviceInStorageService.flushDevice();
            log.info("Saved device [ {} ] to DE-LOCK", deviceInDeposito);

            log.info("Cancello il dispositivo: {}", deviceInDeposito);
            deviceInStorageService.delete(deviceInDeposito);

            log.debug("flush device deleted");
            deviceInStorageService.flushDevice();

            deviceRichiesta = deviceInStorageService.save(deviceRichiesta.seriale(serialeDispositivoInDeposito).stato(StatoDispositivo.IN_DEPOSITO_IN_ATTESA_SPEDIZIONE));
            applicationEventPublisher.publishEvent(new DispositivoDepositoOutEvent(this, deviceRichiesta.getTipoDispositivo()));
            log.info("Saved SERIALE {} on dispositivo {} related on Richiesta {}", serialeDispositivoInDeposito, identificativoDevice,
                     identificativoRichiesta);
          } catch (Exception e) {
            log.error("Exception on manage device in depositivo for richiesta LiberT: ", e);
            throw new RuntimeException(e);
          }
        } else {
          throw new RuntimeException("Richiesta can not be null");
        }
      } else {
        throw new RuntimeException("Message can not be null");
      }

      success = true;
    }

    return success;
  }
}
