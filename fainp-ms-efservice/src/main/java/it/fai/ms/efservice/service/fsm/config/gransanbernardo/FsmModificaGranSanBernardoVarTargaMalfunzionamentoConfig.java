package it.fai.ms.efservice.service.fsm.config.gransanbernardo;

import java.util.Arrays;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionCreateNewRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardTipoRichiesta;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig.MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig extends FsmRichiestaConfig {

  public static final String MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO = "modTesGranSanbernardoVarTargaMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig(final FsmSenderToQueue _senderFsmService) {
    super();
    this.senderFsmService = _senderFsmService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaGranSanBernardoVarTargaMalfunzionamento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.VARIAZIONE_TARGA)
               .guard(new GuardTipoRichiesta(TipoRichiesta.VARIAZIONE_TARGA))
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.MEZZO_RITARGATO)
               .guard(new GuardTipoRichiesta(TipoRichiesta.MEZZO_RITARGATO))
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.MALFUNZIONAMENTO)
               .guard(new GuardTipoRichiesta(Arrays.asList(TipoRichiesta.MALFUNZIONAMENTO,
                                                           TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE)))
               .action(new FsmActionCreateNewRichiesta(senderFsmService.getSenderJmsService()))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.VARIAZIONE_TARGA)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MEZZO_RITARGATO)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MALFUNZIONAMENTO)
               .target(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISPOSITIVO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionRientratoDispositivo(senderFsmService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO) //FIXME-fsm quando arriva qui?
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO) //FIXME-fsm non raggiungibile?
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO) //FIXME-fsm non raggiungibile?
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmService));
  }

}
