package it.fai.ms.efservice.service.fsm.type.viatoll;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.viatoll.FsmInoltroViaTollConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroViaToll.FSM_INOLTRO_VIATOLL)
public class FsmInoltroViaToll extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_VIATOLL = "fsmInoltroViaToll";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroViaToll(@Qualifier(FsmInoltroViaTollConfig.INOLTRO_VIATOLL) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                           FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_VIATOLL;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.VIA_TOLL };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
