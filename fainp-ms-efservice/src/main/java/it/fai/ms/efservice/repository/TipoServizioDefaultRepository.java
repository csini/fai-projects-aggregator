package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.TipoServizioDefault;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TipoServizioDefault entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoServizioDefaultRepository extends JpaRepository<TipoServizioDefault, Long> {

}
