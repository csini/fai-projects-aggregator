package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;

public class CreatedFsmChangeStatusAsyncDTO implements Serializable {

  private static final long serialVersionUID = -7716788886123308155L;

  private StateChangeStatusFsm state;

  public CreatedFsmChangeStatusAsyncDTO(StateChangeStatusFsm state) {
    this.state = state;
  }

  public CreatedFsmChangeStatusAsyncDTO state(StateChangeStatusFsm state) {
    this.state = state;
    return this;
  }

  public StateChangeStatusFsm getState() {
    return state;
  }

  public void setState(StateChangeStatusFsm state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "CreatedFsmChangeStatusAsyncDTO [state=" + state + "]";
  }

}
