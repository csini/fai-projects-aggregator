package it.fai.ms.efservice.service.fsm.exception;

public class FsmExecuteCommandException extends Exception {

  private static final long serialVersionUID = 3917433148209276818L;
  
  public FsmExecuteCommandException() {
    this(FsmExecuteCommandException.class.toString());
  }

  public FsmExecuteCommandException(String msgId, Throwable cause) {
    super(msgId, cause);
  }

  public FsmExecuteCommandException(Throwable cause) {
    super(cause);
  }

  public FsmExecuteCommandException(String message) {
    super(message);
  }

}