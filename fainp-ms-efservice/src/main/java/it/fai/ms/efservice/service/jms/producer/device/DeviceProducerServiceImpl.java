package it.fai.ms.efservice.service.jms.producer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Richiesta;

@Service
@Transactional
public class DeviceProducerServiceImpl implements DeviceProducerService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceOrderingMessageProducerService deviceOrderingMessageProducer;

  private final DeviceServiceActivationMessageProducerService activationProducerService;

  public DeviceProducerServiceImpl(final DeviceOrderingMessageProducerService _deviceOrderingMessageProducer,
                                   final DeviceServiceActivationMessageProducerService _activationProducerService) {
    deviceOrderingMessageProducer = _deviceOrderingMessageProducer;
    activationProducerService = _activationProducerService;
  }

  @Override
  public void deviceOrdering(Richiesta richiesta) {
    log.info("device ordering for OrderRequestDecorator {}", richiesta);
    deviceOrderingMessageProducer.deviceOrdering(richiesta);
  }

  @Override
  public void activateDevice(Richiesta richiesta) {
    log.info("device activate for OrderRequestDecorator {}", richiesta);
    activationProducerService.activateDevice(richiesta);
  }

  @Override
  public void deactivateDevice(Richiesta richiesta) {
    log.info("device deactivate for OrderRequestDecorator {}", richiesta);
    activationProducerService.deactivateDevice(richiesta);
  }

  @Override
  public void updateDeviceServiceStatus(Richiesta richiesta) {
    log.info("update device services status for OrderRequestDecorator {}", richiesta);
    activationProducerService.updateDeviceServiceStatus(richiesta);
  }

}
