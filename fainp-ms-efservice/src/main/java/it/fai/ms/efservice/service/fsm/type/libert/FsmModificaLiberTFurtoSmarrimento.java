package it.fai.ms.efservice.service.fsm.type.libert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.libert.FsmModificaLiberTFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaLiberTFurtoSmarrimento.FSM_MODIFICA_LIBER_T_FURTO_SMARRIMENTO)
public class FsmModificaLiberTFurtoSmarrimento extends FsmRichiestaGeneric {
  
  public static final String FSM_MODIFICA_LIBER_T_FURTO_SMARRIMENTO = "fsmModificaLiberTFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaLiberTFurtoSmarrimento(@Qualifier(FsmModificaLiberTFurtoSmarrimentoConfig.MOD_LIBER_T_FURTO_SMARRIMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    this.fsmType = FsmType.MOD_LIBER_T_FURTO_SMARRIMENTO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.LIBER_T};
  }

   @Override
   protected FsmType getOldFsm() {
     return null;
   }

   @Override
   protected Logger getLogger() {
     return log;
   }
  
}
