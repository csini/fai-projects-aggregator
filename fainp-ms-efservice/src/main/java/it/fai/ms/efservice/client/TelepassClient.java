package it.fai.ms.efservice.client;

import it.fai.ms.common.jms.dto.telepass.TelepassITRequestDTO;
import it.fai.ms.common.jms.dto.telepass.TelepassITResponseDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

@FeignClient(name = "faitelepass")
public interface TelepassClient {

  String BASE_PATH = "/api/telepassit";

  @GetMapping(BASE_PATH+"/device_plate_insert")
  TelepassITResponseDTO devicePlateInsert(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken, @RequestBody TelepassITRequestDTO dto);

}
