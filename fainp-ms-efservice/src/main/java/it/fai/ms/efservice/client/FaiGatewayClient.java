package it.fai.ms.efservice.client;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.efservice.client.dto.UserDTO;

@FeignClient(name = "faigateway")
public interface FaiGatewayClient {

  public static final String BASE_PATH = "/api/users";

  @GetMapping(BASE_PATH + "/{login}")
  @Timed
  public UserDTO getUser(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken, @PathVariable("login") String login);
  
  @GetMapping("/api/aziendas/findByCodice/{codiceAzienda}")
  @Timed
  public AziendaDTO getAziendaByCodice(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken, @PathVariable("codiceAzienda") String codiceAzienda);

}