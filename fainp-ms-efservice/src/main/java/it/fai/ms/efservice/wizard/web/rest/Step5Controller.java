package it.fai.ms.efservice.wizard.web.rest;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5MatrixVehicle;
import it.fai.ms.efservice.wizard.service.Step5Service;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixServiceType;
import it.fai.ms.efservice.wizard.web.rest.vm.step5.MatrixStep5;
import it.fai.ms.efservice.wizard.web.rest.vm.step5.MatrixStep5BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step5.MatrixStep5Vehicle;

@RestController
@RequestMapping("/api/wizard/step5")
public class Step5Controller {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private Step5Service step5Service;

  @Autowired
  public Step5Controller(final Step5Service _step5Service) {
    step5Service = _step5Service;
  }

  @PostMapping("matrix/{serviceId}")
  public ResponseEntity<List<MatrixStep5>> getMatrix(final @PathVariable(name = "serviceId") @NotNull String _serviceId,
                                                     final @RequestBody @NotNull Set<MatrixStep5BodyRequest> _vehicles) {

    _log.info("Creating matrix for serviceId {} and vehicles size {}", _serviceId, _vehicles.size());

    List<MatrixStep5> matrixStep5List = createMatrixStep5(_vehicles);

    _log.info("Matrixs created {}", matrixStep5List);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(matrixStep5List);
  }

  @PostMapping("matrix/all")
  public ResponseEntity<List<MatrixStep5>> getAllMatrix(final @RequestBody @NotNull Set<MatrixStep5BodyRequest> _vehicles) {

    final long time1 = System.currentTimeMillis();

    _log.info("Creating matrix for ALL services and vehicles size {}", _vehicles.size());

    List<MatrixStep5> matrixStep5List = createMatrixStep5(_vehicles);

    final long time2 = System.currentTimeMillis();

    _log.info("Matrix created (total items: {}) [tot time {} ms]", matrixStep5List.size(), time2 - time1);

    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(matrixStep5List);
  }

  private List<MatrixStep5> createMatrixStep5(final Set<MatrixStep5BodyRequest> _vehicles) {
    final AtomicInteger sequence = newSequence();

    final Set<Map<String, Set<String>>> deviceTypesMaps = createDeviceTypesMaps(_vehicles);

    final List<MatrixStep5> matrixStep5List = step5Service.buildMatrixList(deviceTypesMaps)
                                                          .stream()
                                                          .map(wizardStep5Matrix -> convertWizardStep5Matrix(sequence, wizardStep5Matrix))
                                                          .collect(toList());

    return matrixStep5List;
  }

  private Set<Map<String, Set<String>>> createDeviceTypesMaps(Set<MatrixStep5BodyRequest> _vehicles) {
    Set<Map<String, Set<String>>> deviceTypesMaps = _vehicles.stream()
                                                             .sorted()
                                                             .collect(LinkedHashSet::new,
                                                                      (LinkedHashSet<Map<String, Set<String>>> resultSet,
                                                                       MatrixStep5BodyRequest bodyRequestItem) -> {
                                                                        bodyRequestItem.getDeviceTypeIds()
                                                                                       .stream()
                                                                                       .sorted()
                                                                                       .forEach(deviceTypeId -> {
                                                                                         final Map<String, Set<String>> devices = resultSet.stream()
                                                                                                                                           .filter(device -> device.containsKey(deviceTypeId))
                                                                                                                                           .findFirst()
                                                                                                                                           .orElse(new LinkedHashMap<>());
                                                                                         if (!devices.containsKey(deviceTypeId)) {
                                                                                           resultSet.add(devices);
                                                                                         }
                                                                                         final Set<String> vehicleIds = devices.getOrDefault(deviceTypeId,
                                                                                                                                             new LinkedHashSet<>());
                                                                                         vehicleIds.add(bodyRequestItem.getVehicleUUID());
                                                                                         devices.put(deviceTypeId, vehicleIds);
                                                                                       });
                                                                      }, (listA, listB) -> listA.addAll(listB));

    return deviceTypesMaps;
  }

  private MatrixStep5 convertWizardStep5Matrix(final AtomicInteger _sequence, final WizardStep5Matrix _step5Matrix) {
    final MatrixStep5 matrix = new MatrixStep5(_step5Matrix.getDeviceTypeId());
    matrix.setMultiService(_step5Matrix.isMultiService());
    matrix.setToBeSent(_step5Matrix.isToBeSent());
    final List<MatrixStep5Vehicle> matrixStep5Vehicles = _step5Matrix.getVehicles()
                                                                     .stream()
                                                                     .map(step5WizardMatrixVehicle -> mapToMatrixStep5Vehicle(_sequence,
                                                                                                                              step5WizardMatrixVehicle))
                                                                     .collect(toList());

    matrix.getHeader()
          .addAll(extractHeaders(_step5Matrix.getVehicles()));
    matrix.getContent()
          .addAll(matrixStep5Vehicles);
    return matrix;
  }

  private Set<String> extractHeaders(final Set<WizardStep5MatrixVehicle> _step5WizardMatrixVehicles) {
    return _step5WizardMatrixVehicles.stream()
                                     .map(vehicle -> vehicle.getServiceTypes()
                                                            .values()
                                                            .stream()
                                                            .sorted((o1, o2) -> Long.compare(o1.getOrdering(), o2.getOrdering()))
                                                            .map(step5WizardDeviceType -> step5WizardDeviceType.getId())
                                                            .collect(toList()))
                                     .flatMap(Collection::stream)
                                     .distinct()
                                     .collect(toCollection(LinkedHashSet::new));
  }

  private Map<String, MatrixServiceType> mapToMatrixServiceTypes(final AtomicInteger _sequence,
                                                                 final WizardStep5MatrixVehicle _wizardStep5MatrixVehicle) {
    return new TreeMap<>(_wizardStep5MatrixVehicle.getServiceTypes()
                                                  .values()
                                                  .stream()
                                                  .sorted()
                                                  .collect(toMap(wizardStep5MatrixServiceType -> wizardStep5MatrixServiceType.getId(),
                                                                 wizardStep5MatrixServiceType -> {
                                                                   final MatrixServiceType service = new MatrixServiceType(wizardStep5MatrixServiceType.getId());
                                                                   service.setState(wizardStep5MatrixServiceType.getActivability()
                                                                                                                .getActivabilityState()
                                                                                                                .name());
                                                                   service.setItemKey(String.valueOf(_sequence.getAndIncrement()));
                                                                   if (wizardStep5MatrixServiceType.isNotActivable()) {
                                                                     WizardMatrixActivability activability = wizardStep5MatrixServiceType.getActivability();
                                                                     if (activability.getActivabilityState() == ActivabilityState.ANOMALY) {
                                                                       service.setChecked(false);
                                                                     }
                                                                     Optional.ofNullable(wizardStep5MatrixServiceType.getActivability()
                                                                                                                     .getActivabilityFailures())
                                                                             .ifPresent(allFailures -> {
                                                                               allFailures.stream()
                                                                                          .map(f -> new MatrixActivabilityFailure(f.getCode(),
                                                                                                                                  f.getMess()))
                                                                                          .forEach(failure -> service.addFailure(failure));
                                                                             });
                                                                   }
                                                                   return service;
                                                                 })));
  }

  private MatrixStep5Vehicle mapToMatrixStep5Vehicle(final AtomicInteger _sequence,
                                                     final WizardStep5MatrixVehicle _wizardStep5MatrixVehicle) {
    final MatrixStep5Vehicle matrixStep5Vehicle = new MatrixStep5Vehicle(_wizardStep5MatrixVehicle.getUuid());
    matrixStep5Vehicle.setEuro(_wizardStep5MatrixVehicle.getEuroClass());
    matrixStep5Vehicle.setTarga(_wizardStep5MatrixVehicle.getLicensePlate());
    matrixStep5Vehicle.setTipo(_wizardStep5MatrixVehicle.getType());
    matrixStep5Vehicle.setTargaNazione(_wizardStep5MatrixVehicle.getTargaNazione());
    matrixStep5Vehicle.getItems()
                      .putAll(mapToMatrixServiceTypes(_sequence, _wizardStep5MatrixVehicle));
    return matrixStep5Vehicle;
  }

  private AtomicInteger newSequence() {
    return new AtomicInteger(1);
  }

}
