package it.fai.ms.efservice.service.jms.consumer;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.RichiestaNewDispositivoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@Service
@Transactional
public class NuovoDispositivoConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaServiceExt richiestaServiceExt;
  private final FsmRichiestaUtils   fsmRichiestaUtils;
  private final RichiestaRepository richiestaRepo;

  public NuovoDispositivoConsumer(RichiestaServiceExt _richiestaServiceExt, FsmRichiestaUtils _fsmRichiestaUtils,
                                  RichiestaRepository _richiestaRepo) {
    richiestaServiceExt = _richiestaServiceExt;
    fsmRichiestaUtils = _fsmRichiestaUtils;
    richiestaRepo = _richiestaRepo;
  }

  public void createRichiestaDiSostituzione(RichiestaNewDispositivoDTO dto) throws Exception {
    String identificativoRichiesta = dto.getIdentificativoRichiesta();
    if (StringUtils.isBlank(identificativoRichiesta)) {
      log.error("Identificativo richiesta is null...");
      throw new ValidationException("Identificativo richiesta is null");
    }

    Richiesta newOrder = richiestaServiceExt.createRichiestaDiSostituzioneFromIdentificativo(identificativoRichiesta);
    log.info("CREATED new richiesta di sostituzione...");
    List<Richiesta> richieste = new ArrayList<>();
    richieste.add(newOrder);
    richieste = richiestaServiceExt.save(richieste);
    newOrder = richieste.get(0);
    AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(newOrder);
    log.info("FSM retrieve to change status richiesta is {}", fsm);
    if (fsm != null) {
      log.info("Call fsm to richiesta di sostituzione: " + newOrder.getIdentificativo());
      Richiesta richiestaWithStatusChanged = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, newOrder);
      log.info("Fsm changed status for richiesta: " + richiestaWithStatusChanged.getIdentificativo());

      Richiesta richiestaSaved = richiestaRepo.save(richiestaWithStatusChanged);
      log.info("Saved richiesta di sostituzione: " + richiestaSaved.getIdentificativo());
    } else {
      log.warn("Fsm is not available for richiesta {}", identificativoRichiesta);
    }
  }

}
