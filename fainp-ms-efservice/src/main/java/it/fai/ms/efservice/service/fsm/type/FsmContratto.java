package it.fai.ms.efservice.service.fsm.type;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.AbstractFsmContratto;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmContrattoCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

@Service(value = "fsmContratto")
public class FsmContratto extends AbstractFsmContratto {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private StateMachineContrattoBuilder fsmBuilder;

  private FsmContrattoCacheService cacheService;

  /**
   * @param fsmBuilder
   * @param cacheService
   */
  public FsmContratto(StateMachineContrattoBuilder _fsmBuilder, FsmContrattoCacheService _cacheService) {
    this.fsmBuilder = _fsmBuilder;
    this.cacheService = _cacheService;
  }

  @Override
  public Contratto executeCommandToChangeState(ContrattoEvent command, Contratto contratto, String nota) throws Exception {
    Long contractId = contratto.getId();
    log.debug("Switch state by command {} for Contratto [ID: {}]", command, contractId);

    StateMachine<StatoContratto, ContrattoEvent> stateMachine = null;

    String fsmKey = String.valueOf(contractId);
    boolean availableCommand = isAvailableCommand(command, contratto);
    if (availableCommand) {

      log.info("Command {} is available for dispositivo [ID: {}]", command, contractId);
      if (stateMachine == null) {
        stateMachine = getStateMachine();
        stateMachine.start();
      }

      stateMachine = restoreStateMachine(stateMachine, fsmKey);

      if (command != null) {
        sendEventToFsm(stateMachine, command, contratto, nota);
        log.info("Send command: {} to Contratto [ID: {}]", command, contractId);
        cacheService.persist(stateMachine, fsmKey);
        setOperazioniPossibili(contratto);
      } else {
        log.warn("Enum command (ContrattoEvent) is: {}", command);
      }

      log.debug("=> FINISH switch state with command: {} to Contratto [ID: {}]", command, contractId);
    } else {
      log.warn("Command: {} is not available for Contratto [ID: {}] with stato {}", command, contractId, contratto.getStato());
    }

    return contratto;
  }

  private StateMachine<StatoContratto, ContrattoEvent> restoreStateMachine(StateMachine<StatoContratto, ContrattoEvent> stateMachine,
                                                                           String fsmKey) {
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isNotBlank(jsonContext)) {
        stateMachine = cacheService.restore(stateMachine, fsmKey);
      }
    }

    return stateMachine;
  }

  @Override
  protected StateMachine<StatoContratto, ContrattoEvent> getStateMachine() {
    return fsmBuilder.build();
  }

}
