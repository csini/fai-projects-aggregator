package it.fai.ms.efservice.service.jms.consumer;

import static it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent.IN_SPEDIZIONE;
import static it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent.MS_OBUNO;
import static java.time.ZoneOffset.UTC;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.ObuNotificationMessage;
import it.fai.ms.common.jms.efservice.message.device.model.ObuNotificationService;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmInoltroTelepassEuropeo;
import it.fai.ms.efservice.service.impl.OrderRequestService;

@Service
@Transactional
// FIXME da refactorare
public class ObunoNotificationConsumer {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt  deviceRepository;
  private final FsmDispositivo            fsmDevice;
  private final FsmInoltroTelepassEuropeo fsmInoltro;
  @Autowired
  private OrderRequestService             orderRequestService;
  private final RichiestaRepositoryExt    richiestaRepository;

  private final StatoDispositivoServizioRepository sdsRepository;

  public ObunoNotificationConsumer(final RichiestaRepositoryExt _richiestaRepository, final FsmInoltroTelepassEuropeo _fsmInoltro,
                                   final DispositivoRepositoryExt _deviceRepository, final FsmDispositivo _fsmDevice,
                                   final StatoDispositivoServizioRepository _sdsRepository) {
    super();
    richiestaRepository = _richiestaRepository;
    fsmInoltro = _fsmInoltro;
    deviceRepository = _deviceRepository;
    fsmDevice = _fsmDevice;
    sdsRepository = _sdsRepository;
  }

  public void consume(final ObuNotificationMessage _obuNotificationMessage) throws Exception {

    final Richiesta richiesta = orderRequestService.getOrderRequestsByCompanyCodeAndVehiclePlateNumber(_obuNotificationMessage.getContractUuid()
                                                                                                                              .getUuid(),
                                                                                                       _obuNotificationMessage.getPlateNumber(),
                                                                                                       _obuNotificationMessage.getPlateNumberCountry());

    if (richiesta != null) {

      _log.info("Found Richiesta: {} for {}", richiesta, _obuNotificationMessage);
      final Set<Dispositivo> dispositivos = richiesta.getDispositivos();
      if (dispositivos.size() > 1) {
        _log.error("OrderRequestDecorator {} has more than one device [total devices: {}]", richiesta.getIdentificativo(), dispositivos.size());
      }

      final Optional<Dispositivo> optionalDevice = dispositivos.stream()
                                                               .findFirst();
      if (optionalDevice.isPresent()) {

        // state machine for device
        executeStateMachine(optionalDevice.get());
        updateModel(optionalDevice.get());

        // update device
        updateDeviceWithObuno(optionalDevice.get(), _obuNotificationMessage);
        updateDeviceServicesWithObuno(optionalDevice.get(), _obuNotificationMessage.getObunoServices());

        // state machine for orderrequest
        if (_obuNotificationMessage.anyServiceIsActive()) {
          final Optional<Richiesta> optionalOrder = executeStateMachine(richiesta);
          updateModel(optionalOrder);
        } else {
          _log.info("No active services in {} : state machine execution skipped", _obuNotificationMessage);
        }

      } else {
        _log.warn("No devices to process for OrderRequestDecorator {}", richiesta.getIdentificativo());
      }

    } else {
      _log.warn("OrderRequestDecorator not found for contract uuid {} and license plate {}", _obuNotificationMessage.getContractUuid(),
                _obuNotificationMessage.getPlateNumber());
    }
  }

  private String decode(String _serviceCode) {
    // fixme
    String serviceTypeName = null;
    switch (_serviceCode) {
    case "05":
      serviceTypeName = "PEDAGGI_ITALIA";
      break;
    case "07":
      serviceTypeName = "PEDAGGI_FRANCIA";
      break;
    case "09":
      serviceTypeName = "PEDAGGI_SPAGNA";
      break;
    case "11":
      serviceTypeName = "PEDAGGI_PORTOGALLO";
      break;
    case "13":
      serviceTypeName = "PEDAGGI_AUSTRIA";
      break;
    case "14":
      serviceTypeName = "PEDAGGI_POLONIA";
      break;
    case "15":
      serviceTypeName = "PEDAGGI_GERMANIA";
      break;
    case "17":
      serviceTypeName = "PEDAGGI_BELGIO";
      break;
    default:
      _log.warn("Service code {} not mapped", _serviceCode);
      serviceTypeName = "N.A.";
      break;
    }
    return serviceTypeName;
  }

  private Optional<Dispositivo> executeStateMachine(final Dispositivo _device) {
    Dispositivo device = null;
    if (fsmDevice.isAvailableCommand(IN_SPEDIZIONE, _device)) {
      try {
        device = fsmDevice.executeCommandToChangeState(IN_SPEDIZIONE, _device);
      } catch (final Exception _e) {
        _log.warn("FinalStateMachine error {}", _e);
      }
    } else {
      _log.warn("FinalStateMachine Event {} not available for device {}", IN_SPEDIZIONE, _device.getIdentificativo());
    }
    return Optional.ofNullable(device);
  }

  private Optional<Richiesta> executeStateMachine(final Richiesta _orderRequest) {
    Richiesta order = null;
    if (fsmInoltro.isAvailableCommand(MS_OBUNO, _orderRequest)) {
      try {
        order = fsmInoltro.executeCommandToChangeState(MS_OBUNO, _orderRequest);
      } catch (final Exception _e) {
        _log.warn("FinalStateMachine error {}", _e);
      }
    } else {
      _log.warn("FinalStateMachine Event {} not available for device {}", MS_OBUNO, _orderRequest.getIdentificativo());
    }
    return Optional.ofNullable(order);
  }

  private Instant toInstant(final LocalDate _localDate) {
    return _localDate.atStartOfDay()
                     .toInstant(UTC);
  }

  private void updateDeviceServicesWithObuno(final Dispositivo _device, final ObuNotificationService _obunoService) {
    _log.info("Updating services for {} ", _device);

    Set<StatoDispositivoServizio> statoDispositivoServizios = _device.getStatoDispositivoServizios();
    Optional<StatoDispositivoServizio> optionalSds = statoDispositivoServizios.stream()
                                                                              .filter(sds -> sds.getTipoServizio()
                                                                                                .getNome()
                                                                                                .equals(decode(_obunoService.getServiceCode())))
                                                                              .findFirst();

    if (optionalSds.isPresent()) {
      final StatoDispositivoServizio serviceState = optionalSds.get();
      _log.info("Found {} ", serviceState);
      serviceState.setPan(_obunoService.getPan());
      serviceState.setStato(StatoDS.ATTIVO);
      serviceState.setDataAttivazione(Instant.now());
      final Instant expirationDate = toInstant(_obunoService.getExpirationDate());
      serviceState.setDataDisattivazione(expirationDate);
      serviceState.setDataScadenza(expirationDate);
      updateModel(serviceState);
    } else {
      _log.info("StatoDispositivoServizio not found for {} ", _obunoService);
    }

    _log.info("Device updated : {}", _device);
  }

  private void updateDeviceServicesWithObuno(final Dispositivo _device, Set<ObuNotificationService> _obunoServices) {
    _log.info("Updating services for {} ", _device);

    _obunoServices.forEach(obunoService -> updateDeviceServicesWithObuno(_device, obunoService));

    _log.info("Device updated : {}", _device);
  }

  private void updateDeviceWithObuno(final Dispositivo _device, final ObuNotificationMessage _obuNotificationMessage) {
    _log.info("Updating {}", _device);
    _device.setSeriale(_obuNotificationMessage.getObuNumber());
//    _device.setDataPrimaAttivazione(Instant.now());
    _device.setShipmentReference(_obuNotificationMessage.getShipmentReference());
    _device.setTrackingNumber(_obuNotificationMessage.getTrackingNumber());
    _device.setTipoHardware(_obuNotificationMessage.getObuType());
    _log.info("Device updated : {}", _device);
  }

  private void updateModel(final Dispositivo _device) {
    final Dispositivo savedDevice = deviceRepository.save(_device);
    _log.info("Saved {}", savedDevice);
  }

  private void updateModel(final Optional<Richiesta> _orderRequest) {
    _orderRequest.ifPresent(order -> {
      Richiesta savedOrderRequest = richiestaRepository.save(order);
      _log.info("Saved {}", savedOrderRequest);
    });
  }

  private void updateModel(final StatoDispositivoServizio _serviceState) {
    final StatoDispositivoServizio savedSds = sdsRepository.save(_serviceState);
    _log.info("Saved {}", savedSds);
  }

}
