package it.fai.ms.efservice.wizard.repository.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;

public class WizardVehicleEntity implements Serializable {

  private static final long serialVersionUID = -4172252716453217426L;

  private String                   euroClass;
  private String                   licensePlate;
  private String                   state;
  private String                   type;
  private String                   uuid;
  private String                   libretto;
  private String                   country;
  private HashMap<String, Instant> expireDateOwnership;

  public WizardVehicleEntity(final String _uuid) {
    uuid = _uuid;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardVehicleEntity) _obj).getEuroClass(), euroClass)
                 && Objects.equals(((WizardVehicleEntity) _obj).getLicensePlate(), licensePlate)
                 && Objects.equals(((WizardVehicleEntity) _obj).getState(), state)
                 && Objects.equals(((WizardVehicleEntity) _obj).getType(), type)
                 && Objects.equals(((WizardVehicleEntity) _obj).getUuid(), uuid)
                 && Objects.equals(((WizardVehicleEntity) _obj).getLibretto(), libretto);
    }
    return isEquals;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public String getState() {
    return state;
  }

  public String getType() {
    return type;
  }

  public String getUuid() {
    return uuid;
  }

  public String getLibretto() {
    return libretto;
  }

  @Override
  public int hashCode() {
    return Objects.hash(euroClass, licensePlate, state, uuid, type, libretto);
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public void setState(final String _state) {
    state = _state;
  }

  public void setType(final String _type) {
    type = _type;
  }

  public void setLibretto(String _libretto) {
    libretto = _libretto;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public HashMap<String, Instant> getExpireDateOwnership() {
    return expireDateOwnership;
  }

  public void setExpireDateOwnership(HashMap<String, Instant> expireDateOwnership) {
    this.expireDateOwnership = expireDateOwnership;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardVehicleEntity [");
    if (this.uuid != null) {
      builder.append("uuid=");
      builder.append(this.uuid);
    }

    if (this.licensePlate != null) {
      builder.append(", ");
      builder.append("licensePlate=");
      builder.append(this.licensePlate);
    }
    if (this.euroClass != null) {
      builder.append(", ");
      builder.append("euroClass=");
      builder.append(this.euroClass);
    }
    if (this.state != null) {
      builder.append(", ");
      builder.append("state=");
      builder.append(this.state);
    }
    if (this.type != null) {
      builder.append(", ");
      builder.append("type=");
      builder.append(this.type);
    }
    if (this.country != null) {
      builder.append(", ");
      builder.append("country=");
      builder.append(this.country);
    }
    if (this.country != null) {
      builder.append(", ");
      builder.append("expireDateOwnership=");
      builder.append(this.expireDateOwnership);
    }
    builder.append("]");
    return builder.toString();
  }

}
