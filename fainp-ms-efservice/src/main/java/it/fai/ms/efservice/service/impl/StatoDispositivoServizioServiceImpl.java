package it.fai.ms.efservice.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.service.StatoDispositivoServizioService;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import it.fai.ms.efservice.service.mapper.StatoDispositivoServizioMapper;

/**
 * Service Implementation for managing StatoDispositivoServizio.
 */
@Service
@Transactional
public class StatoDispositivoServizioServiceImpl implements StatoDispositivoServizioService {

  private final Logger log = LoggerFactory.getLogger(StatoDispositivoServizioServiceImpl.class);

  private final StatoDispositivoServizioRepository statoDispositivoServizioRepository;

  private final StatoDispositivoServizioMapper statoDispositivoServizioMapper;

  public StatoDispositivoServizioServiceImpl(StatoDispositivoServizioRepository statoDispositivoServizioRepository,
                                             StatoDispositivoServizioMapper statoDispositivoServizioMapper) {
    this.statoDispositivoServizioRepository = statoDispositivoServizioRepository;
    this.statoDispositivoServizioMapper = statoDispositivoServizioMapper;
  }

  /**
   * Save a statoDispositivoServizio.
   *
   * @param statoDispositivoServizioDTO
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public StatoDispositivoServizioDTO save(StatoDispositivoServizioDTO statoDispositivoServizioDTO) {
    log.debug("Request to save StatoDispositivoServizio : {}", statoDispositivoServizioDTO);
    StatoDispositivoServizio statoDispositivoServizio = statoDispositivoServizioMapper.toEntity(statoDispositivoServizioDTO);
    statoDispositivoServizio = statoDispositivoServizioRepository.save(statoDispositivoServizio);
    return statoDispositivoServizioMapper.toDto(statoDispositivoServizio);
  }

  @Override
  public StatoDispositivoServizio save(StatoDispositivoServizio sds) {
    return statoDispositivoServizioRepository.save(sds);
  }

  /**
   * Get all the statoDispositivoServizios.
   *
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public List<StatoDispositivoServizioDTO> findAll() {
    log.debug("Request to get all StatoDispositivoServizios");
    return statoDispositivoServizioRepository.findAll()
                                             .stream()
                                             .map(statoDispositivoServizioMapper::toDto)
                                             .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get one statoDispositivoServizio by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Override
  @Transactional(readOnly = true)
  public StatoDispositivoServizioDTO findOne(Long id) {
    log.debug("Request to get StatoDispositivoServizio : {}", id);
    StatoDispositivoServizio statoDispositivoServizio = statoDispositivoServizioRepository.findOne(id);
    return statoDispositivoServizioMapper.toDto(statoDispositivoServizio);
  }

  /**
   * Delete the statoDispositivoServizio by id.
   *
   * @param id
   *          the id of the entity
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete StatoDispositivoServizio : {}", id);
    statoDispositivoServizioRepository.delete(id);
  }
}
