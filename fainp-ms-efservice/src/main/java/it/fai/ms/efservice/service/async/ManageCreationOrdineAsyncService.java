package it.fai.ms.efservice.service.async;

import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CreatedRichiestasAsyncDTO;

public interface ManageCreationOrdineAsyncService {

  /**
   * @param carrelloDTO
   * @return
   */
  CreatedRichiestasAsyncDTO generateOrdineAsync(CarrelloDTO carrelloDTO);

}
