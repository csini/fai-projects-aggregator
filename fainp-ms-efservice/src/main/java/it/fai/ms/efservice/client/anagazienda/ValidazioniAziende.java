package it.fai.ms.efservice.client.anagazienda;

import java.io.Serializable;
import java.util.List;


public class ValidazioniAziende implements Serializable{
  private static final long serialVersionUID = 1L;

  public ValidazioniAziende(List<ValidazioneCliente> validazioni) {
    this.validazioni = validazioni;
  }

  public ValidazioniAziende() {
  }

  private List<ValidazioneCliente> validazioni = null;



  public List<ValidazioneCliente> getValidazioni() {
    return validazioni;
  }

  public void setValidazioni(List<ValidazioneCliente> validazioni) {
    this.validazioni = validazioni;
  }

  @Override
  public String toString() {
    return "ValidazioniAziende [validazioni=" + validazioni + "]";
  }
}
