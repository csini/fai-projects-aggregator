/**
 * View Models used by Spring MVC REST controllers.
 */
package it.fai.ms.efservice.web.rest.vm;
