package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.OrdineClienteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OrdineCliente and its DTO OrdineClienteDTO.
 */
@Mapper(componentModel = "spring", uses = {IndirizzoSpedizioneOrdiniMapper.class, ClienteFaiMapper.class, })
public interface OrdineClienteMapper extends EntityMapper <OrdineClienteDTO, OrdineCliente> {

    @Mapping(source = "indirizzoDiTransito.id", target = "indirizzoDiTransitoId")

    @Mapping(source = "indirizzoDestinazioneFinale.id", target = "indirizzoDestinazioneFinaleId")

    @Mapping(source = "clienteAssegnatario.id", target = "clienteAssegnatarioId")
    OrdineClienteDTO toDto(OrdineCliente ordineCliente); 
    @Mapping(target = "richiestas", ignore = true)

    @Mapping(source = "indirizzoDiTransitoId", target = "indirizzoDiTransito")

    @Mapping(source = "indirizzoDestinazioneFinaleId", target = "indirizzoDestinazioneFinale")

    @Mapping(source = "clienteAssegnatarioId", target = "clienteAssegnatario")
    OrdineCliente toEntity(OrdineClienteDTO ordineClienteDTO); 
    default OrdineCliente fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrdineCliente ordineCliente = new OrdineCliente();
        ordineCliente.setId(id);
        return ordineCliente;
    }
}
