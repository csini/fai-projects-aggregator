package it.fai.ms.efservice.wizard.repository;

import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardDevice;
import it.fai.ms.efservice.wizard.model.WizardDeviceId;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardService;
import it.fai.ms.efservice.wizard.model.WizardServiceId;
import it.fai.ms.efservice.wizard.model.WizardServiceType;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

@Service
public class WizardDeviceRepositoryImpl implements WizardDeviceRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private AssociazioneDVRepository devicesForVehicleRepository;

  @Autowired
  public WizardDeviceRepositoryImpl(final AssociazioneDVRepository _devicesForVehicleRepository) {
    devicesForVehicleRepository = _devicesForVehicleRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDevice> findByWizardVehicleId(final WizardVehicleId _wizardVehicleId) {
    Set<WizardDevice> wizardDevices = null;
    final long time1 = System.currentTimeMillis();
    final List<AssociazioneDV> byUuidVeicolo = devicesForVehicleRepository.findByUuidVeicolo(_wizardVehicleId.getId());
    if (byUuidVeicolo != null) {
      wizardDevices = byUuidVeicolo.stream()
                                   .filter(associazioneDV -> associazioneDV.getDispositivo()
                                                           .getStato()
                                                           .isGreen()
                                                     || associazioneDV.getDispositivo()
                                                              .getStato()
                                                              .isYellow()) // FILTRO SOLO QUELLI ATTIVI O IN ATTIVAZIONE
                                   .map(device -> mapToWizardDevice(device))
                                   .collect(toSet());
    } else {
      wizardDevices = Collections.emptySet();
    }
    final long time2 = System.currentTimeMillis();
    _log.debug("Devices found for {}: {} [time {} ms]", _wizardVehicleId, wizardDevices, time2 - time1);
    return wizardDevices;

  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDevice> findByWizardVehicleIdFilteredByDevice(final WizardVehicleId _wizardVehicleId,
                                                                 Set<WizardDeviceTypeId> deviceTypeIds) {
    Set<WizardDevice> wizardDevices = null;
    final long time1 = System.currentTimeMillis();
    Set<TipoDispositivoEnum> DevicesTypeName = deviceTypeIds.stream()
                                                            .map(wdt -> TipoDispositivoEnum.valueOf(wdt.getId()))
                                                            .collect(toSet());
    final List<Dispositivo> devicesOnVehicle = devicesForVehicleRepository.findByUuidVeicoloAndTipoDispositivoNomeInAndStatoDispositivoNotIn(_wizardVehicleId.getId(),
                                                                                                                                             DevicesTypeName,
                                                                                                                                             StatoDispositivo.getBlueState());
    if (devicesOnVehicle != null) {
      wizardDevices = devicesOnVehicle.parallelStream()
                                      .map(device -> mapToWizardDevice(device))
                                      .collect(toSet());
    } else {
      wizardDevices = Collections.emptySet();
    }
    final long time2 = System.currentTimeMillis();
    _log.debug("Devices found for {}: {} [time {} ms]", _wizardVehicleId, wizardDevices.size(), time2 - time1);
    return wizardDevices;

  }

  private WizardDevice mapToWizardDevice(final AssociazioneDV assDv) {
    return mapToWizardDevice(assDv.getDispositivo());
  }

  private WizardDevice mapToWizardDevice(final Dispositivo device) {
    final WizardDevice wizardDevice = new WizardDevice(new WizardDeviceId(device.getIdentificativo()));
    wizardDevice.setActivationDate(device.getDataPrimaAttivazione());
    StatoDispositivo deviceStatus = device.getStato();
    if (deviceStatus.isYellow()) {
      wizardDevice.setOngoingRequest();
    }

    wizardDevice.setState(deviceStatus.isYellow() ? ActivabilityState.ONGOING_REQUEST : ActivabilityState.ACTIVABLE);
    TipoDispositivo deviceType = device.getTipoDispositivo();
    wizardDevice.setType(new WizardDeviceType(new WizardDeviceTypeId(deviceType.getNome()
                                                                               .name()),
                                              deviceType.isMultiservizio()));
    device.getStatoDispositivoServizios()
          .stream()
          .forEach(ds -> {
            final WizardService wizardService = new WizardService(new WizardServiceId(ds.getId() + ""));
            wizardService.setActivationDate(ds.getDataAttivazione());
            wizardService.setState(ds.getStato()
                                     .name());
            wizardService.setType(new WizardServiceType(new WizardServiceTypeId(ds.getTipoServizio()
                                                                                  .getNome())));
            wizardDevice.addService(wizardService);
          });

    wizardDevice.setSeriale(device.getSeriale());
    if (device.getContratto() != null && device.getContratto()
                                               .getClienteFai() != null) {
      wizardDevice.setCodiceCliente(device.getContratto()
                                          .getClienteFai()
                                          .getCodiceCliente());
    }
    return wizardDevice;
  }

}
