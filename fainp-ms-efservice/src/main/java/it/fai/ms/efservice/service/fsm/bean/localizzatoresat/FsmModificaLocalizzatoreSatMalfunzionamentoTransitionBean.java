package it.fai.ms.efservice.service.fsm.bean.localizzatoresat;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.localizzatoresat.FsmModificaLocalizzatoreSatMalfunzionamento;

@WithStateMachine(id = FsmModificaLocalizzatoreSatMalfunzionamento.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO)
public class FsmModificaLocalizzatoreSatMalfunzionamentoTransitionBean extends AbstractFsmRichiestaTransition {

}
