package it.fai.ms.efservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.listener.AssociazioneDvEntityListener;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A AssociazioneDV.
 */
@Entity
@Audited(withModifiedFlag = true, targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "associazione_dv")
@EntityListeners(value = { AssociazioneDvEntityListener.class })
public class AssociazioneDV implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "data")
  @Convert(converter = DateInstantConverter.class)
  private Instant data;

  @Column(name = "uuid_veicolo")
  private String uuidVeicolo;

  @Transient
  private String oldUuidVeicolo;

  @NotAudited
  @ManyToOne()
  @JsonIgnore
  private Dispositivo dispositivo;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Instant getData() {
    return data;
  }

  public AssociazioneDV data(Instant data) {
    this.data = data;
    return this;
  }

  public void setData(Instant data) {
    this.data = data;
  }

  public String getUuidVeicolo() {
    return uuidVeicolo;
  }

  public AssociazioneDV uuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
    return this;
  }

  public void setUuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
  }

  public Dispositivo getDispositivo() {
    return dispositivo;
  }

  public AssociazioneDV dispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
    return this;
  }

  public void setDispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
  }
  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  public AssociazioneDV oldUuidVeicolo(String oldUuidVeicolo) {
    this.oldUuidVeicolo = oldUuidVeicolo;
    return this;
  }

  public String getOldUuidVeicolo() {
    return oldUuidVeicolo;
  }

  public void setOldUuidVeicolo(String oldUuidVeicolo) {
    this.oldUuidVeicolo = oldUuidVeicolo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssociazioneDV associazioneDV = (AssociazioneDV) o;
    if (associazioneDV.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), associazioneDV.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "AssociazioneDV{" + "id=" + getId() + ", data='" + getData() + "'" + ", uuidVeicolo='" + getUuidVeicolo() + "'"
           + ", dispositivo='" + getDispositivo() + "'" + ", OLD_uuidVeicolo='" + getOldUuidVeicolo() + "'" + "}";
  }


}
