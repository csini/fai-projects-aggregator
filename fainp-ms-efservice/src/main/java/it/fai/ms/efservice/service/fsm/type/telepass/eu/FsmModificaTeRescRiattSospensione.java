/**
 * 
 */
package it.fai.ms.efservice.service.fsm.type.telepass.eu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.eu.FsmModificaTeRescRiattSospesoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

/**
 * @author Luca Vassallo
 */
@Service(value = FsmModificaTeRescRiattSospensione.FSM_MOD_TE_RESC_RIATT_SOSPESO)
public class FsmModificaTeRescRiattSospensione extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TE_RESC_RIATT_SOSPESO = "fsmModificaTeRescRiattSospeso";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTeRescRiattSospensione(@Qualifier(FsmModificaTeRescRiattSospesoConfig.MOD_TE_RESC_RIATT_SOSPESO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                           FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TE_RESC_RIATT_SOSPENSIONE;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_EUROPEO };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
