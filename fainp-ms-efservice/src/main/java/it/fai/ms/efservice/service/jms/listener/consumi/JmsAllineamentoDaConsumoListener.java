package it.fai.ms.efservice.service.jms.listener.consumi;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.KeyDatiAggiuntivi;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class JmsAllineamentoDaConsumoListener extends JmsObjectMessageListenerTemplate<AllineamentoDaConsumoMessage> implements JmsQueueListener {

  private static final String ANOMALY_CONSUMO_ALLINEAMENTO = "CONS_ALL001";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils   fsmRichiestaUtil;
  private final NotificationService notificationService;

  public JmsAllineamentoDaConsumoListener(final FsmRichiestaUtils _fsmRichiestaUtil, final NotificationService _notificationService) {
    fsmRichiestaUtil = _fsmRichiestaUtil;
    notificationService = _notificationService;
  }

  @Override
  public void consumeMessage(AllineamentoDaConsumoMessage allineamentoDaConsumo) {
    log.debug("Consume message: {}", allineamentoDaConsumo);
    TipoDispositivoEnum deviceType = allineamentoDaConsumo.getDeviceType();

    StatoRichiesta statoRichiesta = getRequestStatusByDeviceType(deviceType);
    RichiestaEvent operation = getOperationToFsmByDeviceType(deviceType);

    if (areFieldsValid(allineamentoDaConsumo, statoRichiesta, operation) && areFieldsValidToHgvDevice(allineamentoDaConsumo)) {
      String licensePlate = allineamentoDaConsumo.getLicensePlateNumber();
      String countryLicensePlate = allineamentoDaConsumo.getLicensePlateNation();
      List<Richiesta> richieste = fsmRichiestaUtil.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(licensePlate,
                                                                                                               countryLicensePlate,
                                                                                                               deviceType, statoRichiesta);

      if (richieste != null && !richieste.isEmpty()) {
        Richiesta richiesta = getRichiestaByDeviceType(richieste, allineamentoDaConsumo);
        log.info("Found request {} to ALLINEAMENTO DA CONSUMO for device type: {}", richiesta.getIdentificativo(),
                 richiesta.getTipoDispositivo());
        try {
			changeStatusRequest(richiesta, operation);
		} catch (FsmExecuteCommandException e) {
			throw new RuntimeException(e);
		}
      } else {
        sendAnomalia(allineamentoDaConsumo, statoRichiesta);
      }
    } else {
      log.error("Any fields are not present to consume message");
    }
  }

  private void sendAnomalia(AllineamentoDaConsumoMessage allineamentoDaConsumo, StatoRichiesta statoRichiesta) {
    StringBuilder sb = new StringBuilder("No request found for [");
    sb.append("LicensePlate: ")
      .append(allineamentoDaConsumo.getLicensePlateNumber());
    sb.append(" - Country_LicensePlate: ")
      .append(allineamentoDaConsumo.getLicensePlateNation());
    sb.append(" - Device type: ")
      .append(allineamentoDaConsumo.getDeviceType());
    sb.append(" - Request status: ")
      .append(statoRichiesta);
    Optional<String> contractCode = allineamentoDaConsumo.getContractCode();
    if (contractCode.isPresent()) {
      sb.append(" - ContractCode: ")
        .append(contractCode.get());
    }
    Optional<String> startDate = allineamentoDaConsumo.getStartDate();
    if (startDate.isPresent()) {
      sb.append(" - Start Date: ")
        .append(startDate.get());
    }
    Optional<String> endDate = allineamentoDaConsumo.getEndDate();
    if (endDate.isPresent()) {
      sb.append(" - End Date: ")
        .append(endDate.get());
    }
    sb.append("]");

    Map<String, Object> parameters = new HashMap<>();
    parameters.put("message", sb.toString());
    notificationService.notify(ANOMALY_CONSUMO_ALLINEAMENTO, parameters);
  }

  private boolean areFieldsValidToHgvDevice(AllineamentoDaConsumoMessage allineamentoDaConsumo) {
    boolean areFieldsValid = true;
    TipoDispositivoEnum deviceType = allineamentoDaConsumo.getDeviceType();
    if (!deviceType.equals(TipoDispositivoEnum.HGV))
      return true;

    if (!allineamentoDaConsumo.getContractCode()
                              .isPresent()) {
      log.error("Not present contract code");
      areFieldsValid = false;
    }
    if (!allineamentoDaConsumo.getStartDate()
                              .isPresent()) {
      log.error("Not present StartDate");
      areFieldsValid = false;
    }
    if (!allineamentoDaConsumo.getEndDate()
                              .isPresent()) {
      log.error("Not present EndDate");
      areFieldsValid = false;
    }
    return areFieldsValid;
  }

  private Richiesta getRichiestaByDeviceType(List<Richiesta> richieste, AllineamentoDaConsumoMessage allineamentoDaConsumo) {
    Richiesta richiesta = null;
    TipoDispositivoEnum deviceType = allineamentoDaConsumo.getDeviceType();
    switch (deviceType) {
    case TOLL_COLLECT:
      if (richieste.size() > 1) {
        log.error("Found more requests to deviceType: {}", allineamentoDaConsumo.getDeviceType());
        throw new IllegalArgumentException("Found more requests to deviceType: " + allineamentoDaConsumo.getDeviceType());
      }
      richiesta = richieste.get(0);
      break;

    case HGV:
      richiesta = richieste.stream()
                           .filter(r -> r.getContratto()
                                         .getCodContrattoCliente()
                                         .equals(allineamentoDaConsumo.getContractCode()
                                                                      .get()))
                           .map(r -> {
                             Optional<Richiesta> optRichiesta = Optional.empty();
                             List<DatiAggiuntiviRichiesta> datiAggDataDa = r.getDatiAggiuntivi();
                             List<DatiAggiuntiviRichiesta> datiAggDataA = r.getDatiAggiuntivi();
                             boolean validDataDa = checkDateOnDatiAggiuntiviRichiesta(datiAggDataDa, KeyDatiAggiuntivi.DATA_PRENOTAZIONE_DA,
                                                                                      allineamentoDaConsumo.getStartDate()
                                                                                                           .get());
                             boolean validDataA = checkDateOnDatiAggiuntiviRichiesta(datiAggDataA, KeyDatiAggiuntivi.DATA_PRENOTAZIONE_A,
                                                                                     allineamentoDaConsumo.getEndDate()
                                                                                                          .get());
                             if (validDataDa && validDataA) {
                               optRichiesta = Optional.of(r);
                             }
                             return optRichiesta;
                           })
                           .filter(optRequest -> optRequest.isPresent())
                           .map(optRichiesta -> optRichiesta.get())
                           .findFirst()
                           .get();
      break;

    default:
      log.warn("Not manage device {} to consumer message ALLINEAMENTO DA CONSUMO.", deviceType);
      break;
    }
    return richiesta;
  }

  private boolean checkDateOnDatiAggiuntiviRichiesta(List<DatiAggiuntiviRichiesta> datiAggiuntivi, KeyDatiAggiuntivi tipoDato,
                                                     String data) {
    return datiAggiuntivi.stream()
                         .filter(da -> da.getChiave()
                                         .equals(tipoDato)
                                       && da.getValore()
                                            .equals(data))
                         .findFirst()
                         .isPresent();
  }

  private RichiestaEvent getOperationToFsmByDeviceType(TipoDispositivoEnum deviceType) {
    RichiestaEvent operation = null;
    switch (deviceType) {
    case TOLL_COLLECT:
      operation = RichiestaEvent.MU_ATTIVO;
      break;
    case HGV:
      operation = RichiestaEvent.MU_CONSUMO;
      break;
    // TODO add device to managed
    default:
      log.warn("This device type: {} is not managed to set operation to FSM", deviceType);
    }
    return operation;
  }

  private StatoRichiesta getRequestStatusByDeviceType(TipoDispositivoEnum deviceType) {
    StatoRichiesta statoRichiesta = null;

    switch (deviceType) {
    case TOLL_COLLECT:
      statoRichiesta = StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO;
      break;
    case HGV:
      statoRichiesta = StatoRichiesta.INOLTRATO;
      break;
    // TODO add device to managed
    default:
      log.warn("This device type: {} is not managed to set stato richiesta to find request", deviceType);
    }

    return statoRichiesta;
  }

  private void changeStatusRequest(Richiesta richiesta, RichiestaEvent operation) throws FsmExecuteCommandException {
    log.info("Call change status to richiesta {} with command: {}", richiesta.getIdentificativo(), operation);
    Set<String> identifierRequests = new HashSet<>();
    identifierRequests.add(richiesta.getIdentificativo());
    fsmRichiestaUtil.changeStatusRichieste(identifierRequests, operation.name(), "", "");
  }

  private boolean areFieldsValid(AllineamentoDaConsumoMessage allineamentoDaConsumo, StatoRichiesta statoRichiesta,
                                 RichiestaEvent operation) {
    String countryLicensePlate = allineamentoDaConsumo.getLicensePlateNation();
    String licensePlate = allineamentoDaConsumo.getLicensePlateNumber();
    if (isBlankCountryLicensePlate(countryLicensePlate) || isBlankLicensePlate(licensePlate) || isNullStatoRichiesta(statoRichiesta)
        || isNullOperationToFsm(operation)) {
      return false;
    }
    return true;
  }

  private boolean isBlankCountryLicensePlate(String countryLicensePlate) {
    if (StringUtils.isBlank(countryLicensePlate)) {
      log.error("Country License Plate on AllineamentoDaConsumo is blank");
      return true;
    }
    return false;
  }

  private boolean isBlankLicensePlate(String licensePlate) {
    if (StringUtils.isBlank(licensePlate)) {
      log.error("Country License Plate on AllineamentoDaConsumo is blank");
      return true;
    }
    return false;
  }

  private boolean isNullStatoRichiesta(StatoRichiesta statoRichiesta) {
    if (statoRichiesta == null) {
      log.error("Stato richiesta is not set");
      return true;
    }
    return false;
  }

  private boolean isNullOperationToFsm(RichiestaEvent operationToFsm) {
    if (operationToFsm == null) {
      log.error("Operation to FSM is not set");
      return true;
    }
    return false;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.ALLINEAMENTI_DA_CONSUMO;
  }
}
