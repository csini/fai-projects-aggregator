package it.fai.ms.efservice.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.fai.ms.common.jms.dto.document.delivery.DeliveryType;
import it.fai.ms.common.jms.dto.efservice.DeliveryCompanyDTO;
import it.fai.ms.common.jms.dto.efservice.DeliveryDeviceDTO;

public class DeliveryDTO implements Serializable {

  private static final long serialVersionUID = -6988085805117857824L;

  private DeliveryCompanyDTO company;

  private DeliveryCompanyDTO accountHolder;

  private Map<DeliveryType, List<DeliveryDeviceDTO>> deliveries = new HashMap<DeliveryType, List<DeliveryDeviceDTO>>();

  public DeliveryCompanyDTO getCompany() {
    return company;
  }

  public void setCompany(DeliveryCompanyDTO company) {
    this.company = company;
  }

  public DeliveryCompanyDTO getAccountHolder() {
    return accountHolder;
  }

  public void setAccountHolder(DeliveryCompanyDTO accountHolder) {
    this.accountHolder = accountHolder;
  }

  public Map<DeliveryType, List<DeliveryDeviceDTO>> getDeliveries() {
    return deliveries;
  }

  public void setDeliveries(Map<DeliveryType, List<DeliveryDeviceDTO>> deliveries) {
    this.deliveries = deliveries;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DeliveryDTO [company=");
    builder.append(company);
    builder.append(", accountHolder=");
    builder.append(accountHolder);
    builder.append(", deliveries=");
    builder.append(deliveries);
    builder.append("]");
    return builder.toString();
  }

}
