package it.fai.ms.efservice.service.fsm.type.telepass.eusat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.eusat.FsmModificaTeSatVarTargaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTeSatVarTarga.FSM_MOD_TE_SAT_VARTARGA)
public class FsmModificaTeSatVarTarga extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TE_SAT_VARTARGA = "fsmModificaTeSatVarTarga";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTeSatVarTarga(@Qualifier(FsmModificaTeSatVarTargaConfig.MOD_TE_SAT_VARTARGA) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                  FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TE_SAT_VARIAZIONE_TARGA;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_EUROPEO_SAT };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
