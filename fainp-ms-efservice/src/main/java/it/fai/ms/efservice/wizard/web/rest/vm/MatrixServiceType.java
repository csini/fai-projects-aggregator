package it.fai.ms.efservice.wizard.web.rest.vm;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
public class MatrixServiceType {

  private boolean                         checked = true;
  private List<MatrixActivabilityFailure> errors;
  private String                          id;
  private String                          itemKey;
  private String                          state;

  public MatrixServiceType(final String _id) {
    id = _id;
  }

  public void addFailure(final MatrixActivabilityFailure _failure) {
    Optional.ofNullable(_failure)
            .ifPresent(failure -> {
              errors = Optional.ofNullable(errors)
                               .orElse(new LinkedList<>());
              errors.add(_failure);
            });
  }

  @JsonInclude(Include.NON_NULL)
  public List<MatrixActivabilityFailure> getErrors() {
    return errors;
  }

  public String getItemKey() {
    return itemKey;
  }

  @JsonProperty(value = "stato")
  public String getStato() {
    return state;
  }

  public String getTipoServizio() {
    return id;
  }

  public boolean isChecked() {
    return checked;
  }

  public void setChecked(boolean checked) {
    this.checked = checked;
  }

  public void setItemKey(final String _itemKey) {
    itemKey = _itemKey;
  }

  public void setState(final String _state) {
    state = _state;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixServiceType [errors=");
    builder.append(this.errors);
    builder.append(", id=");
    builder.append(this.id);
    builder.append(", itemKey=");
    builder.append(this.itemKey);
    builder.append(", state=");
    builder.append(this.state);
    builder.append(", checked=");
    builder.append(this.checked);
    builder.append("]");
    return builder.toString();
  }

}
