/**
 * 
 */
package it.fai.ms.efservice.service.fsm.serializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@Service
public class FsmRichiestaSerializer {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public String serialize(StateMachineContext<StatoRichiesta, RichiestaEvent> obj) {
    String jsonInString = null;
    if (obj != null) {
      ObjectMapper objMap = new ObjectMapper();
      try {
        jsonInString = objMap.writeValueAsString(obj);
      } catch (JsonProcessingException e) {
        log.error("Exception JSON", e);
        throw new RuntimeException("Exception on serialize Json: " + e.getMessage(), e);
      }
    } else {
      log.error("Null object FSM to serialize");
      throw new RuntimeException("Null object FSM to serialize");
    }

    return jsonInString;
  }

  public StateMachineContext<StatoRichiesta, RichiestaEvent> deSerialize(String json) {
    return deSerialize(json, null);
  }

  @SuppressWarnings("unchecked")
  public StateMachineContext<StatoRichiesta, RichiestaEvent> deSerialize(String json, String idNewMachine) {
    DefaultStateMachineContext<StatoRichiesta, RichiestaEvent> defaultStateMachineContext = null;

    ObjectMapper objMap = new ObjectMapper();
    Object obj = null;
    try {
      obj = objMap.readValue(json, Object.class);

      if (obj instanceof HashMap<?, ?>) {
        HashMap<String, Object> map = (HashMap<String, Object>) obj;

        Object object = map.get("historyStates");
        Map<StatoRichiesta, StatoRichiesta> mapping = new HashMap<>();
        if (object instanceof Map<?, ?>) {
          mapping = (Map<StatoRichiesta, StatoRichiesta>) object;
        }

        ExtendedState extendedState = new DefaultExtendedState();
        object = map.get("extendedState");
        if (object instanceof HashMap<?, ?>) {
          HashMap<String, Object> mapExtendedState = (HashMap<String, Object>) object;
          Object variables = mapExtendedState.get("variables");
          extendedState.getVariables()
                       .putAll((Map<? extends Object, ? extends Object>) variables);
        }

        String idMachine = null;
        if (StringUtils.isNotBlank(idNewMachine)) {
          log.info("idNewMachine {}", idNewMachine);
          idMachine = idNewMachine;
        } else {
          idMachine = (String) map.get("id");
        }

        defaultStateMachineContext = new DefaultStateMachineContext<StatoRichiesta, RichiestaEvent>((List<StateMachineContext<StatoRichiesta, RichiestaEvent>>) map.get("childs"),
                                                                                                    StatoRichiesta.valueOf((String) map.get("state")),
                                                                                                    null, null, extendedState, mapping,
                                                                                                    idMachine);
      }
    } catch (JsonParseException e) {
      // TODO manage exception
      e.printStackTrace();
    } catch (JsonMappingException e) {
      // TODO manage exception
      e.printStackTrace();
    } catch (IOException e) {
      // TODO manage exception
      e.printStackTrace();
    }

    return defaultStateMachineContext;
  }

}
