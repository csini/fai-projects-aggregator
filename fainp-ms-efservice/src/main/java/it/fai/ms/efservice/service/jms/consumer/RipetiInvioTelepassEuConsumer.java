package it.fai.ms.efservice.service.jms.consumer;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.telepass.TelepassEuRipetiInvioInboundDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.FsmByTipoDispositivoService;

@Service
@Transactional
public class RipetiInvioTelepassEuConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaRepository         richiestaRepo;
  private final RichiestaRepositoryExt      richiestaRepoExt;
  private final FsmByTipoDispositivoService fsmRichiestaDeviceTypeService;

  /**
   * @param richiestaRepo
   * @param fsmInoltroTe
   */
  public RipetiInvioTelepassEuConsumer(RichiestaRepository richiestaRepo, RichiestaRepositoryExt richiestaRepoExt,
                                       final FsmByTipoDispositivoService _fsmRichiestaDeviceTypeService) {
    this.richiestaRepo = richiestaRepo;
    this.richiestaRepoExt = richiestaRepoExt;
    fsmRichiestaDeviceTypeService = _fsmRichiestaDeviceTypeService;
  }

  public void ripetiInvio(TelepassEuRipetiInvioInboundDTO dto) throws Exception {

    String codiceCliente = dto.getCodiceCliente();
    List<Richiesta> ordini = richiestaRepoExt.findAllByCodiceClienteFaiAndStatoRichiesta(StatoRichiesta.ORDINE_SOSPESO, codiceCliente);
    if (ordini == null || (ordini != null && ordini.isEmpty())) {
      log.warn("Not found Richieste with state: ORDINE_SOSPESO for cliente fai: {}", codiceCliente);
      return;
    }

    List<Richiesta> richiesteFilteredByType = ordini.stream()
                                                    .filter(r -> r.getTipoDispositivo() != null)
                                                    .filter(r -> r.getTipoDispositivo()
                                                                  .getNome()
                                                                  .equals(TipoDispositivoEnum.TELEPASS_EUROPEO)
                                                                 || r.getTipoDispositivo()
                                                                     .getNome()
                                                                     .equals(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT))
                                                    .collect(toList());

    for (Richiesta richiesta : richiesteFilteredByType) {
      RichiestaEvent commandRichiesta = RichiestaEvent.MS_RIPETI_INVIO;
      AbstractFsmRichiesta fsm = fsmRichiestaDeviceTypeService.getFsmNewOrderByTipoDispositivo(richiesta.getTipoDispositivo()
                                                                                                        .getNome());
      boolean availableCommand = fsm.isAvailableCommand(commandRichiesta, richiesta);
      if (availableCommand) {
        log.info("Fsm Inoltro Telepass EU => Command: " + commandRichiesta + " is available...");
        Richiesta richiestaWithStateSwitched = fsm.executeCommandToChangeState(commandRichiesta, richiesta);
        richiestaRepo.save(richiestaWithStateSwitched);
      } else {
        log.warn("Fsm Inoltro Telepass EU => Command " + commandRichiesta + " is not available for Richiesta: "
                 + richiesta.getIdentificativo() + " [" + richiesta.getId() + "]");
      }
    }
  }
}
