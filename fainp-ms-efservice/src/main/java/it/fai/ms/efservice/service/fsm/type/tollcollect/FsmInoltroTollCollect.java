package it.fai.ms.efservice.service.fsm.type.tollcollect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.tollcollect.FsmInoltroTollCollectConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroTollCollect.FSM_INOLTRO_TOLLCOLLECT)
public class FsmInoltroTollCollect extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_TOLLCOLLECT = "fsmInoltroTollCollect";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroTollCollect(@Qualifier(FsmInoltroTollCollectConfig.INOLTRO_TOLLCOLLECT) StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory,
                               FsmRichiestaCacheService cacheService) {
    this.stateMachineFactory = stateMachineFactory;
    this.cacheService = cacheService;
    fsmType = FsmType.INOLTRO_TOLL_COLLECT;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TOLL_COLLECT };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
