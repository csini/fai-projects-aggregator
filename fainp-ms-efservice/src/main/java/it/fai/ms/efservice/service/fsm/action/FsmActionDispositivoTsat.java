package it.fai.ms.efservice.service.fsm.action;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.state.State;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionDispositivoTsat implements Action<StatoRichiesta, RichiestaEvent> {
  
  private final static Logger log = LoggerFactory.getLogger(FsmActionDispositivoTsat.class);
  
  private FsmSenderToQueue senderFsmUtil;
  
  public FsmActionDispositivoTsat(FsmSenderToQueue senderFsmUtil) {
    this.senderFsmUtil = senderFsmUtil;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    State<StatoRichiesta, RichiestaEvent> source = context.getSource();
    State<StatoRichiesta, RichiestaEvent> target = context.getTarget();
    if(source == null || target == null) {
      log.debug("Source {} or Target {} is null", source, target);
      return;
    }
    
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        
        Set<Dispositivo> dispositivos = richiesta.getDispositivos();
        StatoRichiesta from = source.getId();
        StatoRichiesta to = target.getId();
        if(from.equals(StatoRichiesta.ACCETTATO) && to.equals(StatoRichiesta.VERIFICA_ACCETTAZIONE)) {
          
          sendMessageToChangeStatusDispositivi(dispositivos, DispositivoEvent.TSAT_DEPOSITO);
        } else if(from.equals(StatoRichiesta.VERIFICA_ACCETTAZIONE)) {
          
          if(to.equals(StatoRichiesta.ABBINATO) || to.equals(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC)) {
            
            sendMessageToChangeStatusDispositivi(dispositivos, DispositivoEvent.TSAT_DEPOSITO_ATTESA_SPEDIZIONE);
          } else {
            log.error("This case [Source {} - Target {}] is not possible....", source, target);
          }
        } else if(from.equals(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC) && to.equals(StatoRichiesta.VERIFICA_ACCETTAZIONE)) {
          
          sendMessageToChangeStatusDispositivi(dispositivos, DispositivoEvent.TSAT_DEPOSITO_PER_VERIFICA);
        } else if(from.equals(StatoRichiesta.ACCETTATO_FORNITORE) && to.equals(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)) {
          
          sendMessageToChangeStatusDispositivi(dispositivos, DispositivoEvent.TSAT_ATTIVO_ATTESA_SPEDIZIONE);
        } else {
          
          log.warn("This case [Source {} - Target {}] in not managed....", source, target);
        }
      }
    }

  }
  
  private void sendMessageToChangeStatusDispositivi(Set<Dispositivo> dispositivi, DispositivoEvent event) {
    for (Dispositivo dispositivo : dispositivi) {
      log.debug("Send message {} to change status for dispositivo {}", event.name(), dispositivo.getIdentificativo());
      senderFsmUtil.sendMessageForChangeStatoDispositivo(dispositivo, event);
    }
    log.debug("FINISH send message to change status for dispositivi");
  }

}
