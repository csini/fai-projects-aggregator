package it.fai.ms.efservice.service.report.dto;

public class AnomalieContrattiAttiviDTO {

//  public AnomalieContrattiAttiviDTO(String contrattoNumero, String produttoreNome, String ragioneSociale, String codiceAzienda,
//                                    String nazioneAzienda) {
//    super();
//    this.contrattoNumero = contrattoNumero;
//    this.produttoreNome  = produttoreNome;
//    this.ragioneSociale  = ragioneSociale;
//    this.codiceAzienda   = codiceAzienda;
//    this.nazioneAzienda  = nazioneAzienda;
//  }

  private static final long serialVersionUID = 1L;

  private String contrattoNumero;
  private String produttoreNome;
  private String ragioneSociale;
  private String codiceAzienda;
  private String nazioneAzienda;

  public String getContrattoNumero() {
    return contrattoNumero;
  }

  public void setContrattoNumero(String contrattoNumero) {
    this.contrattoNumero = contrattoNumero;
  }

  public String getProduttoreNome() {
    return produttoreNome;
  }

  public void setProduttoreNome(String produttoreNome) {
    this.produttoreNome = produttoreNome;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getNazioneAzienda() {
    return nazioneAzienda;
  }

  public void setNazioneAzienda(String nazioneAzienda) {
    this.nazioneAzienda = nazioneAzienda;
  }

  @Override
  public String toString() {
    return "AnomalieContrattiAttiviDTO{" +
      "contrattoNumero='" + contrattoNumero + '\'' +
      ", produttoreNome='" + produttoreNome + '\'' +
      ", ragioneSociale='" + ragioneSociale + '\'' +
      ", codiceAzienda='" + codiceAzienda + '\'' +
      ", nazioneAzienda='" + nazioneAzienda + '\'' +
      '}';
  }
}
