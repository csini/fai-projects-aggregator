package it.fai.ms.efservice.service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.common.jms.dto.efservice.DispositivoDTO;
import it.fai.ms.common.jms.email.EmailMessage.RECIPIENTS;
import it.fai.ms.common.jms.email.EmailMessageByTemplate;
import it.fai.ms.common.utils.MessageSourceServiceExt;
import it.fai.ms.efservice.client.dto.UserDTO;
import it.fai.ms.efservice.repository.dispositivo.TipoDispositivoAssegnabile;

@Service
@Transactional
public class MailActivatedDeviceService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public final static String PREFIX_PATH                    = "mails/";
  public final static String TEMPLATE_OPERATOR_DEVICE       = "operatoreDevicesTemplate";
  public final static String TEMPLATE_TRACKYCARD_PIN        = "trackycardPinRequestTemplate";
  public final static String TEMPLATE_ACTIVATION_VIACARD    = "EmailActivationViaCard";
  public final static String TEMPLATE_ACTIVATION_TRACKYCARD = "EmailActivationTrackyCard";

  public final static String SUBJECT_KEY_DEVICES_ASSIGNED = "email.device.assigned";
  public final static String SUBJECT_KEY_DEVICE_ACTIVE    = "email.activated.device";
  public final static String SUBJECT_KEY_PIN_TRACKYCARD   = "email.richiestapin";

  private final static String DEVICE_TYPE_KEY   = "#deviceType#";
  private final static String NUMBER_DEVICE_KEY = "#numDevice#";
  private final static String CLIENTE_CODE_KEY  = "#clientCode#";

  public final String defaultLangKey = "it";

  private final GatewayClientService    gatewayClientService;
  private final EmailNotificationSender emailNotificationSender;

  public MailActivatedDeviceService(final GatewayClientService _gatewayClientService,
                                    final EmailNotificationSender _emailNotificationSender) {
    gatewayClientService = _gatewayClientService;
    emailNotificationSender = _emailNotificationSender;
  }

  public boolean sendActivationViaCard(String codiceCliente) {
    log.info("Start sending mail to notify activation ViaCard by codice cliente: {}", codiceCliente);
    return sendMailByCodiceClienteAndTemplate(codiceCliente, TEMPLATE_ACTIVATION_VIACARD, TipoDispositivoEnum.TELEPASS_ITALIANO);
  }

  public boolean sendActivationTrackyCard(String codiceCliente) {
    log.info("Start sending mail to notify activation TrackyCard by codice cliente: {}", codiceCliente);
    return sendMailByCodiceClienteAndTemplate(codiceCliente, TEMPLATE_ACTIVATION_TRACKYCARD, TipoDispositivoEnum.GO_BOX);
  }

  public boolean sendAssociationFromDeposito(String codiceCliente, TipoDispositivoAssegnabile tipoDispositivo, int quantita, String lang,
                                             List<DispositivoDTO> dispositiviAssegnati) {
    String langKey = getLanguage(lang);
    Map<String, Object> templateParams = new HashMap<>();
    templateParams.put("devices", dispositiviAssegnati);
    templateParams.put("customerCode", codiceCliente);
    templateParams.put("quantita", quantita);
    templateParams.put("deviceTypeAssigned", tipoDispositivo.getTipoDispositivo()
                                                            .name());

    AziendaDTO azienda = getAziendaByCodice(codiceCliente);
    if(azienda !=null)
      templateParams.put("ragioneSociale",azienda.getRagioneSociale());
    else
      templateParams.put("ragioneSociale",codiceCliente);

    Map<String, String> recipients = new HashMap<>();
    recipients.put(RECIPIENTS.operatore.name(), "operatore");

    EmailMessageByTemplate dto = generateEmailMessageAssociationDevice(recipients, langKey, TEMPLATE_OPERATOR_DEVICE, templateParams);
    return emailNotificationSender.sendNotification(dto);
  }





  public boolean sendTrackyCardPin(String codiceCliente,String targa, String trackycardSeriale, String pin) {
    log.info("Start sending mail for codiceCliente:=[{}], trackycardSeriale:=[{}]", codiceCliente, trackycardSeriale);

    UserDTO user = getUserByClientCode(codiceCliente);
    if (user == null) {
      return false;
    }

    Map<String, Object> templateParams = new HashMap<>();
    templateParams.put("customerCode", codiceCliente);
    templateParams.put("trackycardSeriale", trackycardSeriale);
    templateParams.put("pin", pin);
    AziendaDTO azienda = getAziendaByCodice(codiceCliente);
    if(azienda !=null)
      templateParams.put("ragioneSociale",azienda.getRagioneSociale());
    else
      templateParams.put("ragioneSociale",codiceCliente);

    templateParams.put("targa", targa);

    return sendMailToTrackyCardPin(user, trackycardSeriale, templateParams);
  }

  private boolean sendMailByCodiceClienteAndTemplate(String codiceCliente, String templateName, TipoDispositivoEnum tipoDispositivo) {
    UserDTO user = getUserByClientCode(codiceCliente);
    if (user == null) {
      return false;
    }

    EmailMessageByTemplate dto = generateEmailMessageActivationDevice(user, templateName, tipoDispositivo);
    log.info("Sending message {}", dto);
    return emailNotificationSender.sendNotification(dto);
  }

  private boolean sendMailToTrackyCardPin(UserDTO user, String trackycardSeriale, Map<String, Object> templateParams) {
    EmailMessageByTemplate dto = generateEmailMessageTrackycardPin(user.getEmail(), user.getLangKey(), trackycardSeriale,
                                                                   TEMPLATE_TRACKYCARD_PIN, templateParams);
    return emailNotificationSender.sendNotification(dto);
  }

  public EmailMessageByTemplate generateEmailMessageActivationDevice(UserDTO user, String templateName,
                                                                     TipoDispositivoEnum tipoDispositivo) {
    EmailMessageByTemplate messageDTO = new EmailMessageByTemplate();
    String langKey = (user != null) ? user.getLangKey() : defaultLangKey;
    if (!"EN".equalsIgnoreCase(langKey) && !"DE".equalsIgnoreCase(langKey)&&!"ES".equalsIgnoreCase(langKey)&&!"PL".equalsIgnoreCase(langKey))
      langKey = defaultLangKey;
    messageDTO.setLangKey(langKey);
    messageDTO.setTo(user.getEmail());
    String subject = getSubjectByLangAndKey(langKey, SUBJECT_KEY_DEVICE_ACTIVE);
    subject = subject.replace(DEVICE_TYPE_KEY, tipoDispositivo.name());
    messageDTO.setSubject(subject);
    messageDTO.setTemplateName(getTemplateNameByLang(templateName, langKey));

    Map<String, Object> templateParams = new HashMap<String, Object>();
    templateParams.put("langKey", langKey);
    AziendaDTO azienda = getAziendaByCodice(user.getCodiceAzienda());
    if(azienda !=null)
      templateParams.put("ragioneSociale",azienda.getRagioneSociale());
    else
      templateParams.put("ragioneSociale",user.getCodiceAzienda());
    messageDTO.setTemplateParams(templateParams);

    log.info("DTO to send notification: {}", messageDTO);
    return messageDTO;
  }

  // FIXME todo aligned to retrieve template and subject by language.
  public EmailMessageByTemplate generateEmailMessageTrackycardPin(String emailTo, String langKey, String trackycardSeriale,
                                                                  String templateName, Map<String, Object> templateParams) {
    EmailMessageByTemplate dto = new EmailMessageByTemplate();
    dto.setTo(emailTo);
    dto.setLangKey(langKey);
    String[] strArray = new String[1];
    strArray[0] = trackycardSeriale;
    dto.setSubject(MessageSourceServiceExt.getMessage(SUBJECT_KEY_PIN_TRACKYCARD, strArray, Locale.forLanguageTag(langKey)));
    dto.setDatetime(Instant.now());
    dto.setTemplateName(getTemplateNameByLang(templateName, langKey));
    dto.setTemplateParams(templateParams);
    return dto;
  }

  public EmailMessageByTemplate generateEmailMessageAssociationDevice(Map<String, String> recipients, String langKey, String template,
                                                                      Map<String, Object> templateParams) {
    EmailMessageByTemplate dto = new EmailMessageByTemplate();
    dto.setRecipients(recipients);
    String subject = getSubjectByLangAndKey(langKey, SUBJECT_KEY_DEVICES_ASSIGNED);
    subject = subject.replace(CLIENTE_CODE_KEY, (String) templateParams.get("customerCode"));
    subject = subject.replace(DEVICE_TYPE_KEY, (String) templateParams.get("deviceTypeAssigned"));
    subject = subject.replace(NUMBER_DEVICE_KEY, ((int) templateParams.get("quantita")) + "");
    dto.setSubject(subject);
    // FIXME
    dto.setTo("");
    dto.setLangKey(langKey);
    dto.setTemplateName(getTemplateNameByLang(template, langKey));
    dto.setTemplateParams(templateParams);
    return dto;
  }

  private String getSubjectByLangAndKey(String langKey, String key_subject) {
    Locale locale = Locale.forLanguageTag(langKey);
    return MessageSourceServiceExt.getMessage(key_subject, locale);
  }

  private String getTemplateNameByLang(String templateName, String langKey) {
    String templateNameWithLanguage = templateName;
    if (StringUtils.isNotBlank(langKey)) {
      templateNameWithLanguage = templateName + "-" + (langKey.toLowerCase());
    }
    log.info("Template by lang: {}", templateNameWithLanguage);
    return templateNameWithLanguage;
  }

  private String getLanguage(String lang) {
    if (!"EN".equalsIgnoreCase(lang) && !"DE".equalsIgnoreCase(lang)) {
      return defaultLangKey;
    }
    return lang;
  }

  private UserDTO getUserByClientCode(String clientCode) {
    return gatewayClientService.getUserByCodiceCliente(clientCode);
  }

  private AziendaDTO getAziendaByCodice(String clientCode) {
    return gatewayClientService.getAziendaByCodice(clientCode);
  }



}
