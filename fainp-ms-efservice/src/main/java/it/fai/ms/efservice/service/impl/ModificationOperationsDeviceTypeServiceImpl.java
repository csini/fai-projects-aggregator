package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ModificationOperationsDeviceType;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ModificationOperationsDeviceTypeRepository;
import it.fai.ms.efservice.service.ModificationOperationsDeviceTypeService;

@Service
@Transactional
public class ModificationOperationsDeviceTypeServiceImpl implements ModificationOperationsDeviceTypeService {

  private final ModificationOperationsDeviceTypeRepository modificationOperationsDeviceTypeRepository;

  public ModificationOperationsDeviceTypeServiceImpl(final ModificationOperationsDeviceTypeRepository _modificationOperationsDeviceTypeRepository) {
    modificationOperationsDeviceTypeRepository = _modificationOperationsDeviceTypeRepository;
  }

  @Override
  public List<String> getModificationOperations(TipoDispositivoEnum deviceType) {
    List<String> operations = new ArrayList<>();

    Optional<ModificationOperationsDeviceType> modificationOperationsOpt = modificationOperationsDeviceTypeRepository.findByDeviceType(deviceType);
    if (modificationOperationsOpt.isPresent()) {
      operations.addAll(modificationOperationsOpt.get()
                                                 .getOperations());
    } else {
      operations = TipoRichiesta.OPERAZIONI_MODIFICA_DISPOSITIVO.stream()
                                                                .map(tipoRichiesta -> tipoRichiesta.name())
                                                                .collect(toList());
    }
    return operations;
  }

}
