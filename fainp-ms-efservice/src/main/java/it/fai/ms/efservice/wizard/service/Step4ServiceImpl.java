package it.fai.ms.efservice.wizard.service;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.dto.WizardVehicleDTO;
import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardDevice;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixDeviceType;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixVehicle;
import it.fai.ms.efservice.wizard.repository.WizardDeviceRepository;
import it.fai.ms.efservice.wizard.repository.WizardDeviceTypeRepository;
import it.fai.ms.efservice.wizard.repository.WizardVehicleRepository;

@Service
public class Step4ServiceImpl implements Step4Service {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final static String SERVICE_PEDAGGI_AUSTRIA = "PEDAGGI_AUSTRIA";
  private final String        DEVICE_TRACKYCARD       = "TRACKYCARD";

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  private WizardDeviceRepository                  deviceRepository;
  private WizardDeviceTypeRepository              deviceTypeRepository;
  private PrecalculatedRuleService                ruleService;
  private WizardVehicleRepository                 vehicleRepository;
  private WizardVehicleService                    wizardVehicleService;
  private ConfigurazioneDispositiviServiziService configDevicesService;
  final VehicleClient                             vehicleClient;

  @Autowired
  public Step4ServiceImpl(final PrecalculatedRuleService _precalculatedRuleService, final WizardDeviceRepository _deviceRepository,
                          final WizardDeviceTypeRepository _deviceTypeRepository, final WizardVehicleRepository _vehicleRepository,
                          final WizardVehicleService _wizardVehicleService,
                          final ConfigurazioneDispositiviServiziService _configDevicesService, final VehicleClient _vehicleClient) {
    ruleService = _precalculatedRuleService;
    deviceRepository = _deviceRepository;
    deviceTypeRepository = _deviceTypeRepository;
    vehicleRepository = _vehicleRepository;
    wizardVehicleService = _wizardVehicleService;
    configDevicesService = _configDevicesService;
    vehicleClient = _vehicleClient;
  }

  @Override
  @Transactional(readOnly = true)
  public WizardStep4Matrix buildCompleteMatrix(final List<WizardVehicleDTO> wizardVehiclesDTO, final String codiceCliente) {

    _log.info("Building complete wizardStep4Matrix");
    _log.trace("codiceCliente:=[{}] -  WizardVehiclesDTO: ", codiceCliente, wizardVehiclesDTO);

    final long time1 = System.currentTimeMillis();

    WizardStep4Matrix wizardStep4Matrix = new WizardStep4Matrix(null);

    final Set<WizardDeviceTypeId> deviceTypeIds = deviceTypeRepository.findAllIds();

    wizardVehiclesDTO.forEach(wizardVehicleDto -> {
      String uuidVehicle = wizardVehicleDto.getUuidVehicle();
      _log.trace("Composing matrix for uuidVehicle:=[{}]", uuidVehicle);

      Optional<WizardVehicle> optWizardVehicle = vehicleRepository.find(uuidVehicle);
      _log.trace("vehicleRepository.find(uuid):=[{}]", optWizardVehicle);

      if (optWizardVehicle.isPresent()) {

        WizardVehicle wizardVehicle = optWizardVehicle.get();
        wizardVehicle.setHasDevicesActive(wizardVehicleDto.isHasDevicesActive());
        wizardVehicle.setExpiring(wizardVehicleDto.isExpiring());
        wizardVehicle.setExpired(wizardVehicleDto.isExpired());

        final Set<WizardDevice> wizardDevices = deviceRepository.findByWizardVehicleIdFilteredByDevice(wizardVehicle.getId(),
                                                                                                       deviceTypeIds);
        _log.trace("deviceRepository.findByWizardVehicleIdFilteredByDevice:=[{}]", wizardDevices);

        wizardVehicle.getDevices()
                     .addAll(wizardDevices);
        wizardVehicle.setLoadDevices();
        createMatrixStep4(wizardStep4Matrix, wizardVehicle, deviceTypeIds, codiceCliente);
      } else {
        _log.error("vehicle not found ... forcing dml sync");
        try {
          vehicleClient.sendVehicleByUUID(jwt, uuidVehicle);
        } catch (Throwable e) {
          _log.error("error during forcing dml sync", e);
        }
      }
    });
    final long time2 = System.currentTimeMillis();
    _log.info("Load complete step 4 matrix in {} ms", (time2 - time1));
    return wizardStep4Matrix;
  }

  @Override
  @Transactional(readOnly = true)
  public WizardStep4Matrix buildMatrix(final WizardServiceTypeId _wizardServiceTypeId, final Set<WizardVehicleId> _wizardVehicleIds,
                                       final String codiceCliente) {
    _log.info("Building wizardStep4Matrix for serviceTypeId {}", _wizardServiceTypeId.getId());

    WizardStep4Matrix wizardStep4Matrix = new WizardStep4Matrix(_wizardServiceTypeId.getId());

    final Set<WizardDeviceTypeId> deviceTypeIds = deviceTypeRepository.findByServiceTypeId(_wizardServiceTypeId)
                                                                      .stream()
                                                                      .filter(wizardDevice -> (!wizardDevice.isHidden()
                                                                                               || (wizardDevice.getId() != null
                                                                                                   && wizardDevice.getId()
                                                                                                                  .getId()
                                                                                                                  .equals("HGV"))))
                                                                      .map(deviceType -> deviceType.getId())
                                                                      .collect(toCollection(LinkedHashSet::new));

    addWizardDeviceTypeTrackyCardIfServiceIsPedaggiAustria(_wizardServiceTypeId, deviceTypeIds, _wizardVehicleIds);

    _log.debug("Building wizardStep4Matrix, found deviceTypeIds {} in the deviceTypeRepository for serviceTypeId {}", deviceTypeIds,
               _wizardServiceTypeId);

    WizardStep4Matrix createMatrixStep4 = createMatrixStep4(wizardStep4Matrix, _wizardVehicleIds, deviceTypeIds, _wizardServiceTypeId,
                                                            codiceCliente);
    return createMatrixStep4;
  }

  private void addWizardDeviceTypeTrackyCardIfServiceIsPedaggiAustria(final WizardServiceTypeId _wizardServiceTypeId,
                                                                      final Set<WizardDeviceTypeId> deviceTypeIds,
                                                                      final Set<WizardVehicleId> _wizardVehicleIds) {
    if (_wizardServiceTypeId.getId()
                            .equals(SERVICE_PEDAGGI_AUSTRIA)) {
      if (isNotPresentDevice(deviceTypeIds, DEVICE_TRACKYCARD)) {
        _log.debug("Device not found in list: {}", deviceTypeIds);
        Set<WizardDeviceType> wDeviceTypes = deviceTypeRepository.findAll();
        _log.debug("Found: {}", wDeviceTypes);
        wDeviceTypes.parallelStream()
                    .filter(dt -> dt.getId() != null && DEVICE_TRACKYCARD.equals(dt.getId()
                                                                                   .getId()))
                    .findFirst()
                    .ifPresent(wd -> {
                      WizardDeviceTypeId idWd = wd.getId();
                      _log.info("Add {} to LinkedHashSet: {}", idWd, deviceTypeIds);
                      deviceTypeIds.add(idWd);
                    });
      }
    }
  }

  private boolean isNotPresentDevice(Set<WizardDeviceTypeId> deviceTypeIds, String deviceTrackycard) {
    boolean isNotPresent = true;

    for (WizardDeviceTypeId wizardDeviceTypeId : deviceTypeIds) {
      if (wizardDeviceTypeId.getId()
                            .equals(DEVICE_TRACKYCARD)) {
        isNotPresent = false;
        break;
      }
    }

    return isNotPresent;
  }

  private boolean checkCondition02(final WizardDeviceTypeId _wizardDeviceTypeId, final WizardVehicle _wizardVehicle) {
    _log.debug("Condition:   2 [vehicle {} deviceType {} - has ongoing requests?]", _wizardVehicle.getId(), _wizardDeviceTypeId);
    final long time2s = System.currentTimeMillis();
    boolean hasOngoingRequest = wizardVehicleService.hasOngoingRequest(_wizardVehicle, _wizardDeviceTypeId);
    final long time2e = System.currentTimeMillis();
    _log.debug("Condition:   2 [vehicle {} deviceType {} - has ongoing requests: {}] execution time {} ms", _wizardVehicle.getId(),
               _wizardDeviceTypeId, hasOngoingRequest, time2e - time2s);
    return hasOngoingRequest;
  }

  private WizardStep4Matrix createMatrixStep4(final WizardStep4Matrix _wizardStep4Matrix, final Set<WizardVehicleId> _wizardVehicleIds,
                                              final Set<WizardDeviceTypeId> _deviceTypeIds, final WizardServiceTypeId _wizardServiceTypeId,
                                              final String codiceCliente) {
    long t1 = System.currentTimeMillis();
    Set<WizardStep4MatrixVehicle> wizardMatrixVehicles = _wizardVehicleIds.parallelStream()
                                                                          .map(wizardVehicleId -> {
                                                                            return vehicleRepository.find(wizardVehicleId.getId());
                                                                          })
                                                                          .filter(optionalWizardVehicle -> optionalWizardVehicle.isPresent())
                                                                          .map(optionalWizardVehicle -> optionalWizardVehicle.get())
                                                                          .map(wizardVehicle -> {
                                                                            _log.debug("Building step4WizardMatrix, found vehicle in the vehicleRepository: {}",
                                                                                       wizardVehicle);
                                                                            final Map<String, WizardStep4MatrixDeviceType> wizardMatrixDeviceTypesMap = _deviceTypeIds.parallelStream()
                                                                                                                                                                      .collect(toMap(wizardDeviceTypeId -> wizardDeviceTypeId.getId(),
                                                                                                                                                                                     wizardDeviceTypeId -> newStep4WizardMatrixDeviceType(_wizardServiceTypeId,
                                                                                                                                                                                                                                          wizardDeviceTypeId,
                                                                                                                                                                                                                                          wizardVehicle,
                                                                                                                                                                                                                                          codiceCliente)));

                                                                            return mapToWizardStep4MatrixVehicle(wizardVehicle,
                                                                                                                 wizardMatrixDeviceTypesMap);
                                                                          })
                                                                          .collect(toSet());
    _log.debug("Execution time to CreateMatrixStep4 : {} ms", (System.currentTimeMillis() - t1));
    _wizardStep4Matrix.getVehicles()
                      .addAll(wizardMatrixVehicles);

    _log.debug("State for WizardServiceTypeId {} and WizardVehicleIds {}: {}", _wizardServiceTypeId, _wizardVehicleIds, _wizardStep4Matrix);

    return _wizardStep4Matrix;
  }

  private void createMatrixStep4(final WizardStep4Matrix _wizardStep4Matrix, final WizardVehicle _wizardVehicle,
                                 final Set<WizardDeviceTypeId> _deviceTypeIds, final String codiceCliente) {
    _log.trace("createMatrixStep4 _wizardStep4Matrix:=[{}], _wizardVehicle:=[{}], _deviceTypeIds:=[{}], codiceCliente:=[{}] ",
               _wizardStep4Matrix, _wizardVehicle, _deviceTypeIds, codiceCliente);

    final Map<String, WizardStep4MatrixDeviceType> wizardMatrixDeviceTypesMap = _deviceTypeIds.stream()
                                                                                              .collect(toMap(wizardDeviceTypeId -> wizardDeviceTypeId.getId(),
                                                                                                             wizardDeviceTypeId -> newStep4WizardMatrixDeviceType(null,
                                                                                                                                                                  wizardDeviceTypeId,
                                                                                                                                                                  _wizardVehicle,
                                                                                                                                                                  codiceCliente)));
    _log.trace("wizardMatrixDeviceTypesMap:=[{}]", wizardMatrixDeviceTypesMap);
    WizardStep4MatrixVehicle wizardVehicleMatrix = mapToWizardStep4MatrixVehicle(_wizardVehicle, wizardMatrixDeviceTypesMap);
    _log.trace("wizardVehicleMatrix:=[{}]", wizardVehicleMatrix);

    _log.trace("BEFORE ADD _wizardStep4Matrix.getVehicles():=[{}]", _wizardStep4Matrix.getVehicles());
    _wizardStep4Matrix.getVehicles()
                      .add(wizardVehicleMatrix);
    _log.trace("AFTER ADD _wizardStep4Matrix.getVehicles():=[{}]", _wizardStep4Matrix.getVehicles());
  }

  private WizardStep4Matrix createMatrixStep4(final WizardStep4Matrix _wizardStep4Matrix, final Set<WizardVehicle> _wizardVehicles,
                                              final Set<WizardDeviceTypeId> _deviceTypeIds, final String codiceCliente) {

    final long time1g = System.currentTimeMillis();

    Set<WizardStep4MatrixVehicle> wizardVehiclesMatrix = _wizardVehicles.parallelStream()
                                                                        .map(wizardVehicle -> {
                                                                          _log.debug("Building step4WizardMatrix, found vehicle in the vehicleRepository: {}",
                                                                                     wizardVehicle);
                                                                          final long time1 = System.currentTimeMillis();
                                                                          final Map<String, WizardStep4MatrixDeviceType> wizardMatrixDeviceTypesMap = _deviceTypeIds.stream()
                                                                                                                                                                    .collect(toMap(wizardDeviceTypeId -> wizardDeviceTypeId.getId(),
                                                                                                                                                                                   wizardDeviceTypeId -> newStep4WizardMatrixDeviceType(null,
                                                                                                                                                                                                                                        wizardDeviceTypeId,
                                                                                                                                                                                                                                        wizardVehicle,
                                                                                                                                                                                                                                        codiceCliente)));
                                                                          final long time2 = System.currentTimeMillis();
                                                                          _log.debug("Step4WizardMatrix for wizardVehicleId {} [execution time {} ms]",
                                                                                     wizardVehicle.getId(), time2 - time1);
                                                                          return mapToWizardStep4MatrixVehicle(wizardVehicle,
                                                                                                               wizardMatrixDeviceTypesMap);
                                                                        })
                                                                        .collect(toSet());

    _wizardStep4Matrix.getVehicles()
                      .addAll(wizardVehiclesMatrix);

    final long time2g = System.currentTimeMillis();

    _log.debug("Matrix Step 4: {} WizardVehicles {} is completed in {} ms", wizardVehiclesMatrix.size(), _wizardVehicles.size(),
               time2g - time1g);
    return _wizardStep4Matrix;
  }

  private Optional<RuleOutcome> executeRule(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId,
                                            final WizardDeviceTypeId _wizardDeviceTypeId) {

    Optional<RuleOutcome> ruleOutcome = null;
    ruleOutcome = getVehiclePrecondition(_wizardVehicleId.getId());
    if (ruleOutcome.isPresent() && ruleOutcome.get()
                                              .getOutcome()) {

      ruleOutcome = getVehicleDevicePrecondition(_wizardDeviceTypeId.getId(), _wizardVehicleId.getId());
      if (ruleOutcome.isPresent() && ruleOutcome.get()
                                                .getOutcome()) {
        if (_wizardServiceTypeId != null) {
          ruleOutcome = getVehicleDeviceServicePrecondition(_wizardServiceTypeId.getId(), _wizardDeviceTypeId.getId(),
                                                            _wizardVehicleId.getId());
        }
      }
    }
    return ruleOutcome;
  }

  private Optional<RuleOutcome> getVehicleDeviceServicePrecondition(String serviceId, String deviceId, String vehicleId) {
    return ruleService.execute(RuleEngineServiceType.as(serviceId)
                                                    .getId(),
                               RuleEngineDeviceType.as(deviceId)
                                                   .getId(),
                               RuleEngineVehicle.as(vehicleId)
                                                .getId());
  }

  private Optional<RuleOutcome> getVehicleDevicePrecondition(String deviceId, String vehicleId) {
    return ruleService.execute(RuleEngineDeviceType.as(deviceId)
                                                   .getId(),
                               RuleEngineVehicle.as(vehicleId)
                                                .getId());
  }

  private Optional<RuleOutcome> getVehiclePrecondition(String vehicleId) {
    return ruleService.execute(RuleEngineVehicle.as(vehicleId)
                                                .getId());
  }

  private WizardStep4MatrixVehicle mapToWizardStep4MatrixVehicle(final WizardVehicle wizardVehicle,
                                                                 final Map<String, WizardStep4MatrixDeviceType> _wizardMatrixDeviceTypesMap) {
    final WizardStep4MatrixVehicle vehicle = new WizardStep4MatrixVehicle(wizardVehicle.getId()
                                                                                       .getId());
    vehicle.setType(wizardVehicle.getType());
    vehicle.setEuroClass(wizardVehicle.getEuroClass());
    vehicle.setLicensePlate(wizardVehicle.getLicensePlate());
    vehicle.setTargaNazione(wizardVehicle.getCountry());
    vehicle.setHasDevicesActive(wizardVehicle.isHasDevicesActive());
    vehicle.setExpiring(wizardVehicle.isExpiring());
    vehicle.setExpired(wizardVehicle.isExpired());
    vehicle.getDeviceTypes()
           .putAll(_wizardMatrixDeviceTypesMap);
    return vehicle;
  }

  private WizardStep4MatrixDeviceType newStep4WizardMatrixDeviceType(final WizardServiceTypeId _wizardServiceTypeId,
                                                                     final WizardDeviceTypeId _wizardDeviceTypeId,
                                                                     final WizardVehicle _wizardVehicle, final String codiceCliente) {
    long t1 = System.currentTimeMillis();
    final WizardStep4MatrixDeviceType deviceType = new WizardStep4MatrixDeviceType(_wizardDeviceTypeId.getId());
    deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));

    if (!_wizardVehicle.isLoadDevices()) {
      long tReload1 = System.currentTimeMillis();
      _wizardVehicle.getDevices()
                    .addAll(this.deviceRepository.findByWizardVehicleId(_wizardVehicle.getId()));
      _wizardVehicle.setLoadDevices();
      _log.debug("Reload device on vehicle: {} in {} ms", _wizardVehicle.getId(), System.currentTimeMillis() - tReload1);
    }

    boolean hasDevicesActive = _wizardVehicle.isHasDevicesActive();
    boolean expired = _wizardVehicle.isExpired();
    if (hasDevicesActive && expired) {
      String identifierVehicle = _wizardVehicle.getId()
                                               .getId();
      _log.info("wizardVehicle [ {} - {} - {} ] hasDevicesActive [{}] and isExpired [{}]", identifierVehicle,
                _wizardVehicle.getLicensePlate(), _wizardVehicle.getCountry(), hasDevicesActive, expired);

      WizardMatrixActivability wizardMatrixActivability = new WizardMatrixActivability(ActivabilityState.VEHICLE_ANOMALY);
      WizardPreconditionCode possessoAnomaly = WizardPreconditionCode.POSSESSO_ANOMALY;
      WizardMatrixActivabilityFailure step4WizardMatrixActivabilityFailure = new WizardMatrixActivabilityFailure(possessoAnomaly.code(),
                                                                                                                 possessoAnomaly.codeName());
      wizardMatrixActivability.addFailure(step4WizardMatrixActivabilityFailure);
      deviceType.setActivability(wizardMatrixActivability);
      _log.debug("Return {} because wizardVehicle {} hasDevicesActive and is expired", deviceType, identifierVehicle);
      return deviceType;
    }

    List<WizardDevice> devices = _wizardVehicle.getDevices()
                                               .stream()
                                               .filter(deviceF -> deviceF.getType()
                                                                         .getId()
                                                                         .getId()
                                                                         .equals(_wizardDeviceTypeId.getId()))
                                               .collect(Collectors.toList());

    if (devices != null && devices.size() == 1 && devices.stream()
                                                         .filter(d -> d.getCodiceCliente() != null && !d.getCodiceCliente()
                                                                                                        .equals(codiceCliente))
                                                         .findFirst()
                                                         .isPresent()) {
      WizardMatrixActivabilityFailure wizardMatrixActivabilityFailure = new WizardMatrixActivabilityFailure(WizardPreconditionCode.VEHICLE_CLIENT_ANOMALY.code(),
                                                                                                            WizardPreconditionCode.VEHICLE_CLIENT_ANOMALY.codeName());
      deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE));
      deviceType.getActivability()
                .addFailure(wizardMatrixActivabilityFailure);
      _log.debug("Execution when there is a Vehicle Client anomaly in {} ms", System.currentTimeMillis() - t1);
      return deviceType;
    }
    boolean hasAnomalies = devices != null && devices.size() > 1;
    boolean hasDeviceActive = devices != null && devices.size() == 1 && devices.get(0) != null;
    boolean hasAllActiveServices = false;

    _log.debug("hasDeviceActive = {}", hasDeviceActive);
    if (devices != null && devices.size() > 1 && devices.get(0) != null) {
      deviceType.setActivationDate(devices.get(0)
                                          .getActivationDate());
      deviceType.setSeriale(devices.get(0)
                                   .getSeriale());
    }
    _log.debug("deviceTypeDefined : {}", deviceType);
    if (hasDeviceActive) {
      String deviceName = devices.get(0)
                                 .getType()
                                 .getId()
                                 .getId();
      boolean isEmptyRemoteConfig = true;
      try {
        TipoDispositivoEnum deviceTypeName = TipoDispositivoEnum.valueOf(deviceName);
        isEmptyRemoteConfig = configDevicesService.findRemoteConfigurations(deviceTypeName)
                                                  .isEmpty();
      } catch (Exception e) {
        _log.warn("Exception to cast device: {}", deviceName);
      }
      if (isEmptyRemoteConfig) {
        if (_wizardDeviceTypeId.getActivableServicesNumber() > 1) {

          hasAllActiveServices = devices.get(0)
                                        .getServices()
                                        .values()
                                        .stream()
                                        .filter(service -> service.isActive())
                                        .count() == _wizardDeviceTypeId.getActivableServicesNumber();
        } else {
          hasAllActiveServices = true;
        }
      } else {
        hasAllActiveServices = false;
      }
    }

    boolean hasDeviceActiveAndAnyServices = hasDeviceActive && !hasAllActiveServices;
    boolean hasOngoingRequest = hasDeviceActive && checkCondition02(_wizardDeviceTypeId, _wizardVehicle);
    if (hasOngoingRequest) {
      deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ONGOING_REQUEST));
    } else if (hasAllActiveServices) {
      deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVE_ON_ALL));
    } else if (hasAnomalies) {
      deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE));
      deviceType.getActivability()
                .addFailure(new WizardMatrixActivabilityFailure(WizardPreconditionCode.ANOMALY_VEHICLE.code(),
                                                                WizardPreconditionCode.ANOMALY_VEHICLE.codeName()));
    } else if (hasDeviceActiveAndAnyServices) {
      deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVE));
    } else {
      final Optional<RuleOutcome> ruleOutcome = executeRule(_wizardServiceTypeId, _wizardVehicle.getId(), _wizardDeviceTypeId);
      _log.debug("Precondition Vehicle - Device: {}", ruleOutcome);
      boolean isPresentRuleOutcome = ruleOutcome.isPresent();
      boolean preconditionsAreOk = isPresentRuleOutcome && ruleOutcome.get()
                                                                      .getOutcome();

      if (!preconditionsAreOk && isPresentRuleOutcome) {
        deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ANOMALY));
        WizardMatrixActivabilityFailure defaultActivabilityFailure = new WizardMatrixActivabilityFailure(WizardPreconditionCode.DEFAULT.code(),
                                                                                                         WizardPreconditionCode.DEFAULT.codeName());
        Optional<RuleFailure> failure = ruleOutcome.get()
                                                   .getFailure();
        if (failure.isPresent()) {
          RuleFailure ruleFailure = failure.get();
          _log.debug("RuleFailure found to Vehicle {}: {}", _wizardVehicle.getId(), ruleFailure);
          defaultActivabilityFailure = new WizardMatrixActivabilityFailure(ruleFailure.decode(), ruleFailure.getMess());
          // Check if the code of ruleFailure reference vehicle errors;
          if (ruleFailure.isVehicleAnomaly()) {
            deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.VEHICLE_ANOMALY));
          }

          if (ruleFailure.isTesseraCaronteNotActivable()) {
            deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE));
          }
        } else {
          _log.warn("Not found failure on ruleOutCome to Vehicle: {} Device: {}", _wizardVehicle.getId(), _wizardDeviceTypeId.getId());
        }
        deviceType.getActivability()
                  .addFailure(defaultActivabilityFailure);
      } else if (!ruleOutcome.isPresent()) {
        deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE));
        deviceType.getActivability()
                  .addFailure(new WizardMatrixActivabilityFailure(WizardPreconditionCode.ANOMALY_INFINISPAN_VEHICLE.code(),
                                                                  WizardPreconditionCode.ANOMALY_INFINISPAN_VEHICLE.codeName()));
        _log.error("Precalculated RuleOutcome not found for {} {} {}", _wizardServiceTypeId, _wizardVehicle.getId(), _wizardDeviceTypeId);
      } else if (hasDeviceActive) {
        deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVE));
      } else {
        deviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
      }
    }

    String vehicle = (_wizardVehicle != null) ? _wizardVehicle.getId()
                                                              .getId()
                                              : null;
    String device = (_wizardDeviceTypeId != null) ? _wizardDeviceTypeId.getId() : null;
    String service = (_wizardServiceTypeId != null) ? _wizardServiceTypeId.getId() : null;
    _log.debug("Exceution rule vehicle: {} Device: {} Service: {}  in {} ms", vehicle, device, service, System.currentTimeMillis() - t1);
    return deviceType;
  }

}
