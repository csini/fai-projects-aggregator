package it.fai.ms.efservice.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;
import it.fai.ms.efservice.repository.TipoHardwareDispositivoRepository;
import it.fai.ms.efservice.service.TipoHardwareDispositivoService;

@Service
@Transactional
public class TipoHardwareDispositivoServiceImpl implements TipoHardwareDispositivoService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final TipoHardwareDispositivoRepository hardwareTypeDeviceRepository;

  public TipoHardwareDispositivoServiceImpl(final TipoHardwareDispositivoRepository hardwareTypeDeviceRepository) {
    this.hardwareTypeDeviceRepository = hardwareTypeDeviceRepository;
  }

  @Override
  public TipoHardwareDispositivo save(TipoHardwareDispositivo hardwareTypeDevice) {
    log.debug("Save: {}", hardwareTypeDevice);
    TipoHardwareDispositivo savedEntity = hardwareTypeDeviceRepository.save(hardwareTypeDevice);
    log.debug("Saved: {}", savedEntity);
    return savedEntity;
  }
  
  @Override
  public TipoHardwareDispositivo findOne(Long id) {
    return hardwareTypeDeviceRepository.findOne(id);
  }

  @Override
  public List<TipoHardwareDispositivo> findActiveByDeviceType(TipoDispositivoEnum deviceTypeEnum) {
    log.debug("Found hardware types device by device type: {}", deviceTypeEnum);
    List<TipoHardwareDispositivo> hardwareTypesDevice = hardwareTypeDeviceRepository.findByTipoDispositivo_nomeAndAttivoTrue(deviceTypeEnum);
    log.debug("Found hardware types device: {}", hardwareTypesDevice != null ? hardwareTypesDevice.size() : 0);
    return hardwareTypesDevice;
  }

  @Override
  public List<TipoHardwareDispositivo> findActiveByDeviceTypeId(Long idDeviceType) {
    log.debug("Found hardware types device by device type ID: {}", idDeviceType);
    List<TipoHardwareDispositivo> hardwareTypesDevice = hardwareTypeDeviceRepository.findByTipoDispositivo_idAndAttivoTrue(idDeviceType);
    log.debug("Found hardware types device: {}", hardwareTypesDevice != null ? hardwareTypesDevice.size() : 0);
    return hardwareTypesDevice;
  }

  @Override
  public List<TipoHardwareDispositivo> findAll() {
    return hardwareTypeDeviceRepository.findAll();
  }
  
  @Override
  public Optional<TipoHardwareDispositivo> findByTipoDispositivo_nomeAndHardwareType(TipoDispositivoEnum tipoDispositivo, String tipoHardware) {
    return hardwareTypeDeviceRepository.findByTipoDispositivo_nomeAndHardwareTypeAndAttivoTrue(tipoDispositivo, tipoHardware);
  }
  
  @Override
  public List<TipoHardwareDispositivo> findByDeviceType(TipoDispositivoEnum deviceType) {
    return hardwareTypeDeviceRepository.findByTipoDispositivo_nome(deviceType);
  }

  @Override
  public List<TipoHardwareDispositivo> findByDeviceTypeId(Long tipoDispositivoId) {
    return hardwareTypeDeviceRepository.findByTipoDispositivo_id(tipoDispositivoId);
  }

  @Override
  public void deleteAll() {
    hardwareTypeDeviceRepository.deleteAll();
  }

  @Override
  public void delete(Long id) {
    hardwareTypeDeviceRepository.delete(id);
  }

}
