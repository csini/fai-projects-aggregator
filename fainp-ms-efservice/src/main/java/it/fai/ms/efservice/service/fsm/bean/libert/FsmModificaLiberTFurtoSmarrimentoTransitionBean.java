package it.fai.ms.efservice.service.fsm.bean.libert;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.libert.FsmModificaLiberTFurtoSmarrimento;

@WithStateMachine(id = FsmModificaLiberTFurtoSmarrimento.FSM_MODIFICA_LIBER_T_FURTO_SMARRIMENTO)
public class FsmModificaLiberTFurtoSmarrimentoTransitionBean extends AbstractFsmRichiestaTransition {
}
