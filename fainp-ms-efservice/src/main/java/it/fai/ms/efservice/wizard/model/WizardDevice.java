package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class WizardDevice implements Serializable {

  private static final long serialVersionUID = -2924552043637025321L;

  private Instant                             activationDate;
  private boolean                             hasOngoingRequest;
  private WizardDeviceId                      id;
  private Map<WizardServiceId, WizardService> services = new HashMap<>();
  private WizardDeviceType                    type;
  private ActivabilityState                   state;
  private String                              seriale;
  private boolean                             hidden   = false;
  private String codiceCliente;

  public WizardDevice(final WizardDeviceId _wizardDeviceId) {
    id = _wizardDeviceId;
  }

  public void addService(WizardService _wizardService) {
    this.services.put(_wizardService.getId(), _wizardService);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((WizardDevice) _obj).getId(), id);
    }
    return isEquals;
  }

  public Instant getActivationDate() {
    return activationDate;
  }

  public WizardDeviceId getId() {
    return id;
  }

  public Map<WizardServiceId, WizardService> getServices() {
    return services;
  }

  public WizardDeviceType getType() {
    return type;
  }

  public boolean hasActiveService(final WizardServiceTypeId _wizardServiceTypeId) {
    return services.values()
                   .stream()
                   .filter(wizardService -> wizardService.getType()
                                                         .getId()
                                                         .equals(_wizardServiceTypeId))
                   .anyMatch(wizardService -> wizardService.isActive());
  }

  public boolean hasActiveServices() {
    return services.values()
                   .stream()
                   .anyMatch(wizardService -> wizardService.isActive());
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  public boolean hasOnGoingRequest() {
    return hasOngoingRequest;
  }

  public boolean isOfTypeId(final WizardDeviceTypeId _wizardDeviceTypeId) {
    return getType().getId()
                    .equals(_wizardDeviceTypeId);
  }

  public void setActivationDate(final Instant _activationDate) {
    this.activationDate = _activationDate;
  }

  public void setOngoingRequest() {
    hasOngoingRequest = true;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public void setType(final WizardDeviceType _type) {
    type = _type;
  }

  public void setHidden(boolean _hidden) {
    hidden = _hidden;
  }

  public boolean isHidden() {
    return hidden;
  }
  
  public ActivabilityState getState() {
    return state;
  }

  public void setState(ActivabilityState state) {
    this.state = state;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardDevice [activationDate=");
    builder.append(activationDate);
    builder.append(", hasOngoingRequest=");
    builder.append(hasOngoingRequest);
    builder.append(", id=");
    builder.append(id);
    builder.append(", services=");
    builder.append(services);
    builder.append(", type=");
    builder.append(type);
    builder.append(", hidden=");
    builder.append(hidden);
    builder.append(", codiceCliente=");
    builder.append(codiceCliente);
    builder.append("]");
    return builder.toString();
  }

}
