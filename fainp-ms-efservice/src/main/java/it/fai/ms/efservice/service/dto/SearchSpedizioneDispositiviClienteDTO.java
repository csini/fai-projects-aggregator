package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public class SearchSpedizioneDispositiviClienteDTO implements Serializable {

  private static final long serialVersionUID = -4115044845426736234L;

  private String codiceAzienda;

  private String ragioneSociale;

  private String cerca;

  private String targa;

  private String seriale;

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCerca() {
    return cerca;
  }

  public void setCerca(String cerca) {
    this.cerca = cerca;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SearchSpedizioneDispositiviClienteDTO [codiceAzienda=");
    builder.append(codiceAzienda);
    builder.append(", ragioneSociale=");
    builder.append(ragioneSociale);
    builder.append(", cerca=");
    builder.append(cerca);
    builder.append(", targa=");
    builder.append(targa);
    builder.append(", seriale=");
    builder.append(seriale);
    builder.append("]");
    return builder.toString();
  }

}
