package it.fai.ms.efservice.service.jms.producer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessageFactory;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ObuType;
import it.fai.ms.common.jms.efservice.message.device.DeviceReturnMessage;
import it.fai.ms.common.jms.efservice.message.device.ObuDeviceInfo;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@Service
@Transactional
public class DeviceReturnMessageProducerServiceImpl
  implements DeviceReturnMessageProducerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  private final JmsSenderQueueMessageService jmsSenderQueueService;

  @Autowired
  public DeviceReturnMessageProducerServiceImpl(final JmsProperties _jmsProperties,
                                                final JmsSenderQueueMessageService _jmsSenderQueueService) {
    jmsProperties = _jmsProperties;
    jmsSenderQueueService = _jmsSenderQueueService;
  }

  @Override
  public void deactivateDevice(final Richiesta _orderRequestEntity) {
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(_orderRequestEntity);

    DeviceReturnMessage deviceReturnMessage = mapTo(orderRequest);
    // deviceReturnMessage.getServices()
    // .addAll(DeviceService.fromDeviceServiceNames(orderRequest.findActiveServicesNames()));

    _log.info("Message produced : {}", deviceReturnMessage);

    try {
      JmsTopicSenderUtil.publish(jmsProperties, JmsTopicNames.DEVICE_RETURN, deviceReturnMessage);
    } catch (final Exception _e) {
      _log.error("Message not published {}", deviceReturnMessage, _e);
    }
  }

  private DeviceReturnMessage mapTo(final OrderRequestDecorator _orderRequest) {
    return new DeviceReturnMessage(new ContractOrderRequest(_orderRequest.findNumeroContract(),_orderRequest.findCodiceCliente()),
                                   new ObuDeviceInfo(new ObuNumber(_orderRequest.getDeviceSerialId()),
                                                     ObuType.fromString(_orderRequest.getHardwareDeviceType())));
  }

  @Override
  public void deactivateDeviceTelepassIta(final Richiesta richiesta) {
    DeviceEventMessage eventMesage = buildDeviceEventMessage(richiesta);
    jmsSenderQueueService.sendDeviceReturnMessage(eventMesage);
  }

  private DeviceEventMessage buildDeviceEventMessage(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    OrderRequestDecorator orderRequest = new OrderRequestDecorator(richiesta);

    DeviceEventMessage deviceEventMessage = DeviceEventMessageFactory.createDeviceReturnMessage(
      new DeviceDataDTO()
        .contractCode(orderRequest.findNumeroContract())
        .requestId(orderRequest.getIdentificativo())
        .deviceCode(orderRequest.getDeviceSerialId())
        .deviceType(orderRequestUtil.getObuType().getId().name())
    );
    return deviceEventMessage;
  }

}
