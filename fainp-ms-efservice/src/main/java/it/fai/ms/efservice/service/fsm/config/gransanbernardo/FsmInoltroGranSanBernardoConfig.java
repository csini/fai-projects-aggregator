package it.fai.ms.efservice.service.fsm.config.gransanbernardo;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActiveDevice;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGenerateContratto;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardGranSanBernardoInDepositoKo;
import it.fai.ms.efservice.service.fsm.guard.GuardGranSanBernardoInDepositoOk;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtFai;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroGranSanBernardoConfig.INOLTRO_TES_GRAN_SANBERNARDO)
public class FsmInoltroGranSanBernardoConfig extends  FsmRichiestaConfig {
  
  public static final String INOLTRO_TES_GRAN_SANBERNARDO = "inoltroGranSanBernardo";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  private final ManageDeviceSentModeService manageDeviceSentModeService;
  
  private final ManageDevicesInStorageService deviceInStorageServiceExt;

  private final DeviceProducerService deviceProducerService;
  
  private final ApplicationEventPublisher applicationEventPublisher;
  
  private final GeneratorContractCodeService generatorContractCodeService;


  public FsmInoltroGranSanBernardoConfig(final FsmSenderToQueue _senderFsmService,
                                         final ManageDevicesInStorageService _deviceInStorageServiceExt,
                                         final ManageDeviceSentModeService manageDeviceSentModeService,
                                         final ApplicationEventPublisher _applicationEventPublisher,
                                         final DeviceProducerService deviceProducerService,
                                         final GeneratorContractCodeService generatorContractCodeService) {
    super();
    this.senderFsmService = _senderFsmService;
    this.deviceInStorageServiceExt = _deviceInStorageServiceExt;
    this.manageDeviceSentModeService = manageDeviceSentModeService;
    this.applicationEventPublisher = _applicationEventPublisher;
    this.deviceProducerService = deviceProducerService;
    this.generatorContractCodeService = generatorContractCodeService;
  }
  
  @Override
  protected Logger getLogger() {
    return log;
  }
  
  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_GRAN_SAN_BERNARDO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroGranSanBernardo();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
    .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.VERIFICA_TTGSB_IN_DEPOSITO)
    .action(new FsmActionGenerateContratto(generatorContractCodeService))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.VERIFICA_TTGSB_IN_DEPOSITO)
    .target(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO)
    .guard(new GuardGranSanBernardoInDepositoKo(deviceInStorageServiceExt))
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// MANUAL
    .source(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO)
    .target(StatoRichiesta.VERIFICA_TTGSB_IN_DEPOSITO)
    .event(RichiestaEvent.MU_DISPOSITIVO_IN_DEPOSITO)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.VERIFICA_TTGSB_IN_DEPOSITO)
    .target(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
    .guard(new GuardGranSanBernardoInDepositoOk(deviceInStorageServiceExt, applicationEventPublisher))
    .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.INOLTRATO))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
    .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
    .guard(new GuardIsToBeSentAtFai(manageDeviceSentModeService))
    .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ACCETTATO))
    .and()
    .withExternal()// MANUAL
    .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
    .target(StatoRichiesta.ATTIVO_EVASO)
    .event(RichiestaEvent.MU_SPEDIZIONE_COMPLETA)
    .action(new FsmActionActiveDevice(senderFsmService, deviceProducerService, DispositivoEvent.SPEDITO_DA_FAI))
    .and()
    .withExternal()// MANUAL
    .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
    .target(StatoRichiesta.CONCLUSO_NON_SPEDITO)
    .event(RichiestaEvent.MU_CHIUSURA_ORDINE)
    .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.REVOCA));
    
  }

}
