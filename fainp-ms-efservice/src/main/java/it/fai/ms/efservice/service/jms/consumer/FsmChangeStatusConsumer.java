package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.dto.FsmCommandDTO;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class FsmChangeStatusConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils fsmRichiestaUtils;

  public FsmChangeStatusConsumer(final FsmRichiestaUtils fsmRichiestaUtils) {
    this.fsmRichiestaUtils = fsmRichiestaUtils;
  }

  public void consume(FsmCommandDTO dto) throws FsmExecuteCommandException {
    log.info("Consume message: {}", dto);
    String identificativo = dto.getIdentificativo();
    String operazione = dto.getOperazione();
    String errorMessage = dto.getErrorMessage();

    log.info("Search Richiesta {} to change status by command {}", identificativo, operazione);
    Richiesta richiesta = fsmRichiestaUtils.findRichiesta(identificativo);
    richiesta = fsmRichiestaUtils.changeStatusToRichiesta(richiesta, operazione, errorMessage, null);
    log.info("Richiesta updated: [{} - {}]", identificativo, (richiesta != null) ? richiesta.getStato() : null);
  }

}
