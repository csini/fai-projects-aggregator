package it.fai.ms.efservice.vehicle.ownership.service;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnership;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipCompanyCode;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipDevice;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipServiceTypeId;
import it.fai.ms.efservice.vehicle.ownership.repository.VehicleOwnershipDeviceRepository;

@Service
public class VehicleOwnershipServiceImpl implements VehicleOwnershipService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  @Value("${efservice.authorizationHeader}")
  private String authorizationToken;

  private VehicleOwnershipDeviceRepository vehicleOwnershipDeviceRepository;
  private PrecalculatedRuleService         precalculatedRuleService;
  private VehicleClient                    vehicleClient;

  @Autowired
  public VehicleOwnershipServiceImpl(final VehicleOwnershipDeviceRepository _vehicleOwnershipDeviceTypeRepository,
                                     final PrecalculatedRuleService _precalculatedRuleService, final VehicleClient _vehicleClient) {
    vehicleOwnershipDeviceRepository = _vehicleOwnershipDeviceTypeRepository;
    precalculatedRuleService = _precalculatedRuleService;
    vehicleClient = _vehicleClient;
  }
  
  @Override
  public Set<VehicleOwnership> getEnabledVehicles(final VehicleOwnershipCompanyCode _vehicleOwnershipCompanyCode,
                                                  final VehicleOwnershipId _vehicleOwnershipId,String _deviceType) {
	  
	  TipoDispositivoEnum tipo = TipoDispositivoEnum.valueOf(_deviceType);
	  Set<TipoDispositivoEnum> tipiDispositivo = new HashSet<>(Arrays.asList(tipo));
	  
	  final Set<VehicleOwnershipDevice> devices = vehicleOwnershipDeviceRepository.findByVehicleOwnershipId(_vehicleOwnershipId,tipiDispositivo);

	    String codiceAzienda = _vehicleOwnershipCompanyCode.getId();
	    List<VehicleDTO> vehiclesDTO = vehicleClient.findAllByCodiceAzienda(authorizationToken, codiceAzienda);
	    Set<VehicleOwnership> vehiclesOwnerShip = mapToVehicleOwnerShip(vehiclesDTO, _vehicleOwnershipCompanyCode);
	    Set<VehicleOwnership> vehicleOwnerships = vehiclesOwnerShip.parallelStream()
	                                                               .filter(vehicleOwnership -> theIdIsDifferent(_vehicleOwnershipId,
	                                                                                                            vehicleOwnership))
	                                                               .filter(vehicleOwnership -> theRulesOutcomeIsPositive(vehicleOwnership,
	                                                                                                                     devices))
	                                                               .collect(toSet());
	    return vehicleOwnerships;
	  
  }

  @Override
  public Set<VehicleOwnership> getEnabledVehicles(final VehicleOwnershipCompanyCode _vehicleOwnershipCompanyCode,
                                                  final VehicleOwnershipId _vehicleOwnershipId) {
    final Set<VehicleOwnershipDevice> devices = vehicleOwnershipDeviceRepository.findByVehicleOwnershipId(_vehicleOwnershipId);

    String codiceAzienda = _vehicleOwnershipCompanyCode.getId();
    List<VehicleDTO> vehiclesDTO = vehicleClient.findAllByCodiceAzienda(authorizationToken, codiceAzienda);
    Set<VehicleOwnership> vehiclesOwnerShip = mapToVehicleOwnerShip(vehiclesDTO, _vehicleOwnershipCompanyCode);
    Set<VehicleOwnership> vehicleOwnerships = vehiclesOwnerShip.parallelStream()
                                                               .filter(vehicleOwnership -> theIdIsDifferent(_vehicleOwnershipId,
                                                                                                            vehicleOwnership))
                                                               .filter(vehicleOwnership -> theRulesOutcomeIsPositive(vehicleOwnership,
                                                                                                                     devices))
                                                               .collect(toSet());
    return vehicleOwnerships;
  }

  private Set<VehicleOwnership> mapToVehicleOwnerShip(List<VehicleDTO> vehiclesDTO,
                                                      VehicleOwnershipCompanyCode _vehicleOwnershipCompanyCode) {
    return vehiclesDTO.stream()
                      .map(dto -> new VehicleOwnership(dto.getIdentificativo(), dto.getTarga(), _vehicleOwnershipCompanyCode))
                      .collect(toSet());
  }

  private boolean theRulesOutcomeIsPositive(final VehicleOwnership _vehicleOwnership, final Set<VehicleOwnershipDevice> _devices) {
    boolean allMatch = _devices.stream()
                               .allMatch(device -> device.getServiceTypeIds()
                                                         .stream()
                                                         .allMatch(serviceTypeId -> executeRule(_vehicleOwnership, device,
                                                                                                serviceTypeId).orElse(new RuleOutcome())
                                                                                                              .getOutcome()));

    _log.debug("VehicleOwnership {} rules outcome is {}", _vehicleOwnership, allMatch);
    return allMatch;
  }

  private Optional<RuleOutcome> executeRule(final VehicleOwnership _vehicleOwnership, VehicleOwnershipDevice device,
                                            VehicleOwnershipServiceTypeId serviceTypeId) {
    return precalculatedRuleService.execute(new RuleEngineServiceTypeId(serviceTypeId.getId()),
                                            new RuleEngineDeviceTypeId(device.getDeviceTypeId()
                                                                             .getId()),
                                            new RuleEngineVehicleId(_vehicleOwnership.getUuid()));
  }

  private boolean theIdIsDifferent(final VehicleOwnershipId _vehicleOwnershipId, final VehicleOwnership vehicleOwnership) {
    boolean theIdIsDifferent = !vehicleOwnership.getUuid()
                                                .equals(_vehicleOwnershipId.getId());
    if (!theIdIsDifferent) {
      _log.info("VehicleOwnership will be skipped (is the same from input): {}", vehicleOwnership);
    }
    return theIdIsDifferent;
  }

}
