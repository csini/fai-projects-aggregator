package it.fai.ms.efservice.service.impl;

import java.util.List;

import it.fai.ms.efservice.domain.Richiesta;

public interface OrderRequestService {

  List<Richiesta> getOrderRequestsWithoutContract(String companyCodeUuid);

  Richiesta getOrderRequestsByCompanyCodeAndVehiclePlateNumber(String companyCodeUuid, String vehiclePlateNumber,
                                                               String vehiclePlateCountry);

}
