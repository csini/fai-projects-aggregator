package it.fai.ms.efservice.service.fsm.util;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.StatoDispositivo;

public class StatoDispositivoUtil {
  
  private final static Logger log = LoggerFactory.getLogger(StatoDispositivoUtil.class);
  
  public static Set<StatoDispositivo> getAllStatoDispositivo() {
    HashSet<StatoDispositivo> list = new HashSet<>();
    
    StatoDispositivo[] values = StatoDispositivo.values();
    for (int i = 0; i < values.length; i++) {
      StatoDispositivo statoDispositivo = values[i];
      list.add(statoDispositivo);
    }
    
    log.trace("Add Stati dispositivo {}", list);
    return list;
  }

}
