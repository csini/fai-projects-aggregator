package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;

public class FsmActivationDeActivationService implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceProducerService deviceProducerService;

  public FsmActivationDeActivationService(DeviceProducerService deviceProducerService) {
    this.deviceProducerService = deviceProducerService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;
      sendActivationDeActivationServices(richiesta);
    } else {
      log.warn("Not instance object {} to Richiesta", obj);
    }
  }

  public void sendActivationDeActivationServices(Richiesta richiesta) {
    if (richiesta == null) {
      log.warn("Richiesta is NULL");
      return;
    }

    TipoRichiesta tipo = richiesta.getTipo();
    log.info("Richiesta type: {}", tipo);
    if (tipo == null) {
      throw new RuntimeException("Richiesta type is null, so impossible send message to ACTIVATION/DEACTIVATION Service");
    }

    String identificativo = richiesta.getIdentificativo();
    switch (tipo) {
    case ATTIVAZIONE_SERVIZIO:
      log.debug("Activation service to Richiesta {}", identificativo);
      deviceProducerService.activateDevice(richiesta);
      break;

    case DISATTIVAZIONE_SERVIZIO:
      log.debug("Deactivation service to Richiesta {}", identificativo);
      deviceProducerService.deactivateDevice(richiesta);
      break;

    default:
      log.warn("For this type {} doesn't send message to ACTIVATION/DEACTIVATION Service...", tipo);
      break;
    }
  }

}
