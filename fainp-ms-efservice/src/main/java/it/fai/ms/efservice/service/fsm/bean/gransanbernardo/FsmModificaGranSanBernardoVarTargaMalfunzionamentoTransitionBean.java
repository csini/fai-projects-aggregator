package it.fai.ms.efservice.service.fsm.bean.gransanbernardo;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmModificaGranSanBernardoVarTargaMalfunzionamento;

@WithStateMachine(id = FsmModificaGranSanBernardoVarTargaMalfunzionamento.FSM_MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaGranSanBernardoVarTargaMalfunzionamentoTransitionBean extends AbstractFsmRichiestaTransition {
}
