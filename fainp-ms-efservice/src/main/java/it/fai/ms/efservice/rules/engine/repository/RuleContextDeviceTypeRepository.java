package it.fai.ms.efservice.rules.engine.repository;

import java.util.Set;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;

public interface RuleContextDeviceTypeRepository {

  Set<RuleEngineDeviceType> findAll();

  RuleEngineDeviceType newStarContextDeviceType();

}
