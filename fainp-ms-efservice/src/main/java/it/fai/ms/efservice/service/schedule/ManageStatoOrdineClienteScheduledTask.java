package it.fai.ms.efservice.service.schedule;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.config.ApplicationProperties;
import it.fai.ms.efservice.service.ManageStatoOrdineClienteService;

@Component
public class ManageStatoOrdineClienteScheduledTask {

  private final Logger log = LoggerFactory.getLogger(ManageStatoOrdineClienteScheduledTask.class);

  private final ManageStatoOrdineClienteService manageStatoOrdineService;

  private final ApplicationProperties applicationProperties;

  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  private Boolean enabledSchedule;

  public ManageStatoOrdineClienteScheduledTask(ManageStatoOrdineClienteService _manageStatoOrdineService,
                                               final ApplicationProperties _applicationProperties) {
    manageStatoOrdineService = _manageStatoOrdineService;
    applicationProperties = _applicationProperties;
  }

  @PostConstruct
  public void init() {
    if (applicationProperties != null) {
      enabledSchedule = applicationProperties.getScheduler()
                                             .isEnable();
    } else {
      log.error("Not initialize boolean value: enabledScheduler");
    }
  }

  // @Scheduled(cron = "*/60 * * * * MON-FRI")
  @Scheduled(cron = "${application.scheduler.ordinicron}")
  public void run() {
    long tStart = System.currentTimeMillis();
    String simpleNameClass = ManageStatoOrdineClienteScheduledTask.class.getSimpleName();
    if (enabledSchedule != null && enabledSchedule) {
      log.info("-> Starting {} at {}", simpleNameClass, formattedLocalDate());

      manageStatoOrdineService.manageOrdiniInCache();

      long tFinish = System.currentTimeMillis();
      log.info("-> Ending {} at {} in {} ms", simpleNameClass, formattedLocalDate(), (tFinish - tStart));
    } else {
      log.warn("Scheduler for change stato Ordine Cliente is enabled ? {}", enabledSchedule);
    }
  }

  @Scheduled(cron = "${application.scheduler.cacheordini}")
  public void runOrdineToCache() {
    long tStart = System.currentTimeMillis();
    String simpleNameClass = ManageStatoOrdineClienteScheduledTask.class.getSimpleName();
    if (enabledSchedule != null && enabledSchedule) {
      log.info("-> Starting put ordini in cache {} at {}", simpleNameClass, formattedLocalDate());

      manageStatoOrdineService.putOrdiniInCache();

      long tFinish = System.currentTimeMillis();
      log.info("-> Ending put ordini in cache {} at {} in {} ms", simpleNameClass, formattedLocalDate(), (tFinish - tStart));
    } else {
      log.warn("Scheduler for change stato Ordine Cliente is enabled ? {}", enabledSchedule);
    }
  }

  private String formattedLocalDate() {
    LocalDateTime date = LocalDateTime.now();
    String dateFormatted = date.format(formatter);
    return dateFormatted;
  }

}
