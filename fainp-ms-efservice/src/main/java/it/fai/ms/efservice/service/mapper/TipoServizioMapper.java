package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TipoServizio and its DTO TipoServizioDTO.
 */
@Mapper(componentModel = "spring", uses = {TipoServizioDefaultMapper.class, CategoriaServizioMapper.class, })
public interface TipoServizioMapper extends EntityMapper <TipoServizioDTO, TipoServizio> {

    @Mapping(source = "tipoServizioDefault.id", target = "tipoServizioDefaultId")

    @Mapping(source = "categoriaServizio.id", target = "categoriaServizioId")
    @Mapping(source = "categoriaServizio.nome", target = "categoriaServizioNome")
    TipoServizioDTO toDto(TipoServizio tipoServizio); 

    @Mapping(source = "tipoServizioDefaultId", target = "tipoServizioDefault")

    @Mapping(source = "categoriaServizioId", target = "categoriaServizio")
    @Mapping(target = "tipoDispositivos", ignore = true)
    TipoServizio toEntity(TipoServizioDTO tipoServizioDTO); 
    default TipoServizio fromId(Long id) {
        if (id == null) {
            return null;
        }
        TipoServizio tipoServizio = new TipoServizio();
        tipoServizio.setId(id);
        return tipoServizio;
    }
}
