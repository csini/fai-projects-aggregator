package it.fai.ms.efservice.service.fsm.action;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.common.jms.dto.RichiestaNewDispositivoDTO;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class
FsmActionFurtoSmarrimento implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(FsmActionFurtoSmarrimento.class);

  private final FsmSenderToQueue senderFsmService;

  public FsmActionFurtoSmarrimento(FsmSenderToQueue senderFsmService) {
    this.senderFsmService = senderFsmService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      TipoRichiesta tipo = richiesta.getTipo();
      DispositivoEvent command = null;
      if (tipo.equals(TipoRichiesta.FURTO) || tipo.equals(TipoRichiesta.FURTO_CON_SOSTITUZIONE)) {
        command = DispositivoEvent.BLOCCO_FURTO;
      } else if (tipo.equals(TipoRichiesta.SMARRIMENTO) || tipo.equals(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE)) {
        command = DispositivoEvent.BLOCCO_SMARRIMENTO;
      } else {
        log.error("Not found command for tipo richiesta: " + tipo.name() + " in operation FURTO/SMARRIMENTO");
        return;
      }

      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      try {
        changeStateWithCommand(dispositivi, command);
      } catch (Exception e) {
        log.error("Exception", e);
      }

      switch (tipo) {
      case FURTO_CON_SOSTITUZIONE:
      case SMARRIMENTO_CON_SOSTITUZIONE:
        createNewOrder(richiesta);
        break;

      default:
        break;
      }
    }
  }

  private void changeStateWithCommand(Set<Dispositivo> dispositivi, DispositivoEvent event) throws Exception {
    if (dispositivi != null) {
      Iterator<Dispositivo> iterator = dispositivi.iterator();
      while (iterator != null && iterator.hasNext()) {
        Dispositivo dispositivo = iterator.next();
        senderFsmService.sendMessageForChangeStatoDispositivo(dispositivo, event);
      }
    }

  }

  private void createNewOrder(Richiesta richiesta) {
    Long idRichiesta = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    RichiestaNewDispositivoDTO dispositivoNewRichiestaDTO = new RichiestaNewDispositivoDTO(idRichiesta, identificativo);
    try {
      log.info("Send message to generate new richiesta of sostitution from richiesta: " + identificativo);
      senderFsmService.getSenderJmsService()
                      .publishRequestNewDispositivoMessage(dispositivoNewRichiestaDTO);
    } catch (Exception e) {
      log.error("Error", e);
    }

  }

}
