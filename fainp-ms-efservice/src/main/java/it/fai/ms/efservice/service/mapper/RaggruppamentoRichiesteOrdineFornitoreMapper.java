package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.RaggruppamentoRichiesteOrdineFornitoreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RaggruppamentoRichiesteOrdineFornitore and its DTO RaggruppamentoRichiesteOrdineFornitoreDTO.
 */
@Mapper(componentModel = "spring", uses = {TipoDispositivoMapper.class, })
public interface RaggruppamentoRichiesteOrdineFornitoreMapper extends EntityMapper <RaggruppamentoRichiesteOrdineFornitoreDTO, RaggruppamentoRichiesteOrdineFornitore> {

    @Mapping(source = "tipoDispositivo.id", target = "tipoDispositivoId")
    @Mapping(source = "tipoDispositivo.nome", target = "tipoDispositivoNome")
    RaggruppamentoRichiesteOrdineFornitoreDTO toDto(RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore); 
    @Mapping(target = "richiestas", ignore = true)

    @Mapping(source = "tipoDispositivoId", target = "tipoDispositivo")
    RaggruppamentoRichiesteOrdineFornitore toEntity(RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO); 
    default RaggruppamentoRichiesteOrdineFornitore fromId(Long id) {
        if (id == null) {
            return null;
        }
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = new RaggruppamentoRichiesteOrdineFornitore();
        raggruppamentoRichiesteOrdineFornitore.setId(id);
        return raggruppamentoRichiesteOrdineFornitore;
    }
}
