package it.fai.ms.efservice.service.jms.listener.device;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.DispositivoNotargaDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;

@Service(JmsListenerDeviceNoPlateInsert.QUALIFIER)
@Transactional
public class JmsListenerDeviceNoPlateInsert implements MessageListener {

  public final static String QUALIFIER = "jmsListenerDeviceNoPlateInsert";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt repository;
  private final ClienteFaiRepository clienteFaiRepository;
  private final ContrattoRepositoryExt contrattoRepository;
  private final ProduttoreRepository produttoreRepository;
  private final TipoDispositivoRepository tipoDispositivoRepository;
  private final TipoServizioRepositoryExt tipoServizioRepository;
  private final StatoDispositivoServizioRepository statoDispositivoServizioRepository;

  private final List<String> servizi = Arrays.asList("PEDAGGI_ITALIA", "PARCHEGGI", "TRASPORTO_TRAGHETTI");

  public JmsListenerDeviceNoPlateInsert(DispositivoRepositoryExt repository, ClienteFaiRepository clienteFaiRepository,
                                        ContrattoRepositoryExt contrattoRepository, ProduttoreRepository produttoreRepository,
                                        TipoDispositivoRepository tipoDispositivoRepository,
                                        TipoServizioRepositoryExt tipoServizioRepository,
                                        StatoDispositivoServizioRepository statoDispositivoServizioRepository) {
    super();
    this.repository = repository;
    this.clienteFaiRepository = clienteFaiRepository;
    this.contrattoRepository = contrattoRepository;
    this.produttoreRepository = produttoreRepository;
    this.tipoDispositivoRepository = tipoDispositivoRepository;
    this.tipoServizioRepository = tipoServizioRepository;
    this.statoDispositivoServizioRepository = statoDispositivoServizioRepository;
  }

  @Override
  public void onMessage(Message message) {
    try {
      Produttore produttoreTP = produttoreRepository.findOneByNome("TELEPASS");
      //produttoreTP.setId(2L); // TELEPASS
      TipoDispositivo tipoDispositivoTP = tipoDispositivoRepository.findOneByNome(TipoDispositivoEnum.TELEPASS_ITALIANO);
      //tipoDispositivoTP.setId(1L); // TELEPASS

      DispositivoNotargaDTO dto = (DispositivoNotargaDTO) ((ObjectMessage)message).getObject();
      _log.debug("Received message DispositivoNotargaDTO : {}", dto);

      Contratto contratto = contrattoRepository.findFirstByCodContrattoCliente(dto.getContractNumber());
      if (contratto == null) {
        contratto = new Contratto();
        contratto.setCodContrattoCliente(dto.getContractNumber());
        contratto.setStato(StatoContratto.ATTIVO);
        contratto.setProduttore(produttoreTP);
        contratto.setDataModificaStato(Instant.now());

        ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(dto.getCodiceCliente());
        if (clienteFai != null) {
          contratto.setClienteFai(clienteFai);
          contratto = contrattoRepository.saveAndFlush(contratto);
        }
      }

      if (contratto.getId() != 0) {
        
        final Contratto contrattoToUpdate = contratto;
        
        //TODO verificare se esiste già un dispostivo con seriale e tipo uguali (TELEPASS_ITALIANO)
        List<Dispositivo> disp = repository.findBySerialeAndTipoDispositivo(dto.getSeriale(), tipoDispositivoTP);
        Dispositivo dispositivoToUpdate = null;
        for (Dispositivo dispositivo : disp) {
			if(dispositivo.getContratto().getId().equals(contratto.getId())) {
				_log.warn("Dipostivo {} già esistente e associato al contratto {}",dto.getSeriale(),contratto.getCodContrattoCliente());
				return;
			}else {
				if(dispositivoToUpdate == null || dispositivoToUpdate.getId() < dispositivo.getId())
					dispositivoToUpdate = dispositivo;
			}
		}
        
        // se esiste fare un update cambiando il riferimento al contratto, la modifca stato, le note tecniche, stato attivo
        if(dispositivoToUpdate!=null) {
            _log.info("Update dispositivo with seriale {}", dto.getSeriale());
            dispositivoToUpdate.setContratto(contrattoToUpdate);
            dispositivoToUpdate.setStato(StatoDispositivo.ATTIVO);
            dispositivoToUpdate.setDataModificaStato(dto.getData().atStartOfDay().toInstant(ZoneOffset.UTC)); 
            dispositivoToUpdate.setNoteTecniche(String.format("Cambio contratto ALTIT %s", FastDateFormat.getInstance("dd/MM/yyyy HH:mm").format(new Date())));
        }else{
          _log.info("Create new dispositivo with seriale {}", dto.getSeriale());
          dispositivoToUpdate = new Dispositivo();
          dispositivoToUpdate.setModalitaSpedizione(ModalitaSpedizione.A_FAI);
          dispositivoToUpdate.setSeriale(dto.getSeriale());
          dispositivoToUpdate.setStato(StatoDispositivo.ATTIVO);
          dispositivoToUpdate.tipoMagazzino(TipoMagazzino.NOTARGA);
          dispositivoToUpdate.setContratto(contrattoToUpdate);
          dispositivoToUpdate.setIdentificativo(dto.getSeriale());
          dispositivoToUpdate.setTipoDispositivo(tipoDispositivoTP);
          dispositivoToUpdate.setDataModificaStato(dto.getData().atStartOfDay().toInstant(ZoneOffset.UTC)); 
          dispositivoToUpdate.setNoteTecniche(String.format("Inserito ALTIT %s", FastDateFormat.getInstance("dd/MM/yyyy HH:mm").format(new Date())));
        };
        
        dispositivoToUpdate = repository.saveAndFlush(dispositivoToUpdate);
        _log.debug("Added device no plate : {}", dispositivoToUpdate);

        for (String s : servizi) {
          addService(dispositivoToUpdate, s);
        }        
      }
    } catch (Exception e) {
      _log.error("JmsListenerDeviceNoPlateInsert", e);
      throw new RuntimeException(e);
    }
  }

  private StatoDispositivoServizio addService(Dispositivo dispositivo, String servizio) {
    _log.debug("Added service '{}' to device with id : {}", servizio, dispositivo.getId());

    TipoServizio ts = tipoServizioRepository.findByNome(servizio);
    StatoDispositivoServizio sds = new StatoDispositivoServizio()
        .dataAttivazione(Instant.now())
        .stato(StatoDS.ATTIVO)
        .dispositivo(dispositivo)
        .tipoServizio(ts);
    return statoDispositivoServizioRepository.saveAndFlush(sds);
  } 
  
}
