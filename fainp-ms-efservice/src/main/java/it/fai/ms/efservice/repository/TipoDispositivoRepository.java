package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.CategoriaServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;

/**
 * Spring Data JPA repository for the TipoDispositivo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoDispositivoRepository extends JpaRepository<TipoDispositivo, Long> {
    @Query("select distinct tipo_dispositivo from TipoDispositivo tipo_dispositivo left join fetch tipo_dispositivo.tipoServizios")
    List<TipoDispositivo> findAllWithEagerRelationships();

    @Query("select tipo_dispositivo from TipoDispositivo tipo_dispositivo left join fetch tipo_dispositivo.tipoServizios where tipo_dispositivo.id =:id")
    TipoDispositivo findOneWithEagerRelationships(@Param("id") Long id);
    
    TipoDispositivo findOneByNome(TipoDispositivoEnum nome);

    TipoDispositivo findOneTipoDispositivoAndServiziByNome(TipoDispositivoEnum nome);

    @Query("select distinct td from TipoDispositivo td join td.tipoServizios ts join ts.categoriaServizio as cs where cs.nome = :nomeCategoria order by td.nome")
    List<TipoDispositivo> findByNomeCategoriaServizioOrderByNome(@Param("nomeCategoria")String nomeCategoria);
    
    @Query("select td from TipoDispositivo td where td.hidden = 0")
    List<TipoDispositivo> findNotHiddenDeviceType();

    TipoDispositivo findOneById(Long id);

}
