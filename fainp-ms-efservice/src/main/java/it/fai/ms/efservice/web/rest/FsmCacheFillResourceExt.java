package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.domain.FsmCacheDispositivo;
import it.fai.ms.efservice.domain.FsmCacheRichiesta;
import it.fai.ms.efservice.service.FsmCacheDispositivoService;
import it.fai.ms.efservice.service.FsmCacheRichiestaService;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmDispositivoInfinispanService;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaInfinispanCache;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(FsmCacheFillResourceExt.BASE_PATH)
public class FsmCacheFillResourceExt {

  private final Logger log = LoggerFactory.getLogger(FsmCacheFillResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  public static final String FSM_CACHE_RICHIESTA = "/fsmcache/richiesta";

  public static final String FSM_CACHE_DEVICE = "/fsmcache/device";

  public static final String FSM_FILL_RICHIESTA = "fsmcache/fillrichiesta";

  public static final String FSM_FILL_DEVICE = "fsmcache/filldevice";

  // private static final String ENTITY_NAME = "fsmrichiesta";

  private final FsmCacheRichiestaService fsmCacheRichiesta;

  private final FsmRichiestaInfinispanCache fsmInfinispanRichiesta;

  private final FsmCacheDispositivoService fsmCacheDispositivo;

  private final FsmDispositivoInfinispanService fsmInfinispanDispositivo;

  public FsmCacheFillResourceExt(final FsmCacheRichiestaService fsmCacheRichiesta, final FsmRichiestaInfinispanCache fsmInfinispanRichiesta,
                                 final FsmCacheDispositivoService fsmCacheDispositivo,
                                 final FsmDispositivoInfinispanService fsmInfinispanDispositivo) {
    this.fsmCacheRichiesta = fsmCacheRichiesta;
    this.fsmInfinispanRichiesta = fsmInfinispanRichiesta;
    this.fsmCacheDispositivo = fsmCacheDispositivo;
    this.fsmInfinispanDispositivo = fsmInfinispanDispositivo;
  }

  /**
   * GET /FSM_CACHE/{id} : get user command available.
   *
   * @return the String with status 200 (OK) and the list of String of command
   */
  @GetMapping(FSM_CACHE_RICHIESTA + "/{id}")
  @Timed
  public String getFsmState(@PathVariable Long id) {
    log.debug("REST request to get Fsm state by ID: {}", id);
    String json = fsmCacheRichiesta.get(id);
    log.debug("Found these JSON: {}", json);
    return json;
  }

  @PostMapping(FSM_FILL_RICHIESTA)
  public void fillRichiesta() throws InterruptedException {
    List<String> keysWithException = new ArrayList<>();
    long t1 = System.currentTimeMillis();
    log.info("Start fill FSM RICHIESTA CACHE: {}", t1);
    List<FsmCacheRichiesta> list = new ArrayList<>();
    List<String> keys = fsmInfinispanRichiesta.getAllKeysFsm();
    
    list = keys.parallelStream()
               .map(k -> {
                 try {
                   Long id = Long.valueOf(k);
                   log.debug("Manage key: {}", id);

                   String json = fsmInfinispanRichiesta.getStateMachineContext(k);
                   log.debug("Json found by key {} : [ {} ]", k, json);

                   String fsmJson = fsmCacheRichiesta.get(id);
                   if (StringUtils.isBlank(fsmJson)) {
                     FsmCacheRichiesta fsmCache = new FsmCacheRichiesta(id, json);
                     return fsmCache;
                   } else {
                     log.warn("Skip insert because exists already row: ACTUAL: [{} - {}] - NEW: [{} - {}]", id, fsmJson, id, json);
                   }
                 } catch (Exception e) {
                   log.error("Not processed ID: [ " + k + " ] with Exception: ", e);
                   keysWithException.add(k);
                 }
                 return null;
               })
               .filter(fsmCache -> fsmCache != null)
               .collect(toList());

    fsmCacheRichiesta.putAll(list);
    log.debug("Insert all rows: {}", list.size());
    long t2 = System.currentTimeMillis();
    log.info("Finish fill FSM RICHIESTA CACHE: {}", t2);
    if (!keysWithException.isEmpty()) {
      log.error("Keys in exception : {}", keysWithException);
    }
    log.info("Time used: {}", t2 - t1);
  }

  /**
   * GET /FSM_CACHE/{id} : get user command available.
   *
   * @return the String with status 200 (OK) and the list of String of command
   */
  @GetMapping(FSM_CACHE_DEVICE + "/{id}")
  @Timed
  public String getFsmStateDevice(@PathVariable Long id) {
    log.debug("REST request to get Fsm state by ID: {}", id);
    String json = fsmCacheDispositivo.get(id);
    log.debug("Found these JSON: {}", json);
    return json;
  }

  @PostMapping(FSM_FILL_DEVICE)
  public void fillDevice() throws InterruptedException {
    List<String> keysWithException = new ArrayList<>();
    long t1 = System.currentTimeMillis();
    log.info("Start fill FSM DEVICE CACHE: {}", t1);
    List<FsmCacheDispositivo> list = new ArrayList<>();
    List<String> keys = fsmInfinispanDispositivo.getAllKeysFsm();

    list = keys.parallelStream()
               .map(k -> {
                 try {
                   Long id = Long.valueOf(k);
                   log.debug("Manage key: {}", id);

                   String json = fsmInfinispanDispositivo.getStateMachineContext(k);
                   log.debug("Json found by key {} : [ {} ]", k, json);

                   String fsmJson = fsmCacheDispositivo.get(id);
                   if (StringUtils.isBlank(fsmJson)) {
                     FsmCacheDispositivo fsmCache = new FsmCacheDispositivo(id, json);
                     return fsmCache;
                   } else {
                     log.warn("Skip insert because exists already row: ACTUAL: [{} - {}] - NEW: [{} - {}]", id, fsmJson, id, json);
                   }
                 } catch (Exception e) {
                   log.error("Not processed ID: [ " + k + " ] with Exception: ", e);
                   keysWithException.add(k);
                 }
                 return null;
               })
               .filter(fsmCache -> fsmCache != null)
               .collect(toList());

    fsmCacheDispositivo.putAll(list);
    log.debug("Insert all rows: {}", list.size());
    long t2 = System.currentTimeMillis();
    log.info("Finish fill FSM DEVICE CACHE: {}", t2);
    if (!keysWithException.isEmpty()) {
      log.error("Keys in exception : {}", keysWithException);
    }
    log.info("Time used: {}", t2 - t1);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
