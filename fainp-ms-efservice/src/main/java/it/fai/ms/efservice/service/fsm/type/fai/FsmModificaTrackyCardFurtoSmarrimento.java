package it.fai.ms.efservice.service.fsm.type.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.fai.FsmModificaTrackyCardFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTrackyCardFurtoSmarrimento.FSM_MODIFICA_TRACKYCARD_FURTO_SMARRIMENTO)
public class FsmModificaTrackyCardFurtoSmarrimento extends FsmRichiestaGeneric {

  public static final String FSM_MODIFICA_TRACKYCARD_FURTO_SMARRIMENTO = "fsmModificaTrackyCardFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTrackyCardFurtoSmarrimento(@Qualifier(FsmModificaTrackyCardFurtoSmarrimentoConfig.MOD_TRACKYCARD_FURTO_SMARRIMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                               FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TRACKY_FURTO_SMARRIMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TRACKYCARD };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.INOLTRO_TRACKYCARD;
  }

}
