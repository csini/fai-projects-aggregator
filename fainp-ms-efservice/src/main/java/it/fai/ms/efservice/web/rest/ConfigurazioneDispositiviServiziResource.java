package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.web.rest.util.PaginationUtil;
import it.fai.ms.efservice.service.dto.ConfigurazioneDispositiviServiziCriteria;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ConfigurazioneDispositiviServizi.
 */
@RestController
@RequestMapping("/api")
public class ConfigurazioneDispositiviServiziResource {

    private final Logger log = LoggerFactory.getLogger(ConfigurazioneDispositiviServiziResource.class);

    private static final String ENTITY_NAME = "configurazioneDispositiviServizi";

    private final ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService;

    private final ConfigurazioneDispositiviServiziQueryService configurazioneDispositiviServiziQueryService;

    public ConfigurazioneDispositiviServiziResource(ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService, ConfigurazioneDispositiviServiziQueryService configurazioneDispositiviServiziQueryService) {
        this.configurazioneDispositiviServiziService = configurazioneDispositiviServiziService;
        this.configurazioneDispositiviServiziQueryService = configurazioneDispositiviServiziQueryService;
    }

    /**
     * POST  /configurazione-dispositivi-servizis : Create a new configurazioneDispositiviServizi.
     *
     * @param configurazioneDispositiviServizi the configurazioneDispositiviServizi to create
     * @return the ResponseEntity with status 201 (Created) and with body the new configurazioneDispositiviServizi, or with status 400 (Bad Request) if the configurazioneDispositiviServizi has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/configurazione-dispositivi-servizis")
    @Timed
    public ResponseEntity<ConfigurazioneDispositiviServizi> createConfigurazioneDispositiviServizi(@Valid @RequestBody ConfigurazioneDispositiviServizi configurazioneDispositiviServizi) throws URISyntaxException {
        log.debug("REST request to save ConfigurazioneDispositiviServizi : {}", configurazioneDispositiviServizi);
        if (configurazioneDispositiviServizi.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new configurazioneDispositiviServizi cannot already have an ID")).body(null);
        }
        ConfigurazioneDispositiviServizi result = configurazioneDispositiviServiziService.save(configurazioneDispositiviServizi);
        return ResponseEntity.created(new URI("/api/configurazione-dispositivi-servizis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /configurazione-dispositivi-servizis : Updates an existing configurazioneDispositiviServizi.
     *
     * @param configurazioneDispositiviServizi the configurazioneDispositiviServizi to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated configurazioneDispositiviServizi,
     * or with status 400 (Bad Request) if the configurazioneDispositiviServizi is not valid,
     * or with status 500 (Internal Server Error) if the configurazioneDispositiviServizi couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/configurazione-dispositivi-servizis")
    @Timed
    public ResponseEntity<ConfigurazioneDispositiviServizi> updateConfigurazioneDispositiviServizi(@Valid @RequestBody ConfigurazioneDispositiviServizi configurazioneDispositiviServizi) throws URISyntaxException {
        log.debug("REST request to update ConfigurazioneDispositiviServizi : {}", configurazioneDispositiviServizi);
        if (configurazioneDispositiviServizi.getId() == null) {
            return createConfigurazioneDispositiviServizi(configurazioneDispositiviServizi);
        }
        ConfigurazioneDispositiviServizi result = configurazioneDispositiviServiziService.save(configurazioneDispositiviServizi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, configurazioneDispositiviServizi.getId().toString()))
            .body(result);
    }

    /**
     * GET  /configurazione-dispositivi-servizis : get all the configurazioneDispositiviServizis.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of configurazioneDispositiviServizis in body
     */
    @GetMapping("/configurazione-dispositivi-servizis")
    @Timed
    public ResponseEntity<List<ConfigurazioneDispositiviServizi>> getAllConfigurazioneDispositiviServizis(ConfigurazioneDispositiviServiziCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get ConfigurazioneDispositiviServizis by criteria: {}", criteria);
        Page<ConfigurazioneDispositiviServizi> page = configurazioneDispositiviServiziQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/configurazione-dispositivi-servizis");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /configurazione-dispositivi-servizis/:id : get the "id" configurazioneDispositiviServizi.
     *
     * @param id the id of the configurazioneDispositiviServizi to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the configurazioneDispositiviServizi, or with status 404 (Not Found)
     */
    @GetMapping("/configurazione-dispositivi-servizis/{id}")
    @Timed
    public ResponseEntity<ConfigurazioneDispositiviServizi> getConfigurazioneDispositiviServizi(@PathVariable Long id) {
        log.debug("REST request to get ConfigurazioneDispositiviServizi : {}", id);
        ConfigurazioneDispositiviServizi configurazioneDispositiviServizi = configurazioneDispositiviServiziService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(configurazioneDispositiviServizi));
    }

    /**
     * DELETE  /configurazione-dispositivi-servizis/:id : delete the "id" configurazioneDispositiviServizi.
     *
     * @param id the id of the configurazioneDispositiviServizi to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/configurazione-dispositivi-servizis/{id}")
    @Timed
    public ResponseEntity<Void> deleteConfigurazioneDispositiviServizi(@PathVariable Long id) {
        log.debug("REST request to delete ConfigurazioneDispositiviServizi : {}", id);
        configurazioneDispositiviServiziService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
