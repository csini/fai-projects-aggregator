package it.fai.ms.efservice.wizard.web.rest;

import static it.fai.ms.efservice.wizard.model.WizardVehicleId.mapToWizardVehicleIds;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.step3.WizardStep3Matrix;
import it.fai.ms.efservice.wizard.service.Step3Service;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.web.rest.vm.step3.MatrixStep3;
import it.fai.ms.efservice.wizard.web.rest.vm.step3.MatrixStep3Vehicle;

@RestController
@RequestMapping("/api/wizard/step3")
public class Step3Controller {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private Step3Service step3Service;

  @Autowired
  public Step3Controller(final Step3Service _step3Service) {
    step3Service = _step3Service;
  }

  @PostMapping("matrix/{serviceId}")
  public ResponseEntity<MatrixStep3> getMatrix(final @PathVariable(name = "serviceId") @NotNull String _serviceId,
                                               final @RequestBody @NotNull Set<String> _vehicleIds) {

    final MatrixStep3 matrix = convertWizardStep3Matrix(step3Service.buildMatrix(new WizardServiceTypeId(_serviceId),
                                                                                 mapToWizardVehicleIds(_vehicleIds)));

    _log.info("Matrix created (total items: {})", matrix.getVehicles()
                                                        .size());
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(matrix);
  }

  private MatrixStep3 convertWizardStep3Matrix(final WizardStep3Matrix _wizardStep3Matrix) {
    MatrixStep3 matrixStep3 = new MatrixStep3(_wizardStep3Matrix.getServiceTypeId());

    final Map<String, MatrixStep3Vehicle> map = _wizardStep3Matrix.getMap()
                                                                  .entrySet()
                                                                  .stream()
                                                                  .collect(toMap(wizardVehicleId -> wizardVehicleId.getKey()
                                                                                                                   .getId(),
                                                                                 wizardMatrixActivability -> mapToMatrixStep3Vehicle(wizardMatrixActivability.getValue())));
    matrixStep3.getVehicles()
               .putAll(map);
    return matrixStep3;
  }

  private MatrixStep3Vehicle mapToMatrixStep3Vehicle(final WizardMatrixActivability _wizardMatrixActivability) {
    final MatrixStep3Vehicle matrixStep3Vehicle = new MatrixStep3Vehicle();
    matrixStep3Vehicle.setState(_wizardMatrixActivability.getActivabilityState()
                                                         .name());
    List<MatrixActivabilityFailure> errors = _wizardMatrixActivability.getActivabilityFailures() != null
                                             && !_wizardMatrixActivability.getActivabilityFailures()
                                                                          .isEmpty() ? _wizardMatrixActivability.getActivabilityFailures()
                                                                                                                .stream()
                                                                                                                .map(w -> new MatrixActivabilityFailure(w.getCode(),
                                                                                                                                                        w.getMess()))
                                                                                                                .collect(toList())
                                                                                     : null;

    matrixStep3Vehicle.setErrors(errors);
    return matrixStep3Vehicle;
  }

}
