package it.fai.ms.efservice.wizard.service;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.repository.WizardDeviceRepository;
import it.fai.ms.efservice.wizard.repository.WizardVehicleRepository;

@Service
@Transactional
public class WizardVehicleServiceImpl implements WizardVehicleService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private WizardDeviceRepository  wizardDeviceRepository;
  private WizardVehicleRepository wizardVehicleRepository;

  public WizardVehicleServiceImpl() {
  }

  @Autowired
  public WizardVehicleServiceImpl(final WizardVehicleRepository _wizardVehicleRepository,
                                  final WizardDeviceRepository _wizardDeviceRepository) {
    wizardVehicleRepository = _wizardVehicleRepository;
    wizardDeviceRepository = _wizardDeviceRepository;
  }

  @Override
  @Transactional
  public void addVehicle(final WizardVehicle _wizardVehicle) {
    wizardVehicleRepository.save(_wizardVehicle);
  }

  @Override
  public Integer count() {
    return wizardVehicleRepository.count();
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<WizardVehicle> getVehicle(final WizardVehicleId _wizardVehicleId) {
    return wizardVehicleRepository.find(_wizardVehicleId.getId());
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasActiveServices(final WizardVehicleId _wizardVehicleId) {
    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .anyMatch(wizardDevice -> wizardDevice.hasActiveServices());
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasActiveServices(final WizardVehicleId _wizardVehicleId, final WizardDeviceTypeId _wizardDeviceTypeId) {
    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .filter(wizardDevice -> wizardDevice.getType()
                                                                     .getId()
                                                                     .equals(_wizardDeviceTypeId))
                                 .anyMatch(wizardDevice -> wizardDevice.hasActiveServices());
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasActiveService(final WizardVehicleId _wizardVehicleId, final WizardDeviceTypeId _wizardDeviceTypeId,
                                  final WizardServiceTypeId _wizardServiceTypeId) {

    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .filter(wizardDevice -> wizardDevice.getType()
                                                                     .getId()
                                                                     .equals(_wizardDeviceTypeId))
                                 .anyMatch(wizardDevice -> wizardDevice.hasActiveService(_wizardServiceTypeId));
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasAnomalies(final WizardVehicle _wizardVehicle, final WizardDeviceTypeId _wizardDeviceTypeId) {

    boolean result = false;

    // prendo tutti i dispositivi associati ad un id veicolo

    /*
     * ResponseEntity<List<AssociazioneDV>> listaDispositiviAssociatiAVeicoloResponseEntity =
     * associazioneDVServiceExt.findAssociazioneDVbyVehicleId(_wizardVehicleId.getId() .toString());
     * List<AssociazioneDV> listaDispositiviAssociatiAVeicolo =
     * listaDispositiviAssociatiAVeicoloResponseEntity.getBody(); if ((listaDispositiviAssociatiAVeicolo != null) &&
     * (listaDispositiviAssociatiAVeicolo.size()) > 0) { for (int i = 0; i < listaDispositiviAssociatiAVeicolo.size();
     * i++) { // recupero il numero di servizi attivi su quel dispositivo ResponseEntity<Integer> anomaliaResponseEntity
     * = statoDispositivoServizioServiceExt.findAnomaly(listaDispositiviAssociatiAVeicolo.get(i) .getDispositivo());
     * Integer anomalia = anomaliaResponseEntity.getBody(); if (anomalia > 1) { result = true; } } }
     */

    return result;
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasOngoingRequest(final WizardVehicle _wizardVehicle) {
    return _wizardVehicle.getDevices()
                         .stream()
                         .anyMatch(wizardDevice -> wizardDevice.hasOnGoingRequest());
  }

  @Override
  @Transactional(readOnly = true)
  public boolean hasOngoingRequest(final WizardVehicle _wizardVehicle, final WizardDeviceTypeId _wizardDeviceTypeId) {
    return _wizardVehicle.getDevices()
                         .stream()
                         .filter(wizardDevice -> wizardDevice.getType()
                                                             .getId()
                                                             .equals(_wizardDeviceTypeId))
                         .anyMatch(wizardDevice -> wizardDevice.hasOnGoingRequest());
  }

}
