package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.OrdineFornitoreService;
import it.fai.ms.efservice.web.rest.errors.BadRequestAlertException;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OrdineFornitore.
 */
@RestController
@RequestMapping("/api")
public class OrdineFornitoreResource {

    private final Logger log = LoggerFactory.getLogger(OrdineFornitoreResource.class);

    private static final String ENTITY_NAME = "ordineFornitore";

    private final OrdineFornitoreService ordineFornitoreService;

    public OrdineFornitoreResource(OrdineFornitoreService ordineFornitoreService) {
        this.ordineFornitoreService = ordineFornitoreService;
    }

    /**
     * POST  /ordine-fornitores : Create a new ordineFornitore.
     *
     * @param ordineFornitoreDTO the ordineFornitoreDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ordineFornitoreDTO, or with status 400 (Bad Request) if the ordineFornitore has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ordine-fornitores")
    @Timed
    public ResponseEntity<OrdineFornitoreDTO> createOrdineFornitore(@Valid @RequestBody OrdineFornitoreDTO ordineFornitoreDTO) throws URISyntaxException {
        log.debug("REST request to save OrdineFornitore : {}", ordineFornitoreDTO);
        if (ordineFornitoreDTO.getId() != null) {
            throw new BadRequestAlertException("A new ordineFornitore cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrdineFornitoreDTO result = ordineFornitoreService.save(ordineFornitoreDTO);
        return ResponseEntity.created(new URI("/api/ordine-fornitores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ordine-fornitores : Updates an existing ordineFornitore.
     *
     * @param ordineFornitoreDTO the ordineFornitoreDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ordineFornitoreDTO,
     * or with status 400 (Bad Request) if the ordineFornitoreDTO is not valid,
     * or with status 500 (Internal Server Error) if the ordineFornitoreDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ordine-fornitores")
    @Timed
    public ResponseEntity<OrdineFornitoreDTO> updateOrdineFornitore(@Valid @RequestBody OrdineFornitoreDTO ordineFornitoreDTO) throws URISyntaxException {
        log.debug("REST request to update OrdineFornitore : {}", ordineFornitoreDTO);
        if (ordineFornitoreDTO.getId() == null) {
            return createOrdineFornitore(ordineFornitoreDTO);
        }
        OrdineFornitoreDTO result = ordineFornitoreService.save(ordineFornitoreDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ordineFornitoreDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ordine-fornitores : get all the ordineFornitores.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ordineFornitores in body
     */
    @GetMapping("/ordine-fornitores")
    @Timed
    public List<OrdineFornitoreDTO> getAllOrdineFornitores() {
        log.debug("REST request to get all OrdineFornitores");
        return ordineFornitoreService.findAll();
        }

    /**
     * GET  /ordine-fornitores/:id : get the "id" ordineFornitore.
     *
     * @param id the id of the ordineFornitoreDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ordineFornitoreDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ordine-fornitores/{id}")
    @Timed
    public ResponseEntity<OrdineFornitoreDTO> getOrdineFornitore(@PathVariable Long id) {
        log.debug("REST request to get OrdineFornitore : {}", id);
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ordineFornitoreDTO));
    }

    /**
     * DELETE  /ordine-fornitores/:id : delete the "id" ordineFornitore.
     *
     * @param id the id of the ordineFornitoreDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ordine-fornitores/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrdineFornitore(@PathVariable Long id) {
        log.debug("REST request to delete OrdineFornitore : {}", id);
        ordineFornitoreService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
