package it.fai.ms.efservice.service.fsm.bean.fai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.bean.StatesOnTransition;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.NotificationActivationTrackyCardService;

@WithStateMachine(id = "fsmInoltroFrejus")
public class FsmInoltroFrejusTransitionBean extends AbstractFsmRichiestaTransition {

}
