/**
 * 
 */
package it.fai.ms.efservice.service.fsm.action;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

/**
 * @author Luca Vassallo
 */
public class FsmActionDispositivoSospeso implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public FsmActionDispositivoSospeso(FsmSenderToQueue senderFsmService) {
    this.senderFsmService = senderFsmService;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.action.Action#execute(org.springframework.statemachine.StateContext)
   */
  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      try {
        changeStateWithCommand(dispositivi, DispositivoEvent.SOSPENSIONE);
      } catch (Exception e) {
        log.error("Exception", e);
      }
    }
  }

  private void changeStateWithCommand(Set<Dispositivo> dispositivi, DispositivoEvent event) throws Exception {
    if (dispositivi != null) {
      Iterator<Dispositivo> iterator = dispositivi.iterator();
      while (iterator != null && iterator.hasNext()) {
        Dispositivo dispositivo = iterator.next();
        senderFsmService.sendMessageForChangeStatoDispositivo(dispositivo, event);
      }
    }

  }

}
