package it.fai.ms.efservice.service.fsm.config;

import java.util.Set;

import org.slf4j.Logger;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public abstract class FsmModificaSimpleConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  protected abstract Logger getLogger();

  private final FsmSenderToQueue fsmSenderToQueue;

  public FsmModificaSimpleConfig(final FsmSenderToQueue fsmSenderToQueue) {
    this.fsmSenderToQueue = fsmSenderToQueue;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId(getMachineId());
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());

  }

  protected abstract String getMachineId();

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(getStatoRichiestaInitial(), ctx -> getLogger().trace("Target: " + ctx.getTarget()
                                                                                        .getIds()))
          .states(getAvailableStates());
  }

  protected abstract StatoRichiesta getStatoRichiestaInitial();

  protected abstract Set<StatoRichiesta> getAvailableStates();

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(getStatoRichiestaInitial())
               .target(StatoRichiesta.INIZIALE)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.MU_SOSPENDI)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INIZIALE)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.RICHIESTA_RIFIUTATA)
               .event(RichiestaEvent.MU_RIFIUTATA_RICHIESTA)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(fsmSenderToQueue))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(fsmSenderToQueue));
  }
}
