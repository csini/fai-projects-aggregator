package it.fai.ms.efservice.wizard.model.matrix;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import it.fai.ms.efservice.wizard.model.ActivabilityState;

public class WizardMatrixActivability implements Serializable {

  private static final long serialVersionUID = 4022033599699566594L;

  private List<WizardMatrixActivabilityFailure> activabilityFailures;
  private ActivabilityState                     activabilityState;

  public WizardMatrixActivability(final ActivabilityState _activabilityState) {
    Objects.nonNull("ActivabilityState must be present");
    activabilityState = _activabilityState;
  }

  public void addFailure(final WizardMatrixActivabilityFailure _step4WizardMatrixActivabilityFailure) {
    Optional.ofNullable(_step4WizardMatrixActivabilityFailure)
            .ifPresent(failure -> {
              activabilityFailures = Optional.ofNullable(activabilityFailures)
                                             .orElse(new LinkedList<>());
              activabilityFailures.add(_step4WizardMatrixActivabilityFailure);
            });

  }

  public List<WizardMatrixActivabilityFailure> getActivabilityFailures() {
    return activabilityFailures;
  }

  public ActivabilityState getActivabilityState() {
    return activabilityState;
  }

  @Override
  public int hashCode() {
    return Objects.hash(activabilityState, activabilityFailures);
  }

  
  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardMatrixActivability) _obj).getActivabilityState(), activabilityState)
                 && Objects.equals(((WizardMatrixActivability) _obj).getActivabilityFailures(), activabilityFailures);
    }
    return isEquals;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardMatrixActivability [");
    if (this.activabilityFailures != null) {
      builder.append("activabilityFailures=");
      builder.append(this.activabilityFailures);
      builder.append(", ");
    }
    if (this.activabilityState != null) {
      builder.append("activabilityState=");
      builder.append(this.activabilityState);
    }
    builder.append("]");
    return builder.toString();
  }

}
