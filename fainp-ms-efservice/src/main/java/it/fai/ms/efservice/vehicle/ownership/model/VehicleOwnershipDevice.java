package it.fai.ms.efservice.vehicle.ownership.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class VehicleOwnershipDevice implements Serializable {

  private static final long serialVersionUID = -7463978481787308863L;

  private VehicleOwnershipDeviceTypeId       deviceTypeId;
  private boolean                            ongoingRequest;
  private Set<VehicleOwnershipServiceTypeId> serviceTypeIds = new HashSet<>();

  public VehicleOwnershipDevice(final VehicleOwnershipDeviceTypeId _vehicleOwnershipDeviceTypeId) {
    Objects.requireNonNull(_vehicleOwnershipDeviceTypeId, "VehicleOwnershipDeviceTypeId is mandatory");
    deviceTypeId = _vehicleOwnershipDeviceTypeId;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((VehicleOwnershipDevice) _obj).getDeviceTypeId(), deviceTypeId);
    }
    return isEquals;
  }

  public VehicleOwnershipDeviceTypeId getDeviceTypeId() {
    return deviceTypeId;
  }

  public Set<VehicleOwnershipServiceTypeId> getServiceTypeIds() {
    return serviceTypeIds;
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceTypeId);
  }

  public boolean hasOngoingRequest() {
    return ongoingRequest;
  }

  public void setOngoingRequest(final boolean _ongoingRequest) {
    ongoingRequest = _ongoingRequest;
  }

  public void setServiceTypeIds(Set<VehicleOwnershipServiceTypeId> serviceTypeIds) {
    this.serviceTypeIds = serviceTypeIds;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("VehicleOwnershipDevice [deviceTypeId=");
    builder.append(this.deviceTypeId);
    builder.append(", ongoingRequest=");
    builder.append(this.ongoingRequest);
    builder.append("]");
    return builder.toString();
  }

}
