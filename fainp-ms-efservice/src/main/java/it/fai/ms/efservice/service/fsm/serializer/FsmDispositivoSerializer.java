/**
 * 
 */
package it.fai.ms.efservice.service.fsm.serializer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

/**
 * @author Luca Vassallo
 * @param <S>
 * @param <T>
 */
@Service
public class FsmDispositivoSerializer {

  public String serialize(StateMachineContext<StatoDispositivo, DispositivoEvent> obj) {

    ObjectMapper objMap = new ObjectMapper();
    String jsonInString = null;
    try {
      jsonInString = objMap.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      // TODO manage exception;
      e.printStackTrace();
    }

    return jsonInString;
  }

  public StateMachineContext<StatoDispositivo, DispositivoEvent> deSerialize(String json) {
    return deSerialize(json, null);
  }

  @SuppressWarnings("unchecked")
  public StateMachineContext<StatoDispositivo, DispositivoEvent> deSerialize(String json, String idNewMachine) {
    DefaultStateMachineContext<StatoDispositivo, DispositivoEvent> defaultStateMachineContext = null;

    ObjectMapper objMap = new ObjectMapper();
    Object obj = null;
    try {
      obj = objMap.readValue(json, Object.class);

      if (obj instanceof HashMap<?, ?>) {
        HashMap<String, Object> map = (HashMap<String, Object>) obj;

        Object object = map.get("historyStates");
        Map<StatoDispositivo, StatoDispositivo> mapping = new HashMap<>();
        if (object instanceof Map<?, ?>) {
          mapping = (Map<StatoDispositivo, StatoDispositivo>) object;
        }

        ExtendedState extendedState = new DefaultExtendedState();
        object = map.get("extendedState");
        if (object instanceof HashMap<?, ?>) {
          HashMap<String, Object> mapExtendedState = (HashMap<String, Object>) object;
          Object variables = mapExtendedState.get("variables");
          extendedState.getVariables()
                       .putAll((Map<? extends Object, ? extends Object>) variables);
        }

        String idMachine = null;
        if (StringUtils.isNotBlank(idNewMachine)) {
          idMachine = idNewMachine;
        } else {
          idMachine = (String) map.get("id");
        }

        defaultStateMachineContext = new DefaultStateMachineContext<StatoDispositivo, DispositivoEvent>((List<StateMachineContext<StatoDispositivo, DispositivoEvent>>) map.get("childs"),
                                                                                            StatoDispositivo.valueOf((String) map.get("state")),
                                                                                            null, null, extendedState, mapping, idMachine);
      }
    } catch (JsonParseException e) {
      // TODO manage exception
      e.printStackTrace();
    } catch (JsonMappingException e) {
      // TODO manage exception
      e.printStackTrace();
    } catch (IOException e) {
      // TODO manage exception
      e.printStackTrace();
    }

    return defaultStateMachineContext;
  }

}
