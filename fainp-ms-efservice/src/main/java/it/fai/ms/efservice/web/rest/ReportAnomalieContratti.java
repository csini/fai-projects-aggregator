package it.fai.ms.efservice.web.rest;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.service.report.ReportAnomalieContrattiService;
import it.fai.ms.efservice.service.report.dto.AnomalieContrattiAttiviDTO;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.util.ResponsePage;

@RestController
@RequestMapping("api")
public class ReportAnomalieContratti {
  private static final String KEY_JOB           = "job";
  final static String         BASE_URL          = "/_search";
  private static final String BASE_REPORT_PATH  = "/report";
  private static final String BASE_POLLING_PATH = "/polling" + BASE_REPORT_PATH;

  private final Logger log = LoggerFactory.getLogger(getClass());

  final ReportAnomalieContrattiService service;

  private final AsyncJobServiceImpl asyncJobService;

  public ReportAnomalieContratti(ReportAnomalieContrattiService service, AsyncJobServiceImpl asyncJobService) {
    this.service         = service;
    this.asyncJobService = asyncJobService;
  }

  @PostMapping(BASE_URL+"/CONTRATTI_ATTIVI_CON_DISPOSITIVI_BLOCCATI")
  @Timed
  @ResponseBody
  public ResponseEntity<ResponsePage<AnomalieContrattiAttiviDTO>> searchReportCONTRATTI_ATTIVI_CON_DISPOSITIVI_BLOCCATI(Pageable pageable) {
    return ResponseEntity.ok(new ResponsePage<>(service.reportContrattiAttiviCondispositiviBloccati(pageable)));
  }

  @GetMapping(BASE_REPORT_PATH + "/CONTRATTI_ATTIVI_CON_DISPOSITIVI_BLOCCATI")
  @Timed
  @ResponseBody
  public ResponseEntity<Map<String, String>> report() {
    log.debug("Call async fro processing Report CONTRATTI_ATTIVI_CON_DISPOSITIVI_BLOCCATI");

    String identifierJob = asyncJobService.generateKeyCacheAndPushValue("ANOMALIE_CONTRATTI",
                                                                        new AsyncJobResponse(StateChangeStatusFsm.PENDING, null));

    CompletableFuture
      .supplyAsync(() -> service
        .reportContrattiAttiviTuttiDispositiviFatturati(identifierJob))
      .exceptionally(e -> {
        log.error("Exception in Transiti Resource method processAsyncTransiti: {}", e);
        AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, null);
        response.add(Errno.NOT_FOUND_CONTRATTO_CLIENTEFAI);
        return response;
      })
      .thenAccept(state -> asyncJobService.save(identifierJob, state));
    return ResponseEntity.ok(Collections.singletonMap(KEY_JOB, identifierJob));
  }

  @GetMapping(BASE_POLLING_PATH + "/{identificativo}")
  @Timed
  public ResponseEntity<AsyncJobResponse> pollingReport(@PathVariable String identificativo) {
    log.debug("Rest request for polling for jobkey: {}", identificativo);
    AsyncJobResponse ouput = (AsyncJobResponse) asyncJobService.getValue(identificativo);
    log.info("Response polling is {}", ouput);
    return ResponseEntity.ok(ouput);
  }
}
