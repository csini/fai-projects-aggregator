package it.fai.ms.efservice.web.rest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.FsmRichiesteDTO;
import it.fai.ms.efservice.service.fsm.FsmResetService;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(FsmRichiestaResourceExt.BASE_PATH)
public class FsmRichiestaResourceExt {

  private final Logger log = LoggerFactory.getLogger(FsmRichiestaResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  public static final String FSM_RICHIESTA = "/statemachinerichiesta";

  public static final String USER_AVAILABLE_COMMAND = "/useravailablecommand";

  public static final String AVAILABLE_COMMAND = "/availablecommand";

  public static final String FSM_RESET_STATE = "/fsmreset";

  public static final String FSM_STATE = "/fsmstate";

  // private static final String ENTITY_NAME = "fsmrichiesta";

  private final FsmAsyncService            fsmAsyncService;
  private final AsyncPollingJobServiceImpl asyncPollingJob;
  private final FsmRichiestaUtils          fsmRichiestaUtils;
  private final FsmResetService            fsmResetService;

  public FsmRichiestaResourceExt(FsmAsyncService _fsmAsyncService, AsyncPollingJobServiceImpl _asyncPollingJob,
                                 FsmRichiestaUtils _fsmRichiestaUtils, final FsmResetService _fsmResetService) {
    fsmAsyncService = _fsmAsyncService;
    asyncPollingJob = _asyncPollingJob;
    fsmRichiestaUtils = _fsmRichiestaUtils;
    fsmResetService = _fsmResetService;
  }

  @PostMapping(FSM_RICHIESTA + "/{identificativo}")
  @Timed
  public ResponseEntity<Boolean> changeStatusToRichiesta(@PathVariable String identificativo,
                                                         @RequestParam(required = true) RichiestaEvent command,
                                                         @RequestParam(required = false) String motivoSospensione,
                                                         @RequestParam(required = false) String nota) throws CustomException,
                                                                                                      InterruptedException {
    long ti = System.currentTimeMillis();
    if (StringUtils.isBlank(identificativo)) {
      log.error("Identificativo Richiesta NULL");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    if (command == null) {
      log.error("Command to execute is NULL");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identificativoJob = fsmAsyncService.getKeyCacheAndPushDefaultValue("JHIPSTER");

    FsmRichiesteDTO fsmDTO = new FsmRichiesteDTO();
    Set<String> identificativi = new HashSet<>();
    identificativi.add(identificativo);
    fsmDTO.setIdentificativi(identificativi);
    fsmDTO.setOperazione(command.name());
    fsmDTO.setMotivoSospensione(motivoSospensione);
    fsmDTO.setNota(nota);

    log.info("Richiesta: [Identificativo: {}] - Command: {} - MotivoSospensione: {} - Nota: {}", identificativo, command, motivoSospensione,
             nota);
    fsmAsyncService.changeAsyncFsmStatus(identificativoJob, fsmDTO);

    StateChangeStatusFsm state = asyncPollingJob.pollingRichiesteApiGateway(identificativoJob);

    boolean valueToReturn = true;
    if (state == null || state != StateChangeStatusFsm.SUCCESS) {
      log.warn("Not changed status. Polling result on identificativo job {} : {}", identificativoJob, state);
      valueToReturn = false;
    }

    long tf = System.currentTimeMillis();
    log.debug("FSM Richiesta Resource Ext Change status Richiesta in: {} ms", (tf - ti));
    return ResponseEntity.ok(valueToReturn);
  }

  /**
   * GET /useravailablecommand/id/{id} : get user command available.
   *
   * @return the List<String> with status 200 (OK) and the list of String of command
   */
  @GetMapping(USER_AVAILABLE_COMMAND + "/id" + "/{id}")
  @Timed
  public List<String> getUserAvailableCommandByRichiestaId(@PathVariable Long id) {
    log.debug("REST request to get user command available for Richiesta by ID: " + id);
    List<String> availableCommandUser = fsmRichiestaUtils.getAvailableCommandByRichiestaId(id, true);
    log.debug("Found these USER available commands: {}", availableCommandUser);
    return availableCommandUser;
  }

  /**
   * GET /availablecommand/id/{id} : get user command available.
   *
   * @return the List<String> with status 200 (OK) and the list of String of command
   */
  @GetMapping(AVAILABLE_COMMAND + "/id" + "/{id}")
  @Timed
  public List<String> getAvailableCommandByRichiestaId(@PathVariable Long id) {
    log.debug("REST request to get ALL command available Richiesta by ID: " + id);
    List<String> availableCommandUser = fsmRichiestaUtils.getAvailableCommandByRichiestaId(id, false);
    log.debug("Found these available commands: {}", availableCommandUser);
    return availableCommandUser;
  }

  /**
   * GET /useravailablecommand/{idenditifcativo} : get user command available.
   *
   * @return the List<String> with status 200 (OK) and the list of String of command
   */
  @GetMapping(USER_AVAILABLE_COMMAND + "/{identificativo}")
  @Timed
  public List<String> getUserAvailableCommandByIdentificativoRichiesta(@PathVariable String identificativo) {
    log.debug("REST request to get user command available for Richiesta by Identificativo: " + identificativo);
    List<String> availableCommandUser = fsmRichiestaUtils.getAvailableCommandByIdentificativoRichiesta(identificativo, true);
    log.debug("Found these USER available commands: {}", availableCommandUser);
    return availableCommandUser;
  }

  /**
   * GET /availablecommand/{identificativo} : get user command available.
   *
   * @return the List<String> with status 200 (OK) and the list of String of command
   */
  @GetMapping(AVAILABLE_COMMAND + "/{identificativo}")
  @Timed
  public List<String> getAvailableCommandByIdentificativoRichiesta(@PathVariable String identificativo) {
    log.debug("REST request to get ALL command available for Richiesta by Identificativo: " + identificativo);
    List<String> availableCommandUser = fsmRichiestaUtils.getAvailableCommandByIdentificativoRichiesta(identificativo, false);
    log.debug("Found these available commands: {}", availableCommandUser);
    return availableCommandUser;
  }

  @PostMapping(FSM_RESET_STATE + "/{identificativo}/{stato}")
  @Timed
  public ResponseEntity<String> getAvailableCommandByIdentificativoRichiesta(@PathVariable String identificativo,
                                                                             @PathVariable StatoRichiesta stato) throws CustomException {

    if (StringUtils.isBlank(identificativo) || stato == null) {
      log.error("Identificativo {} or stato {} is not valid", identificativo, stato);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String newContextFsm = fsmResetService.resetStateFsm(identificativo, stato);
    if (newContextFsm != null) {
      fsmRichiestaUtils.setOperazioniPossibiliOnRichiesta(identificativo);
    } else {
      log.error("Exception on reset FSM for identificativo: {}", identificativo);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.EXCEPTION_RESET_STATE_FSM);
    }
    return ResponseEntity.ok(newContextFsm);
  }

  @PostMapping(FSM_STATE + "/{identificativo}")
  @Timed
  public ResponseEntity<String> getStateFsm(@PathVariable String identificativo) throws CustomException {
    if (StringUtils.isBlank(identificativo)) {
      log.error("Identificativo {} is not valid", identificativo);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }
    String state = fsmResetService.getState(identificativo);
    return ResponseEntity.ok(state);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
