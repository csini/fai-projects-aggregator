package it.fai.ms.efservice.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.dto.AssociazioneVeicoliCounterDTO;
import it.fai.ms.efservice.web.rest.vm.NumeroDispositiviVM;

/**
 * Spring Data JPA repository for the AssociazioneDV entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AssociazioneDVRepository extends JpaRepository<AssociazioneDV, Long> {

  List<AssociazioneDV> findByUuidVeicolo(String _vehicleUuid);

  @Query("select adv from  AssociazioneDV adv where adv.dispositivo.id = :idDispositivo")
  List<AssociazioneDV> findByDispositivoId(@Param("idDispositivo") Long idDispositivo);

  List<AssociazioneDV> findByUuidVeicoloIn(Set<String> uuidVeicoli);

  @Query("select adv from  AssociazioneDV adv where adv.uuidVeicolo = :uuidVehicle and adv.dispositivo.contratto.clienteFai.codiceCliente = :codiceAzienda")
  List<AssociazioneDV> findByUuidVeicoloAndCodiceClienteFai(@Param("uuidVehicle") String uuid,
                                                            @Param("codiceAzienda") String codiceAzienda);

  AssociazioneDV findFirstByUuidVeicoloAndDispositivo(String uuid, Dispositivo dispositivo);
  
  AssociazioneDV findFirstByDispositivo(Dispositivo dispositivo);


  // select dispositivo.seriale, count(associazione_dv.uuid_veicolo) as counter from dbo.dispositivo left join
  // dbo.associazione_dv
  // on dbo.dispositivo.id=dbo.associazione_dv.id group by dispositivo.seriale
  @Query("select new it.fai.ms.efservice.service.dto.AssociazioneVeicoliCounterDTO(d.seriale, count(a.uuidVeicolo)) "
         + " from Dispositivo d left join AssociazioneDV a " + " on d.id=a.id " + " where d.seriale in (:uuids) " + " group by d.seriale")
  List<AssociazioneVeicoliCounterDTO> getAssociazioneVeicoliCounter(@Param("uuids") List<String> uuids);

  Optional<AssociazioneDV> findByDispositivo_Seriale(String seriale);

  @Query("select distinct ad " + "from AssociazioneDV ad " + "join ad.dispositivo d " + "join d.richiestas r " + "join r.contratto c "
         + "join c.clienteFai cf " + "join d.tipoDispositivo td " + "where r.stato = :stato " + "and cf.codiceCliente = :codiceAzienda "
         + "and ( td.nome = 'TELEPASS_EUROPEO_SAT' or " + " td.nome = 'TELEPASS_EUROPEO' ) "
         + "and (r.nomeServizio like '%PEDAGGI_AUSTRIA%' OR r.nomeServizio like '%PEDAGGI_BELGIO%')")
  List<AssociazioneDV> findAllWithRichiestaStatoAndCodiceAzienda(@Param("stato") StatoRichiesta statoRichiesta,
                                                                 @Param("codiceAzienda") String codiceAzienda);

  @Query("select distinct ad " + "from AssociazioneDV ad " + "join ad.dispositivo d " + "join d.richiestas r " + "join r.contratto c "
         + "join d.tipoDispositivo td " + "where r.stato = :stato " + "and d.id = :dispositivoId "
         + "and ( td.nome = 'TELEPASS_EUROPEO_SAT' or " + " td.nome = 'TELEPASS_EUROPEO' ) "
         + "and (r.nomeServizio like '%PEDAGGI_AUSTRIA%' OR r.nomeServizio like '%PEDAGGI_BELGIO%')")
  List<AssociazioneDV> findAllWithRichiestaStatoAndDispositivo(@Param("stato") StatoRichiesta statoRichiesta,
                                                               @Param("dispositivoId") Long dispositivoId);

  @Query("select d from AssociazioneDV ad join ad.dispositivo d join d.tipoDispositivo td join fetch d.statoDispositivoServizios sds where ad.uuidVeicolo = :uuidVehicle and td.nome in (:deviceType) and d.stato not in (:statusDevice)")
  List<Dispositivo> findByUuidVeicoloAndTipoDispositivoNomeInAndStatoDispositivoNotIn(@Param("uuidVehicle") String _vehicleUuid,
                                                                 @Param("deviceType") Set<TipoDispositivoEnum> devicesTypeName, @Param("statusDevice") Set<StatoDispositivo> status);

  List<AssociazioneDV> findByDispositivoOrderByDataDescIdDesc(Dispositivo dispositivo);

  @Query("SELECT new it.fai.ms.efservice.web.rest.vm.NumeroDispositiviVM(count(ad),cl.codiceCliente) FROM AssociazioneDV ad JOIN ad.dispositivo d JOIN d.contratto c JOIN c.clienteFai cl WHERE ad.uuidVeicolo = :uuidVehicle AND c.clienteFai = :clienteFai AND d.stato in (:statusDevice) GROUP BY cl.codiceCliente") 
  List<NumeroDispositiviVM> countDispositiviPerVeicolo(@Param("uuidVehicle") String vehicleUuid, @Param("clienteFai") ClienteFai clienteFai, @Param("statusDevice") Collection<StatoDispositivo> statiDispositivo);

}
