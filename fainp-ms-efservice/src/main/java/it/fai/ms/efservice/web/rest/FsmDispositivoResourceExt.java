package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.fsm.FsmResetService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(FsmDispositivoResourceExt.BASE_PATH)
public class FsmDispositivoResourceExt {

  private final Logger log = LoggerFactory.getLogger(FsmDispositivoResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  public static final String FSM_DISPOSITIVO = "/dispositivo/statemachine";

  public static final String AVAILABLE_COMMAND = "/dispositivo/availablecommand";

  public static final String FSM_RESET_STATE = "/dispositivo/fsmreset";

  public static final String FSM_STATE = "/dispositivo/fsmstate";
  
  public static final String ALL_STATUS_DISPOSITIVO = "/dispositivo/allstatus";

  private final FsmAsyncService            fsmAsyncService;
  private final AsyncPollingJobServiceImpl asyncPollingJob;
  private final FsmDispositivo             fsmDispositivo;
  private final DispositivoServiceExt      deviceService;
  private final FsmResetService            fsmResetService;

  public FsmDispositivoResourceExt(FsmAsyncService _fsmAsyncService, AsyncPollingJobServiceImpl _asyncPollingJob,
                                   final FsmDispositivo _fsmDispositivo, final DispositivoServiceExt _deviceService,
                                   final FsmResetService _fsmResetService) {
    fsmAsyncService = _fsmAsyncService;
    asyncPollingJob = _asyncPollingJob;
    fsmDispositivo = _fsmDispositivo;
    deviceService = _deviceService;
    fsmResetService = _fsmResetService;
  }

  @PostMapping(FSM_DISPOSITIVO + "/{identificativo}")
  @Timed
  public ResponseEntity<Boolean> changeStatusToDispositivo(@PathVariable String identificativo,
                                                           @RequestParam(required = true) DispositivoEvent command,
                                                           @RequestParam(required = false) String nota) throws CustomException,
                                                                                                        InterruptedException {
    long ti = System.currentTimeMillis();
    if (StringUtils.isBlank(identificativo)) {
      log.error("Identificativo Dispositivo NULL");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    if (command == null) {
      log.error("Command to execute is NULL");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String identificativoJob = fsmAsyncService.getKeyCacheAndPushDefaultValue("JHIPSTER");

    FsmDispositivoDTO fsmDTO = new FsmDispositivoDTO();
    Set<String> identificativi = new HashSet<>();
    identificativi.add(identificativo);
    fsmDTO.setIdentificativi(identificativi);
    fsmDTO.setOperazione(command.name());
    fsmDTO.setNota(nota);

    log.info("Dispositivo: [Identificativo: {}] - Command: {} - Nota: {}", identificativo, command, nota);
    fsmAsyncService.changeAsyncFsmDispositivo(identificativoJob, fsmDTO);

    StateChangeStatusFsm state = asyncPollingJob.pollingRichiesteApiGateway(identificativoJob);

    boolean valueToReturn = true;
    if (state == null || state != StateChangeStatusFsm.SUCCESS) {
      log.warn("Not changed status. Polling result on identificativo job {} : {}", identificativoJob, state);
      valueToReturn = false;
    }

    long tf = System.currentTimeMillis();
    log.debug("FSM Richiesta Resource Ext Change status Richiesta in: {} ms", (tf - ti));
    return ResponseEntity.ok(valueToReturn);
  }

  /**
   * GET /useravailablecommand/id/{id} : get user command available.
   *
   * @return the List<String> with status 200 (OK) and the list of String of command
   */
  @GetMapping(AVAILABLE_COMMAND + "/identifier/{identificativo}")
  @Timed
  public List<String> getAllAvailableCommandByIdentificativo(@PathVariable String identificativo) {
    log.debug("REST request to get user command available for Device by identifier: {}", identificativo);

    Dispositivo device = deviceService.findOneByIdentificativo(identificativo);
    Collection<DispositivoEvent> allAvailableCommand = fsmDispositivo.getAllAvailableCommand(device);

    List<String> availableCommand = allAvailableCommand.stream()
                                                       .map(event -> event.name())
                                                       .collect(toList());

    log.debug("Found these available commands: {}", availableCommand);
    return availableCommand;
  }

  @PostMapping(FSM_RESET_STATE + "/{identificativo}/{stato}")
  @Timed
  public ResponseEntity<String> getAvailableCommandByIdentificativoRichiesta(@PathVariable String identificativo,
                                                                             @PathVariable StatoDispositivo stato) throws CustomException {

    if (StringUtils.isBlank(identificativo) || stato == null) {
      log.error("Identificativo {} or stato {} is not valid", identificativo, stato);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String newContextFsm = fsmResetService.resetStateFsmDispositivo(identificativo, stato);
    if (newContextFsm != null) {
      Dispositivo device = deviceService.findOneByIdentificativo(identificativo);
      String operazioniPossibiliDispositivo = fsmDispositivo.getOperazioniPossibiliDispositivo(device);
      deviceService.save(device.operazioniPossibili(operazioniPossibiliDispositivo));
      log.info("Saved on device {} the operations possible: {}", identificativo, operazioniPossibiliDispositivo);
    } else {
      log.error("Exception on reset FSM for identificativo: {}", identificativo);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.EXCEPTION_RESET_STATE_FSM);
    }
    return ResponseEntity.ok(newContextFsm);
  }

  @PostMapping(FSM_STATE + "/{identificativo}")
  @Timed
  public ResponseEntity<String> getStateFsm(@PathVariable String identificativo) throws CustomException {
    if (StringUtils.isBlank(identificativo)) {
      log.error("Identificativo {} is not valid", identificativo);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }
    String state = fsmResetService.getStateFsmDevice(identificativo);
    return ResponseEntity.ok(state);
  }
  
  @GetMapping(ALL_STATUS_DISPOSITIVO)
  @Timed
  public ResponseEntity<List<StatoDispositivo>> getAllStatusDevice() throws CustomException {
    StatoDispositivo[] valuesAllStatus = StatoDispositivo.values();
    List<StatoDispositivo> allStatusDevice = new ArrayList<>();
    for (int i = 0; i < valuesAllStatus.length; i++) {
      StatoDispositivo statusDevice = valuesAllStatus[i];
      allStatusDevice.add(statusDevice);
    }
    log.debug("All StatusDevice: {}", allStatusDevice);
    return ResponseEntity.ok(allStatusDevice);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
