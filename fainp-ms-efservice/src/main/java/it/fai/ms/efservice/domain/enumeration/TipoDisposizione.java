package it.fai.ms.efservice.domain.enumeration;

/**
 * The TipoDisposizione enumeration.
 */
public enum TipoDisposizione {
    ACQUISITO,  DA_ACQUISIRE
}
