package it.fai.ms.efservice.service.fsm.config.tollcollect;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.config.FsmModificaSimpleConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTollCollectConfig.MODIFICA_TOLLCOLLECT)
public class FsmModificaTollCollectConfig extends FsmModificaSimpleConfig {
  
  public static final String MODIFICA_TOLLCOLLECT = "modificaTollCollect";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaTollCollectConfig(FsmSenderToQueue fsmSenderToQueue) {
    super(fsmSenderToQueue);
  }
  
  @Override
  protected String getMachineId() {
    return FsmType.MODIFICA_TOLL_COLLECT.fsmName();
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaTollCollect();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
