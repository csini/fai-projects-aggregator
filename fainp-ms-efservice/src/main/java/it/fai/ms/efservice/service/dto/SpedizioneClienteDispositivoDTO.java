package it.fai.ms.efservice.service.dto;

import it.fai.ms.efservice.web.rest.errors.PayloadError;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class SpedizioneClienteDispositivoDTO {
  private String identificativo;
  private String targa;
  private String tipoDispositivo;
  private String seriale;
  private Instant dataRichiestaDispositivo;
  private Instant dataAccettazioneRichiesta;
  private List<PayloadError.Error> anomalieDispositivi = new ArrayList<>();
  private List<PayloadError.Error> anomalieCRM = new ArrayList<>();
  private Boolean forzaturaDispositivo = false;

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public Instant getDataRichiestaDispositivo() {
    return dataRichiestaDispositivo;
  }

  public void setDataRichiestaDispositivo(Instant dataRichiestaDispositivo) {
    this.dataRichiestaDispositivo = dataRichiestaDispositivo;
  }

  public Instant getDataAccettazioneRichiesta() {
    return dataAccettazioneRichiesta;
  }

  public void setDataAccettazioneRichiesta(Instant dataAccettazioneRichiesta) {
    this.dataAccettazioneRichiesta = dataAccettazioneRichiesta;
  }

  public List<PayloadError.Error> getAnomalieDispositivi() {
    return anomalieDispositivi;
  }

  public void setAnomalieDispositivi(List<PayloadError.Error> anomalieDispositivi) {
    this.anomalieDispositivi = anomalieDispositivi;
  }

  public List<PayloadError.Error> getAnomalieCRM() {
    return anomalieCRM;
  }

  public void setAnomalieCRM(List<PayloadError.Error> anomalieCRM) {
    this.anomalieCRM = anomalieCRM;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Boolean getForzaturaDispositivo() {
    return forzaturaDispositivo;
  }

  public void setForzaturaDispositivo(Boolean forzaturaDispositivo) {
    this.forzaturaDispositivo = forzaturaDispositivo;
  }

  public Boolean getChecked() {
    if(forzaturaDispositivo != null  && forzaturaDispositivo){
      return true;
    }
    if(anomalieDispositivi == null || anomalieDispositivi.isEmpty())
      return true;

    return false;
  }

  public Boolean getDisabled() {
    return getChecked();
  }
}
