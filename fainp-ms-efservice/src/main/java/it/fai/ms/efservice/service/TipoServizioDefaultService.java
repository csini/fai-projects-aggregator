package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.TipoServizioDefaultDTO;
import java.util.List;

/**
 * Service Interface for managing TipoServizioDefault.
 */
public interface TipoServizioDefaultService {

    /**
     * Save a tipoServizioDefault.
     *
     * @param tipoServizioDefaultDTO the entity to save
     * @return the persisted entity
     */
    TipoServizioDefaultDTO save(TipoServizioDefaultDTO tipoServizioDefaultDTO);

    /**
     *  Get all the tipoServizioDefaults.
     *
     *  @return the list of entities
     */
    List<TipoServizioDefaultDTO> findAll();

    /**
     *  Get the "id" tipoServizioDefault.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TipoServizioDefaultDTO findOne(Long id);

    /**
     *  Delete the "id" tipoServizioDefault.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
