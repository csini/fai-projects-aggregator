package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.RaggruppamentoRichiesteFornitoreView;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteFornitoreView_;

import static it.fai.ms.efservice.domain.RaggruppamentoRichiesteFornitoreView_.*;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface RaggruppamentoRichiesteFornitoreViewRepositoryExt extends
  JpaRepository<RaggruppamentoRichiesteFornitoreView, Long>,
  JpaSpecificationExecutor<RaggruppamentoRichiesteFornitoreView> {

  List<RaggruppamentoRichiesteFornitoreView> findAllByTipoDispositivo(String tipoDispositivo);
  
  List<RaggruppamentoRichiesteFornitoreView> findAllByTipoDispositivoOrderByDataGenerazioneFileDesc(String tipoDispositivoNome);
  
  public static Specification<RaggruppamentoRichiesteFornitoreView> allOpenByTipoDispositivo(String pTipoDispositivo){
	  return (root, query, cb) -> {
		return cb.and(
				cb.equal(root.get(tipoDispositivo), pTipoDispositivo),
				cb.not(cb.equal(root.get(stato), "CHIUSO"))
				
			);
	 };
	  
  }

}
