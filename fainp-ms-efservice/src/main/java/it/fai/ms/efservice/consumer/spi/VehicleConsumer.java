package it.fai.ms.efservice.consumer.spi;

import it.fai.ms.efservice.consumer.model.VehicleMessage;

public interface VehicleConsumer {

  void consume(VehicleMessage newMessage);

}
