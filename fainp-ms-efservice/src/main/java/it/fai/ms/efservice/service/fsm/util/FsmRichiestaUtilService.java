package it.fai.ms.efservice.service.fsm.util;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.notification.LogOperazioniPublishDTO;
import it.fai.ms.common.jms.dto.notification.TipologiaOggettoDTOEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

@Service
public class FsmRichiestaUtilService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private RichiestaServiceExt   richiestaService;
  private ClienteFaiRepository  clienteFaiRepo;
  private OrdineClienteService  ordineClienteService;
  private JmsTopicSenderService senderJmsService;

  /**
   * @param _richiestaService
   * @param _clienteFaiRepo
   * @param ordineClienteService
   * @param _senderJmsService
   */
  public FsmRichiestaUtilService(RichiestaServiceExt _richiestaService, ClienteFaiRepository _clienteFaiRepo,
                                 OrdineClienteService ordineClienteService, JmsTopicSenderService _senderJmsService) {
    this.richiestaService = _richiestaService;
    this.clienteFaiRepo = _clienteFaiRepo;
    this.ordineClienteService = ordineClienteService;
    this.senderJmsService = _senderJmsService;
  }

  public boolean switchToState(Richiesta richiesta, StatoRichiesta toState, Transition<StatoRichiesta, RichiestaEvent> transition) {
    boolean isSwitch = switchToState(richiesta, toState);
    if (isSwitch) {
      Trigger<StatoRichiesta, RichiestaEvent> trigger = transition.getTrigger();
      boolean isAutoTransition = false;
      if (trigger == null || (trigger != null && trigger.getEvent()
                                                        .name()
                                                        .startsWith("MS_"))) {
        isAutoTransition = true;
      }

      boolean isAutoTx = isAutoTransition;
      sendNotificationByTransitionType(isAutoTx, richiesta, transition);

    } else {
      return false;
    }

    return true;
  }

  private boolean sendNotificationByTransitionType(boolean isAutoTx, Richiesta richiesta,
                                                   Transition<StatoRichiesta, RichiestaEvent> transition) {
    log.debug("Invio Notifica di cambio stato " + ((isAutoTx) ? "AUTOMATICO" : "MANUALE") + ".");
    StatoRichiesta stateFrom = null;
    StatoRichiesta stateTo = null;

    stateFrom = transition.getSource()
                          .getId();
    stateTo = transition.getTarget()
                        .getId();

    LocalDate now = LocalDate.now();
    boolean sent = false;
    if (isAutoTx) {
      sent = sendNotification(richiesta, now, stateFrom, stateTo);
    } else {
      String faiOperator = SecurityUtils.getCurrentUserLogin();
      sent = sendNotification(richiesta, now, stateFrom, stateTo, faiOperator);
    }
    return sent;
  }

  private boolean sendNotification(Richiesta richiesta, LocalDate now, StatoRichiesta stateFrom, StatoRichiesta stateTo) {
    return sendNotification(richiesta, now, stateFrom, stateTo, null);
  }

  private boolean sendNotification(Richiesta richiesta, LocalDate now, StatoRichiesta stateFrom, StatoRichiesta stateTo,
                                   String operatoreFai) {
    if (richiesta == null) {
      log.error("Not found Richiesta {} for change status from {} to {}", stateFrom, stateTo);
      return false;
    }

    String identificativoRichiesta = richiesta.getIdentificativo();
    String ultimaNota = richiesta.getUltimaNota();
    log.debug("INFO TRANSITION:\n\t- IdentificativoRichiesta: {}\n\t- Data: {}\n\t- Transizione => Da: [{}] A: [{}]\n\t- UltimaNota: {}\n\t- OperatoreFai: {}",
              identificativoRichiesta, new Date(), stateFrom, stateTo, ultimaNota, operatoreFai);

    // Il cliente Fai è obbligatorio a livello di Entity Richiesta;
    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    ClienteFai clienteFai = (ordineCliente != null) ? ordineCliente.getClienteAssegnatario() : null;
    if (clienteFai == null) {
      log.error("Error in codice cliente FAI: [Identificativo richiesta: {} - numeroOrdineCliente: {}]", identificativoRichiesta,
                ((ordineCliente != null) ? ordineCliente.getNumeroOrdine() : null));
      return false;
    }

    String clienteFaiCodiceCliente = clienteFai.getCodiceCliente();

    // Richiesta numero <ID_RICHIESTA> cambia stato in <STATO_DESTINAZIONE>. Autore della modifica:
    // <SISTEMA_OPPURE_OPERATORE_FAI>. Note: <NOTE>
    StringBuilder sb = new StringBuilder("Cambio stato da " + stateFrom.name() + " ");
    sb.append("a " + stateTo.name() + ". ");
    sb.append("Autore della modifica: " + ((StringUtils.isNotBlank(operatoreFai)) ? operatoreFai : "Sistema"));
    if (ultimaNota != null) {
      sb.append(". Note: " + ultimaNota);
    }
    sb.append(". [" + identificativoRichiesta + "]");
    String message = sb.toString();

    LogOperazioniPublishDTO logOperationDTO = buildLogOperationDto(clienteFaiCodiceCliente, now, message, identificativoRichiesta);
    log.debug("LogOperazioni DTO: {}", logOperationDTO);

    sendLogOperazioni(logOperationDTO);
    return true;
  }

  private void sendLogOperazioni(LogOperazioniPublishDTO logOperationDTO) {
    CompletableFuture.supplyAsync(() -> {
      try {
        senderJmsService.publishLogTopic(logOperationDTO);
      } catch (Exception eMsg) {
        throw new RuntimeException(eMsg);
      }
      return true;
    })
                     .exceptionally(e -> {
                       log.error("Exception on sending message LogOperation: ", e);
                       return false;
                     })
                     .thenAccept(dto -> {
                       if (dto) {
                         log.info("Sended message LogOperation...");
                       } else {
                         log.warn("Not send messagge...");
                       }
                     });
  }

  private LogOperazioniPublishDTO buildLogOperationDto(String clienteFaiCodiceCliente, LocalDate now, String msg,
                                                       String identificativoRichiesta) {
    LogOperazioniPublishDTO dto = new LogOperazioniPublishDTO();
    dto.setData(now);
    dto.setIdentificativo(identificativoRichiesta);
    dto.setUtenteDestinatario(clienteFaiCodiceCliente);
    dto.setMessaggio(msg);
    dto.setTipologiaOggetto(TipologiaOggettoDTOEnum.ORDINI);

    return dto;
  }

  private boolean switchToState(Richiesta richiesta, StatoRichiesta toState) {
    if (richiesta != null) {
      richiesta.setStato(toState);
      Instant dataModificaStato = Instant.now();
      richiesta.setDataModificaStato(dataModificaStato);
    } else {
      return false;
    }

    return true;
  }

}
