package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public abstract class CaricamentoOrdineFornitoreDTO implements Serializable {

  private static final long serialVersionUID = -7819866193625523812L;

  private Long id;

  private boolean skipValidation;

  private String note;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public boolean isSkipValidation() {
    return skipValidation;
  }

  public void setSkipValidation(boolean skipValidation) {
    this.skipValidation = skipValidation;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Override
  public String toString() {
    return "CaricamentoOrdineFornitoreDTO [id=" + id + ", skipValidation=" + skipValidation + ", note=" + note + "]";
  }

}
