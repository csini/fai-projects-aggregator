package it.fai.ms.efservice.service.jms.listener.device;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceLostMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceStolenMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceStolenConsumer;

@Service
@Transactional
public class JmsListenerDeviceStolen implements MessageListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceStolenConsumer consumer;

  @Autowired
  public JmsListenerDeviceStolen(DeviceStolenConsumer _consumer) throws Exception {
    consumer = _consumer;
  }

  @Override
  public void onMessage(final Message _message) {
    DeviceStolenMessage deviceStolenMessage = null;
    try {
      try {
        deviceStolenMessage = (DeviceStolenMessage) ((ObjectMessage) _message).getObject();
        _log.info("Received jms message {}", deviceStolenMessage);
        consumer.consume(deviceStolenMessage);
      } catch (@SuppressWarnings("unused") final ClassCastException _e) {
        _log.warn("JMS ObjectMessage isn't {}", DeviceLostMessage.class.getSimpleName());
      }
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

}