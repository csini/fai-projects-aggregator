package it.fai.ms.efservice.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.repository.AnagraficaGiacenzaRepository;
import it.fai.ms.efservice.repository.OrdineFornitoreRepository;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import it.fai.ms.efservice.service.jms.dml.OrdineFornitoreDepositoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.OrdineFornitoreDepositoDmlSenderMapper;
import it.fai.ms.efservice.service.mapper.OrdineFornitoreMapper;

/**
 * Service Implementation for managing OrdineFornitore.
 */
@Service
@Transactional
public class OrdineFornitoreService {

  private final Logger log = LoggerFactory.getLogger(OrdineFornitoreService.class);

  private final OrdineFornitoreRepository ordineFornitoreRepository;
  
  private final AnagraficaGiacenzaRepository anagraficaGiacenzaRepository;

  private final OrdineFornitoreMapper ordineFornitoreMapper;

  private final OrdineFornitoreDepositoDmlSenderUtil ordineFornitoreDepositoDmlSenderUtil;

  public OrdineFornitoreService(OrdineFornitoreRepository ordineFornitoreRepository, OrdineFornitoreMapper ordineFornitoreMapper,
                                AnagraficaGiacenzaRepository anagraficaGiacenzaRepository,
                                JmsProperties jmsProperties,
                                OrdineFornitoreDepositoDmlSenderMapper ordineFornitoreDepositoDmlSenderMapper) {
    this.ordineFornitoreRepository = ordineFornitoreRepository;
    this.anagraficaGiacenzaRepository = anagraficaGiacenzaRepository;
    this.ordineFornitoreMapper = ordineFornitoreMapper;
    ordineFornitoreDepositoDmlSenderUtil = new OrdineFornitoreDepositoDmlSenderUtil(jmsProperties, ordineFornitoreDepositoDmlSenderMapper);
  }

  /**
   * Save a ordineFornitore.
   *
   * @param ordineFornitoreDTO
   *          the entity to save
   * @return the persisted entity
   */
  public OrdineFornitoreDTO save(OrdineFornitoreDTO ordineFornitoreDTO) {
    log.debug("Request to save OrdineFornitore : {}", ordineFornitoreDTO);
    OrdineFornitore ordineFornitore = ordineFornitoreMapper.toEntity(ordineFornitoreDTO);
    AnagraficaGiacenza anagraficaGiacenza =  ordineFornitoreDTO.getAnagraficaGiacenzaId()!=null ? anagraficaGiacenzaRepository.findOne(ordineFornitoreDTO.getAnagraficaGiacenzaId()) : null;
    ordineFornitore.setAnagraficaGiacenza(anagraficaGiacenza);
    ordineFornitore = ordineFornitoreRepository.saveAndFlush(ordineFornitore);

    // Send notification DML
    ordineFornitoreDepositoDmlSenderUtil.sendSaveNotification(ordineFornitore);

    return ordineFornitoreMapper.toDto(ordineFornitore);
  }

  /**
   * Get all the ordineFornitores.
   *
   * @return the list of entities
   */
  @Transactional(readOnly = true)
  public List<OrdineFornitoreDTO> findAll() {
    log.debug("Request to get all OrdineFornitores");
    return ordineFornitoreRepository.findAll()
                                    .stream()
                                    .map(ordineFornitoreMapper::toDto)
                                    .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get one ordineFornitore by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Transactional(readOnly = true)
  public OrdineFornitoreDTO findOne(Long id) {
    log.debug("Request to get OrdineFornitore : {}", id);
    OrdineFornitore ordineFornitore = ordineFornitoreRepository.findOne(id);
    return ordineFornitoreMapper.toDto(ordineFornitore);
  }

  /**
   * Delete the ordineFornitore by id.
   *
   * @param id
   *          the id of the entity
   */
  public void delete(Long id) {
    log.debug("Request to delete OrdineFornitore : {}", id);
    ordineFornitoreRepository.delete(id);
  }

}
