package it.fai.ms.efservice.dto;

import java.time.LocalDate;

public class ResponseTessereFrejusDto {
	private String nomeFile;
	private String numeroTessera;
	private LocalDate dataScadenza;
	private String targa;
	private String classeEuro;
	private String nomeCliente;
	private LocalDate attivazione;
	private String categoriaVeicolo;
	private String nazioneTarga;

	public String getNumeroTessera() {
		return numeroTessera;
	}
	public void setNumeroTessera(String numeroTessera) {
		this.numeroTessera = numeroTessera;
	}
	public LocalDate getDataScadenza() {
		return dataScadenza;
	}
	public void setDataScadenza(LocalDate dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	public String getTarga() {
		return targa;
	}
	public void setTarga(String targa) {
		this.targa = targa;
	}
	public String getClasseEuro() {
		return classeEuro;
	}
	public void setClasseEuro(String classeEuro) {
		this.classeEuro = classeEuro;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public LocalDate getAttivazione() {
		return attivazione;
	}
	public void setAttivazione(LocalDate attivazione) {
		this.attivazione = attivazione;
	}
	public String getCategoriaVeicolo() {
		return categoriaVeicolo;
	}
	public void setCategoriaVeicolo(String categoriaVeicolo) {
		this.categoriaVeicolo = categoriaVeicolo;
	}
	public String getNazioneTarga() {
		return nazioneTarga;
	}
	public void setNazioneTarga(String nazioneTarga) {
		this.nazioneTarga = nazioneTarga;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attivazione == null) ? 0 : attivazione.hashCode());
		result = prime * result + ((categoriaVeicolo == null) ? 0 : categoriaVeicolo.hashCode());
		result = prime * result + ((classeEuro == null) ? 0 : classeEuro.hashCode());
		result = prime * result + ((dataScadenza == null) ? 0 : dataScadenza.hashCode());
		result = prime * result + ((nazioneTarga == null) ? 0 : nazioneTarga.hashCode());
		result = prime * result + ((nomeCliente == null) ? 0 : nomeCliente.hashCode());
		result = prime * result + ((numeroTessera == null) ? 0 : numeroTessera.hashCode());
		result = prime * result + ((targa == null) ? 0 : targa.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseTessereFrejusDto other = (ResponseTessereFrejusDto) obj;
		if (attivazione == null) {
			if (other.attivazione != null)
				return false;
		} else if (!attivazione.equals(other.attivazione))
			return false;
		if (categoriaVeicolo == null) {
			if (other.categoriaVeicolo != null)
				return false;
		} else if (!categoriaVeicolo.equals(other.categoriaVeicolo))
			return false;
		if (classeEuro == null) {
			if (other.classeEuro != null)
				return false;
		} else if (!classeEuro.equals(other.classeEuro))
			return false;
		if (dataScadenza == null) {
			if (other.dataScadenza != null)
				return false;
		} else if (!dataScadenza.equals(other.dataScadenza))
			return false;
		if (nazioneTarga == null) {
			if (other.nazioneTarga != null)
				return false;
		} else if (!nazioneTarga.equals(other.nazioneTarga))
			return false;
		if (nomeCliente == null) {
			if (other.nomeCliente != null)
				return false;
		} else if (!nomeCliente.equals(other.nomeCliente))
			return false;
		if (numeroTessera == null) {
			if (other.numeroTessera != null)
				return false;
		} else if (!numeroTessera.equals(other.numeroTessera))
			return false;
		if (targa == null) {
			if (other.targa != null)
				return false;
		} else if (!targa.equals(other.targa))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ResponseDto [numeroTessera=" + numeroTessera + ", dataScadenza=" + dataScadenza + ", targa=" + targa
				+ ", classeEuro=" + classeEuro + ", nomeCliente=" + nomeCliente + ", attivazione=" + attivazione
				+ ", categoriaVeicolo=" + categoriaVeicolo + ", nazioneTarga=" + nazioneTarga + "]";
	}
	public String getNomeFile() {
		return nomeFile;
	}
	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

}
