package it.fai.ms.efservice.dto;

import java.io.Serializable;

public class FsmCommandDTO implements Serializable {

  private static final long serialVersionUID = -7279859461398211176L;

  private String identificativo;
  
  private String errorMessage;
  
  private String operazione;

  public String getIdentificativo() {
    return identificativo;
  }
  
  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public FsmCommandDTO identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public FsmCommandDTO errorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getOperazione() {
    return operazione;
  }
  
  public FsmCommandDTO operazione(String operazione) {
    this.operazione = operazione;
    return this;
  }

  public void setOperazione(String operazione) {
    this.operazione = operazione;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("FsmCommandDTO [identificativO=");
    builder.append(identificativo);
    builder.append(", errorMessage=");
    builder.append(errorMessage);
    builder.append(", operazione=");
    builder.append(operazione);
    builder.append("]");
    return builder.toString();
  }
  
}
