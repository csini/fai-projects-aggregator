package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;

public interface DeliveryAsyncServiceExt {
  void processAsyncDelivery(String identificativoJob, DeliveryPrintDTO dto);
  void processAsyncDeliveryProduttore(String identificativoJob, DeliveryPrintDTO dto);
  String getKeyCacheAndPushDefaultValue(String key);
}
