package it.fai.ms.efservice.domain;

import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.common.dml.AbstractDml;
import it.fai.ms.efservice.domain.enumeration.StatoAzienda;

/**
 * A ClienteFai.
 */
@Entity
@Table(name = "cliente_fai")
public class ClienteFai implements AbstractDml {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "codice_cliente", nullable = false)
  private String codiceCliente;

  @Column(name = "email")
  private String email;

  @Enumerated(EnumType.STRING)
  @Column(name = "stato")
  private StatoAzienda stato;

  @Column(name = "numero_account")
  private Long numeroAccount;

  @Column(name = "ragione_sociale")
  private String ragioneSociale;

  @Column(name = "codice_fiscale")
  private String codiceFiscale;

  @Column(name = "partita_iva")
  private String partitaIva;

  @Column(name = "codice_agente")
  private String codiceAgente;

  @Column(name = "raggruppamento_impresa")
  private String raggruppamentoImpresa;

  @Column(name = "descrizione_raggruppamento_impresa")
  private String descrizioneRaggruppamentoImpresa;

  @Column(name = "consorzio_padre")
  private Boolean consorzioPadre;

  @NotNull
  @Column(name = "via", nullable = false)
  private String via;

  @NotNull
  @Column(name = "citta", nullable = false)
  private String citta;

  @Column(name = "cap")
  private String cap;

  @Column(name = "provincia")
  private String provincia;

  @Size(max = 2)
  @Column(name = "paese", length = 2)
  private String paese;

  @Column(name = "email_legale_rappresentante")
  private String emailLegaleRappresentante;

  @Column(name = "nome_legale_rappresentante")
  private String nomeLegaleRappresentante;

  @Column(name = "cognome_legale_rappresentante")
  private String cognomeLegaleRappresentante;

  @Column(name = "contatto_telefonico_legale_rappresentante")
  private String contattoTelefonicoLegaleRappresentante;

  /*
   * TO REMOVE: dato non più necessario
   */
  @Deprecated
  @Column(name = "codice_cliente_fatturazione")
  private String codiceClienteFatturazione;

  @NotNull
  @Column(name = "dml_revision_timestamp", nullable = false)
  private Instant dmlRevisionTimestamp;

  @Column(name = "nome_intestanzione_tessere")
  private String nomeIntestazioneTessere;

  @OneToMany(mappedBy = "clienteFai")
  @JsonIgnore
  private Set<Contratto> contrattos = new HashSet<>();

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public ClienteFai codiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
    return this;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getEmail() {
    return email;
  }

  public ClienteFai email(String email) {
    this.email = email;
    return this;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public StatoAzienda getStato() {
    return stato;
  }

  public ClienteFai stato(StatoAzienda stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(StatoAzienda stato) {
    this.stato = stato;
  }

  public Long getNumeroAccount() {
    return numeroAccount;
  }

  public ClienteFai numeroAccount(Long numeroAccount) {
    this.numeroAccount = numeroAccount;
    return this;
  }

  public void setNumeroAccount(Long numeroAccount) {
    this.numeroAccount = numeroAccount;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public ClienteFai ragioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
    return this;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCodiceFiscale() {
    return codiceFiscale;
  }

  public ClienteFai codiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
    return this;
  }

  public void setCodiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
  }

  public String getPartitaIva() {
    return partitaIva;
  }

  public ClienteFai partitaIva(String partitaIva) {
    this.partitaIva = partitaIva;
    return this;
  }

  public void setPartitaIva(String partitaIva) {
    this.partitaIva = partitaIva;
  }

  public String getCodiceAgente() {
    return codiceAgente;
  }

  public ClienteFai codiceAgente(String codiceAgente) {
    this.codiceAgente = codiceAgente;
    return this;
  }

  public void setCodiceAgente(String codiceAgente) {
    this.codiceAgente = codiceAgente;
  }

  public String getRaggruppamentoImpresa() {
    return raggruppamentoImpresa;
  }

  public ClienteFai raggruppamentoImpresa(String raggruppamentoImpresa) {
    this.raggruppamentoImpresa = raggruppamentoImpresa;
    return this;
  }

  public void setRaggruppamentoImpresa(String raggruppamentoImpresa) {
    this.raggruppamentoImpresa = raggruppamentoImpresa;
  }

  public String getDescrizioneRaggruppamentoImpresa() {
    return descrizioneRaggruppamentoImpresa;
  }

  public ClienteFai descrizioneRaggruppamentoImpresa(String descrizioneRaggruppamentoImpresa) {
    this.descrizioneRaggruppamentoImpresa = descrizioneRaggruppamentoImpresa;
    return this;
  }

  public void setDescrizioneRaggruppamentoImpresa(String descrizioneRaggruppamentoImpresa) {
    this.descrizioneRaggruppamentoImpresa = descrizioneRaggruppamentoImpresa;
  }

  public Boolean isConsorzioPadre() {
    return consorzioPadre;
  }

  public ClienteFai consorzioPadre(Boolean consorzioPadre) {
    this.consorzioPadre = consorzioPadre;
    return this;
  }

  public void setConsorzioPadre(Boolean consorzioPadre) {
    this.consorzioPadre = consorzioPadre;
  }

  public String getVia() {
    return via;
  }

  public ClienteFai via(String via) {
    this.via = via;
    return this;
  }

  public void setVia(String via) {
    this.via = via;
  }

  public String getCitta() {
    return citta;
  }

  public ClienteFai citta(String citta) {
    this.citta = citta;
    return this;
  }

  public void setCitta(String citta) {
    this.citta = citta;
  }

  public String getCap() {
    return cap;
  }

  public ClienteFai cap(String cap) {
    this.cap = cap;
    return this;
  }

  public void setCap(String cap) {
    this.cap = cap;
  }

  public String getProvincia() {
    return provincia;
  }

  public ClienteFai provincia(String provincia) {
    this.provincia = provincia;
    return this;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public String getPaese() {
    return paese;
  }

  public ClienteFai paese(String paese) {
    this.paese = paese;
    return this;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public String getEmailLegaleRappresentante() {
    return emailLegaleRappresentante;
  }

  public ClienteFai emailLegaleRappresentante(String emailLegaleRappresentante) {
    this.emailLegaleRappresentante = emailLegaleRappresentante;
    return this;
  }

  public void setEmailLegaleRappresentante(String emailLegaleRappresentante) {
    this.emailLegaleRappresentante = emailLegaleRappresentante;
  }

  public String getNomeLegaleRappresentante() {
    return nomeLegaleRappresentante;
  }

  public ClienteFai nomeLegaleRappresentante(String nomeLegaleRappresentante) {
    this.nomeLegaleRappresentante = nomeLegaleRappresentante;
    return this;
  }

  public void setNomeLegaleRappresentante(String nomeLegaleRappresentante) {
    this.nomeLegaleRappresentante = nomeLegaleRappresentante;
  }

  public String getCognomeLegaleRappresentante() {
    return cognomeLegaleRappresentante;
  }

  public ClienteFai cognomeLegaleRappresentante(String cognomeLegaleRappresentante) {
    this.cognomeLegaleRappresentante = cognomeLegaleRappresentante;
    return this;
  }

  public void setCognomeLegaleRappresentante(String cognomeLegaleRappresentante) {
    this.cognomeLegaleRappresentante = cognomeLegaleRappresentante;
  }

  public String getContattoTelefonicoLegaleRappresentante() {
    return contattoTelefonicoLegaleRappresentante;
  }

  public ClienteFai contattoTelefonicoLegaleRappresentante(String contattoTelefonicoLegaleRappresentante) {
    this.contattoTelefonicoLegaleRappresentante = contattoTelefonicoLegaleRappresentante;
    return this;
  }

  public void setContattoTelefonicoLegaleRappresentante(String contattoTelefonicoLegaleRappresentante) {
    this.contattoTelefonicoLegaleRappresentante = contattoTelefonicoLegaleRappresentante;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public ClienteFai codiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
    return this;
  }

  public void setCodiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
  }

  @Override
  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public ClienteFai dmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
    return this;
  }

  public void setDmlUniqueIdentifier(String dmlUniqueIdentifier) {
    this.codiceCliente = dmlUniqueIdentifier;
  }

  @Override
  public String getDmlUniqueIdentifier() {
    return codiceCliente;
  }

  public ClienteFai dmlUniqueIdentifier(String dmlUniqueIdentifier) {
    this.codiceCliente = dmlUniqueIdentifier;
    return this;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  public String getNomeIntestazioneTessere() {
    return nomeIntestazioneTessere;
  }
  
  public ClienteFai nomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
    return this;
  }

  public void setNomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
  }

  public Set<Contratto> getContrattos() {
    return contrattos;
  }

  public ClienteFai contrattos(Set<Contratto> contrattos) {
    this.contrattos = contrattos;
    return this;
  }

  public ClienteFai addContratto(Contratto contratto) {
    this.contrattos.add(contratto);
    contratto.setClienteFai(this);
    return this;
  }

  public ClienteFai removeContratto(Contratto contratto) {
    this.contrattos.remove(contratto);
    contratto.setClienteFai(null);
    return this;
  }

  public void setContrattos(Set<Contratto> contrattos) {
    this.contrattos = contrattos;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClienteFai clienteFai = (ClienteFai) o;
    if (clienteFai.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), clienteFai.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "ClienteFai{" + "id=" + getId() + ", codiceCliente='" + getCodiceCliente() + "'" + ", email='" + getEmail() + "'" + ", stato='"
           + getStato() + "'" + ", numeroAccount=" + getNumeroAccount() + ", ragioneSociale='" + getRagioneSociale() + "'"
           + ", codiceFiscale='" + getCodiceFiscale() + "'" + ", partitaIva='" + getPartitaIva() + "'" + ", codiceAgente='"
           + getCodiceAgente() + "'" + ", raggruppamentoImpresa='" + getRaggruppamentoImpresa() + "'"
           + ", descrizioneRaggruppamentoImpresa='" + getDescrizioneRaggruppamentoImpresa() + "'" + ", consorzioPadre='"
           + isConsorzioPadre() + "'" + ", via='" + getVia() + "'" + ", citta='" + getCitta() + "'" + ", cap='" + getCap() + "'"
           + ", provincia='" + getProvincia() + "'" + ", paese='" + getPaese() + "'" + ", emailLegaleRappresentante='"
           + getEmailLegaleRappresentante() + "'" + ", nomeLegaleRappresentante='" + getNomeLegaleRappresentante() + "'"
           + ", cognomeLegaleRappresentante='" + getCognomeLegaleRappresentante() + "'" + ", contattoTelefonicoLegaleRappresentante='"
           + getContattoTelefonicoLegaleRappresentante() + "'" + ", codiceClienteFatturazione='" + getCodiceClienteFatturazione() + "'"
           + ", dmlRevisionTimestamp='" + getDmlRevisionTimestamp() + "'" + ", nomeIntestazioneTessere='" + getNomeIntestazioneTessere() + "'" + "}";
  }
}
