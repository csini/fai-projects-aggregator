package it.fai.ms.efservice.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;


@RestController
@RequestMapping(OrdrcGeneratorDebug.BASE_PATH)
public class OrdrcGeneratorDebug {
	private final Logger log = LoggerFactory.getLogger(getClass());

	public final static String BASE_PATH ="/api/public/ordrc";

	private final FsmSenderToQueue telepassSender;
	private final RichiestaRepository richiestaService;

	public OrdrcGeneratorDebug(RichiestaRepository richiestaService, FsmSenderToQueue senderFsmService) {
		this.telepassSender = senderFsmService;
		this.richiestaService = richiestaService;
	}

	@GetMapping("resubmit/{id}")
	public void reSendRequest(@PathVariable("id")long id) {
		log.info("resubmit Order to telepass for request id {}",id);
		Richiesta request = richiestaService.findOne(id);
		log.debug("found request {}",request);
		if(request == null)
		  throw new IllegalArgumentException("Request not found " + id );
		telepassSender.sendMessageTo(request);

	}
}
