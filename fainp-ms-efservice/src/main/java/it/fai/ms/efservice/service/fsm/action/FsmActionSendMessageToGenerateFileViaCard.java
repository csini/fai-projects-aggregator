package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;

public class FsmActionSendMessageToGenerateFileViaCard implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceProducerService deviceProducerService;

  public FsmActionSendMessageToGenerateFileViaCard(final DeviceProducerService _deviceProducerService) {
    deviceProducerService = _deviceProducerService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        final Richiesta richiesta = (Richiesta) object;
        log.info("Device ordering... {}", richiesta);
        deviceProducerService.deviceOrdering(richiesta);
      } else {
        log.warn("Object {} is not {}", object, Richiesta.class.getName());
      }
    } else {
      log.warn("Message is NULL...");
    }
  }

}
