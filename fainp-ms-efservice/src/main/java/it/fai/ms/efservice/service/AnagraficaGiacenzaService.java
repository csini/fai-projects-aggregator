package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.repository.AnagraficaGiacenzaRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.OrdineFornitoreRepository;
import it.fai.ms.efservice.service.dto.AnagraficaGiacenzaDTO;
import it.fai.ms.efservice.service.mapper.AnagraficaGiacenzaMapper;

/**
 * Service Implementation for managing AnagraficaGiacenza.
 */
@Service
@Transactional
public class AnagraficaGiacenzaService {

    private final Logger log = LoggerFactory.getLogger(AnagraficaGiacenzaService.class);

    private final AnagraficaGiacenzaRepository anagraficaGiacenzaRepository;

    private final AnagraficaGiacenzaMapper anagraficaGiacenzaMapper;

    private final DispositivoRepositoryExt dispositivoRepositoryExt;

    private final OrdineFornitoreRepository ordineFornitoreRepository;

    public AnagraficaGiacenzaService(
      AnagraficaGiacenzaRepository anagraficaGiacenzaRepository,
      AnagraficaGiacenzaMapper anagraficaGiacenzaMapper,
      DispositivoRepositoryExt dispositivoRepositoryExt,
      OrdineFornitoreRepository ordineFornitoreRepository
    ) {
      this.anagraficaGiacenzaRepository = anagraficaGiacenzaRepository;
      this.anagraficaGiacenzaMapper = anagraficaGiacenzaMapper;
      this.dispositivoRepositoryExt = dispositivoRepositoryExt;
      this.ordineFornitoreRepository = ordineFornitoreRepository;
    }

    /**
     * Save a anagraficaGiacenza.
     *
     * @param anagraficaGiacenzaDTO the entity to save
     * @return the persisted entity
     */
    public AnagraficaGiacenzaDTO save(AnagraficaGiacenzaDTO anagraficaGiacenzaDTO) {
        log.debug("Request to save AnagraficaGiacenza : {}", anagraficaGiacenzaDTO);
        AnagraficaGiacenza anagraficaGiacenza = anagraficaGiacenzaMapper.toEntity(anagraficaGiacenzaDTO);
        anagraficaGiacenza = anagraficaGiacenzaRepository.save(anagraficaGiacenza);
        return anagraficaGiacenzaMapper.toDto(anagraficaGiacenza);
    }

    /**
     * Get all the anagraficaGiacenzas.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<AnagraficaGiacenzaDTO> findAll() {
      log.debug("Request to get all AnagraficaGiacenzas");
      Map<String, Long> dispositiviInDeposito = dispositivoRepositoryExt.findAllByStato(
        StatoDispositivo.IN_DEPOSITO
      ).stream().collect(groupingBy(o -> o.getTipoDispositivo().getNome().name(), counting()));

      Map<Long, List<OrdineFornitore>> ordiniByGiacenza = ordineFornitoreRepository.findAll().stream().collect(
        groupingBy(o -> o.getAnagraficaGiacenza().getId())
      );

      return anagraficaGiacenzaRepository.findAll().stream()
        .peek(a->log.debug("in, {}", a))
        .map(anagraficaGiacenzaMapper::toDto)
        .peek(anagraficaGiacenzaDTO -> anagraficaGiacenzaDTO.setTotaleDispositiviInGiacenza(
            dispositiviInDeposito.get(anagraficaGiacenzaDTO.getTipoDispositivo())
        ))
        .peek(anagraficaGiacenzaDTO -> anagraficaGiacenzaDTO.setDataUltimoRiordino(
          Optional.ofNullable(
            ordiniByGiacenza.get(anagraficaGiacenzaDTO.getId())
          ).orElse(Lists.newArrayList()).stream().map(
            OrdineFornitore::getDataRiordino
          ).max(Instant::compareTo).orElse(null)
        ))
        .peek(a->log.debug("out, {}", a))
        .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Update a anagraficaGiacenza.
     *
     * @param anagraficaGiacenzaDTO the entity to save
     * @return the persisted entity
     */
    public void updateRiordino(Integer quantita, Long idGiacenza) {
      log.debug("updateRiordino AnagraficaGiacenza {}",quantita);

      // aggiorna la quantita riordino in giacenza (moreno)
      AnagraficaGiacenza anagraficaGiacenza = anagraficaGiacenzaRepository.findOne(idGiacenza);
      log.debug("updateRiordino AnagraficaGiacenza.getTipoDispositivo {}",anagraficaGiacenza.getTipoDispositivo());
      anagraficaGiacenza.setQuantitaUltimoRiordino(quantita);
      anagraficaGiacenzaRepository.save(anagraficaGiacenza);

    }


    /**
     * Get one anagraficaGiacenza by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public AnagraficaGiacenzaDTO findOne(Long id) {
        log.debug("Request to get AnagraficaGiacenza : {}", id);
        AnagraficaGiacenza anagraficaGiacenza = anagraficaGiacenzaRepository.findOne(id);
        return anagraficaGiacenzaMapper.toDto(anagraficaGiacenza);
    }

    /**
     * Delete the anagraficaGiacenza by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AnagraficaGiacenza : {}", id);
        anagraficaGiacenzaRepository.delete(id);
    }
}
