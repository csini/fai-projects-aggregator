package it.fai.ms.efservice.service.fsm.cacheconfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.infinispan.Cache;
import org.infinispan.CacheCollection;
import org.infinispan.CacheSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.service.fsm.serializer.FsmRichiestaSerializer;

@Component
public class FsmRichiestaInfinispanCache {

  private Logger log = LoggerFactory.getLogger(getClass());

  private FsmRichiestaSerializer     serializer;
  private FsmInfinispanConfigFactory infinispanConfig;

  private Cache<String, String> cache;

  public FsmRichiestaInfinispanCache(FsmRichiestaSerializer _serializer, FsmInfinispanConfigFactory _infinispanConfig) {
    serializer = _serializer;
    infinispanConfig = _infinispanConfig;
  }

  @PostConstruct
  public void init() {
    log.debug("Init service FSM_ORDERS Cache");
    cache = infinispanConfig.buildCache("fsm_richiestas");
  }

  public void putStateMachineContext(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    cache.put(key, json);
  }

  public String getStateMachineContext(String key) {
    String json = cache.get(key);
    log.debug("Get object from Map with Key {} Value {}", key, json);
    return json;
  }
  
  public List<String> getAllKeysFsm() {
    List<String> keys = new ArrayList<>();
    CacheSet<String> keySet = cache.keySet();
    keySet.forEach(key -> {
      keys.add(key);
    });
    return keys;
  }
  
  public String getIdStateMachine(String key) {
    String jsonSmContext = getStateMachineContext(key);

    return jsonSmContext;
  }

}

