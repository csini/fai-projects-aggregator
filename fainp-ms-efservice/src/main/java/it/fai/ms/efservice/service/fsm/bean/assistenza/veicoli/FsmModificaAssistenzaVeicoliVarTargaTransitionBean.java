package it.fai.ms.efservice.service.fsm.bean.assistenza.veicoli;

import org.springframework.statemachine.annotation.WithStateMachine; 
import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaAssistenzaVeicoliVarTarga")
public class FsmModificaAssistenzaVeicoliVarTargaTransitionBean extends AbstractFsmRichiestaTransition {
}