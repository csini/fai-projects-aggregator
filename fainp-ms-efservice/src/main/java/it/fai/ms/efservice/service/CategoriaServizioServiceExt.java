package it.fai.ms.efservice.service;

import java.util.List;

import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTOLight;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;


public interface CategoriaServizioServiceExt {
    CategoriaServizioDTO findByNome(String nomeCategoriaServizi);

    List<TipoServizioDTO> findServiziByNomeCategoria(String nomeCategoria);
    
    List<TipoDispositivoDTOLight> findTipoDispositivoByNomeCategoria(String nomeCategoria);
}
