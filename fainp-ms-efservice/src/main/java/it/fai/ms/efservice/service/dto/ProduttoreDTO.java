package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the Produttore entity.
 */
public class ProduttoreDTO implements Serializable {

  private Long id;

  private String nome;

  private String codiceFornitoreNav;

  private Set<TipoDispositivoDTO> tipiDispositivo;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCodiceFornitoreNav() {
    return codiceFornitoreNav;
  }

  public void setCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
  }

  public Set<TipoDispositivoDTO> getTipiDispositivo() {
    return tipiDispositivo;
  }

  public void setTipiDispositivo(Set<TipoDispositivoDTO> tipiDispositivo) {
    this.tipiDispositivo = tipiDispositivo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ProduttoreDTO produttoreDTO = (ProduttoreDTO) o;
    if (produttoreDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), produttoreDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "ProduttoreDTO{" + "id=" + getId() + ", nome='" + getNome() + "'" + ", codiceFornitoreNav='" + getCodiceFornitoreNav() + "'"
           + ", tipiDispositivo='" + getTipiDispositivo() + "'" + "}";
  }

}
