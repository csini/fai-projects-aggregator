package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;

public class RuleContext implements Serializable {

  private static final long serialVersionUID = -8704934745191839615L;

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;

  public RuleContext(final RuleEngineServiceType _serviceTypeRuleContext, final RuleEngineDeviceType _deviceTypeRuleContext,
                     final RuleEngineVehicle _vehicleRuleContext) {
    serviceTypeRuleContext = _serviceTypeRuleContext;
    deviceTypeRuleContext = _deviceTypeRuleContext;
    vehicleRuleContext = _vehicleRuleContext;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((RuleContext) _obj).getDeviceTypeRuleContext(), deviceTypeRuleContext)
             && Objects.equals(((RuleContext) _obj).getServiceTypeRuleContext(), serviceTypeRuleContext)
             && Objects.equals(((RuleContext) _obj).getVehicleRuleContext(), vehicleRuleContext);
    }
    return isEquals;
  }

  public RuleEngineDeviceType getDeviceTypeRuleContext() {
    return this.deviceTypeRuleContext;
  }

  public RuleEngineServiceType getServiceTypeRuleContext() {
    return this.serviceTypeRuleContext;
  }

  public RuleEngineVehicle getVehicleRuleContext() {
    return this.vehicleRuleContext;
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceTypeRuleContext, serviceTypeRuleContext, vehicleRuleContext);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleContext [deviceTypeRuleContext=");
    builder.append(deviceTypeRuleContext != null ? deviceTypeRuleContext.getId() != null ? deviceTypeRuleContext.getId()
                                                                                                                .getId()
                                                                                         : null
                                                 : null);
    builder.append(", serviceTypeRuleContext=");
    builder.append(serviceTypeRuleContext != null ? serviceTypeRuleContext.getId() != null ? serviceTypeRuleContext.getId()
                                                                                                                   .getId()
                                                                                           : null
                                                  : null);
    builder.append(", vehicleRuleContext=[");
    builder.append(vehicleRuleContext != null ? vehicleRuleContext.getId() != null ? vehicleRuleContext.getId()
                                                                                                       .getId()
                                                                                   : null
                                              : null);
    builder.append(" LicensePlate=");
    builder.append(vehicleRuleContext != null ? vehicleRuleContext.getLicensePlate() : null);
    builder.append(", Country=");
    builder.append(vehicleRuleContext != null ? vehicleRuleContext.getCountry() : null);
    builder.append(", EuroClass=");
    builder.append(vehicleRuleContext != null ? vehicleRuleContext.getEuroClass() : null);
    builder.append("] ");
    builder.append("]");
    return builder.toString();
  }

}
