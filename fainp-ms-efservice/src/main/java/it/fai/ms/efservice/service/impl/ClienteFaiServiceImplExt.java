package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import it.fai.ms.efservice.service.mapper.ClienteFaiMapper;

/**
 * Service Implementation for managing ClienteFai.
 */
@Service
@Transactional
public class ClienteFaiServiceImplExt implements ClienteFaiServiceExt {

  private final Logger log = LoggerFactory.getLogger(ClienteFaiServiceImplExt.class);

  private final ClienteFaiRepository clienteFaiRepository;

  @Autowired
  private ClienteFaiMapper clienteFaiMapper;

  public ClienteFaiServiceImplExt(ClienteFaiRepository clienteFaiRepository) {
    this.clienteFaiRepository = clienteFaiRepository;
  }

  public ClienteFai getClienteFaiByCodiceAzienda(String codiceAzienda) {
    log.debug("Get cliente Fai with codice azienda: {}", codiceAzienda);
    ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(codiceAzienda);
    return clienteFai;
  }

  public Set<Dispositivo> findDispositivoByClienteAndTipoDispositivo(String codiceAzienda, TipoDispositivoEnum tipoDispositivo) {
    ClienteFai clienteFai = getClienteFaiByCodiceAzienda(codiceAzienda);
    log.debug("Get all contratti with cliente fai {}", clienteFai);
    Set<Contratto> contrattos = clienteFai.getContrattos();
    log.debug("Get all dispositivi filter by cliente fai and tipo dispositivo {}", tipoDispositivo.name());
    Set<Dispositivo> dispositivi = contrattos.stream()
                                             .map(c -> c.getDispositivo()
                                                        .stream()
                                                        .filter(d -> d.getTipoDispositivo()
                                                                      .getNome()
                                                                      .equals(tipoDispositivo)))
                                             .flatMap(d -> d)
                                             .collect(toSet());

    log.debug("Found dispositivi: {}", dispositivi);
    return dispositivi;

  }

  @Override
  public ClienteFai findById(Long id) {
    if (id == null) {
      log.warn("Id cliente Fai passed is NULL");
      return null;
    }
    return clienteFaiRepository.findOne(id);
  }

  @Override
  public ClienteFai save(ClienteFai clienteFai){
    return clienteFaiRepository.save(clienteFai);
  }

  @Override
  public ClienteFaiDTO findDTOByCodiceCliente(String codiceCliente) {
    ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(codiceCliente);
    return clienteFaiMapper.toDto(clienteFai);
  }

  @Override
  public ClienteFai findByCodiceCliente(String codiceCliente) {
    log.debug("Find customer by code : {}", codiceCliente);
    return clienteFaiRepository.findOneByCodiceCliente(codiceCliente);
  }

  @Override
  public ClienteFaiDTO findDTOByCodiceClienteFatturazione(String codiceClienteFatturazione) {
    ClienteFai clienteFai = findByCodiceClienteFatturazione(codiceClienteFatturazione);
    return clienteFaiMapper.toDto(clienteFai);

  }

  @Override
  public ClienteFai findByCodiceClienteFatturazione(String codiceClienteFatturazione) {
    return clienteFaiRepository.findOneByCodiceClienteFatturazione(codiceClienteFatturazione);
  }

  @Override
  public List<ClienteFai> findByRaggruppamentoImpresa(String raggruppamentoImpresa) {
    return clienteFaiRepository.findByRaggruppamentoImpresa(raggruppamentoImpresa);
  }

}
