package it.fai.ms.efservice.service.dto;


import java.io.Serializable;

/**
 * A DTO for the OrdineFornitore entity.
 */
public class OrdineFornitoreDTOExt implements Serializable {

    private Integer quantita;

    private Long anagraficaGiacenzaId;

    public Integer getQuantita() {
        return quantita;
    }

    public void setQuantita(Integer quantita) {
        this.quantita = quantita;
    }

    public Long getAnagraficaGiacenzaId() {
        return anagraficaGiacenzaId;
    }

    public void setAnagraficaGiacenzaId(Long anagraficaGiacenzaId) {
        this.anagraficaGiacenzaId = anagraficaGiacenzaId;
    }

    @Override
    public String toString() {
      return "OrdineFornitoreDTOExt [quantita=" + quantita + ", anagraficaGiacenzaId=" + anagraficaGiacenzaId + "]";
    }
}
