package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class FsmActionWsAbbinamento implements Action<StatoRichiesta, RichiestaEvent> {
  
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    //TODO implements sending to topic jms;
    
    log.warn("TODO implements sending message in topic jms");
  }

}
