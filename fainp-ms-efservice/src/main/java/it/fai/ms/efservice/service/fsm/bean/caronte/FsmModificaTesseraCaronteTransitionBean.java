package it.fai.ms.efservice.service.fsm.bean.caronte;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.caronte.FsmModificaTesseraCaronte;

@WithStateMachine(id = FsmModificaTesseraCaronte.FSM_MODIFICA_TESSERA_CARONTE)
public class FsmModificaTesseraCaronteTransitionBean extends AbstractFsmRichiestaTransition {
}
