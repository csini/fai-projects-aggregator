package it.fai.ms.efservice.rules.engine.repository.entity;

import java.io.Serializable;
import java.util.Objects;

public class RuleEntityKey implements Serializable {

  private static final long serialVersionUID = 618150405397244479L;

  private String deviceTypeId;
  private String serviceTypeId;
  private String vehicleId;

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleEntityKey) _obj).getServiceTypeId(), serviceTypeId)
                 && Objects.equals(((RuleEntityKey) _obj).getDeviceTypeId(), deviceTypeId)
                 && Objects.equals(((RuleEntityKey) _obj).getVehicleId(), vehicleId);
    }
    return isEquals;
  }

  public String getDeviceTypeId() {
    return deviceTypeId;
  }

  public String getServiceTypeId() {
    return serviceTypeId;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceTypeId, deviceTypeId, vehicleId);
  }

  public void setDeviceTypeId(final String _deviceTypeId) {
    deviceTypeId = _deviceTypeId;
  }

  public void setServiceTypeId(final String _serviceTypeId) {
    serviceTypeId = _serviceTypeId;
  }

  public void setVehicleId(final String _vehicleId) {
    vehicleId = _vehicleId;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEntityKey [");
    if (serviceTypeId != null) {
      builder.append("serviceTypeId=");
      builder.append(serviceTypeId);
      builder.append(", ");
    }
    if (deviceTypeId != null) {
      builder.append("deviceTypeId=");
      builder.append(deviceTypeId);
      builder.append(", ");
    }
    if (vehicleId != null) {
      builder.append("vehicleId=");
      builder.append(vehicleId);
    }
    builder.append("]");
    return builder.toString();
  }

}
