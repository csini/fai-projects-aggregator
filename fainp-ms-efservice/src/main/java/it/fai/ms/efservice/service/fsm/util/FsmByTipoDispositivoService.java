package it.fai.ms.efservice.service.fsm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;

@Service
@Transactional
public class FsmByTipoDispositivoService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmFactory fsmFactory;

  public FsmByTipoDispositivoService(final FsmFactory _fsmFactory) {
    fsmFactory = _fsmFactory;
  }

  public AbstractFsmRichiesta getFsmNewOrderByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_GOBOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_FREJUS;
      break;
    case DARTFORD_CROSSING:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_DARTFORD_CROSSING;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_GRAN_SAN_BERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VIA_TOLL;
      break;
    case ASSISTENZA_VEICOLI:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_ASSISTENZA_VEICOLI;
      break;
    case VISPRO:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VISPRO;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_ACCETTATO_PER_INOLTRO_LIBER_T;
      break;

    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.NUOVO_ORDINE);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmMalfunzionamentoSostituzioneByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TRACKY;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_GRAN_SANBERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_RIENTRO_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo,
               TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmMalfunzionamentoByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TRACKY;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_GRAN_SANBERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_LIBER_T;
      break;
    case LOCALIZZAZIONE_SATELITTARE:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_LOCALIZZATORE_SAT;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.MALFUNZIONAMENTO);
      break;
    }
    return fsmFactory.getFsm(command);

  }

  public AbstractFsmRichiesta getFsmVariazioneTargaByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MODIFICA_FREJUS;
      break;
    case TESSERA_CARONTE:
      command = FsmCommand.CMD_MODIFICA_TESSERA_CARONTE;
      break;
    case DARTFORD_CROSSING:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_DARTFORD_CROSSING;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_GRAN_SANBERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_VIATOLL;
      break;
    case ASSISTENZA_VEICOLI:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_ASSISTENZA_VEICOLI;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_VARIAZIONE_TARGA_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.VARIAZIONE_TARGA);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmMezzoRitargatoByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_MEZZO_RITARGATO_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_MEZZO_RITARGATO_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_MEZZO_RITARGATO_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_MEZZO_RITARGATO_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_MEZZO_RITARGATO_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MODIFICA_FREJUS;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_MEZZO_RITARGATO_GRAN_SANBERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_MEZZO_RITARGATO_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_MEZZO_RITARGATO_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.MEZZO_RITARGATO);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmSmarrimentoSostituzioneByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_GRAN_SAN_BERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_SMARRIMENTO_SOST_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo,
               TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE);
      break;
    }

    if (command == null) {
      return null;
    }

    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmSmarrimentoByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case TESSERA_CARONTE:
      command = FsmCommand.CMD_MODIFICA_TESSERA_CARONTE;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_GRAN_SAN_BERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_SMARRIMENTO_NO_SOST_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.SMARRIMENTO);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmFurtoSostituzioneByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_FURTO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_FURTO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_FURTO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_FURTO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_FURTO_SOST_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_FURTO_SOST_GRAN_SAN_BERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_FURTO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_FURTO_SOST_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.FURTO_CON_SOSTITUZIONE);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmFurtoByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TELEPASS_EUROPEO:
      command = FsmCommand.CMD_FURTO_NO_SOST_TE;
      break;
    case TELEPASS_EUROPEO_SAT:
      command = FsmCommand.CMD_FURTO_NO_SOST_TE_SAT;
      break;
    case TELEPASS_ITALIANO:
      command = FsmCommand.CMD_FURTO_NO_SOST_TI;
      break;
    case VIACARD:
      command = FsmCommand.CMD_FURTO_NO_SOST_VC;
      break;
    case TRACKYCARD:
      command = FsmCommand.CMD_FURTO_NO_SOST_TRACKY;
      break;
    case GO_BOX:
      command = FsmCommand.CMD_MODIFICA_BO_BOX;
      break;
    case TOLL_COLLECT:
      command = FsmCommand.CMD_MODIFICA_TOLLCOLLECT;
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      command = FsmCommand.CMD_MALFUNZIONAMENTO_FREJUS;
      break;
    case TESSERA_CARONTE:
      command = FsmCommand.CMD_MODIFICA_TESSERA_CARONTE;
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      command = FsmCommand.CMD_FURTO_NO_SOST_GRAN_SAN_BERNARDO;
      break;
    case VIA_TOLL:
      command = FsmCommand.CMD_FURTO_NO_SOST_VIATOLL;
      break;
    case LIBER_T:
      command = FsmCommand.CMD_FURTO_NO_SOST_LIBER_T;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.FURTO);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmActiveServiceByTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {
    case TOLL_COLLECT:
      command = FsmCommand.CMD_ATTIVAZIONE_SERVIZIO_TOLLCOLLECT;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.ATTIVAZIONE_SERVIZIO);
      break;
    }
    return fsmFactory.getFsm(command);
  }

  public AbstractFsmRichiesta getFsmAccettazione() {
    return fsmFactory.getFsm(FsmCommand.CMD_IN_ACCETTAZIONE);
  }

  public AbstractFsmRichiesta getFsmDisattivazione(TipoDispositivoEnum tipoDispositivo) {
    FsmCommand command = null;
    switch (tipoDispositivo) {

    case LIBER_T:
      command = FsmCommand.CMD_RIENTRO_LIBER_T;
      break;
    case ASSISTENZA_VEICOLI:
      // Assitenza Veicoli --> FSM Variazione Targa is equal to Disattivazione
      command = FsmCommand.CMD_VARIAZIONE_TARGA_ASSISTENZA_VEICOLI;
      break;
    default:
      log.warn("Tipologia dispositivo {} non gestito per questa richiesta: {}", tipoDispositivo, TipoRichiesta.DISATTIVAZIONE);
      break;
    }
    return fsmFactory.getFsm(command);
  }

}
