package it.fai.ms.efservice.service.fsm.config.tollcollect;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActiveDevice;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardContractCodeUser;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsTollCollectMissing;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsTollCollectPresent;
import it.fai.ms.efservice.service.fsm.guard.GuardNotContractCodeUser;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroTollCollectConfig.INOLTRO_TOLLCOLLECT)
public class FsmInoltroTollCollectConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_TOLLCOLLECT = "inoltroTollCollect";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue      senderFsmService;
  private final DocumentService       documentService;
  private final FsmSenderToQueue      fsmSenderToQueue;
  private final DeviceProducerService deviceProducerService;

  public FsmInoltroTollCollectConfig(FsmSenderToQueue senderFsmService, DocumentService documentService, FsmSenderToQueue fsmSenderToQueue,
                                     DeviceProducerService deviceProducerService) {
    this.senderFsmService = senderFsmService;
    this.documentService = documentService;
    this.fsmSenderToQueue = fsmSenderToQueue;
    this.deviceProducerService = deviceProducerService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_TOLL_COLLECT.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroTollCollect();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL AUTO GUARD
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.CONTROLLO_DOCUMENTI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
               .target(StatoRichiesta.INCOMPLETO_TECNICO)
               .guard(new GuardDocumentsTollCollectMissing(documentService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
               .target(StatoRichiesta.DA_VALIDARE)
               .guard(new GuardDocumentsTollCollectPresent(documentService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INCOMPLETO_TECNICO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_VALIDARE)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_VALIDARE)
               .target(StatoRichiesta.DA_INOLTRARE)
               .event(RichiestaEvent.MU_DOCUMENTI_VALIDATI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INCOMPLETO_TECNICO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .event(RichiestaEvent.MU_DOCUMENTI_VALIDATI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .event(RichiestaEvent.MU_INOLTRO_COMPLETO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRO_PARZIALE)
               .event(RichiestaEvent.MU_INOLTRO_PARZIALE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRO_PARZIALE)
               .target(StatoRichiesta.INOLTRATO)
               .event(RichiestaEvent.MU_INOLTRO_COMPLETO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.VALIDAZIONE_CODICE_CONTRATTO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()//  AUTOGUARD
               .source(StatoRichiesta.VALIDAZIONE_CODICE_CONTRATTO)
               .target(StatoRichiesta.VALIDATO_COMPLETO)
               .guard(new GuardContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.VALIDAZIONE_CODICE_CONTRATTO)
               .target(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .guard(new GuardNotContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .target(StatoRichiesta.VALIDAZIONE_CODICE_CONTRATTO)
               .event(RichiestaEvent.MU_CODICE_CONTRATTO_INSERITO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.VALIDATO_COMPLETO)
               .target(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_ATTIVO)
               .action(new FsmActionActiveDevice(fsmSenderToQueue, deviceProducerService, DispositivoEvent.SPEDITO_DAL_FORNITORE))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ANNULLAMENTO));
  }

}
