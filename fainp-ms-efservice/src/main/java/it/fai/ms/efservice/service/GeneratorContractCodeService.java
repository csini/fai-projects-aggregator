package it.fai.ms.efservice.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;

/**
 * Service to generate contract code by device type.
 */
@Service
@Transactional
public class GeneratorContractCodeService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ContrattoRepositoryExt contrattoRepository;

  public GeneratorContractCodeService(final ContrattoRepositoryExt _contrattoRepository) {
    contrattoRepository = _contrattoRepository;
  }

  public String generateContractCode(TipoDispositivoEnum tipoDispositivo, Contratto contract) {
    String codiceContratto = null;
    switch (tipoDispositivo) {
    case TRACKYCARD:
      codiceContratto = generateContractCodeTrackyCard(contract);
      break;
    case GO_BOX:
      codiceContratto = generateContractCodeGoBox(contract);
      break;
    case TES_TRA_FREJUS_MONTE_BIANCO:
      codiceContratto = generateContractFrejus(contract);
      break;
    case DARTFORD_CROSSING:
      codiceContratto = generateContractDartFordCrossing(contract);
      break;
    case TES_TRAF_GRAN_SANBERNARDO:
      codiceContratto = generateContractGranSanBernardo(contract);
      break;
    case LIBER_T:
      codiceContratto = generateContractLiberT(contract);
      break;
    case HGV:
      codiceContratto = generateContractHGV(contract);
      break;
    case VIA_TOLL:
      codiceContratto = generateContractViaToll(contract);
      break;
    case ASSISTENZA_VEICOLI:
      codiceContratto = generateContractAssistenzaVeicoli(contract);
      break;

    default:
      log.warn("Tipo Dispositivo non gestito per la generazione del contratto tramite regole predefinite....");
      break;
    }
    return codiceContratto;
  }

  public Contratto generateAndSaveContractCode(TipoDispositivoEnum tipoDispositivo, Contratto contract) {
    String contractCode = contract.getCodContrattoCliente();
    if (StringUtils.isNotBlank(contractCode)) {
      return contract;
    }

    contractCode = generateContractCode(tipoDispositivo, contract);
    contract = contrattoRepository.save(contract.codContrattoCliente(contractCode));
    return contract;
  }

  private String generateContractCodeTrackyCard(Contratto contract) {
    String codiceContratto = null;
    ClienteFai clienteFai = contract.getClienteFai();
    if (clienteFai == null) {
      throw new IllegalArgumentException("Not found Cliente FAI on contratto: " + contract);
    }

    String codiceCliente = clienteFai.getCodiceCliente();
    if (StringUtils.isNotBlank(codiceCliente)) {
      int lengthCodeClient = codiceCliente.length();
      if (lengthCodeClient > 8) {
        throw new RuntimeException("Codice cliente length is more than 8 chars");
      }

      codiceContratto = codiceCliente;
      if (lengthCodeClient < 8) {
        codiceContratto = StringUtils.leftPad(codiceContratto, 8, "0");
      }
    } else {
      throw new RuntimeException("Codice cliente cannot be null");
    }

    log.info("Generate codice contratto to TrackyCard device: {}", codiceContratto);
    return codiceContratto;
  }

  private String generateContractCodeGoBox(Contratto contract) {
    log.info("Generate contract code for GoBox using algorith TrackyCard");
    String contractCodeGoBox = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to GoBox device: {}", contractCodeGoBox);
    return contractCodeGoBox;
  }

  private String generateContractFrejus(Contratto contract) {
    log.info("Generate contract code for Frejus using algorith TrackyCard");
    String contractCodeGoBox = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to Frejus device: {}", contractCodeGoBox);
    return contractCodeGoBox;
  }

  private String generateContractDartFordCrossing(Contratto contract) {
    log.info("Generate contract code for DartFordCrossing using algorith TrackyCard");
    String contractCodeGoBox = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to DartFordCrossing device: {}", contractCodeGoBox);
    return contractCodeGoBox;
  }

  private String generateContractGranSanBernardo(Contratto contract) {
    log.info("Generate contract code for Tessera Traforo GranSanBernardo using algorith TrackyCard");
    String contractCodeTesTrafGranSanBernardo = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to Tessera Traforo GranSanBernardo device: {}", contractCodeTesTrafGranSanBernardo);
    return contractCodeTesTrafGranSanBernardo;
  }

  private String generateContractLiberT(Contratto contract) {
    log.info("Generate contract code for LiberT using algorith TrackyCard");
    String contractCodeLiberT = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to LiberT device: {}", contractCodeLiberT);
    return contractCodeLiberT;
  }

  private String generateContractHGV(Contratto contract) {
    log.info("Generate contract code for HGV using algorith TrackyCard");
    String contractCodeHGV = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to HGV device: {}", contractCodeHGV);
    return contractCodeHGV;
  }

  private String generateContractViaToll(Contratto contract) {
    log.info("Generate contract code for ViaToll using algorith TrackyCard");
    String contractCodeViaToll = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to ViaToll device: {}", contractCodeViaToll);
    return contractCodeViaToll;
  }

  private String generateContractAssistenzaVeicoli(Contratto contract) {
    log.info("Generate contract code for AssistenzaVeicoli using algorithm TrackyCard");
    String contractCodeAssistenzaVeicoli = generateContractCodeTrackyCard(contract);
    log.info("Generate codice contratto to AssistenzaVeicoli device: {}", contractCodeAssistenzaVeicoli);
    return contractCodeAssistenzaVeicoli;
  }
}
