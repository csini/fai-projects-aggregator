package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.efservice.domain.ClienteFai;

/**
 * Spring Data JPA repository for the ClienteFai entity.
 */
@Repository
public interface ClienteFaiRepository extends JpaRepository<ClienteFai, Long>, InterfaceDmlRepository<ClienteFai> {

  @Query("SELECT cf FROM ClienteFai cf where cf.codiceCliente = :codiceCliente")
  public ClienteFai findByDmlUniqueIdentifier(@Param("codiceCliente") String dmlUniqueIdentifier);

  ClienteFai findOneByCodiceCliente(String codiceCliente);

  @SuppressWarnings("unchecked")
  ClienteFai save(ClienteFai entity);

  @Query("SELECT cf FROM ClienteFai  cf" + " JOIN Contratto c on cf.id = c.clienteFai.id" + " JOIN Dispositivo d on d.contratto.id = c.id"
         + " WHERE d.seriale = :seriale")
  List<ClienteFai> findBySerialeDispositivo(@Param("seriale") String seriale);

  public ClienteFai findOneByCodiceClienteFatturazione(String codiceClienteFatturazione);

  public List<ClienteFai> findByRaggruppamentoImpresa(String raggruppamentoImpresa);
}
