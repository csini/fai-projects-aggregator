package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;

/**
 * Service for managing DispositivoInStorage.
 */
@Service
@Transactional
public class ManageDevicesInStorageService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private AssociazioneDVRepository              assDvRepo;
  private DispositivoRepositoryExt              deviceRepoExt;
  private StatoDispositivoServizioRepositoryExt sdsRepoExt;

  public ManageDevicesInStorageService(final AssociazioneDVRepository _assDvRepo, final DispositivoRepositoryExt _deviceRepoExt,
                                       final StatoDispositivoServizioRepositoryExt _sdsRepoExt) {
    assDvRepo = _assDvRepo;
    deviceRepoExt = _deviceRepoExt;
    sdsRepoExt = _sdsRepoExt;
  }

  public Optional<Dispositivo> findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo statoDispositivo,
                                                                      TipoDispositivoEnum tipoDispositivo) {
    return deviceRepoExt.findTop1ByStatoAndTipoDispositivo_nome(statoDispositivo, tipoDispositivo);
  }

  public Optional<Dispositivo> findTop1ByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(StatoDispositivo statoDispositivo,
                                                                                        TipoDispositivoEnum tipoDispositivo, String tipoHardwareNot) {
    return deviceRepoExt.findByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(statoDispositivo, tipoDispositivo, tipoHardwareNot)
                        .stream()
                        .findFirst();
  }

  public void delete(Dispositivo device) {
    deviceRepoExt.delete(device);
  }

  public void flushDevice() {
    deviceRepoExt.flush();
    sdsRepoExt.flush();
  }

  public void moveStatoDispositivoServizios(Dispositivo source, Dispositivo target) {
    Set<StatoDispositivoServizio> statoDispositivoServizios = source.getStatoDispositivoServizios();
    log.info("Find SdS: {}", statoDispositivoServizios != null ? statoDispositivoServizios.size() : 0);

    if (statoDispositivoServizios == null || statoDispositivoServizios.isEmpty()) {
      log.warn("Not remove Stato dispositivo servizio related on Device: {}", source);
      return;
    }

    List<StatoDispositivoServizio> sdss = statoDispositivoServizios.stream()
                                                                   .map(sds -> sds.dispositivo(target))
                                                                   .collect(toList());
    if (sdss != null) {
      sdsRepoExt.save(sdss);
    } else {
      log.warn("Not remove Stato dispositivo servizio related on Device: {}", source);
    }

  }

  public void removeStatoDispositivoServizios(Dispositivo device) {
    moveStatoDispositivoServizios(device,null);
  }

  public void removeAssociazioneDvs(Dispositivo device) {
    Set<AssociazioneDV> associazioneDispositivoVeicolos = device.getAssociazioneDispositivoVeicolos();
    log.info("Find Associazione Dvs: {}", associazioneDispositivoVeicolos != null ? associazioneDispositivoVeicolos.size() : 0);

    if (associazioneDispositivoVeicolos == null || associazioneDispositivoVeicolos.isEmpty()) {
      log.warn("Not remove Associaizone Dvs related on Device: {}", device);
      return;
    }

    List<AssociazioneDV> asdvs = associazioneDispositivoVeicolos.stream()
                                                                .map(asdv -> asdv.dispositivo(null))
                                                                .collect(toList());
    if (asdvs != null) {
      assDvRepo.save(asdvs);
    } else {
      log.warn("Not remove Associaizone Dvs related on Device: {}", device);
    }
  }

  public Set<Dispositivo> findDispositiviByStateAndType(StatoDispositivo statoDispositivo, TipoDispositivoEnum tipoDispositivo) {
    return deviceRepoExt.findDispositiviByStateAndTipo(statoDispositivo, tipoDispositivo);
  }

  public Dispositivo save(Dispositivo device) {
    log.info("Save device: {}", device);
    return deviceRepoExt.save(device);
  }



}
