package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the TipoServizio entity.
 */
public class TipoServizioDTO implements Serializable {

  private Long id;

  @NotNull
  private String nome;

  private String descrizione;

  private String nomeBusiness;

  private String testoPromo;

  private Long ordinamento;

  private String documenti;

  private Long tipoServizioDefaultId;

  private Long categoriaServizioId;

  private String categoriaServizioNome;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public String getNomeBusiness() {
    return nomeBusiness;
  }

  public void setNomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
  }

  public String getTestoPromo() {
    return testoPromo;
  }

  public void setTestoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
  }

  public Long getOrdinamento() {
    return ordinamento;
  }

  public void setOrdinamento(Long ordinamento) {
    this.ordinamento = ordinamento;
  }

  public String getDocumenti() {
    return documenti;
  }

  public void setDocumenti(String documenti) {
    this.documenti = documenti;
  }

  public Long getTipoServizioDefaultId() {
    return tipoServizioDefaultId;
  }

  public void setTipoServizioDefaultId(Long tipoServizioDefaultId) {
    this.tipoServizioDefaultId = tipoServizioDefaultId;
  }

  public Long getCategoriaServizioId() {
    return categoriaServizioId;
  }

  public void setCategoriaServizioId(Long categoriaServizioId) {
    this.categoriaServizioId = categoriaServizioId;
  }

  public String getCategoriaServizioNome() {
    return categoriaServizioNome;
  }

  public void setCategoriaServizioNome(String categoriaServizioNome) {
    this.categoriaServizioNome = categoriaServizioNome;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TipoServizioDTO tipoServizioDTO = (TipoServizioDTO) o;
    if (tipoServizioDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), tipoServizioDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "TipoServizioDTO{" + "id=" + getId() + ", nome='" + getNome() + "'" + ", descrizione='" + getDescrizione() + "'"
           + ", nomeBusiness='" + getNomeBusiness() + "'" + ", testoPromo='" + getTestoPromo() + "'" + ", ordinamento=" + getOrdinamento()
           + ", documenti='" + getDocumenti() + "'" + "}";
  }

}