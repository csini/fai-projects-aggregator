package it.fai.ms.efservice.service.fsm.config.telepass.eu;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActiveDevice;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoAttivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRichiestaContratto;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendMail;
import it.fai.ms.efservice.service.fsm.action.FsmActivationDeActivationService;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardAttesaTransito;
import it.fai.ms.efservice.service.fsm.guard.GuardContractCodeUser;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtClient;
import it.fai.ms.efservice.service.fsm.guard.GuardIsToBeSentAtFai;
import it.fai.ms.efservice.service.fsm.guard.GuardNotContractCodeUser;
import it.fai.ms.efservice.service.fsm.guard.GuardOrdrcOrdim;
import it.fai.ms.efservice.service.fsm.guard.GuardRispostaRichiestaKo;
import it.fai.ms.efservice.service.fsm.guard.GuardRispostaRichiestaOk;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroTelepassEuropeoConfig.INOLTRO_TE)
public class FsmInoltroTelepassEuropeoConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_TE = "inoltroTE";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceProducerService deviceProducerService;

  private final FsmSenderToQueue senderFsmService;

  private final JmsProperties jmsProperties;

  private final ManageDeviceSentModeService manageDeviceSentModeService;

  public FsmInoltroTelepassEuropeoConfig(final FsmSenderToQueue _senderFsmService, final JmsProperties _jmsProperties,
                                         final DeviceProducerService _deviceProducerService,
                                         final ManageDeviceSentModeService _manageDeviceSentModeService) {
    senderFsmService = _senderFsmService;
    jmsProperties = _jmsProperties;
    deviceProducerService = _deviceProducerService;
    manageDeviceSentModeService = _manageDeviceSentModeService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_TE.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroTE();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.SERVIZI_INOLTRATI)
               .event(RichiestaEvent.INOLTRO_SERVIZI)
               .action(new FsmActivationDeActivationService(deviceProducerService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SERVIZI_INOLTRATI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.RESPONSE_OK)
               .action(new FsmActionActiveDevice(senderFsmService, deviceProducerService, DispositivoEvent.SPEDITO_DA_FAI))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.SERVIZI_INOLTRATI)
               .target(StatoRichiesta.ERRORE_GESTIONE_SERVIZIO)
               .event(RichiestaEvent.RESPONSE_KO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ERRORE_GESTIONE_SERVIZIO)
               .target(StatoRichiesta.SERVIZI_INOLTRATI)
               .event(RichiestaEvent.MU_INVIO_SERVIZI)
               .action(new FsmActivationDeActivationService(deviceProducerService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.INCOMPLETO_TECNICO)
               .guard(new GuardNotContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.COMPLETO)
               .guard(new GuardContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.INCOMPLETO_TECNICO)
               .target(StatoRichiesta.ATTESA_RISPOSTA)
               .action(new FsmActionRichiestaContratto(senderFsmService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_RISPOSTA)
               .target(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .event(RichiestaEvent.MS_CTRRI)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .target(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .guard(new GuardNotContractCodeUser())
               .action(new FsmActionSendMail(jmsProperties))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO)
               .target(StatoRichiesta.COMPLETO)
               .guard(new GuardContractCodeUser())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO)
               .target(StatoRichiesta.INCOMPLETO_TECNICO)
               // .event(OrderEvent.MU_RIPETI_INVIO) // Removed because in the file: 'Processo Ordine Unificato
               // v1.5-Rev-INT' is an auto-transition;
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.COMPLETO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .guard(new GuardOrdrcOrdim(senderFsmService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.INOLTRATO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO)
               .target(StatoRichiesta.ANALISI_RISPOSTA_ORDINE)
               .event(RichiestaEvent.MS_ORDRI_RISIM)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_ORDINE)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .guard(new GuardRispostaRichiestaKo())
               .action(new FsmActionSendMail(jmsProperties))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.PORTALE_WEB_FORNITORE)
               .event(RichiestaEvent.MU_CHIUDI_ORDINE_SU_PORTALE_TELEPASS)
               .action(new FsmActionDispositivoAttivo(senderFsmService)) // FIXME Luca => Controllare analisi funzionale
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.ANNULLAMENTO))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .event(RichiestaEvent.MS_RIPETI_INVIO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ANALISI_RISPOSTA_ORDINE)
               .target(StatoRichiesta.ACCETTATO_FORNITORE)
               .guard(new GuardRispostaRichiestaOk())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_FORNITORE)
               .target(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .event(RichiestaEvent.MS_OBUNO)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.IN_SPEDIZIONE))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .guard(new GuardIsToBeSentAtFai(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .guard(new GuardAttesaTransito(manageDeviceSentModeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
               .target(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .event(RichiestaEvent.MU_SPEDIZIONE_FAI)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_A_FAI))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.PREPARAZIONE_SPEDIZIONE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .guard(new GuardIsToBeSentAtClient(manageDeviceSentModeService))
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.SPEDITO_DAL_FORNITORE))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_SPEDIZIONE_COMPLETA)
               .action(new FsmActionActiveDevice(senderFsmService, deviceProducerService, DispositivoEvent.SPEDITO_DA_FAI))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)
               .target(StatoRichiesta.CONCLUSO_NON_SPEDITO)
               .event(RichiestaEvent.MU_CHIUSURA_ORDINE)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.REVOCA));
  }

}
