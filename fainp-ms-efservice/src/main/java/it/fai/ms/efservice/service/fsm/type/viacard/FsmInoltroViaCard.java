package it.fai.ms.efservice.service.fsm.type.viacard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.viacard.FsmInoltroViaCardConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = "fsmInoltroViaCard")
public class FsmInoltroViaCard extends FsmRichiestaGeneric {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroViaCard(@Qualifier(FsmInoltroViaCardConfig.INOLTRO_VIACARD) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                           FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_VIACARD;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.VIACARD };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
