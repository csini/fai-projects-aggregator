package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RaggruppamentoRichiesteOrdineFornitore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaggruppamentoRichiesteOrdineFornitoreRepository extends JpaRepository<RaggruppamentoRichiesteOrdineFornitore, Long> {

}
