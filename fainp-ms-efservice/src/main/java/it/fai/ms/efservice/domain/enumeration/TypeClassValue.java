package it.fai.ms.efservice.domain.enumeration;

import java.time.Instant;

public enum TypeClassValue {

  STRING, INSTANT;

  public static TypeClassValue getTypeClass(Object obj) {
    if (obj instanceof Instant) {
      return INSTANT;
    } else if (obj instanceof String) {
      return STRING;
    }
    throw new IllegalArgumentException("Not parse this object: " + obj);
  }
}
