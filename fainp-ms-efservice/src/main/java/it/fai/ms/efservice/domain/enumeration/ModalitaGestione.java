package it.fai.ms.efservice.domain.enumeration;

/**
 * The ModalitaGestione enumeration.
 */
public enum ModalitaGestione {
    DA_REMOTO, OFFLINE
}
