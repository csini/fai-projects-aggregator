package it.fai.ms.efservice.service.fsm.type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;

@Service
public class StateMachineDispositivoBuilder {

  private final Logger _log = LoggerFactory.getLogger(this.getClass());

  private StateMachineFactory<StatoDispositivo, DispositivoEvent> stateMachineFactory;

  @Autowired
  public StateMachineDispositivoBuilder(@Qualifier("dispositivo") final StateMachineFactory<StatoDispositivo, DispositivoEvent> _stateMachineFactory) {
    stateMachineFactory = _stateMachineFactory;
  }

  public StateMachine<StatoDispositivo, DispositivoEvent> build() {
    StateMachine<StatoDispositivo, DispositivoEvent> stateMachine = stateMachineFactory.getStateMachine(FsmType.DISPOSITIVO.fsmName());
    _log.debug("Get StateMachine {}", stateMachine.getId());
    return stateMachine;
  }
}
