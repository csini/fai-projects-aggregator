package it.fai.ms.efservice.service.jms.listener.orderrequest;

import static it.fai.ms.common.jms.JmsTopicNames.ORDERREQUEST_RECEIVED;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.message.orderrequest.OrderRequestReceivedMessage;
import it.fai.ms.efservice.service.jms.consumer.OrderRequestReceivedConsumer;

@Service
@Transactional
public class JmsListenerOrderRequestReceivedService extends JmsObjectMessageListenerTemplate<OrderRequestReceivedMessage> implements JmsTopicListener {

  private final Logger                       _log = LoggerFactory.getLogger(getClass());
  private final OrderRequestReceivedConsumer orderRequestReceivedConsumer;

  @Autowired
  public JmsListenerOrderRequestReceivedService(final OrderRequestReceivedConsumer _orderRequestReceivedConsumer) throws Exception {
    orderRequestReceivedConsumer = _orderRequestReceivedConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return ORDERREQUEST_RECEIVED;
  }

  @Override
  protected void consumeMessage(OrderRequestReceivedMessage orderRequestReceivedMessage) {
    try {
      _log.info("Received jms message {}", orderRequestReceivedMessage);
      orderRequestReceivedConsumer.consume(orderRequestReceivedMessage);
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", orderRequestReceivedMessage, _e);
      throw new RuntimeException(_e);
    }

  }

}
