package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardServiceType implements Serializable {

  private static final long serialVersionUID = 5369548120220605297L;

  private WizardServiceTypeId id;
  private long ordering;

  public WizardServiceType(final WizardServiceTypeId _id) {
    Objects.requireNonNull(_id, "WizardServiceTypeId is mandatory");
    id = _id;
  }
  
  public WizardServiceType(final WizardServiceTypeId _id, long _ordering) {
    this(_id);
    this.ordering = _ordering;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((WizardServiceType) _obj).getId(), id);
    }
    return isEquals;
  }

  public WizardServiceTypeId getId() {
    return this.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  public long getOrdering() {
    return ordering;
  }

  public void setOrdering(long ordering) {
    this.ordering = ordering;
  }

  @Override
  public String toString() {
    return "WizardServiceType [id=" + id + ", ordering=" + ordering + "]";
  }
}
