package it.fai.ms.efservice.wizard.service;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.model.matrix.step3.WizardStep3Matrix;
import it.fai.ms.efservice.wizard.repository.WizardDeviceRepository;

@Service
public class Step3ServiceImpl implements Step3Service {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private WizardDeviceRepository   wizardDeviceRepository;
  private PrecalculatedRuleService precalculatedRuleService;

  public Step3ServiceImpl(final PrecalculatedRuleService _precalculatedRuleService, final WizardDeviceRepository _wizardDeviceRepository) {
    precalculatedRuleService = _precalculatedRuleService;
    wizardDeviceRepository = _wizardDeviceRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public WizardStep3Matrix buildMatrix(final WizardServiceTypeId _wizardServiceTypeId, final Set<WizardVehicleId> _wizardVehicleIds) {

    _log.info("Building step3WizardMatrix for serviceTypeId {}", _wizardServiceTypeId.getId());

    final WizardStep3Matrix wizardStep3Matrix = new WizardStep3Matrix(_wizardServiceTypeId.getId());

    Map<WizardVehicleId, WizardMatrixActivability> collect = _wizardVehicleIds.stream()
                                                                              .collect(Collectors.toMap(wizardVehicleId -> wizardVehicleId,
                                                                                                        wizardVehicleId -> mapToState(_wizardServiceTypeId,
                                                                                                                                      wizardVehicleId)));
    wizardStep3Matrix.getMap()
                     .putAll(collect);
    return wizardStep3Matrix;
  }

  private WizardMatrixActivability mapToState(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId) {
    WizardMatrixActivability matrixActivability = new WizardMatrixActivability(ActivabilityState.ANOMALY);
    final Optional<RuleOutcome> ruleOutcome = executeRule(_wizardVehicleId);
    boolean ruleOutcomeIsTrue = !ruleOutcome.isPresent() || ruleOutcome.get()
                                                                       .getOutcome();
    if (ruleOutcomeIsTrue) {
      boolean notMatch = notMatch(_wizardServiceTypeId, _wizardVehicleId);
      if (notMatch) {
        _log.debug("ServiceTypeId {} not active on {}", _wizardServiceTypeId, _wizardVehicleId);
        matrixActivability = new WizardMatrixActivability(ActivabilityState.ACTIVABLE_ALL);
      } else {
        if (matchONGOING_REQUEST(_wizardServiceTypeId, _wizardVehicleId)) {
          _log.debug("ServiceTypeId {} is ONGOING_REQUEST on {}", _wizardServiceTypeId, _wizardVehicleId);
          matrixActivability = new WizardMatrixActivability(ActivabilityState.ONGOING_REQUEST);
        } else {
          _log.debug("ServiceTypeId {} is active on {}", _wizardServiceTypeId, _wizardVehicleId);
          boolean allMatch = allMatch(_wizardServiceTypeId, _wizardVehicleId);
          if (allMatch) {
            _log.debug("ServiceTypeId {} active on all {} devices", _wizardServiceTypeId, _wizardVehicleId);
            matrixActivability = new WizardMatrixActivability(ActivabilityState.ACTIVE);
          } else {
            _log.debug("ServiceTypeId {} active on some {} devices", _wizardServiceTypeId, _wizardVehicleId);
            matrixActivability = new WizardMatrixActivability(ActivabilityState.ACTIVABLE);
          }
        }
      }
    } else {
      _log.debug("WizardVehicleId {} ruleOutcome fail", _wizardVehicleId);
      matrixActivability = new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE);
      WizardMatrixActivabilityFailure activabilityFailure = new WizardMatrixActivabilityFailure(WizardPreconditionCode.DEFAULT.code(),
                                                                                                WizardPreconditionCode.DEFAULT.codeName());
      if (ruleOutcome.isPresent()) {
        RuleOutcome rule = ruleOutcome.get();
        Optional<RuleFailure> failure = rule.getFailure();
        if (failure.isPresent()) {
          RuleFailure rf = failure.get();
          _log.debug("RuleFailure found to Vehicle {}: {}", _wizardVehicleId.getId(), rf);
          activabilityFailure = new WizardMatrixActivabilityFailure(rf.decode(), rf.getMess());
        }
      }

      _log.debug("There is an ANOMALY on vehicle: {} => {}", _wizardVehicleId.getId(), activabilityFailure);
      matrixActivability.addFailure(activabilityFailure);
    }
    _log.debug("State for WizardServiceTypeId {} and WizardVehicleId {}: {}", _wizardServiceTypeId, _wizardVehicleId, matrixActivability);
    return matrixActivability;
  }

  private boolean matchONGOING_REQUEST(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId) {
    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .anyMatch(wizardDevice -> wizardDevice.getState() == ActivabilityState.ONGOING_REQUEST
                                                           && wizardDevice.hasActiveService(_wizardServiceTypeId));
  }

  private boolean allMatch(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId) {
    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .allMatch(wizardDevice -> wizardDevice.hasActiveService(_wizardServiceTypeId));
  }

  private boolean notMatch(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId) {
    return wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId)
                                 .stream()
                                 .noneMatch(wizardDevice -> wizardDevice.hasActiveService(_wizardServiceTypeId));
  }

  private Optional<RuleOutcome> executeRule(final WizardVehicleId _wizardVehicleId) {
    return precalculatedRuleService.execute(mapToRuleContextVehicle(_wizardVehicleId).getId());
  }

  private RuleEngineVehicle mapToRuleContextVehicle(WizardVehicleId wizardVehicleId) {
    return new RuleEngineVehicle(new RuleEngineVehicleId(wizardVehicleId.getId()));
  }
}
