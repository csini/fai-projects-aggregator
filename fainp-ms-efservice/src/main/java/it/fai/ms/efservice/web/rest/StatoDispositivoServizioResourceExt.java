package it.fai.ms.efservice.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.service.StatoDispositivoServizioServiceExt;
import it.fai.ms.efservice.service.dto.ChangeStatoDispositivoServizioDTO;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import it.fai.ms.efservice.service.mapper.StatoDispositivoServizioMapper;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.ResponsePage;

@RestController
@RequestMapping(StatoDispositivoServizioResourceExt.BASE_PATH)
public class StatoDispositivoServizioResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/stato-dispositivo-servizios";
  public static final String STATO_DISPOSTIVI_SERVIZI_BY_DISPOSITIVO = "/dispositivo";

  public static final String CHANGE = "/change";
  public static final String API_CHANGE = BASE_PATH + CHANGE;

  private StatoDispositivoServizioServiceExt statoDispositivoServizioServiceExt;

  private StatoDispositivoServizioMapper statoDispositivoServizioMapper;

  public StatoDispositivoServizioResourceExt() { }

  @GetMapping(STATO_DISPOSTIVI_SERVIZI_BY_DISPOSITIVO + "/{uuIdDispositivo}")
  @Timed
  public ResponseEntity<ResponsePage<StatoDispositivoServizioDTO>>  getStatoDispositivoServizio(
                                                                                                @PathVariable String uuIdDispositivo,
                                                                                                Pageable pageable
      ) {
    log.debug("REST request to get StatoDispositivoServizio by dispositivo : {}", uuIdDispositivo);

    int pageNumber = 0;
    if(pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<StatoDispositivoServizio> page = statoDispositivoServizioServiceExt
        .getAllByDispostivoUuid(uuIdDispositivo, pageRequest);

    List<StatoDispositivoServizio> statoDispositivoServizioList = page.getContent();

    List<StatoDispositivoServizioDTO> dispositiviServizi =  statoDispositivoServizioList
        .stream()
        .map(statoDispositivoServizioMapper::toDto)
        .collect(Collectors.toList());

    Page<StatoDispositivoServizioDTO> serviziPage = new PageImpl<>(
        dispositiviServizi,
        pageRequest,
        page.getTotalElements()
        );

    return ResponseEntity.ok(
                             new ResponsePage<>(serviziPage)
        );
  }

  @PostMapping(CHANGE)
  @Timed
  public ResponseEntity<ChangeStatoDispositivoServizioDTO> changeStatoDispositivoServizio(@RequestBody ChangeStatoDispositivoServizioDTO dto) throws Exception {
    log.debug("REST request to change StatoDispositivoServizio by dto : {}", dto);

    if (dto == null) {
      log.warn("Device assignation dto is null or empty.");
      throw CustomException.builder(HttpStatus.BAD_REQUEST).add(Errno.VALIDATION_FAILED);
    }    

    dto = statoDispositivoServizioServiceExt.changeStatoDispositivoServizio(dto);
    return (dto != null) ? ResponseEntity.ok().body(dto) : ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);  
  }

  @Autowired
  public void setStatoDispositivoServizioServiceExt(StatoDispositivoServizioServiceExt statoDispositivoServizioServiceExt) {
    this.statoDispositivoServizioServiceExt = statoDispositivoServizioServiceExt;
  }

  @Autowired
  public void setStatoDispositivoServizioMapper(StatoDispositivoServizioMapper statoDispositivoServizioMapper) {
    this.statoDispositivoServizioMapper = statoDispositivoServizioMapper;
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
        .body(ex.getPayloadError());
  }

}
