package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.TipoServizioDefaultDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TipoServizioDefault and its DTO TipoServizioDefaultDTO.
 */
@Mapper(componentModel = "spring", uses = {TipoDispositivoMapper.class, })
public interface TipoServizioDefaultMapper extends EntityMapper <TipoServizioDefaultDTO, TipoServizioDefault> {

    @Mapping(source = "tipoDispositivo.id", target = "tipoDispositivoId")
    TipoServizioDefaultDTO toDto(TipoServizioDefault tipoServizioDefault); 

    @Mapping(source = "tipoDispositivoId", target = "tipoDispositivo")
    @Mapping(target = "tipoServizios", ignore = true)
    TipoServizioDefault toEntity(TipoServizioDefaultDTO tipoServizioDefaultDTO); 
    default TipoServizioDefault fromId(Long id) {
        if (id == null) {
            return null;
        }
        TipoServizioDefault tipoServizioDefault = new TipoServizioDefault();
        tipoServizioDefault.setId(id);
        return tipoServizioDefault;
    }
}
