package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniServiceExt;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.cache.StatoOrdineClienteCache;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

/**
 * Service Implementation for managing OrdineCliente.
 */
@Service
@Transactional
public class OrdineClienteServiceImplExt implements OrdineClienteServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final OrdineClienteService                ordineClienteService;
  private final OrdineClienteRepositoryExt          ordineClienteRepoExt;
  private final StatoOrdineClienteCache             cacheService;
  private final ClienteFaiServiceExt                clienteFaiServiceExt;
  private final IndirizzoSpedizioneOrdiniServiceExt indirizzoSpedizioneServiceExt;

  public OrdineClienteServiceImplExt(final OrdineClienteService _ordineClienteService,
                                     final OrdineClienteRepositoryExt _ordineClienteRepoExt, final StatoOrdineClienteCache _cacheService,
                                     final ClienteFaiServiceExt _clienteFaiServiceExt,
                                     final IndirizzoSpedizioneOrdiniServiceExt _indirizzoSpedizioneServiceExt) {
    ordineClienteService = _ordineClienteService;
    ordineClienteRepoExt = _ordineClienteRepoExt;
    cacheService = _cacheService;
    clienteFaiServiceExt = _clienteFaiServiceExt;
    indirizzoSpedizioneServiceExt = _indirizzoSpedizioneServiceExt;
  }

  @Override
  public OrdineCliente save(OrdineCliente ordineCliente) {
    log.debug("Save OrdineCliente {}", ordineCliente);
    String numeroOrdine = generateNumeroOrdine(ordineCliente);
    ordineCliente.setNumeroOrdine(numeroOrdine);
    indirizzoSpedizioneServiceExt.setIndirizziOnOrdineCliente(ordineCliente);
    ordineCliente = ordineClienteService.save(ordineCliente);
    Long idOrdineCliente = ordineCliente.getId();
    String identificativoOrdineCliente = ordineCliente.getIdentificativo();
    cacheService.putObject(String.valueOf(idOrdineCliente), identificativoOrdineCliente);
    return ordineCliente;
  }

  private String generateNumeroOrdine(OrdineCliente ordineCliente) {
    String numeroOrdine = ordineCliente.getNumeroOrdine();
    if (StringUtils.isBlank(numeroOrdine)) {
      LocalDate date = LocalDate.now();
      String dateFormatted = date.format(DateTimeFormatter.BASIC_ISO_DATE);
      numeroOrdine = "ORD" + dateFormatted;
    } else {
      if (!numeroOrdine.contains("-") && ordineCliente.getId() != null) {
        Long idOrdineCliente = ordineCliente.getId();
        ordineCliente.setNumeroOrdine(numeroOrdine + "-" + idOrdineCliente);
        numeroOrdine = numeroOrdine + "-" + idOrdineCliente;
      }
    }
    log.info("Generate numero ordine: {} to OrdineCliente: {}", numeroOrdine, ordineCliente);
    return numeroOrdine;
  }

  @Override
  public OrdineCliente loadClienteAssegnatarioOnOrdineCliente(OrdineCliente ordineCliente) {
    Long idOrdineCliente = ordineCliente.getId();
    return loadClienteAssegnatarioOnOrdineCliente(idOrdineCliente);
  }

  @Override
  public OrdineCliente loadClienteAssegnatarioOnOrdineCliente(Long idOrdineCliente) {
    OrdineCliente ordineCliente = ordineClienteRepoExt.findOne(idOrdineCliente);
    ClienteFai clienteAssegnatario = ordineCliente.getClienteAssegnatario();
    ClienteFai clienteFai = clienteFaiServiceExt.findById(clienteAssegnatario.getId());
    ordineCliente.setClienteAssegnatario(clienteFai);
    return ordineCliente;
  }

  @Override
  public OrdineCliente findById(Long ordineClienteId, boolean eager) {
    log.debug("Reuqest RELOAD OrdineCliente by ID {}, eager: {}", ordineClienteId, eager);
    OrdineCliente ordineCliente = ordineClienteRepoExt.findOne(ordineClienteId);
    if (eager) {
      ClienteFai clienteAssegnatario = ordineCliente.getClienteAssegnatario();
      if (clienteAssegnatario == null) {
        log.warn("Not load cliente assegnatario ordineCliente {}", ordineClienteId);
      }
      IndirizzoSpedizioneOrdini indirizzoDiTransito = ordineCliente.getIndirizzoDiTransito();
      if (indirizzoDiTransito == null) {
        log.warn("Not load indirizzoDiTransito ordineCliente {}", ordineClienteId);
      }
      IndirizzoSpedizioneOrdini indirizzoDestinazioneFinale = ordineCliente.getIndirizzoDestinazioneFinale();
      if (indirizzoDestinazioneFinale == null) {
        log.warn("Not load indirizzoDestinazioneFinale ordineCliente {}", ordineClienteId);
      }
    }

    log.debug("Loaded OrdineCliente {}", ordineCliente);
    return ordineCliente;
  }

  @Override
  public OrdineCliente findByIdentificativoOrdineCliente(String identificativoOrdineCliente) {
    OrdineCliente ordineCliente = ordineClienteRepoExt.findByIdentificativoOrdine(identificativoOrdineCliente);
    // ordineCliente = loadClienteAssegnatarioOnOrdineCliente(ordineCliente);
    log.trace("Loaded OrdineCliente {}", ordineCliente);
    return ordineCliente;
  }

  @Override
  public Set<OrdineCliente> findAll() {
    List<OrdineCliente> findAll = ordineClienteRepoExt.findAll();
    return new HashSet<OrdineCliente>(findAll);
  }

  @Override
  public Set<Richiesta> findRichiesteByIdentificativoOrdine(String identificativo) {
    Set<Richiesta> richieste = ordineClienteRepoExt.findRichieste(identificativo);
    log.info("Find richieste by identificativo Ordine {}: {}", identificativo, richieste);
    return richieste;
  }

  @Override
  public OrdineCliente updateNumeroOrdine(OrdineCliente ordineCliente) {
    ordineCliente = save(ordineCliente);
    return ordineCliente;
  }

  @Override
  public Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIsAndNotInCache(StatoOrdineCliente stato, List<String> keysId) {
    Set<OrdineClienteCacheDTO> ordini = null;

    List<Long> ids = null;
    if (keysId != null && !keysId.isEmpty()) {
      ids = keysId.stream()
                  .map(s -> Long.parseLong(s))
                  .collect(toList());
    }

    if (ids == null || ids.isEmpty()) {
      ordini = ordineClienteRepoExt.findFilteredByStatoNotIs(stato);
    } else {
      List<Long> tempIds = new ArrayList<>();
      ordini = new HashSet<OrdineClienteCacheDTO>();
      for (Long id : ids) {
        tempIds.add(id);
        if (tempIds.size() > 500) {
          ordini.addAll(ordineClienteRepoExt.findFilteredByStatoNotIsAndNotInCache(stato, tempIds));
          tempIds.clear();
        }
      }

      if (tempIds.size() > 0) {
        ordini.addAll(ordineClienteRepoExt.findFilteredByStatoNotIsAndNotInCache(stato, tempIds));
      }
    }

    return ordini;
  }

  @Override
  public OrdineCliente createOrdineClienteByRichieste(List<Richiesta> richieste, CarrelloModificaRichiestaDTO carrelloDTO) {
    OrdineCliente ordineCliente = new OrdineCliente();
    try {
      TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
      Optional<Richiesta> optRichiesta = richieste.stream()
                                                  .findFirst();
      if (optRichiesta.isPresent()) {
        Richiesta richiesta = optRichiesta.get();
        ordineCliente = richiesta.getOrdineCliente();
        ordineCliente.setTipo(TipoOrdineCliente.VARIAZIONE);
        ordineCliente.setPrevisioneSpesaMensile(null);

        String newUuidVehicle = carrelloDTO.getNewVeicolo();
        if (StringUtils.isNotBlank(newUuidVehicle)) {
          log.debug("Set UUID new Vehicle {}", newUuidVehicle);
          ordineCliente.setUuidVeicolo(newUuidVehicle);
        }
        ordineCliente.setRichiedente(carrelloDTO.getRichiedente());

        ordineCliente = save(ordineCliente);
        ordineCliente = updateNumeroOrdine(ordineCliente);
        log.info("Created OrdineCliente [ID: {} - Identificativo: {} - NumeroOrdine: {}] di modifica: {}", ordineCliente.getId(),
                 ordineCliente.getIdentificativo(), ordineCliente.getNumeroOrdine(), carrelloDTO.getTipoOperazioneModifica());

        final OrdineCliente orderClient = ordineCliente;
        richieste = richieste.stream()
                             .map(r -> r.ordineCliente(orderClient))
                             .collect(toList());
      } else {
        log.warn("Not found request to create and persiste OrdineCliente to operations: {}", tipoOperazioneModifica);
      }
    } catch (Exception e) {
      log.error("Exception on persist OrdineCliente from modification requests: ", e);
      throw e;
    }
    return ordineCliente;
  }

  @Override
  public void delete(OrdineCliente ordineCliente) {
    if (ordineCliente != null) {

      try {
        // Aggiunto per ovviare al problema della sincronizzazione delle code...
        Thread.sleep(500L);
      } catch (InterruptedException ie) {
        log.warn("Skip sleep because received an {}: {}", InterruptedException.class.getSimpleName(), ie.getMessage());
      }

      Long idOrder = ordineCliente.getId();
      ordineClienteService.delete(idOrder);
      log.info("Delete Order: [{}] that not has requests.", idOrder);
    } else {
      log.debug("Skip delete order because is null or ID is null");
    }
  }

  @Override
  public OrdineCliente saveAndFlush(OrdineCliente ordineCliente) {
    this.ordineClienteRepoExt.saveAndFlush(ordineCliente);
    ordineCliente = this.ordineClienteRepoExt.findOne(ordineCliente.getId());
    log.info("CREATED Order: {} \nwith Richieste: {}", ordineCliente,
             ordineCliente.getRichiestas() != null ? ordineCliente.getRichiestas() : "[]");
    return ordineCliente;
  }

  @Override
  public List<OrdineClienteCacheDTO> findByStatoNotIs(StatoOrdineCliente stato) {
    return ordineClienteRepoExt.findByStatoNotIs(stato);
  }

}
