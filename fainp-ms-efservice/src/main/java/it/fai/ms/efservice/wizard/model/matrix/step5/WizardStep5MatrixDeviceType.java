package it.fai.ms.efservice.wizard.model.matrix.step5;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class WizardStep5MatrixDeviceType implements Serializable, Comparable<WizardStep5MatrixDeviceType> {

  private static final long serialVersionUID = -250251233912022727L;

  private String                        id;
  private Set<WizardStep5MatrixVehicle> vehicles = new LinkedHashSet<>();

  public WizardStep5MatrixDeviceType(final String _id) {
    id = _id;
  }

  @Override
  public int compareTo(final WizardStep5MatrixDeviceType _obj) {
    return id.compareTo(_obj.getId());
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep5MatrixDeviceType) _obj).getId(), id)
                 && Objects.equals(((WizardStep5MatrixDeviceType) _obj).getVehicles(), vehicles);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  public Set<WizardStep5MatrixVehicle> getVehicles() {
    return this.vehicles;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, vehicles);
  }

  public void setVehicles(final Set<WizardStep5MatrixVehicle> _vehicles) {
    vehicles = _vehicles;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Step5WizardMatrixDeviceType [id=");
    builder.append(this.id);
    builder.append(", vehicles=");
    builder.append(this.vehicles);
    builder.append("]");
    return builder.toString();
  }

}
