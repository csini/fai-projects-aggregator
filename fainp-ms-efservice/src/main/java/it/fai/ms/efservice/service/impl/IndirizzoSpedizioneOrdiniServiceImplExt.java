package it.fai.ms.efservice.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.client.AnagaziendeClient;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.client.dto.IndirizzoSpedizioneDTO;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniServiceExt;

@Service
@Transactional
public class IndirizzoSpedizioneOrdiniServiceImplExt implements IndirizzoSpedizioneOrdiniServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneRepo;

  private AnagaziendeClient anagaziendeClient;

  private String securityToken;

  public IndirizzoSpedizioneOrdiniServiceImplExt(final IndirizzoSpedizioneOrdiniRepository _indirizzoSpedizioneRepo,
                                                 final AnagaziendeClient _anagaziendeClient,
                                                 @Value("${efservice.authorizationHeader}") String _securityToken) {
    indirizzoSpedizioneRepo = _indirizzoSpedizioneRepo;
    anagaziendeClient = _anagaziendeClient;
    securityToken = _securityToken;
  }

  @Override
  public OrdineCliente setIndirizziOnOrdineCliente(OrdineCliente ordineCliente) {
    IndirizzoSpedizioneOrdini destinazioneFinale = ordineCliente.getIndirizzoDestinazioneFinale();
    IndirizzoSpedizioneOrdini transito = ordineCliente.getIndirizzoDiTransito();
    if (destinazioneFinale != null || transito != null) {
      log.info("Skip set indirizzi, because already set: Transito: {} - Destinazione finale: {}", transito, destinazioneFinale);
    } else {

      String codiceCliente = ordineCliente.getClienteAssegnatario()
                                          .getCodiceCliente();
      IndirizziDiSpedizioneDTO indirizzi = getIndirizziDiSpedizioneByNav(codiceCliente);

      IndirizzoSpedizioneOrdini indirizzo = null;
      IndirizzoSpedizioneDTO indirizzoDiTransitoDTO = indirizzi.getTransito();
      log.info("Create indirizzo di transito");
      indirizzo = mapAndpersistIndirizzoSpedizioneOrdini(indirizzoDiTransitoDTO);
      Boolean indirizzoTransitoFai = indirizzo.isFai();
      ordineCliente.setIndirizzoDiTransito(indirizzo);

      IndirizzoSpedizioneDTO indirizzoDestFinaleDTO = indirizzi.getFinale();
      log.info("Create indirizzo destinazione Finale");
      indirizzo = mapAndpersistIndirizzoSpedizioneOrdini(indirizzoDestFinaleDTO);
      Boolean indirizzoDestFinaleFai = indirizzo.isFai();
      ordineCliente.setIndirizzoDestinazioneFinale(indirizzo);

      ordineCliente.setDestinazioneFai(false);
      if (indirizzoTransitoFai != null && indirizzoTransitoFai == true) {
        ordineCliente.setDestinazioneFai(true);
      } else {
        if (indirizzoDestFinaleFai != null && indirizzoDestFinaleFai == true) {
          ordineCliente.setDestinazioneFai(true);
        }
      }
    }

    return ordineCliente;
  }

  @Override
  public IndirizziDiSpedizioneDTO getIndirizziDiSpedizioneByNav(String codiceCliente) {
    IndirizziDiSpedizioneDTO indirizzi = null;
    try {
      indirizzi = anagaziendeClient.getIndirizzoSpedizioneClienteFai(securityToken, codiceCliente);
      log.debug("Found indirizzi {} for codice cliente {}", indirizzi, codiceCliente);
    } catch (Exception e) {
      log.error("Exception on retrieve Indirizzi Spedizione by NAV: ", e);
      throw new RuntimeException(e);
    }
    return indirizzi;
  }

  @Override
  public IndirizzoSpedizioneOrdini mapAndpersistIndirizzoSpedizioneOrdini(IndirizzoSpedizioneDTO indirizzoSpedizione) {
    IndirizzoSpedizioneOrdini indirizzo = mapNewIndirizzoDiSpezioneOrdini(indirizzoSpedizione);
    indirizzo = indirizzoSpedizioneRepo.saveAndFlush(indirizzo);
    log.debug("Persisted {}", indirizzo);
    return indirizzo;
  }

  private IndirizzoSpedizioneOrdini mapNewIndirizzoDiSpezioneOrdini(IndirizzoSpedizioneDTO indirizzoSpedizioneDTO) {
    IndirizzoSpedizioneOrdini newIndirizzo = new IndirizzoSpedizioneOrdini();
    newIndirizzo.setFai(true); // Use to set default value;
    if (indirizzoSpedizioneDTO != null) {
      newIndirizzo.setVia(indirizzoSpedizioneDTO.getVia());
      newIndirizzo.setCap(indirizzoSpedizioneDTO.getCap());
      newIndirizzo.setCitta(indirizzoSpedizioneDTO.getCitta());
      newIndirizzo.setProvincia(indirizzoSpedizioneDTO.getProvincia());
      newIndirizzo.setPaese(indirizzoSpedizioneDTO.getPaese());

      Boolean indirizzoFai = indirizzoSpedizioneDTO.getIndirizzoFai();
      newIndirizzo.setFai((indirizzoFai != null) ? indirizzoFai : false);
    } else {
      log.warn("Indirizzo di spedizione is null");
    }

    return newIndirizzo;
  }

}
