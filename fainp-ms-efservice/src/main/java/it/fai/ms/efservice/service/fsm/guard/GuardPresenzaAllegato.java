package it.fai.ms.efservice.service.fsm.guard;

import org.apache.commons.lang3.StringUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardPresenzaAllegato implements Guard<StatoRichiesta, RichiestaEvent> {

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;

        String uuidDocumento = richiesta.getUuidDocumento();
        if (StringUtils.isNotBlank(uuidDocumento)) {
          return true;
        }
      }
    }

    return false;
  }

}
