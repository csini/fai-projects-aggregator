package it.fai.ms.efservice.wizard.web.rest.vm;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
public class MatrixDeviceType {

  private List<MatrixActivabilityFailure> errors;
  private String                          id;
  private String                          itemKey;
  private String                          state;

  public MatrixDeviceType(final String _id) {
    id = _id;
  }

  public void addFailure(final MatrixActivabilityFailure _failure) {
    Optional.ofNullable(_failure)
            .ifPresent(failure -> {
              errors = Optional.ofNullable(errors)
                               .orElse(new LinkedList<>());
              errors.add(_failure);
            });
  }

  @JsonInclude(Include.NON_NULL)
  public List<MatrixActivabilityFailure> getErrors() {
    return errors;
  }

  public String getId() {
    return id;
  }

  public String getItemKey() {
    return itemKey;
  }

  @JsonProperty(value = "stato")
  public String getStato() {
    return state;
  }

  public void setItemKey(final String _itemKey) {
    itemKey = _itemKey;
  }

  public void setState(final String _state) {
    state = _state;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixDeviceType [errors=");
    builder.append(this.errors);
    builder.append(", id=");
    builder.append(this.id);
    builder.append(", itemKey=");
    builder.append(this.itemKey);
    builder.append(", state=");
    builder.append(this.state);
    builder.append("]");
    return builder.toString();
  }

}
