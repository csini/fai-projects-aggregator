package it.fai.ms.efservice.service.fsm.guard;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardTelepassItaInDepositoKo implements Guard<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(GuardTelepassItaInDepositoKo.class);

  private final ManageDevicesInStorageService deviceInStorageServiceExt;

  public GuardTelepassItaInDepositoKo(final ManageDevicesInStorageService _deviceInStorageServiceExt) {
    deviceInStorageServiceExt = _deviceInStorageServiceExt;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    boolean success = false;
    Set<Dispositivo> devicesInDeposito = deviceInStorageServiceExt.findDispositiviByStateAndType(StatoDispositivo.IN_DEPOSITO,
                                                                                                 TipoDispositivoEnum.TELEPASS_ITALIANO);
    if (devicesInDeposito == null || devicesInDeposito.isEmpty()) {
      log.info("Not found Dispositivi IN_DEPOSITO");
      success = true;
    } else {
      Optional<Dispositivo> deviceOpt = devicesInDeposito.stream()
                                                         .filter(d -> !"AS".equalsIgnoreCase(d.getTipoHardware()))
                                                         .findFirst();
      if (!deviceOpt.isPresent()) {
        success = true;
      }
    }
    return success;
  }

}
