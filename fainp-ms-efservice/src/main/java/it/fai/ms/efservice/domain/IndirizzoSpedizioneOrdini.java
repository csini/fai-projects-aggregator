package it.fai.ms.efservice.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A IndirizzoSpedizioneOrdini.
 */
@Entity
@Table(name = "indirizzo_spedizione_ordini")
public class IndirizzoSpedizioneOrdini implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "via")
    private String via;

    @Column(name = "citta")
    private String citta;

    @Column(name = "cap")
    private String cap;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "paese")
    private String paese;

    @Column(name = "fai")
    private Boolean fai;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVia() {
        return via;
    }

    public IndirizzoSpedizioneOrdini via(String via) {
        this.via = via;
        return this;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getCitta() {
        return citta;
    }

    public IndirizzoSpedizioneOrdini citta(String citta) {
        this.citta = citta;
        return this;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCap() {
        return cap;
    }

    public IndirizzoSpedizioneOrdini cap(String cap) {
        this.cap = cap;
        return this;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getProvincia() {
        return provincia;
    }

    public IndirizzoSpedizioneOrdini provincia(String provincia) {
        this.provincia = provincia;
        return this;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPaese() {
        return paese;
    }

    public IndirizzoSpedizioneOrdini paese(String paese) {
        this.paese = paese;
        return this;
    }

    public void setPaese(String paese) {
        this.paese = paese;
    }

    public Boolean isFai() {
        return fai;
    }

    public IndirizzoSpedizioneOrdini fai(Boolean fai) {
        this.fai = fai;
        return this;
    }

    public void setFai(Boolean fai) {
        this.fai = fai;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini = (IndirizzoSpedizioneOrdini) o;
        if (indirizzoSpedizioneOrdini.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), indirizzoSpedizioneOrdini.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IndirizzoSpedizioneOrdini{" +
            "id=" + getId() +
            ", via='" + getVia() + "'" +
            ", citta='" + getCitta() + "'" +
            ", cap='" + getCap() + "'" +
            ", provincia='" + getProvincia() + "'" +
            ", paese='" + getPaese() + "'" +
            ", fai='" + isFai() + "'" +
            "}";
    }
}
