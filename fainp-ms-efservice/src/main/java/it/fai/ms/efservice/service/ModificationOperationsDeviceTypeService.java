package it.fai.ms.efservice.service;

import java.util.List;

import it.fai.common.enumeration.TipoDispositivoEnum;

public interface ModificationOperationsDeviceTypeService {

  List<String> getModificationOperations(TipoDispositivoEnum nome);

}
