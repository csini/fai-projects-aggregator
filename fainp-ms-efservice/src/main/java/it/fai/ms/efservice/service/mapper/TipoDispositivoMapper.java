package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TipoDispositivo and its DTO TipoDispositivoDTO.
 */
@Mapper(componentModel = "spring", uses = {IndirizzoMapper.class, ProduttoreMapper.class, TipoServizioMapper.class, })
public interface TipoDispositivoMapper extends EntityMapper <TipoDispositivoDTO, TipoDispositivo> {

    @Mapping(source = "indirizzoRientro.id", target = "indirizzoRientroId")

    @Mapping(source = "produttore.id", target = "produttoreId")
    TipoDispositivoDTO toDto(TipoDispositivo tipoDispositivo); 

    @Mapping(source = "indirizzoRientroId", target = "indirizzoRientro")

    @Mapping(target = "tipoServizioDefaults", ignore = true)

    @Mapping(source = "produttoreId", target = "produttore")
    @Mapping(target = "dispositivos", ignore = true)
    TipoDispositivo toEntity(TipoDispositivoDTO tipoDispositivoDTO); 
    default TipoDispositivo fromId(Long id) {
        if (id == null) {
            return null;
        }
        TipoDispositivo tipoDispositivo = new TipoDispositivo();
        tipoDispositivo.setId(id);
        return tipoDispositivo;
    }
}
