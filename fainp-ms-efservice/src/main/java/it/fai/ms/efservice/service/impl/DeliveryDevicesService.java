package it.fai.ms.efservice.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.dml.document.DeliveryPrintRequestDTO;
import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.DeliveryDeviceService;
import it.fai.ms.efservice.service.DispositiviDaSpedireCacheService;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;

@Service
@Transactional
public class DeliveryDevicesService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  private final DocumentClient                   documentClient;
  private final DispositiviDaSpedireCacheService deviceDeliveryCache;
  private final VehicleClient                    vehicleClient;
  private final DispositivoRepositoryExt         deviceRepoExt;
  private final DeliveryDeviceService            deliveryDeviceService;

  public DeliveryDevicesService(final DocumentClient documentClient, final DispositiviDaSpedireCacheService deviceDeliveryCache,
                                final VehicleClient vehicleClient, final DispositivoRepositoryExt deviceRepoExt,
                                final DeliveryDeviceService deliveryDeviceService) {
    this.documentClient = documentClient;
    this.deviceDeliveryCache = deviceDeliveryCache;
    this.vehicleClient = vehicleClient;
    this.deviceRepoExt = deviceRepoExt;
    this.deliveryDeviceService = deliveryDeviceService;
  }

  public AsyncJobResponse processDevices(DeliveryPrintDTO dto) {
    log.info("processing print request with dto: {}", dto);

    DeliveryPrintRequestDTO deliveryPrintRequestDTO = new DeliveryPrintRequestDTO();
    Instant deliveryDate = dto.getDataSpedizione();
    LocalDate dataSpedizione = LocalDateTime.ofInstant(deliveryDate, ZoneOffset.UTC)
                                            .toLocalDate();
    deliveryPrintRequestDTO.setDeliveryDate(dataSpedizione);
    deliveryPrintRequestDTO.setMultiDispositivo(true);
    deliveryPrintRequestDTO.setCompanyGroups(new ArrayList<>());

    List<String> spedizioni = dto.getSpedizioni();
    log.info("Spedizioni: {}", spedizioni);
    if (spedizioni == null) {
      log.debug("Print delivery ALL devices...");
      Set<Dispositivo> devices = deviceRepoExt.findByRichiestasIsNullAndStatoIs(StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE);
      log.info("Devices in state {} found: {}", StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE, (devices != null ? devices.size() : 0));
      devices.stream()
             .filter(d -> this.canBePrinted(d))
             .collect(Collectors.groupingBy(d -> {
               return d.getContratto()
                       .getClienteFai()
                       .getCodiceCliente();
             }))
             .forEach((codiceCliente, dispositivi) -> {
               Map<TipoDispositivoEnum, List<Dispositivo>> map = dispositivi.stream()
                                                                            .map(d -> updateStateAndDeliveryDateDevice(d, deliveryDate))
                                                                            .collect(Collectors.groupingBy(d -> {
                                                                              return d.getTipoDispositivo()
                                                                                      .getNome();
                                                                            }));
               deliveryDeviceService.getDeliveryDtoByDevicesCustomer(codiceCliente, map, deliveryPrintRequestDTO);
             });
    } else {
      log.debug("Print delivery {} devices...", spedizioni.size());
      spedizioni.forEach(codiceCliente -> {
        Set<Dispositivo> devicesClient = deviceRepoExt.findByRichiestasIsNullAndStatoIsAndContratto_ClienteFai_codiceCliente(StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE,
                                                                                                                             codiceCliente);
        log.info("Devices for Client {} in state {} found: {}", codiceCliente, StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE,
                 (devicesClient != null ? devicesClient.size() : 0));
        Map<TipoDispositivoEnum, List<Dispositivo>> map = devicesClient.stream()
                                                                       .filter(device -> this.canBePrinted(device))
                                                                       .map(d -> updateStateAndDeliveryDateDevice(d, deliveryDate))
                                                                       .collect(Collectors.groupingBy(d -> {
                                                                         return d.getTipoDispositivo()
                                                                                 .getNome();
                                                                       }));

        deliveryDeviceService.getDeliveryDtoByDevicesCustomer(codiceCliente, map, deliveryPrintRequestDTO);
      });
    }

    log.info("Finish proccess of devices print request. Asking for MSdocument to print pdf: {}", deliveryPrintRequestDTO);
    Map<String, String> documentResponse = documentClient.printDelivery(jwt, deliveryPrintRequestDTO);

    return new AsyncJobResponse(StateChangeStatusFsm.SUCCESS, documentResponse);
  }

  private Dispositivo updateStateAndDeliveryDateDevice(Dispositivo device, Instant deliveryDate) {
    StatoDispositivo deviceStatus = StatoDispositivo.ATTIVO_SPEDITO_DA_FAI;
    device = deviceRepoExt.save(device.stato(deviceStatus)
                                      .dataSpedizione(deliveryDate));
    log.info("Updated data spedizione {} on device: {}", device.getDataSpedizione(), device.getIdentificativo());
    return device;
  }

  private Boolean canBePrinted(Dispositivo device) {
    if (device == null) {
      return false;
    }

    String identificativo = device.getIdentificativo();
    Boolean canBePrinted = BooleanUtils.isTrue(deviceDeliveryCache.get(identificativo));
    if (canBePrinted) {
      log.debug("Not found device: {} in list that can be delivery", identificativo);
      return true;
    }

    Set<AssociazioneDV> advs = device.getAssociazioneDispositivoVeicolos();
    if (advs == null || advs.isEmpty()) {
      log.info("Not found associazione DVs on Device: {}", device.getIdentificativo());
      return true;
    } else {
      List<String> uuidsVehicle = advs.parallelStream()
                                      .filter(adv -> adv != null && StringUtils.isNotBlank(adv.getUuidVeicolo()))
                                      .map(AssociazioneDV::getUuidVeicolo)
                                      .collect(Collectors.toList());

      if (uuidsVehicle == null || uuidsVehicle.isEmpty()) {
        log.info("Not found UUID vehicle related on Device: {}", device.getIdentificativo());
        return true;
      } else {
        List<VehicleDTO> issues = vehicleClient.findVehicleWithoutBooklet(jwt, uuidsVehicle);
        Boolean hasIssues = issues != null && issues.isEmpty();
        canBePrinted = !hasIssues;
        log.debug("Device identifier: {} has issues: {}", identificativo, hasIssues);
        return canBePrinted;
      }
    }

  }

}
