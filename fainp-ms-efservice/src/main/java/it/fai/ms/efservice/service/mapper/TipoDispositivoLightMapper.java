package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTOLight;

import org.mapstruct.*;

/**
 * Mapper for the entity TipoDispositivo and its DTO TipoDispositivoDTOLight.
 */
@Mapper(componentModel = "spring")
public interface TipoDispositivoLightMapper extends EntityMapper <TipoDispositivoDTOLight, TipoDispositivo> {

    TipoDispositivoDTOLight toDto(TipoDispositivo tipoDispositivo); 

    TipoDispositivo toEntity(TipoDispositivoDTOLight tipoDispositivoDTO); 
    default TipoDispositivo fromId(Long id) {
        if (id == null) {
            return null;
        }
        TipoDispositivo tipoDispositivo = new TipoDispositivo();
        tipoDispositivo.setId(id);
        return tipoDispositivo;
    }
}
