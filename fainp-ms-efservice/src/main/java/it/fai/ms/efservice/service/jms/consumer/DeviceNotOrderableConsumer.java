package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceNotOrderableMessage;
import it.fai.ms.efservice.service.jms.JmsDeviceErrorService;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceResponseKOConsumer;

@Service 
public class DeviceNotOrderableConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsDeviceErrorService jmsDeviceErrorService;

  private final DeviceResponseKOConsumer deviceResponseKoConsumer;

  public DeviceNotOrderableConsumer(final DeviceResponseKOConsumer _deviceResponseKoConsumer,
                                    final JmsDeviceErrorService _jmsDeviceErrorService) {
    deviceResponseKoConsumer = _deviceResponseKoConsumer;
    jmsDeviceErrorService = _jmsDeviceErrorService;
  }

  public void consume(DeviceNotOrderableMessage message) {
    log.info("Manage message: {}", message);
    String identificativoRichiesta = message.getOrderRequestUuid()
                                            .getUuid();
    String errorMsg = jmsDeviceErrorService.buildErrorMessage(message);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

  public void consume(DeviceEventMessage eventMessage) {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();

    String errorMsg = jmsDeviceErrorService.buildErrorMessage(eventMessage);
    deviceResponseKoConsumer.consume(identificativoRichiesta, errorMsg);
  }

}
