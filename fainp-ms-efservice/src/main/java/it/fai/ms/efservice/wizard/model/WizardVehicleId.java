package it.fai.ms.efservice.wizard.model;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class WizardVehicleId implements Serializable {

  private static final long serialVersionUID = 5917811276486225136L;

  public static Set<WizardVehicleId> mapToWizardVehicleIds(final Set<String> _vehicleIds) {
    return _vehicleIds.stream()
                      .filter(vehicleId -> vehicleId != null && !vehicleId.isEmpty())
                      .map(itm -> new WizardVehicleId(itm))
                      .collect(toSet());
  }

  private String id;

  public WizardVehicleId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardVehicleId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return this.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardVehicleId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
