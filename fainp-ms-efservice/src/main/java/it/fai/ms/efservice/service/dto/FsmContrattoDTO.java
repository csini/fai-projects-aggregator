package it.fai.ms.efservice.service.dto;

import static java.util.stream.Collectors.toSet;

import java.util.Map;
import java.util.Set;

public class FsmContrattoDTO extends FsmDTO {

  private static final long serialVersionUID = -7811389338086277602L;

  private Map<String, String> mapCodContrattoProduttore;

  public Map<String, String> getMapCodContrattoProduttore() {
    return mapCodContrattoProduttore;
  }

  public void setMapCodContrattoProduttore(Map<String, String> mapCodContrattoProduttore) {
    this.mapCodContrattoProduttore = mapCodContrattoProduttore;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("FsmContrattoDTO ");
    sb.append("[");

    sb.append("operazione: ");
    sb.append(getOperazione());
    sb.append(", Codici Contratto - Produttore: ");
    sb.append(getMapToString(mapCodContrattoProduttore));
    sb.append(", nota: ");
    sb.append(getNota());

    sb.append("]");
    return sb.toString();
  }

  private String getMapToString(Map<String, String> map) {
    StringBuilder mapToString = new StringBuilder("");
    if (map != null && !map.isEmpty()) {
      Set<String> keySet = map.keySet();
      keySet.stream()
            .map(k -> {
              String value = map.get(k);
              mapToString.append(k + " - " + value + "\n");
              return k;
            })
            .collect(toSet());
    }

    return mapToString.toString();
  }

}
