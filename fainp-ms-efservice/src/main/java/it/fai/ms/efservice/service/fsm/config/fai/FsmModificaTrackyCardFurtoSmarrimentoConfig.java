package it.fai.ms.efservice.service.fsm.config.fai;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDisableAllService;
import it.fai.ms.efservice.service.fsm.action.FsmActionFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTrackyCardFurtoSmarrimentoConfig.MOD_TRACKYCARD_FURTO_SMARRIMENTO)
public class FsmModificaTrackyCardFurtoSmarrimentoConfig extends FsmRichiestaConfig {

  public static final String MOD_TRACKYCARD_FURTO_SMARRIMENTO = "ModTrackyCardFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue                              senderFsmService;
  private final DeviceServiceActivationMessageProducerService deviceServiceActivationService;

  public FsmModificaTrackyCardFurtoSmarrimentoConfig(final FsmSenderToQueue _senderFsmService,
                                                     final DeviceServiceActivationMessageProducerService _deviceServiceActivationService) {
    senderFsmService = _senderFsmService;
    deviceServiceActivationService = _deviceServiceActivationService;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmModificaTrackyCardFurtoSmarrimento");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY, ctx -> log.trace("Initial: " + ctx.getTarget()
                                                                                                .getIds()))
          .states(StatoRichiestaUtil.getStateOfModificaTrackyCardFurtoSmarrimento());
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TRACKY_FURTO_SMARRIMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaTrackyCardFurtoSmarrimento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.SMARRIMENTO)
               .guard(new GuardSmarrimento())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.FURTO)
               .guard(new GuardFurto())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.SMARRIMENTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(new FsmActionDisableAllService(deviceServiceActivationService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.FURTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(new FsmActionDisableAllService(deviceServiceActivationService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.EVASO);
  }

}
