package it.fai.ms.efservice.rules.engine.repository.entity;

import org.infinispan.persistence.keymappers.TwoWayKey2StringMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleEntityKeyMapper implements TwoWayKey2StringMapper {

  public final static String SEPARATOR = "@";

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  public RuleEntityKeyMapper() {
  }

  @Override
  public Object getKeyMapping(final String _str) {
    final String[] split = _str.split(SEPARATOR);
    final RuleEntityKey ruleEntityKey = new RuleEntityKey();
    ruleEntityKey.setServiceTypeId(split[0]);
    ruleEntityKey.setDeviceTypeId(split[1]);
    ruleEntityKey.setVehicleId(split[2]);
    _log.debug("From string {} to : {}", _str, ruleEntityKey);
    return ruleEntityKey;
  }

  @Override
  public String getStringMapping(final Object _key) {
    String res = null;
    final RuleEntityKey ruleEntityKey = (RuleEntityKey) _key;
    res = ruleEntityKey.getServiceTypeId() + SEPARATOR + ruleEntityKey.getDeviceTypeId() + SEPARATOR + ruleEntityKey.getVehicleId();
    _log.debug("From key {} to : {}", _key, res);
    return res;
  }

  @Override
  public boolean isSupportedType(final Class<?> keyType) {
    final boolean isSupported = keyType.equals(RuleEntityKey.class);
    if (!isSupported) {
      _log.error("Type {} is not supported: {}", keyType, isSupported);
    }
    return isSupported;
  }

}
