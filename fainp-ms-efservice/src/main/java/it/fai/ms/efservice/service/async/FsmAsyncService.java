package it.fai.ms.efservice.service.async;

import java.io.Serializable;

import it.fai.ms.efservice.service.dto.FsmContrattoDTO;
import it.fai.ms.efservice.service.dto.FsmDTO;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.dto.FsmRichiesteDTO;

public interface FsmAsyncService {

  /**
   * @return
   */
  String getKeyCacheAndPushDefaultValue();

  /**
   * @param string
   * @return
   */
  String getKeyCacheAndPushDefaultValue(String string);

  /**
   * @param fsmChangeStatusDTO
   * @return
   * @throws Exception
   */
  void changeAsyncFsmStatus(String identificativoJob, FsmRichiesteDTO fsmChangeStatusDTO);

  /**
   * @param uuidFsmChangeStatus
   * @param fsmDTO
   */
  void changeAsyncFsmContratto(String uuidFsmChangeStatus, FsmContrattoDTO fsmDTO);
  
  /**
   * @param uuidFsmChangeStatus
   * @param fsmDTO
   */
  void changeAsyncStatusContratto(String uuidFsmChangeStatus, FsmDTO fsmDTO);

  /**
   * @param identificativoJon
   * @param fsmDTO
   */
  void changeAsyncFsmDispositivo(String identificativoJon, FsmDispositivoDTO fsmDTO);

  /**
   * @param identifierJob
   * @param state
   */
  void saveStateJob(String identifierJob, Serializable state);

}
