package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Set;

import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

public interface ContrattoNavService {

  List<ContrattoNavDTO> findContractsToNav(String codiceCliente);

  Contratto changeStatusContractByNav(String identificativo, ContrattoEvent event) throws Exception;

  StateChangeStatusFsm changeAsyncStatusContractByNav(Set<String> identifierContracts, ContrattoEvent event);

}
