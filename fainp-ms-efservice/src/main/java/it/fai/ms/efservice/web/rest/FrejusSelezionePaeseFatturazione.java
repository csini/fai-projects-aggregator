package it.fai.ms.efservice.web.rest;

import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.frejus.ContrattiFrejusService;

@RestController
@RequestMapping(FrejusSelezionePaeseFatturazione.BASE_PATH)
public class FrejusSelezionePaeseFatturazione {
	public static final String BASE_PATH = "/api/public";
	private final ContrattiFrejusService contrattiService;

	public static final class NazioneFatturazione {
		public NazioneFatturazione() {
		}

		public NazioneFatturazione(String nazioneFatturazione) {
			this.nazioneFatturazione = nazioneFatturazione;
		}

		public String nazioneFatturazione;
	}

	public FrejusSelezionePaeseFatturazione(ContrattiFrejusService contrattiService) {
		this.contrattiService = contrattiService;
	}

	/*
	 * creazione contratto POST
	 * /fai/api/efservice/1.0/api/public/nazioneFatturazione/:codiceAzienda/:
	 * tipoDispositivo
	 * 
	 * Request:
	 * 
	 * codiceAzienda query param tipoDispositivo query param body {
	 * "nazioneFatturazione": "IT" } Response:
	 * 
	 * response body { "nazioneFatturazione": "IT" }
	 */
	@PostMapping("nazioneFatturazione/{codiceAzienda}/{tipoDispositivo}")
	@Timed
	public ResponseEntity<NazioneFatturazione> saveNazioneFatturazione(@PathVariable String codiceAzienda,
			@PathVariable TipoDispositivoEnum tipoDispositivo,
			@RequestBody(required = true) NazioneFatturazione nazioneFatturazione) {
		return createResponse(contrattiService.saveNazioneFatturazione(codiceAzienda, tipoDispositivo,
				nazioneFatturazione.nazioneFatturazione));
	}

	/*
	 * modifica Contratto PUT
	 * /fai/api/efservice/1.0/api/public/nazioneFatturazione/:codiceAzienda/:
	 * tipoDispositivo
	 */
	@PutMapping("nazioneFatturazione/{codiceAzienda}/{tipoDispositivo}")
	@Timed
	public ResponseEntity<NazioneFatturazione> changeNazioneFatturazione(@PathVariable String codiceAzienda,
			@PathVariable TipoDispositivoEnum tipoDispositivo,
			@RequestBody(required = true) NazioneFatturazione nazioneFatturazione) {
		return createResponse(contrattiService.changeNazioneFatturazione(codiceAzienda, tipoDispositivo,
				nazioneFatturazione.nazioneFatturazione));
	}

	/*
	 * verifica nazione fatturazione GET
	 * /fai/api/efservice/1.0/api/public/nazioneFatturazione/:codiceAzienda/:
	 * tipoDispositivo
	 * 
	 * Request:
	 * 
	 * codiceAzienda query param tipoDispositivo query param Response: 200 ok 404
	 * nazione fatturazione mancante
	 */
	@GetMapping("nazioneFatturazione/{codiceAzienda}/{tipoDispositivo}")
	@Timed
	public ResponseEntity<NazioneFatturazione> getNazioneFatturazione(@PathVariable String codiceAzienda,
			@PathVariable TipoDispositivoEnum tipoDispositivo) {
		return createResponse(contrattiService.getNazioneFatturazione(codiceAzienda, tipoDispositivo));
	}

	private ResponseEntity<NazioneFatturazione> createResponse(Optional<String> nazione) {
		if (nazione.isPresent())
			return ResponseEntity.ok(new NazioneFatturazione(nazione.get()));
		return ResponseEntity.notFound().build();
	}
}
