package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.common.dto.efservice.ContractCommand;
import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.ContrattoNavService;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.CreatedFsmChangeStatusAsyncDTO;
import it.fai.ms.efservice.service.dto.FsmDTO;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(ContrattoNavResource.BASE_PATH)
public class ContrattoNavResource {

  private final Logger log = LoggerFactory.getLogger(ContrattoNavResource.class);

  public static final String BASE_PATH = "/api/public";

  public static final String POLLING = "/polling";

  private static final String LIST_CONTRATTI = "/nav/contratti";

  private static final String CHANGE_STATUS_CONTRATTO = "/nav/contratto/cambiostato";

  public static final String FSM_CONTRATTO = "/fsmcontratto";

  public static final String FSM_POLLING_CONTRATTO = FSM_CONTRATTO + POLLING;

  private static final String KEY_JOB = "job";

  private static final String CONTRATTI_KEY_JOB = "FSM-CONTRACT";

  private final ContrattoNavService contrattoNavService;

  private final FsmAsyncService fsmAsyncService;

  private final AsyncPollingJobServiceImpl asyncPollingJobService;
  
  private final ContrattoServiceExt contrattoService;


  public ContrattoNavResource(final ContrattoNavService _contrattoNavService, final FsmAsyncService _fsmAsyncService,
                              final AsyncPollingJobServiceImpl _asyncPollingJobService,final ContrattoServiceExt _contrattoService) {
    contrattoNavService = _contrattoNavService;
    fsmAsyncService = _fsmAsyncService;
    asyncPollingJobService = _asyncPollingJobService;
    contrattoService = _contrattoService;
  }

  @GetMapping(LIST_CONTRATTI)
  @Timed
  public ResponseEntity<List<ContrattoNavDTO>> getContractsForNav(@RequestParam(
                                                                                value = "codiceCliente",
                                                                                required = false) String codiceCliente) throws URISyntaxException,
                                                                                                                        CustomException {
    StringBuilder sb = new StringBuilder("REST request to get all contract");
    if (StringUtils.isNotBlank(codiceCliente)) {
      sb.append(" by codice cliente: " + codiceCliente);
    }
    log.debug(sb.toString());
    List<ContrattoNavDTO> contractsDTO = new ArrayList<>();
    try {
      contractsDTO = contrattoNavService.findContractsToNav(codiceCliente);
    } catch (Exception e) {
      log.error("Exception on retrieve Contract to NAV. ", e);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.NOT_FOUND_CONTRATTO_CLIENTEFAI);
    }
    return ResponseEntity.ok(contractsDTO);
  }

  @PutMapping(CHANGE_STATUS_CONTRATTO + "/{identificativo}/{command}")
  @Timed
  public ResponseEntity<String> changeStatusContractByIdentificativo(@PathVariable(
                                                                                   name = "identificativo",
                                                                                   required = true) String identificativo,
                                                                     @PathVariable(
                                                                                   name = "command",
                                                                                   required = true) ContractCommand command) throws CustomException {
    log.debug("REST request to change status to Contract identifier {} through command {}", identificativo, command);
    Contratto contract = null;
    ContrattoEvent event = mapToContrattoEvent(command);
    try {
      contract = contrattoNavService.changeStatusContractByNav(identificativo, event);
    } catch (Exception e) {
      log.error("Exception on change status", e);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_ON_PROCESSING_CHANGE_STATUS);
    }

    if (StringUtils.isBlank(contract.getIdentificativo())) {
      log.error("Not found contract with identifier: " + identificativo);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_ON_PROCESSING_CHANGE_STATUS);
    }
    return ResponseEntity.ok(contract.getIdentificativo());
  }

  @PostMapping(FSM_CONTRATTO)
  @Timed
  public ResponseEntity<Map<String, String>> changeStatusContratto(@RequestBody FsmDTO dto) throws CustomException {
    log.debug("Change status for contracts...");
    long ti = System.currentTimeMillis();
    Set<String> identificativi = dto.getIdentificativi();
    String operazione = dto.getOperazione();
    validationDTO(identificativi, operazione);
    log.debug("Change status for identificativi {} with operation: {}", identificativi, operazione);

    String identifierJob = fsmAsyncService.getKeyCacheAndPushDefaultValue(CONTRATTI_KEY_JOB);
    log.info("Identificativi {} is related to async job: {}", identificativi, identifierJob);
    log.debug("Create Async Job to change status by FSM-Contratto with identifier {}", identifierJob);
    /*  
    for (String uuid : identificativi) {
        contrattoService.findByIdentificativo(uuid).filter(c -> c.getCodContrattoCliente()!=null && !c.getCodContrattoCliente().isEmpty()).orElseThrow(() -> new IllegalArgumentException("Contratto con uuid "+uuid+" non ha il numero contratto"));
	}
	*/
    
    fsmAsyncService.changeAsyncStatusContratto(identifierJob, dto);

    long tf = System.currentTimeMillis();
    log.info("Finish change status contratto in: {} s", (tf - ti));
    return getResponse(identifierJob);
  }

  private void validationDTO(Set<String> identificativi, String operazione) throws CustomException {
    if (identificativi == null || identificativi.isEmpty()) {
      log.error("Identificativi contracts are not found....");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    if (StringUtils.isBlank(operazione)) {
      log.error("Operazione is null or empty");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    try {
      ContrattoEvent.valueOf(operazione);
    } catch (Exception e) {
      log.error("Operation {} to change status contracts are not in {}", operazione, ContrattoEvent.values());
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

  }

  @GetMapping(FSM_POLLING_CONTRATTO + "/{identificativo}")
  @Timed
  public ResponseEntity<CreatedFsmChangeStatusAsyncDTO> pollingFsmContratto(@PathVariable String identificativo) {
    log.debug("Rest request for polling FSM-Contratto change status {}", identificativo);
    return ResponseEntity.ok(getResponsePolling(identificativo));
  }

  private ContrattoEvent mapToContrattoEvent(ContractCommand command) {
    ContrattoEvent event = null;
    switch (command) {
    case ATTIVA:
      event = ContrattoEvent.MS_RIATTIVA;
      break;
    case SOSPENDI:
      event = ContrattoEvent.MS_SOSPENDI;
      break;
    case REVOCA:
      event = ContrattoEvent.MU_REVOCA;
      break;
    default:
      throw new IllegalArgumentException("Command is not valid: " + command);
    }

    return event;
  }

  private ResponseEntity<Map<String, String>> getResponse(String identifierJob) {
    Map<String, String> response = new HashMap<>();
    response.put(KEY_JOB, identifierJob);
    return ResponseEntity.ok(response);
  }

  private CreatedFsmChangeStatusAsyncDTO getResponsePolling(String identificativoJob) {
    StateChangeStatusFsm result = (StateChangeStatusFsm) asyncPollingJobService.getValue(identificativoJob);
    CreatedFsmChangeStatusAsyncDTO createdFsmChangeStatusAsyncDTO = new CreatedFsmChangeStatusAsyncDTO(result);
    log.info("Response polling is {}", createdFsmChangeStatusAsyncDTO);
    return createdFsmChangeStatusAsyncDTO;
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
