package it.fai.ms.efservice.service.jms.listener.consumi;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.consumi.CambioStatoDaConsumo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;

@Service
@Transactional
public class JmsCambioStatoDaConsumoListener extends JmsObjectMessageListenerTemplate<CambioStatoDaConsumo> implements JmsQueueListener {

  private static final Logger log = LoggerFactory.getLogger(JmsCambioStatoDaConsumoListener.class);

  private final DispositivoRepositoryExt  dispositivoRepositoryExt;
  private final TipoDispositivoServiceExt tipoDispositivoServiceExt;

  public JmsCambioStatoDaConsumoListener(DispositivoRepositoryExt dispositivoRepositoryExt,
                                         TipoDispositivoServiceExt tipoDispositivoServiceExt) {
    this.dispositivoRepositoryExt  = dispositivoRepositoryExt;
    this.tipoDispositivoServiceExt = tipoDispositivoServiceExt;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.CAMBIO_STATO_DA_CONSUMO;
  }

  @Override
  protected void consumeMessage(CambioStatoDaConsumo message) {
    Instant           dataCambioStato = Optional.ofNullable(message.getDataModificaStato()).orElse(Instant.now());
    List<Dispositivo> devices         = getDevices(message);
    if (validateDevices(message, devices)) {
      devices.stream()
        .map(d -> d.stato(message.getStato()).dataModificaStato(dataCambioStato))
        .forEach(d -> dispositivoRepositoryExt.save(d));
    }
  }

  private List<Dispositivo> getDevices(CambioStatoDaConsumo message) {
    List<Dispositivo> devices = Collections.emptyList();
    if (message.getIdentificativo() != null) {
      devices = Arrays.asList(dispositivoRepositoryExt.findOneByIdentificativo(message.getIdentificativo()));
    } else if (message.getTipoDispositivo() != null && message.getSeriale() != null) {
      TipoDispositivo tipoDispositivo = tipoDispositivoServiceExt.findOneByNome(message.getTipoDispositivo());
      devices = dispositivoRepositoryExt.findBySerialeAndTipoDispositivo(message.getSeriale(), tipoDispositivo);
    }
    return devices;
  }

  private boolean validateDevices(CambioStatoDaConsumo message, List<Dispositivo> devices) {
    boolean result = true;
    if (message.getSeriale() != null && devices.stream().anyMatch(d -> !message.getSeriale().equals(d.getSeriale()))) {
      log.error("not consistent device identifier {} Cannot change status", message);
      result = false;
    }
    if (message.getTipoDispositivo() != null
        && devices.stream().anyMatch(d -> !message.getTipoDispositivo().equals(d.getTipoDispositivo().getNome()))) {
      log.error("not consistent device identifier {} Cannot change status", message);
      result = false;
    }
    return result;
  }

}
