package it.fai.ms.efservice.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import it.fai.ms.efservice.domain.enumeration.TipoDisposizione;

/**
 * A DTO for the OrdineFornitore entity.
 */
public class OrdineFornitoreDTO implements Serializable {

  private Long id;

  private Integer quantita;

  private TipoDisposizione tipologiaDisposizione;

  private String richiedente;

  private Instant dataRiordino;

  @NotNull
  @Min(value = 0)
  private Integer numeroDispositiviAcquisiti;

  private Long anagraficaGiacenzaId;

  private String note;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getQuantita() {
    return quantita;
  }

  public void setQuantita(Integer quantita) {
    this.quantita = quantita;
  }

  public TipoDisposizione getTipologiaDisposizione() {
    return tipologiaDisposizione;
  }

  public void setTipologiaDisposizione(TipoDisposizione tipologiaDisposizione) {
    this.tipologiaDisposizione = tipologiaDisposizione;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }

  public Instant getDataRiordino() {
    return dataRiordino;
  }

  public void setDataRiordino(Instant dataRiordino) {
    this.dataRiordino = dataRiordino;
  }

  public Integer getNumeroDispositiviAcquisiti() {
    return numeroDispositiviAcquisiti;
  }

  public void setNumeroDispositiviAcquisiti(Integer numeroDispositiviAcquisiti) {
    this.numeroDispositiviAcquisiti = numeroDispositiviAcquisiti;
  }

  public Long getAnagraficaGiacenzaId() {
    return anagraficaGiacenzaId;
  }

  public void setAnagraficaGiacenzaId(Long anagraficaGiacenzaId) {
    this.anagraficaGiacenzaId = anagraficaGiacenzaId;
  }

  public String getNote() {
    return note;
  }

  public OrdineFornitoreDTO note(String note) {
    this.note = note;
    return this;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    OrdineFornitoreDTO ordineFornitoreDTO = (OrdineFornitoreDTO) o;
    if (ordineFornitoreDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), ordineFornitoreDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "OrdineFornitoreDTO{" + "id=" + getId() + ", quantita=" + getQuantita() + ", tipologiaDisposizione='"
           + getTipologiaDisposizione() + "'" + ", richiedente='" + getRichiedente() + "'" + ", dataRiordino='" + getDataRiordino() + "'"
           + ", numeroDispositiviAcquisiti=" + getNumeroDispositiviAcquisiti() + ", note=" + getNote() + "}";
  }

}
