package it.fai.ms.efservice.service.specifications;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.SearchRaggruppamentoDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class SearchRaggruppamentoOrdineSpecification implements Specification<RaggruppamentoRichiesteFornitoreView> {

  private final SearchRaggruppamentoDTO dto;

  public SearchRaggruppamentoOrdineSpecification(SearchRaggruppamentoDTO dto) {
    this.dto = dto;
  }

  @Override
  public Predicate toPredicate(
    Root<RaggruppamentoRichiesteFornitoreView> root,
    CriteriaQuery<?> criteriaQuery,
    CriteriaBuilder builder
  ) {
    List<Predicate> predicates = new ArrayList<>();

    if (StringUtils.isNotEmpty(dto.getProduttore())) {
      predicates.add(
        builder.equal(
          root.get(RaggruppamentoRichiesteFornitoreView_.produttore),
          dto.getProduttore()
        )
      );
    }

    if (StringUtils.isNotEmpty(dto.getStato())) {
      predicates.add(
        builder.equal(
          root.get(RaggruppamentoRichiesteFornitoreView_.stato),
          dto.getStato()
        )
      );
    }

    if(StringUtils.isNotEmpty(dto.getTipoDispositivo())){
      predicates.add(
        builder.equal(
          root.get(RaggruppamentoRichiesteFornitoreView_.tipoDispositivo),
          dto.getTipoDispositivo()
        )
      );
    }

    if(dto.getDataInvioFile() != null){
      predicates.add(
        builder.equal(
          root.get(RaggruppamentoRichiesteFornitoreView_.dataUltimaAcquisizione),
          dto.getDataInvioFile()
        )
      );
    }

    if(dto.getDataAcquisizioneFile() != null){
      predicates.add(
        builder.equal(
          root.get(RaggruppamentoRichiesteFornitoreView_.dataUltimaAcquisizione),
          dto.getDataAcquisizioneFile()
        )
      );
    }

    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
  }
}
