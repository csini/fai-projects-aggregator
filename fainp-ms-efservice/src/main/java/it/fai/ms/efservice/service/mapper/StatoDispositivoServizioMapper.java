package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity StatoDispositivoServizio and its DTO StatoDispositivoServizioDTO.
 */
@Mapper(componentModel = "spring", uses = {TipoServizioMapper.class, DispositivoMapper.class, })
public interface StatoDispositivoServizioMapper extends EntityMapper <StatoDispositivoServizioDTO, StatoDispositivoServizio> {

    @Mapping(source = "tipoServizio.id", target = "tipoServizioId")
    @Mapping(source = "tipoServizio.nome", target = "tipoServizioNome")

    @Mapping(source = "dispositivo.id", target = "dispositivoId")
    @Mapping(source = "dispositivo.seriale", target = "dispositivoSeriale")
    @Mapping(source = "dispositivo.identificativo", target = "dispositivoIdentificativo")
    StatoDispositivoServizioDTO toDto(StatoDispositivoServizio statoDispositivoServizio);

    @Mapping(source = "tipoServizioId", target = "tipoServizio")

    @Mapping(source = "dispositivoId", target = "dispositivo")
    StatoDispositivoServizio toEntity(StatoDispositivoServizioDTO statoDispositivoServizioDTO);
    default StatoDispositivoServizio fromId(Long id) {
        if (id == null) {
            return null;
        }
        StatoDispositivoServizio statoDispositivoServizio = new StatoDispositivoServizio();
        statoDispositivoServizio.setId(id);
        return statoDispositivoServizio;
    }
}
