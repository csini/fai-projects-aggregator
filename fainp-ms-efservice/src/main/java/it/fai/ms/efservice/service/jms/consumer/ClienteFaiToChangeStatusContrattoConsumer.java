package it.fai.ms.efservice.service.jms.consumer;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.enumeration.StatoAzienda;
import it.fai.ms.efservice.service.ContrattoNavService;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

@Service
@Transactional
public class ClienteFaiToChangeStatusContrattoConsumer {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final ContrattoNavService contrattoNavService;

  public ClienteFaiToChangeStatusContrattoConsumer(final ContrattoNavService _contrattoNavService) {
    contrattoNavService = _contrattoNavService;
  }

  public void consume(AziendaDMLDTO dmlDto) {
    String codiceCliente = dmlDto.getCodiceAzienda();
    if (isStatoBloccato(dmlDto.getStato())) {
      List<ContrattoNavDTO> contractsByCliente = contrattoNavService.findContractsToNav(codiceCliente);
      List<ContrattoNavDTO> contractsFiltered = filterContracts(contractsByCliente);

      contractsFiltered.forEach(contract -> {
        try {
          contrattoNavService.changeStatusContractByNav(contract.getIdentificativo(), ContrattoEvent.MU_REVOCA);
        } catch (Exception e) {
          log.error("Exception on manage message to revoke contract by changed stato azienda (DML)", e);
          throw new RuntimeException(e);
        }
      });
    } else {
      log.info("Stato azienda is not {}, so skip message...", StatoAzienda.BLOCCATO);
    }
  }

  private List<ContrattoNavDTO> filterContracts(List<ContrattoNavDTO> contractsByCliente) {
    return contractsByCliente.parallelStream()
                             .filter(c -> c.getStato() != StatoContratto.REVOCATO.name())
                             .collect(toList());
  }

  private boolean isStatoBloccato(String stato) {
    return StatoAzienda.valueOf(stato)
                       .equals(StatoAzienda.BLOCCATO);
  }

}
