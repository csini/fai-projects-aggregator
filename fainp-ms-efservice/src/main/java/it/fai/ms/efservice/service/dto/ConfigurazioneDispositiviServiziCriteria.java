package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the ConfigurazioneDispositiviServizi entity. This class is used in ConfigurazioneDispositiviServiziResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /configurazione-dispositivi-servizis?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ConfigurazioneDispositiviServiziCriteria implements Serializable {
    /**
     * Class for filtering ModalitaGestione
     */
    public static class ModalitaGestioneFilter extends Filter<ModalitaGestione> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private ModalitaGestioneFilter modalitaGestione;

    private LongFilter tipoDispositivoId;

    private LongFilter tipoServizioId;

    public ConfigurazioneDispositiviServiziCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ModalitaGestioneFilter getModalitaGestione() {
        return modalitaGestione;
    }

    public void setModalitaGestione(ModalitaGestioneFilter modalitaGestione) {
        this.modalitaGestione = modalitaGestione;
    }

    public LongFilter getTipoDispositivoId() {
        return tipoDispositivoId;
    }

    public void setTipoDispositivoId(LongFilter tipoDispositivoId) {
        this.tipoDispositivoId = tipoDispositivoId;
    }

    public LongFilter getTipoServizioId() {
        return tipoServizioId;
    }

    public void setTipoServizioId(LongFilter tipoServizioId) {
        this.tipoServizioId = tipoServizioId;
    }

    @Override
    public String toString() {
        return "ConfigurazioneDispositiviServiziCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (modalitaGestione != null ? "modalitaGestione=" + modalitaGestione + ", " : "") +
                (tipoDispositivoId != null ? "tipoDispositivoId=" + tipoDispositivoId + ", " : "") +
                (tipoServizioId != null ? "tipoServizioId=" + tipoServizioId + ", " : "") +
            "}";
    }

}
