package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.dml.document.CompanyGroupDTO;
import it.fai.ms.common.dml.document.ContrattiDTO;
import it.fai.ms.common.dml.document.DeliveryDTO;
import it.fai.ms.common.dml.document.DeliveryPrintProduttoreRequestDTO;
import it.fai.ms.common.dml.document.DeliveryPrintRequestDTO;
import it.fai.ms.common.dml.document.DeliveryProduttoreDTO;
import it.fai.ms.common.dml.document.DeviceInfoDelivery;
import it.fai.ms.common.dml.document.DispositivoDTO;
import it.fai.ms.common.dml.document.InfoDeliveryProduttore;
import it.fai.ms.common.dml.document.ServizioDTO;
import it.fai.ms.common.dml.document.TipiDispositivo;
import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.common.jms.dto.efservice.DeliveryCompanyDTO;
import it.fai.ms.efservice.client.AnagaziendeClientService;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.client.dto.IndirizzoSpedizioneDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.service.impl.ClienteFaiServiceImplExt;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

@Service
public class DeliveryDeviceService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final AnagaziendeClientService           anagaziendeClientService;
  private final ClienteFaiServiceImplExt           clienteFaiService;
  private final StatoDispositivoServizioServiceExt statoDispositivoServizioService;
  private final DispositivoServiceExt              dispositivoService;
  private final GatewayClientService               gatewayClientService;

  private final Map<String, WizardVehicleEntity> cacheVehicleWizard;

  public DeliveryDeviceService(@Qualifier(AnagaziendeClientService.QUALIFIER) AnagaziendeClientService _anagaziendeClientService,
                               ClienteFaiServiceImplExt _clienteFaiService, DispositivoServiceExt _dispositivoService,
                               @Qualifier("wizard_vehicle") Map<String, WizardVehicleEntity> _cacheVehicleWizard,
                               StatoDispositivoServizioServiceExt _statoDispositivoServizioService,
                               GatewayClientService _gatewayClientService) {
    anagaziendeClientService = _anagaziendeClientService;
    clienteFaiService = _clienteFaiService;
    dispositivoService = _dispositivoService;
    cacheVehicleWizard = _cacheVehicleWizard;
    statoDispositivoServizioService = _statoDispositivoServizioService;
    gatewayClientService = _gatewayClientService;
  }

  public DeliveryDTO getDeliveryDtoByRequestsCustomer(final String _customerCode, List<Richiesta> richieste,
                                                      DeliveryPrintRequestDTO deliveryPrintRequestDTO) {
    _log.info("Find devices by customer code : {} with requests : {}", _customerCode, richieste);
    DeliveryDTO deliveryDTO = new DeliveryDTO();

    ClienteFai clienteFai = clienteFaiService.findByCodiceCliente(_customerCode);
    setCompanyAndAccountByIndirizzi(deliveryDTO, clienteFai);
    Optional<ClienteFai> optConsorzio = getConsorzio(clienteFai);
    if (optConsorzio.isPresent()) {
      ClienteFai consorzio = optConsorzio.get();
      _log.info("Manage Consorzio: {}", consorzio);
      setConsorzioCodeOnDeliveryDTO(consorzio, deliveryDTO);
      manageConsorzioOnDeliveryPrint(consorzio, deliveryPrintRequestDTO);
    }

    final List<TipiDispositivo> deviceTypesDTO = deliveryDTO.getTipiDispositivo();
    richieste.stream()
             .forEach(req -> {
               final Optional<Dispositivo> deviceOpt = req.getDispositivos()
                                                          .stream()
                                                          .findFirst();
               if (deviceOpt.isPresent()) {
                 Dispositivo device = deviceOpt.get();
                 addDeviceOnDtoDeviceTypes(device, deviceTypesDTO);
               }
             });

    deliveryDTO.setTipiDispositivo(deviceTypesDTO);
    _log.info("Generate DeliveryDTO for customer {}: {}", _customerCode, deliveryDTO);
    addDtoToDeliveryPrintRequestDTO(deliveryDTO, deliveryPrintRequestDTO);
    return deliveryDTO;
  }

  private void setConsorzioCodeOnDeliveryDTO(ClienteFai consorzio, DeliveryDTO deliveryDTO) {
    String codeConsorzio = consorzio.getCodiceCliente();

    DeliveryCompanyDTO company = deliveryDTO.getCompany();
    if (company != null) {
      _log.debug("ParentCompanyCode to Company: {} is '{}'", company.getCompanyCode(), codeConsorzio);
      company.setParentCompanyCode(codeConsorzio);
    }

    // Modifica richiesta da Jonathan per avere il parentCompanyCode anche sull'accountHolder.
    DeliveryCompanyDTO accountHolder = deliveryDTO.getAccountHolder();
    if (accountHolder != null) {
      _log.debug("ParentCompanyCode to AccountHolder: {} is '{}'", accountHolder.getCompanyCode(), codeConsorzio);
      accountHolder.setParentCompanyCode(codeConsorzio);
    }
  }

  private void manageConsorzioOnDeliveryPrint(ClienteFai consorzio, DeliveryPrintRequestDTO deliveryPrintRequestDTO) {
    final String codeConsorzio = consorzio.getCodiceCliente();
    List<CompanyGroupDTO> companyGroups = deliveryPrintRequestDTO.getCompanyGroups();
    if (companyGroups != null && !companyGroups.isEmpty()) {
      Optional<CompanyGroupDTO> optCompanyGroups = companyGroups.stream()
                                                                .filter(c -> c.getDeliveryCompanyGroup() != null
                                                                             && c.getDeliveryCompanyGroup()
                                                                                 .getCompanyCode()
                                                                                 .equals(codeConsorzio))
                                                                .findFirst();
      if (optCompanyGroups.isPresent()) {
        _log.debug("{} is already present in list", codeConsorzio);
        return;
      }
    }

    if (companyGroups == null) {
      companyGroups = new ArrayList<>();
    }

    CompanyGroupDTO companyGroup = new CompanyGroupDTO();
    DeliveryCompanyDTO company = createCompany(consorzio);
    company.setCardHolderName(consorzio.getNomeIntestazioneTessere());
    companyGroup.setDeliveryCompanyGroup(company);
    companyGroups.add(companyGroup);
    deliveryPrintRequestDTO.setCompanyGroups(companyGroups);
  }

  private Optional<ClienteFai> getConsorzio(ClienteFai clienteFai) {
    Optional<ClienteFai> optConsorzio = Optional.ofNullable(clienteFai);
    if (clienteFai.isConsorzioPadre() != null && clienteFai.isConsorzioPadre()) {
      _log.debug("ClienteFai passed on method is already an Consorzio.");
      return optConsorzio;
    }

    String raggruppamentoImpresa = clienteFai.getRaggruppamentoImpresa();
    if (StringUtils.isNotBlank(raggruppamentoImpresa)) {
      List<ClienteFai> clientiFaiByRaggruppamento = clienteFaiService.findByRaggruppamentoImpresa(raggruppamentoImpresa);
      optConsorzio = clientiFaiByRaggruppamento.stream()
                                               .filter(c -> !c.getCodiceCliente()
                                                              .equals(clienteFai.getCodiceCliente()))
                                               .filter(c -> c.isConsorzioPadre())
                                               .findFirst();
    } else {
      optConsorzio = Optional.empty();
    }

    return optConsorzio;
  }

  private void addDeviceOnDtoDeviceTypes(Dispositivo device, List<TipiDispositivo> deviceTypesDTO) {
    TipiDispositivo tipiDispositivo = null;

    TipoDispositivo deviceType = device.getTipoDispositivo();
    TipoDispositivoEnum nameDeviceType = deviceType.getNome();
    _log.debug("Managed deviceType: {}", nameDeviceType);

    Optional<TipiDispositivo> optDeviceTypes = deviceTypesDTO.stream()
                                                             .filter(t -> nameDeviceType.name()
                                                                                        .equals(t.getCodice()))
                                                             .findFirst();
    if (optDeviceTypes.isPresent()) {
      tipiDispositivo = optDeviceTypes.get();
    } else {
      _log.debug("new TipiDispositivo");
      tipiDispositivo = new TipiDispositivo();
      tipiDispositivo.setCodice(nameDeviceType.name());
      tipiDispositivo.setDescrizione(deviceType.getNomeBusiness());
      deviceTypesDTO.add(tipiDispositivo);
    }

    List<ContrattiDTO> contratti = tipiDispositivo.getContratti();
    if (contratti == null) {
      contratti = new ArrayList<>();
    }
    ContrattiDTO contract = generateContrattoIfNecessaryAndDevice(contratti, device);
    if (!contratti.contains(contract))
      contratti.add(contract);
    tipiDispositivo.setContratti(contratti);
    _log.debug("Add {} on deviceTypesDTO", tipiDispositivo);
  }

  private ContrattiDTO generateContrattoIfNecessaryAndDevice(List<ContrattiDTO> contratti, Dispositivo device) {
    Contratto contract = device.getContratto();
    String codiceContratto = (contract != null) ? contract.getCodContrattoCliente() : "";
    _log.debug("Managed Contract: {}", codiceContratto);

    ContrattiDTO contrattoDTO = null;
    if (contratti == null || contratti.isEmpty()) {
      contratti = new ArrayList<>();
      contrattoDTO = generateContrattoDTO(codiceContratto, contratti);
    } else {
      Optional<ContrattiDTO> optContractDTO = contratti.stream()
                                                       .filter(c -> codiceContratto.equals(c.getCodiceContratto()))
                                                       .findFirst();

      if (optContractDTO.isPresent()) {
        contrattoDTO = optContractDTO.get();
      } else {
        contrattoDTO = generateContrattoDTO(codiceContratto, contratti);
      }
    }

    List<DispositivoDTO> dispositivi = contrattoDTO.getDispositivi();
    if (dispositivi == null) {
      dispositivi = new ArrayList<>();
    }
    DispositivoDTO deviceDTO = generateDispositivoDTO(device);
    _log.debug("DeviceDTO generated: {}", deviceDTO);
    dispositivi.add(deviceDTO);
    contrattoDTO.setDispositivi(dispositivi);
    return contrattoDTO;
  }

  private ContrattiDTO generateContrattoDTO(String codiceContratto, List<ContrattiDTO> contratti) {
    _log.debug("new ContrattoDTO");
    ContrattiDTO contrattoDTO = new ContrattiDTO();
    contrattoDTO.setCodiceContratto(codiceContratto);
    contratti.add(contrattoDTO);
    return contrattoDTO;
  }

  private DispositivoDTO generateDispositivoDTO(Dispositivo device) {
    DispositivoDTO dispositivo = new DispositivoDTO();
    dispositivo.setSeriale(device.getSeriale());

    final Optional<AssociazioneDV> assDvOpt = device.getAssociazioneDispositivoVeicolos()
                                                    .stream()
                                                    .findFirst();
    
    dispositivo.setClasse("");
    dispositivo.setTarga("");
    if (assDvOpt.isPresent()) {
      AssociazioneDV assDv = assDvOpt.get();
      WizardVehicleEntity wizardVehicleEntity = getVehicleInformation(assDv);
      if (wizardVehicleEntity == null) {
        _log.warn("Not found vehicle in cache with AssociazioneDV {}", assDv);
      } else {
        String euroClass = wizardVehicleEntity.getEuroClass();
        String licensePlate = wizardVehicleEntity.getLicensePlate();
        // String country = wizardVehicleEntity.getCountry();
        dispositivo.setClasse(euroClass);
        dispositivo.setTarga(licensePlate);
      }
    } else {
      _log.warn("No vehicle related to device : {} (id : {})", device.getSeriale(), device.getId());
    }
    
    List<ServizioDTO> serviceTypeOnDevice = generateServiceType(device);
    _log.debug("Service type on device: {}", serviceTypeOnDevice);
    dispositivo.setServizi(serviceTypeOnDevice);
    return dispositivo;
  }

  private WizardVehicleEntity getVehicleInformation(AssociazioneDV associazioneDv) {
    WizardVehicleEntity wizardVehicleEntity = null;
    String uuidVehicle = associazioneDv.getUuidVeicolo();
    if(StringUtils.isNotBlank(uuidVehicle)) {
      wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);  
    }
    _log.debug("Found in cache vehicle: {} by UUID: {}", wizardVehicleEntity, uuidVehicle);
    return wizardVehicleEntity;
  }

  private List<ServizioDTO> generateServiceType(Dispositivo device) {
    List<StatoDispositivoServizio> servicesOnDevice = statoDispositivoServizioService.findbyDispositivo(device);
    Map<String, StatoDS> mapServiceStatusDevice = servicesOnDevice.stream()
                                                                  .collect(Collectors.toMap(sds -> sds.getTipoServizio()
                                                                                                      .getNomeBusiness(),
                                                                                            sds -> sds.getStato()));
    Set<TipoServizio> allServiceByDeviceType = device.getTipoDispositivo()
                                                     .getTipoServizios();
    return allServiceByDeviceType.stream()
                                 .map(serviceType -> {
                                   String nomeBusiness = serviceType.getNomeBusiness();
                                   ServizioDTO service = new ServizioDTO();
                                   service.setNome(nomeBusiness);
                                   service.setAbilitato(false);
                                   if (mapServiceStatusDevice.containsKey(nomeBusiness)) {
                                     service.setAbilitato(true);
                                   }
                                   return service;
                                 })
                                 .collect(toList());

  }

  public DeliveryDTO getDeliveryDtoByDevicesCustomer(final String _customerCode, Map<TipoDispositivoEnum, List<Dispositivo>> mapTypeDevice,
                                                     DeliveryPrintRequestDTO deliveryPrintRequestDTO) {

    _log.info("Find devices by customer code : {} with devices map : {}", _customerCode, mapTypeDevice);
    DeliveryDTO deliveryDTO = new DeliveryDTO();

    ClienteFai clienteFai = clienteFaiService.findByCodiceCliente(_customerCode);
    setCompanyAndAccountByIndirizzi(deliveryDTO, clienteFai);
    Optional<ClienteFai> optConsorzio = getConsorzio(clienteFai);
    if (optConsorzio.isPresent()) {
      ClienteFai consorzio = optConsorzio.get();
      _log.info("Manage Consorzio: {}", consorzio);
      setConsorzioCodeOnDeliveryDTO(consorzio, deliveryDTO);
      manageConsorzioOnDeliveryPrint(consorzio, deliveryPrintRequestDTO);
    }

    Set<TipoDispositivoEnum> keySet = mapTypeDevice.keySet();

    List<TipiDispositivo> tipiDispositivo = deliveryDTO.getTipiDispositivo();
    keySet.forEach(key -> {
      List<Dispositivo> devices = mapTypeDevice.get(key);
      devices.stream()
             .forEach(device -> {
               addDeviceOnDtoDeviceTypes(device, tipiDispositivo);
             });
    });

    deliveryDTO.setTipiDispositivo(tipiDispositivo);
    _log.info("Generate deliveryDTO: {}", deliveryDTO);
    addDtoToDeliveryPrintRequestDTO(deliveryDTO, deliveryPrintRequestDTO);
    return deliveryDTO;
  }

  private void setCompanyAndAccountByIndirizzi(DeliveryDTO deliveryDTO, ClienteFai clienteFai) {

    String codiceCliente = clienteFai.getCodiceCliente();
    IndirizziDiSpedizioneDTO indirizzo = anagaziendeClientService.getIndirizzoSpedizioneClienteFai(codiceCliente);
    IndirizzoSpedizioneDTO indirizzoDiTransito = indirizzo.getTransito();
    IndirizzoSpedizioneDTO indirizzoFinale = indirizzo.getFinale();

    setCompanyIfIndirizziAreNotSet(deliveryDTO, indirizzoDiTransito, indirizzoFinale, clienteFai);
    setCompanyIfIndirizzoIsNotTransito(deliveryDTO, indirizzoDiTransito, indirizzoFinale, clienteFai);
    setCompanyAndAccountIfIndirizzoIsTransito(deliveryDTO, indirizzoDiTransito, indirizzoFinale, clienteFai);

    String nomeIntestazioneTessere = clienteFai.getNomeIntestazioneTessere();
    if (StringUtils.isBlank(nomeIntestazioneTessere)) {
      _log.warn("Nome intestatario tessere is not set for client code: {}", clienteFai);
    }
    deliveryDTO.setCardHolderName(nomeIntestazioneTessere);
  }

  private void setCompanyIfIndirizzoIsNotTransito(DeliveryDTO deliveryDTO, IndirizzoSpedizioneDTO indirizzoDiTransito,
                                                  IndirizzoSpedizioneDTO indirizzoFinale, ClienteFai clienteFai) {
    if (indirizzoDiTransito == null && indirizzoFinale != null) {
      DeliveryCompanyDTO company = new DeliveryCompanyDTO();
      setFieldsByIndirizzoAndClienteFai(company, indirizzoFinale, clienteFai);
      deliveryDTO.setCompany(company);
    }
  }

  private void setCompanyAndAccountIfIndirizzoIsTransito(DeliveryDTO deliveryDTO, IndirizzoSpedizioneDTO indirizzoDiTransito,
                                                         IndirizzoSpedizioneDTO indirizzoFinale, ClienteFai clienteFai) {
    if (indirizzoDiTransito != null && indirizzoFinale != null) {
      DeliveryCompanyDTO company = new DeliveryCompanyDTO();
      setFieldsByIndirizzoAndClienteFai(company, indirizzoDiTransito, clienteFai);
      deliveryDTO.setCompany(company);

      DeliveryCompanyDTO account = new DeliveryCompanyDTO();
      setFieldsByIndirizzoAndClienteFai(account, indirizzoFinale, clienteFai);
      deliveryDTO.setAccountHolder(account);
    }
  }

  private void setCompanyIfIndirizziAreNotSet(DeliveryDTO deliveryDTO, IndirizzoSpedizioneDTO indirizzoDiTransito,
                                              IndirizzoSpedizioneDTO indirizzoFinale, ClienteFai clienteFai) {
    if (indirizzoDiTransito == null && indirizzoFinale == null) {
      _log.warn("FAILED Feign AnagAzienda, so used repository values!");
      DeliveryCompanyDTO company = createCompany(clienteFai);
      deliveryDTO.setCompany(company);
    }
  }

  private DeliveryCompanyDTO createCompany(ClienteFai clienteFai) {
    DeliveryCompanyDTO company = new DeliveryCompanyDTO();
    company.setAddress(clienteFai.getVia());
    company.setCity(clienteFai.getCitta());
    company.setCompanyCode(clienteFai.getCodiceCliente());
    company.setCompanyName(clienteFai.getRagioneSociale());
    company.setCountry(clienteFai.getPaese());
    company.setPostalCode(clienteFai.getCap());
    company.setProvince(clienteFai.getProvincia());

    AziendaDTO aziendaDTO = gatewayClientService.getAziendaByCodice(clienteFai.getCodiceCliente());
    if (aziendaDTO != null && StringUtils.isNotBlank(aziendaDTO.getPaeseNascitaLegaleRappresentante())) {
      company.setLang(aziendaDTO.getPaeseNascitaLegaleRappresentante()
                                .toLowerCase());
    }
    return company;
  }

  private void setFieldsByIndirizzoAndClienteFai(DeliveryCompanyDTO deliveryCompany, IndirizzoSpedizioneDTO indirizzoFinale,
                                                 ClienteFai clienteFai) {
    deliveryCompany.setAddress(indirizzoFinale.getVia());
    deliveryCompany.setCity(indirizzoFinale.getCitta());
    deliveryCompany.setCompanyCode(clienteFai.getCodiceCliente());
    deliveryCompany.setCompanyName(clienteFai.getRagioneSociale());
    deliveryCompany.setCountry(indirizzoFinale.getPaese());
    deliveryCompany.setPostalCode(indirizzoFinale.getCap());
    deliveryCompany.setProvince(indirizzoFinale.getProvincia());

    AziendaDTO aziendaDTO = gatewayClientService.getAziendaByCodice(clienteFai.getCodiceCliente());
    if (aziendaDTO != null && StringUtils.isNotBlank(aziendaDTO.getPaeseNascitaLegaleRappresentante())) {
      deliveryCompany.setLang(aziendaDTO.getPaeseNascitaLegaleRappresentante()
                                        .toLowerCase());
    }
  }

  public void getDeviceDetail(final String _customerCode) {
    _log.debug("Retrieve device detail for customer : {}", _customerCode);
    List<Dispositivo> devices = dispositivoService.getDevicesByCodiceAzienda(_customerCode);
    devices.stream()
           .forEach(device -> {
             final Optional<AssociazioneDV> associazioneDV = device.getAssociazioneDispositivoVeicolos()
                                                                   .stream()
                                                                   .findFirst();
             if (associazioneDV.isPresent()) {
               String uuidVehicle = associazioneDV.get()
                                                  .getUuidVeicolo();
               WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);
               if (wizardVehicleEntity == null) {
                 _log.warn("Not found vehicle in cache with UUID : {} to device : {} (id : {})", uuidVehicle, device.getSeriale(),
                           device.getId());
               } else {
                 _log.warn("wizardVehicleEntity : {}", wizardVehicleEntity);
               }
             } else {
               _log.warn("No vehicle related to device : {} (id : {})", device.getSeriale(), device.getId());
             }
           });
  }

  private void addDtoToDeliveryPrintRequestDTO(DeliveryDTO deliveryDTO, DeliveryPrintRequestDTO deliveryPrintRequestDTO) {
    List<DeliveryDTO> deliveryList = deliveryPrintRequestDTO.getDeliveryList();
    if (deliveryList == null) {
      deliveryList = new ArrayList<>();
    }
    deliveryList.add(deliveryDTO);
    _log.debug("Add deliveryDTO: {} to {}", deliveryDTO, DeliveryPrintRequestDTO.class.getSimpleName());
    deliveryPrintRequestDTO.setDeliveryList(deliveryList);
  }

  public void getDeliveryDtoByProduttore(List<Dispositivo> devicesToDelivery,
                                         DeliveryPrintProduttoreRequestDTO deliveryPrintProduttoreRequestDTO) {
    if (_log.isDebugEnabled())
      _log.debug("Get Delivery DTO to Produttore by Devices: {}", mapIndentificativoRichieste(devicesToDelivery));

    if (devicesToDelivery == null || devicesToDelivery.isEmpty()) {
      String messageError = "Not found devices to delivery at the Produttore...";
      _log.error(messageError);
      throw new IllegalArgumentException(messageError);
    }

    Map<Produttore, List<Dispositivo>> mapDeviceByType = devicesToDelivery.stream()
                                                                          .sorted((d1, d2) -> d1.getTipoDispositivo()
                                                                                                .getNome()
                                                                                                .compareTo(d2.getTipoDispositivo()
                                                                                                             .getNome()))
                                                                          .sorted((d1, d2) -> d1.getSeriale()
                                                                                                .compareTo(d2.getSeriale()))
                                                                          .collect(groupingBy(d -> d.getTipoDispositivo()
                                                                                                    .getProduttore()));
    final List<DeliveryProduttoreDTO> deliveriesProduttore = new ArrayList<>();
    mapDeviceByType.entrySet()
                   .forEach(es -> {
                     Produttore produttore = es.getKey();
                     _log.debug("Produttore: {}", produttore);
                     DeliveryProduttoreDTO deliveryProduttore = new DeliveryProduttoreDTO();

                     InfoDeliveryProduttore infoDelivery = new InfoDeliveryProduttore();
                     String codiceFornitoreNav = produttore.getCodiceFornitoreNav();
                     infoDelivery.setProduttoreCodeNav(codiceFornitoreNav);

                     // TODO add call to nav to retrieve address information...
                     // TODO Alternative: Scheduler to save produttore information on table.

                     String nomeProduttore = produttore.getNome();
                     infoDelivery.setProduttoreName(nomeProduttore);

                     List<Dispositivo> devices = es.getValue();
                     List<DeviceInfoDelivery> devicesInfo = devices.stream()
                                                                   .map(d -> mapToDeliveryDevice(d))
                                                                   .collect(toList());

                     deliveryProduttore.setProduttore(infoDelivery);
                     deliveryProduttore.setDevice(devicesInfo);
                     _log.debug("DeliveryProduttore: {}", deliveriesProduttore);
                     deliveriesProduttore.add(deliveryProduttore);
                   });

    deliveryPrintProduttoreRequestDTO.setDeliveryProduttore(deliveriesProduttore);
  }

  private List<String> mapIndentificativoRichieste(List<Dispositivo> devicesToDelivery) {
    if (devicesToDelivery == null || devicesToDelivery.isEmpty()) {
      return null;
    }

    return devicesToDelivery.parallelStream()
                            .map(r -> r.getIdentificativo())
                            .collect(toList());
  }

  private DeviceInfoDelivery mapToDeliveryDevice(Dispositivo d) {
    DeviceInfoDelivery deviceInfo = new DeviceInfoDelivery();
    String seriale = d.getSeriale();
    deviceInfo.setSeriale(seriale);

    Contratto contratto = d.getContratto();
    if (contratto == null) {
      String messageError = String.format("Not found contract related on device: %s", d);
      _log.error(messageError);
      throw new IllegalArgumentException(messageError);
    }

    ClienteFai clienteFai = contratto.getClienteFai();
    if (clienteFai == null) {
      String messageError = String.format("Not found clienteFai related on contract: %s - %s", contratto.getId(),
                                          contratto.getCodContrattoCliente());
      _log.error(messageError);
      throw new IllegalArgumentException(messageError);
    }

    String ragioneSociale = clienteFai.getRagioneSociale();
    deviceInfo.setRagioneSocialeCliente(ragioneSociale);
    deviceInfo.setTipoDispositivo(d.getTipoDispositivo()
                                   .getNomeBusiness());

    Optional<AssociazioneDV> assDvOpt = d.getAssociazioneDispositivoVeicolos()
                                         .stream()
                                         .findFirst();
    if (assDvOpt.isPresent()) {
      WizardVehicleEntity vehicleInformation = getVehicleInformation(assDvOpt.get());
      String licensePlate = vehicleInformation.getLicensePlate();
      deviceInfo.setTarga(licensePlate);
      String country = vehicleInformation.getCountry();
      deviceInfo.setNazione(country);
    } else {
      String messageError = String.format("Not found vehicle associated on Device: %s", d);
      _log.error(messageError);
      throw new IllegalArgumentException(messageError);
    }

    return deviceInfo;
  }

}
