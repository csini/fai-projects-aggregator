package it.fai.ms.efservice.service.fsm.action;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionChangeStatusDispositivo implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  private final DispositivoEvent event;

  public FsmActionChangeStatusDispositivo(final FsmSenderToQueue _senderFsmService, final DispositivoEvent _event) {
    senderFsmService = _senderFsmService;
    event = _event;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      if (log.isDebugEnabled()) {
        log.debug("Change Stato Richiesta [ID: " + ((richiesta != null) ? richiesta.getId() : null) + "] per spedizioni Dispositivi...");
      }

      Set<Dispositivo> dispositivi = richiesta.getDispositivos();
      sendChangeStatus(dispositivi);
    }
  }

  private void sendChangeStatus(Set<Dispositivo> dispositivi) {
    Set<Dispositivo> dispositiviChangesStatus = dispositivi.stream()
                                                           .map(d -> {
                                                             senderFsmService.sendMessageForChangeStatoDispositivo(d, event);
                                                             return d;
                                                           })
                                                           .collect(toSet());
    log.info("Change status with command {} for dispositivi: {}", event, dispositiviChangesStatus);
  }

}
