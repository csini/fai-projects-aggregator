package it.fai.ms.efservice.domain.enumeration;

/**
 * The TipoOrdineCliente enumeration.
 */
public enum TipoOrdineCliente {
    NUOVO_ORDINE, VARIAZIONE
}
