package it.fai.ms.efservice.service.impl;

import java.util.Optional;

public interface ObunoNotificationVehicleService {

  Optional<String> findCompanyCodeByContractUuid(String contractUuid);

}
