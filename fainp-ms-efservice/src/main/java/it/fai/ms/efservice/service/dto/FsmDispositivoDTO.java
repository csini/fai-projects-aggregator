package it.fai.ms.efservice.service.dto;

public class FsmDispositivoDTO extends FsmDTO {

  private static final long serialVersionUID = -9015571367738606292L;

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("FsmDispositivoDTO ");
    sb.append("[");

    sb.append("operazione: ");
    sb.append(getOperazione());
    sb.append(", identificativi: ");
    sb.append(getStringIdentificativi());
    sb.append(", nota: ");
    sb.append(getNota());

    sb.append("]");
    return sb.toString();
  }

}
