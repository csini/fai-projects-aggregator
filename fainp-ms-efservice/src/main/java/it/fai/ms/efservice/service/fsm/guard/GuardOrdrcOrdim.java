/**
 * 
 */
package it.fai.ms.efservice.service.fsm.guard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

/**
 * @author Luca Vassallo
 */
public class GuardOrdrcOrdim implements Guard<StatoRichiesta, RichiestaEvent> {
  
  private Logger logger = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public GuardOrdrcOrdim(FsmSenderToQueue senderFsmService) {
    this.senderFsmService = senderFsmService;
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.guard.Guard#evaluate(org.springframework.statemachine.StateContext)
   */
  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    logger.debug("Evaluate....");
    Richiesta richiesta = null;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        richiesta = (Richiesta) object;
        senderFsmService.sendMessageTo(richiesta);
      }
    }

    
    return true;
  }

}
