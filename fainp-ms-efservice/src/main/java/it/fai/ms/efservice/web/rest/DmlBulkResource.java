package it.fai.ms.efservice.web.rest;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepository;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.OrdineClienteRepository;
import it.fai.ms.efservice.repository.OrdineFornitoreRepository;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.jms.dml.AssociazioneDvDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.ClienteFaiDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.ContrattoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.ContrattoMessageSenderService;
import it.fai.ms.efservice.service.jms.dml.DispositivoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.OrdineClienteDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.OrdineFornitoreDepositoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.dml.RichiestaDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.ContrattoDmlSenderMapper;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;
import it.fai.ms.efservice.service.jms.mapper.OrdineFornitoreDepositoDmlSenderMapper;

/**
 * REST controller for managing bulk dml message.
 */
@RestController
@RequestMapping(DmlBulkResource.BASE_PATH)
@Transactional
public class DmlBulkResource {

  private final Logger log = LoggerFactory.getLogger(DmlBulkResource.class);

  public static final String BASE_PATH = "/api/dmlbulk";

  public static final String DISPOSITIVI     = "/dispositivi";
  public static final String DISPOSITIVI_UUID     = "/dispositivibyuuid";
  public static final String ASSOCIAZIONE_DV = "/associazionedv";
  public static final String ORDINI          = "/ordini";
  public static final String RICHIESTE       = "/richieste";
  public static final String CONTRATTI       = "/contratti";
  public static final String AZIENDE         = "/aziende";
  public static final String BULK_AZIENDA    = "/bulk-aziende";

  public static final String API_DISPOSITIVI     = BASE_PATH + DISPOSITIVI;
  public static final String API_ASSOCIAZIONE_DV = BASE_PATH + ASSOCIAZIONE_DV;
  public static final String API_ORDINI          = BASE_PATH + ORDINI;
  public static final String API_RICHIESTE       = BASE_PATH + RICHIESTE;
  public static final String API_CONTRATTI       = BASE_PATH + CONTRATTI;
  public static final String API_AZIENDE         = BASE_PATH + AZIENDE;

  private final JmsProperties jmsProperties;

  private final DispositivoRepositoryExt               dispositivoRepository;
  private final DispositivoRepository                  dispositivoRepositoryOrig;
  private final OrdineClienteRepository                ordineRepository;
  private final RichiestaRepository                    richiestaRepository;
  private final ContrattoRepository                    contrattoRepository;
  private final ClienteFaiRepository                   clienteFaiRepository;
  private final OrdineFornitoreRepository              ordineFornitoreRepository;
  private final OrdineFornitoreDepositoDmlSenderMapper ordineFornitoreDepositoDmlSenderMapper;

  private final DispositivoJmsMapper dispositivoJmsMapper;

  private final ContrattoMessageSenderService contrattoMessageSenderService;

  ContrattoDmlSenderMapper contrattoJmsMapper;

  public DmlBulkResource(JmsProperties jmsProperties, DispositivoRepositoryExt dispositivoRepository,
                         OrdineClienteRepository ordineRepository, RichiestaRepository richiestaRepository,
                         ContrattoRepository contrattoRepository, ClienteFaiRepository clienteFaiRepository,
                         ContrattoMessageSenderService contrattoMessageSenderService, OrdineFornitoreRepository ordineFornitoreRepository,
                         OrdineFornitoreDepositoDmlSenderMapper ordineFornitoreDepositoDmlSenderMapper,
                         DispositivoJmsMapper dispositivoJmsMapper, ContrattoDmlSenderMapper contrattoJmsMapper,
                         DispositivoRepository dispositivoRepositoryOrig) {

    this.jmsProperties = jmsProperties;
    this.dispositivoRepository = dispositivoRepository;
    this.ordineRepository = ordineRepository;
    this.richiestaRepository = richiestaRepository;
    this.contrattoRepository = contrattoRepository;
    this.contrattoMessageSenderService = contrattoMessageSenderService;
    this.clienteFaiRepository = clienteFaiRepository;
    this.ordineFornitoreRepository = ordineFornitoreRepository;
    this.ordineFornitoreDepositoDmlSenderMapper = ordineFornitoreDepositoDmlSenderMapper;
    this.dispositivoJmsMapper = dispositivoJmsMapper;
    this.contrattoJmsMapper = contrattoJmsMapper;
    this.dispositivoRepositoryOrig = dispositivoRepositoryOrig;
  }

  @PostMapping(DISPOSITIVI)
  @Timed
  @Async
  public void sendAllDispositiv(@RequestParam(required = true, name = "pageStart", defaultValue = "0") int pageStart,
                                @RequestParam(required = true, name = "pageTo", defaultValue = "0") int pageTo,
                                @RequestParam(required = true, name = "pageSize", defaultValue = "1000") int pageSize) {

    log.info("REST request to send bulk dml message for Dispositivi: pageStart {}, pageTo={}, pageSize={}", pageStart, pageTo, pageSize);

    try (DispositivoDmlSenderUtil sender = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)) {
      sender.openConnectionForSave();

      Pageable pageable = new PageRequest(pageStart, pageSize);
      while (pageable != null) {

        // se pageTo è zero o è maggiore della paggina attuale -> exit
        if (pageTo > 0 && pageable.getPageNumber() >= pageTo) {
          log.info("Dispositivo DML Bulk finish for pageTo limit: currentPage {} pageStart {}, pageTo={}, pageSize={}",
                   pageable.getPageNumber(), pageStart, pageTo, pageSize);
          return;
        }

        log.info("Elaborazione pagina {} : pageStart {}, pageTo={}, pageSize={}", pageable.getPageNumber(), pageStart, pageTo, pageSize);
        Page<Dispositivo> page = dispositivoRepository.findAll(pageable);
        log.info("Found {} dispositivi", pageable.getPageSize());

        for (Dispositivo dispositivo : page) {
          sender.sendSaveNotificationWithOpenedConnection(dispositivo);
        }

        pageable = page.hasNext() ? page.nextPageable() : null;
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
    log.info("Dispositivo DML Bulk finish: pageStart {}, pageTo={}, pageSize={}", pageStart, pageTo, pageSize);
  }

  @PostMapping(DISPOSITIVI + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllDispositiviDeprecated() {
    log.info("REST request to send bulk dml message for Dispositivi");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<Dispositivo> page = dispositivoRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (Dispositivo dispositivo : page) {
        try(DispositivoDmlSenderUtil ocdSenderUtil = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)){
          ocdSenderUtil.sendSaveNotification(dispositivo);
        } catch (Exception e) {
          log.error(e.getMessage());
        }
        //new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper).sendSaveNotification(dispositivo);
      }
    }
  }

  @PostMapping(DISPOSITIVI + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllDispositiviByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for Dispositivi fo azienda {}", codiceAzienda);

    try (DispositivoDmlSenderUtil sender = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)) {
      sender.openConnectionForSave();
      List<Dispositivo> page = dispositivoRepository.findAllByCodiceAzienda(codiceAzienda);
      for (Dispositivo dispositivo : page) {
        sender.sendSaveNotificationWithOpenedConnection(dispositivo);
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(DISPOSITIVI_UUID + "/{identificativo}")
  @Timed
  @Async
  public void sendDispositivoByIdentificativo(@PathVariable(required = true, name = "identificativo") String identificativoDispositivo) {
    log.info("REST request to send bulk dml message for Dispositivo by Identificativo: {}", identificativoDispositivo);

    try (DispositivoDmlSenderUtil sender = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)) {
      sender.openConnectionForSave();
      Dispositivo dispositivo = dispositivoRepository.findOneByIdentificativo(identificativoDispositivo);
      sender.sendSaveNotificationWithOpenedConnection(dispositivo);
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(BASE_PATH + "/dispositiviByTipo/{tipoDispositivo},{stato}")
  @Timed
  @Async
  public void sendAllDispositiviByTipo(@PathVariable(required = true, name = "tipoDispositivo") TipoDispositivoEnum tipoDispositivo,
                                       StatoDispositivo stato) {
    log.info("REST request to send bulk dml message for Dispositivi for tipoDispositivo {}", tipoDispositivo);

    try (DispositivoDmlSenderUtil sender = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)) {
      sender.openConnectionForSave();
      Pageable pageable = new PageRequest(0, 1000, new Sort("dataPrimaAttivazione"));
      Page<Dispositivo> page = dispositivoRepository.findAllByTipoDispositivo_nomeAndStato(Arrays.asList(tipoDispositivo), stato, pageable);

      for (Dispositivo dispositivo : page) {
        sender.sendSaveNotificationWithOpenedConnection(dispositivo);
      }
      while (page.hasNext()) {
        Page<Dispositivo> page1 = dispositivoRepository.findAllByTipoDispositivo_nomeAndStato(Arrays.asList(tipoDispositivo), stato,
                                                                                              page.nextPageable());
        for (Dispositivo dispositivo : page1) {
          sender.sendSaveNotificationWithOpenedConnection(dispositivo);
        }
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(DISPOSITIVI + "_deprecated/{codiceAzienda}")
  @Timed
  @Async
  @Deprecated
  public void sendAllDispositiviByAziendaDeprecated(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for Dispositivi fo azienda {}", codiceAzienda);

    List<Dispositivo> page = dispositivoRepository.findAllByCodiceAzienda(codiceAzienda);
    for (Dispositivo dispositivo : page) {
      try(DispositivoDmlSenderUtil ocdSenderUtil = new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper)){
        ocdSenderUtil.sendSaveNotification(dispositivo);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      //new DispositivoDmlSenderUtil(jmsProperties, dispositivoJmsMapper).sendSaveNotification(dispositivo);
    }
  }

  @PostMapping(ASSOCIAZIONE_DV)
  @Timed
  @Async
  public void sendAllAssociazioniDV(@RequestParam(required = true, name = "pageStart", defaultValue = "0") int pageStart,
                                    @RequestParam(required = true, name = "pageTo", defaultValue = "0") int pageTo,
                                    @RequestParam(required = true, name = "pageSize", defaultValue = "1000") int pageSize) {

    log.info("REST request to send bulk dml message for Associazione DV: pageStart {}, pageTo={}, pageSize={}", pageStart, pageTo,
             pageSize);

    try (AssociazioneDvDmlSenderUtil sender = new AssociazioneDvDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();

      Pageable pageable = new PageRequest(pageStart, pageSize);
      while (pageable != null) {

        // se pageTo è zero o è maggiore della paggina attuale -> exit
        if (pageTo > 0 && pageable.getPageNumber() >= pageTo) {
          log.info("Associazione DML Bulk finish for pageTo limit: currentPage {} pageStart {}, pageTo={}, pageSize={}",
                   pageable.getPageNumber(), pageStart, pageTo, pageSize);
          return;
        }

        log.info("Elaborazione pagina {} : pageStart {}, pageTo={}, pageSize={}", pageable.getPageNumber(), pageStart, pageTo, pageSize);
        Page<Dispositivo> page = dispositivoRepository.findAll(pageable);
        log.info("Found {} dispositivi", pageable.getPageSize());

        for (Dispositivo dispositivo : page) {
          dispositivo.getAssociazioneDispositivoVeicolos()
                     .forEach(sds -> {
                       sender.sendSaveNotificationWithOpenedConnection(sds);
                     });
        }

        pageable = page.hasNext() ? page.nextPageable() : null;

      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(ASSOCIAZIONE_DV + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllAssociazioniDVDeprecated() {
    log.info("REST request to send bulk dml message for Associazione DV");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<Dispositivo> page = dispositivoRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (Dispositivo dispositivo : page) {
        dispositivo.getAssociazioneDispositivoVeicolos()
                   .forEach(sds -> {
                     new AssociazioneDvDmlSenderUtil(jmsProperties).sendSaveNotification(sds);
                   });
      }
    }
  }

  @PostMapping(ASSOCIAZIONE_DV + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllAssociazioniDvByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for Associazione DV fo azienda {}", codiceAzienda);

    try (AssociazioneDvDmlSenderUtil sender = new AssociazioneDvDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();

      List<Dispositivo> page = dispositivoRepository.findAllByCodiceAzienda(codiceAzienda);
      for (Dispositivo dispositivo : page) {
        dispositivo.getAssociazioneDispositivoVeicolos()
                   .forEach(sds -> {
                     sender.sendSaveNotificationWithOpenedConnection(sds);
                   });
      }

    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(ASSOCIAZIONE_DV + "_deprecated/{codiceAzienda}")
  @Timed
  @Async
  @Deprecated
  public void sendAllAssociazioniDvByAziendaDeprecated(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for Associazione DV fo azienda {}", codiceAzienda);

    List<Dispositivo> page = dispositivoRepository.findAllByCodiceAzienda(codiceAzienda);
    for (Dispositivo dispositivo : page) {
      dispositivo.getAssociazioneDispositivoVeicolos()
                 .forEach(sds -> {
                   new AssociazioneDvDmlSenderUtil(jmsProperties).sendSaveNotification(sds);
                 });
    }

  }

  @PostMapping(ORDINI)
  @Timed
  @Async
  public void sendAllOrdini() {
    log.info("REST request to send bulk dml message for ORDINI");

    try (OrdineClienteDmlSenderUtil sender = new OrdineClienteDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();

      Pageable pageable = new PageRequest(0, 100);
      while (pageable != null) {
        Page<OrdineCliente> page = ordineRepository.findAll(pageable);
        pageable = page.hasNext() ? page.nextPageable() : null;
        for (OrdineCliente m : page) {
          sender.sendSaveNotificationWithOpenedConnection(m);
        }
      }

    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei dispositivi", e);
    }
  }

  @PostMapping(ORDINI + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllOrdiniDeprecated() {
    log.info("REST request to send bulk dml message for ORDINI");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<OrdineCliente> page = ordineRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (OrdineCliente m : page) {
        try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
          ocdSenderUtil.sendSaveNotification(m);
        } catch (Exception e) {
          log.error(e.getMessage());
        }
        //new OrdineClienteDmlSenderUtil(jmsProperties).sendSaveNotification(m);
      }
    }
  }

  @PostMapping(ORDINI + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllOrdiniByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for ORDINI by azienda {}", codiceAzienda);

    try (OrdineClienteDmlSenderUtil sender = new OrdineClienteDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();
      List<OrdineCliente> page = ordineRepository.findAllByCodiceAzienda(codiceAzienda);
      for (OrdineCliente m : page) {
        sender.sendSaveNotificationWithOpenedConnection(m);
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei ordini cliente", e);
    }
  }

  @PostMapping(ORDINI + "_deprecated/{codiceAzienda}")
  @Timed
  @Async
  @Deprecated
  public void sendAllOrdiniByAziendaDeprecated(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for ORDINI by azienda {}", codiceAzienda);

    List<OrdineCliente> page = ordineRepository.findAllByCodiceAzienda(codiceAzienda);
    for (OrdineCliente m : page) {
      try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(m);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      //new OrdineClienteDmlSenderUtil(jmsProperties).sendSaveNotification(m);
    }
  }

  @PostMapping(RICHIESTE)
  @Timed
  @Async
  public void sendAllRichieste() {
    log.info("REST request to send bulk dml message for RICHIESTE");
    try (RichiestaDmlSenderUtil sender = new RichiestaDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();
      Pageable pageable = new PageRequest(0, 100);
      while (pageable != null) {
        Page<Richiesta> page = richiestaRepository.findAll(pageable);
        pageable = page.hasNext() ? page.nextPageable() : null;
        for (Richiesta m : page) {
          sender.sendSaveNotificationWithOpenedConnection(m);
        }
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei ordini cliente", e);
    }
  }

  @PostMapping(RICHIESTE + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllRichiesteDeprecated() {
    log.info("REST request to send bulk dml message for RICHIESTE");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<Richiesta> page = richiestaRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (Richiesta m : page) {
        try(RichiestaDmlSenderUtil ocdSenderUtil = new RichiestaDmlSenderUtil(jmsProperties)){
          ocdSenderUtil.sendSaveNotification(m);
        } catch (Exception e) {
          log.error(e.getMessage());
        }
        //new RichiestaDmlSenderUtil(jmsProperties).sendSaveNotification(m);
      }
    }
  }

  @PostMapping(RICHIESTE + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllRichiesteByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for RICHIESTE fo azienda {}", codiceAzienda);

    try (RichiestaDmlSenderUtil sender = new RichiestaDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();
      List<Richiesta> page = richiestaRepository.findAllByCodiceAzienda(codiceAzienda);
      for (Richiesta r : page) {
        sender.sendSaveNotificationWithOpenedConnection(r);
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei ordini cliente", e);
    }
  }

  @PostMapping(RICHIESTE + "_deprecated/{codiceAzienda}")
  @Timed
  @Async
  @Deprecated
  public void sendAllRichiesteByAziendaDeprecated(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for RICHIESTE fo azienda {}", codiceAzienda);

    List<Richiesta> page = richiestaRepository.findAllByCodiceAzienda(codiceAzienda);
    for (Richiesta r : page) {
      try(RichiestaDmlSenderUtil ocdSenderUtil = new RichiestaDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(r);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      //new RichiestaDmlSenderUtil(jmsProperties).sendSaveNotification(r);
    }
  }

  @PostMapping(CONTRATTI)
  @Timed
  @Async
  public void sendAllContratti() {
    log.info("REST request to send bulk dml message for CONTRATTI");

    try (ContrattoDmlSenderUtil sender = new ContrattoDmlSenderUtil(jmsProperties, contrattoJmsMapper)) {
      sender.openConnectionForSave();
      Pageable pageable = new PageRequest(0, 100);
      while (pageable != null) {
        Page<Contratto> page = contrattoRepository.findAll(pageable);
        pageable = page.hasNext() ? page.nextPageable() : null;
        for (Contratto m : page) {
          sender.sendSaveNotificationWithOpenedConnection(m);
        }
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei contratti", e);
    }
  }

  @PostMapping(CONTRATTI + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllContrattiDeprecated() {
    log.info("REST request to send bulk dml message for CONTRATTI");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<Contratto> page = contrattoRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (Contratto m : page) {
        contrattoMessageSenderService.sendSaveNotification(m);
      }
    }
  }

  @PostMapping(CONTRATTI + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllContrattiByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for CONTRATTI by Azienda {}", codiceAzienda);

    try (ContrattoDmlSenderUtil sender = new ContrattoDmlSenderUtil(jmsProperties, contrattoJmsMapper)) {
      sender.openConnectionForSave();
      List<Contratto> page = contrattoRepository.findAllByCodiceAzienda(codiceAzienda);
      for (Contratto m : page) {
        sender.sendSaveNotificationWithOpenedConnection(m);
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei contratti", e);
    }
  }

  @PostMapping(CONTRATTI + "_deprecated/{codiceAzienda}")
  @Timed
  @Async
  @Deprecated
  public void sendAllContrattiByAziendaDeprecated(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for CONTRATTI by Azienda {}", codiceAzienda);

    List<Contratto> page = contrattoRepository.findAllByCodiceAzienda(codiceAzienda);
    for (Contratto m : page) {
      contrattoMessageSenderService.sendSaveNotification(m);
    }
  }

  @PostMapping(AZIENDE)
  @Timed
  @Async
  public void sendAllAziende() {
    log.info("REST request to send bulk dml message for Aziende");

    try (ClienteFaiDmlSenderUtil sender = new ClienteFaiDmlSenderUtil(jmsProperties)) {
      sender.openConnectionForSave();
      Pageable pageable = new PageRequest(0, 100);
      while (pageable != null) {
        Page<ClienteFai> page = clienteFaiRepository.findAll(pageable);
        pageable = page.hasNext() ? page.nextPageable() : null;
        for (ClienteFai m : page) {
          sender.sendSaveNotificationWithOpenedConnection(m);
        }
      }
    } catch (Exception e) {
      log.error("Errore nel invio del bulk dei contratti", e);
    }
  }

  @PostMapping(AZIENDE + "_deprecated")
  @Timed
  @Async
  @Deprecated
  public void sendAllAziendeDeprecated() {
    log.info("REST request to send bulk dml message for Aziende");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<ClienteFai> page = clienteFaiRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (ClienteFai m : page) {
        try(ClienteFaiDmlSenderUtil ocdSenderUtil = new ClienteFaiDmlSenderUtil(jmsProperties)){
          ocdSenderUtil.sendSaveNotification(m);
        } catch (Exception e) {
          log.error(e.getMessage());
        }
        //new ClienteFaiDmlSenderUtil(jmsProperties).sendSaveNotification(m);
      }
    }
  }

  @PostMapping(AZIENDE + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send bulk dml message for Azienda {}", codiceAzienda);

    ClienteFai m = clienteFaiRepository.findOneByCodiceCliente(codiceAzienda);
    if (m != null)
      try(ClienteFaiDmlSenderUtil ocdSenderUtil = new ClienteFaiDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(m);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      //new ClienteFaiDmlSenderUtil(jmsProperties).sendSaveNotification(m);
    else {
      log.error("no client found with code {}", codiceAzienda);
      throw new IllegalArgumentException("no client found with code " + codiceAzienda);
    }
  }

  @PostMapping(BULK_AZIENDA + "/{codiceAzienda}")
  @Timed
  @Async
  public void sendAllBulksByAzienda(@PathVariable(required = true, name = "codiceAzienda") String codiceAzienda) {
    log.info("REST request to send all bulks dml message for Azienda {}", codiceAzienda);

    this.sendAzienda(codiceAzienda);
    this.sendAllContrattiByAzienda(codiceAzienda);
    this.sendAllDispositiviByAzienda(codiceAzienda);
    this.sendAllAssociazioniDvByAzienda(codiceAzienda);
    this.sendAllOrdiniByAzienda(codiceAzienda);
    this.sendAllRichiesteByAzienda(codiceAzienda);
  }

  @PostMapping("ordiniFornitoreDeposito")
  @Timed
  @Async
  public void sendAllOrdiniFornitoreDeposito() {
    log.info("REST request to send bulk dml message for ordiniFornitoreDeposito");

    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<OrdineFornitore> page = ordineFornitoreRepository.findAll(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      for (OrdineFornitore m : page) {
        try(OrdineFornitoreDepositoDmlSenderUtil ocdSenderUtil = new OrdineFornitoreDepositoDmlSenderUtil(jmsProperties, ordineFornitoreDepositoDmlSenderMapper)){
          ocdSenderUtil.sendSaveNotification(m);
        } catch (Exception e) {
          log.error(e.getMessage());
        }
        //new OrdineFornitoreDepositoDmlSenderUtil(jmsProperties, ordineFornitoreDepositoDmlSenderMapper).sendSaveNotification(m);
      }
    }
  }

}
