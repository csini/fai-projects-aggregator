package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

public class RuleEngineDeviceType implements Serializable {

  private static final long serialVersionUID = -581142077328886758L;

  private RuleEngineDeviceTypeId id;

  public RuleEngineDeviceType(final RuleEngineDeviceTypeId _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineDeviceType Id is mandatory"));
  }

  public static RuleEngineDeviceType as(final String _id) {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId(_id));
  }

  public RuleEngineDeviceTypeId getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((RuleEngineDeviceType) _obj).getId(), id);
    }
    return isEquals;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineDeviceType [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
