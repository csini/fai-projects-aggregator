package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.TipoServizioService;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import it.fai.ms.efservice.service.mapper.TipoServizioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing TipoServizio.
 */
@Service
@Transactional
public class TipoServizioServiceImpl implements TipoServizioService{

    private final Logger log = LoggerFactory.getLogger(TipoServizioServiceImpl.class);

    private final TipoServizioRepository tipoServizioRepository;

    private final TipoServizioMapper tipoServizioMapper;

    public TipoServizioServiceImpl(TipoServizioRepository tipoServizioRepository, TipoServizioMapper tipoServizioMapper) {
        this.tipoServizioRepository = tipoServizioRepository;
        this.tipoServizioMapper = tipoServizioMapper;
    }

    /**
     * Save a tipoServizio.
     *
     * @param tipoServizioDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TipoServizioDTO save(TipoServizioDTO tipoServizioDTO) {
        log.debug("Request to save TipoServizio : {}", tipoServizioDTO);
        TipoServizio tipoServizio = tipoServizioMapper.toEntity(tipoServizioDTO);
        tipoServizio = tipoServizioRepository.save(tipoServizio);
        return tipoServizioMapper.toDto(tipoServizio);
    }

    /**
     *  Get all the tipoServizios.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TipoServizioDTO> findAll() {
        log.debug("Request to get all TipoServizios");
        return tipoServizioRepository.findAll().stream()
            .map(tipoServizioMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one tipoServizio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TipoServizioDTO findOne(Long id) {
        log.debug("Request to get TipoServizio : {}", id);
        TipoServizio tipoServizio = tipoServizioRepository.findOne(id);
        return tipoServizioMapper.toDto(tipoServizio);
    }

    /**
     *  Delete the  tipoServizio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoServizio : {}", id);
        tipoServizioRepository.delete(id);
    }
}
