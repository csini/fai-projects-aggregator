package it.fai.ms.efservice.service;

import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;
import it.fai.ms.efservice.service.mapper.IndirizzoSpedizioneOrdiniMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing IndirizzoSpedizioneOrdini.
 */
@Service
@Transactional
public class IndirizzoSpedizioneOrdiniService {

    private final Logger log = LoggerFactory.getLogger(IndirizzoSpedizioneOrdiniService.class);

    private final IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepository;

    private final IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioneOrdiniMapper;

    public IndirizzoSpedizioneOrdiniService(IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepository, IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioneOrdiniMapper) {
        this.indirizzoSpedizioneOrdiniRepository = indirizzoSpedizioneOrdiniRepository;
        this.indirizzoSpedizioneOrdiniMapper = indirizzoSpedizioneOrdiniMapper;
    }

    /**
     * Save a indirizzoSpedizioneOrdini.
     *
     * @param indirizzoSpedizioneOrdiniDTO the entity to save
     * @return the persisted entity
     */
    public IndirizzoSpedizioneOrdiniDTO save(IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO) {
        log.debug("Request to save IndirizzoSpedizioneOrdini : {}", indirizzoSpedizioneOrdiniDTO);
        IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniMapper.toEntity(indirizzoSpedizioneOrdiniDTO);
        indirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniRepository.save(indirizzoSpedizioneOrdini);
        return indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdini);
    }

    /**
     *  Get all the indirizzoSpedizioneOrdinis.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<IndirizzoSpedizioneOrdiniDTO> findAll(Pageable pageable) {
        log.debug("Request to get all IndirizzoSpedizioneOrdinis");
        return indirizzoSpedizioneOrdiniRepository.findAll(pageable)
            .map(indirizzoSpedizioneOrdiniMapper::toDto);
    }

    /**
     *  Get one indirizzoSpedizioneOrdini by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public IndirizzoSpedizioneOrdiniDTO findOne(Long id) {
        log.debug("Request to get IndirizzoSpedizioneOrdini : {}", id);
        IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniRepository.findOne(id);
        return indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdini);
    }

    /**
     *  Delete the  indirizzoSpedizioneOrdini by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete IndirizzoSpedizioneOrdini : {}", id);
        indirizzoSpedizioneOrdiniRepository.delete(id);
    }
}
