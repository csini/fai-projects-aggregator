package it.fai.ms.efservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.FsmManageDeviceSent;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;

@Service
@Transactional
public class ManageDeviceSentModeService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public ManageDeviceSentModeService() {
  }

  public boolean isToBeSentAt(Richiesta richiesta, FsmManageDeviceSent manageSent) {
    boolean isToBeSent = false;
    TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
    ModalitaSpedizione modalitaSpedizione = tipoDispositivo.getModalitaSpedizione();
    TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
    log.info("Modalità spedizione dispositivo {} is {}", deviceType, modalitaSpedizione);

    if (modalitaSpedizione == null) {
      log.error("Modalità di spedizione is null for TipoDispositivo: {}", tipoDispositivo);
      return isToBeSent;
    }

    switch (manageSent) {
    case TRANSITO:
      isToBeSent = isDeviceInTransito(richiesta, modalitaSpedizione, deviceType);
      break;
    case FAI:
      isToBeSent = isToBeSentByFaiService(richiesta, modalitaSpedizione, deviceType);
      break;
    case CLIENTE:
      isToBeSent = isToBeSentAtClient(richiesta, modalitaSpedizione, deviceType);
      break;

    default:
      log.warn("Not managed FsmManageDeviceSent: {}", manageSent);
      break;
    }

    log.info("TipoDispositivo {} is to be sent at {}: {}", tipoDispositivo, manageSent, isToBeSent);
    return isToBeSent;
  }

  private boolean isDeviceInTransito(Richiesta richiesta, ModalitaSpedizione modalitaSpedizione, TipoDispositivoEnum deviceType) {
    boolean isTransit = false;
    switch (modalitaSpedizione) {
    case A_FAI:
      isTransit = true;
    case A_FAI_O_CLIENTE:
      Boolean destinazioneFai = richiesta.getOrdineCliente()
                                         .isDestinazioneFai();
      if (destinazioneFai != null && destinazioneFai) {
        isTransit = true;
      }
      break;
    case A_CLIENTE:
    case DA_DEPOSITO_FAI:
      break;
    default:
      log.warn("Modalità spedizione {} non gestita...", modalitaSpedizione);
      break;
    }
    return isTransit;
  }

  public boolean isToBeSentByFaiService(Richiesta richiesta, ModalitaSpedizione modalitaSpedizione, TipoDispositivoEnum deviceType) {
    boolean isToBeSentAtFai = false;
    switch (modalitaSpedizione) {
    case DA_DEPOSITO_FAI:
      isToBeSentAtFai = true;
      break;
    case A_FAI_O_CLIENTE:
    case A_CLIENTE:
    case A_FAI:
      break;
    default:
      log.warn("Modalità spedizione {} non gestita...", modalitaSpedizione);
      break;
    }

    return isToBeSentAtFai;
  }

  public boolean isToBeSentAtClient(Richiesta richiesta, ModalitaSpedizione modalitaSpedizione, TipoDispositivoEnum deviceType) {
    boolean isToBeSentAtClient = false;

    switch (modalitaSpedizione) {
    case A_CLIENTE:
      isToBeSentAtClient = true;
      break;
    case A_FAI_O_CLIENTE:
      Boolean destinazioneFai = richiesta.getOrdineCliente()
                                         .isDestinazioneFai();
      if (destinazioneFai != null && !destinazioneFai) {
        isToBeSentAtClient = true;
      }
      break;
    case A_FAI:
    case DA_DEPOSITO_FAI:
      break;
    default:
      log.warn("Modalità spedizione {} non gestita...", modalitaSpedizione);
      break;
    }

    return isToBeSentAtClient;
  }

}
