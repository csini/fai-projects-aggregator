package it.fai.ms.efservice.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RequestTheftLossMalfunctionService;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

@Service
@Transactional
public class RequestTheftLossMalfunctionServiceImpl extends RequestUtilService implements RequestTheftLossMalfunctionService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DispositivoServiceExt deviceServiceExt;

  public RequestTheftLossMalfunctionServiceImpl(final DispositivoServiceExt _deviceServiceExt,
                                                final ClienteFaiServiceExt _clienteFaiService) {
    super(_clienteFaiService);
    deviceServiceExt = _deviceServiceExt;

  }

  @Override
  public List<Richiesta> generateModificationRequestDeviceService(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException  {
    log.info("Start creationModificationRequest by carrelloDTO: {}", carrelloDTO);
    List<Richiesta> modificationRequests = new ArrayList<>();
      OrdineCliente clientOrderForRequest = createDefaultOrdineCliente(carrelloDTO.getCodiceAzienda());

      List<String> devicesIdentifier = carrelloDTO.getDispositivi();
      boolean isOnlyOneDevice = isOnlyOneDeviceToRequest(carrelloDTO.getDispositivi());
      if (!isOnlyOneDevice) {
        log.error("Impossible manage generation modification request of type: {} to more that one device.", carrelloDTO.getTipoOperazioneModifica() );
        throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
        .add(Errno.CANNOT_PROCESS_MULTIPLE_DEVICE);
      }

      Dispositivo device = retrieveDeviceByIdentifier(devicesIdentifier.get(0));
      boolean isGreen = isGreenDeviceStatus(device);
      if (isGreen) {
        checkDeviceType(device.getTipoDispositivo(), carrelloDTO.getTipoDispositivo());
        modificationRequests = generateRequest(carrelloDTO, device);
        assignClientOrderOnRequests(modificationRequests, clientOrderForRequest);
      }


    return modificationRequests;
  }

  private List<Richiesta> generateRequest(CarrelloModificaRichiestaDTO carrelloDTO, Dispositivo device) throws CustomException {
    List<Richiesta> requests = new ArrayList<>();

    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    Richiesta richiesta = createDefaultRequestByTypeAndDeviceType(carrelloDTO.getTipoOperazioneModifica(), tipoDispositivo);
    StatoRichiesta statusRequestByDeviceType = getStatusRequestByDeviceType(tipoDispositivo.getNome());
    richiesta.setStato(statusRequestByDeviceType);
    richiesta.setContratto(device.getContratto());

    richiesta.setUuidDocumento(carrelloDTO.getDocumento());

    richiesta.addDispositivo(device);
    try {
      deviceServiceExt.save(device);
    } catch (Exception e) {
      log.error("Exception on save device: " + device, e);
      throw CustomException.builder(HttpStatus.INTERNAL_SERVER_ERROR)
      .add(Errno.ERROR_CREATION_DEVICE);
    }
    
    requests.add(richiesta);
    return requests;
  }

  private void checkDeviceType(TipoDispositivo tipoDispositivo, TipoDispositivoEnum deviceTypeFromCarrello) {
    if (tipoDispositivo == null || !tipoDispositivo.getNome()
                                                   .equals(deviceTypeFromCarrello)) {
      throw new IllegalArgumentException("Device Type of device [" + ((tipoDispositivo != null) ? tipoDispositivo.getNome() : null)
                                         + "] is not equal to device Type passed on Cart [" + deviceTypeFromCarrello + "].");
    }
  }

  private boolean isGreenDeviceStatus(Dispositivo device) {
    StatoDispositivo statusDevice = device.getStato();

    if (statusDevice.isYellow()) {
      log.error("There is an operation pending on device: {} - {}, beacuse status is {} (yellow)", device.getIdentificativo(),
                device.getSeriale(), statusDevice);
      throw new IllegalArgumentException("Status Device [ " + statusDevice + " ] is yellow...so there is an operation pending.");
    }

    if (statusDevice.isBlue()) {
      log.warn("Skip generation modification request because device [ {} - {} ] is disable...", device.getIdentificativo(),
               device.getSeriale());
      return false;
    }
    return true;
  }

  private Dispositivo retrieveDeviceByIdentifier(String deviceIdentifier) throws CustomException {
    Dispositivo device = deviceServiceExt.findOneByIdentificativo(deviceIdentifier);
    if (device == null) {
      log.error("Device not found for identifier: {}" , deviceIdentifier);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED).add(Errno.DISPOSITIVO_ID_REQUIRED );
    }
    return device;
  }

  private boolean isOnlyOneDeviceToRequest(List<String> dispositivi) throws CustomException  {
    if (dispositivi == null || dispositivi.isEmpty()) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
      .add(Errno.DISPOSITIVO_ID_REQUIRED);
    }

    if (dispositivi.size() > 1) {
      return false;
    }
    return true;
  }

}
