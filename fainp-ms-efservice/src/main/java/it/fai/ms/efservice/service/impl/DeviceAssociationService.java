package it.fai.ms.efservice.service.impl;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.telepass.TelepassITRequestDTO;
import it.fai.ms.common.jms.dto.telepass.TelepassITResponseDTO;
import it.fai.ms.efservice.client.TelepassClient;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class DeviceAssociationService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DispositivoServiceExt    deviceService;
  private final VehicleWizardCacheServiceExt vehicleWizardCacheServiceExt;
  private final AssociazioneDVServiceExt associazioneService;

  private final String authorizationToken;
  private final TelepassClient telepassClient;

  public DeviceAssociationService(
    final DispositivoServiceExt _deviceService,
    final VehicleWizardCacheServiceExt _vehicleWizardCacheServiceExt,
    final AssociazioneDVServiceExt _associazioneService,
    final TelepassClient _telepassClient,
    @Value("${efservice.authorizationHeader}") String _authorizationToken) {

    deviceService = _deviceService;
    vehicleWizardCacheServiceExt = _vehicleWizardCacheServiceExt;
    associazioneService = _associazioneService;
    telepassClient = _telepassClient;
    authorizationToken = _authorizationToken;
  }

  public List<Long> generateAssociationDeviceVehicle(TipoDispositivoEnum tipoDispositivo, String identifierDevice, String newVeicolo) throws Exception {

    log.info("Load device: {}", identifierDevice);
    Dispositivo device = deviceService.findOneByIdentificativo(identifierDevice);

    if (isaTelepassItaliano(tipoDispositivo)){

      String targa = vehicleWizardCacheServiceExt.getTargaByUuidVehicle(newVeicolo);
      String nazione = vehicleWizardCacheServiceExt.getNazioneByUuidVehicle(newVeicolo);
      String codContrattoCliente = device.getContratto().getCodContrattoCliente();
      String seriale = device.getSeriale();

      TelepassITRequestDTO telepassITRequestDTO = new TelepassITRequestDTO();
      telepassITRequestDTO.setRequestId("generateAssociationDeviceVehicle§" + identifierDevice + "§" + newVeicolo);
      telepassITRequestDTO.setCodContrattoCliente(codContrattoCliente);
      telepassITRequestDTO.setSerialeDispositivo(seriale);
      telepassITRequestDTO.setTarga(targa);
      telepassITRequestDTO.setCodiceNazione(nazione);

      TelepassITResponseDTO resp = telepassClient.devicePlateInsert(authorizationToken, telepassITRequestDTO);
      if (TelepassITResponseDTO.OUTCOME.OK != resp.getOutcome()){
        throw new RuntimeException(resp.getMessage());
      }

    }
    return createAndSaveAssociazioneDv(newVeicolo, device);
  }

  private List<Long> createAndSaveAssociazioneDv(String newVeicolo, Dispositivo device) throws Exception {
    device.setTipoMagazzino(TipoMagazzino.NONE);
    device = deviceService.save(device);
    AssociazioneDV assDv = new AssociazioneDV().data(Instant.now())
                                               .uuidVeicolo(newVeicolo)
                                               .dispositivo(device);
    assDv = associazioneService.save(assDv);
    log.info("Persisted entity {}: [ {} ]", assDv.getClass()
                                                 .getSimpleName(),
             assDv);

    return new ArrayList<>(Arrays.asList(assDv.getId()));
  }

  private boolean isaTelepassItaliano(TipoDispositivoEnum tipoDispositivo) {
    return TipoDispositivoEnum.TELEPASS_ITALIANO == tipoDispositivo;
  }

}
