package it.fai.ms.efservice.client;


import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import it.fai.ms.efservice.client.anagazienda.ValidazioniAziende;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;

import java.util.List;

@FeignClient(name = "faianagaziende")
public interface AnagaziendeClient {

  public static final String BASE_PATH = "/api/accountazienda";
  public static final String VALIDATION_API = BASE_PATH + "/validazione";

  @GetMapping(BASE_PATH + "/{codiceAzienda}/indirizzo/spedizioneClienteFai")
  @Timed
  public IndirizziDiSpedizioneDTO getIndirizzoSpedizioneClienteFai(
    @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
    @PathVariable("codiceAzienda") String codiceAzienda
  );

  @PostMapping(VALIDATION_API)
  @Timed
  public ValidazioniAziende validaAzienda(
    @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
    @RequestBody List<String> codiciAzienda
  );

}
