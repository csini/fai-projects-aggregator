package it.fai.ms.efservice.service.fsm.bean.hgv;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.hgv.FsmInoltroHGV;

@WithStateMachine(id = FsmInoltroHGV.FSM_INOLTRO_HGV)
public class FsmInoltroHGVTransitionBean extends AbstractFsmRichiestaTransition {
}
