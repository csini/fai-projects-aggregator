package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_DartfordCrossing implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;

  public RuleEngine_Device_DartfordCrossing(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome,
                                            final RuleEngineServiceType _serviceTypeRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
    serviceTypeRuleContext = _serviceTypeRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean licensePlateNotNullNotEmpty = (vehicle.licensePlate != null && vehicle.licensePlate != empty);");
    rules.add("boolean makeNotNullNotEmpty = (vehicle.make != null && vehicle.make != empty);");
    rules.add("boolean colorNotNullNotEmpty = (vehicle.color != null && vehicle.color != empty);");
    rules.add("boolean modelNotNullNotEmpty = (vehicle.model != null && vehicle.model != empty);");
    rules.add("if (!licensePlateNotNullNotEmpty) return 'licensePlate';");
    rules.add("if (!makeNotNullNotEmpty) return 'make';");
    rules.add("if (!modelNotNullNotEmpty) return 'model';");
    rules.add("if (!colorNotNullNotEmpty) return 'color';");
    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);

    String res = null;
    switch (preconditionCode) {
    case LICENSE_PLATE:
      res = "licensePlate must be present";
      break;
    case MAKE:
      res = "make must be present";
      break;
    case TYPE:
      res = "type must be present";
      break;
    case MODEL:
      res = "model must be present";
      break;
    case COLOR:
      res = "color must be present";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
