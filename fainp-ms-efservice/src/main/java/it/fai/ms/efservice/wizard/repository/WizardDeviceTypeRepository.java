package it.fai.ms.efservice.wizard.repository;

import java.util.Optional;
import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;

public interface WizardDeviceTypeRepository {

  Set<WizardDeviceType> findAll();

  Set<WizardDeviceTypeId> findAllIds();

  Optional<WizardDeviceType> findByDeviceTypeId(WizardDeviceTypeId wizardDeviceTypeId);

  Set<WizardDeviceType> findByServiceTypeId(WizardServiceTypeId wizardServiceTypeId);
}
