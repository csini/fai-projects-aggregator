package it.fai.ms.efservice.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.ClienteFai_;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Contratto_;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;

@Repository
public interface ContrattoRepositoryExt extends ContrattoRepository,JpaSpecificationExecutor<Contratto> {

  @Query("SELECT c FROM Contratto c INNER JOIN c.clienteFai fai INNER JOIN c.produttore p WHERE fai.codiceCliente = :codiceAzienda AND p.nome = :produttore AND c.primario = true")
  List<Contratto> findByNomeProduttoreAndCodiceAzienda(@Param("produttore") String nomeProduttore,
                                                           @Param("codiceAzienda") String codiceAzienda);

  Optional<Contratto> findFirstByCodContrattoClienteAndProduttore_nome(String codContrattoCliente, String nomeProduttore);

  @Query("SELECT c FROM Contratto c JOIN Richiesta r ON  r.contratto.id = c.id" + " JOIN ClienteFai cf ON c.clienteFai.id = cf.id "
         + " WHERE cf.codiceCliente = :codiceAzienda and r.nomeServizio = :serviceName")
  List<Contratto> findByServiceNameAndCodiceAzienda(@Param("serviceName") String serviceName, @Param("codiceAzienda") String codiceAzienda);

  List<Contratto> findByClienteFaiAndProduttore(ClienteFai cliente, Produttore produttore);

  Contratto findFirstByOrderByIdDesc();

  @Override
  Contratto findFirstByCodContrattoCliente(String codContrattoCliente);

  /**
   * select distinct contratto.* from dispositivo inner join associazione_dv on
   * dispositivo.id=associazione_dv.dispositivo_id inner join contratto on dispositivo.contratto_id=contratto.id where
   * associazione_dv.uuid_veicolo = '9a4c1ac0-d075-11e7-8fab-cec278b6b50a'
   */
  @Query("select distinct c " + "from " + "Dispositivo d inner join AssociazioneDV ad on d.id=ad.dispositivo.id "
         + "inner join Contratto c on d.contratto.id=c.id " + "where " + "ad.uuidVeicolo = :uuidVeicolo ")
  List<Contratto> findAllByVehicleUuid(@Param("uuidVeicolo") String uuidVeicolo);

  List<Contratto> findByCodContrattoCliente(String codiceContratto);

  Optional<Contratto> findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue(String codiceCliente,
                                                                                        TipoDispositivoEnum tipoDispositivo);
  
  List<Contratto> findByClienteFai_codiceClienteAndProduttore_nomeAndStatoIn(@NotNull String codiceCliente, @NotNull String nomeProduttore,
                                                                                   List<StatoContratto> statusContract);

//  Boolean existsByCodContrattoCliente(String codContrattoCliente);

  Optional<Contratto> findByIdentificativo(String identificativo);

  List<Contratto> findByClienteFai_codiceCliente(String codiceCliente);

  boolean existsByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai);

  boolean existsByCodContrattoClienteAndProduttore_Id(String codContrattoCliente, Long produttoreId);

  boolean existsByCodContrattoClienteAndProduttore_IdAndClienteFai_IdNot(String codContrattoCliente, Long produttoreId, Long myClienteFaiId);

//  @Query("select codContrattoCliente,produttore.nome,clienteFai.ragioneSociale,clienteFai.codiceCliente,clienteFai.paese from Contratto where ")
  @Query("select c from Contratto c where not exists (select d from Dispositivo d where c = d.contratto  and d.stato not in :stati) and c.stato = 'ATTIVO' and size(c.dispositivo) > 0")
  Page<Contratto> reportContrattiAttiviConTuttiDispositiviBloccati(@Param("stati") Set<StatoDispositivo> stati, Pageable pageable);

  static Specification<Contratto> ofAzienda(String codiceAzienda) {
    return (root, query, cb) -> cb.equal(root.get(Contratto_.clienteFai).get(ClienteFai_.codiceCliente), codiceAzienda);
  }

}
