package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

@Service
@Transactional
public class VehicleWizardCacheServiceImplExt implements VehicleWizardCacheServiceExt {

  private final Logger log = LoggerFactory.getLogger(VehicleWizardCacheServiceImplExt.class);

  private final Map<String, WizardVehicleEntity> cacheVehicleWizard;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public VehicleWizardCacheServiceImplExt(@Qualifier("wizard_vehicle") Map<String, WizardVehicleEntity> _cacheVehicleWizard) {
    cacheVehicleWizard = _cacheVehicleWizard;
  }

  @Override
  public String getTargaByUuidVehicle(String uuidVehicle) {
    WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);
    if (wizardVehicleEntity == null) {
      log.warn("Not found vehicle in cache with UUID {}", uuidVehicle);
      return null;
    }
    String targa = wizardVehicleEntity.getLicensePlate();
    log.info("Found targa {} for uuidVehicle {}", targa, uuidVehicle);
    return targa;
  }

  @Override
  public String getNazioneByUuidVehicle(String uuidVehicle) {
    WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);
    if (wizardVehicleEntity == null) {
      log.warn("Not found vehicle in cache with UUID {}", uuidVehicle);
      return null;
    }
    String country = wizardVehicleEntity.getCountry();
    log.info("Found country {} for uuidVehicle {}", country, uuidVehicle);
    return country;
  }

  @Override
  public String getLibrettoByUuidVehicle(String uuidVehicle) {
    WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);
    if (wizardVehicleEntity == null) {
      log.warn("Not found vehicle in cache with UUID {}", uuidVehicle);
      return null;
    }
    return wizardVehicleEntity.getLibretto();
  }

  @Override
  public List<Richiesta> assignTargaOnRichieste(List<Richiesta> richieste) {
    return richieste.stream()
                    .map(r -> {
                      Richiesta request = assignTargaOnRichiesta(r);
                      log.debug("Richiesta with targa-nazione: {}", request);
                      return request;
                    })
                    .collect(toList());
  }

  @Override
  public List<Richiesta> assignTargaOnRichiesteDiSostituzione(List<Richiesta> richieste) {
    return richieste.stream()
                    .map(r -> {
                      OrdineCliente ordineCliente = r.getOrdineCliente();
                      Set<Richiesta> richiesteOrdine = ordineCliente.getRichiestas();
                      boolean check = richiesteContainsVariazioneOrMezzoRitargato(richiesteOrdine);
                      log.info("Richieste ordine contains type Variazione Targa or Mezzo Ritargato: {}", check);
                      if (check) {
                        r = assignTargaOnRichiesteDiSostituzione(r);
                      } else {
                        r = assignTargaOnRichiesta(r);
                      }

                      return r;
                    })
                    .collect(toList());
  }

  private boolean richiesteContainsVariazioneOrMezzoRitargato(Set<Richiesta> richiesteOrdine) {
    Optional<Richiesta> find = richiesteOrdine.stream()
                                              .filter(r -> r.getTipo() == TipoRichiesta.MEZZO_RITARGATO
                                                           || r.getTipo() == TipoRichiesta.VARIAZIONE_TARGA)
                                              .findFirst();
    return find.isPresent();
  }

  @Override
  public Richiesta assignTargaOnRichiesteDiSostituzione(Richiesta richiesta) {
    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    if (ordineCliente != null) {
      String uuidVeicolo = ordineCliente.getUuidVeicolo();
      if (StringUtils.isNotBlank(uuidVeicolo)) {
        String targa = getTargaByUuidVehicle(uuidVeicolo);
        log.info("Found Targa {} for UUID vehicle {}", targa, uuidVeicolo);
        richiesta.setAssociazione(targa);
      } else {
        log.error("Not set UUID Vehicle on OrdineCliente {} for Richiesta di sostituzione [Identificativo: {} - Tipo Richiesta: {}",
                  ordineCliente.getIdentificativo(), richiesta.getIdentificativo(), richiesta.getTipo());
      }
    } else {
      log.warn("Ordine Cliente is null for Richieste {}", richiesta.getIdentificativo());
    }
    return richiesta;
  }

  @Override
  public Richiesta assignTargaOnRichiesta(Richiesta richiesta) {
    log.debug("Assign Targa on richiesta by Vehicle Wizard Cache");

    // Per regola la corrispondenza tra richiesta e dispositivo deve essere uno ad uno, quindi posso prendere il primo
    // elemento della lista dei dispositivi;
    // Deve per forza esserci un dispositivo legato ad una richiesta, se no è un errore;
    Dispositivo dispositivo = richiesta.getDispositivos()
                                       .stream()
                                       .findFirst()
                                       .get();
    if (dispositivo != null) {
      // Di regola dovrebbe esserci solo un veicolo associato ad un dispositivo;
      Optional<AssociazioneDV> associazioneDvOpt = dispositivo.getAssociazioneDispositivoVeicolos()
                                                              .stream()
                                                              .findFirst();

      AssociazioneDV associazioneDV = null;
      if (associazioneDvOpt.isPresent()) {
        associazioneDV = associazioneDvOpt.get();
      } else {
        log.warn("Not found associazione DV for device: {}", dispositivo);
      }

      if (associazioneDV != null) {
        String uuidVeicolo = associazioneDV.getUuidVeicolo();
        log.debug("Retrieve targa by UUID Vehicle {}", uuidVeicolo);
        WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVeicolo);
        if (wizardVehicleEntity != null) {
          String licensePlate = wizardVehicleEntity.getLicensePlate();
          log.debug("Set targa {} for identificativo veicolo {} on richiesta {}", licensePlate, uuidVeicolo, richiesta.getIdentificativo());
          richiesta.setAssociazione(licensePlate);
          String country = wizardVehicleEntity.getCountry();
          log.debug("Country of targa {} : {}", licensePlate, country);
          richiesta.setCountry(country);
        } else {
          log.warn("Not found Wizard vehicle in cache for uuidVehicle {}", uuidVeicolo);
        }
      } else {
        log.warn("Not found associazioneDV for dispositivo {} for richiesta {}", dispositivo.getIdentificativo(),
                 richiesta.getIdentificativo());
      }
    } else {
      String msgErrorExp = "Not found dispositivo related on richiesta: " + richiesta.getIdentificativo();
      log.error(msgErrorExp);
    }

    return richiesta;
  }

  @Override
  public List<Richiesta> assignTargaOnRichiesteFiltered(List<Richiesta> richieste, TipoDispositivoEnum deviceType) {
    log.info("Assign Plate on richieste filtered by device type: {}", deviceType);
    return richieste.stream()
                    .map(r -> {
                      if (r.getTipoDispositivo() != null && r.getTipoDispositivo()
                                                             .getNome() == deviceType) {
                        return assignTargaOnRichiesta(r);
                      } else {
                        return r;
                      }
                    })
                    .collect(toList());
  }

  @Override
  public String getClasseEuroByUuidVehicle(String uuidVehicle) {
    WizardVehicleEntity wizardVehicleEntity = cacheVehicleWizard.get(uuidVehicle);
    if (wizardVehicleEntity == null) {
      log.warn("Not found vehicle in cache with UUID {}", uuidVehicle);
      return null;
    }
    String euroClass = wizardVehicleEntity.getEuroClass();
    log.info("Found country {} for euroClass {}", euroClass, uuidVehicle);
    return euroClass;
  }

}
