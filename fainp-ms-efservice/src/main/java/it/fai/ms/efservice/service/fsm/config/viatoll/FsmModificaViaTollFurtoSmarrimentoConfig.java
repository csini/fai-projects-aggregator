package it.fai.ms.efservice.service.fsm.config.viatoll;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoBloccato;
import it.fai.ms.efservice.service.fsm.action.FsmActionFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionSendDeviceLossMessage;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardAssenzaAllegato;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardPresenzaAllegato;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaViaTollFurtoSmarrimentoConfig.MOD_VIATOLL_FURTO_SMARRIMENTO)
public class FsmModificaViaTollFurtoSmarrimentoConfig extends FsmRichiestaConfig {

  public static final String MOD_VIATOLL_FURTO_SMARRIMENTO = "modViaTollFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue               senderFsmService;
  private final FsmActionSendDeviceLossMessage fsmActionSendDeviceLoss;

  public FsmModificaViaTollFurtoSmarrimentoConfig(final FsmSenderToQueue _senderFsmService,
                                                  final FsmActionSendDeviceLossMessage _fsmActionSendDeviceLoss) {
    super();
    senderFsmService = _senderFsmService;
    fsmActionSendDeviceLoss = _fsmActionSendDeviceLoss;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_VIATOLL_FURTO_SMARRIMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaViaTollFurtoSmarrimento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {

    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.SMARRIMENTO)
               .guard(new GuardSmarrimento())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.FURTO)
               .guard(new GuardFurto())
               .action(new FsmActionFurtoSmarrimento(senderFsmService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.SMARRIMENTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(fsmActionSendDeviceLoss)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.FURTO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .action(fsmActionSendDeviceLoss)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.MS_ORDINE_SOSPESO_EXCEPTION)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.DA_PROCESSARE)
               .event(RichiestaEvent.RESPONSE_OK)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .event(RichiestaEvent.RESPONSE_KO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA)
               .event(RichiestaEvent.MU_REINOLTRA)
               .action(fsmActionSendDeviceLoss)
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DA_PROCESSARE)
               .target(StatoRichiesta.ASSENZA_ALLEGATO)
               .guard(new GuardAssenzaAllegato())
               .action(new FsmActionGeneric()) // TODO
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DA_PROCESSARE)
               .target(StatoRichiesta.ESAME_DENUNCIA)
               .guard(new GuardPresenzaAllegato())
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ASSENZA_ALLEGATO)
               .target(StatoRichiesta.EVASO_POST_ALLINEAMENTO)
               .event(RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ASSENZA_ALLEGATO)
               .target(StatoRichiesta.EVASO_CON_DENUNCIA)
               .event(RichiestaEvent.MU_ALLEGATO_RICEVUTO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ASSENZA_ALLEGATO)
               .target(StatoRichiesta.ESAME_DENUNCIA)
               .event(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE)
               .action(new FsmActionDispositivoBloccato(senderFsmService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ESAME_DENUNCIA)
               .target(StatoRichiesta.ASSENZA_ALLEGATO)
               .event(RichiestaEvent.MU_ALLEGATO_ERRATO)
               .action(new FsmActionFurtoSmarrimento(senderFsmService)) //FIXME-fsm potrebbe arrivare qui da ATTIVo-SMARRIMENTO o ATTIVO-FURTO dove già avrebbe questo stato
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ESAME_DENUNCIA)
               .target(StatoRichiesta.EVASO_CON_DENUNCIA)
               .event(RichiestaEvent.MU_ALLEGATO_CORRETTO)
               .action(new FsmActionGeneric());
  }

}
