package it.fai.ms.efservice.service;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.email.EmailMessageByTemplate;
import it.fai.ms.common.jms.email.EmailSenderClient;

@Service
public class EmailNotificationSender {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  public EmailNotificationSender(final JmsProperties jmsProperties) {
    this.jmsProperties = jmsProperties;
  }

  public boolean sendNotification(EmailMessageByTemplate messageDTO) {
    if (messageDTO == null) {
      log.error("Not send mail because message is null");
      return false;
    }

    messageDTO.setDatetime(Instant.now());
    new EmailSenderClient(jmsProperties).sendNotification(messageDTO);
    return true;
  }

}
