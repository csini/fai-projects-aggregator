package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.Indirizzo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Indirizzo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndirizzoRepository extends JpaRepository<Indirizzo, Long> {

}
