package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.common.jms.dto.telepass.TelepassEuNuovoDispositivoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionMalfunzionamento implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(FsmActionMalfunzionamento.class);

  private final FsmSenderToQueue senderFsmService;

  public FsmActionMalfunzionamento(FsmSenderToQueue senderFsmService) {
    this.senderFsmService = senderFsmService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {

    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;

      TipoRichiesta tipo = richiesta.getTipo();

      switch (tipo) {
      case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
        createNewOrder(richiesta);
        break;

      default:
        break;
      }
    }
  }

  private void createNewOrder(Richiesta richiesta) {
    String identificativo = richiesta.getIdentificativo();
    TelepassEuNuovoDispositivoDTO telepassEuNuovoDispositivoDTO = new TelepassEuNuovoDispositivoDTO(identificativo);
    try {
      log.info("Send message to generate new richiesta of sostitution from richiesta: " + identificativo);
      JmsTopicSenderService senderJmsService = senderFsmService.getSenderJmsService();
      senderJmsService.publishTelepassEuNuovoDispositivoMessage(telepassEuNuovoDispositivoDTO);
    } catch (Exception e) {
      log.error("Error", e);
    }

  }

}
