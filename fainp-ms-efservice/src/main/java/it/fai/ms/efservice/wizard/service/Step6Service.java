package it.fai.ms.efservice.wizard.service;

import java.util.List;

import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DocumentStep6Response;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ServiceDocumentsStep6BodyRequest;

public interface Step6Service {

  List<DocumentStep6Response> getDocuments(ServiceDocumentsStep6BodyRequest _request, String _codiceAzienda);

  List<DocumentStep6Response> getDocuments(ContractDocumentsStep6BodyRequest _request, String _codiceAzienda);

  List<DocumentStep6Response> getDocuments(DeviceDocumentsStep6BodyRequest _request, String _codiceAzienda);

}
