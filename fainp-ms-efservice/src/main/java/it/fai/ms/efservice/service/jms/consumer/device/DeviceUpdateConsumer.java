package it.fai.ms.efservice.service.jms.consumer.device;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import it.fai.common.enumeration.StatoDispositivo;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.message.device.DeviceUpdateMessage;
import it.fai.ms.common.jms.efservice.message.device.GoboxDeviceInfo;
import it.fai.ms.common.jms.efservice.message.device.TrackycardDeviceInfo;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.DispositivoServiceExt;

@Service
@Transactional
public class DeviceUpdateConsumer
  implements Serializable {

  private static final long serialVersionUID = 2060207917160615313L;

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private DispositivoServiceExt dispositivoServiceExt;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  public void consume(DeviceUpdateMessage message) throws Exception {
    log.info("Manage message: {}", message);
    TrackycardDeviceInfo trackycardDeviceInfo = (TrackycardDeviceInfo) message.getDeviceInfo();



    Dispositivo trackycardDevice = dispositivoServiceExt.findOneByIdentificativo(trackycardDeviceInfo.getDeviceUuid().getUuid());
    //MAPPING STATUS
    if (trackycardDevice == null) {
      log.error("ERROR!!! DeviceUpdateMessage [{}] consumed, but no trackyCardDevice found fot this uuid:=[{}]",
                trackycardDeviceInfo.getDeviceUuid().getUuid());
      return;
    }

    final String targetService = trackycardDeviceInfo.getServiceStatus().getService();
    List<StatoDispositivoServizio> serviceToUpdate = new ArrayList<>();
    for (StatoDispositivoServizio statoDispositivoServizio :trackycardDevice.getStatoDispositivoServizios()) {
      if (statoDispositivoServizio.getTipoServizio().getNome().equals(targetService)){
        serviceToUpdate.add(statoDispositivoServizio);
      }
    }
    /*
    trackycardDevice
      .getStatoDispositivoServizios() //ConsumoAbstractProcessorTest.validateAndNotify_failiure_should_send_notitication:64
      .stream()
      .filter(statoDispositivoServizio -> statoDispositivoServizio.getTipoServizio().getNome().equals(targetService))
      .collect(Collectors.toList());
     */

    serviceToUpdate.forEach(
      statoDispositivoServizio -> statoDispositivoServizio.setStato(StatoDS.valueOf(trackycardDeviceInfo.getServiceStatus().getStatus()))
    );
    dispositivoServiceExt.saveWithChild(trackycardDevice, trackycardDevice.getAssociazioneDispositivoVeicolos(), trackycardDevice.getStatoDispositivoServizios());

    GoboxDeviceInfo goboxDeviceInfo = trackycardDeviceInfo.getGoboxDeviceInfo();
    //update su trackycard! Asfinag deve aver spostato un gobox
    if (goboxDeviceInfo == null){
      log.debug("No goboxDeviceInfo! Must be a gobox move! Update only trackycard service");
    } else {
      Dispositivo goboxDevice;
      if (goboxDeviceInfo.getDeviceUuid() != null) {
        goboxDevice = dispositivoServiceExt.findOneByIdentificativo(goboxDeviceInfo.getDeviceUuid().getUuid());
        if (goboxDevice == null) {
          log.error("ERROR!!! DeviceUpdateMessage [{}] consumed, but no gobox found fot this uuid:=[{}]",
            goboxDeviceInfo.getDeviceUuid().getUuid());
          return;
        }
      } else {
        goboxDevice = cloneFromTrackycardDevice(trackycardDevice, serviceToUpdate);
      }

      //MAP seriale
      goboxDevice.setSeriale(goboxDeviceInfo.getObuNumber().getCode());

      //MAP veicolo
      goboxDevice.getAssociazioneDispositivoVeicolos().stream().forEach(associazioneDV -> {
        associazioneDV.setUuidVeicolo(goboxDeviceInfo.getVehicleUuid().getUuid());
      });

      //MAP pan
      goboxDevice.getStatoDispositivoServizios().forEach(statoDispositivoServizio -> {
        statoDispositivoServizio.setPan(goboxDeviceInfo.getPan().getCode());
        statoDispositivoServizio.setStato(StatoDS.valueOf(trackycardDeviceInfo.getServiceStatus().getStatus()));
      });
      goboxDevice.setStato(StatoDispositivo.valueOf(goboxDeviceInfo.getDeviceStatus()));
      dispositivoServiceExt.saveWithChild(goboxDevice, goboxDevice.getAssociazioneDispositivoVeicolos(), goboxDevice.getStatoDispositivoServizios());
    }
  }

  private Dispositivo cloneFromTrackycardDevice(Dispositivo trackycardDevice, List<StatoDispositivoServizio> serviceToUpdate) {
    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setModalitaSpedizione(trackycardDevice.getModalitaSpedizione());
    dispositivo.setStato(trackycardDevice.getStato());
    dispositivo.tipoMagazzino(trackycardDevice.getTipoMagazzino());
    dispositivo.setContratto(trackycardDevice.getContratto());
    dispositivo.setIdentificativo(RandomStringUtils.random(17, true, true));
    dispositivo.setTipoDispositivo(tipoDispositivoRepository.findOneByNome(TipoDispositivoEnum.GO_BOX));
    dispositivo.setDataModificaStato(Instant.now());
    dispositivo.setOperazioniPossibili(trackycardDevice.getOperazioniPossibili());

    serviceToUpdate.forEach(statoDispositivoServizio -> {
      StatoDispositivoServizio clonedStatoDispositivoServizio = new StatoDispositivoServizio();
      clonedStatoDispositivoServizio.setStato(statoDispositivoServizio.getStato());
      clonedStatoDispositivoServizio.setDataAttivazione(statoDispositivoServizio.getDataAttivazione());
      clonedStatoDispositivoServizio.setDataDisattivazione(statoDispositivoServizio.getDataDisattivazione());
      clonedStatoDispositivoServizio.setDataScadenza(statoDispositivoServizio.getDataScadenza());
      clonedStatoDispositivoServizio.setDispositivo(dispositivo);
      clonedStatoDispositivoServizio.setTipoServizio(statoDispositivoServizio.getTipoServizio());
      dispositivo.getStatoDispositivoServizios().add(clonedStatoDispositivoServizio);
    });

    trackycardDevice.getAssociazioneDispositivoVeicolos().forEach(associazioneDV -> {;
      AssociazioneDV clonedAssociazioneDV = new AssociazioneDV();
      clonedAssociazioneDV.setUuidVeicolo(associazioneDV.getUuidVeicolo());
      clonedAssociazioneDV.setDispositivo(dispositivo);
      clonedAssociazioneDV.setData(associazioneDV.getData());
      dispositivo.getAssociazioneDispositivoVeicolos().add(clonedAssociazioneDV);
    });
    return dispositivo;
  }

}
