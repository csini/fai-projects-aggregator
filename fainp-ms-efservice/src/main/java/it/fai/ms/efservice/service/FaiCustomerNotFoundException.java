package it.fai.ms.efservice.service;

public class FaiCustomerNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 3574471042362835519L;

  public FaiCustomerNotFoundException() {
  }

  public FaiCustomerNotFoundException(final String _message) {
    super(_message);
  }

  public FaiCustomerNotFoundException(final Throwable _cause) {
    super(_cause);
  }

  public FaiCustomerNotFoundException(final String _message, final Throwable _cause) {
    super(_message, _cause);
  }

}
