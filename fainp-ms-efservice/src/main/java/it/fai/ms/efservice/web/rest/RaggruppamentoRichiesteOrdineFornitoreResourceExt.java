package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteFornitoreView;
import it.fai.ms.efservice.service.RaggruppamentoRichiesteOrdineFornitoreServiceExt;
import it.fai.ms.efservice.service.dto.SearchRaggruppamentoDTO;
import it.fai.ms.efservice.web.rest.util.ResponsePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(RaggruppamentoRichiesteOrdineFornitoreResourceExt.BASE_URL)
public class RaggruppamentoRichiesteOrdineFornitoreResourceExt {

  private final Logger log = LoggerFactory.getLogger(RaggruppamentoRichiesteOrdineFornitoreResourceExt.class);

  static final String BASE_URL = "/api";

  private static final String RAGGRUPPAMENTO_RICHIESTE_BY_DISP = "/raggruppamento-richieste-ordine-fornitores/byDevice";
  private static final String SEARCH_RAGGRUPPAMENTO_RICHIESTE = "/_search/raggruppamento-richieste";

  private final RaggruppamentoRichiesteOrdineFornitoreServiceExt serviceExt;

  public RaggruppamentoRichiesteOrdineFornitoreResourceExt(RaggruppamentoRichiesteOrdineFornitoreServiceExt serviceExt){
    this.serviceExt = serviceExt;
  }

  @GetMapping(RAGGRUPPAMENTO_RICHIESTE_BY_DISP + "/{tipoDispositivo}")
  @Timed
  public List<RaggruppamentoRichiesteFornitoreView> getAllRaggruppamentoRichiesteOrdineFornitoresByDevice(
    @PathVariable String tipoDispositivo
  ) {
    log.debug("REST request to get all RaggruppamentoRichiesteOrdineFornitores for TipoDispositivo: {}", tipoDispositivo);
    return serviceExt.findAllByTipoDispositivo(tipoDispositivo);
  }


  @PostMapping(SEARCH_RAGGRUPPAMENTO_RICHIESTE)
  @Timed
  public ResponseEntity<ResponsePage> searchRaggruppamentoRichiesteOrdineFornitore(
    @RequestBody(required = false) SearchRaggruppamentoDTO dto,
    Pageable pageable
  ) {
    log.debug("REST request to search RaggruppamentoRichiesteOrdineFornitore : {}", dto);
    if(dto == null) {
      dto = new SearchRaggruppamentoDTO();
    }
    int pageNumber = 0;
    if(pageable.getPageNumber() > 0) {
      pageNumber = pageable.getPageNumber() - 1;
    }

    PageRequest pageRequest = new PageRequest(pageNumber, pageable.getPageSize(), pageable.getSort());

    Page<RaggruppamentoRichiesteFornitoreView> page = serviceExt.findAllWithFilter(dto, pageRequest);

    return ResponseEntity.ok(
      new ResponsePage<>(page)
    );
  }

}
