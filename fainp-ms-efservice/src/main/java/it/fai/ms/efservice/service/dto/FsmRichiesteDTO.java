package it.fai.ms.efservice.service.dto;

import java.util.Objects;

public class FsmRichiesteDTO extends FsmDTO {

  private static final long serialVersionUID = 1337926327933476146L;

  private String motivoSospensione;

  public String getMotivoSospensione() {
    return motivoSospensione;
  }

  public void setMotivoSospensione(String motivoSospensione) {
    this.motivoSospensione = motivoSospensione;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("FsmRichiesteDTO ");
    sb.append("[");
    
    sb.append("operazione: ");
    sb.append(getOperazione());
    sb.append(", identificativi: ");
    sb.append(getStringIdentificativi());
    sb.append(", motivoSospensione: ");
    sb.append(motivoSospensione);
    sb.append(", nota: ");
    sb.append(getNota());
    
    sb.append("]");
    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FsmRichiesteDTO that = (FsmRichiesteDTO) o;
    return Objects.equals(motivoSospensione, that.motivoSospensione);
  }

  @Override
  public int hashCode() {
    return Objects.hash(motivoSospensione);
  }
}
