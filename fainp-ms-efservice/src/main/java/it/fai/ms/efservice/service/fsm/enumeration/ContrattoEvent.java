package it.fai.ms.efservice.service.fsm.enumeration;

public enum ContrattoEvent {
  MS_SOSPENDI, MU_REVOCA, MS_RIATTIVA;
}
