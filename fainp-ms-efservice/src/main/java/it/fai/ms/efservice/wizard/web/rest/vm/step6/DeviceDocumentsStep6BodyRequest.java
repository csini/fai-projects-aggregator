package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;
import java.util.List;

public class DeviceDocumentsStep6BodyRequest implements Serializable {
  private static final long serialVersionUID = 6036609154785959431L;

  private List<DeviceStep6> devices;

  public DeviceDocumentsStep6BodyRequest() {
  }

  public List<DeviceStep6> getDevices() {
    return devices;
  }

  public void setDevices(List<DeviceStep6> devices) {
    this.devices = devices;
  }

  @Override
  public String toString() {
    return "DeviceDocumentsStep6BodyRequest [devices=" + devices + "]";
  }
}
