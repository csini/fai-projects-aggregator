package it.fai.ms.efservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.TipoServizio;


/**
 * Spring Data JPA repository for the TipoServizio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoServizioRepository extends JpaRepository<TipoServizio, Long>,JpaSpecificationExecutor<TipoServizio> {

}
