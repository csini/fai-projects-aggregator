package it.fai.ms.efservice.listener.events;

import it.fai.ms.efservice.domain.TipoDispositivo;
import org.springframework.context.ApplicationEvent;

public class DispositivoDepositoOutEvent extends ApplicationEvent {

  private TipoDispositivo tipoDispositivo;

  public DispositivoDepositoOutEvent(Object source, TipoDispositivo tipoDispositivo) {
    super(source);
    this.tipoDispositivo = tipoDispositivo;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  @Override
  public String toString() {
    return "DispositivoDepositoOutEvent{" +
      "tipoDispositivo=" + tipoDispositivo +
      '}';
  }
}
