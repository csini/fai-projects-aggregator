package it.fai.ms.efservice.service.jms.consumer;

import static it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent.MS_CTRRI;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.contract.ContractCreatedMessage;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.impl.OrderRequestService;

@Service
@Transactional
public class ContractCreatedConsumer {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaUtils      finalStateMachine;
  private final RichiestaRepositoryExt richiestaRepository;
  private final RichiestaServiceExt    richiestaService;
  private final OrderRequestService    orderRequestService;

  @Autowired
  public ContractCreatedConsumer(final RichiestaRepositoryExt _richiestaRepository, final FsmRichiestaUtils _finalStateMachine,
                                 final RichiestaServiceExt _richiestaService, final OrderRequestService _orderRequestService) {
    richiestaRepository = _richiestaRepository;
    richiestaService    = _richiestaService;
    finalStateMachine   = _finalStateMachine;
    orderRequestService = _orderRequestService;
  }

  // only for Telepass device
  public void consume(final ContractCreatedMessage _contractCreateMessage) throws Exception {

    final String companyCodeUuid = _contractCreateMessage.getCompanyCodeUuid().getUuid();
    final String contractUuid    = _contractCreateMessage.getContractUuid().getUuid();

    final List<Richiesta> orderRequests = orderRequestService.getOrderRequestsWithoutContract(companyCodeUuid);
    for (Richiesta orderRequest : orderRequests) {
      if (orderRequest.getTipoDispositivo() != null && orderRequest.getTipoDispositivo().getProduttore() != null
          && orderRequest.getTipoDispositivo().getProduttore().getNome().equals("TELEPASS")) {
        try {
          _log.info("Processing {} for OrderRequestDecorator {} ", _contractCreateMessage, orderRequest.getIdentificativo());
          Richiesta request = createNewContract(contractUuid, orderRequest.getIdentificativo());
          _log.info("Request with contract: {}", request);
          executeStateMachine(request).ifPresent(orderUpdatedByFinalStateMachine -> {
            richiestaRepository.save(orderUpdatedByFinalStateMachine);
            _log.info("Saved {}", orderUpdatedByFinalStateMachine);
          });
        } catch (final Exception _e) {
          _log.error("Error processing {}", _contractCreateMessage, _e);
        }
      } else {
        _log.debug("Skipped because produttore for request: {} is not TELEPASS.", orderRequest != null ? orderRequest.getIdentificativo() : null);
      }
    }
  }

  private Richiesta createNewContract(final String _contractUuid, final String _orderRequestUuid) throws Exception {
    Richiesta request = richiestaService.associateContrattoToRichiesta(_contractUuid, _orderRequestUuid);
    _log.info("Contract {} created for OrderRequestDecorator {}", _contractUuid, _orderRequestUuid);
    return request;
  }

  private Optional<Richiesta> executeStateMachine(final Richiesta _richiesta) throws Exception {
    Richiesta richiesta =  finalStateMachine.changeStatusToRichiesta(_richiesta, MS_CTRRI.name(), "", null);
    return Optional.ofNullable(richiesta);
  }

}
