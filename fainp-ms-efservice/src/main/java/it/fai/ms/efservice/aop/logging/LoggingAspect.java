package it.fai.ms.efservice.aop.logging;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import io.github.jhipster.config.JHipsterConstants;

/**
 * Aspect for logging execution of service and repository Spring components. By default, it only runs with the "dev"
 * profile.
 */
@Aspect
public class LoggingAspect {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private final Environment env;

  public LoggingAspect(Environment env) {
    this.env = env;
  }

  /**
   * Pointcut that matches all repositories, services and Web REST endpoints.
   */
  @Pointcut("within(@org.springframework.stereotype.Repository *)" + " || within(@org.springframework.stereotype.Service *)"
      + " || within(@org.springframework.web.bind.annotation.RestController *)")
  public void springBeanPointcut() {
    // Method is empty as this is just a Pointcut, the implementations are in the advices.
  }

  /**
   * Pointcut that matches all Spring beans in the application's main packages.
   */
  @Pointcut("within(it.fai.ms.efservice.repository..*)" + " || within(it.fai.ms.efservice.service..*)"
      + " || within(it.fai.ms.efservice.web.rest..*)")
  public void applicationPackagePointcut() {
    // Method is empty as this is just a Pointcut, the implementations are in the advices.
  }

  /**
   * Advice that logs methods throwing exceptions.
   *
   * @param joinPoint
   *          join point for advice
   * @param e
   *          exception
   */
  @AfterThrowing(pointcut = "applicationPackagePointcut() && springBeanPointcut()", throwing = "e")
  public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
    if (env.acceptsProfiles(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)) {
      log.error("Exception in {}.{}() with cause = \'{}\' and exception = \'{}\'", joinPoint.getSignature()
                .getDeclaringTypeName(),
                joinPoint.getSignature()
                .getName(),
                e.getCause() != null ? e.getCause() : "NULL", e.getMessage(), e);

    } else {
      log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature()
                .getDeclaringTypeName(),
                joinPoint.getSignature()
                .getName(),
                e.getCause() != null ? e.getCause() : "NULL");
    }
  }

  /**
   * Advice that logs when a method is entered and exited.
   *
   * @param joinPoint
   *          join point for advice
   * @return result
   * @throws Throwable
   *           throws IllegalArgumentException
   */
  @Around("applicationPackagePointcut() && springBeanPointcut()")
  public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
    if (log.isDebugEnabled()) {
      log.debug("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature()
                .getDeclaringTypeName(),
                joinPoint.getSignature()
                .getName(),
                Arrays.toString(joinPoint.getArgs()));
    }
    try {
      Object result = joinPoint.proceed();
      if (log.isDebugEnabled()) {
        final int MAXELEMENTS = 1000;
        if (isCollectionOrArrayWithLessThan(result,1000)) {
        log.debug("Exit: {}.{}() with result = {}", joinPoint.getSignature()
                  .getDeclaringTypeName(),
                  joinPoint.getSignature()
                  .getName(),
                  result);
        }else {
          log.debug("Exit: {}.{}() with result = {}", joinPoint.getSignature()
                    .getDeclaringTypeName(),
                    joinPoint.getSignature()
                    .getName(),
                    "too many elemets (>"+MAXELEMENTS+") of "+result.getClass().getName()+" to print");
        }
      }
      return result;
    } catch (IllegalArgumentException e) {
      log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()), joinPoint.getSignature()
                .getDeclaringTypeName(),
                joinPoint.getSignature()
                .getName());

      throw e;
    }
  }
  
  
  public static boolean isCollectionOrArrayWithLessThan(Object ob, int allowedElements) {
    int size = 0;
    if (ob instanceof Collection)
      size = ((Collection<?>)ob).size();
    else if (ob instanceof Map)
      size = ((Map<?,?>)ob).size();
    else if (ob!=null && ob.getClass().isArray())
      size = Array.getLength(ob);
    else
      return true;
    return size < allowedElements;
    
  }
}
