package it.fai.ms.efservice.service.fsm.bean.gransanbernardo;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmModificaGranSanBernardoFurtoSmarrimento;

@WithStateMachine(id = FsmModificaGranSanBernardoFurtoSmarrimento.FSM_MODIFICA_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO)
public class FsmModificaGranSanBernardoFurtoSmarrimentoTransitionBean extends AbstractFsmRichiestaTransition {
}
