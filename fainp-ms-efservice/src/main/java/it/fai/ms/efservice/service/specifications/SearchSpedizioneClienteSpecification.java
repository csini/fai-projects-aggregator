package it.fai.ms.efservice.service.specifications;

import it.fai.ms.efservice.domain.SpedizioneClienteView;
import it.fai.ms.efservice.domain.SpedizioneClienteView_;
import it.fai.ms.efservice.service.dto.SearchSpedizioneClienteDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class SearchSpedizioneClienteSpecification implements Specification<SpedizioneClienteView> {

  private final SearchSpedizioneClienteDTO dto;

  public SearchSpedizioneClienteSpecification(SearchSpedizioneClienteDTO dto) {
    this.dto = dto;
  }

  private String likeIt(String param) {
    return "%"+param+"%";
  }

  @Override
  public Predicate toPredicate(
    Root<SpedizioneClienteView> root,
    CriteriaQuery<?> criteriaQuery,
    CriteriaBuilder builder
  ) {
    List<Predicate> predicates = new ArrayList<>();

    //mostriamo solo quelli disponibili
    predicates.add(
      builder.greaterThan(root.get(SpedizioneClienteView_.numeroDispositiviDisponibili), 0L)
    );

    if (StringUtils.isNotEmpty(dto.getCerca())) {
      predicates.add(
        builder.or(
          builder.like(
            root.get(SpedizioneClienteView_.codiceAzienda),
            likeIt(dto.getCerca())
          ),
          builder.like(
            root.get(SpedizioneClienteView_.ragioneSociale),
            likeIt(dto.getCerca())
          )
        )
      );
    }

    return builder.and(predicates.toArray(new Predicate[predicates.size()]));
  }
}
