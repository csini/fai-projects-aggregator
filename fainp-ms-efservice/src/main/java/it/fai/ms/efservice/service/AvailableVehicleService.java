package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnership;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipCompanyCode;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;
import it.fai.ms.efservice.vehicle.ownership.service.VehicleOwnershipService;
import it.fai.ms.efservice.vehicle.ownership.web.rest.vm.AvailableVehicle;

@Service
@Transactional
public class AvailableVehicleService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final VehicleOwnershipService vehicleOwnershipService;

  private final RuleOutcomeRepository ruleOutcomeRepository;

  private final DispositivoServiceExt deviceService;

  public AvailableVehicleService(final VehicleOwnershipService _vehicleOwnershipService, final RuleOutcomeRepository _ruleOutcomeRepository,
                                 final DispositivoServiceExt _deviceService) {
    vehicleOwnershipService = _vehicleOwnershipService;
    ruleOutcomeRepository = _ruleOutcomeRepository;
    deviceService = _deviceService;
  }

  public Set<AvailableVehicle> getAvailableVehicleFiltered(String _companyCode, String _deviceType, String _vehicleUuid) {
    Set<VehicleOwnership> enabledVehicles = vehicleOwnershipService.getEnabledVehicles(new VehicleOwnershipCompanyCode(_companyCode),
                                                                                       new VehicleOwnershipId(_vehicleUuid),
                                                                                       _deviceType);
    Set<StatoDispositivoServizio> sdss = getStatoDispositivoServiziosByUuidVehicle(_vehicleUuid, _deviceType);
    Set<AvailableVehicle> availableVehicles = getAvailableVehicle(enabledVehicles, sdss, _deviceType);
    return availableVehicles;
  }

  private Set<AvailableVehicle> getAvailableVehicle(Set<VehicleOwnership> enabledVehicles, Set<StatoDispositivoServizio> sdss,
                                                    String deviceType) {
    Set<AvailableVehicle> availableVehicles = new HashSet<>();
    if (deviceType.equals(TipoDispositivoEnum.TESSERA_CARONTE.name())) {
      availableVehicles = enabledVehicles.parallelStream()
                                         .map(vehicleOwnership -> mapVehicleOwnershipToAvailableVehicle(vehicleOwnership))
                                         .collect(toSet());
    } else {
      availableVehicles = enabledVehicles.parallelStream()
                                         .map(vehicleOwnership -> {
                                           vehicleOwnership = getAvailableVehicle(vehicleOwnership, deviceType, sdss);
                                           if (vehicleOwnership != null) {
                                             return mapVehicleOwnershipToAvailableVehicle(vehicleOwnership);
                                           } else {
                                             return null;
                                           }
                                         })
                                         .filter(a -> a != null)
                                         .collect(toSet());
    }
    return availableVehicles;
  }

  public Set<AvailableVehicle> getAvailableVehicleFiltered(String _companyCode, String _vehicleUuid) {
    Set<AvailableVehicle> availableVehicles = vehicleOwnershipService.getEnabledVehicles(new VehicleOwnershipCompanyCode(_companyCode),
                                                                                         new VehicleOwnershipId(_vehicleUuid))
                                                                     .stream()
                                                                     .map(vehicleOwnership -> {
                                                                       return mapVehicleOwnershipToAvailableVehicle(vehicleOwnership);
                                                                     })
                                                                     .collect(toSet());
    return availableVehicles;
  }

  public AvailableVehicle mapVehicleOwnershipToAvailableVehicle(VehicleOwnership vos) {
    AvailableVehicle availableVehicle = new AvailableVehicle();
    availableVehicle.setLicensePlate(vos.getLicensePlate());
    availableVehicle.setUuid(vos.getUuid());
    return availableVehicle;
  }

  public VehicleOwnership getAvailableVehicle(VehicleOwnership vehicleOwnership, String deviceType, Set<StatoDispositivoServizio> sdss) {
    VehicleOwnership vo = null;

    String uuid = vehicleOwnership.getUuid();
    RuleEngineVehicleId vehicleId = new RuleEngineVehicleId(uuid);
    if (isValidVehicleRule(vehicleId)) {
      RuleEngineDeviceTypeId deviceTypeId = new RuleEngineDeviceTypeId(deviceType);
      if (isValidDeviceRule(deviceTypeId, vehicleId)) {
        if (isValidAllService(deviceTypeId, vehicleId, sdss)) {
          vo = vehicleOwnership;
        }
      }
    }

    return vo;
  }

  public Set<StatoDispositivoServizio> getStatoDispositivoServiziosByUuidVehicle(String uuidVehicleSelected, String deviceType) {
    Set<StatoDispositivoServizio> sdss = null;
    Set<Dispositivo> devices = deviceService.findByAssociazioneDispositivoVeicolos_uuidVeicolo(uuidVehicleSelected);
    Optional<Dispositivo> deviceFilteredOpt = devices.stream()
                                                     .filter(d -> d.getTipoDispositivo()
                                                                   .getNome()
                                                                   .name()
                                                                   .equals(deviceType))
                                                     .findFirst();
    if (!deviceFilteredOpt.isPresent()) {
      log.error("Not found device related on vehicle {} of type {}", uuidVehicleSelected, deviceType);
    } else {
      Dispositivo device = deviceFilteredOpt.get();
      sdss = device.getStatoDispositivoServizios();
    }

    return sdss;
  }
  
  public Set<AvailableVehicle> filterVehicleByDeviceTypeIsNot(Set<AvailableVehicle> availableVehicles, String deviceType) {
    log.debug("Device type: {}", deviceType);
    if(deviceType != "undefined") {
      Set<AvailableVehicle> filteredAvailableVehicles = availableVehicles.parallelStream().map(av -> {
        String uuidVehicle = av.getUuid();
        Set<Dispositivo> devices = deviceService.findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(uuidVehicle, TipoDispositivoEnum.valueOf(deviceType));
        Optional<Dispositivo> optDevice = devices.stream().filter(d -> !StatoDispositivo.getBlueState().contains(d.getStato())).findFirst();
        if(optDevice.isPresent()) {
          Dispositivo device = optDevice.get();
          log.debug("Found at least Device [{} - {}] with state {}, so skip vehicle...", device.getIdentificativo(), device.getTipoDispositivo().getNome(), device.getStato());
          return null;
        } else {
          log.debug("Not found at least device with state not blue...so add vehicle {}", uuidVehicle);
          return av;
        }
      }).filter(av -> av != null).collect(toSet());
      
      log.debug("Filtered vehicles found: {}", filteredAvailableVehicles.size());
      availableVehicles = filteredAvailableVehicles;
    }
    return availableVehicles;
  }

  private boolean isValidAllService(RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId,
                                    Set<StatoDispositivoServizio> sdss) {
    boolean isValid = true;
    if (sdss != null) {
      isValid = checkValidationAllService(sdss, deviceTypeId, vehicleId);
    }
    return isValid;
  }

  private boolean isValidVehicleRule(RuleEngineVehicleId vehicleId) {
    log.debug("Is valid Vehicle: {}", vehicleId);
    Optional<RuleOutcome> optRuleOutcomeVehicle = ruleOutcomeRepository.find(vehicleId);
    return isValidPrecondition(optRuleOutcomeVehicle);
  }

  private boolean isValidDeviceRule(RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId) {
    log.debug("Is valid Device: {} and Vehicle: {}", deviceTypeId, vehicleId);
    Optional<RuleOutcome> optRuleOutcomeDevice = ruleOutcomeRepository.find(deviceTypeId, vehicleId);
    return isValidPrecondition(optRuleOutcomeDevice);
  }

  private boolean checkValidationAllService(Set<StatoDispositivoServizio> sdss, RuleEngineDeviceTypeId deviceTypeId,
                                            RuleEngineVehicleId vehicleId) {
    Optional<Boolean> optServiceNotValid = sdss.parallelStream()
                                               .filter(s -> s.getStato() == StatoDS.ATTIVO || s.getStato() == StatoDS.IN_ATTIVAZIONE)
                                               .map(s -> {
                                                 TipoServizio tipoServizio = s.getTipoServizio();
                                                 RuleEngineServiceTypeId serviceTypeId = new RuleEngineServiceTypeId(tipoServizio.getNome());
                                                 log.debug("Is valid to Vehicle: {}, Device: {} and Service: {}", vehicleId.getId(),
                                                          deviceTypeId.getId(), serviceTypeId);
                                                 Optional<RuleOutcome> optRuleOutcomeService = ruleOutcomeRepository.find(serviceTypeId,
                                                                                                                          deviceTypeId,
                                                                                                                          vehicleId);
                                                 if (isValidPrecondition(optRuleOutcomeService)) {
                                                   return true;
                                                 } else {
                                                   log.info("Validation of service {} is not valid!", tipoServizio.getNome());
                                                   return false;
                                                 }
                                               })
                                               .filter(b -> b == false)
                                               .findFirst();
    return !optServiceNotValid.isPresent();
  }

  private boolean isValidPrecondition(Optional<RuleOutcome> rule) {
    boolean isValid = false;
    if (rule.isPresent()) {
      RuleOutcome ruleOutcome = rule.get();
      Boolean outcome = ruleOutcome.getOutcome();
      if (outcome != null && outcome) {
        isValid = true;
      } else {
        log.info("Precondition failed: {}", ruleOutcome);
      }
    }

    return isValid;
  }

}
