package it.fai.ms.efservice.client;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.io.IOException;
import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import it.fai.ms.common.jms.dto.frejus.ConfirmTesseraDto;
import it.fai.ms.efservice.dto.ResponseTessereFrejusDto;

@FeignClient(name = "faiminorservice")
//@RequestMapping(FrejusClient.BASE_PATH)
public interface FrejusClient {

  public static final String BASE_PATH = "/api/frejus";


  @PostMapping(BASE_PATH+"/confermaTransito/{paese}/{uuid}")
  @ResponseBody
  public List<ConfirmTesseraDto> processFile(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,@PathVariable("uuid") String uuid,@PathVariable("paese")String paese) throws IOException;

  @PostMapping(BASE_PATH+"/confermaTransitoData/{paese}")
  ConfirmTesseraDto processTransito(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,ResponseTessereFrejusDto data, @PathVariable("paese")String paese);

}
