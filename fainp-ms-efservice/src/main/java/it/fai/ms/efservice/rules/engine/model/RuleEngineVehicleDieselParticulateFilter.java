package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;

public class RuleEngineVehicleDieselParticulateFilter implements Serializable {

  private static final long serialVersionUID = 6168640680073593176L;

  private Integer filterClass;

  public RuleEngineVehicleDieselParticulateFilter(final Integer _filterClass) {
    filterClass = _filterClass;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleEngineVehicleDieselParticulateFilter) _obj).getFilterClass(), filterClass);
    }
    return isEquals;
  }

  public Integer getFilterClass() {
    return filterClass;
  }

  @Override
  public int hashCode() {
    return Objects.hash(filterClass);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineVehicleDieselParticulateFilter [filterClass=");
    builder.append(filterClass);
    builder.append("]");
    return builder.toString();
  }

}
