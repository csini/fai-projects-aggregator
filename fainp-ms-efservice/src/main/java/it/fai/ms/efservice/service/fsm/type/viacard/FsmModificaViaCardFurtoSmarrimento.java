package it.fai.ms.efservice.service.fsm.type.viacard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.viacard.FsmModificaViaCardFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaViaCardFurtoSmarrimento.FSM_MOD_VIACARD_FURTO_SMARRIMENTO)
public class FsmModificaViaCardFurtoSmarrimento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_VIACARD_FURTO_SMARRIMENTO = "fsmModificaViaCardFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaViaCardFurtoSmarrimento(@Qualifier(FsmModificaViaCardFurtoSmarrimentoConfig.MOD_VIACARD_FURTO_SMARRIMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                            FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_VC_FURTO_SMARRIMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.VIACARD };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
