package it.fai.ms.efservice.service.fsm.bean.telepass.ita;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaTItaRientroMalfunzionamento")
public class FsmModificaTItaRientroMalfunzionamentoTransitionBean extends AbstractFsmRichiestaTransition {
}