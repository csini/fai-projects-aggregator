package it.fai.ms.efservice.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.repository.OrdineClienteRepository;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.service.dto.OrdineClienteDTO;
import it.fai.ms.efservice.service.jms.dml.OrdineClienteDmlSenderUtil;
import it.fai.ms.efservice.service.mapper.OrdineClienteMapper;

/**
 * Service Implementation for managing OrdineCliente.
 */
@Service
@Transactional
public class OrdineClienteServiceImpl implements OrdineClienteService {

  private final Logger log = LoggerFactory.getLogger(OrdineClienteServiceImpl.class);

  private final OrdineClienteRepository ordineClienteRepository;

  private final OrdineClienteMapper ordineClienteMapper;

  @Autowired
  JmsProperties jmsProperties;

  public OrdineClienteServiceImpl(OrdineClienteRepository ordineClienteRepository, OrdineClienteMapper ordineClienteMapper) {
    this.ordineClienteRepository = ordineClienteRepository;
    this.ordineClienteMapper = ordineClienteMapper;
  }

  /**
   * Save a ordineCliente.
   *
   * @param ordineClienteDTO
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public OrdineClienteDTO save(OrdineClienteDTO ordineClienteDTO) {
    log.debug("Request to save OrdineCliente : {}", ordineClienteDTO);
    OrdineCliente ordineCliente = ordineClienteMapper.toEntity(ordineClienteDTO);
    boolean isNew = ordineCliente.getId() == null;
    ordineCliente = ordineClienteRepository.save(ordineCliente);
    OrdineClienteDTO rs = ordineClienteMapper.toDto(ordineCliente);

    if (ordineCliente.getRichiestas() == null || ordineCliente.getRichiestas()
                                                              .isEmpty()) {
      log.warn("ordineCliente.getRichiestas() is null or empty {}", ordineCliente.getRichiestas());
    }

    // send notifica save Ordine
    if (!isNew)
      try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(ordineCliente);
      } catch (Exception e) {
        log.error(e.getMessage());
      }

    return rs;
  }

  /**
   * Save a ordineCliente.
   *
   * @param ordineCliente
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public OrdineCliente save(OrdineCliente ordineCliente) {
    boolean isNew = ordineCliente.getId() == null;
    ordineCliente = ordineClienteRepository.save(ordineCliente);
    if (ordineCliente.getRichiestas() == null || ordineCliente.getRichiestas()
                                                              .isEmpty()) {
      log.warn("ordineCliente.getRichiestas() is null or empty {}", ordineCliente.getRichiestas());
    }

    // send notifica save Ordine
    if (!isNew && ordineCliente.isEnabledDmlMessage()) {
      log.info("Send ordineCliente DML notification....");
      try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
        ocdSenderUtil.sendSaveNotification(ordineCliente);
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      //new OrdineClienteDmlSenderUtil(jmsProperties).sendSaveNotification(ordineCliente);
    }
    return ordineCliente;
  }

  /**
   * Get all the ordineClientes.
   *
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public List<OrdineClienteDTO> findAll() {
    log.debug("Request to get all OrdineClientes");
    return ordineClienteRepository.findAll()
                                  .stream()
                                  .map(ordineClienteMapper::toDto)
                                  .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get one ordineCliente by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Override
  @Transactional(readOnly = true)
  public OrdineClienteDTO findOne(Long id) {
    log.debug("Request to get OrdineCliente : {}", id);
    OrdineCliente ordineCliente = ordineClienteRepository.findOne(id);
    return ordineClienteMapper.toDto(ordineCliente);
  }

  /**
   * Delete the ordineCliente by id.
   *
   * @param id
   *          the id of the entity
   */
  @Override
  public void delete(Long id) {
    OrdineClienteDTO ordineClienteDTO = findOne(id);
    log.debug("Request to delete OrdineCliente : {}", id);
    ordineClienteRepository.delete(id);
    try(OrdineClienteDmlSenderUtil ocdSenderUtil = new OrdineClienteDmlSenderUtil(jmsProperties)){
      ocdSenderUtil.sendDeleteNotification(ordineClienteDTO.getIdentificativo());
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    //new OrdineClienteDmlSenderUtil(jmsProperties).sendDeleteNotification(ordineClienteDTO.getIdentificativo());
  }
}
