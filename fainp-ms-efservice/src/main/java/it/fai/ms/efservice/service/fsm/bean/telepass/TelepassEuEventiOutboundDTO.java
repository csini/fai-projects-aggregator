package it.fai.ms.efservice.service.fsm.bean.telepass;

import java.io.Serializable;

import it.fai.ms.common.jms.dto.telepass.TipoEventoOutbound;

/**
 * A DTO for the Telepass EU Outbound entity. Publish to XXX
 */
public class TelepassEuEventiOutboundDTO implements Serializable {

  private static final long serialVersionUID = -3624255297789284250L;

  private TipoEventoOutbound tipoEventoOutbound;
  private String             identificativoRichiesta;
  private String             identificativoOrdineCliente;
  private String             codiceClienteFai;

  public TelepassEuEventiOutboundDTO() {
  }

  public TelepassEuEventiOutboundDTO(TipoEventoOutbound tipoEventoOutbound, String identificativoRichiesta,
                                     String identificativoOrdineCliente, String codiceClienteFai) {
    this.tipoEventoOutbound = tipoEventoOutbound;
    this.identificativoRichiesta = identificativoRichiesta;
    this.identificativoOrdineCliente = identificativoOrdineCliente;
    this.setCodiceClienteFai(codiceClienteFai);
  }

  public TipoEventoOutbound getTipoEventoOutbound() {
    return tipoEventoOutbound;
  }

  public String getIdentificativoRichiesta() {
    return identificativoRichiesta;
  }

  public void setTipoEventoOutbound(TipoEventoOutbound tipoEventoOutbound) {
    this.tipoEventoOutbound = tipoEventoOutbound;
  }

  public void setIdentificativoRichiesta(String identificativoRichiesta) {
    this.identificativoRichiesta = identificativoRichiesta;
  }

  public TelepassEuEventiOutboundDTO tipoEventoOutbound(TipoEventoOutbound tipoEventoOutbound) {
    this.tipoEventoOutbound = tipoEventoOutbound;
    return this;
  }

  public TelepassEuEventiOutboundDTO identificativoRichiesta(String identificativoRichiesta) {
    this.identificativoRichiesta = identificativoRichiesta;
    return this;
  }

  public TelepassEuEventiOutboundDTO identificativoOrdineCliente(String identificativoOrdineCliente) {
    this.identificativoOrdineCliente = identificativoOrdineCliente;
    return this;
  }

  public String getIdentificativoOrdineCliente() {
    return identificativoOrdineCliente;
  }

  public void setIdentificativoOrdineCliente(String identificativoOrdineCliente) {
    this.identificativoOrdineCliente = identificativoOrdineCliente;
  }

  public TelepassEuEventiOutboundDTO codiceClienteFai(String codiceClienteFai) {
    this.codiceClienteFai = codiceClienteFai;
    return this;
  }

  public String getCodiceClienteFai() {
    return codiceClienteFai;
  }

  public void setCodiceClienteFai(String codiceClienteFai) {
    this.codiceClienteFai = codiceClienteFai;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codiceClienteFai == null) ? 0 : codiceClienteFai.hashCode());
    result = prime * result + ((identificativoOrdineCliente == null) ? 0 : identificativoOrdineCliente.hashCode());
    result = prime * result + ((identificativoRichiesta == null) ? 0 : identificativoRichiesta.hashCode());
    result = prime * result + ((tipoEventoOutbound == null) ? 0 : tipoEventoOutbound.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TelepassEuEventiOutboundDTO other = (TelepassEuEventiOutboundDTO) obj;
    if (codiceClienteFai == null) {
      if (other.codiceClienteFai != null)
        return false;
    } else if (!codiceClienteFai.equals(other.codiceClienteFai))
      return false;
    if (identificativoOrdineCliente == null) {
      if (other.identificativoOrdineCliente != null)
        return false;
    } else if (!identificativoOrdineCliente.equals(other.identificativoOrdineCliente))
      return false;
    if (identificativoRichiesta == null) {
      if (other.identificativoRichiesta != null)
        return false;
    } else if (!identificativoRichiesta.equals(other.identificativoRichiesta))
      return false;
    if (tipoEventoOutbound != other.tipoEventoOutbound)
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TelepassEuEventiOutboundDTO [");
    if (tipoEventoOutbound != null) {
      builder.append("tipoEventoOutbound=");
      builder.append(tipoEventoOutbound);
      builder.append(", ");
    }
    if (identificativoRichiesta != null) {
      builder.append("identificativoRichiesta=");
      builder.append(identificativoRichiesta);
      builder.append(", ");
    }
    if (identificativoOrdineCliente != null) {
      builder.append("identificativoOrdineCliente=");
      builder.append(identificativoOrdineCliente);
      builder.append(", ");
    }
    if (codiceClienteFai != null) {
      builder.append("codiceClienteFai=");
      builder.append(codiceClienteFai);
    }
    builder.append("]");
    return builder.toString();
  }

}
