package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.ProduttoreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Produttore and its DTO ProduttoreDTO.
 */
@Mapper(componentModel = "spring", uses = { TipoDispositivoMapper.class })
public interface ProduttoreMapper extends EntityMapper<ProduttoreDTO, Produttore> {

  @Mapping(source = "tipoDispositivos", target = "tipiDispositivo")
  ProduttoreDTO toDto(Produttore produttore);

  @Mapping(target = "contrattos", ignore = true)
  @Mapping(target = "tipoDispositivos", ignore = true)
  Produttore toEntity(ProduttoreDTO produttoreDTO);

  default Produttore fromId(Long id) {
    if (id == null) {
      return null;
    }
    Produttore produttore = new Produttore();
    produttore.setId(id);
    return produttore;
  }
}
