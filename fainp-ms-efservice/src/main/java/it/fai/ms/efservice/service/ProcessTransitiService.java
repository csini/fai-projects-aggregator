package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.*;

import it.fai.ms.common.jms.dto.TransitErrorDTO;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.TransitDTO;
import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.TrackyCardClient;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.dto.ResponseTessereFrejusDto;
import it.fai.ms.efservice.enumeration.TipiAnomalieTransitoEnum;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.dto.ConfermaTransitoDTO;
import it.fai.ms.efservice.service.dto.TransitoAnomaliaDTO;
import it.fai.ms.efservice.service.dto.TransitoRispostaDTO;
import it.fai.ms.efservice.service.frejus.ConfermaTesseraFrejus;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.web.rest.errors.Errno;

@Service
@Transactional
public class ProcessTransitiService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private static final String PREFIX_TOKEN = "Bearer ";

  private static final String OPERAZIONE_SPEDIZIONE_FAI = "MU_SPEDIZIONE_FAI";

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  private final RaggruppamentoRichiesteOrdineFornitoreServiceExt raggruppamentoService;
  private final TrackyCardClient trackyCardClient;
  private final DocumentClient documentClient;
  private final DispositivoServiceExt dispositivoServiceExt;
  private final DispositivoRepositoryExt dispositivoRepo;
  private final ClienteFaiRepository clienteFaiRepo;
  private final FsmRichiestaUtils fsmRichiestaUtils;

  private final ConfermaTesseraFrejus confermeTessereFrejus;

  public ProcessTransitiService(
    final RaggruppamentoRichiesteOrdineFornitoreServiceExt raggruppamentoService,
    final TrackyCardClient trackyCardClient, final DocumentClient documentClient,
    final DispositivoServiceExt dispositivoServiceExt, final DispositivoRepositoryExt dispositivoRepo,
    final ClienteFaiRepository clienteFaiRepo, final FsmRichiestaUtils fsmRichiestaUtils,ConfermaTesseraFrejus frejusClient
  ) {
    this.raggruppamentoService = raggruppamentoService;
    this.trackyCardClient = trackyCardClient;
    this.documentClient = documentClient;
    this.dispositivoServiceExt = dispositivoServiceExt;
    this.dispositivoRepo = dispositivoRepo;
    this.clienteFaiRepo = clienteFaiRepo;
    this.fsmRichiestaUtils = fsmRichiestaUtils;
    this.confermeTessereFrejus = frejusClient;
  }

  @Transactional
  public AsyncJobResponse process(ConfermaTransitoDTO confermaTransitoDTO) {
    log.debug("Processing TransitoDTO: {}", confermaTransitoDTO);
    List<String> seriali = new ArrayList<>();
    List<String> errori = new ArrayList<>();

    switch (confermaTransitoDTO.getModalitaCaricamento()) {
      case FILE_DA_FORNITORE:
        Set<Richiesta> richieste = raggruppamentoService.findAllByIdIn(confermaTransitoDTO.getIdSelezionati())
          .stream()
          .flatMap(raggruppamento -> raggruppamento.getRichiestas()
            .stream())
          .distinct()
          .collect(toSet());
        return processRichiesteRequest(richieste);
      case FILE_GENERICO:
        Pair<List<String>, List<String>> resultPair = processGenericFile(confermaTransitoDTO);
        seriali = resultPair.getFirst();
        errori = resultPair.getSecond();
        break;
      case FILE_CSV:
        seriali = parseSerialiFromCsv(confermaTransitoDTO.getUuidDocumento());
        break;
      case MANUALE:
        seriali = confermaTransitoDTO.getSeriali();
        break;
    }

    if (seriali == null || seriali.isEmpty()) {
      AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, "No serial found on input");
      response.add(Errno.TRANSIT_NO_SERIALS_FOUND_ON_INPUT);
      return response;
    }

    AsyncJobResponse asyncJobResponse = processSerialListRequest(seriali, errori, confermaTransitoDTO.getTipoDispositivo());
    return asyncJobResponse;
  }

  private Pair<List<String>, List<String>> processGenericFile(ConfermaTransitoDTO confermaTransitoDTO) {
    switch (confermaTransitoDTO.getTipoDispositivo()) {
      case TRACKYCARD:
        List<TransitDTO> transiti = trackyCardClient.trackyCardFileUpdate(
          jwt, confermaTransitoDTO.getUuidDocumento());
        log.debug("returning transiti from trackycard client {}", transiti);
        List<String> serialiFromTrackyCard = transiti.stream()
          //.filter(transitDTO -> transitDTO.getTransitError() != null && transitDTO.getTransitError().isPresent())
          .filter(transitDTO -> StringUtils.isNotEmpty(transitDTO.getSerialNumber()))
          .map(TransitDTO::getSerialNumber)
          .collect(toList());

        List<String> errorsFromTrackycard = transiti.stream()
          .filter(transitDTO -> transitDTO.getTransitError() != null && transitDTO.getTransitError().isPresent())
          .map(transitDTO -> transitDTO.getTransitError())
          .map(transitErrorDTO -> transitErrorDTO.get())
          .map(transitErrorDTO -> Optional.ofNullable(transitErrorDTO.getCode()).orElse("") + transitErrorDTO.getDescription())
        .collect(toList());

        log.debug("serials found from trackycard client response {}", serialiFromTrackyCard);
        return Pair.of(serialiFromTrackyCard, errorsFromTrackycard);
      case TES_TRA_FREJUS_MONTE_BIANCO:
        List<String> serialiFromFrejus  = confermeTessereFrejus.processConferme(confermaTransitoDTO);
        log.debug("serials found from frejus client response {}", serialiFromFrejus);
        return Pair.of(serialiFromFrejus, Collections.emptyList());
      default:
        return Pair.of(Collections.emptyList(), Collections.emptyList());
    }
  }

  private AsyncJobResponse processSerialListRequest(List<String> seriali, List<String> errori, TipoDispositivoEnum tipoDispositivo) {

    log.debug("Processecing seriali: {} with tipoDispositivo: {}", seriali, tipoDispositivo);
    List<Dispositivo> dispositivi = dispositivoRepo.findAllBySerialeInAndTipoDispositivo_nome(seriali, tipoDispositivo);

    List<String> serialiFound = dispositivi.stream()
      .map(Dispositivo::getSeriale)
      .collect(toList());
    log.debug("Serials found in the system: {}", serialiFound);
    List<String> serialiNotFound = seriali.stream()
      .filter(seriale -> !serialiFound.contains(seriale))
      .collect(toList());

    log.debug("Serials not found in the system: {}", serialiNotFound);

    dispositivi.forEach(dispositivo -> {
      Hibernate.initialize(dispositivo.getRichiestas());
    });

    List<String> serialiWithoutRichiesta = dispositivi.stream()
      .filter(dispositivo -> dispositivo.getRichiestas()
        .stream()
        .anyMatch(richiesta -> !StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE.equals(richiesta.getStato())))
      .map(Dispositivo::getSeriale)
      .collect(toList());

    log.debug("Serials without requests: {}", serialiWithoutRichiesta);

    Map<StatoRichiesta, List<Richiesta>> richiesteGroupped = dispositivi.stream()
      .flatMap(dispositivo -> dispositivo.getRichiestas()
        .stream())
      .collect(groupingBy(Richiesta::getStato));

    Set<String> richiesteInAttesaFornitore = Optional.ofNullable(richiesteGroupped.get(
      StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE)
    )
      .orElse(Collections.emptyList())
      .stream()
      .map(Richiesta::getIdentificativo)
      .collect(toSet());

    log.debug("Serials with state {} found: {}",
      StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE,
      richiesteInAttesaFornitore
    );

    StateChangeStatusFsm stateChangeResponse = changeRichiesteStatus(richiesteInAttesaFornitore, OPERAZIONE_SPEDIZIONE_FAI);

    log.debug("Response from changeRichiesteStatus: {}", stateChangeResponse);

    // TODO ccambiare stato di tutti i dispositivi con anomalie a SOSPESO_TRANSITO

    if (StateChangeStatusFsm.ERROR.equals(stateChangeResponse)) {
      AsyncJobResponse response = new AsyncJobResponse(stateChangeResponse, null);
      response.add(Errno.EXCEPTION_ON_PROCESSING_TRANSIT);
      return response;
    }

    TransitoRispostaDTO dto = new TransitoRispostaDTO();
    dto.setNumDispositiviAbbinati((long) serialiFound.size());
    long numDispDaAbbinare = seriali.size() - serialiFound.size();
    dto.setNumDispositiviDaAbbinare(numDispDaAbbinare);

    List<TransitoAnomaliaDTO> anomalie = new ArrayList<>();

    serialiNotFound.forEach(seriale -> {
      TransitoAnomaliaDTO anomaliaDTO = new TransitoAnomaliaDTO();
      anomaliaDTO.setSerialeDispositivo(seriale);
      anomaliaDTO.setTipoAnomalia(TipiAnomalieTransitoEnum.SERIALE_NON_PRESENTE_IN_SISTEMA);
      anomaliaDTO.setTipoDispositivo(tipoDispositivo.name());
      anomalie.add(anomaliaDTO);
    });

    serialiWithoutRichiesta.forEach(seriale -> {
      TransitoAnomaliaDTO anomaliaDTO = new TransitoAnomaliaDTO();
      anomaliaDTO.setSerialeDispositivo(seriale);
      anomaliaDTO.setTipoAnomalia(TipiAnomalieTransitoEnum.SERIALE_CON_STATO_INVALIDO);
      anomaliaDTO.setTipoDispositivo(tipoDispositivo.name());

      Optional.ofNullable(dispositivoServiceExt.findVehicleBySerialeDispositivo(seriale))
        .ifPresent(vehicle -> {
          anomaliaDTO.setClasseEuro(vehicle.getEuroClass());
          anomaliaDTO.setTargaVeicolo(vehicle.getLicensePlate());
        });

      clienteFaiRepo.findBySerialeDispositivo(seriale)
        .stream()
        .findFirst()
        .ifPresent(clienteFai -> {
          anomaliaDTO.setCodiceAzienda(clienteFai.getCodiceCliente());
          anomaliaDTO.setRagioneSociale(clienteFai.getRagioneSociale());
        });

      anomalie.add(anomaliaDTO);
    });

    errori.forEach(errore -> {
      TransitoAnomaliaDTO anomaliaDTO = new TransitoAnomaliaDTO();
      anomaliaDTO.setSerialeDispositivo("ND");
      anomaliaDTO.setTipoAnomalia(TipiAnomalieTransitoEnum.ERRORE_GENERICO);
      anomaliaDTO.setTipoDispositivo(tipoDispositivo.name());
      anomaliaDTO.setMessaggioErrore(errore);
      anomalie.add(anomaliaDTO);
    });

    dto.setAnomalie(anomalie);

    log.debug("Responding with TransitDTO: {}", dto);

    return new AsyncJobResponse(stateChangeResponse, dto);
  }

  private AsyncJobResponse processRichiesteRequest(Set<Richiesta> richieste) {
    Set<String> identificativiRichieste = richieste.stream()
      .filter(richiesta -> StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE.equals(richiesta.getStato()))
      .map(Richiesta::getIdentificativo)
      .collect(toSet());

    StateChangeStatusFsm stateChangeResponse = changeRichiesteStatus(identificativiRichieste, OPERAZIONE_SPEDIZIONE_FAI);

    TransitoRispostaDTO dto = new TransitoRispostaDTO();
    dto.setNumDispositiviAbbinati((long) identificativiRichieste.size());

    long numDispDaAbbinare = richieste.size() - identificativiRichieste.size();

    dto.setNumDispositiviDaAbbinare(numDispDaAbbinare);

    return new AsyncJobResponse(stateChangeResponse, dto);
  }

  public StateChangeStatusFsm changeRichiesteStatus(Set<String> identificativiRichieste, String tipoOperazione) {
    StateChangeStatusFsm state;

    if(identificativiRichieste.isEmpty()){
      log.debug("changeRichiesteStatus - nessun seriale trovato");
      return StateChangeStatusFsm.SUCCESS;
    }

    try {
      boolean esito = fsmRichiestaUtils.changeStatusRichieste(
        identificativiRichieste,
        tipoOperazione,
        null,
        null
      );
      if (esito) {
        state = StateChangeStatusFsm.SUCCESS;
      } else {
        state = StateChangeStatusFsm.ERROR;
      }
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
    return state;
  }

  private List<String> parseSerialiFromCsv(String uuidDocument) {
    return documentClient.getColumnListFromFileCsv(jwt, uuidDocument, 0);
  }

  public AsyncJobResponse processFrejus(ResponseTessereFrejusDto data, String paese) {
    String seriale =  confermeTessereFrejus.processConferma(data,paese);
    return processSerialListRequest(Arrays.asList(seriale), Collections.emptyList(), TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO);
  }

}
