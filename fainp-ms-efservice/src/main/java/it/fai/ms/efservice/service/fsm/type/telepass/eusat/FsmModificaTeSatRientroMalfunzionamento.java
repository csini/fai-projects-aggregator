package it.fai.ms.efservice.service.fsm.type.telepass.eusat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.eusat.FsmModificaTeSatRientroMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTeSatRientroMalfunzionamento.FSM_MOD_TE_SAT_RIENTRO_MALF)
public class FsmModificaTeSatRientroMalfunzionamento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TE_SAT_RIENTRO_MALF = "fsmModificaTeSatRientroMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTeSatRientroMalfunzionamento(@Qualifier(FsmModificaTeSatRientroMalfunzionamentoConfig.FSM_MOD_TE_SAT_RIENTRO_MALF) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                                 FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TE_SAT_RIENTRO_MALFUNZIONAMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_EUROPEO_SAT };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
