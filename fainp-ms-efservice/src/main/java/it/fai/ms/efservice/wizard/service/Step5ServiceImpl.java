package it.fai.ms.efservice.wizard.service;

import static it.fai.ms.efservice.wizard.model.ActivabilityState.ACTIVABLE;
import static it.fai.ms.efservice.wizard.model.ActivabilityState.ACTIVE;
import static it.fai.ms.efservice.wizard.model.WizardVehicleId.mapToWizardVehicleIds;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardDevice;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardService;
import it.fai.ms.efservice.wizard.model.WizardServiceType;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5MatrixServiceType;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5MatrixVehicle;
import it.fai.ms.efservice.wizard.repository.WizardDeviceRepository;
import it.fai.ms.efservice.wizard.repository.WizardDeviceTypeRepository;
import it.fai.ms.efservice.wizard.repository.WizardServiceTypeRepository;
import it.fai.ms.efservice.wizard.repository.WizardVehicleRepository;

@Service
public class Step5ServiceImpl implements Step5Service {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private WizardVehicleRepository     vehicleRepository;
  private WizardDeviceRepository      wizardDeviceRepository;
  private WizardDeviceTypeRepository  wizardDeviceTypeRepository;
  private WizardServiceTypeRepository wizardServiceTypeRepository;
  private PrecalculatedRuleService    ruleService;

  @Autowired
  public Step5ServiceImpl(final WizardServiceTypeRepository _wizardServiceTypeRepository,
                          final WizardDeviceRepository _wizardDeviceRepository,
                          final WizardDeviceTypeRepository _wizardDeviceTypeRepository, final WizardVehicleRepository _vehicleRepository,
                          final PrecalculatedRuleService _ruleService) {
    wizardServiceTypeRepository = _wizardServiceTypeRepository;
    wizardDeviceRepository = _wizardDeviceRepository;
    wizardDeviceTypeRepository = _wizardDeviceTypeRepository;
    vehicleRepository = _vehicleRepository;
    ruleService = _ruleService;
  }

  @Override
  @Transactional(readOnly = true)
  public List<WizardStep5Matrix> buildMatrixList(final Set<Map<String, Set<String>>> _deviceTypesMaps) {
    return _deviceTypesMaps.parallelStream()
                           .map(deviceTypesMap -> deviceTypesMap.entrySet()
                                                                .stream()
                                                                .sorted()
                                                                .map(entry -> this.buildMatrix(new WizardDeviceTypeId(entry.getKey()),
                                                                                               mapToWizardVehicleIds(entry.getValue())))
                                                                .filter(wizardStep5Matrix -> wizardStep5Matrix.hasVehicles())
                                                                .collect(toList()))
                           .flatMap(List::stream)
                           .collect(toList());
  }

  private WizardStep5Matrix buildMatrix(final WizardDeviceTypeId _wizardDeviceTypeId, final Set<WizardVehicleId> _wizardVehicleIds) {

    final long time1 = System.currentTimeMillis();

    if (_log.isTraceEnabled()) {
      _log.trace("Building step5WizardMatrix for deviceType Id {} and vehicle ids:", _wizardDeviceTypeId.getId());
      _wizardVehicleIds.forEach(vehicleId -> _log.debug("  {}", vehicleId.getId()));
    }

    final WizardStep5Matrix wizardStep5Matrix = new WizardStep5Matrix(_wizardDeviceTypeId.getId());
    final Optional<WizardDeviceType> wizardDeviceType = wizardDeviceTypeRepository.findByDeviceTypeId(_wizardDeviceTypeId);
    if (wizardDeviceType.isPresent()) {
      WizardDeviceType wDeviceType = wizardDeviceType.get();
      wizardStep5Matrix.setMultiService(wDeviceType.isMultiService());
      wizardStep5Matrix.setIsToBeSent(wDeviceType.isToBeSent());
      final Set<WizardServiceType> serviceTypes = wizardServiceTypeRepository.findAllByDeviceTypeId(_wizardDeviceTypeId);
      if (_log.isTraceEnabled()) {
        _log.trace("Building step5WizardMatrix, found serviceTypes for deviceType Id {}", _wizardDeviceTypeId.getId());
        serviceTypes.forEach(serviceType -> _log.debug("  {}", serviceType));
      }

      Set<WizardStep5MatrixVehicle> setWizardStep5Vehicle = _wizardVehicleIds.parallelStream()
                                                                             .map(wizardVehicleId -> vehicleRepository.find(wizardVehicleId.getId()))
                                                                             .filter(optWizardVehicle -> optWizardVehicle.isPresent())
                                                                             .map(opt -> opt.get())
                                                                             .map(wizardVehicle -> {
                                                                               final Map<String, WizardStep5MatrixServiceType> serviceTypesMap = serviceTypes.stream()
                                                                                                                                                             .collect(Collectors.toMap(wizardServiceType -> wizardServiceType.getId()
                                                                                                                                                                                                                             .getId(),
                                                                                                                                                                                       wizardServiceType -> newStep5WizardMatrixServiceType(wizardServiceType.getId(),
                                                                                                                                                                                                                                            wizardServiceType.getOrdering(),
                                                                                                                                                                                                                                            _wizardDeviceTypeId,
                                                                                                                                                                                                                                            wizardVehicle.getId())));
                                                                               return mapToStep5WizardMatrixVehicle(wizardVehicle,
                                                                                                                    serviceTypesMap);
                                                                             })
                                                                             .collect(toSet());

      wizardStep5Matrix.getVehicles()
                       .addAll(setWizardStep5Vehicle);
    }
    final long time2 = System.currentTimeMillis();

    _log.debug("State for WizardDeviceTypeId {} and WizardVehicleIds {}: {}", _wizardDeviceTypeId, _wizardVehicleIds, wizardStep5Matrix);
    _log.debug("Built step5WizardMatrix for deviceTypeId {} [tot time {} ms]", _wizardDeviceTypeId, time2 - time1);

    return wizardStep5Matrix;
  }

  private boolean isTheSelectedDevice(final WizardDeviceTypeId _wizardDeviceTypeId, final WizardDevice _wizardDevice) {
    boolean equals = _wizardDevice.isOfTypeId(_wizardDeviceTypeId);
    _log.debug("Is the selected device? {} Device {} {} for step5", equals, _wizardDevice, equals ? "enabled" : "disabled");
    return equals;
  }

  private WizardStep5MatrixVehicle mapToStep5WizardMatrixVehicle(final WizardVehicle wizardVehicle,
                                                                 final Map<String, WizardStep5MatrixServiceType> _wizardMatrixServiceTypesMap) {
    final WizardStep5MatrixVehicle vehicle = new WizardStep5MatrixVehicle(wizardVehicle.getId()
                                                                                       .getId());
    vehicle.setType(wizardVehicle.getType());
    vehicle.setEuroClass(wizardVehicle.getEuroClass());
    vehicle.setLicensePlate(wizardVehicle.getLicensePlate());
    vehicle.setTargaNazione(wizardVehicle.getCountry());
    vehicle.getServiceTypes()
           .putAll(_wizardMatrixServiceTypesMap);
    return vehicle;
  }

  private WizardStep5MatrixServiceType newStep5WizardMatrixServiceType(final WizardServiceTypeId _wizardServiceTypeId,
                                                                       final long serviceTypeOrdering,
                                                                       final WizardDeviceTypeId _wizardDeviceTypeId,
                                                                       final WizardVehicleId _wizardVehicleId) {
    final WizardStep5MatrixServiceType wizardStep5MatrixServiceType = new WizardStep5MatrixServiceType(_wizardServiceTypeId.getId(),
                                                                                                       new WizardMatrixActivability(ACTIVABLE),
                                                                                                       serviceTypeOrdering);

    final Set<WizardDevice> wizardDevices = wizardDeviceRepository.findByWizardVehicleId(_wizardVehicleId);
    final Optional<WizardDevice> optionalWizardDevice = wizardDevices.stream()
                                                                     .filter(wizardDevice -> isTheSelectedDevice(_wizardDeviceTypeId,
                                                                                                                 wizardDevice))
                                                                     .findFirst();
    if (optionalWizardDevice.isPresent()) {
      final Optional<WizardService> optionalWizardService = optionalWizardDevice.get()
                                                                                .getServices()
                                                                                .values()
                                                                                .stream()
                                                                                .filter(wizardService -> wizardService.isOfTypeId(_wizardServiceTypeId))
                                                                                .findFirst();
      if (optionalWizardService.isPresent()) {
        if (optionalWizardService.get()
                                 .isActive()) {
          wizardStep5MatrixServiceType.setActivability(new WizardMatrixActivability(ACTIVE));
        }
      }
    }

    Optional<RuleOutcome> ruleOpt = executeRule(_wizardServiceTypeId, _wizardVehicleId, _wizardDeviceTypeId);
    if (ruleOpt.isPresent()) {
      _log.debug("rule present");
      RuleOutcome ruleOutcome = ruleOpt.get();
      if (!ruleOutcome.getOutcome()) {
        ActivabilityState state = ActivabilityState.ANOMALY;
        WizardMatrixActivability wizardMatrixActivability = new WizardMatrixActivability(state);
        wizardStep5MatrixServiceType.setActivability(wizardMatrixActivability);
        Optional<RuleFailure> failure = ruleOutcome.getFailure();
        if (failure.isPresent()) {
          RuleFailure ruleFailure = failure.get();
          String codice = ruleFailure.getCode();
          Number numCode = ruleFailure.decode();
          _log.info("Decode codice [{}] to: {}", codice, numCode);
          WizardMatrixActivabilityFailure matrixActivabilityFailure = new WizardMatrixActivabilityFailure(numCode, ruleFailure.getMess());
          wizardMatrixActivability.addFailure(matrixActivabilityFailure);
        }

        _log.debug("Set {} for RuleOutcome: {}", state, ruleOutcome);
      }
    }

    return wizardStep5MatrixServiceType;
  }

  private Optional<RuleOutcome> executeRule(final WizardServiceTypeId _wizardServiceTypeId, final WizardVehicleId _wizardVehicleId,
                                            final WizardDeviceTypeId _wizardDeviceTypeId) {
    Optional<RuleOutcome> ruleOutcome = null;
    _log.debug("Execute rule for: WizardServiceType: {} - WizardVehicleId: {} - WizardDeviceTypeId: {}", _wizardServiceTypeId,
               _wizardVehicleId, _wizardDeviceTypeId);
    if (_wizardServiceTypeId == null) {
      ruleOutcome = ruleService.execute(RuleEngineDeviceType.as(_wizardDeviceTypeId.getId())
                                                            .getId(),
                                        RuleEngineVehicle.as(_wizardVehicleId.getId())
                                                         .getId());
    } else {
      ruleOutcome = ruleService.execute(RuleEngineServiceType.as(_wizardServiceTypeId.getId())
                                                             .getId(),
                                        RuleEngineDeviceType.as(_wizardDeviceTypeId.getId())
                                                            .getId(),
                                        RuleEngineVehicle.as(_wizardVehicleId.getId())
                                                         .getId());

    }
    return ruleOutcome;
  }

}
