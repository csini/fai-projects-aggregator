package it.fai.ms.efservice.wizard.model.matrix.step5;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class WizardStep5Matrix implements Serializable {

  private static final long serialVersionUID = -1540734721040369791L;

  private String                        deviceTypeId;
  private boolean                       multiService;
  private Boolean                       isToBeSent;
  private Set<WizardStep5MatrixVehicle> vehicles = new LinkedHashSet<>();

  public WizardStep5Matrix(final String _deviceTypeId) {
    deviceTypeId = _deviceTypeId;
  }

  public void addVehicle(final WizardStep5MatrixVehicle _wizardStep5MatrixVehicle) {
    Optional.ofNullable(_wizardStep5MatrixVehicle)
            .ifPresent(vehicle -> vehicles.add(vehicle));
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep5Matrix) _obj).getDeviceTypeId(), deviceTypeId)
                 && Objects.equals(((WizardStep5Matrix) _obj).getVehicles(), vehicles);
    }
    return isEquals;
  }

  public String getDeviceTypeId() {
    return deviceTypeId;
  }

  public Set<WizardStep5MatrixVehicle> getVehicles() {
    return vehicles;
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceTypeId, getVehicles());
  }

  public boolean hasVehicles() {
    return !vehicles.isEmpty();
  }

  public boolean isMultiService() {
    return multiService;
  }

  public void setMultiService(boolean _multiService) {
    multiService = _multiService;
  }

  public Boolean isToBeSent() {
    return isToBeSent;
  }

  public void setIsToBeSent(Boolean isToBeSent) {
    this.isToBeSent = isToBeSent;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardStep5Matrix [deviceTypeId=");
    builder.append(deviceTypeId);
    builder.append(",multiService=");
    builder.append(multiService);
    builder.append(",isToBeSent=");
    builder.append(isToBeSent);
    builder.append(",vehicles=");
    builder.append(vehicles);
    builder.append("]");
    return builder.toString();
  }

}
