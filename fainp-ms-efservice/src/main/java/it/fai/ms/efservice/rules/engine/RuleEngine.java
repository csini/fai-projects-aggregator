package it.fai.ms.efservice.rules.engine;

import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public interface RuleEngine {

  RuleOutcome executeRule();

}
