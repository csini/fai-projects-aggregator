package it.fai.ms.efservice.rules.web.rest;

import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.RulePrecalculator;
import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;
import it.fai.ms.efservice.rules.web.rest.vm.ResourceNotFoundException;

@RestController
@RequestMapping("/api/rule")
public class RuleController {

  private PrecalculatedRuleService precalculatedRuleService;
  private RulePrecalculator        rulePrecalculator;

  public RuleController(final PrecalculatedRuleService _precalculatedRuleService, final RulePrecalculator _rulePrecalculator) {
    precalculatedRuleService = _precalculatedRuleService;
    rulePrecalculator = _rulePrecalculator;
  }

  @GetMapping("{serviceTypeId}/{deviceTypeId}/{vehicleId}")
  @Timed
  public RuleOutcome getRuleOutcome(@PathVariable String serviceTypeId, @PathVariable String deviceTypeId, @PathVariable String vehicleId) {
    Optional<RuleOutcome> ruleOutcome = precalculatedRuleService.execute(new RuleEngineServiceTypeId(serviceTypeId),
                                                                         new RuleEngineDeviceTypeId(deviceTypeId),
                                                                         new RuleEngineVehicleId(vehicleId));

    return ruleOutcome.orElseThrow(() -> new ResourceNotFoundException("Precalculated rule not found"));
  }

  @PostMapping("calculate/{_serviceTypeId}/{_deviceType}/{_vehicleId}")
  @Timed
  public RuleOutcome calculateRuleOutcome(@PathVariable String _serviceTypeId, @PathVariable RuleBucket _deviceType,
                                          @PathVariable String _vehicleId) {

    RuleEngineServiceTypeId serviceTypeId = new RuleEngineServiceTypeId(_serviceTypeId);
    RuleEngineServiceType _serviceTypeRuleContext = new RuleEngineServiceType(serviceTypeId);
    RuleEngineDeviceTypeId deviceTypeId = new RuleEngineDeviceTypeId(_deviceType.deviceType());
    RuleEngineDeviceType _deviceTypeRuleContext = new RuleEngineDeviceType(deviceTypeId);
    RuleEngineVehicleId vehicleId = new RuleEngineVehicleId(_vehicleId);
    RuleEngineVehicle _vehicleRuleContext = new RuleEngineVehicle(vehicleId);

    RuleContext ruleContext = new RuleContext(_serviceTypeRuleContext, _deviceTypeRuleContext, _vehicleRuleContext);
    rulePrecalculator.process(ruleContext, _deviceType);

    Optional<RuleOutcome> ruleOutcome = precalculatedRuleService.execute(serviceTypeId, deviceTypeId, vehicleId);
    return ruleOutcome.orElseThrow(() -> new ResourceNotFoundException("Precalculated rule not found"));
  }

}
