package it.fai.ms.efservice.service.fsm.bean.telepass.sat;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaTSatVarTarga")
public class FsmModificaTSatVarTargaTransitionBean extends AbstractFsmRichiestaTransition {
}
