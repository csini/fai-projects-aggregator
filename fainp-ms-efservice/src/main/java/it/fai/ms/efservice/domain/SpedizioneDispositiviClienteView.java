package it.fai.ms.efservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_spedizioni_dispositivi_clienti")
public class SpedizioneDispositiviClienteView {
  @Id
  private Long id;

  @Column(name = "codice_cliente")
  private String codiceAzienda;

  @Column(name = "ragione_sociale")
  private String ragioneSociale;

  @Column(name = "numero_dispositivi_spedibili")
  private Long numeroDispositiviSpedibili = 0L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SpedizioneDispositiviClienteView [id=");
    builder.append(id);
    builder.append(", codiceAzienda=");
    builder.append(codiceAzienda);
    builder.append(", ragioneSociale=");
    builder.append(ragioneSociale);
    builder.append(", numeroDispositiviSpedibili=");
    builder.append(numeroDispositiviSpedibili);
    builder.append("]");
    return builder.toString();
  }

}
