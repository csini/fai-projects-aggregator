package it.fai.ms.efservice.web.rest;

import java.util.Comparator;

import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;

final class FieldOrdinamentoComparator implements Comparator<CategoriaServizioDTO> {

	FieldOrdinamentoComparator() {
	}

	@Override
	public int compare(CategoriaServizioDTO categoriaServizio1, CategoriaServizioDTO categoriaServizio2) {
		return categoriaServizio1.getOrdinamento().compareTo(categoriaServizio2.getOrdinamento());
	}
}