package it.fai.ms.efservice.wizard.web.rest.vm;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class MatrixActivabilityFailure {

  private Number code;
  private String mess;

  public MatrixActivabilityFailure(final Number _code, final String _mess) {
    code = _code;
    mess = _mess;
  }

  public Number getCode() {
    return code;
  }

  public String getMess() {
    return mess;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixActivabilityFailure [code=");
    builder.append(this.code);
    builder.append(", mess=");
    builder.append(this.mess);
    builder.append("]");
    return builder.toString();
  }

}
