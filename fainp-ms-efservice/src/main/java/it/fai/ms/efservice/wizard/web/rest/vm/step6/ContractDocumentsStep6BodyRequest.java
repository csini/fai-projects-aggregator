package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;
import java.util.List;

public class ContractDocumentsStep6BodyRequest implements Serializable {
  private static final long serialVersionUID = 6036609154785959431L;

  private List<ContractStep6> contracts;

  public ContractDocumentsStep6BodyRequest() {
  }

  public List<ContractStep6> getContracts() {
    return contracts;
  }

  public void setContracts(List<ContractStep6> contracts) {
    this.contracts = contracts;
  }

  @Override
  public String toString() {
    return "ContractDocumentsStep6BodyRequest [contracts=" + contracts + "]";
  }
}
