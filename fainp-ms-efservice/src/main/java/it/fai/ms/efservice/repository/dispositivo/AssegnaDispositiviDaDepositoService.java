package it.fai.ms.efservice.repository.dispositivo;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.efservice.DispositivoDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Dispositivo_;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo_;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.TipoServizio_;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.web.rest.ContrattoResourceExt;

@Service
@Transactional
public class AssegnaDispositiviDaDepositoService {
	// private static final TransactionDefinition REQUIRES_NEW_TRANSACTION = new
	// DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	// private PlatformTransactionManager transactionManager;
	private final DispositivoRepository dispositivoRepo;
	private final StatoDispositivoServizioRepository servizioRepo;
	private final TipoServizioRepository tipoServiziRepo;
	private final ContrattoRepositoryExt contrattoRepo;
	private final ClienteFaiRepository clienteFaiRepo;
	private final ContrattoResourceExt contrattoResourceExt;

	public AssegnaDispositiviDaDepositoService(
			/* PlatformTransactionManager transactionManager, */ DispositivoRepository dispositivoRepo,
			StatoDispositivoServizioRepository servizioRepo, TipoServizioRepository tipoServiziRepo,
			ContrattoRepositoryExt contrattoRepo, ClienteFaiRepository clienteFaiRepository,ContrattoResourceExt contrattoResourceExt) {
		// this.transactionManager = transactionManager;
		this.dispositivoRepo = dispositivoRepo;
		this.servizioRepo = servizioRepo;
		this.tipoServiziRepo = tipoServiziRepo;
		this.contrattoRepo = contrattoRepo;
		this.clienteFaiRepo = clienteFaiRepository;
		this.contrattoResourceExt = contrattoResourceExt;
	}

	public List<DispositivoDTO> assegnaDispositivi(TipoDispositivoAssegnabile deviceType, int quantity,
			String idContratto) {
		return assegnaDispositiviInternal(deviceType, quantity, idContratto).stream().map(d -> {
			DispositivoDTO dto = new DispositivoDTO();
			dto.setContratto(d.getContratto().getCodContrattoCliente());
			if(d.getDataScadenza()!=null) {
			  dto.setDataScadenza(d.getDataScadenza().toInstant());
			}
			dto.setIdentificativo(d.getIdentificativo());
			dto.setSeriale(d.getSeriale());
			dto.setTipo(d.getTipoDispositivo().getNome().name());
			dto.setTipoHardware(d.getTipoHardware());
			return dto;
		}).collect(Collectors.toList());
	}

	List<Dispositivo> assegnaDispositiviInternal(TipoDispositivoAssegnabile deviceType, int quantity,
			String idContratto) {
		Contratto contratto = contrattoRepo.findFirstByCodContrattoCliente(idContratto);
		List<Dispositivo> orderedDevices = new ArrayList<>(quantity);
		for (int i = 0; i < quantity; i++) {
			// new TransactionTemplate(transactionManager,
			// REQUIRES_NEW_TRANSACTION).execute(status -> {
			Optional<Dispositivo> dispositivo = findDeviceFromDeposito(deviceType.getTipoDispositivo(),
					deviceType.getTipoHardware());
			dispositivo.ifPresent(disp -> {

				if (disp.getStatoDispositivoServizios().isEmpty()) {
					createStatoServizio(disp);
				} else {
				  updateStatoServizioDates(disp);
        }

				assegnaDispositivo(disp, contratto);

				disp.setStato(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
				disp.setTipoMagazzino(TipoMagazzino.SCORTA);
				orderedDevices.add(disp);

				dispositivoRepo.save(disp);
				contrattoRepo.save(contratto);
			});

			// return null;
			// });
		}
		return orderedDevices;
	}

	private void assegnaDispositivo(Dispositivo dispositivo, Contratto contratto) {
		dispositivo.setContratto(contratto);
		HashSet<Dispositivo> set = new HashSet<>(1);
		set.add(dispositivo);
		contratto.setDispositivo(set);

	}

	private void createStatoServizio(Dispositivo dispositivo) {
		StatoDS stato = StatoDS.ATTIVO;
		List<TipoServizio> statiServizio = findTipoServizi(dispositivo.getTipoDispositivo());
		for (TipoServizio statoDispositivoServizio : statiServizio) {
			String pan = null;
			StatoDispositivoServizio servizio = new StatoDispositivoServizio().dataAttivazione(Instant.now())
					.dataScadenza(dispositivo.getDataScadenza().toInstant()).dispositivo(dispositivo)
					.tipoServizio(statoDispositivoServizio).stato(stato).pan(pan);
			dispositivo.getStatoDispositivoServizios().add(servizio);
			servizioRepo.save(servizio);
		}
	}

  private void updateStatoServizioDates(Dispositivo dispositivo) {
    StatoDS stato = StatoDS.ATTIVO;
    for (StatoDispositivoServizio statoDispositivoServizio : dispositivo.getStatoDispositivoServizios()) {
      if (statoDispositivoServizio.getDataAttivazione() == null) {
        statoDispositivoServizio.dataAttivazione(Instant.now());
      }
      servizioRepo.save(statoDispositivoServizio);
    }
  }


  private List<TipoServizio> findTipoServizi(TipoDispositivo dispositivo) {
		return tipoServiziRepo.findAll((Specification<TipoServizio>) (root, query, cb) -> cb
				.and(cb.isMember(dispositivo, root.get(TipoServizio_.tipoDispositivos))));
	}

	private Optional<Dispositivo> findDeviceFromDeposito(TipoDispositivoEnum deviceType, String tipoHardware) {
		List<Dispositivo> devices = dispositivoRepo.findAll(
				Specifications.where(dispositiviInDeposito(deviceType, tipoHardware)),
				new Sort(Direction.ASC, Dispositivo_.dataScadenza.getName()));
		if (devices.isEmpty())
			return Optional.empty();
		return Optional.of(devices.iterator().next());
	}

	private Optional<Dispositivo> findDeviceFromDepositoBySeriale(TipoDispositivoEnum deviceType, String seriale) {
		List<Dispositivo> devices = dispositivoRepo.findAll(
				Specifications.where(dispositiviInDepositoBySeriale(deviceType, seriale)),
				new Sort(Direction.ASC, Dispositivo_.dataScadenza.getName()));
		if (devices.isEmpty())
			return Optional.empty();
		return Optional.of(devices.iterator().next());
	}

	public Long countDeviceInDeposito(TipoDispositivoAssegnabile deviceType) {
		return dispositivoRepo
				.count(dispositiviInDeposito(deviceType.getTipoDispositivo(), deviceType.getTipoHardware()));
	}

	private Specification<Dispositivo> dispositiviInDeposito(TipoDispositivoEnum deviceType, String tipoHardware) {
		return (root, query, cb) -> {
			ZonedDateTime dateToCheck = ZonedDateTime.now().plus(1, ChronoUnit.DAYS);

			Predicate and = cb.and(cb.equal(root.get(Dispositivo_.stato), StatoDispositivo.IN_DEPOSITO),
					cb.equal(root.get(Dispositivo_.tipoDispositivo).get(TipoDispositivo_.nome), deviceType),
					cb.or(cb.isNull(root.get(Dispositivo_.dataScadenza)),
							cb.greaterThan(root.get(Dispositivo_.dataScadenza), dateToCheck)));
			if (tipoHardware != null) {
				and = cb.and(and, cb.equal(root.get(Dispositivo_.tipoHardware), tipoHardware));
			}
			return and;
		};
	}

	private Specification<Dispositivo> dispositiviInDepositoBySeriale(TipoDispositivoEnum deviceType, String seriale) {
		return (root, query, cb) -> {
			ZonedDateTime dateToCheck = ZonedDateTime.now().plus(1, ChronoUnit.DAYS);

			Predicate and = cb.and(cb.equal(root.get(Dispositivo_.stato), StatoDispositivo.IN_DEPOSITO),
					cb.equal(root.get(Dispositivo_.tipoDispositivo).get(TipoDispositivo_.nome), deviceType),
					cb.or(cb.isNull(root.get(Dispositivo_.dataScadenza)),
							cb.greaterThan(root.get(Dispositivo_.dataScadenza), dateToCheck)));
				and = cb.and(and, cb.equal(root.get(Dispositivo_.seriale), seriale));
			return and;
		};
	}

	public String assegnaDispositivi(TipoDispositivo tipoDispositivo, String serialeDispositivo, String codiceCliente) {
		if(tipoDispositivo == null) {
			return "tipoDispositivo non può essere null";
		}else if(StringUtils.isBlank(serialeDispositivo)) {
			return "serialeDispositivo non può essere blank";
		}else if(StringUtils.isBlank(codiceCliente)) {
			return "codiceCliente non può essere blank";
		}

		//cerco cliente
		ClienteFai clienteFai = clienteFaiRepo.findOneByCodiceCliente(codiceCliente);
		if(clienteFai == null) {
          return "cliente " + codiceCliente + "non trovato";
		}

		// cerco Produttore by Tipodispositivo
		Produttore produttore = tipoDispositivo.getProduttore();

		//cerco contratto
		List<Contratto>         contrattos      = contrattoRepo.findByProduttoreAndClienteFai(produttore,clienteFai);
		Contratto contratto = contrattos.stream().filter(c -> c.isPrimario()).findFirst().orElse(contrattos.size()>0 ? contrattos.get(0) : null);
		// se non esiste contratto, creo contratto
		if(contratto == null) {
			String contractNumber = clienteFai.getCodiceCliente() + "_" + produttore.getNome();
			// creo contratto ..
			contratto = contrattoResourceExt.createContratto( clienteFai, produttore, contractNumber,StatoContratto.ATTIVO, Instant.now(), null);
		}

		//assegno dispositivo a cliente
      Optional<Dispositivo> dispositivo = findDeviceFromDepositoBySeriale(tipoDispositivo.getNome(), serialeDispositivo);
      if(dispositivo.isPresent()) {
    	  Dispositivo disp = dispositivo.get();

    	if(disp.getContratto()!=null) {
    		return "dispositivo "+tipoDispositivo.getNome()+" con seriale "+serialeDispositivo+" già assegnato al contratto "+disp.getContratto().getCodContrattoCliente();
    	}

        if (disp.getStatoDispositivoServizios().isEmpty()) {
          createStatoServizio(disp);
        } else {
          updateStatoServizioDates(disp);
        }

        assegnaDispositivo(disp, contratto);

        disp.setStato(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
        disp.setTipoMagazzino(TipoMagazzino.SCORTA);

        dispositivoRepo.save(disp);
        contrattoRepo.save(contratto);
      }else{
  		return "nessun dispositivo "+tipoDispositivo.getNome()+" con seriale "+serialeDispositivo+" trovato in deposito";
      };

	  return null;
	}
}
