package it.fai.ms.efservice.rules.web.rest;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.wizard.web.rest.vm.ResourceNotFoundException;

@RestController
@RequestMapping("/api/rule/repository")
public class RuleOutcomeRepositoryController {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private RuleOutcomeRepository ruleOutcomeRepository;

  public RuleOutcomeRepositoryController(final RuleOutcomeRepository _ruleOutcomeRepository) {
    ruleOutcomeRepository = _ruleOutcomeRepository;
  }

  @GetMapping("keys/count")
  public ResponseEntity<Integer> countKeys() {
    _log.debug("Getting RuleOutcomes count");
    final Integer count = ruleOutcomeRepository.count();
    _log.debug("RuleOutcomes count found: {}", count);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(count);
  }

  @DeleteMapping("keys")
  public void deleteAll() {
    ruleOutcomeRepository.deleteAll();
  }

  @GetMapping("key/{vehicleId}")
  public ResponseEntity<RuleOutcomeDTO> getRuleOutcome(final @PathVariable(name = "vehicleId") @NotNull String _vehicleId) {
    _log.debug("Getting RuleOutcome with vehicleId {}", _vehicleId);
    final RuleOutcome ruleOutcome = ruleOutcomeRepository.find(new RuleEngineVehicleId(_vehicleId))
                                                         .orElseThrow((ResourceNotFoundException::new));
    _log.debug("RuleOutcome found: {}", ruleOutcome);
    RuleOutcomeDTO ruleOutcomeDTO = mapToRuleOutcomeDTO(ruleOutcome);
    _log.debug("RuleOutcomeDTO found: {}", ruleOutcome);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(ruleOutcomeDTO);
  }

  @GetMapping("preload/key/{vehicleId}")
  public ResponseEntity<Void> getPreloadRuleOutcome(final @PathVariable(name = "vehicleId") @NotNull String _vehicleId) {
    ResponseEntity<RuleOutcomeDTO> ruleOutcome = getRuleOutcome(_vehicleId);
    BodyBuilder bodyBuilderResponse = ResponseEntity.ok();
    if (!ruleOutcome.getStatusCode()
                    .is2xxSuccessful()) {
      bodyBuilderResponse = ResponseEntity.badRequest();
    }
    return bodyBuilderResponse.build();
  }

  @GetMapping("key/{deviceTypeId}/{vehicleId}")
  public ResponseEntity<RuleOutcomeDTO> getRuleOutcomeVehicleDevice(final @PathVariable(
                                                                                        name = "deviceTypeId") @NotNull TipoDispositivoEnum _deviceTypeId,
                                                                    final @PathVariable(name = "vehicleId") @NotNull String _vehicleId) {
    _log.debug("Getting RuleOutcome with deviceTypeId {} - vehicleId {}", _deviceTypeId, _vehicleId);
    final RuleOutcome ruleOutcome = ruleOutcomeRepository.find(new RuleEngineDeviceTypeId(_deviceTypeId.name()),
                                                               new RuleEngineVehicleId(_vehicleId))
                                                         .orElseThrow((ResourceNotFoundException::new));

    _log.debug("RuleOutcome found: {}", ruleOutcome);
    RuleOutcomeDTO ruleOutcomeDTO = mapToRuleOutcomeDTO(ruleOutcome);
    _log.debug("RuleOutcomeDTO found: {}", ruleOutcome);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(ruleOutcomeDTO);
  }

  @GetMapping("key/{serviceTypeId}/{deviceTypeId}/{vehicleId}")
  public ResponseEntity<RuleOutcomeDTO> getRuleOutcome(final @PathVariable(name = "serviceTypeId") @NotNull String _serviceTypeId,
                                                       final @PathVariable(name = "deviceTypeId") @NotNull String _deviceTypeId,
                                                       final @PathVariable(name = "vehicleId") @NotNull String _vehicleId) {
    _log.debug("Getting RuleOutcome with serviceTypeId {} - deviceTypeId {} - vehicleId {}", _serviceTypeId, _deviceTypeId, _vehicleId);
    final RuleOutcome ruleOutcome = ruleOutcomeRepository.find(new RuleEngineServiceTypeId(_serviceTypeId),
                                                               new RuleEngineDeviceTypeId(_deviceTypeId),
                                                               new RuleEngineVehicleId(_vehicleId))
                                                         .orElseThrow((ResourceNotFoundException::new));

    _log.debug("RuleOutcome found: {}", ruleOutcome);
    RuleOutcomeDTO ruleOutcomeDTO = mapToRuleOutcomeDTO(ruleOutcome);
    _log.debug("RuleOutcomeDTO found: {}", ruleOutcome);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(ruleOutcomeDTO);
  }

  private RuleOutcomeDTO mapToRuleOutcomeDTO(RuleOutcome ruleOutcome) {
    boolean outcome = ruleOutcome.getOutcome() != null ? ruleOutcome.getOutcome() : false;
    Optional<RuleFailure> failure = ruleOutcome.getFailure();
    String code = failure.isPresent() ? failure.get()
                                               .getCode()
                                      : null;
    String msg = failure.isPresent() ? failure.get()
                                              .getMess()
                                     : null;
    return new RuleOutcomeDTO().rule(outcome)
                               .ruleCode(code)
                               .ruleMsg(msg)
                               .creationDate(ruleOutcome.getCreateDate());
  }

  public class RuleOutcomeDTO {

    private boolean rule = true;
    private String  ruleCode;
    private String  ruleMsg;
    private Instant creationDate;

    public RuleOutcomeDTO() {
    }

    public boolean isRule() {
      return rule;
    }

    public RuleOutcomeDTO rule(boolean rule) {
      this.rule = rule;
      return this;
    }

    public String getRuleCode() {
      return ruleCode;
    }

    public RuleOutcomeDTO ruleCode(String ruleCode) {
      this.ruleCode = ruleCode;
      return this;
    }

    public String getRuleMsg() {
      return ruleMsg;
    }

    public RuleOutcomeDTO ruleMsg(String ruleMsg) {
      this.ruleMsg = ruleMsg;
      return this;
    }

    public Instant getCreationDate() {
      return creationDate;
    }

    public RuleOutcomeDTO creationDate(Instant date) {
      this.creationDate = date;
      return this;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("RuleOutcomeDTO [rule=");
      builder.append(rule);
      builder.append(", ruleCode=");
      builder.append(ruleCode);
      builder.append(", ruleMsg=");
      builder.append(ruleMsg);
      builder.append(", date=");
      builder.append(creationDate);
      builder.append("]");
      return builder.toString();
    }

  }
}
