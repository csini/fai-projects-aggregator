package it.fai.ms.efservice.wizard.web.rest.vm.step4;

import java.util.Set;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixAbstract;

@JsonInclude(Include.NON_EMPTY)
public class MatrixStep4 extends MatrixAbstract {

  private static final long serialVersionUID = 5328201193799288769L;

  private Set<MatrixStep4Vehicle> content = new TreeSet<>();

  public MatrixStep4() {
  }

  @JsonProperty(value = "content")
  public Set<MatrixStep4Vehicle> getContent() {
    return content;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName() + " [content=");
    builder.append(content);
    builder.append(",header=");
    builder.append(getHeader());
    builder.append("]");
    return builder.toString();
  }

}
