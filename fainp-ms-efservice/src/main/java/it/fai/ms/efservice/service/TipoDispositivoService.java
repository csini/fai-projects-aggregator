package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import java.util.List;

/**
 * Service Interface for managing TipoDispositivo.
 */
public interface TipoDispositivoService {

    /**
     * Save a tipoDispositivo.
     *
     * @param tipoDispositivoDTO the entity to save
     * @return the persisted entity
     */
    TipoDispositivoDTO save(TipoDispositivoDTO tipoDispositivoDTO);

    /**
     *  Get all the tipoDispositivos.
     *
     *  @return the list of entities
     */
    List<TipoDispositivoDTO> findAll();

    /**
     *  Get the "id" tipoDispositivo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TipoDispositivoDTO findOne(Long id);

    /**
     *  Delete the "id" tipoDispositivo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
