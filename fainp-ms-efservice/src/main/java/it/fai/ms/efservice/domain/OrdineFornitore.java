package it.fai.ms.efservice.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.efservice.domain.enumeration.TipoDisposizione;

/**
 * A OrdineFornitore.
 */
@Entity
@Table(name = "ordine_fornitore")
public class OrdineFornitore implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "quantita")
  private Integer quantita;

  @Enumerated(EnumType.STRING)
  @Column(name = "tipologia_disposizione")
  private TipoDisposizione tipologiaDisposizione;

  @Column(name = "richiedente")
  private String richiedente;

  @Column(name = "data_riordino")
  private Instant dataRiordino;

  @NotNull
  @Min(value = 0)
  @Column(name = "numero_dispositivi_acquisiti", nullable = false)
  private Integer numeroDispositiviAcquisiti;

  @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.REFRESH)
  private AnagraficaGiacenza anagraficaGiacenza;

  @Column(name = "note")
  private String note;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getQuantita() {
    return quantita;
  }

  public OrdineFornitore quantita(Integer quantita) {
    this.quantita = quantita;
    return this;
  }

  public void setQuantita(Integer quantita) {
    this.quantita = quantita;
  }

  public TipoDisposizione getTipologiaDisposizione() {
    return tipologiaDisposizione;
  }

  public OrdineFornitore tipologiaDisposizione(TipoDisposizione tipologiaDisposizione) {
    this.tipologiaDisposizione = tipologiaDisposizione;
    return this;
  }

  public void setTipologiaDisposizione(TipoDisposizione tipologiaDisposizione) {
    this.tipologiaDisposizione = tipologiaDisposizione;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public OrdineFornitore richiedente(String richiedente) {
    this.richiedente = richiedente;
    return this;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }

  public Instant getDataRiordino() {
    return dataRiordino;
  }

  public OrdineFornitore dataRiordino(Instant dataRiordino) {
    this.dataRiordino = dataRiordino;
    return this;
  }

  public void setDataRiordino(Instant dataRiordino) {
    this.dataRiordino = dataRiordino;
  }

  public Integer getNumeroDispositiviAcquisiti() {
    return numeroDispositiviAcquisiti;
  }

  public OrdineFornitore numeroDispositiviAcquisiti(Integer numeroDispositiviAcquisiti) {
    this.numeroDispositiviAcquisiti = numeroDispositiviAcquisiti;
    return this;
  }

  public void setNumeroDispositiviAcquisiti(Integer numeroDispositiviAcquisiti) {
    this.numeroDispositiviAcquisiti = numeroDispositiviAcquisiti;
  }

  public AnagraficaGiacenza getAnagraficaGiacenza() {
    return anagraficaGiacenza;
  }

  public OrdineFornitore anagraficaGiacenza(AnagraficaGiacenza anagraficaGiacenza) {
    this.anagraficaGiacenza = anagraficaGiacenza;
    return this;
  }

  public void setAnagraficaGiacenza(AnagraficaGiacenza anagraficaGiacenza) {
    this.anagraficaGiacenza = anagraficaGiacenza;
  }

  public String getNote() {
    return note;
  }

  public OrdineFornitore note(String note) {
    this.note = note;
    return this;
  }

  public void setNote(String note) {
    this.note = note;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrdineFornitore ordineFornitore = (OrdineFornitore) o;
    if (ordineFornitore.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), ordineFornitore.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "OrdineFornitore{" + "id=" + getId() + ", quantita=" + getQuantita() + ", tipologiaDisposizione='" + getTipologiaDisposizione()
           + "'" + ", richiedente='" + getRichiedente() + "'" + ", dataRiordino='" + getDataRiordino() + "'"
           + ", numeroDispositiviAcquisiti=" + getNumeroDispositiviAcquisiti() + ", note=" + getNote() + "}";
  }
}
