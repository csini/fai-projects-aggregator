package it.fai.ms.efservice.service.jms.consumer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional
public class DeviceAssociatedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseOkConsumer deviceResponseOkConsumer;

  public DeviceAssociatedConsumer(final DeviceResponseOkConsumer _deviceResponseOkConsumer) {
    deviceResponseOkConsumer = _deviceResponseOkConsumer;
  }

  public void consume(DeviceEventMessage eventMessage) throws FsmExecuteCommandException {
    log.info("Manage message: {}", eventMessage);
    final DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();
    log.debug("Simple change status Richiesta....");
    try {
    	deviceResponseOkConsumer.changeStatoRichiesta(identificativoRichiesta);
    } catch (FsmExecuteCommandException e) {
		if(e.getMessage().contains("ORDINE_SOSPESO")) {
			log.warn("Non è possibile cambiare lo stato di un richiesta sospesa: {}",eventMessage);
		}else {
			throw e;
		}
    }
  }

}
