package it.fai.ms.efservice.domain.enumeration;

/**
 * The TipoMagazzino enumeration.
 */
public enum TipoMagazzino {
    NONE, SCORTA, NOTARGA
}
