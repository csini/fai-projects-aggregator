package it.fai.ms.efservice.service.jms.consumer;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;

@Service
@Transactional
public class NewRequestConsumer {
  
  private final Logger log = LoggerFactory.getLogger(getClass().getName());
  
  final VehicleWizardCacheServiceExt                  vehicleWizardCacheService;
  final RichiestaServiceExt                           richiestaServiceExt;
  final FsmRichiestaUtils                             fsmRichiestaUtils;
  final DeviceServiceActivationMessageProducerService activationProducerService;
  final TipoDispositivoServiceExt                     tipoDispositivoServiceExt;

  public NewRequestConsumer(final VehicleWizardCacheServiceExt vehicleWizardCacheService,
                            final RichiestaServiceExt richiestaServiceExt, final FsmRichiestaUtils fsmRichiestaUtils,
                            final DeviceServiceActivationMessageProducerService activationProducerService,
                            final TipoDispositivoServiceExt tipoDispositivoServiceExt) {
this.vehicleWizardCacheService = vehicleWizardCacheService;
this.richiestaServiceExt = richiestaServiceExt;
this.fsmRichiestaUtils = fsmRichiestaUtils;
this.activationProducerService = activationProducerService;
this.tipoDispositivoServiceExt = tipoDispositivoServiceExt;
  }
  
  public void consume(String identificativoRichiesta) throws FsmExecuteCommandException {
    log.debug("Identificativo Richiesta: {}", identificativoRichiesta);
    
    Richiesta richiesta = fsmRichiestaUtils.findRichiesta(identificativoRichiesta);
    if(richiesta != null) {
      
      richiesta = assignTargaOnRichiesta(richiesta);
      richiesta = executeFsmForRichiesteDispositiviDaRestituire(richiesta);

      sendMessageToActiveDeActiveServices(richiesta);
      autoAcceptRequestToGoBox(richiesta);
    } else {
      log.error("Not found request by indetifier: {}. This should not be possible", identificativoRichiesta);
    }
  }
  
  private Richiesta assignTargaOnRichiesta(Richiesta richiesta) throws FsmExecuteCommandException {
    Set<Dispositivo> dispositivos = richiesta.getDispositivos();
    Optional<Dispositivo> optDevice = dispositivos.stream().findFirst();
    if(optDevice.isPresent()) {
      Dispositivo device = optDevice.get();
      //tipo magazzino no_targa &&
      //non c'è no targa in magazzi
      Set<AssociazioneDV> adv = device.getAssociazioneDispositivoVeicolos();
      Optional<AssociazioneDV> optAdv = adv.stream().findFirst();
      
      if(optAdv.isPresent()) {
        
        AssociazioneDV associazioneDV = optAdv.get();
       
        String uuidVeicolo = associazioneDV.getUuidVeicolo(); 
        String licensePlate = vehicleWizardCacheService.getTargaByUuidVehicle(uuidVeicolo);
        
        if (    
              device.getTipoDispositivo().getNome().equals(TipoDispositivoEnum.VIACARD) && 
              !Objects.equals(device.getTipoMagazzino(), TipoMagazzino.NOTARGA) &&
              StringUtils.isBlank(licensePlate)
             
        ) {
          
          log.error("Targa non recuperata per veicolo uuidVeicolo {}", uuidVeicolo);
          richiesta.setStato(StatoRichiesta.SOSPESA);
          richiesta.setAnomalia("Targa mancante");
          richiesta.setMotivoSospensione("Annullare richiesta"); 
          richiesta.setUltimaNota("Annullare richiesta"); 
          
        }else {
          
          richiesta.setAssociazione(licensePlate); 
          String country = vehicleWizardCacheService.getNazioneByUuidVehicle(uuidVeicolo);
          richiesta.setCountry(country);
          log.info("Associo targa/nazione {} {}", licensePlate, country);
          
        }
        
      } else {
        log.debug("Not found associazione DV for device: {}", device);
      }
    } else {
      log.debug("Not found Device on Richiesta: {}", richiesta.getIdentificativo());
    }
    
    log.info("Saved targa on Richiesta: {}", richiesta);
    return richiestaServiceExt.save(richiesta);
  } 
   
  
  private Richiesta executeFsmForRichiesteDispositiviDaRestituire(Richiesta richiesta) {
    String identificativoRichiesta = richiesta.getIdentificativo();
    TipoRichiesta tipoRichiesta = richiesta.getTipo();
    log.info("Change status for richiesta [Identificativo: {} - Tipo: {}]", identificativoRichiesta, tipoRichiesta);
    switch (tipoRichiesta) {
    case MALFUNZIONAMENTO:
      try {
        richiesta = fsmRichiestaUtils.changeStatusToRichiesta(richiesta, RichiestaEvent.INITIAL.name(), "", "");
      } catch (Exception e) {
        log.error("Exception on FSM Richiesta malfunzionamento Dispositivi da restituire", e);
        throw new RuntimeException(e);
      }
      break;

    default:
      log.warn("Tipo Richiesta {} for Richiesta: {} skipped to change status", tipoRichiesta, identificativoRichiesta);
      break;
    }

    return richiesta;
  }

  private void sendMessageToActiveDeActiveServices(Richiesta richiesta) {
    if(richiesta != null) {
      String identificativo = richiesta.getIdentificativo();
      TipoRichiesta tipoRichiesta = richiesta.getTipo();
      if(tipoRichiesta == TipoRichiesta.ATTIVAZIONE_SERVIZIO
          || tipoRichiesta == TipoRichiesta.DISATTIVAZIONE_SERVIZIO) {
            log.debug("TipoRichiesta: {} for identifier: {}", tipoRichiesta, identificativo);
            
            try {
            richiesta = fsmRichiestaUtils.executeFsmAndPersist(null, richiesta,
                                                   RichiestaEvent.INOLTRO_SERVIZI);
            log.debug("Change status SDS on DB start");
            activationProducerService.updateDeviceServiceStatus(richiesta);
            } catch (Exception e) {
              log.error("Exception on change status for EVTNO [Richiesta "
                        + richiesta.getIdentificativo() + "]", e);
              throw new RuntimeException(e);
            }
            log.info("Send message to Activation/DeActivation service to Richiesta: {}", identificativo);
          } else {
            log.debug("Skip send activation/deactivation services because request type is: {}", tipoRichiesta);
          }
    }
  }

  private void autoAcceptRequestToGoBox(Richiesta richiesta) throws FsmExecuteCommandException {
    if(richiesta != null) {
      TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
      if(tipoDispositivo != null && richiesta.getTipoDispositivo()
          .getNome()
          .equals(TipoDispositivoEnum.GO_BOX)) {
        
        richiesta = fsmRichiestaUtils.changeStatusToRichiesta(richiesta, RichiestaEvent.MU_ACCETTA_RICHIESTA.name(),
                                                  null, "ACCETTATA DA SISTEMA");
        log.info("Change status to GoBox request: {}", richiesta);
      } else {
        log.debug("Skip auto-accept request, because device is [{}] and it's not GO BOX.", tipoDispositivo);
      }
    }
  }
  
}
