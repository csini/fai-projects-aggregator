package it.fai.ms.efservice.service.fsm.type.frejus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.frejus.FsmMalfunzionamentoFrejusSostConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmMalfunzionamentoFrejusSost.FSM_NAME)
public class FsmMalfunzionamentoFrejusSost extends FsmRichiestaGeneric {

  public static final String FSM_NAME = "FsmMalfunzionamentoFrejusSost";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmMalfunzionamentoFrejusSost(@Qualifier(FsmMalfunzionamentoFrejusSostConfig.MOD_FREJUS_MALF_SOST) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                       FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService        = _cacheService;
    fsmType             = FsmType.MALFUNZIONAMENTO_FREJUS;
    deviceTypes         = new TipoDispositivoEnum[] { TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
