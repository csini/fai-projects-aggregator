package it.fai.ms.efservice.web.rest;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.CategoriaServizioServiceExt;
import it.fai.ms.efservice.service.TipoServizioServiceExt;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.dto.ServizioUtenteDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

@RestController
@RequestMapping(CategoriaServizioResourceExt.BASE_PATH)
public class TipoServizioResourceExt {

  private final Logger log = LoggerFactory.getLogger(TipoServizioResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  public static final String SERVIZI_UTENTE_LIST_PATH = "/getListaServiziUtente";

  private final TipoServizioServiceExt tipoServizioServiceExt;

  private CategoriaServizioServiceExt categoriaServizioService;

  private final TipoDispositivoRepository tipoDispositivoService;

  public TipoServizioResourceExt(TipoServizioServiceExt tipoServizioServiceExt, TipoDispositivoRepository tipoDispositivoService) {
    this.tipoServizioServiceExt = tipoServizioServiceExt;
    this.tipoDispositivoService = tipoDispositivoService;
  }

  @GetMapping(SERVIZI_UTENTE_LIST_PATH)
  @Timed
  @ApiOperation(value = "Recupera la lista con tutti i servizi legati ad un utente", response = CategoriaServizioResourceExt.class)
  @ApiResponses(
                value = { @ApiResponse(code = 200, message = "getListaServiziUtente retrieved"),
                          @ApiResponse(code = 400, message = "getListaServiziUtente error") })
  public ResponseEntity<List<ServizioUtenteDTO>> getListaServiziUtente(@RequestParam String codiceAzienda,
                                                                       @RequestParam String categoriaServizio) throws Exception {

    CategoriaServizioDTO categoriaServizioDTO = categoriaServizioService.findByNome(categoriaServizio);

    if (categoriaServizioDTO == null) {
      throw CustomException.builder(HttpStatus.UNPROCESSABLE_ENTITY)
        .add(Errno.INVALID_CATEGORY_TYPE);
    }

    List<ServizioUtenteDTO> lista = tipoServizioServiceExt.getListaServiziUtente(codiceAzienda, categoriaServizioDTO);

    // Sorting
    // Collections.sort(lista, new FieldOrdinamentoComparator());

    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(lista));

  }

  @GetMapping("avaiable/{tipoDispositivo}")
  @Transactional
  public Set<String> getAvaiableByTipoDispositivo(@PathVariable("tipoDispositivo") TipoDispositivoEnum tipoDispositivoName,
                                                        @RequestParam(name = "tipoHardware",required=false) String tipoHardware) {

    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(tipoDispositivoName);
    return this.tipoServizioServiceExt.getAvaiableByTipoDispositivo(tipoDispositivo, tipoHardware).stream().map(td->td.getNome()).collect(Collectors.toSet());
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
      .body(ex.getPayloadError());
  }

  @Autowired
  public void setCategoriaServizioService(CategoriaServizioServiceExt categoriaServizioService) {
    this.categoriaServizioService = categoriaServizioService;
  }
}
