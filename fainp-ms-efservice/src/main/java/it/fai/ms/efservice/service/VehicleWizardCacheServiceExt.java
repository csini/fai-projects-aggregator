package it.fai.ms.efservice.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;

/**
 * Service Interface for managing Richiesta.
 */

public interface VehicleWizardCacheServiceExt {

  String getTargaByUuidVehicle(String uuidVehicle);

  String getNazioneByUuidVehicle(String uuidVehicle);

  String getLibrettoByUuidVehicle(String uuidVehicle);

  Richiesta assignTargaOnRichiesta(Richiesta richiesta);

  Richiesta assignTargaOnRichiesteDiSostituzione(Richiesta richiesta);

  List<Richiesta> assignTargaOnRichieste(List<Richiesta> richieste);

  List<Richiesta> assignTargaOnRichiesteDiSostituzione(List<Richiesta> richieste);

  List<Richiesta> assignTargaOnRichiesteFiltered(List<Richiesta> richieste, TipoDispositivoEnum deviceType);

  String getClasseEuroByUuidVehicle(String uuidVeicolo);

}
