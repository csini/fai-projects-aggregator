package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.TelepassEuDenunciaAllegatoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeFurtoSmarrimento;

@Service
@Transactional
public class DenunciaDaAllineamentoConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final RichiestaRepository              richiestaRepo;
  private final RichiestaRepositoryExt           richiestaRepoExt;
  private final FsmModificaTeFurtoSmarrimento fsmModTeFurtoSmarrimento;

  /**
   * @param richiestaRepo
   * @param fsmModTeFurtoSmarrimento
   */
  public DenunciaDaAllineamentoConsumer(RichiestaRepository richiestaRepo, RichiestaRepositoryExt richiestaRepoExt,
                                        FsmModificaTeFurtoSmarrimento fsmModTeFurtoSmarrimento) {
    this.richiestaRepo = richiestaRepo;
    this.richiestaRepoExt = richiestaRepoExt;
    this.fsmModTeFurtoSmarrimento = fsmModTeFurtoSmarrimento;
  }

  public void ricevutaDenuncia(TelepassEuDenunciaAllegatoDTO dto) throws Exception {

    String identificativoRichiesta = dto.getIdentificativoRichiesta();
    Richiesta richiesta = richiestaRepoExt.findOneByIdentificativo(identificativoRichiesta);
    log.info("Load richiesta [ ID: " + richiesta.getId() + " - IDENTIFICATIVO: " + richiesta.getIdentificativo() + "]");

    RichiestaEvent command = RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO;
    richiesta = fsmModTeFurtoSmarrimento.executeCommandToChangeState(command, richiesta);
    richiestaRepo.save(richiesta);
  }
}
