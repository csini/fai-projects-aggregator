package it.fai.ms.efservice.service.jms.listener.orderrequest;

import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderedMessage;
import it.fai.ms.common.utils.SpringProfiles;
import it.fai.ms.efservice.service.jms.consumer.DeviceOrderedConsumer;

@Service
@Transactional
@Profile({SpringProfiles.LINE_1,SpringProfiles.NOT_PROD})
public class JmsListenerTrackyCardDeviceOrdered implements JmsQueueListener {

	private final Logger _log = LoggerFactory.getLogger(getClass());

	private final DeviceOrderedConsumer consumer;

	@Autowired
	public JmsListenerTrackyCardDeviceOrdered(DeviceOrderedConsumer _consumer) throws Exception {
		consumer = _consumer;
	}

	@Override
	public void onMessage(final Message _message) {
		DeviceOrderedMessage deviceOrderedMessage = null;
		try {
			try {
				deviceOrderedMessage = (DeviceOrderedMessage) ((ObjectMessage) _message).getObject();
				_log.info("Received jms message {}", deviceOrderedMessage);
				consumer.consume(deviceOrderedMessage);
			} catch (@SuppressWarnings("unused") final ClassCastException _e) {
				_log.warn("JMS ObjectMessage isn't {}", DeviceOrderedMessage.class.getSimpleName());
			}
		} catch (final Exception _e) {
			_log.error("Error consuming message {}", _message, _e);
			throw new RuntimeException(_e);
		}
	}

	@Override
	public JmsQueueNames getQueueName() {
		return JmsQueueNames.TRACKYCARD_DEVICE_ORDERED;
	}

}