package it.fai.ms.efservice.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A AnagraficaGiacenza.
 */
@Entity
@Table(name = "anagrafica_giacenza")
public class AnagraficaGiacenza implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tipo_dispositivo")
    private String tipoDispositivo;

    @Column(name = "produttore_nome")
    private String produttoreNome;

    @Column(name = "giacenza_minima")
    private Integer giacenzaMinima;

    @Column(name = "alert_riordino")
    private Integer alertRiordino;

    @Column(name = "quantita_ultimo_riordino")
    private Integer quantitaUltimoRiordino;

    @Column(name = "notification_send")
    private Boolean notificationSend;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoDispositivo() {
        return tipoDispositivo;
    }

    public AnagraficaGiacenza tipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
        return this;
    }

    public void setTipoDispositivo(String tipoDispositivo) {
        this.tipoDispositivo = tipoDispositivo;
    }

    public String getProduttoreNome() {
        return produttoreNome;
    }

    public AnagraficaGiacenza produttoreNome(String produttoreNome) {
        this.produttoreNome = produttoreNome;
        return this;
    }

    public void setProduttoreNome(String produttoreNome) {
        this.produttoreNome = produttoreNome;
    }

    public Integer getGiacenzaMinima() {
        return giacenzaMinima;
    }

    public AnagraficaGiacenza giacenzaMinima(Integer giacenzaMinima) {
        this.giacenzaMinima = giacenzaMinima;
        return this;
    }

    public void setGiacenzaMinima(Integer giacenzaMinima) {
        this.giacenzaMinima = giacenzaMinima;
    }

    public Integer getAlertRiordino() {
        return alertRiordino;
    }

    public AnagraficaGiacenza alertRiordino(Integer alertRiordino) {
        this.alertRiordino = alertRiordino;
        return this;
    }

    public void setAlertRiordino(Integer alertRiordino) {
        this.alertRiordino = alertRiordino;
    }

    public Integer getQuantitaUltimoRiordino() {
        return quantitaUltimoRiordino;
    }

    public AnagraficaGiacenza quantitaUltimoRiordino(Integer quantitaUltimoRiordino) {
        this.quantitaUltimoRiordino = quantitaUltimoRiordino;
        return this;
    }

    public void setQuantitaUltimoRiordino(Integer quantitaUltimoRiordino) {
        this.quantitaUltimoRiordino = quantitaUltimoRiordino;
    }

    public Boolean isNotificationSend() {
        return notificationSend;
    }

    public AnagraficaGiacenza notificationSend(Boolean notificationSend) {
        this.notificationSend = notificationSend;
        return this;
    }

    public void setNotificationSend(Boolean notificationSend) {
        this.notificationSend = notificationSend;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnagraficaGiacenza anagraficaGiacenza = (AnagraficaGiacenza) o;
        if (anagraficaGiacenza.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), anagraficaGiacenza.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AnagraficaGiacenza{" +
            "id=" + getId() +
            ", tipoDispositivo='" + getTipoDispositivo() + "'" +
            ", produttoreNome='" + getProduttoreNome() + "'" +
            ", giacenzaMinima=" + getGiacenzaMinima() +
            ", alertRiordino=" + getAlertRiordino() +
            ", quantitaUltimoRiordino=" + getQuantitaUltimoRiordino() +
            ", notificationSend='" + isNotificationSend() + "'" +
            "}";
    }
}
