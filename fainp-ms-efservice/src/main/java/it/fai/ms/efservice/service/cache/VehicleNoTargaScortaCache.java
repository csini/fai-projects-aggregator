package it.fai.ms.efservice.service.cache;

import javax.annotation.PostConstruct;

import org.infinispan.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleNoTargaScortaCache {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private VehicleNoTargaScortaInfinispanConfigFactory infinispanConfig;

  private Cache<String, String> cache;

  @PostConstruct
  public void init() {
    log.debug("Init service VehicleNoTargaScorta Cache");
    cache = infinispanConfig.buildCache("vehicle_notarga_scorta");
  }

  public void putObject(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    cache.put(key, json);
  }

  public String getObject(String key) {
    log.debug("Get object from Map with Key: {}", key);
    String json = cache.get(key);
    return json;
  }

  public void clearCache() {
    log.debug("Clear cache {}", ((cache != null) ? cache.getName() : ""));
    cache.clear();
  }

}
