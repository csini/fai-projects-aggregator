package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.listener.RichiestaEntityListener;
import org.hibernate.LazyInitializationException;

/**
 * A Richiesta.
 */
@Entity
@EntityListeners({ RichiestaEntityListener.class, })
@Table(name = "richiesta")
public class Richiesta implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "identificativo")
  private String identificativo;

  @NotNull
  @Column(name = "data", nullable = false)
  @Convert(converter = DateInstantConverter.class)
  private Instant data;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "tipo", nullable = false)
  private TipoRichiesta tipo;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "stato", nullable = false)
  private StatoRichiesta stato;

  @Column(name = "ultima_nota")
  private String ultimaNota;

  @Column(name = "uuid_documento")
  private String uuidDocumento;

  @Column(name = "anomalia")
  private String anomalia;

  @Column(name = "motivo_sospensione")
  private String motivoSospensione;

  @Column(name = "associazione")
  private String associazione;

  @Column(name = "operazioni_possibili")
  private String operazioniPossibili;

  @Column(name = "data_modifica_stato")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataModificaStato;

  @Column(name = "nome_servizio")
  private String nomeServizio;

  @ManyToOne
  private Contratto contratto;

  @ManyToMany
  @JoinTable(
             name = "richiesta_dispositivo",
             joinColumns = @JoinColumn(name = "richiestas_id", referencedColumnName = "id"),
             inverseJoinColumns = @JoinColumn(name = "dispositivos_id", referencedColumnName = "id"))
  private Set<Dispositivo> dispositivos = new HashSet<>();

  @ManyToOne
  private OrdineCliente ordineCliente;

  @ManyToOne
  private RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore;

  @ManyToOne
  private TipoDispositivo tipoDispositivo;

  @Column(name = "nazione_targa")
  private String country;

  @Column(name = "new_targa_nazione")
  private String newTargaNazione;

  @Column(name = "punto_blu")
  private Boolean puntoBlu = false;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "richiesta")
  private List<DatiAggiuntiviRichiesta> datiAggiuntivi;

  @Transient
  public String[] getTipiServizioList() {
    if (this.nomeServizio != null) {
      return nomeServizio.split(",");
    }
    return null;
  }

  @Transient
  public void setTipiServizioList(String[] tipiServizio) {
    if (tipiServizio != null) {
      this.nomeServizio = String.join(",", tipiServizio);
    }
  }

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public Richiesta identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Instant getData() {
    return data;
  }

  public Richiesta data(Instant data) {
    this.data = data;
    return this;
  }

  public void setData(Instant data) {
    this.data = data;
  }

  public TipoRichiesta getTipo() {
    return tipo;
  }

  public Richiesta tipo(TipoRichiesta tipo) {
    this.tipo = tipo;
    return this;
  }

  public void setTipo(TipoRichiesta tipo) {
    this.tipo = tipo;
  }

  public StatoRichiesta getStato() {
    return stato;
  }

  public Richiesta stato(StatoRichiesta stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(StatoRichiesta stato) {
    this.stato = stato;
  }

  public String getUltimaNota() {
    return ultimaNota;
  }

  public Richiesta ultimaNota(String ultimaNota) {
    this.ultimaNota = ultimaNota;
    return this;
  }

  public void setUltimaNota(String ultimaNota) {
    this.ultimaNota = ultimaNota;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }

  public Richiesta uuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
    return this;
  }

  public void setUuidDocumento(String uuidDocumento) {
    if (StringUtils.isNotBlank(uuidDocumento)) {
      this.uuidDocumento = uuidDocumento;
    }
  }

  @JsonIgnore
  public void setUuidDocuments(String... uuidDocuments) {
    this.uuidDocumento = String.join(",", uuidDocuments);
  }

  @JsonIgnore
  public List<String> getUuidDocuments() {
    List<String> listUuidDoc = new ArrayList<>();
    if (this.uuidDocumento != null) {
      String[] splittedUuidDoc = StringUtils.split(this.uuidDocumento, ",");
      for (String uuidDoc : splittedUuidDoc) {
        listUuidDoc.add(uuidDoc);
      }
    }
    return listUuidDoc;
  }

  public String getAnomalia() {
    return anomalia;
  }

  public Richiesta anomalia(String anomalia) {
    this.anomalia = anomalia;
    return this;
  }

  public void setAnomalia(String anomalia) {
    this.anomalia = anomalia;
  }

  public String getMotivoSospensione() {
    return motivoSospensione;
  }

  public Richiesta motivoSospensione(String motivoSospensione) {
    this.motivoSospensione = motivoSospensione;
    return this;
  }

  public void setMotivoSospensione(String motivoSospensione) {
    this.motivoSospensione = motivoSospensione;
  }

  public String getAssociazione() {
    return associazione;
  }

  public Richiesta associazione(String associazione) {
    this.associazione = associazione;
    return this;
  }

  public void setAssociazione(String associazione) {
    this.associazione = associazione;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public Richiesta operazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
    return this;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public Richiesta dataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
    return this;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public String getNomeServizio() {
    return nomeServizio;
  }

  public Richiesta nomeServizio(String nomeServizio) {
    this.nomeServizio = nomeServizio;
    return this;
  }

  public void setNomeServizio(String nomeServizio) {
    this.nomeServizio = nomeServizio;
  }

  public Contratto getContratto() {
    return contratto;
  }

  public Richiesta contratto(Contratto contratto) {
    this.contratto = contratto;
    return this;
  }

  public void setContratto(Contratto contratto) {
    this.contratto = contratto;
  }

  public Set<Dispositivo> getDispositivos() {
    return dispositivos;
  }

  public Richiesta dispositivos(Set<Dispositivo> dispositivos) {
    this.dispositivos = dispositivos;
    return this;
  }

  public Richiesta addDispositivo(Dispositivo dispositivo) {
    if (this.dispositivos == null) {
      this.dispositivos = new HashSet<>();
    }
    this.dispositivos.add(dispositivo);
    dispositivo.getRichiestas()
               .add(this);
    return this;
  }

  public Richiesta removeDispositivo(Dispositivo dispositivo) {
    this.dispositivos.remove(dispositivo);
    dispositivo.getRichiestas()
               .remove(this);
    return this;
  }

  public void setDispositivos(Set<Dispositivo> dispositivos) {
    this.dispositivos = dispositivos;
  }

  public OrdineCliente getOrdineCliente() {
    return ordineCliente;
  }

  public Richiesta ordineCliente(OrdineCliente ordineCliente) {
    this.ordineCliente = ordineCliente;
    return this;
  }

  public void setOrdineCliente(OrdineCliente ordineCliente) {
    this.ordineCliente = ordineCliente;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }

  public Richiesta tipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getCountry() {
    return country;
  }

  public Richiesta country(String country) {
    this.country = country;
    return this;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getNewTargaNazione() {
    return newTargaNazione;
  }

  public Richiesta newTargaNazione(String newTargaNazione) {
    this.newTargaNazione = newTargaNazione;
    return this;
  }

  public void setNewTargaNazione(String newTargaNazione) {
    this.newTargaNazione = newTargaNazione;
  }

  public Boolean getPuntoBlu() {
    return puntoBlu;
  }

  public Richiesta puntoBlu(Boolean puntoBlu) {
    if (puntoBlu == null) {
      puntoBlu = false;
    }
    this.puntoBlu = puntoBlu;
    return this;
  }

  public void setPuntoBlu(Boolean puntoBlu) {
    if (puntoBlu == null) {
      puntoBlu = false;
    }
    this.puntoBlu = puntoBlu;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  public RaggruppamentoRichiesteOrdineFornitore getRaggruppamentoRichiesteOrdineFornitore() {
    return raggruppamentoRichiesteOrdineFornitore;
  }

  public void setRaggruppamentoRichiesteOrdineFornitore(RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore) {
    this.raggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitore;
  }

  public List<DatiAggiuntiviRichiesta> getDatiAggiuntivi() {
    return this.datiAggiuntivi;
  }

  public void setDatiAggiuntivi(List<DatiAggiuntiviRichiesta> datiAggiuntivi) {
    this.datiAggiuntivi = datiAggiuntivi;
  }

  public void addDatiAggiuntivi(DatiAggiuntiviRichiesta datiAggiuntiviRichiesta) {
    if (this.datiAggiuntivi == null) {
      this.datiAggiuntivi = new ArrayList<>();
    }
    this.datiAggiuntivi.add(datiAggiuntiviRichiesta);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Richiesta richiesta = (Richiesta) o;
    if (richiesta.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), richiesta.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("Richiesta {");
    safeAppendToBuilder(sb, "id", id);
    safeAppendToBuilder(sb, "identificativo", identificativo);
    safeAppendToBuilder(sb, "data", data);
    safeAppendToBuilder(sb, "tipo", tipo);
    safeAppendToBuilder(sb, "stato", stato);
    safeAppendToBuilder(sb, "ultimaNota", ultimaNota);
    safeAppendToBuilder(sb, "uuidDocumento", uuidDocumento);
    safeAppendToBuilder(sb, "anomalia", anomalia);
    safeAppendToBuilder(sb, "motivoSospensione", motivoSospensione);
    safeAppendToBuilder(sb, "associazione", associazione);
    safeAppendToBuilder(sb, "operazioniPossibili", operazioniPossibili);
    safeAppendToBuilder(sb, "dataModificaStato", dataModificaStato);
    safeAppendToBuilder(sb, "nomeServizio", nomeServizio);
    safeAppendToBuilder(sb, "contratto", contratto);
    safeAppendToBuilder(sb, "dispositivos", dispositivos);
    safeAppendToBuilder(sb, "ordineCliente", ordineCliente);
    safeAppendToBuilder(sb, "tipoDispositivo", tipoDispositivo);
    safeAppendToBuilder(sb, "country", country);
    safeAppendToBuilder(sb, "newTargaNazione", newTargaNazione);
    safeAppendToBuilder(sb, "puntoBlu", puntoBlu);
    sb.append("} ");
    return sb.toString();
  }

  private void safeAppendToBuilder(StringBuffer sb, String name, Object obj) {
    boolean isString = obj != null && obj.getClass().isAssignableFrom(String.class);
    sb.append(name).append("=");
    if (isString){
      sb.append("\'");
    }

    try {
      sb.append(obj);
    } catch (LazyInitializationException e){
      sb.append("[ ...lazy_entity(ies)_here... ]");
    }

    if (isString){
      sb.append("\'");
    }
    sb.append(", ");
  }
}
