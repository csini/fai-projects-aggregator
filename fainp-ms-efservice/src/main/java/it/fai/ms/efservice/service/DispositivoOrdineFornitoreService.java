package it.fai.ms.efservice.service;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreBySerialeDTO.OrdineFornitoreDispositivo;

/**
 * Service Interface for managing DispositivoOrdineFornitore.
 */
public interface DispositivoOrdineFornitoreService {

  Set<Dispositivo> createDispositivoByRange(String tipoDispositivo, String pad, String rangeStart, String rangeStop,
                                            Set<String> serialiToSkip);

  Set<Dispositivo> createDispositiviByList(String tipoDispositivo, Collection<OrdineFornitoreDispositivo> rowList, Set<String> serialiToSkip);

  Dispositivo createDispositivoBySeriale(String tipoDispositivo, String seriale, String pan) throws Exception;

  Optional<Dispositivo> findDeviceByTipoAndSeriale(TipoDispositivoEnum tipoDispositivoEnum, String seriale);

  ZonedDateTime parseDate(String string);

}
