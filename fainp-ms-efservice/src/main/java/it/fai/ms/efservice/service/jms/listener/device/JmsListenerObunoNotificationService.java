package it.fai.ms.efservice.service.jms.listener.device;

import static it.fai.ms.common.jms.JmsTopicNames.OBUNO_NOTIFICATION;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.message.device.ObuNotificationMessage;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.consumer.ObunoNotificationConsumer;

@Service
@Transactional
public class JmsListenerObunoNotificationService extends JmsObjectMessageListenerTemplate<ObuNotificationMessage> implements JmsTopicListener {

  private final Logger _log = LoggerFactory.getLogger(JmsTopicSenderService.class);

  private final ObunoNotificationConsumer obunoNotificationConsumer;

  @Autowired
  public JmsListenerObunoNotificationService(final ObunoNotificationConsumer _obunoNotificationConsumer) throws Exception {
    obunoNotificationConsumer = _obunoNotificationConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return OBUNO_NOTIFICATION;
  }

  @Override
  protected void consumeMessage(ObuNotificationMessage obunoNotificationMessage) {
    try {
      _log.info("Received jms message {}", obunoNotificationMessage);
      obunoNotificationConsumer.consume(obunoNotificationMessage);
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", obunoNotificationMessage, _e);
      throw new RuntimeException(_e);
    }
  }

}