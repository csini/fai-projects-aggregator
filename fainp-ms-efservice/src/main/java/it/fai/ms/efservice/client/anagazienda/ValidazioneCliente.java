/*
 * Nekte WebApi
 * Rest Web Api NAV<->CRM
 *
 * OpenAPI spec version: v1
 * Contact: help@nekte.it
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package it.fai.ms.efservice.client.anagazienda;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Modello Validazione Cliente
 */
@ApiModel(description = "Modello Validazione Cliente")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-05-14T17:16:08.536+02:00")
public class ValidazioneCliente {
  @JsonProperty("codiceCliente")
  private String codiceCliente = null;

  @JsonProperty("attivo")
  private Boolean attivo = null;

  @JsonProperty("fideiussioni")
  private Boolean fideiussioni = null;

  @JsonProperty("modalitaPagamento")
  private Boolean modalitaPagamento = null;

  @JsonProperty("licenzaValida")
  private Boolean licenzaValida = null;

  @JsonProperty("cartaIdentitaLegRapp")
  private Boolean cartaIdentitaLegRapp = null;

  @JsonProperty("dataAccettPrat")
  private Boolean dataAccettPrat = null;

  @JsonProperty("fidoMensile")
  private Boolean fidoMensile = null;

  @JsonProperty("forzLetSped")
  private Boolean forzLetSped = null;

  @JsonProperty("dataMaxForzLetSped")
  private OffsetDateTime dataMaxForzLetSped = null;

  public ValidazioneCliente codiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
    return this;
  }

   /**
   * Codice Cliente per cui è stata richiesta la validazione
   * @return codiceCliente
  **/
  @ApiModelProperty(value = "Codice Cliente per cui è stata richiesta la validazione")
  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public ValidazioneCliente attivo(Boolean attivo) {
    this.attivo = attivo;
    return this;
  }

   /**
   * Indica se il cliente è attivo
   * @return attivo
  **/
  @ApiModelProperty(value = "Indica se il cliente è attivo")
  public Boolean isAttivo() {
    return attivo;
  }

  public void setAttivo(Boolean attivo) {
    this.attivo = attivo;
  }

  public ValidazioneCliente fideiussioni(Boolean fideiussioni) {
    this.fideiussioni = fideiussioni;
    return this;
  }

   /**
   * Indica se il cliente ha almeno una fideiussione
   * @return fideiussioni
  **/
  @ApiModelProperty(value = "Indica se il cliente ha almeno una fideiussione")
  public Boolean isFideiussioni() {
    return fideiussioni;
  }

  public void setFideiussioni(Boolean fideiussioni) {
    this.fideiussioni = fideiussioni;
  }

  public ValidazioneCliente modalitaPagamento(Boolean modalitaPagamento) {
    this.modalitaPagamento = modalitaPagamento;
    return this;
  }

   /**
   * Indica se il cliente ha una modalità di pagamento associata
   * @return modalitaPagamento
  **/
  @ApiModelProperty(value = "Indica se il cliente ha una modalità di pagamento associata")
  public Boolean isModalitaPagamento() {
    return modalitaPagamento;
  }

  public void setModalitaPagamento(Boolean modalitaPagamento) {
    this.modalitaPagamento = modalitaPagamento;
  }

  public ValidazioneCliente licenzaValida(Boolean licenzaValida) {
    this.licenzaValida = licenzaValida;
    return this;
  }

   /**
   * Indica se il cliente ha un licenza valida
   * @return licenzaValida
  **/
  @ApiModelProperty(value = "Indica se il cliente ha un licenza valida")
  public Boolean isLicenzaValida() {
    return licenzaValida;
  }

  public void setLicenzaValida(Boolean licenzaValida) {
    this.licenzaValida = licenzaValida;
  }

  public ValidazioneCliente cartaIdentitaLegRapp(Boolean cartaIdentitaLegRapp) {
    this.cartaIdentitaLegRapp = cartaIdentitaLegRapp;
    return this;
  }

   /**
   * Indica se il contatto di tipo Legale rappresentante  del cliente ha compilato il campo relativo al numero della  carta d&#39;identità
   * @return cartaIdentitaLegRapp
  **/
  @ApiModelProperty(value = "Indica se il contatto di tipo Legale rappresentante  del cliente ha compilato il campo relativo al numero della  carta d'identità")
  public Boolean isCartaIdentitaLegRapp() {
    return cartaIdentitaLegRapp;
  }

  public void setCartaIdentitaLegRapp(Boolean cartaIdentitaLegRapp) {
    this.cartaIdentitaLegRapp = cartaIdentitaLegRapp;
  }

  public ValidazioneCliente dataAccettPrat(Boolean dataAccettPrat) {
    this.dataAccettPrat = dataAccettPrat;
    return this;
  }

   /**
   * Indica se il cliente ha valorizzata la data accettazione pratica
   * @return dataAccettPrat
  **/
  @ApiModelProperty(value = "Indica se il cliente ha valorizzata la data accettazione pratica")
  public Boolean isDataAccettPrat() {
    return dataAccettPrat;
  }

  public void setDataAccettPrat(Boolean dataAccettPrat) {
    this.dataAccettPrat = dataAccettPrat;
  }

  public ValidazioneCliente fidoMensile(Boolean fidoMensile) {
    this.fidoMensile = fidoMensile;
    return this;
  }

   /**
   * Indica se il cliente ha fido mensile
   * @return fidoMensile
  **/
  @ApiModelProperty(value = "Indica se il cliente ha fido mensile")
  public Boolean isFidoMensile() {
    return fidoMensile;
  }

  public void setFidoMensile(Boolean fidoMensile) {
    this.fidoMensile = fidoMensile;
  }

  public ValidazioneCliente forzLetSped(Boolean forzLetSped) {
    this.forzLetSped = forzLetSped;
    return this;
  }

   /**
   * Forzatura lettere di spedizione
   * @return forzLetSped
  **/
  @ApiModelProperty(value = "Forzatura lettere di spedizione")
  public Boolean isForzLetSped() {
    return forzLetSped;
  }

  public void setForzLetSped(Boolean forzLetSped) {
    this.forzLetSped = forzLetSped;
  }

  public ValidazioneCliente dataMaxForzLetSped(OffsetDateTime dataMaxForzLetSped) {
    this.dataMaxForzLetSped = dataMaxForzLetSped;
    return this;
  }

   /**
   * Data massima forzatura lettere di spedizione
   * @return dataMaxForzLetSped
  **/
  @ApiModelProperty(value = "Data massima forzatura lettere di spedizione")
  public OffsetDateTime getDataMaxForzLetSped() {
    return dataMaxForzLetSped;
  }

  public void setDataMaxForzLetSped(OffsetDateTime dataMaxForzLetSped) {
    this.dataMaxForzLetSped = dataMaxForzLetSped;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ValidazioneCliente validazioneCliente = (ValidazioneCliente) o;
    return Objects.equals(this.codiceCliente, validazioneCliente.codiceCliente) &&
        Objects.equals(this.attivo, validazioneCliente.attivo) &&
        Objects.equals(this.fideiussioni, validazioneCliente.fideiussioni) &&
        Objects.equals(this.modalitaPagamento, validazioneCliente.modalitaPagamento) &&
        Objects.equals(this.licenzaValida, validazioneCliente.licenzaValida) &&
        Objects.equals(this.cartaIdentitaLegRapp, validazioneCliente.cartaIdentitaLegRapp) &&
        Objects.equals(this.dataAccettPrat, validazioneCliente.dataAccettPrat) &&
        Objects.equals(this.fidoMensile, validazioneCliente.fidoMensile) &&
        Objects.equals(this.forzLetSped, validazioneCliente.forzLetSped) &&
        Objects.equals(this.dataMaxForzLetSped, validazioneCliente.dataMaxForzLetSped);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codiceCliente, attivo, fideiussioni, modalitaPagamento, licenzaValida, cartaIdentitaLegRapp, dataAccettPrat, fidoMensile, forzLetSped, dataMaxForzLetSped);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ValidazioneCliente {\n");

    sb.append("    codiceCliente: ").append(toIndentedString(codiceCliente)).append("\n");
    sb.append("    attivo: ").append(toIndentedString(attivo)).append("\n");
    sb.append("    fideiussioni: ").append(toIndentedString(fideiussioni)).append("\n");
    sb.append("    modalitaPagamento: ").append(toIndentedString(modalitaPagamento)).append("\n");
    sb.append("    licenzaValida: ").append(toIndentedString(licenzaValida)).append("\n");
    sb.append("    cartaIdentitaLegRapp: ").append(toIndentedString(cartaIdentitaLegRapp)).append("\n");
    sb.append("    dataAccettPrat: ").append(toIndentedString(dataAccettPrat)).append("\n");
    sb.append("    fidoMensile: ").append(toIndentedString(fidoMensile)).append("\n");
    sb.append("    forzLetSped: ").append(toIndentedString(forzLetSped)).append("\n");
    sb.append("    dataMaxForzLetSped: ").append(toIndentedString(dataMaxForzLetSped)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

