package it.fai.ms.efservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = true)
public class ApplicationProperties {

  public static class Infinispan {

    private boolean clusterEnabled;

    public boolean isClusterEnabled() {
      return clusterEnabled;
    }

    public void setClusterEnabled(boolean _clusterEnabled) {
      clusterEnabled = _clusterEnabled;
    }

  }

  private final Infinispan infinispan = new Infinispan();

  public Infinispan getInfinispan() {
    return infinispan;
  }

  private String hostManagementHazelcast = null;

  public String getHostManagementHazelcast() {
    return hostManagementHazelcast;
  }

  public void setHostManagementHazelcast(String hostManagementHazelcast) {
    this.hostManagementHazelcast = hostManagementHazelcast;
  }

  private final Scheduler scheduler = new Scheduler();

  public static class Scheduler {

    private boolean enable;

    private String ordinicron;

    private String cacheordini;

    private String forcedevicetosend;
    
    private String deviceexpired;

    public boolean isEnable() {
      return enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }

    public String getOrdinicron() {
      return ordinicron;
    }

    public void setOrdinicron(String ordinicron) {
      this.ordinicron = ordinicron;
    }

    public String getCacheordini() {
      return cacheordini;
    }

    public void setCacheordini(String cacheordini) {
      this.cacheordini = cacheordini;
    }

    public String getForcedevicetosend() {
      return forcedevicetosend;
    }

    public void setForcedevicetosend(String forcedevicetosend) {
      this.forcedevicetosend = forcedevicetosend;
    }
    
    public String getDeviceexpired() {
      return deviceexpired;
    }

    public void setDeviceexpired(String deviceexpired) {
      this.deviceexpired = deviceexpired;
    }

    @Override
    public String toString() {
      return "CronScheduler [enable=" + enable + ", ordinicron=" + ordinicron + ", cacheordini=" + cacheordini + ", forcedevicetosend="
             + forcedevicetosend + "]";
    }

  }

  public Scheduler getScheduler() {
    return scheduler;
  }

}
