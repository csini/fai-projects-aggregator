package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.service.DeliveryAsyncServiceExt;
import it.fai.ms.efservice.service.DeliveryService;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;
import it.fai.ms.efservice.web.rest.errors.Errno;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.CompletableFuture;

@Service
public class DeliveryAsyncServiceExtImpl implements DeliveryAsyncServiceExt {
  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AsyncJobServiceImpl asyncJobService;
  private final DeliveryService deliveryService;

  public DeliveryAsyncServiceExtImpl(
    AsyncJobServiceImpl asyncJobService,
    DeliveryService deliveryService
  ) {
    this.asyncJobService = asyncJobService;
    this.deliveryService = deliveryService;
  }

  @Override
  public void processAsyncDelivery(String identificativoJob, DeliveryPrintDTO dto) {
    log.debug("Call async fro processing print deliveries: dto {}", dto);

    CompletableFuture.supplyAsync(() -> deliveryService.process(dto))
      .exceptionally(e -> {
        log.error("Exception in Delivery Resource method processAsyncDelivery: {}", e);
        AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, null);
        response.add(Errno.EXCEPTION_ON_PROCESSING_PRINT);
        return response;
      })
      .thenAccept(state -> asyncJobService.save(identificativoJob, state));
  }

  @Override
  public void processAsyncDeliveryProduttore(String identificativoJob, DeliveryPrintDTO dto) {
    log.debug("Call async for processing print deliveries for produttore: dto {}", dto);

    CompletableFuture.supplyAsync(() -> deliveryService.processProduttore(dto))
      .exceptionally(e -> {
        log.error("Exception in Delivery Resource method processAsyncDeliveryProduttore: {}", e);
        AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, null);
        response.add(Errno.EXCEPTION_ON_PROCESSING_PRINT);
        return response;
      })
      .thenAccept(state -> asyncJobService.save(identificativoJob, state));
  }

  @Override
  public String getKeyCacheAndPushDefaultValue(String key) {
    return asyncJobService.generateKeyCacheAndPushValue(
      key, new AsyncJobResponse(StateChangeStatusFsm.PENDING, null)
    );
  }
}
