package it.fai.ms.efservice.rules.engine;

import java.util.Optional;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public interface PrecalculatedRuleService {

  Optional<RuleOutcome> execute(RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId);

  Optional<RuleOutcome> execute(RuleEngineServiceTypeId serviceTypeId, RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId);

  Optional<RuleOutcome> execute(RuleEngineVehicleId vehicleId);

}
