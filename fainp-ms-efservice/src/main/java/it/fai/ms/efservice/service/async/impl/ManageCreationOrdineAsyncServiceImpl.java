package it.fai.ms.efservice.service.async.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.enumeration.StateCreationRichiesta;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import it.fai.ms.efservice.service.async.ManageCreationOrdineAsyncService;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CreatedRichiestasAsyncDTO;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;

@Service
@Transactional
public class ManageCreationOrdineAsyncServiceImpl implements ManageCreationOrdineAsyncService {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());

  @Autowired
  JmsProperties jmsProperties;

  final RichiestaServiceExt richiestaServiceExt;

  final DispositivoRepositoryExt deviceRepoExt;

  public enum TipoServizioNecessaryBooklet {
    PEDAGGI_POLONIA, PEDAGGI_BELGIO, PEDAGGI_AUSTRIA;
  }

  public ManageCreationOrdineAsyncServiceImpl(RichiestaServiceExt _richiestaServiceExt,
                                              TipoDispositivoServiceExt _tipoDispositivoServiceExt,
                                              VehicleWizardCacheServiceExt _targaWizardVehicleCacheService,
                                              FsmRichiestaUtils _fsmRichiestaUtils,
                                              DeviceServiceActivationMessageProducerService _activationProducerService,
                                              DispositivoRepositoryExt _deviceRepoExt) {
    richiestaServiceExt = _richiestaServiceExt;
    deviceRepoExt = _deviceRepoExt;
  }

  public CreatedRichiestasAsyncDTO generateOrdineAsync(CarrelloDTO carrelloDTO) {
    CreatedRichiestasAsyncDTO createdRichiestasAsyncDTO = null;
    try {
      createdRichiestasAsyncDTO = createRichiesteForOrdineAsyncByCarrelloDTO(carrelloDTO);
      createdRichiestasAsyncDTO.setState(StateCreationRichiesta.SUCCESS);
    } catch (Exception e) {
      throw new RuntimeException("Exception in creation richiesta: ", e);
    }

    return createdRichiestasAsyncDTO;
  }

  public CreatedRichiestasAsyncDTO createRichiesteForOrdineAsyncByCarrelloDTO(CarrelloDTO carrelloDTO) throws Exception {
    log.info("Call async creation richiestas...");
    CreatedRichiestasAsyncDTO asyncCreatedRichiestaDTO = new CreatedRichiestasAsyncDTO(StateCreationRichiesta.PENDING);
    try {
      log.debug("Create richiestas....");
      List<Richiesta> richieste = richiestaServiceExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrelloDTO);

      // TODO put this in jms queue
      if (richieste != null) {
        richieste.stream()
                 .forEach(r -> JmsQueueSenderUtil.publish(jmsProperties, JmsQueueNames.RICHIESTA_CREATION_STEP2, r.getIdentificativo()));
        String numeroOrdineCliente = getNumeroOrdineByRichieste(richieste);
        asyncCreatedRichiestaDTO.setNumeroOrdineCliente(numeroOrdineCliente);
      }
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new Exception(e);
    }
    return asyncCreatedRichiestaDTO;
  }

  private String getNumeroOrdineByRichieste(List<Richiesta> richieste) {
    if (richieste != null && !richieste.isEmpty()) {
      OrdineCliente ordineCliente = richieste.get(0)
                                             .getOrdineCliente();
      return ordineCliente.getNumeroOrdine();
    }
    return null;
  }
}
