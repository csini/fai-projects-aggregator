package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Spring Data JPA repository for the AnagraficaGiacenza entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnagraficaGiacenzaRepository extends JpaRepository<AnagraficaGiacenza, Long> {
  Optional<AnagraficaGiacenza> findByTipoDispositivo(String tipoDispositivo);

  @Modifying
  @Query("UPDATE AnagraficaGiacenza a SET a.notificationSend = false")
  @Transactional
  void resetSendFlag();
}
