package it.fai.ms.efservice.service.fsm.type.localizzatoresat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.localizzatoresat.FsmModificaLocalizzatoreSatMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaLocalizzatoreSatMalfunzionamento.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO)
public class FsmModificaLocalizzatoreSatMalfunzionamento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO = "fsmModificaLocalizzatoreSatMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmModificaLocalizzatoreSatMalfunzionamento(@Qualifier(FsmModificaLocalizzatoreSatMalfunzionamentoConfig.MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                                     FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO;
    this.deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.LOCALIZZAZIONE_SATELITTARE };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
