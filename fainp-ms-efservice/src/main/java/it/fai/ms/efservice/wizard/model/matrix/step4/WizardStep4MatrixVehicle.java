package it.fai.ms.efservice.wizard.model.matrix.step4;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixVehicle;

public class WizardStep4MatrixVehicle extends WizardMatrixVehicle {

  private static final long serialVersionUID = 49802521087752263L;

  private Map<String, WizardStep4MatrixDeviceType> deviceTypes = new LinkedHashMap<>();
  private String                                   type;

  public WizardStep4MatrixVehicle(final String _id) {
    super(_id);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardStep4MatrixVehicle) _obj).getDeviceTypes(), deviceTypes)
                 && Objects.equals(((WizardStep4MatrixVehicle) _obj).getEuroClass(), getEuroClass())
                 && Objects.equals(((WizardStep4MatrixVehicle) _obj).getLicensePlate(), getLicensePlate())
                 && Objects.equals(((WizardStep4MatrixVehicle) _obj).getTargaNazione(), getTargaNazione())
                 && Objects.equals(((WizardStep4MatrixVehicle) _obj).getUuid(), getUuid())
                 && Objects.equals(((WizardStep4MatrixVehicle) _obj).getType(), type);
    }
    return isEquals;
  }

  public Map<String, WizardStep4MatrixDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public String getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceTypes, getEuroClass(), getLicensePlate(), getTargaNazione(), getUuid(), type);
  }

  public void setType(final String _type) {
    type = _type;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(getClass().getSimpleName() + " [deviceTypes=");
    builder.append(this.deviceTypes);
    builder.append(",type=");
    builder.append(type);
    builder.append(",deviceTypes=");
    builder.append(getDeviceTypes());
    builder.append(",type=");
    builder.append(getType());
    builder.append("]");
    return builder.toString();
  }

}
