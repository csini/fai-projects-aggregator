package it.fai.ms.efservice.service.fsm.guard;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardTipoRichiesta implements Guard<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final List<TipoRichiesta> requestTypes;

  public GuardTipoRichiesta(TipoRichiesta requestType) {
    this.requestTypes = Arrays.asList(requestType);
  }

  public GuardTipoRichiesta(List<TipoRichiesta> requestTypes) {
    this.requestTypes = requestTypes;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean isRequestType = false;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        TipoRichiesta tipoRichiesta = richiesta.getTipo();
        Optional<TipoRichiesta> optRequestType = requestTypes.stream()
                                                             .filter(requestType -> requestType.name()
                                                                                               .equals(tipoRichiesta.name()))
                                                             .findFirst();
        if (optRequestType.isPresent()) {
          log.info("Request Type found: {}", tipoRichiesta);
          isRequestType = true;
        }
      }
    }

    return isRequestType;
  }

}
