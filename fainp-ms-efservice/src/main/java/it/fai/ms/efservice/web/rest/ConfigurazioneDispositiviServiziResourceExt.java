package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

/**
 * REST controller for managing ConfigurazioneDispositiviServizi.
 */
@RestController
@RequestMapping(ConfigurazioneDispositiviServiziResourceExt.BASE_PATH)
public class ConfigurazioneDispositiviServiziResourceExt {

  private final Logger log = LoggerFactory.getLogger(ConfigurazioneDispositiviServiziResourceExt.class);

  protected static final String BASE_PATH = "/api/public";

  private static final String CONFIGURATION = "/configurazione";

  private final ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService;

  public ConfigurazioneDispositiviServiziResourceExt(ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService) {
    this.configurazioneDispositiviServiziService = configurazioneDispositiviServiziService;
  }

  /**
   * @param tipoDispositivo
   * @param nomeServizio
   * @return
   * @throws URISyntaxException
   * @throws CustomException
   */
  @GetMapping(CONFIGURATION + "/{tipoDispositivo}/{servizio}")
  @Timed
  public ResponseEntity<ConfigurazioneDispositiviServizi> getConfigByTipoDispositivoServizio(@PathVariable(
                                                                                                           value = "tipoDispositivo",
                                                                                                           required = true) TipoDispositivoEnum tipoDispositivo,
                                                                                             @PathVariable(
                                                                                                           value = "servizio",
                                                                                                           required = true) String nomeServizio) throws URISyntaxException,
                                                                                                                                                 CustomException {
    log.debug("REST request to get configurazione dispositivo {} servizio {}", tipoDispositivo, nomeServizio);
    Optional<ConfigurazioneDispositiviServizi> resultOpt = configurazioneDispositiviServiziService.findConfigurazione(tipoDispositivo,
                                                                                                                      nomeServizio);
    if (!resultOpt.isPresent()) {
      throw CustomException.builder(HttpStatus.NOT_FOUND)
                           .add(Errno.NOT_FOUND_CONFIGURATION_DISPOSITIVO_SERVIZIO);
    }

    return ResponseEntity.ok()
                         .body(resultOpt.get());
  }

  /**
   * @param tipoDispositivo
   * @param nomeServizio
   * @param modalitaGestione
   * @return
   * @throws URISyntaxException
   * @throws CustomException
   */
  @PostMapping(CONFIGURATION + "/{tipoDispositivo}/{servizio}/{modalitaGestione}")
  @Timed
  public ResponseEntity<ConfigurazioneDispositiviServizi> getConfigByTipoDispositivoServizioModalitaGestione(@PathVariable(
                                                                                                                           value = "tipoDispositivo",
                                                                                                                           required = true) TipoDispositivoEnum tipoDispositivo,
                                                                                                             @PathVariable(
                                                                                                                           value = "servizio",
                                                                                                                           required = true) String nomeServizio,
                                                                                                             @PathVariable(
                                                                                                                           value = "modalitaGestione",
                                                                                                                           required = true) ModalitaGestione modalitaGestione) throws URISyntaxException,
                                                                                                                                                                               CustomException {
    log.debug("REST request to get configurazione dispositivo {} servizio {} managed {}", tipoDispositivo, nomeServizio, modalitaGestione);
    Optional<ConfigurazioneDispositiviServizi> resultOpt = configurazioneDispositiviServiziService.findConfigurazione(tipoDispositivo,
                                                                                                                      nomeServizio,
                                                                                                                      modalitaGestione);
    if (!resultOpt.isPresent()) {
      throw CustomException.builder(HttpStatus.NOT_FOUND)
                           .add(Errno.NOT_FOUND_CONFIGURATION_DISPOSITIVO_SERVIZIO);
    }

    return ResponseEntity.ok()
                         .body(resultOpt.get());
  }

}
