package it.fai.ms.efservice.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.*; // for static metamodels
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniCriteria;

import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;
import it.fai.ms.efservice.service.mapper.IndirizzoSpedizioneOrdiniMapper;

/**
 * Service for executing complex queries for IndirizzoSpedizioneOrdini entities in the database.
 * The main input is a {@link IndirizzoSpedizioneOrdiniCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link IndirizzoSpedizioneOrdiniDTO} or a {@link Page} of {%link IndirizzoSpedizioneOrdiniDTO} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class IndirizzoSpedizioneOrdiniQueryService extends QueryService<IndirizzoSpedizioneOrdini> {

    private final Logger log = LoggerFactory.getLogger(IndirizzoSpedizioneOrdiniQueryService.class);


    private final IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepository;

    private final IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioneOrdiniMapper;

    public IndirizzoSpedizioneOrdiniQueryService(IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepository, IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioneOrdiniMapper) {
        this.indirizzoSpedizioneOrdiniRepository = indirizzoSpedizioneOrdiniRepository;
        this.indirizzoSpedizioneOrdiniMapper = indirizzoSpedizioneOrdiniMapper;
    }

    /**
     * Return a {@link List} of {%link IndirizzoSpedizioneOrdiniDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndirizzoSpedizioneOrdiniDTO> findByCriteria(IndirizzoSpedizioneOrdiniCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<IndirizzoSpedizioneOrdini> specification = createSpecification(criteria);
        return indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdiniRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {%link IndirizzoSpedizioneOrdiniDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndirizzoSpedizioneOrdiniDTO> findByCriteria(IndirizzoSpedizioneOrdiniCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<IndirizzoSpedizioneOrdini> specification = createSpecification(criteria);
        final Page<IndirizzoSpedizioneOrdini> result = indirizzoSpedizioneOrdiniRepository.findAll(specification, page);
        return result.map(indirizzoSpedizioneOrdiniMapper::toDto);
    }

    /**
     * Function to convert IndirizzoSpedizioneOrdiniCriteria to a {@link Specifications}
     */
    private Specifications<IndirizzoSpedizioneOrdini> createSpecification(IndirizzoSpedizioneOrdiniCriteria criteria) {
        Specifications<IndirizzoSpedizioneOrdini> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), IndirizzoSpedizioneOrdini_.id));
            }
            if (criteria.getVia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVia(), IndirizzoSpedizioneOrdini_.via));
            }
            if (criteria.getCitta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCitta(), IndirizzoSpedizioneOrdini_.citta));
            }
            if (criteria.getCap() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCap(), IndirizzoSpedizioneOrdini_.cap));
            }
            if (criteria.getProvincia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvincia(), IndirizzoSpedizioneOrdini_.provincia));
            }
            if (criteria.getPaese() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPaese(), IndirizzoSpedizioneOrdini_.paese));
            }
            if (criteria.getFai() != null) {
                specification = specification.and(buildSpecification(criteria.getFai(), IndirizzoSpedizioneOrdini_.fai));
            }
        }
        return specification;
    }

}
