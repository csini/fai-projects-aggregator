package it.fai.ms.efservice.wizard.repository;

import static java.util.stream.Collectors.toSet;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.infinispan.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

@Service
public class WizardVehicleRepositoryImpl implements WizardVehicleRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private Map<String, WizardVehicleEntity> persistentCache;

  @Autowired
  public WizardVehicleRepositoryImpl(@Qualifier("wizard_vehicle") final Cache<String, WizardVehicleEntity> _persistentCache) {
    persistentCache = _persistentCache;
  }

  @Override
  @Transactional(readOnly = true)
  public Integer count() {
    int size = persistentCache.size();
    _log.info("Size: {}", size);
    return size;
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<WizardVehicle> find(final String _uuid) {
    _log.debug("Try to restore WizardVehicle from Infinispan Repository: {}", _uuid);
    WizardVehicle wizardVehicle = null;
    final WizardVehicleEntity wizardVehicleEntity = restore(_uuid);
    if (wizardVehicleEntity != null) {
      wizardVehicle = mapToWizardVehicle(wizardVehicleEntity);
      _log.debug("Found WizardVehicleEntity {} for {} (converted to {})", wizardVehicleEntity, _uuid, wizardVehicle);
    } else {
      _log.warn("WizardVehicle not on Infinispan Repository: {}", _uuid);
    }
    return Optional.ofNullable(wizardVehicle);
  }

  @Override
  @Transactional
  public void save(final WizardVehicle _wizardVehicle) {
    _log.debug("Try to persist WizardVehicle in Infinispan Repository: {}", _wizardVehicle);
    persist(_wizardVehicle.getId()
                          .getId(),
            convertWizardVehicle(_wizardVehicle));
    _log.info("Persisted {} in Infinispan Repository: ", _wizardVehicle);
  }

  /**
   * internal use
   */
  @Override
  @Transactional(readOnly = true)
  public WizardVehicleEntity remove(final String _uuid) {
    _log.debug("Try to remove WizardVehicle in Infinispan Repository: {}", _uuid);
    WizardVehicleEntity wizardVehicleEntity = persistentCache.remove(_uuid);
    _log.info("Removed {} in Infinispan Repository: ", _uuid);
    return wizardVehicleEntity;
  }

  private WizardVehicleEntity convertWizardVehicle(final WizardVehicle _wizardVehicle) {
    WizardVehicleEntity entity = new WizardVehicleEntity(_wizardVehicle.getId()
                                                                       .getId());
    entity.setEuroClass(_wizardVehicle.getEuroClass());
    entity.setLicensePlate(_wizardVehicle.getLicensePlate());
    entity.setState(_wizardVehicle.getState());
    entity.setType(_wizardVehicle.getType());
    entity.setLibretto(_wizardVehicle.getLibretto());
    entity.setCountry(_wizardVehicle.getCountry());
    return entity;
  }

  private WizardVehicle mapToWizardVehicle(final WizardVehicleEntity _vehicleEntity) {
    final WizardVehicle wizardVehicle = new WizardVehicle(new WizardVehicleId(_vehicleEntity.getUuid()));
    wizardVehicle.setEuroClass(_vehicleEntity.getEuroClass());
    wizardVehicle.setLicensePlate(_vehicleEntity.getLicensePlate());
    wizardVehicle.setState(_vehicleEntity.getState());
    wizardVehicle.setType(_vehicleEntity.getType());
    wizardVehicle.setLibretto(_vehicleEntity.getLibretto());
    wizardVehicle.setCountry(_vehicleEntity.getCountry());
    return wizardVehicle;
  }

  private void persist(final String _wizardVehicleId, final WizardVehicleEntity _wizardVehicleEntity) {
    WizardVehicleEntity put = persistentCache.put(_wizardVehicleId, _wizardVehicleEntity);
    if (put != null) {
      _log.debug("  Cache value updated for key {}", _wizardVehicleId);
    } else {
      _log.debug("  Cache value created for key {}", _wizardVehicleId);
    }
  }

  private WizardVehicleEntity restore(final String _wizardVehicleId) {
    return persistentCache.get(_wizardVehicleId);
  }

  @Override
  public Set<WizardVehicle> find(Set<String> uuidVehicles) {
    return uuidVehicles.parallelStream()
                       .map(uuid -> find(uuid))
                       .filter(opt -> opt.isPresent())
                       .map(opt -> opt.get())
                       .collect(toSet());
  }

}
