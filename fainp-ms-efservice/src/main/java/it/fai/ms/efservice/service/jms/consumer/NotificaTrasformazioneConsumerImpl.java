package it.fai.ms.efservice.service.jms.consumer;

import static it.fai.ms.efservice.domain.StatoContratto.REVOCATO;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.anagazienda.TrasformazioneDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;

@Service
public class NotificaTrasformazioneConsumerImpl implements NotificaTrasformazioneConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ClienteFaiServiceExt clienteFaiServiceExt;
  private final ContrattoServiceExt  contractService;

  public NotificaTrasformazioneConsumerImpl(ClienteFaiServiceExt clienteFaiServiceExt, ContrattoServiceExt contractService) {
    this.clienteFaiServiceExt = clienteFaiServiceExt;
    this.contractService = contractService;
  }

  @Override
  @Transactional
  public void consume(TrasformazioneDTO notificaTrasformazione) {
    log.info("Received notificaTrasformazione:=[{}]", notificaTrasformazione);
    Optional.ofNullable(notificaTrasformazione)
            .orElseThrow(() -> new IllegalArgumentException("Wrong notificaTrasformazione received! notificaTrasformazione:="
                                                            + notificaTrasformazione));
    Optional.ofNullable(notificaTrasformazione.getCodiceAziendaA())
            .orElseThrow(() -> new IllegalArgumentException("NotificaTrasformazione has codiceAziendaA null! notificaTrasformazione:="
                                                            + notificaTrasformazione));
    Optional.ofNullable(notificaTrasformazione.getCodiceAziendaDa())
            .orElseThrow(() -> new IllegalArgumentException("NotificaTrasformazione has codiceAziendaDa null! notificaTrasformazione:="
                                                            + notificaTrasformazione));

    final String codiceAziendaDa = notificaTrasformazione.getCodiceAziendaDa();
    final String codiceAziendaA = notificaTrasformazione.getCodiceAziendaA();

    ClienteFai clienteDa = clienteFaiServiceExt.findByCodiceCliente(codiceAziendaDa);
    if (clienteDa == null) {
      log.warn("Received codiceAziendaDa [{}], but no ClienteFai with this code found!!", codiceAziendaDa);
      // FIXME send anomalia?
      return;
    }
    ClienteFai clienteA = clienteFaiServiceExt.findByCodiceCliente(codiceAziendaA);
    if (clienteA == null) {
      log.warn("Received codiceAziendaA [{}], but no ClienteFai with this code found!!", codiceAziendaA);
      // FIXME send anomalia?
      return;
    }

    log.debug("Retrieving contratti from clienteDa:=[{}]", clienteDa);
    List<Contratto> contrattiClienteDa = clienteDa.getContrattos()
                                                  .stream()
                                                  .filter(contratto -> REVOCATO != contratto.getStato())
                                                  .collect(Collectors.toList());

    // rimuovo tutti i contratti non revocati
    clienteDa.getContrattos()
             .removeAll(contrattiClienteDa);

    log.debug("Retrieved contrattiClienteDa:=[{}]", contrattiClienteDa);

    log.debug("Retrieving contratti from clienteA:=[{}]", clienteA);
    List<Contratto> contrattiClienteA = clienteA.getContrattos()
                                                .stream()
                                                .filter(contratto -> REVOCATO != contratto.getStato())
                                                .collect(Collectors.toList());
    log.debug("Retrieved contrattiClienteA:=[{}]", contrattiClienteA);

    log.debug("Assigning contrattiClienteDa to clienteA");
    contrattiClienteDa.forEach(contrattoClienteDa -> {
      contrattoClienteDa.setClienteFai(clienteA);
      if (haveSameProduttore(contrattiClienteA, contrattoClienteDa)) {
        contrattoClienteDa.setPrimario(false);
      }
      OffsetDateTime changedate = notificaTrasformazione.getData();
      if(changedate ==null)
        changedate = OffsetDateTime.now();
      contrattoClienteDa.setDataModificaStato(changedate.toInstant());
      contrattoClienteDa = contractService.save(contrattoClienteDa);
      log.info("Add contract [{}] to new cliente [{}]", contrattoClienteDa, clienteA);
      clienteA.getContrattos()
              .add(contrattoClienteDa);
    });

    clienteFaiServiceExt.save(clienteDa);
    clienteFaiServiceExt.save(clienteA);
  }

  public boolean haveSameProduttore(List<Contratto> contrattiClienteA, Contratto contrattoClienteDa) {
    return contrattiClienteA.stream()
                            .filter(contrattoClienteA -> contrattoClienteA.getProduttore()
                                                                          .getId()
                                                                          .equals(contrattoClienteDa.getProduttore()
                                                                                                    .getId()))
                            .count() >= 1;
  }
}
