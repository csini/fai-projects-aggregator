package it.fai.ms.efservice.service.fsm.type.assistenza.veicoli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.assistenza.veicoli.FsmInoltroAssistenzaVeicoliConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroAssistenzaVeicoli.FSM_INOLTRO_ASSISTENZA_VEICOLI)
public class FsmInoltroAssistenzaVeicoli extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_ASSISTENZA_VEICOLI = "fsmInoltroAssistenzaVeicoli";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroAssistenzaVeicoli(@Qualifier(FsmInoltroAssistenzaVeicoliConfig.INOLTRO_ASSISTENZA_VEICOLI) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                       FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_ASSISTENZA_VEICOLI;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.ASSISTENZA_VEICOLI };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}

/* copiato da FsmInoltroHGV  */
