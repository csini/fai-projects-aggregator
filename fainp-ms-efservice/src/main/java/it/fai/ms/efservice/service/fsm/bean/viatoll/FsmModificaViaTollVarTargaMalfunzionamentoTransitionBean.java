package it.fai.ms.efservice.service.fsm.bean.viatoll;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmModificaViaTollVarTargaMalfunzionamento;

@WithStateMachine(id = FsmModificaViaTollVarTargaMalfunzionamento.FSM_MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO)
public class FsmModificaViaTollVarTargaMalfunzionamentoTransitionBean extends AbstractFsmRichiestaTransition {
}
