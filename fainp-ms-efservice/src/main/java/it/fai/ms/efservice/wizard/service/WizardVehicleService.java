package it.fai.ms.efservice.wizard.service;

import java.util.Optional;
import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

public interface WizardVehicleService {

  void addVehicle(WizardVehicle wizardVehicle);

  Integer count();

  Optional<WizardVehicle> getVehicle(WizardVehicleId wizardVehicleId);

  boolean hasActiveService(WizardVehicleId wizardVehicleId, WizardDeviceTypeId wizardDeviceTypeId, WizardServiceTypeId wizardServiceTypeId);

  boolean hasActiveServices(WizardVehicleId wizardVehicleId);

  boolean hasActiveServices(WizardVehicleId wizardVehicleId, WizardDeviceTypeId wizardDeviceTypeId);

  boolean hasAnomalies(WizardVehicle wizardVehicle, WizardDeviceTypeId wizardDeviceTypeId);

  boolean hasOngoingRequest(WizardVehicle wizardVehicle);

  boolean hasOngoingRequest(WizardVehicle wizardVehicle, WizardDeviceTypeId wizardDeviceTypeId);

}
