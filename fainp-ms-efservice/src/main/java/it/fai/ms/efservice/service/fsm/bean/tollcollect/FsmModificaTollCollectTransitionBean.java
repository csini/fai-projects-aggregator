package it.fai.ms.efservice.service.fsm.bean.tollcollect;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

@WithStateMachine(id = "fsmModificaTollCollect")
public class FsmModificaTollCollectTransitionBean extends AbstractFsmRichiestaTransition {
}
