package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.TipoServizioService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoServizio.
 */
@RestController
@RequestMapping("/api")
public class TipoServizioResource {

    private final Logger log = LoggerFactory.getLogger(TipoServizioResource.class);

    private static final String ENTITY_NAME = "tipoServizio";

    private final TipoServizioService tipoServizioService;

    public TipoServizioResource(TipoServizioService tipoServizioService) {
        this.tipoServizioService = tipoServizioService;
    }

    /**
     * POST  /tipo-servizios : Create a new tipoServizio.
     *
     * @param tipoServizioDTO the tipoServizioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoServizioDTO, or with status 400 (Bad Request) if the tipoServizio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-servizios")
    @Timed
    public ResponseEntity<TipoServizioDTO> createTipoServizio(@Valid @RequestBody TipoServizioDTO tipoServizioDTO) throws URISyntaxException {
        log.debug("REST request to save TipoServizio : {}", tipoServizioDTO);
        if (tipoServizioDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipoServizio cannot already have an ID")).body(null);
        }
        TipoServizioDTO result = tipoServizioService.save(tipoServizioDTO);
        return ResponseEntity.created(new URI("/api/tipo-servizios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-servizios : Updates an existing tipoServizio.
     *
     * @param tipoServizioDTO the tipoServizioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoServizioDTO,
     * or with status 400 (Bad Request) if the tipoServizioDTO is not valid,
     * or with status 500 (Internal Server Error) if the tipoServizioDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-servizios")
    @Timed
    public ResponseEntity<TipoServizioDTO> updateTipoServizio(@Valid @RequestBody TipoServizioDTO tipoServizioDTO) throws URISyntaxException {
        log.debug("REST request to update TipoServizio : {}", tipoServizioDTO);
        if (tipoServizioDTO.getId() == null) {
            return createTipoServizio(tipoServizioDTO);
        }
        TipoServizioDTO result = tipoServizioService.save(tipoServizioDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoServizioDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-servizios : get all the tipoServizios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipoServizios in body
     */
    @GetMapping("/tipo-servizios")
    @Timed
    public List<TipoServizioDTO> getAllTipoServizios() {
        log.debug("REST request to get all TipoServizios");
        return tipoServizioService.findAll();
        }

    /**
     * GET  /tipo-servizios/:id : get the "id" tipoServizio.
     *
     * @param id the id of the tipoServizioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoServizioDTO, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-servizios/{id}")
    @Timed
    public ResponseEntity<TipoServizioDTO> getTipoServizio(@PathVariable Long id) {
        log.debug("REST request to get TipoServizio : {}", id);
        TipoServizioDTO tipoServizioDTO = tipoServizioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoServizioDTO));
    }

    /**
     * DELETE  /tipo-servizios/:id : delete the "id" tipoServizio.
     *
     * @param id the id of the tipoServizioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-servizios/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoServizio(@PathVariable Long id) {
        log.debug("REST request to delete TipoServizio : {}", id);
        tipoServizioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
