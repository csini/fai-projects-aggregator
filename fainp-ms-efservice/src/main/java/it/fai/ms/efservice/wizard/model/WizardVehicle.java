package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class WizardVehicle implements Serializable {

  private static final long serialVersionUID = -9151985217966722463L;

  private Set<WizardDeviceTypeId> deviceTypeIds    = new HashSet<>();
  private Set<WizardDevice>       devices          = new HashSet<>();
  private String                  euroClass;
  private WizardVehicleId         id;
  private String                  licensePlate;
  private String                  state;
  private String                  type;
  private String                  libretto;
  private String                  country;
  private boolean                 loadDevices      = false;
  private boolean                 hasDevicesActive = false;
  private boolean                 expiring         = false;
  private boolean                 expired          = false;

  public WizardVehicle(final WizardVehicleId _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardVehicle) _obj).getId(), id) && Objects.equals(((WizardVehicle) _obj).getEuroClass(), euroClass)
                 && Objects.equals(((WizardVehicle) _obj).getLicensePlate(), licensePlate)
                 && Objects.equals(((WizardVehicle) _obj).getState(), state)
                 && Objects.equals(((WizardVehicle) _obj).getDeviceTypeIds(), deviceTypeIds)
                 && Objects.equals(((WizardVehicle) _obj).getDevices(), devices) && Objects.equals(((WizardVehicle) _obj).getType(), type)
                 && Objects.equals(((WizardVehicle) _obj).getLibretto(), libretto);
    }
    return isEquals;
  }

  public Set<WizardDevice> getDevices() {
    return devices;
  }

  public Set<WizardDeviceTypeId> getDeviceTypeIds() {
    return deviceTypeIds;
  }

  public String getEuroClass() {
    return this.euroClass;
  }

  public WizardVehicleId getId() {
    return id;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public String getState() {
    return state;
  }

  public String getType() {
    return type;
  }

  public String getLibretto() {
    return libretto;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, euroClass, licensePlate, state, deviceTypeIds, devices, type, libretto);
  }

  public void setDeviceTypeIds(final Set<WizardDeviceTypeId> _deviceTypeIds) {
    deviceTypeIds = _deviceTypeIds;
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public void setState(final String _state) {
    state = _state;
  }

  public void setType(final String _type) {
    type = _type;
  }

  public void setLibretto(String _libretto) {
    libretto = _libretto;
  }

  public boolean isLoadDevices() {
    return loadDevices;
  }

  public void setLoadDevices() {
    this.loadDevices = true;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public boolean isHasDevicesActive() {
    return hasDevicesActive;
  }

  public void setHasDevicesActive(boolean hasDevicesActive) {
    this.hasDevicesActive = hasDevicesActive;
  }

  public boolean isExpiring() {
    return expiring;
  }

  public void setExpiring(boolean expiring) {
    this.expiring = expiring;
  }

  public boolean isExpired() {
    return expired;
  }

  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardVehicle [deviceTypeIds=");
    builder.append(this.deviceTypeIds);
    builder.append(", euroClass=");
    builder.append(this.euroClass);
    builder.append(", id=");
    builder.append(this.id);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append(", state=");
    builder.append(this.state);
    builder.append(", type=");
    builder.append(this.type);
    builder.append(", libretto=");
    builder.append(this.libretto);
    builder.append(", country=");
    builder.append(this.country);
    builder.append(", hasDevicesActive=");
    builder.append(this.hasDevicesActive);
    builder.append(", expiring=");
    builder.append(this.expiring);
    builder.append(", expired=");
    builder.append(this.expired);
    builder.append("]");
    return builder.toString();
  }

}
