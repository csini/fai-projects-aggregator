package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.ClassificazioneTipoDisp;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;

/**
 * A DTO for the TipoDispositivo entity.
 */
public class TipoDispositivoDTO implements Serializable {

  private static final long serialVersionUID = 1545976737093937738L;

  private Long id;

  private String nomeBusiness;

  private String testoPromo;

  private String descrizione;

  @NotNull
  private Boolean multiservizio;

  @NotNull
  private Boolean virtuale;

  private ClassificazioneTipoDisp classificazione;

  private ModalitaSpedizione modalitaSpedizione;

  private TipoDispositivoEnum nome;

  private Long indirizzoRientroId;

  private Long produttoreId;

  private Set<TipoServizioDTO> tipoServizios = new HashSet<>();
  
  private boolean hidden;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomeBusiness() {
    return nomeBusiness;
  }

  public void setNomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
  }

  public String getTestoPromo() {
    return testoPromo;
  }

  public void setTestoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public Boolean isMultiservizio() {
    return multiservizio;
  }

  public void setMultiservizio(Boolean multiservizio) {
    this.multiservizio = multiservizio;
  }

  public Boolean isVirtuale() {
    return virtuale;
  }

  public void setVirtuale(Boolean virtuale) {
    this.virtuale = virtuale;
  }

  public ClassificazioneTipoDisp getClassificazione() {
    return classificazione;
  }

  public void setClassificazione(ClassificazioneTipoDisp classificazione) {
    this.classificazione = classificazione;
  }

  public ModalitaSpedizione getModalitaSpedizione() {
    return modalitaSpedizione;
  }

  public void setModalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
  }

  public TipoDispositivoEnum getNome() {
    return nome;
  }

  public void setNome(TipoDispositivoEnum nome) {
    this.nome = nome;
  }

  public Long getIndirizzoRientroId() {
    return indirizzoRientroId;
  }

  public void setIndirizzoRientroId(Long indirizzoId) {
    this.indirizzoRientroId = indirizzoId;
  }

  public Long getProduttoreId() {
    return produttoreId;
  }

  public void setProduttoreId(Long produttoreId) {
    this.produttoreId = produttoreId;
  }

  public Set<TipoServizioDTO> getTipoServizios() {
    return tipoServizios;
  }

  public void setTipoServizios(Set<TipoServizioDTO> tipoServizios) {
    this.tipoServizios = tipoServizios;
  }
  
  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TipoDispositivoDTO tipoDispositivoDTO = (TipoDispositivoDTO) o;
    if (tipoDispositivoDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), tipoDispositivoDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoDispositivoDTO [id=");
    builder.append(id);
    builder.append(", nome=");
    builder.append(nome);
    builder.append(", nomeBusiness=");
    builder.append(nomeBusiness);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", multiservizio=");
    builder.append(multiservizio);
    builder.append(", virtuale=");
    builder.append(virtuale);
    builder.append(", produttoreId=");
    builder.append(produttoreId);
    builder.append(", testoPromo=");
    builder.append(testoPromo);
    builder.append(", modalitaSpedizione=");
    builder.append(modalitaSpedizione);
    builder.append(", indirizzoRientroId=");
    builder.append(indirizzoRientroId);
    builder.append(", hidden=");
    builder.append(hidden);
    builder.append(", classificazione=");
    builder.append(classificazione);
    builder.append(", tipoServizios=");
    builder.append(tipoServizios);
    builder.append("]");
    return builder.toString();
  }
  
}
