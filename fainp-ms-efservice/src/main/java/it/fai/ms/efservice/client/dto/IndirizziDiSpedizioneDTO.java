package it.fai.ms.efservice.client.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class IndirizziDiSpedizioneDTO {

  @JsonProperty("transito")
  private IndirizzoSpedizioneDTO transito = null;

  @JsonProperty("finale")
  private IndirizzoSpedizioneDTO finale = null;

  /**
   * Get transito
   * 
   * @return transito
   **/
  @ApiModelProperty(required = false, value = "")
  @NotNull
  public IndirizzoSpedizioneDTO getTransito() {
    return transito;
  }

  public void setTransito(IndirizzoSpedizioneDTO transito) {
    this.transito = transito;
  }

  /**
   * Get finale
   * 
   * @return finale
   **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public IndirizzoSpedizioneDTO getFinale() {
    return finale;
  }

  public void setFinale(IndirizzoSpedizioneDTO finale) {
    this.finale = finale;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((finale == null) ? 0 : finale.hashCode());
    result = prime * result + ((transito == null) ? 0 : transito.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    IndirizziDiSpedizioneDTO other = (IndirizziDiSpedizioneDTO) obj;
    if (finale == null) {
      if (other.finale != null)
        return false;
    } else if (!finale.equals(other.finale))
      return false;
    if (transito == null) {
      if (other.transito != null)
        return false;
    } else if (!transito.equals(other.transito))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "IndirizziDiSpedizioneDTO [transito=" + transito + ", finale=" + finale + "]";
  }
}
