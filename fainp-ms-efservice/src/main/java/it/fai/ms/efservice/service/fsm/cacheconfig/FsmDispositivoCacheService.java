package it.fai.ms.efservice.service.fsm.cacheconfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.region.Region;
import org.springframework.statemachine.state.AbstractState;
import org.springframework.statemachine.state.HistoryPseudoState;
import org.springframework.statemachine.state.PseudoState;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.AbstractStateMachine;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.FsmCacheDispositivoService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.fsm.serializer.FsmDispositivoSerializer;

@Component
public class FsmDispositivoCacheService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private FsmDispositivoSerializer                serializer;
  private final FsmCacheDispositivoService fsmCacheDispositivoService;

  public FsmDispositivoCacheService(FsmDispositivoSerializer _serializer, final FsmCacheDispositivoService _fsmCacheDispositivoService) {
    serializer = _serializer;
    fsmCacheDispositivoService = _fsmCacheDispositivoService;
  }

  @PostConstruct
  public void init() {
    log.debug("Init service Cache Dispositivo on DB");
  }

  public void putStateMachineContext(String key, String json) {
    log.debug("Put in map [key: {} - val: {}]", key, json);
    fsmCacheDispositivoService.put(Long.valueOf(key), json);
  }

  public String getStateMachineContext(String key) {
    String json = fsmCacheDispositivoService.get(Long.valueOf(key));
    log.debug("Get object from Map with Key {} Value {}", key, json);
    return json;
  }

  public String getIdStateMachine(String key) {
    String jsonSmContext = getStateMachineContext(key);

    return jsonSmContext;
  }

  public void clearCache(String key) {
    log.debug("Remove key {} from DB", key);
    fsmCacheDispositivoService.remove(Long.valueOf(key));
  }

  public void clearCache() {
    log.debug("Clear all from DB");
    fsmCacheDispositivoService.removeAll();
  }

  public void persist(StateMachine<StatoDispositivo, DispositivoEvent> stateMachine, String identifier) throws FsmExecuteCommandException {

    log.info("StateMachine to persist: {}", stateMachine.getId());

    StateMachineContext<StatoDispositivo, DispositivoEvent> buildStateMachineContext = buildStateMachineContext(stateMachine);

    if (serializer == null) {
      log.error("***  SERIALIZER is NULL  ***");
      return;
    }

    String objSerialized = serializer.serialize(buildStateMachineContext);
    putStateMachineContext(identifier, objSerialized);
  }

  public void resetIdMachineFromAnotherIdMachineByContext(String json, String identifier, String idNewMachine) {
    StateMachineContext<StatoDispositivo, DispositivoEvent> smc = serializer.deSerialize(json, idNewMachine);
    String objSerialized = serializer.serialize(smc);
    putStateMachineContext(identifier, objSerialized);
  }

  public final StateMachine<StatoDispositivo, DispositivoEvent> restore(StateMachine<StatoDispositivo, DispositivoEvent> stateMachine,
                                                                        String identifier) {
    String json = getStateMachineContext(identifier);
    if (json == null) {
      return stateMachine;
    }

    StateMachineContext<StatoDispositivo, DispositivoEvent> smc = serializer.deSerialize(json);

    if (smc == null) {
      return stateMachine;
    }

    final StateMachineContext<StatoDispositivo, DispositivoEvent> context = smc;
    stateMachine.stop();

    stateMachine.getStateMachineAccessor()
                .doWithAllRegions(access -> {
                  access.resetStateMachine(context);
                });

    stateMachine.start();
    return stateMachine;
  }

  public final StateMachine<StatoDispositivo, DispositivoEvent> restoreStateMachineFromJson(StateMachine<StatoDispositivo, DispositivoEvent> stateMachine,
                                                                                            String defaultJson) {
    StateMachineContext<StatoDispositivo, DispositivoEvent> smc = serializer.deSerialize(defaultJson);

    if (smc == null) {
      return null;
    }

    final StateMachineContext<StatoDispositivo, DispositivoEvent> context = smc;
    log.info("StateMachineContext {}", context);

    stateMachine.stop();

    stateMachine.getStateMachineAccessor()
                .doWithAllRegions(access -> {
                  access.resetStateMachine(context);
                });

    stateMachine.start();

    return stateMachine;
  }

  protected StateMachineContext<StatoDispositivo, DispositivoEvent> buildStateMachineContext(StateMachine<StatoDispositivo, DispositivoEvent> stateMachine) {
    ExtendedState extendedState = new DefaultExtendedState();
    extendedState.getVariables()
                 .putAll(stateMachine.getExtendedState()
                                     .getVariables());

    ArrayList<StateMachineContext<StatoDispositivo, DispositivoEvent>> childs = new ArrayList<>();
    StatoDispositivo id = null;
    State<StatoDispositivo, DispositivoEvent> state = stateMachine.getState();
    if (state.isSubmachineState()) {
      id = getDeepState(state);
    } else if (state.isOrthogonal()) {
      Collection<Region<StatoDispositivo, DispositivoEvent>> regions = ((AbstractState<StatoDispositivo, DispositivoEvent>) state).getRegions();
      for (Region<StatoDispositivo, DispositivoEvent> r : regions) {
        StateMachine<StatoDispositivo, DispositivoEvent> rsm = (StateMachine<StatoDispositivo, DispositivoEvent>) r;
        childs.add(buildStateMachineContext(rsm));
      }
      id = state.getId();
    } else {
      id = state.getId();
    }

    // building history state mappings
    Map<StatoDispositivo, StatoDispositivo> historyStates = new HashMap<StatoDispositivo, StatoDispositivo>();
    PseudoState<StatoDispositivo, DispositivoEvent> historyState = ((AbstractStateMachine<StatoDispositivo, DispositivoEvent>) stateMachine).getHistoryState();
    if (historyState != null) {
      historyStates.put(null, ((HistoryPseudoState<StatoDispositivo, DispositivoEvent>) historyState).getState()
                                                                                                     .getId());
    }
    Collection<State<StatoDispositivo, DispositivoEvent>> states = stateMachine.getStates();
    for (State<StatoDispositivo, DispositivoEvent> ss : states) {
      if (ss.isSubmachineState()) {
        StateMachine<StatoDispositivo, DispositivoEvent> submachine = ((AbstractState<StatoDispositivo, DispositivoEvent>) ss).getSubmachine();
        PseudoState<StatoDispositivo, DispositivoEvent> ps = ((AbstractStateMachine<StatoDispositivo, DispositivoEvent>) submachine).getHistoryState();
        if (ps != null) {
          State<StatoDispositivo, DispositivoEvent> pss = ((HistoryPseudoState<StatoDispositivo, DispositivoEvent>) ps).getState();
          if (pss != null) {
            historyStates.put(ss.getId(), pss.getId());
          }
        }
      }
    }
    return new DefaultStateMachineContext<>(childs, id, null, null, extendedState, historyStates, stateMachine.getId());
  }

  private StatoDispositivo getDeepState(State<StatoDispositivo, DispositivoEvent> state) {
    Collection<StatoDispositivo> ids1 = state.getIds();
    StatoDispositivo[] ids2 = (StatoDispositivo[]) ids1.toArray();
    // TODO: can this be empty as then we'd get error?
    return ids2[ids2.length - 1];
  }

  public String resetStateAndMachineId(String identifierCache, StatoDispositivo stato, String nameFsm) {
    String oldContextFsm = getStateMachineContext(identifierCache);
    StateMachineContext<StatoDispositivo, DispositivoEvent> stateMachine = serializer.deSerialize(oldContextFsm, null);
    StatoDispositivo actualState = stateMachine.getState();
    String newContextFsm = oldContextFsm.replace(actualState.name(), stato.name());
    if (StringUtils.isNotBlank(nameFsm)) {
      String idActualStateMachine = stateMachine.getId();
      newContextFsm = newContextFsm.replace(idActualStateMachine, nameFsm);
    }
    log.info("OldContext FSM: {} - NewContext FSM: {}", oldContextFsm, newContextFsm);
    putStateMachineContext(identifierCache, newContextFsm);
    return newContextFsm;
  }

  public String resetStateFsm(String idDevice, StatoDispositivo stato) {
    return resetStateAndMachineId(idDevice, stato, null);
  }
}
