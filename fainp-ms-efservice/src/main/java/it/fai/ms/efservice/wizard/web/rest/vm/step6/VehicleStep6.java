package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;

public class VehicleStep6 implements Serializable {
  private static final long serialVersionUID = -3237068222402798607L;

  private String identificativo;
  private String targa;
  private String targaNazione;
  
  public VehicleStep6() {}
  
  public VehicleStep6(String identificativo, String targa) {
    super();
    this.identificativo = identificativo;
    this.targa = targa;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getTargaNazione() {
    return targaNazione;
  }

  public void setTargaNazione(String targaNazione) {
    this.targaNazione = targaNazione;
  }

  @Override
  public String toString() {
    return "VehicleStep6 [identificativo=" + identificativo + ", targa=" + targa + ", targaNazione=" + targaNazione + "]";
  }
}
