package it.fai.ms.efservice.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Immutable;
import org.springframework.cache.annotation.Cacheable;

import java.io.Serializable;
import java.util.Objects;

import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;

/**
 * A ConfigurazioneDispositiviServizi.
 */
@Entity
@Table(name = "dispositivi_servizi_config")
@Immutable
@Cacheable
public class ConfigurazioneDispositiviServizi implements Serializable {

  private static final long serialVersionUID = -668082422509950653L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "modalita_gestione", nullable = false)
  private ModalitaGestione modalitaGestione;

  @ManyToOne
  private TipoDispositivo tipoDispositivo;

  @ManyToOne
  private TipoServizio tipoServizio;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ModalitaGestione getModalitaGestione() {
    return modalitaGestione;
  }

  public ConfigurazioneDispositiviServizi modalitaGestione(ModalitaGestione modalitaGestione) {
    this.modalitaGestione = modalitaGestione;
    return this;
  }

  public void setModalitaGestione(ModalitaGestione modalitaGestione) {
    this.modalitaGestione = modalitaGestione;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }

  public ConfigurazioneDispositiviServizi tipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public TipoServizio getTipoServizio() {
    return tipoServizio;
  }

  public ConfigurazioneDispositiviServizi tipoServizio(TipoServizio tipoServizio) {
    this.tipoServizio = tipoServizio;
    return this;
  }

  public void setTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizio = tipoServizio;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfigurazioneDispositiviServizi configurazioneDispositiviServizi = (ConfigurazioneDispositiviServizi) o;
    if (configurazioneDispositiviServizi.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), configurazioneDispositiviServizi.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "ConfigurazioneDispositiviServizi{" + "id=" + getId() + ", modalitaGestione='" + getModalitaGestione() + "'" + "}";
  }
}
