package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.AssociazioneDVService;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import it.fai.ms.efservice.service.mapper.AssociazioneDVMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing AssociazioneDV.
 */
@Service
@Transactional
public class AssociazioneDVServiceImpl implements AssociazioneDVService{

    private final Logger log = LoggerFactory.getLogger(AssociazioneDVServiceImpl.class);

    private final AssociazioneDVRepository associazioneDVRepository;

    private final AssociazioneDVMapper associazioneDVMapper;

    public AssociazioneDVServiceImpl(AssociazioneDVRepository associazioneDVRepository, AssociazioneDVMapper associazioneDVMapper) {
        this.associazioneDVRepository = associazioneDVRepository;
        this.associazioneDVMapper = associazioneDVMapper;
    }

    /**
     * Save a associazioneDV.
     *
     * @param associazioneDVDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AssociazioneDVDTO save(AssociazioneDVDTO associazioneDVDTO) {
        log.debug("Request to save AssociazioneDV : {}", associazioneDVDTO);
        AssociazioneDV associazioneDV = associazioneDVMapper.toEntity(associazioneDVDTO);
        associazioneDV = associazioneDVRepository.save(associazioneDV);
        return associazioneDVMapper.toDto(associazioneDV);
    }

    /**
     *  Get all the associazioneDVS.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AssociazioneDVDTO> findAll() {
        log.debug("Request to get all AssociazioneDVS");
        return associazioneDVRepository.findAll().stream()
            .map(associazioneDVMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one associazioneDV by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AssociazioneDVDTO findOne(Long id) {
        log.debug("Request to get AssociazioneDV : {}", id);
        AssociazioneDV associazioneDV = associazioneDVRepository.findOne(id);
        return associazioneDVMapper.toDto(associazioneDV);
    }

    /**
     *  Delete the  associazioneDV by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AssociazioneDV : {}", id);
        associazioneDVRepository.delete(id);
    }
}
