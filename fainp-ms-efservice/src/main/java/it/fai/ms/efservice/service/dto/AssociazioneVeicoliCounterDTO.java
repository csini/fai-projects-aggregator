package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public class AssociazioneVeicoliCounterDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String seriale;
  private Long counter;

  public AssociazioneVeicoliCounterDTO() {

  }

  public AssociazioneVeicoliCounterDTO(String seriale, Long counter) {
    super();
    this.seriale = seriale;
    this.counter = counter;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public Long getCounter() {
    return counter;
  }

  public void setCounter(Long counter) {
    this.counter = counter;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AssociazioneVeicoliCounterDTO [seriale=")
    .append(seriale)
    .append(", counter=")
    .append(counter)
    .append("]");
    return builder.toString();
  }

}
