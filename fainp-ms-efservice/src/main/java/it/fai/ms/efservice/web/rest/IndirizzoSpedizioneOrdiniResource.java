package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.web.rest.util.PaginationUtil;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniCriteria;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing IndirizzoSpedizioneOrdini.
 */
@RestController
@RequestMapping("/api")
public class IndirizzoSpedizioneOrdiniResource {

    private final Logger log = LoggerFactory.getLogger(IndirizzoSpedizioneOrdiniResource.class);

    private static final String ENTITY_NAME = "indirizzoSpedizioneOrdini";

    private final IndirizzoSpedizioneOrdiniService indirizzoSpedizioneOrdiniService;

    private final IndirizzoSpedizioneOrdiniQueryService indirizzoSpedizioneOrdiniQueryService;

    public IndirizzoSpedizioneOrdiniResource(IndirizzoSpedizioneOrdiniService indirizzoSpedizioneOrdiniService, IndirizzoSpedizioneOrdiniQueryService indirizzoSpedizioneOrdiniQueryService) {
        this.indirizzoSpedizioneOrdiniService = indirizzoSpedizioneOrdiniService;
        this.indirizzoSpedizioneOrdiniQueryService = indirizzoSpedizioneOrdiniQueryService;
    }

    /**
     * POST  /indirizzo-spedizione-ordinis : Create a new indirizzoSpedizioneOrdini.
     *
     * @param indirizzoSpedizioneOrdiniDTO the indirizzoSpedizioneOrdiniDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new indirizzoSpedizioneOrdiniDTO, or with status 400 (Bad Request) if the indirizzoSpedizioneOrdini has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/indirizzo-spedizione-ordinis")
    @Timed
    public ResponseEntity<IndirizzoSpedizioneOrdiniDTO> createIndirizzoSpedizioneOrdini(@RequestBody IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO) throws URISyntaxException {
        log.debug("REST request to save IndirizzoSpedizioneOrdini : {}", indirizzoSpedizioneOrdiniDTO);
        if (indirizzoSpedizioneOrdiniDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new indirizzoSpedizioneOrdini cannot already have an ID")).body(null);
        }
        IndirizzoSpedizioneOrdiniDTO result = indirizzoSpedizioneOrdiniService.save(indirizzoSpedizioneOrdiniDTO);
        return ResponseEntity.created(new URI("/api/indirizzo-spedizione-ordinis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /indirizzo-spedizione-ordinis : Updates an existing indirizzoSpedizioneOrdini.
     *
     * @param indirizzoSpedizioneOrdiniDTO the indirizzoSpedizioneOrdiniDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated indirizzoSpedizioneOrdiniDTO,
     * or with status 400 (Bad Request) if the indirizzoSpedizioneOrdiniDTO is not valid,
     * or with status 500 (Internal Server Error) if the indirizzoSpedizioneOrdiniDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/indirizzo-spedizione-ordinis")
    @Timed
    public ResponseEntity<IndirizzoSpedizioneOrdiniDTO> updateIndirizzoSpedizioneOrdini(@RequestBody IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO) throws URISyntaxException {
        log.debug("REST request to update IndirizzoSpedizioneOrdini : {}", indirizzoSpedizioneOrdiniDTO);
        if (indirizzoSpedizioneOrdiniDTO.getId() == null) {
            return createIndirizzoSpedizioneOrdini(indirizzoSpedizioneOrdiniDTO);
        }
        IndirizzoSpedizioneOrdiniDTO result = indirizzoSpedizioneOrdiniService.save(indirizzoSpedizioneOrdiniDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, indirizzoSpedizioneOrdiniDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /indirizzo-spedizione-ordinis : get all the indirizzoSpedizioneOrdinis.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of indirizzoSpedizioneOrdinis in body
     */
    @GetMapping("/indirizzo-spedizione-ordinis")
    @Timed
    public ResponseEntity<List<IndirizzoSpedizioneOrdiniDTO>> getAllIndirizzoSpedizioneOrdinis(IndirizzoSpedizioneOrdiniCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get IndirizzoSpedizioneOrdinis by criteria: {}", criteria);
        Page<IndirizzoSpedizioneOrdiniDTO> page = indirizzoSpedizioneOrdiniQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/indirizzo-spedizione-ordinis");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /indirizzo-spedizione-ordinis/:id : get the "id" indirizzoSpedizioneOrdini.
     *
     * @param id the id of the indirizzoSpedizioneOrdiniDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the indirizzoSpedizioneOrdiniDTO, or with status 404 (Not Found)
     */
    @GetMapping("/indirizzo-spedizione-ordinis/{id}")
    @Timed
    public ResponseEntity<IndirizzoSpedizioneOrdiniDTO> getIndirizzoSpedizioneOrdini(@PathVariable Long id) {
        log.debug("REST request to get IndirizzoSpedizioneOrdini : {}", id);
        IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = indirizzoSpedizioneOrdiniService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(indirizzoSpedizioneOrdiniDTO));
    }

    /**
     * DELETE  /indirizzo-spedizione-ordinis/:id : delete the "id" indirizzoSpedizioneOrdini.
     *
     * @param id the id of the indirizzoSpedizioneOrdiniDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/indirizzo-spedizione-ordinis/{id}")
    @Timed
    public ResponseEntity<Void> deleteIndirizzoSpedizioneOrdini(@PathVariable Long id) {
        log.debug("REST request to delete IndirizzoSpedizioneOrdini : {}", id);
        indirizzoSpedizioneOrdiniService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
