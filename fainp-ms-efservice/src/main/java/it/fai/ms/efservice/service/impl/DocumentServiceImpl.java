package it.fai.ms.efservice.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.service.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService {

  private DocumentClient documentClient;

  @Value("${efservice.authorizationHeader}")
  private String authorizationHeader;

  @Override
  public Optional<DocumentoDTO> findLastDocument(String codiceAzienda, TipologiaDocumento tipologiaDocumento,
                                                 TipoDispositivoEnum tipoDispositivo, String targa, String targaNazione) {
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("tipoDispositivo", tipoDispositivo.name());
    if (tipologiaDocumento.equals(TipologiaDocumento.RICHIESTA_DISPOSITIVO)) {
      queryParams.put("targa", targa);
      queryParams.put("targaNazione", targaNazione);

    }
    return Optional.ofNullable(documentClient.findLastDocument(authorizationHeader, codiceAzienda, tipologiaDocumento.getName(), targa,
                                                               targaNazione));
  }

  @Autowired
  public void setDocumentClient(DocumentClient documentClient) {
    this.documentClient = documentClient;
  }

  public void setAuthorizationHeader(String authorizationHeader) {
    this.authorizationHeader = authorizationHeader;
  }

  @Override
  public Optional<DocumentoDTO> findByIdentifierAndDocumentType(String uuid, String tipo) {
    return Optional.ofNullable(documentClient.findDocumentoByIdentificativoAndTipo(authorizationHeader, uuid, tipo));
  }
}
