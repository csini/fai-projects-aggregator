package it.fai.ms.efservice.service.dto;

public class SpedizioneDispositiviClienteViewDTO {

  private Long id;

  private String codiceAzienda;

  private String ragioneSociale;

  private Long numeroDispositiviSpedibili = 0L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("SpedizioneDispositiviClienteViewDTO [id=");
    builder.append(id);
    builder.append(", codiceAzienda=");
    builder.append(codiceAzienda);
    builder.append(", ragioneSociale=");
    builder.append(ragioneSociale);
    builder.append(", numeroDispositiviSpedibili=");
    builder.append(numeroDispositiviSpedibili);
    builder.append("]");
    return builder.toString();
  }

}
