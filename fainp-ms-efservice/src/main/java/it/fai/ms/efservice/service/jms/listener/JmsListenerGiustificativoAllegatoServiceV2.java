package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.TelepassEuGiustificativoAggiuntoDTO;
import it.fai.ms.efservice.service.jms.consumer.GiustificativoAllegatoConsumer;

@Service
@Transactional
public class JmsListenerGiustificativoAllegatoServiceV2 extends JmsObjectMessageListenerTemplate<TelepassEuGiustificativoAggiuntoDTO> implements JmsQueueListener {

  private final Logger                         log = LoggerFactory.getLogger(getClass());
  private final GiustificativoAllegatoConsumer giustificativoAllegatoConsumer;

  public JmsListenerGiustificativoAllegatoServiceV2(GiustificativoAllegatoConsumer giustificativoAllegatoConsumer) throws Exception {
    this.giustificativoAllegatoConsumer = giustificativoAllegatoConsumer;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.TELEPASS_EU_EVENTI_ALLEGATO_GIUSTIFICATIVO;
  }

  @Override
  protected void consumeMessage(TelepassEuGiustificativoAggiuntoDTO dto) {
    try {
      giustificativoAllegatoConsumer.changeStatusRichiesta(dto);
    } catch (Exception e) {
      log.error("Error", e);
      throw new RuntimeException(e);
    }

  }

}