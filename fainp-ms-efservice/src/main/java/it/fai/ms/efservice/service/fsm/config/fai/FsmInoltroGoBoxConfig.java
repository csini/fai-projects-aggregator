package it.fai.ms.efservice.service.fsm.config.fai;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.TrackyCardGoBoxService;
import it.fai.ms.efservice.service.fsm.action.FsmActionActivateServiceGoBoxOnTracky;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGenerateContratto;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardCheckIfPresentTrackyCardFalse;
import it.fai.ms.efservice.service.fsm.guard.GuardCheckIfPresentTrackyCardTrue;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroGoBoxConfig.INOLTRO_GO_BOX)
public class FsmInoltroGoBoxConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_GO_BOX = "inoltroGoBox";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final GeneratorContractCodeService generatorContractCodeService;

  private final FsmSenderToQueue fsmSenderToQueue;

  private final TrackyCardGoBoxService trackyCardGoBoxService;

  public FsmInoltroGoBoxConfig(final GeneratorContractCodeService _generatorContractCodeService, final FsmSenderToQueue _fsmSenderToQueue,
                               final TrackyCardGoBoxService _trackyCardGoBoxService, final JmsProperties _jmsProperties) {
    generatorContractCodeService = _generatorContractCodeService;
    fsmSenderToQueue = _fsmSenderToQueue;
    trackyCardGoBoxService = _trackyCardGoBoxService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_GOBOX.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroGoBox();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGenerateContratto(generatorContractCodeService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .guard(new GuardCheckIfPresentTrackyCardTrue(trackyCardGoBoxService))
               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.ACCETTATO))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.ORDINE_SOSPESO)
               .guard(new GuardCheckIfPresentTrackyCardFalse(trackyCardGoBoxService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.ORDINE_SOSPESO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.MU_RIPETI_INVIO)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.SERVIZI_INOLTRATI)
               .action(new FsmActionActivateServiceGoBoxOnTracky(trackyCardGoBoxService))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.SERVIZI_INOLTRATI)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .action(new FsmActionGeneric())
//               .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.SPEDITO_DAL_FORNITORE))
    ;
  }

}
