package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.CategoriaServizioService;
import it.fai.ms.efservice.domain.CategoriaServizio;
import it.fai.ms.efservice.repository.CategoriaServizioRepository;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.mapper.CategoriaServizioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CategoriaServizio.
 */
@Service
@Transactional
public class CategoriaServizioServiceImpl implements CategoriaServizioService{

    private final Logger log = LoggerFactory.getLogger(CategoriaServizioServiceImpl.class);

    private final CategoriaServizioRepository categoriaServizioRepository;

    private final CategoriaServizioMapper categoriaServizioMapper;

    public CategoriaServizioServiceImpl(CategoriaServizioRepository categoriaServizioRepository, CategoriaServizioMapper categoriaServizioMapper) {
        this.categoriaServizioRepository = categoriaServizioRepository;
        this.categoriaServizioMapper = categoriaServizioMapper;
    }

    /**
     * Save a categoriaServizio.
     *
     * @param categoriaServizioDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CategoriaServizioDTO save(CategoriaServizioDTO categoriaServizioDTO) {
        log.debug("Request to save CategoriaServizio : {}", categoriaServizioDTO);
        CategoriaServizio categoriaServizio = categoriaServizioMapper.toEntity(categoriaServizioDTO);
        categoriaServizio = categoriaServizioRepository.save(categoriaServizio);
        return categoriaServizioMapper.toDto(categoriaServizio);
    }

    /**
     *  Get all the categoriaServizios.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CategoriaServizioDTO> findAll() {
        log.debug("Request to get all CategoriaServizios");
        return categoriaServizioRepository.findAll().stream()
            .map(categoriaServizioMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one categoriaServizio by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CategoriaServizioDTO findOne(Long id) {
        log.debug("Request to get CategoriaServizio : {}", id);
        CategoriaServizio categoriaServizio = categoriaServizioRepository.findOne(id);
        return categoriaServizioMapper.toDto(categoriaServizio);
    }

    /**
     *  Delete the  categoriaServizio by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CategoriaServizio : {}", id);
        categoriaServizioRepository.delete(id);
    }
}
