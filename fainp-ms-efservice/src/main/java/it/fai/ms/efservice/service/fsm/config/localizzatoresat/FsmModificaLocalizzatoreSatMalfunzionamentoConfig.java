package it.fai.ms.efservice.service.fsm.config.localizzatoresat;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardMalfunzionamento;
import it.fai.ms.efservice.service.fsm.guard.GuardSendDeviceReturnMessage;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaLocalizzatoreSatMalfunzionamentoConfig.MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO)
public class FsmModificaLocalizzatoreSatMalfunzionamentoConfig extends FsmRichiestaConfig {

  public static final String MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO = "modLocalizzatoreSatMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue                   senderFsmService;
  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  public FsmModificaLocalizzatoreSatMalfunzionamentoConfig(final FsmSenderToQueue _senderFsmService,
                                                           DeviceReturnMessageProducerService _deviceReturnMessageService) {
    this.senderFsmService = _senderFsmService;
    deviceReturnMessageService = _deviceReturnMessageService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaLocalizzatoreSatMalfunzionamento();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO)
               .guard(new GuardMalfunzionamento()) //FIXME-fsm cosa succede se la guard fallisce?
//               .action(new FsmActionAttivoRientro(senderFsmService))

               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MALFUNZIONAMENTO)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
//               .action(new FsmActionAttivoRientro(senderFsmService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .action(new FsmActionVerificaRientroDispositivo(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .guard(new GuardSendDeviceReturnMessage(deviceReturnMessageService))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .event(RichiestaEvent.RESPONSE_OK)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionChangeStatusDispositivo(senderFsmService, DispositivoEvent.RIENTRO));

  }

}
