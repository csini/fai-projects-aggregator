package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;

/**
 * Spring Data JPA repository for the StatoDispositivoServizio entity.
 */
@Repository
public interface StatoDispositivoServizioRepositoryExt extends JpaRepository<StatoDispositivoServizio, Long> {

  // @Query("SELECT sds FROM StatoDispositivoServizio sds where sds.dispositivo = :dispositivoId richiesta by sds.id
  // asc")
  List<StatoDispositivoServizio> findAllByDispositivo(@Param("dispositivo") Dispositivo dispositivo);

  @Query("SELECT count(1) FROM StatoDispositivoServizio sds where sds.stato = 'ATTIVO' and sds.dispositivo = :dispositivo group by sds.dispositivo")
  Integer findAnomaly(@Param("dispositivo") Dispositivo dispositivo);

  @Query("SELECT s FROM StatoDispositivoServizio s where s.dispositivo.identificativo = :uuidDispositivo")
  Page<StatoDispositivoServizio> findAllByUuidDispositivo(@Param("uuidDispositivo") String uuidDispositivo, Pageable pageable);

  @Query("SELECT sds FROM StatoDispositivoServizio sds INNER JOIN sds.dispositivo d INNER JOIN sds.tipoServizio ts WHERE d.identificativo = :uuidDispositivo AND ts.nome = :nomeServizio ORDER BY sds.dataAttivazione DESC")
  List<StatoDispositivoServizio> findByDispositivoUuidAndTipoServizio(@Param("uuidDispositivo") String identificativoDispositivo,
                                                                      @Param("nomeServizio") String nomeServizio);

  @Query("SELECT sds FROM StatoDispositivoServizio sds INNER JOIN sds.dispositivo d INNER JOIN sds.tipoServizio ts WHERE d.identificativo = :uuidDispositivo AND ts.nome = :nomeServizio AND sds.stato = :stato")
  StatoDispositivoServizio findByDispositivoUuidAndTipoServizioAndStato(@Param("uuidDispositivo") String identificativoDispositivo,
                                                                        @Param("nomeServizio") String nomeServizio,@Param("stato") StatoDS statoDS);

  StatoDispositivoServizio findFirstByOrderByIdDesc();

  /*
   * 
   SELECT stato_dispositivo_servizio.* 
FROM stato_dispositivo_servizio INNER JOIN dispositivo  
ON stato_dispositivo_servizio.dispositivo_id=dispositivo.id
INNER JOIN tipo_servizio
ON stato_dispositivo_servizio.tipo_servizio_id=tipo_servizio.id
WHERE dispositivo.identificativo = 'ABCD' 
AND tipo_servizio.nome='PEDAGGI_ITALIA'
ORDER BY stato_dispositivo_servizio.data_attivazione DESC
   * 
   */

//  @Query("select s from StatoDispositivoServizio s, Dispositivo d, TipoServizio t " 
//         + "where s.dispositivo.id=d.id "
//      + "and ")
  StatoDispositivoServizio findOneByDispositivoAndTipoServizio(Dispositivo dispositivo, TipoServizio tipoServizio);

  StatoDispositivoServizio findFirstByDispositivoAndTipoServizio(Dispositivo dispositivo, TipoServizio tipoServizio);

  List<StatoDispositivoServizio> findByDispositivoAndTipoServizio(Dispositivo dispositivo, TipoServizio tipoServizio);

  List<StatoDispositivoServizio> findByDispositivo(Dispositivo dispositivo);

}
