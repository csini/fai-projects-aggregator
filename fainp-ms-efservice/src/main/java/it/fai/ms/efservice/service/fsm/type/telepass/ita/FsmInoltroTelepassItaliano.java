package it.fai.ms.efservice.service.fsm.type.telepass.ita;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.ita.FsmInoltroTelepassItaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmInoltroTelepassItaliano.FSM_INOLTRO_TI)
public class FsmInoltroTelepassItaliano extends FsmRichiestaGeneric {

  public static final String FSM_INOLTRO_TI = "fsmInoltroTI";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmInoltroTelepassItaliano(@Qualifier(FsmInoltroTelepassItaConfig.INOLTRO_TI) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_TI;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_ITALIANO };
  }

  @Override
  protected FsmType getOldFsm() {
    return FsmType.ACCETTAZIONE;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
