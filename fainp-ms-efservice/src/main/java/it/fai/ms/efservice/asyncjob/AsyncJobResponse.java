package it.fai.ms.efservice.asyncjob;

import it.fai.ms.efservice.web.rest.errors.PayloadError;

public class AsyncJobResponse extends PayloadError {

  private StateChangeStatusFsm state;
  private Object warnings;
  private Object data;

  public AsyncJobResponse(){};

  public AsyncJobResponse(StateChangeStatusFsm state, Object data) {
    this.state = state;
    this.data = data;
  }

  public StateChangeStatusFsm getState() {
    return state;
  }

  public void setState(StateChangeStatusFsm state) {
    this.state = state;
  }

  public Object getWarnings() {
    return warnings;
  }

  /**
   * @deprecated use content instead
   */
  @Deprecated
  public void setWarnings(Object warnings) {
    this.warnings = warnings;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "AsyncJobResponse{" +
      "state=" + state +
      ", warnings=" + warnings +
      ", data=" + data +
      '}';
  }
}
