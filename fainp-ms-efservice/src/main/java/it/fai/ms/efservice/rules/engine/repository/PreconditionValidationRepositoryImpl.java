package it.fai.ms.efservice.rules.engine.repository;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.dto.TimestampPreconditionDTO;

@Service
public class PreconditionValidationRepositoryImpl implements PreconditionValidationRepository {

  private final Logger log = LoggerFactory.getLogger(getClass().getName());

  private Map<String, TimestampPreconditionDTO> persistentCache;

  @Autowired
  public PreconditionValidationRepositoryImpl(@Qualifier("precondition-validation") final Map<String, TimestampPreconditionDTO> _cache) {
    persistentCache = _cache;
  }

  @Override
  public void clearCache() {
    log.info("Clear all value in cache");
    persistentCache.clear();
  }

  @Override
  public TimestampPreconditionDTO get(String key) {
    TimestampPreconditionDTO timestampPreconditionDTO = persistentCache.get(key);
    log.info("Get value {} by Key: {}", timestampPreconditionDTO, key);
    return timestampPreconditionDTO;
  }

  @Override
  public void put(String key, Instant dataUltimaModifica) {
    TimestampPreconditionDTO timestamp = new TimestampPreconditionDTO(dataUltimaModifica);
    log.info("Put in cache key: {} and Value: ", key, timestamp);
    put(key, timestamp);
  }

  @Override
  public void put(String key, TimestampPreconditionDTO value) {
    TimestampPreconditionDTO put = persistentCache.put(key, value);
    StringBuilder sb = new StringBuilder("Cache value ");
    sb.append(put);
    if (put != null) {
      sb.append(" updated ");
    } else {
      sb.append(" created ");
    }
    sb.append("for key " + key);

    log.info(sb.toString());
  }

  @Override
  public boolean isPreconditionValid(String uuidVehicle, Instant timestamp) {
    boolean isValid = false;
    TimestampPreconditionDTO timestampPreconditionDTO = get(uuidVehicle);
    log.info("Is precondition recalculated for {} - {} : {} ?", uuidVehicle, timestamp, timestampPreconditionDTO);
    if (timestampPreconditionDTO != null) {
      if (timestampPreconditionDTO.getLastModifiedDateVehicle()
                                  .equals(timestamp)
          && timestampPreconditionDTO.getLastModifiedPrecondition()
                                     .isAfter(timestamp)) {

        isValid = true;
      }
    }

    log.info("isPreconditionValid? {}", isValid);
    return isValid;
  }

}
