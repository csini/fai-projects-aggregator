package it.fai.ms.efservice.service.jms.producer.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessageFactory; 
import it.fai.ms.common.jms.efservice.message.device.AbstractDeviceMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceLicensePlateChangeMessage;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.Richiesta;

@Service
@Transactional
public class DeviceLicensePlateChangeMessageProducerServiceImpl
  implements DeviceLicensePlateChangeMessageProducerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;
  
  @Autowired
  private VehicleClient vehicleClient;
  
  @Value("${efservice.authorizationHeader}")
  String authorizationToken;

  @Autowired
  public DeviceLicensePlateChangeMessageProducerServiceImpl(final JmsProperties _jmsProperties) {
    jmsProperties = _jmsProperties;
  }

  // PER Telepass EU e EU SAT -> da portare tutto su queue
  @Override
  public void deviceLicensePlateChangeToTopic(Richiesta richiesta) {
    
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    
    String targa = orderRequestUtil.getOldLicensePlate().getLicenseId();
    String nazione = orderRequestUtil.getOldLicensePlate().getCountryId();
     
    if ( vehicleClient != null ) {
      this._log.debug("Looking for {}, {}", targa, nazione);
      VehicleDTO vehicleDTO = vehicleClient.findVehiclesByTargaAndNazione(authorizationToken, targa, nazione);
      final DeviceLicensePlateChangeMessage message = new DeviceLicensePlateChangeMessage(orderRequestUtil.getContractUuid(),
                                                                                  orderRequestUtil.getOrderRequestUuid(),
                                                                                  orderRequestUtil.getObuNumber(),
                                                                                  orderRequestUtil.getObuType(),
                                                                                  orderRequestUtil.getVehicleUuid());
      message.setNewVehicleUuid(vehicleDTO.getIdentificativo());
      JmsTopicNames topicName = JmsTopicNames.DEVICE_LICENSEPLATE_CHANGE;
      _log.info("Send message {} to topic {}", message, topicName);
      JmsTopicSenderUtil.publish(jmsProperties, topicName, message);
     
    }
  }

  // Per telepass ITA e altri
  @Override
  public void deviceLicensePlateChangeToQueue(Richiesta richiesta) {
    DeviceEventMessage eventMessage = buildDeviceEventMessage(richiesta);
    JmsQueueNames queueName = JmsQueueNames.DEVICE_PLATE_CHANGE;
    _log.info("Send message {} to queue {}", eventMessage, queueName);
    JmsQueueSenderUtil.publish(jmsProperties, queueName, eventMessage);
  }

  private DeviceEventMessage buildDeviceEventMessage(Richiesta richiesta) {
    OrderRequestUtil orderRequestUtil = new OrderRequestUtil(richiesta);
    OrderRequestDecorator orderRequest = new OrderRequestDecorator(richiesta);

    // Le targhe sono corrette. Non modificare questa parte di codice!
    DeviceEventMessage deviceEventMessage = DeviceEventMessageFactory.createDeviceChangePlateMessage(
      new DeviceDataDTO()
        .contractCode(orderRequest.findNumeroContract())
        .requestId(orderRequest.getIdentificativo())
        .deviceCode(orderRequest.getDeviceSerialId())
        .deviceType(orderRequestUtil.getObuType().getId().name())
        .licensePlateCode(orderRequest.getOldLicensePlate())
        .licensePlateCountry(orderRequest.getOldCountry())
        .newLicensePlateCode(orderRequest.getLicensePlate())
        .newLicensePlateCountry(orderRequest.getCountry())
    );
    return deviceEventMessage;
  }

}
