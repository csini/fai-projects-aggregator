package it.fai.ms.efservice.service.fsm.guard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.FsmManageDeviceSent;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardIsToBeSentAtClient implements Guard<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ManageDeviceSentModeService manageDeviceSentModeService;

  public GuardIsToBeSentAtClient(final ManageDeviceSentModeService _manageDeviceSentModeService) {
    manageDeviceSentModeService = _manageDeviceSentModeService;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    FsmManageDeviceSent manageDeviceSent = FsmManageDeviceSent.CLIENTE;
    boolean daNonSpedireFai = false;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        log.info("isToBeSent to {}", manageDeviceSent);
        daNonSpedireFai = manageDeviceSentModeService.isToBeSentAt(richiesta, manageDeviceSent);
      }
    }

    return daNonSpedireFai;
  }

}
