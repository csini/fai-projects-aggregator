package it.fai.ms.efservice.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipologiaAssociazione;
import it.fai.ms.efservice.dto.CheckDispositiviDTO;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.CreaDispositiviDTO;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.dto.ModificaDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneClienteDispositivoDTO;
import it.fai.ms.efservice.service.dto.SpedizioneFornitoreDispositivoDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

/**
 * Service Interface for managing Dispositivo.
 */
public interface DispositivoServiceExt {

  void prePersistRelatedEntity(Richiesta richiesta, HashMap<String, List<Dispositivo>> mapDispositivoVeicolo) throws Exception;

  void addRelationDispositivoToRichiesta(Richiesta richiesta);

  TipoDispositivo searchTipoDispositivoByNome(String nomeTipoDispositivo);

  Dispositivo addServizioOnDispositivo(List<AssociazioneDV> listAssDispVeicolo, TipoDispositivo tipoDispositivo,
                                       VeicoloCarrelloDTO veicoloCarrelloDTO);

  Dispositivo createDispositivoServizi(TipoDispositivo tipoDispositivo, List<String> nomeTipiServizio1) throws Exception;

  Dispositivo createDispositivoServizi(TipoDispositivo tipoDispositivo, List<String> nomeTipiServizio1, List<String> serviziAggiuntivi) throws Exception;

  TipoServizio findByNomeServizio(String nomeServizio);

  Set<Dispositivo> cloneDispositivi(Set<Dispositivo> dispositivos, Richiesta richiesta, TipoDispositivo tipoDisp, String uuidVeicolo);

  Dispositivo save(Dispositivo dispositivo) ;

  Dispositivo saveWithChild(Dispositivo dispositivo,
                            Set<AssociazioneDV> associazioneDispositivoVeicolos,
                            Set<StatoDispositivoServizio> statoDispositivoServizios) throws Exception;

  Dispositivo findOneByIdentificativo(String identificativo);

  List<String> generateMapAndSendInQueue(List<String> identificativoDispositivi) throws Exception;

  AssociazioneDV associazioneDispositivoVeicolo(TipologiaAssociazione tipologiaAssociazione,
                                                CarrelloModificaRichiestaDTO carrelloDTO) throws Exception;

  List<AssociazioneDV> getAssociazioneDvByUuidVeicoloAndCodiceClienteFai(String uuid, String codiceAzienda);

  Dispositivo disassociaDispositivo(String identificativoDispositivo) throws Exception;

  List<Dispositivo> getDevicesByCodiceAzienda(String codiceAzienda);

  Page<SpedizioneClienteDispositivoDTO> getDevicesByCodiceAzienda(String codiceAzienda, Pageable page);

  Boolean hasVehicleActivedDevices(String codiceAzienda, String identificativoVehicle);

  DispositivoDTO getDispositiviDTO(Dispositivo dispositivo);

  Set<Dispositivo> getDispositiviFilteredByAssociazioneDv(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo,
                                                          String codiceCliente);

  HashMap<TipoRichiesta, Set<TipoServizio>> getServiziGestibiliDaRemoto(Set<TipoServizio> serviziDaDisattivare,
                                                                        Set<TipoServizio> serviziDaAttivare,
                                                                        Dispositivo dispositivo) throws Exception;

  Set<Dispositivo> findDispositiviByCodiceContratto(String codiceContratto);

  void associateContract(Set<Dispositivo> dispositivos, Contratto contract);

  boolean isServiziGestibiliDaRemoto(TipoDispositivoEnum tipoDispositivo, Set<TipoServizio> serviziDaAttivareDisattivare) throws Exception;

  boolean isServiziGestibiliOffline(TipoDispositivoEnum tipoDispositivo, Set<TipoServizio> serviziDaAttivareDisattivare) throws Exception;

  void manageAttivazioneDisattivazioneServiziOffline(Set<TipoServizio> serviziDaDisattivare, Set<TipoServizio> serviziDaAttivare,
                                                     Dispositivo dispositivo) throws Exception;

  void setStatoDispositivoServizio(Dispositivo dispositivo, TipoServizio tipoServizio, TipoRichiesta tipoRichiesta);

  Dispositivo findDispositivoByAssociazioneDvTypeAndCliente(List<AssociazioneDV> associazioniDv, TipoDispositivo tipoDispositivo,
                                                            String codiceCliente) throws Exception;

  Set<TipoServizio> getServiziAttivi(Set<StatoDispositivoServizio> sdss, Dispositivo dispositivo);

  Set<TipoServizio> getServiziDaDisattivare(Set<TipoServizio> serviziAttivi, List<String> nomeTipiServizio);

  Set<TipoServizio> getServiziDaAttivare(Set<TipoServizio> serviziAttivi, List<String> nomeTipiServizio);

  boolean isServiziGestibili(TipoDispositivoEnum tipoDispositivo, Set<TipoServizio> serviziDaAttivareDisattivare);

  List<Dispositivo> save(List<Dispositivo> dispositivo, boolean cascade);

  Dispositivo saveAndFlush(Dispositivo dispositivi) throws Exception;

  List<Dispositivo> saveAndFlush(List<Dispositivo> dispositivi);

  boolean executeFsm(Set<String> identificativoDispositivi, String operazione, String nota);

  Set<Dispositivo> findDispositiviByCodiceClienteAndTipo(String codiceCliente, TipoDispositivoEnum tipoDispositivo);

  int countDispositiviAttiviByCodiceClienteAndTipo(String codiceCliente, TipoDispositivoEnum tipoDispositivo);

  Set<Dispositivo> findDispositiviByStateAndType(StatoDispositivo stato, TipoDispositivoEnum telepassItaliano);

  Dispositivo findOneForUpdateByStateAndType(StatoDispositivo stato, TipoDispositivoEnum telepassItaliano);

  Optional<Dispositivo> findByCodiceContrattoAndSeriale(String contractCode, String serialeDispositivo);

  StatoDispositivoServizio updateStatoDeviceService(StatoDispositivoServizio sds, StatoDS stato);

  String modificaDispositivoByBO(ModificaDispositivoDTO modificaDispositivoDTO) throws Exception;

  List<DispositivoDTO> findDispositiviByCodiceContrattoAndSeriale(String codiceContratto, String seriale);

  List<DispositivoDTO> findDispositiviByCodiceContrattoAndVeicolo(String codiceContratto, String uuidVeicolo);

  DispositivoDTO mapperWithFileAllegato(Dispositivo src);

  void creaDispositivi(CreaDispositiviDTO dto, Contratto contratto, TipoDispositivo tipoDispositivo) throws CustomException;

  Optional<Dispositivo> findViaCardByIdentificativoVeicolo(String identificativoVeicolo);

  Dispositivo createViaCardOnScorta() throws Exception;

  Dispositivo createViaCardOnVehicle(String identificativoVeicolo) throws Exception;

  Dispositivo createDispositivoDefault(TipoDispositivo tipoDispositivo);

  Optional<Dispositivo> findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo inDeposito, TipoDispositivoEnum telepassItaliano);

  void delete(Dispositivo device);

  void flush();

  void removeStatoDispositivoServizios(Dispositivo device);

  void removeAssociazioneDvs(Dispositivo device);

  Optional<Dispositivo> findByTipoDispositivoAndSeriale(TipoDispositivoEnum tipoDispositivo, String serialCode);

  void updateAssociazioneDv(AssociazioneDV associazioneDV);

  WizardVehicleEntity findVehicleBySerialeDispositivo(String dispositivoSeriale);

  Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicoloAndTipoDispositivo_nome(String vehicleUuid,
                                                                                            TipoDispositivoEnum tipoDispositivo);

  Page<SpedizioneFornitoreDispositivoDTO> getDevicesByTipiDispositivo(List<TipoDispositivoEnum> tipiDispositivo, Pageable page);

  Optional<Dispositivo> findActiveDeviceByTypeAndLicensePlateAndCountry(TipoDispositivoEnum tipiDispositivo,
                                                                        Set<StatoDispositivo> statiDispositiviAttivi, String targa,
                                                                        String nazione, String codiceCliente);

  List<DispositivoDTO> findAllByVehicleUuid(String uuidVeicolo);

  Set<Dispositivo> findByAssociazioneDispositivoVeicolos_uuidVeicolo(String uuid);

  Optional<Dispositivo> findGoBoxDeviceRelated(String serialeDeviceSostituito);

  StatoDispositivoServizio updateStatoDispositivoServizio(StatoDispositivoServizio pan);

  List<String> checkDispositivoExists(CheckDispositiviDTO checkDispositiviDto);


  Set<String> checkDispositivoExists(String codiceCliente, TipoDispositivoEnum tipoDispositivoEnum, Set<String> vehicleUUIDs);

  boolean isHgvDevice(TipoDispositivo deviceType);

  List<DispositivoDTO> findByCodiceContratto(String codiceContratto);

  List<DispositivoDTO> getDispositiviAttiviTarghe(LocalDate dateStart);

  List<DispositivoDTO> checkDevicesNotFound(String tipo, List<String> serials);

  void changeStatoServiceToNotActiveByDevice(Dispositivo dispositivo);

  List<Dispositivo> findActiveDeviceExpired();

  int findActiveDeviceExpiredAndUpdateStateToExpired();

  List<it.fai.ms.common.jms.dto.efservice.DispositivoDTO> findActiveByVehicleUuid(String uuidVeicolo,
		String codiceCliente);

}
