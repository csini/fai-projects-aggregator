package it.fai.ms.efservice.service;

import java.util.Set;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;

/**
 * Service Interface for managing ViaCard Device.
 */
public interface ViaCardServiceExt {

  Set<Dispositivo> findByCodiceClienteAndTipoDispositivoAndStatoNotIn(String codiceCliente, TipoDispositivoEnum tipoDispositivo, Set<StatoDispositivo> states);

  long countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum tipoDispositivo, Contratto contratto);

  long getNumberOfViaCardNecessaryForOrder(String codiceCliente, long numViaCard, long numTelepassIta);

}
