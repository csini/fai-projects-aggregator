/**
 * 
 */
package it.fai.ms.efservice.service.fsm.guard;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardGestioneRientro implements Guard<StatoRichiesta, RichiestaEvent> {

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.guard.Guard#evaluate(org.springframework.statemachine.StateContext)
   */
  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    // FIXME CHIDERE A MARCO O EDOARDO SE NECESSARIO;
    return true;
  }

}
