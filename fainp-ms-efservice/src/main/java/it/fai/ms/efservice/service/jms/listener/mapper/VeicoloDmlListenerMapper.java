package it.fai.ms.efservice.service.jms.listener.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.mappable.DmlListenerMapper;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;

@Component
public class VeicoloDmlListenerMapper
  implements DmlListenerMapper<VehicleDMLDTO, VehicleDMLDTO> {

  @Override
  public VehicleDMLDTO toORD(VehicleDMLDTO dml, VehicleDMLDTO ord) {
    //MAPPING?!
    return dml;
  }
}
