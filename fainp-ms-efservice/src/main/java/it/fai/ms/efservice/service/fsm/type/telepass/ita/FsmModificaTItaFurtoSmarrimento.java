package it.fai.ms.efservice.service.fsm.type.telepass.ita;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.telepass.ita.FsmModificaTItaFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaTItaFurtoSmarrimento.FSM_MOD_TITA_FURTO_SMARRIMENTO)
public class FsmModificaTItaFurtoSmarrimento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_TITA_FURTO_SMARRIMENTO = "fsmModificaTItaFurtoSmarrimento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaTItaFurtoSmarrimento(@Qualifier(FsmModificaTItaFurtoSmarrimentoConfig.MOD_TITA_FURTO_SMARRIMENTO) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                         FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_TI_FURTO_SMARRIMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.TELEPASS_ITALIANO };
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

}
