package it.fai.ms.efservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CategoriaServizio.
 */
@Entity
@Table(name = "categoria_servizio")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CategoriaServizio implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "nome", nullable = false)
  private String nome;

  @Column(name = "nome_business")
  private String nomeBusiness;

  @Column(name = "descrizione")
  private String descrizione;

  @Column(name = "testo_promo")
  private String testoPromo;

  @Column(name = "ordinamento")
  private Long ordinamento;

  @NotNull
  @Column(name = "disabilitato", nullable = false)
  private boolean disabilitato = false;

  @OneToMany(mappedBy = "categoriaServizio")
  @JsonIgnore
  private Set<TipoServizio> tipoServizios = new HashSet<>();

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public CategoriaServizio nome(String nome) {
    this.nome = nome;
    return this;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNomeBusiness() {
    return nomeBusiness;
  }

  public CategoriaServizio nomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
    return this;
  }

  public void setNomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public CategoriaServizio descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public String getTestoPromo() {
    return testoPromo;
  }

  public CategoriaServizio testoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
    return this;
  }

  public void setTestoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
  }

  public Long getOrdinamento() {
    return ordinamento;
  }

  public CategoriaServizio ordinamento(Long ordinamento) {
    this.ordinamento = ordinamento;
    return this;
  }

  public void setOrdinamento(Long ordinamento) {
    this.ordinamento = ordinamento;
  }

  public boolean isDisabilitato() {
    return disabilitato;
  }

  public CategoriaServizio disabilitato(boolean disabilitato) {
    this.disabilitato = disabilitato;
    return this;
  }

  public void setDisabilitato(boolean disabilitato) {
    this.disabilitato = disabilitato;
  }

  public Set<TipoServizio> getTipoServizios() {
    return tipoServizios;
  }

  public CategoriaServizio tipoServizios(Set<TipoServizio> tipoServizios) {
    this.tipoServizios = tipoServizios;
    return this;
  }

  public CategoriaServizio addTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizios.add(tipoServizio);
    tipoServizio.setCategoriaServizio(this);
    return this;
  }

  public CategoriaServizio removeTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizios.remove(tipoServizio);
    tipoServizio.setCategoriaServizio(null);
    return this;
  }

  public void setTipoServizios(Set<TipoServizio> tipoServizios) {
    this.tipoServizios = tipoServizios;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CategoriaServizio categoriaServizio = (CategoriaServizio) o;
    if (categoriaServizio.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), categoriaServizio.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "CategoriaServizio{" + "id=" + getId() + ", nome='" + getNome() + "'" + ", nomeBusiness='" + getNomeBusiness() + "'"
           + ", descrizione='" + getDescrizione() + "'" + ", testoPromo='" + getTestoPromo() + "'" + ", ordinamento='" + getOrdinamento()
           + "'" + ", disabilitato='" + isDisabilitato() + "'" + "}";
  }
}
