package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.service.ChangeStatusOrdineClienteService;
import it.fai.ms.efservice.service.ManageStatoOrdineClienteService;
import it.fai.ms.efservice.service.cache.StatoOrdineClienteCache;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

@Service
public class ManageStatoOrdineClienteServiceExt implements ManageStatoOrdineClienteService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ChangeStatusOrdineClienteService changeStatusOrdineClienteservice;

  private final StatoOrdineClienteCache cacheService;

  public ManageStatoOrdineClienteServiceExt(ChangeStatusOrdineClienteService _changeStatusOrdineClienteservice,
                                            StatoOrdineClienteCache _cacheService) {
    this.changeStatusOrdineClienteservice = _changeStatusOrdineClienteservice;
    this.cacheService = _cacheService;
  }

  public void manageOrdiniInCache() {
    Map<String, String> cacheMap = copyMaps();
    Set<String> allKeys = cacheMap.keySet();
    log.debug("Keys: {}", allKeys);
    for (String id : allKeys) {
      String identificativoOrdineCliente = cacheMap.get(id);
      log.debug("Call manage ordine {} - {}", id, identificativoOrdineCliente);
      manageOrdine(id, identificativoOrdineCliente, false);
    }
    Set<String> keysToExpire = allKeys.parallelStream()
                                      .collect(toSet());
    cacheService.expireValueByKeys(keysToExpire);
  }

  private Map<String, String> copyMaps() {
    Map<String, String> map = new HashMap<>();
    Set<String> allKeys = cacheService.getAllKeys();
    allKeys.forEach(k -> {
      map.put(k, cacheService.getObject(k));
    });
    return map;
  }

  public void manageOrdineAndRemoveFromCache(String idOrdineCliente, String identificativoOrdineCliente) {
    manageOrdine(idOrdineCliente, identificativoOrdineCliente, true);
  }

  private void manageOrdine(String idOrdineCliente, String identificativoOrdineCliente, boolean removeFromCache) {
    Set<String> keysToExpire = new HashSet<>();
    long t1 = System.currentTimeMillis();
    String identificativoOrdine = changeStatusOrdineClienteservice.calculateAndChangeStatus(identificativoOrdineCliente);
    log.debug("Elaborated change status OrdineCliente in {} ms", System.currentTimeMillis() - t1);
    if (identificativoOrdine == null) {
      log.debug("Managed ordine cliente [ID: {} - Identificativo: {}]", idOrdineCliente, identificativoOrdineCliente);
      if (removeFromCache) {
        keysToExpire.add(idOrdineCliente);
      }
    }

    if (!keysToExpire.isEmpty()) {
      cacheService.expireValueByKeys(keysToExpire);
    }
  }

  @Override
  public boolean expireOrdineInCache(Long idOrdineCliente) {
    Set<String> keys = new HashSet<>();
    String keyCache = String.valueOf(idOrdineCliente);
    keys.add(keyCache);
    cacheService.expireValueByKeys(keys);
    String identificativo = cacheService.getObject(keyCache);
    if (StringUtils.isBlank(identificativo)) {
      return true;
    }
    return false;
  }

  @Override
  public Set<String> getAllKeyInCache() {
    return cacheService.getAllKeys();
  }

  @Override
  public void putOrdiniInCache() {
    Set<String> allKeys = cacheService.getAllKeys();
    List<String> keysId = allKeys.stream()
                                 .collect(toList());
    Set<OrdineClienteCacheDTO> ordini = changeStatusOrdineClienteservice.findOrdineClienteFilteredByStatoNotIsAndAlreadyInCache(StatoOrdineCliente.COMPLETATO,
                                                                                                                                keysId);
    ordini.stream()
          .map(o -> {
            cacheService.putObject(String.valueOf(o.getId()), o.getIdentificativo());
            return o;
          })
          .collect(toSet());
  }

  @Override
  public void putOrdiniNotCompletedInCache(List<OrdineClienteCacheDTO> ordiniCliente) {
    ordiniCliente.forEach(oc -> {
      Long idOrdineCliente = oc.getId();
      String identificativo = oc.getIdentificativo();
      if (idOrdineCliente != null) {
        log.debug("put object --> key: {} - Value: {}", idOrdineCliente, identificativo);
        cacheService.putObject(String.valueOf(idOrdineCliente), identificativo);
      } else {
        log.info("Skip ordine cliente: {}", oc);
      }
    });
    log.info("Finish put ordini not completed in cache");
  }

}
