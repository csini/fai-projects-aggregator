package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.ChangeStatusOrdineClienteService;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;
import it.fai.ms.efservice.service.dto.RichiestaOrdineClienteDTO;

@Service
@Transactional
public class ChangeStatusOrdineClienteServiceImpl implements ChangeStatusOrdineClienteService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private OrdineClienteServiceExt ordineClienteService;

  private RichiestaRepositoryExt richiestaRepo;

  public ChangeStatusOrdineClienteServiceImpl(OrdineClienteServiceExt _ordineClienteService, RichiestaRepositoryExt richiestaRepo) {
    ordineClienteService = _ordineClienteService;
    this.richiestaRepo = richiestaRepo;
  }

  @Override
  public String calculateAndChangeStatus(String identificativoOrdineCliente) {
    OrdineCliente ordineCliente = ordineClienteService.findByIdentificativoOrdineCliente(identificativoOrdineCliente);
    if (ordineCliente == null) {
      log.warn("Not found Ordine Cliente by identificativo: " + identificativoOrdineCliente);
      return identificativoOrdineCliente;
    }

    Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine = richiestaRepo.findStatoRichiestaByIdentificativoOrdine(identificativoOrdineCliente);
    if (statoRichiesteOrdine != null && !statoRichiesteOrdine.isEmpty()) {
      log.trace("Found {} requests related on OrdineCliente {}", statoRichiesteOrdine.size(), identificativoOrdineCliente);
      log.debug("Actual state ordine cliente: {}", ordineCliente.getStato());

      logStatiRichiesteOrdine(statoRichiesteOrdine);

      boolean ordineComplete = isOrdineComplete(statoRichiesteOrdine);
      if (ordineComplete) {
        saveNewState(ordineCliente, StatoOrdineCliente.COMPLETATO);
        return null;
      }

      boolean isRefused = isOrdineRefused(statoRichiesteOrdine);
      if (isRefused) {
        saveNewState(ordineCliente, StatoOrdineCliente.RIFIUTATO);
        return null;
      }

      boolean richiestaSospesaAndNotAccepted = isLeastOneRichiestaSospesaAndOneNotAccepted(statoRichiesteOrdine);
      if (richiestaSospesaAndNotAccepted) {
        saveNewState(ordineCliente, StatoOrdineCliente.SOSPESO_E_LAVORATO_PARZIALMENTE);
        return null;
      }

      boolean richiestaSospesa = isLeastOneRichiestaSospesa(statoRichiesteOrdine);
      if (richiestaSospesa) {
        saveNewState(ordineCliente, StatoOrdineCliente.SOSPESO);
        return null;
      }

      boolean isPartialAccepted = isAccettatoParziale(statoRichiesteOrdine);
      if (isPartialAccepted) {
        saveNewState(ordineCliente, StatoOrdineCliente.ACCETTATO_PARZIALE);
        return null;
      }

      boolean richiesteNotProcessed = isRichiesteNotProcessed(statoRichiesteOrdine);
      if (richiesteNotProcessed) {
        saveNewState(ordineCliente, StatoOrdineCliente.DA_EVADERE);
        return null;
      }

      boolean richiestaUnderNotProcessing = isLeastOneRichiestaUnderNotProcessing(statoRichiesteOrdine);
      if (richiestaUnderNotProcessing) {
        saveNewState(ordineCliente, StatoOrdineCliente.LAVORATO_PARZIALMENTE);
        return null;
      }

      boolean isAllAccepted = isAllRichiesteAccettate(statoRichiesteOrdine);
      if (isAllAccepted) {
        saveNewState(ordineCliente, StatoOrdineCliente.ACCETTATO);
        return null;
      }

      log.warn("L'Ordine cliente {} non rientra nelle casistiche prestabilite", identificativoOrdineCliente);
    } else {
      log.error("OrdineCliente {} hasn't Richieste...", identificativoOrdineCliente);
    }

    return null;
  }

  private void logStatiRichiesteOrdine(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    if (log.isDebugEnabled()) {
      statoRichiesteOrdine.forEach(r -> {
        log.debug("Richiesta: [Id {} - Stato {}]", r.getId(), r.getStato());
      });
    }
  }

  private void saveNewState(OrdineCliente ordineCliente, StatoOrdineCliente newState) {
    String identificativo = ordineCliente.getIdentificativo();
    StatoOrdineCliente statoAttuale = ordineCliente.getStato();
    if (ordineCliente.getStato() == newState) {
      log.debug("Not save Ordine Cliente {}, because actual State [{}] is equal to new State [{}]", identificativo, statoAttuale, newState);
      return;
    }
    Instant now = Instant.now();
    log.info("Change status OrdineCliente {} from [{}] to [{}] in data {}", identificativo, statoAttuale, newState, now);
    OrdineCliente ordineSaved = ordineClienteService.save(ordineCliente.stato(newState)
                                                                       .dataModificaStato(now));
    log.info("Ordine saved: {}", ordineSaved);
  }

  private boolean isOrdineComplete(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Set<StatoRichiesta> richiesteCompleted = statoRichiesteOrdine.stream()
                                                                 .map(r -> r.getStato())
                                                                 .filter(status -> (status == StatoRichiesta.ATTIVO_EVASO
                                                                                    || status == StatoRichiesta.EVASO_CON_RICONSEGNA))
                                                                 .collect(toSet());

    int numRichiesteOrdine = statoRichiesteOrdine.size();
    boolean isComplete = (richiesteCompleted != null && numRichiesteOrdine == richiesteCompleted.size()) ? true : false;
    log.debug("Richieste completed {} - Richieste Ordine {} => Ordine is Complete: {}",
              (richiesteCompleted != null) ? richiesteCompleted.size() : null, numRichiesteOrdine, isComplete);
    return isComplete;
  }

  private boolean isOrdineRefused(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Set<RichiestaOrdineClienteDTO> richiesteRefused = statoRichiesteOrdine.stream()
                                                                          .filter(r -> r.getStato() == StatoRichiesta.RICHIESTA_RIFIUTATA)
                                                                          .collect(toSet());

    int numRichiesteOrdine = statoRichiesteOrdine.size();
    boolean isAllRefused = (richiesteRefused != null && richiesteRefused.size() == numRichiesteOrdine) ? true : false;
    log.debug("Richieste refused {} - Richieste Ordine {} => Ordine is refused: {}",
              (richiesteRefused != null) ? richiesteRefused.size() : null, numRichiesteOrdine, isAllRefused);
    return isAllRefused;
  }

  private boolean isLeastOneRichiestaSospesa(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Optional<StatoRichiesta> optRichiestaSospesa = statoRichiesteOrdine.stream()
                                                                       .map(r -> r.getStato())
                                                                       .filter(s -> s == StatoRichiesta.SOSPESA)
                                                                       .findFirst();
    return optRichiestaSospesa.isPresent();
  }

  private boolean isLeastOneRichiestaSospesaAndOneNotAccepted(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    boolean richiestaSospesa = isLeastOneRichiestaSospesa(statoRichiesteOrdine);
    Optional<StatoRichiesta> optRichiestasNotAccepted = statoRichiesteOrdine.stream()
                                                                            .map(r -> r.getStato())
                                                                            .filter(s -> s == StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                                                                            .findFirst();

    boolean richiestaNotAccepted = optRichiestasNotAccepted.isPresent();
    log.debug("Richiesta not accepted: {} - Richiesta sospesa: {}", richiestaNotAccepted, richiestaSospesa);
    if (richiestaNotAccepted && richiestaSospesa) {
      return true;
    }
    return false;
  }

  private boolean isAccettatoParziale(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {

    Optional<StatoRichiesta> optRichiestasAccepted = statoRichiesteOrdine.stream()
                                                                         .map(r -> r.getStato())
                                                                         .filter(s -> s != StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE
                                                                                      && s != StatoRichiesta.RICHIESTA_RIFIUTATA)
                                                                         .findFirst();
    Optional<StatoRichiesta> optRichiestasRefused = statoRichiesteOrdine.stream()
                                                                        .map(r -> r.getStato())
                                                                        .filter(s -> s == StatoRichiesta.RICHIESTA_RIFIUTATA)
                                                                        .findFirst();

    log.debug("Richiesta accettata: {} - Richiesta rifiutata: {}", optRichiestasAccepted, optRichiestasRefused);
    if (optRichiestasAccepted.isPresent() && optRichiestasRefused.isPresent()) {
      return true;
    }
    return false;
  }

  private boolean isRichiesteNotProcessed(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Set<RichiestaOrdineClienteDTO> richiesteNotProcessed = statoRichiesteOrdine.stream()
                                                                               .filter(r -> r.getStato() == StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                                                                               .collect(toSet());

    int numRichiesteOrdine = statoRichiesteOrdine.size();
    boolean isNotProcessed = (richiesteNotProcessed != null && richiesteNotProcessed.size() == numRichiesteOrdine) ? true : false;
    log.debug("Richieste not processed {} - Richieste Ordine {} => Ordine is not processed: {}",
              (richiesteNotProcessed != null) ? richiesteNotProcessed.size() : null, numRichiesteOrdine, isNotProcessed);
    return isNotProcessed;
  }

  private boolean isLeastOneRichiestaUnderNotProcessing(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Optional<RichiestaOrdineClienteDTO> optRichiestasFiltered = statoRichiesteOrdine.stream()
                                                                                    .filter(r -> r.getStato() == StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                                                                                    .findFirst();
    return optRichiestasFiltered.isPresent();
  }

  private boolean isAllRichiesteAccettate(Set<RichiestaOrdineClienteDTO> statoRichiesteOrdine) {
    Set<RichiestaOrdineClienteDTO> richiesteAccepted = statoRichiesteOrdine.stream()
                                                                           .filter(r -> r.getStato() != StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                                                                           .filter(r -> r.getStato() != StatoRichiesta.RICHIESTA_RIFIUTATA)
                                                                           .filter(r -> r.getStato() != StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE)
                                                                           .filter(r -> r.getStato() != StatoRichiesta.RICHIESTA_ANNULLATA)
                                                                           .collect(toSet());

    int numRichiesteOrdine = statoRichiesteOrdine.size();
    boolean isAccepted = (richiesteAccepted != null && richiesteAccepted.size() == numRichiesteOrdine) ? true : false;
    log.debug("Richieste accepted {} - Richieste Ordine {} => Ordine is accepted: {}",
              (richiesteAccepted != null) ? richiesteAccepted.size() : null, numRichiesteOrdine);
    return isAccepted;
  }

  @Override
  public Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIs(StatoOrdineCliente stato) {
    return ordineClienteService.findOrdineClienteFilteredByStatoNotIsAndNotInCache(stato, null);
  }

  @Override
  public Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIsAndAlreadyInCache(StatoOrdineCliente stato, List<String> keysId) {
    return ordineClienteService.findOrdineClienteFilteredByStatoNotIsAndNotInCache(stato, keysId);
  }

}
