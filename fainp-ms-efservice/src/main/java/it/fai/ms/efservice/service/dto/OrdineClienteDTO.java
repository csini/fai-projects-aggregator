package it.fai.ms.efservice.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;

/**
 * A DTO for the OrdineCliente entity.
 */
public class OrdineClienteDTO implements Serializable {

    private Long id;

    private Integer previsioneSpesaMensile;

    private String uuidVeicolo;

    private Boolean destinazioneFai;

    private String numeroOrdine;

    private StatoOrdineCliente stato;

    private Instant dataModificaStato;

    private Instant dataCreazione;

    private String richiedente;

    private TipoOrdineCliente tipo;

    private String identificativo;

    private Long indirizzoDiTransitoId;

    private Long indirizzoDestinazioneFinaleId;

    private Long clienteAssegnatarioId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrevisioneSpesaMensile() {
        return previsioneSpesaMensile;
    }

    public void setPrevisioneSpesaMensile(Integer previsioneSpesaMensile) {
        this.previsioneSpesaMensile = previsioneSpesaMensile;
    }

    public String getUuidVeicolo() {
        return uuidVeicolo;
    }

    public void setUuidVeicolo(String uuidVeicolo) {
        this.uuidVeicolo = uuidVeicolo;
    }

    public Boolean isDestinazioneFai() {
        return destinazioneFai;
    }

    public void setDestinazioneFai(Boolean destinazioneFai) {
        this.destinazioneFai = destinazioneFai;
    }

    public String getNumeroOrdine() {
        return numeroOrdine;
    }

    public void setNumeroOrdine(String numeroOrdine) {
        this.numeroOrdine = numeroOrdine;
    }

    public StatoOrdineCliente getStato() {
        return stato;
    }

    public void setStato(StatoOrdineCliente stato) {
        this.stato = stato;
    }

    public Instant getDataModificaStato() {
        return dataModificaStato;
    }

    public void setDataModificaStato(Instant dataModificaStato) {
        this.dataModificaStato = dataModificaStato;
    }

    public Instant getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Instant dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public String getRichiedente() {
        return richiedente;
    }

    public void setRichiedente(String richiedente) {
        this.richiedente = richiedente;
    }

    public TipoOrdineCliente getTipo() {
        return tipo;
    }

    public void setTipo(TipoOrdineCliente tipo) {
        this.tipo = tipo;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public Long getIndirizzoDiTransitoId() {
        return indirizzoDiTransitoId;
    }

    public void setIndirizzoDiTransitoId(Long indirizzoSpedizioneOrdiniId) {
        this.indirizzoDiTransitoId = indirizzoSpedizioneOrdiniId;
    }

    public Long getIndirizzoDestinazioneFinaleId() {
        return indirizzoDestinazioneFinaleId;
    }

    public void setIndirizzoDestinazioneFinaleId(Long indirizzoSpedizioneOrdiniId) {
        this.indirizzoDestinazioneFinaleId = indirizzoSpedizioneOrdiniId;
    }

    public Long getClienteAssegnatarioId() {
        return clienteAssegnatarioId;
    }

    public void setClienteAssegnatarioId(Long clienteFaiId) {
        this.clienteAssegnatarioId = clienteFaiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrdineClienteDTO ordineClienteDTO = (OrdineClienteDTO) o;
        if(ordineClienteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ordineClienteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrdineClienteDTO{" +
            "id=" + getId() +
            ", previsioneSpesaMensile='" + getPrevisioneSpesaMensile() + "'" +
            ", uuidVeicolo='" + getUuidVeicolo() + "'" +
            ", destinazioneFai='" + isDestinazioneFai() + "'" +
            ", numeroOrdine='" + getNumeroOrdine() + "'" +
            ", stato='" + getStato() + "'" +
            ", dataModificaStato='" + getDataModificaStato() + "'" +
            ", dataCreazione='" + getDataCreazione() + "'" +
            ", richiedente='" + getRichiedente() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", identificativo='" + getIdentificativo() + "'" +
            "}";
    }
}
