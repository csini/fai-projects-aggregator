package it.fai.ms.efservice.service.jms.consumer.device;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableSet;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.telepass.AdditionalDetailsDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.DeviceMessageDetail;
import it.fai.ms.common.jms.efservice.FileElaborationDetail;
import it.fai.ms.common.jms.efservice.message.device.DeviceInfo;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderedMessage;
import it.fai.ms.common.jms.efservice.message.device.SimpleDeviceInfo;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RaggruppamentoRichiesteOrdineFornitoreServiceExt;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@Service
@Transactional

// FIXME il raggruppamento file ha problemi di concorrenza, puo' esistere solo 1 listener che lo ascolta
public class DeviceResponseOkConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private static final String LOG_STRING = "Changed status for Richiesta";

  private static final String COMMAND = RichiestaEvent.RESPONSE_OK.name();

  private static final Set<TipoRichiesta> requestTypes = ImmutableSet.of(TipoRichiesta.MALFUNZIONAMENTO,
                                                                         TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE,
                                                                         TipoRichiesta.MEZZO_RITARGATO, TipoRichiesta.VARIAZIONE_TARGA);

  private final FsmRichiestaUtils fsmRichiestaUtil;

  private final DispositivoServiceExt dispositivoServiceExt;

  private final RaggruppamentoRichiesteOrdineFornitoreServiceExt groupRichiesteFornitoreServiceExt;

  final VehicleClient vehicleClient;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public DeviceResponseOkConsumer(final FsmRichiestaUtils _fsmRichiestaUtil, final DispositivoServiceExt _dispositivoServiceExt,
                                  final VehicleWizardCacheServiceExt _vehicleWizardCacheService,
                                  final RaggruppamentoRichiesteOrdineFornitoreServiceExt _groupRichiesteFornitoreServiceExt,
                                  final VehicleClient _vehicleClient) {
    fsmRichiestaUtil = _fsmRichiestaUtil;
    dispositivoServiceExt = _dispositivoServiceExt;
    groupRichiesteFornitoreServiceExt = _groupRichiesteFornitoreServiceExt;
    vehicleClient = _vehicleClient;
  }

  public void changeStatoRichiesta(String identificativoRichiesta) throws FsmExecuteCommandException {
    log.info("Manage change status to Richiesta [Identificativo: {}]", identificativoRichiesta);
    Richiesta richiesta = retrieveRichiestaByIdentificativo(identificativoRichiesta);
    if (richiesta != null) {
      changeStatoRichiesta(richiesta);
    } else {
      log.info("Not found Richiesta: {}", identificativoRichiesta);
    }
  }

  /**
   * Use to manage TARRC flow
   *
   * @param codiceContratto
   * @param targa
   * @param country
   * @throws FsmExecuteCommandException
   */
  public void consumeMessageByContrattoAndTargaNazione(String codiceContratto, String targa,
                                                       String country) throws FsmExecuteCommandException {
    log.info("Manage change status to Richiesta [CodiceContratto {} - Targa {} - Country {}]", codiceContratto, targa, country);
    StatoRichiesta stato = StatoRichiesta.INOLTRATO;
    List<Richiesta> richieste = fsmRichiestaUtil.findRichiestaByContrattoAndTargaNazioneAndStato(codiceContratto, targa, country, stato);
    Richiesta richiesta = null;
    if (richieste != null && richieste.size() == 1) {
      log.info("Found {} richieste", richieste.size());
      richiesta = richieste.get(0);
    } else {
      log.info("Filter requests by {} or {}", TipoRichiesta.VARIAZIONE_TARGA, TipoRichiesta.MEZZO_RITARGATO);
      Optional<Richiesta> optRichiesta = richieste.stream()
                                                  .filter(r -> r.getTipo() == TipoRichiesta.VARIAZIONE_TARGA
                                                               || r.getTipo() == TipoRichiesta.MEZZO_RITARGATO)
                                                  .findFirst();
      if (optRichiesta.isPresent()) {
        richiesta = optRichiesta.get();
      } else {
        log.warn("Not found richiesta from list {} filtered by {} or {}", richieste, TipoRichiesta.VARIAZIONE_TARGA,
                 TipoRichiesta.MEZZO_RITARGATO);
      }
    }

    if (richiesta != null) {
      TipoRichiesta tipo = richiesta.getTipo();
      if (tipo == TipoRichiesta.VARIAZIONE_TARGA) {
        updateAssociazioneDvByTargaNazione(richiesta);
      }

      changeStatoRichiesta(richiesta);
    } else {
      log.warn("Not found Richiesta by Contratto {} , Targa {}, Nazione {}, Stato {}", codiceContratto, targa, country, stato);
    }
  }

  public void consumeDeviceMessageWithOtherInformations(String identificativoRichiesta,
                                                        DeviceOrderedMessage deviceOrderMessage) throws FsmExecuteCommandException {
    Richiesta richiesta = retrieveRichiestaByIdentificativo(identificativoRichiesta);
    if (richiesta != null) {
      TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
      TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
      log.info("Manage response OK to device: {}", deviceType);
      switch (deviceType) {
      case TRACKYCARD:
      case TES_TRA_FREJUS_MONTE_BIANCO:
        try {
          assignDetailOnDevice(richiesta, deviceOrderMessage);
          DeviceMessageDetail deviceMessageDetail = deviceOrderMessage.getDeviceMessageDetail();
          if (deviceMessageDetail != null) {
            saveRaggruppamentoRichiesteFornitore(deviceMessageDetail, tipoDispositivo, richiesta);
          }
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
        break;
      // FIXME non dovrebbe mai succedere che arrivi qui un messaggio per TELEPASS_ITALIANO
      case TELEPASS_ITALIANO:
        log.warn("For device {} the serial number is already present, because device before was IN_DEPOSITO.", deviceType);
        break;

      // Non dovrebbe mai succedere che arrivi qui un messaggio per VIACARD
      case VIACARD:
        throw new IllegalArgumentException("DeviceOrderedMessage cannot handle device with type: " + deviceType);
      default:
        break;
      }

      changeStatoRichiesta(richiesta);
    } else {
      log.info("Not found Richiesta: {}", identificativoRichiesta);
    }
  }

  public void consumeDeviceMessageWithOtherInformations(String identificativoRichiesta,
                                                        DeviceEventMessage eventMessage) throws FsmExecuteCommandException {
    Richiesta richiesta = retrieveRichiestaByIdentificativo(identificativoRichiesta);
    if (richiesta != null) {
      TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
      TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
      log.info("Manage response OK to device: {}", deviceType);
      switch (deviceType) {
      case VIACARD:
        try {

          final DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
          final String serialNumber = deviceDataDTO.getDeviceCode();
          assignDetailOnDevice(richiesta, serialNumber, deviceType);

          final AdditionalDetailsDTO additionalDetailsDTO = eventMessage.getAdditionalDetailsDTO();
          final Instant elaborationDate = additionalDetailsDTO.getElaborationDate();
          final String fileName = additionalDetailsDTO.getFileName();
          saveRaggruppamentoRichiesteFornitore(elaborationDate, fileName, tipoDispositivo, richiesta);

        } catch (Exception e) {
          throw new RuntimeException(e);
        }
        break;

      // Qui dovrebbero arrivare solo messaggi per VIACARD
      case TRACKYCARD:
      case TES_TRA_FREJUS_MONTE_BIANCO:
      case TELEPASS_ITALIANO:
      default:
        throw new IllegalArgumentException("DeviceEventMessage cannot handle device with type: " + deviceType);
      }

      changeStatoRichiesta(richiesta);
    } else {
      log.info("Not found Richiesta: {}", identificativoRichiesta);
    }
  }

  private void assignDetailOnDevice(Richiesta richiesta, DeviceOrderedMessage deviceOrderMessage) throws Exception {
    String identificativo = richiesta.getIdentificativo();
    String serialNumber = null;
    DeviceInfo deviceInfo = deviceOrderMessage.getDeviceInfo();
    ZonedDateTime expireDate = null;
    if (deviceInfo != null && deviceInfo instanceof SimpleDeviceInfo) {
      SimpleDeviceInfo simpleDeviceInfo = (SimpleDeviceInfo) deviceInfo;
      serialNumber = simpleDeviceInfo.getDeviceSerialNumber()
                                     .getCode();
      log.info("Serial number {} for device {} on Richiesta {}", serialNumber, simpleDeviceInfo.getDeviceType()
                                                                                               .getType(),
               identificativo);
      expireDate = simpleDeviceInfo.getExpireDate();
      log.info("Expire Date {} for device {} on Richiesta {}", expireDate, simpleDeviceInfo.getDeviceType()
                                                                                           .getType(),
               identificativo);
    } else if (deviceOrderMessage.getObuId() != null) {
      serialNumber = deviceOrderMessage.getObuId()
                                       .getCode();
      log.info("retrieved SerialNumber {} on Richiesta {}", serialNumber, identificativo);
    }

    assignSerialeOnDevice(richiesta, serialNumber);
    assignExpireDateOnDevice(richiesta, expireDate);
  }

  private void assignDetailOnDevice(Richiesta richiesta, String serialNumber, Object deviceType) throws Exception {
    String identificativo = richiesta.getIdentificativo();

    log.info("Serial number {} for device {} on Richiesta {}", serialNumber, deviceType, identificativo);
    assignSerialeOnDevice(richiesta, serialNumber);
  }

  private void assignExpireDateOnDevice(Richiesta richiesta, ZonedDateTime expireDate) throws Exception {
    if (expireDate != null) {
      Optional<Dispositivo> optDispositivo = getDispositivo(richiesta);
      if (optDispositivo.isPresent()) {
        Dispositivo dispositivo = optDispositivo.get();
        log.info("Set data scadenza {} on dispositivo {} of Richiesta {}", expireDate, dispositivo, richiesta.getIdentificativo());
        dispositivo.setDataScadenza(expireDate);
        dispositivoServiceExt.save(dispositivo);
      } else {
        log.warn("Not found device....to assign Data scadenza {} on device of richiesta {}", expireDate, richiesta.getIdentificativo());
      }
    } else {
      log.warn("ExpireDate is null");
    }

  }

  /**
   * Used to manage device (set serial number) without elaboration flow by File
   *
   * @param identificativoRichiesta
   * @param serialNumber
   * @throws FsmExecuteCommandException
   */
  public void consumeDeviceMessage(String identificativoRichiesta, DeviceOrderedMessage message) throws FsmExecuteCommandException {
    consumeDeviceMessageWithOtherInformations(identificativoRichiesta, message);
  }

  private Richiesta retrieveRichiestaByIdentificativo(String identificativo) {
    log.info("Retrieve Richiesta by identificativo {}", identificativo);
    Richiesta richiesta = fsmRichiestaUtil.findRichiesta(identificativo);
    log.info("Found richiesta: {}", richiesta);
    return richiesta;
  }

  private void changeStatoRichiesta(Richiesta richiesta) throws FsmExecuteCommandException {
    String identificativo = richiesta.getIdentificativo();
    richiesta = fsmRichiestaUtil.changeStatusToRichiesta(richiesta, COMMAND, null, null);
    if (richiesta != null) {
      log.info("{}: {}", LOG_STRING, identificativo);
    } else {
      log.error("NOT {}: {}", LOG_STRING, identificativo);
    }
  }

  private void updateAssociazioneDvByTargaNazione(Richiesta richiesta) {
    String associazione = richiesta.getAssociazione();
    String country = richiesta.getCountry();

    it.fai.ms.efservice.client.dto.VehicleDTO vehicle = vehicleClient.findVehiclesByTargaAndNazione(jwt, associazione, country);
    String identificativoVeicolo = vehicle.getIdentificativo();

    Dispositivo device = richiesta.getDispositivos()
                                  .stream()
                                  .findFirst()
                                  .get();
    Set<AssociazioneDV> associazioneDispositivoVeicolos = device.getAssociazioneDispositivoVeicolos();
    AssociazioneDV associazioneDV = associazioneDispositivoVeicolos.stream()
                                                                   .findFirst()
                                                                   .orElse(new AssociazioneDV());
    associazioneDV.setUuidVeicolo(identificativoVeicolo);
    associazioneDV.data(Instant.now());
    associazioneDV.dispositivo(device);
    dispositivoServiceExt.updateAssociazioneDv(associazioneDV);
  }

  private void assignSerialeOnDevice(Richiesta richiesta, String serialeDispositivo) throws Exception {
    if (StringUtils.isNotBlank(serialeDispositivo)) {
      Optional<Dispositivo> optDispositivo = richiesta.getDispositivos()
                                                      .stream()
                                                      .findFirst();
      log.info("Find dispositivo? {}", optDispositivo);
      if (optDispositivo.isPresent()) {
        Dispositivo dispositivo = optDispositivo.get();
        log.info("Set seriale {} on dispositivo {} of Richiesta {}", serialeDispositivo, dispositivo, richiesta.getIdentificativo());
        dispositivo.setSeriale(serialeDispositivo);
        dispositivoServiceExt.save(dispositivo);

        reAssignPanOnGoBoxIfNecessary(dispositivo);

      } else {
        log.warn("Not found device....to assign Seriale {} on device of richiesta {}", serialeDispositivo, richiesta.getIdentificativo());
      }
    } else {
      log.warn("Seriale is null");
    }
  }

  private Optional<Dispositivo> getDispositivo(Richiesta richiesta) {
    Optional<Dispositivo> optDispositivo = richiesta.getDispositivos()
                                                    .stream()
                                                    .findFirst();
    log.info("Find dispositivo? {}", optDispositivo);
    return optDispositivo;
  }

  /**
   * Metodo per riassegnare il nuovo seriale del dispositivo di tipo TrackyCard sul GoBox legato al vecchio dispositivo
   * TrackyCard. (Caso di Variazioni con sostituzione).
   *
   * @param dispositivo
   */
  private void reAssignPanOnGoBoxIfNecessary(Dispositivo dispositivo) {
    if (dispositivo.getTipoDispositivo() != null && dispositivo.getTipoDispositivo()
                                                               .getNome()
                                                               .equals(TipoDispositivoEnum.TRACKYCARD)) {

      String serialeSostituito = dispositivo.getSerialeSostituito();
      if (StringUtils.isNotBlank(serialeSostituito)) {
        Optional<Dispositivo> deviceGoBoxOpt = getDeviceGoBoxByDeviceSubstitute(serialeSostituito);
        if (deviceGoBoxOpt.isPresent()) {
          Dispositivo goBox = deviceGoBoxOpt.get();
          Optional<StatoDispositivoServizio> sdsOpt = goBox.getStatoDispositivoServizios()
                                                           .stream()
                                                           .findFirst();
          if (sdsOpt.isPresent()) {
            StatoDispositivoServizio sds = sdsOpt.get();
            if (sds.getTipoServizio() != null && sds.getTipoServizio()
                                                    .getNome()
                                                    .equals("PEDAGGI_AUSTRIA")) {
              String seriale = dispositivo.getSeriale();
              dispositivoServiceExt.updateStatoDispositivoServizio(sds.pan(seriale));
              log.info("Update stato dispositivo servizio with new PAN: {}", seriale);
            }
          }
        } else {
          log.info("Not found related Device GO BOX");
        }
      } else {
        log.info("Skip beacuse this is device of new request");
      }
    } else {
      log.info("Skip because device type is {}", (dispositivo.getTipoDispositivo() != null) ? dispositivo.getTipoDispositivo()
                                                                                                         .getNome()
                                                                                            : null);
    }
  }

  private Optional<Dispositivo> getDeviceGoBoxByDeviceSubstitute(String serialeDeviceSostituito) {
    Optional<Dispositivo> deviceGoBoxOpt = dispositivoServiceExt.findGoBoxDeviceRelated(serialeDeviceSostituito);
    return deviceGoBoxOpt;
  }

  public void saveRaggruppamentoRichiesteFornitore(DeviceMessageDetail deviceMessageDetail, TipoDispositivo tipoDispositivo,
                                                   Richiesta richiesta) {

    FileElaborationDetail fileInformation = (FileElaborationDetail) deviceMessageDetail;
    Instant dataGenerazioneFile = fileInformation.getElaborationDate();
    String nomeFileRicevuto = fileInformation.getFileName();
    saveRaggruppamentoRichiesteFornitore(dataGenerazioneFile, nomeFileRicevuto, tipoDispositivo, richiesta);

  }

  // FIXME se ci sono più listener in ascolto, vengono create piu' righe sul raggruppamento
  public void saveRaggruppamentoRichiesteFornitore(Instant dataGenerazioneFile, String nomeFileRicevuto, TipoDispositivo tipoDispositivo,
                                                   Richiesta richiesta) {

    // findByNomeFileRicevutoAndDataGenerazioneFileAndTipoDispositivo ha problemi con l'Instant
    // sembrava che il db salvasse ma non selezionasse i millisecondi della "dataGenerazioneFile".
    // senza troncare ai secondi, viene salvato un valore su db (query con format della data lo mostratno),
    // ma non viene mai selezionato dalla select, e saveRaggruppamentoRichiesteFornitore crea N raggruppamenti, invece
    // di 1...
    // e' stato percio' inserito un filtro lato JAVA
    // ad oggi il bug non si ripresenta, ma abbiamo lasciato il filtro lato JAVA per loggare i risultati della find e
    // del filtro

    List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentiRichieste = groupRichiesteFornitoreServiceExt.findAllByNomeFileAndTipoDispositivo(nomeFileRicevuto,
                                                                                                                                                 tipoDispositivo);
    RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = null;

    if (raggruppamentiRichieste == null || raggruppamentiRichieste.isEmpty()) {
      log.info("Create new raggruppamento richieste ordine fornitore...");
      Set<Richiesta> richieste = new HashSet<>();
      richieste.add(richiesta);
      raggruppamentoRichiesteOrdineFornitore = new RaggruppamentoRichiesteOrdineFornitore().nomeFileRicevuto(nomeFileRicevuto)
                                                                                           .dataGenerazioneFile(dataGenerazioneFile)
                                                                                           .tipoDispositivo(tipoDispositivo)
                                                                                           .richiestas(richieste)
                                                                                           .dataUltimaAcquisizione(null);
    } else {
      if (raggruppamentiRichieste.size() == 1) {
        raggruppamentoRichiesteOrdineFornitore = raggruppamentiRichieste.get(0);
        log.info("Found raggruppamento by {} - {} - {} : {}", nomeFileRicevuto, dataGenerazioneFile, tipoDispositivo,
                 raggruppamentoRichiesteOrdineFornitore);
        raggruppamentoRichiesteOrdineFornitore.addRichiesta(richiesta);
        raggruppamentoRichiesteOrdineFornitore.setDataGenerazioneFile(dataGenerazioneFile);
      } else {
        String messageError = String.format("Found %s %s. This is not possible...", raggruppamentiRichieste.size(),
                                            RaggruppamentoRichiesteOrdineFornitore.class.getSimpleName());
        log.error(messageError);
        throw new IllegalArgumentException(messageError);
      }
    }

    raggruppamentoRichiesteOrdineFornitore = groupRichiesteFornitoreServiceExt.saveAnFlush(raggruppamentoRichiesteOrdineFornitore);
    richiesta.setRaggruppamentoRichiesteOrdineFornitore(raggruppamentoRichiesteOrdineFornitore);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = RuntimeException.class)
  public void consumeLicensePlateChange(String identificativoRichiesta) throws FsmExecuteCommandException {
    Richiesta richiesta = retrieveRichiestaByIdentificativo(identificativoRichiesta);
    try {
      if (richiesta != null) {
        TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
        TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
        switch (deviceType) {
        case TELEPASS_ITALIANO:
          updateAssociazioneDvByTargaNazione(richiesta);
          changeStatoRichiesta(richiesta);
          break;
        case VIACARD:
          log.error("Per il tipo dispositivo {} non dovrebbe esserci nessun flusso di notifica per la variazione della targa....",
                    deviceType);
          break;

        default:
          log.warn("Not manage this device {} to request of change license plate on Richiesta {}", deviceType, richiesta);
          break;
        }
      }
    } catch (FsmExecuteCommandException e) {
      if (e.getMessage()
           .contains("ORDINE_SOSPESO")) {
        log.warn("Non è possibile cambiare lo stato per un ordine sospeso: {}", identificativoRichiesta);
      } else {
        throw e;
      }
    }
  }

  public void changeStatoRichiesta(String contractCode, String serialNumberDevice) throws FsmExecuteCommandException {
    List<Richiesta> optRichiesta = retrieveRichiestaByContrattoAndSerialeDispositivo(contractCode, serialNumberDevice);
    if (!optRichiesta.isEmpty()) {
      Richiesta richiesta = optRichiesta.get(0);
      log.info("Start change status for Richiesta: {}", richiesta);
      changeStatoRichiesta(richiesta);
    } else {
      log.error("Not found request....");
    }
  }

  private List<Richiesta> retrieveRichiestaByContrattoAndSerialeDispositivo(String codiceContratto, String serialNumberDevice) {
    log.info("Retrieve Richiesta by Contratto {} - serialNumberDevice {} - Stato {} - RequestTypes {}", codiceContratto, serialNumberDevice,
             StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO, requestTypes);
    List<Richiesta> optRichiesta = fsmRichiestaUtil.findByCodContrattoAndSerialDeviceAndStateAndRequestTypes(codiceContratto,
                                                                                                             serialNumberDevice,
                                                                                                             StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO,
                                                                                                             requestTypes);
    log.info("Found Optional Request: {}", optRichiesta);
    return optRichiesta;
  }

}
