package it.fai.ms.efservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;

/**
 * Spring Data JPA repository for the ConfigurazioneDispositiviServizi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConfigurazioneDispositiviServiziRepository extends JpaRepository<ConfigurazioneDispositiviServizi, Long>, JpaSpecificationExecutor<ConfigurazioneDispositiviServizi> {

  @Query("select cds from ConfigurazioneDispositiviServizi cds INNER JOIN cds.tipoDispositivo td INNER JOIN cds.tipoServizio ts WHERE td.nome = :tipoDispositivo AND ts.nome = :servizio")
  Optional<ConfigurazioneDispositiviServizi> findConfigurazione(@Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo,
                                                                @Param("servizio") String service);

  @Query("select cds from ConfigurazioneDispositiviServizi cds INNER JOIN cds.tipoDispositivo td INNER JOIN cds.tipoServizio ts WHERE cds.modalitaGestione = :modalitaGestione AND td.nome = :tipoDispositivo AND ts.nome = :servizio")
  Optional<ConfigurazioneDispositiviServizi> findConfigurazione(@Param("tipoDispositivo") TipoDispositivoEnum tipoDispositivo,
                                                                @Param("servizio") String service,
                                                                @Param("modalitaGestione") ModalitaGestione modalitaGestione);

}
