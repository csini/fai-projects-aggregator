package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import java.util.List;

/**
 * Service Interface for managing TipoServizio.
 */
public interface TipoServizioService {

    /**
     * Save a tipoServizio.
     *
     * @param tipoServizioDTO the entity to save
     * @return the persisted entity
     */
    TipoServizioDTO save(TipoServizioDTO tipoServizioDTO);

    /**
     *  Get all the tipoServizios.
     *
     *  @return the list of entities
     */
    List<TipoServizioDTO> findAll();

    /**
     *  Get the "id" tipoServizio.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TipoServizioDTO findOne(Long id);

    /**
     *  Delete the "id" tipoServizio.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
