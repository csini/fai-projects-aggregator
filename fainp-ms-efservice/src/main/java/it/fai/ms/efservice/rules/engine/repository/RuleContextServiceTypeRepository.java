package it.fai.ms.efservice.rules.engine.repository;

import java.util.Set;

import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;

public interface RuleContextServiceTypeRepository {

  Set<TipoServizio> findAll();

  RuleEngineServiceType newStarContextServiceType();

}
