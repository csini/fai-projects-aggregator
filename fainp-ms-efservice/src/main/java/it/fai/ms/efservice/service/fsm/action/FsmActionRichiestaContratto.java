package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

public class FsmActionRichiestaContratto implements Action<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmActionRichiestaContratto.class);

  private final FsmSenderToQueue senderFsmService;

  @Autowired
  public FsmActionRichiestaContratto(FsmSenderToQueue senderFsmService) {
    this.senderFsmService = senderFsmService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    log.debug("Execute action....");

    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        final Richiesta richiesta = (Richiesta) object;
        senderFsmService.sendMessageTo(richiesta);
      }
    }

  }

}
