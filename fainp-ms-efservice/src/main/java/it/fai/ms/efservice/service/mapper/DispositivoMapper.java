package it.fai.ms.efservice.service.mapper;

import java.time.ZonedDateTime;
import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.dto.DispositivoDTO;

/**
 * Mapper for the entity Dispositivo and its DTO DispositivoDTO.
 */
@Mapper(componentModel = "spring", uses = {ContrattoMapper.class, TipoDispositivoMapper.class, StatoDispositivoServizioMapper.class })
public interface DispositivoMapper extends EntityMapper <DispositivoDTO, Dispositivo> {

    @Override
    @Mapping(source = "contratto.id", target = "contrattoId")
    @Mapping(source = "contratto.codContrattoCliente", target = "contrattoCodContrattoCliente")

    @Mapping(source = "tipoDispositivo.id", target = "tipoDispositivoId")
    @Mapping(source = "tipoDispositivo.nome", target = "tipoDispositivoNome")
    DispositivoDTO toDto(Dispositivo dispositivo);

    @Override
    @Mapping(source = "contrattoId", target = "contratto")
    @Mapping(target = "associazioneDispositivoVeicolos", ignore = false)
    @Mapping(target = "statoDispositivoServizios", ignore = false)

    @Mapping(source = "tipoDispositivoId", target = "tipoDispositivo")
    @Mapping(target = "richiestas", ignore = true)
    Dispositivo toEntity(DispositivoDTO dispositivoDTO);
    default Dispositivo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Dispositivo dispositivo = new Dispositivo();
        dispositivo.setId(id);
        return dispositivo;
    }

    default it.fai.ms.common.jms.dto.efservice.DispositivoDTO toJmsDto(Dispositivo dispositivo){
      it.fai.ms.common.jms.dto.efservice.DispositivoDTO dto = new it.fai.ms.common.jms.dto.efservice.DispositivoDTO();
      dto.setCodiceCliente(dispositivo.getContratto().getClienteFai().getCodiceCliente());
      dto.setContratto(dispositivo.getContratto().getCodContrattoCliente());
      Optional.ofNullable(dispositivo.getDataScadenza()).map(ZonedDateTime::toInstant).ifPresent(dto::setDataScadenza);
      dto.setDataSpedizione(dispositivo.getDataSpedizione());
      dto.setIdentificativo(dispositivo.getIdentificativo());
      dto.setSeriale(dispositivo.getSeriale());
      dto.setStatoDispositivo(dispositivo.getStato());
      Optional.ofNullable(dispositivo.getTipoDispositivo()).map(t->t.getNome()).map(TipoDispositivoEnum::name).ifPresent(dto::setTipo);
      dto.setTipoHardware(dispositivo.getTipoHardware());

      return dto;
    }
}
