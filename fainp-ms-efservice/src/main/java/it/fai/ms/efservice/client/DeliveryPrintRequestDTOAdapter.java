package it.fai.ms.efservice.client;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class DeliveryPrintRequestDTOAdapter {

  public DeliveryPrintRequestDTOAdapter() {
  }

  public DeliveryPrintRequestDTOAdapter(Instant instant) {
    this.deliveryDate = instant.atZone(ZoneId.systemDefault())
                               .toLocalDate();
  }

  private LocalDate deliveryDate;

  private List<DeliveryDTO> deliveryList;

  public LocalDate getDeliveryDate() {
    return deliveryDate;
  }

  public void setDeliveryDate(LocalDate deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public List<DeliveryDTO> getDeliveryList() {
    return deliveryList;
  }

  public void setDeliveryList(List<DeliveryDTO> deliveryList) {
    this.deliveryList = deliveryList;
  }

  public void addDeliveryDTO(DeliveryDTO deliveryDTO) {
    if (this.deliveryList == null) {
      this.deliveryList = new ArrayList<DeliveryDTO>();
    }
    this.deliveryList.add(deliveryDTO);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DeliveryPrintRequestDTOAdapter [deliveryDate=")
           .append(deliveryDate)
           .append(", deliveryList=")
           .append(deliveryList)
           .append("]");
    return builder.toString();
  }

}
