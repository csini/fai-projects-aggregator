package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.telepass.TelepassEuRipetiInvioInboundDTO;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.consumer.RipetiInvioTelepassEuConsumer;

@Service
@Transactional
public class JmsListenerInboundRipetiInvioService extends JmsObjectMessageListenerTemplate<TelepassEuRipetiInvioInboundDTO> implements JmsTopicListener {

  private final Logger                        log = LoggerFactory.getLogger(JmsTopicSenderService.class);
  private final RipetiInvioTelepassEuConsumer ripetiInvioTelepassEuConsumer;

  public JmsListenerInboundRipetiInvioService(RipetiInvioTelepassEuConsumer ripetiInvioTelepassEuConsumer) throws Exception {
    this.ripetiInvioTelepassEuConsumer = ripetiInvioTelepassEuConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.TELEPASS_EU_EVENTI_INBOUND;
  }

  @Override
  protected void consumeMessage(TelepassEuRipetiInvioInboundDTO dto) {
    try {
      ripetiInvioTelepassEuConsumer.ripetiInvio(dto);
    } catch (Exception e) {
      log.error("Error", e);
      throw new RuntimeException(e);
    }

  }

}