package it.fai.ms.efservice.service.fsm;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

public abstract class AbstractFsmDispositivo {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String USER_MANUAL_PREFIX = "MU_";

  protected abstract StateMachine<StatoDispositivo, DispositivoEvent> getStateMachine();

  public Dispositivo executeCommandToChangeState(DispositivoEvent command, Dispositivo dispositivo) throws Exception {
    return executeCommandToChangeState(command, dispositivo, null);
  }

  public boolean sendEventToFsm(StateMachine<StatoDispositivo, DispositivoEvent> sm, DispositivoEvent command, Dispositivo dispositivo,
                                String nota) {
    final Message<DispositivoEvent> message = MessageBuilder.withPayload(command)
                                                            .setHeader("object", dispositivo)
                                                            .setHeader("nota", nota)
                                                            .build();
    boolean sended = sm.sendEvent(message);
    log.debug("Sended event to change status: {}", sended);
    if(!sended) {
      throw new RuntimeException("Not sended event...so not change status by command: " + command);
    }
    return sended;
  }

  public abstract Dispositivo executeCommandToChangeState(DispositivoEvent command, Dispositivo dispositivo, String nota) throws Exception;

  public Collection<DispositivoEvent> getAllAvailableCommand(Dispositivo dispositivo) {
    Enum<StatoDispositivo> fsmStatus = dispositivo.getStato();
    Collection<Transition<StatoDispositivo, DispositivoEvent>> transitions = getStateMachine().getTransitions();
    List<DispositivoEvent> deviceEvents = transitions.stream()
                                                     .filter(transition -> transition.getSource()
                                                                                     .getId()
                                                                                     .name()
                                                                                     .equals(fsmStatus.name()))
                                                     .filter(transition -> Objects.nonNull(transition.getTrigger()))
                                                     .map(transition -> transition.getTrigger()
                                                                                  .getEvent())
                                                     .collect(toList());
    return deviceEvents;
  }
  
  public List<StatoDispositivo> getAllTargetStatusForCommand(DispositivoEvent command) {
	    Collection<Transition<StatoDispositivo, DispositivoEvent>> transitions = getStateMachine().getTransitions();
	    List<StatoDispositivo> stati = transitions.stream()
	                                                     .filter(transition -> Objects.nonNull(transition.getTrigger())
	                                                    		 && transition.getTrigger().getEvent() == command)
	                                                     .map(transition -> transition.getTarget().getId())
	                                                     .collect(toList());
	    return stati;
	  }

  public List<String> getUserAvailableCommand(Dispositivo obj) {
    Collection<DispositivoEvent> allAvailableCommand = getAllAvailableCommand(obj);
    List<String> userAvailableCommand = new ArrayList<>();

    for (DispositivoEvent event : allAvailableCommand) {
      String eventName = event.name();
      if (eventName.startsWith(USER_MANUAL_PREFIX)) {
        userAvailableCommand.add(eventName);
      }
    }

    return userAvailableCommand;
  }

  public boolean isAvailableCommand(Enum<?> command, Dispositivo device) {
    Collection<DispositivoEvent> allAvailableCommand = getAllAvailableCommand(device);
    for (DispositivoEvent event : allAvailableCommand) {
      Enum<?> enumCmd = null;
      if (event instanceof Enum<?>) {
        enumCmd = (Enum<?>) event;

      }
      if (enumCmd.name()
                 .equalsIgnoreCase(command.name())) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isAlreadyInTargetState(DispositivoEvent command, Dispositivo device) {
	    List<StatoDispositivo> stati = getAllTargetStatusForCommand(command);
	    return stati.contains(device.getStato());
 }

  public boolean isAutoTransition(Enum<?> command) {
    boolean foundCommand = true;

    Collection<Transition<StatoDispositivo, DispositivoEvent>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StatoDispositivo, DispositivoEvent>> iterator = transitions.iterator();
    while (iterator.hasNext()) {

      Transition<StatoDispositivo, DispositivoEvent> tx = iterator.next();
      Trigger<StatoDispositivo, DispositivoEvent> trigger = tx.getTrigger();
      if (trigger != null) {
        DispositivoEvent event = trigger.getEvent();
        if (event.name()
                 .equalsIgnoreCase(command.name())) {
          foundCommand = false;
          break;
        }
      }
    }

    return foundCommand;
  }

  public Dispositivo setOperazioniPossibiliDispositivo(@Nonnull Dispositivo dispositivo) {
    log.debug("Find operazioni possibili dispositivo: {}", dispositivo);
    if (dispositivo == null) {
      log.error("Dispositivo is null");
      throw new RuntimeException("Dispositivo is null");
    }
    return dispositivo.operazioniPossibili(getOperazioniPossibiliDispositivo(dispositivo));
  }

  public String getOperazioniPossibiliDispositivo(@NotNull Dispositivo dispositivo) {
    String operationsBackOffice = null;
    boolean isNotVirtual = checkNotVirtualDevice(dispositivo);
    if (isNotVirtual) {
      if (dispositivo.getTipoDispositivo() != null && dispositivo.getTipoDispositivo()
                                                                 .getNome()
                                                                 .equals(TipoDispositivoEnum.GO_BOX)) {
        log.info("Device {} has not operations on BackOffice.", dispositivo.getTipoDispositivo() != null ? dispositivo.getTipoDispositivo()
                                                                                                                      .getNome()
                                                                                                         : dispositivo.getIdentificativo());
        return operationsBackOffice;
      }

      List<String> userAvailableCommand = getUserAvailableCommand(dispositivo);
      if (userAvailableCommand != null && !userAvailableCommand.isEmpty()) {
        operationsBackOffice = getOperazioniPossibiliByCommands(userAvailableCommand);
      } else {
        log.warn(" Not found manual operation by BAckOffice to device: {}", dispositivo);
      }

    }

    log.info("Operazioni possibili da BackOffice per il Dispositivo [Identificativo: {} - Tipo: {} - Seriale: {}] : {}",
             dispositivo.getIdentificativo(), dispositivo.getTipoDispositivo(), dispositivo.getSeriale(), operationsBackOffice);
    return operationsBackOffice;
  }

  private boolean checkNotVirtualDevice(Dispositivo dispositivo) {
    TipoDispositivo tipoDispositivo = dispositivo.getTipoDispositivo();
    if (tipoDispositivo == null) {
      log.error("TipoDispositivo is NULL");
      log.warn("return default value = FALSE");
      return false;
    }

    Boolean isVirtuale = tipoDispositivo.isVirtuale();
    log.info("Dispositivo is Virtuale? {}", isVirtuale);
    return !isVirtuale;
  }

  private String getOperazioniPossibiliByCommands(List<String> userAvailableCommand) {
    StringBuilder sb = new StringBuilder();
    for (String command : userAvailableCommand) {
      sb.append(command);
      sb.append(',');
    }

    String operations = sb.toString();
    if (StringUtils.isNotBlank(operations)) {
      operations = operations.substring(0, (operations.length() - 1));
    }
    return operations;
  }
}
