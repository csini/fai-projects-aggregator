package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

/**
 * A DTO for the Ordine entity.
 */
public class OrdineDTO implements Serializable {

    private Long id;

    private String identificativo;

    @NotNull
    private Instant data;

    @NotNull
    private TipoRichiesta tipo;

    @NotNull
    private StatoRichiesta stato;

    private String ultimaNota;

    private String uuidDocumento;

    private ClienteFaiDTO clienteFai;

    private Long contrattoId;

    private String contrattoCodContrattoCliente;

    private Set<DatiAggiuntiviDTO> datiAggiuntivis = new HashSet<>();

    private Set<DispositivoDTO> dispositivos = new HashSet<>();

    private Set<TipoDispositivoDTO> tipoDispositivos = new HashSet<>();

    private Set<TipoServizioDTO> tipoServizios = new HashSet<>();

    private IndirizzoSpedizioneOrdiniDTO indirizzoDiTransito;

    private IndirizzoSpedizioneOrdiniDTO indirizzoDestinazioneFinale;
    
    private Map<String, String> mapDispositivoRIchiesta = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public Instant getData() {
        return data;
    }

    public void setData(Instant data) {
        this.data = data;
    }

    public TipoRichiesta getTipo() {
        return tipo;
    }

    public void setTipo(TipoRichiesta tipo) {
        this.tipo = tipo;
    }

    public StatoRichiesta getStato() {
        return stato;
    }

    public void setStato(StatoRichiesta stato) {
        this.stato = stato;
    }

    public String getUltimaNota() {
        return ultimaNota;
    }

    public void setUltimaNota(String ultimaNota) {
        this.ultimaNota = ultimaNota;
    }

    public String getUuidDocumento() {
        return uuidDocumento;
    }

    public void setUuidDocumento(String uuidDocumento) {
        this.uuidDocumento = uuidDocumento;
    }

    public Long getContrattoId() {
        return contrattoId;
    }

    public void setContrattoId(Long contrattoId) {
        this.contrattoId = contrattoId;
    }

    public String getContrattoCodContrattoCliente() {
        return contrattoCodContrattoCliente;
    }

    public void setContrattoCodContrattoCliente(String contrattoCodContrattoCliente) {
        this.contrattoCodContrattoCliente = contrattoCodContrattoCliente;
    }

    public Set<DatiAggiuntiviDTO> getDatiAggiuntivis() {
        return datiAggiuntivis;
    }

    public void setDatiAggiuntivis(Set<DatiAggiuntiviDTO> datiAggiuntivis) {
        this.datiAggiuntivis = datiAggiuntivis;
    }

    public Set<DispositivoDTO> getDispositivos() {
        return dispositivos;
    }

    public void setDispositivos(Set<DispositivoDTO> dispositivos) {
        this.dispositivos = dispositivos;
    }

    public Set<TipoDispositivoDTO> getTipoDispositivos() {
        return tipoDispositivos;
    }

    public void setTipoDispositivos(Set<TipoDispositivoDTO> tipoDispositivos) {
        this.tipoDispositivos = tipoDispositivos;
    }

    public Set<TipoServizioDTO> getTipoServizios() {
        return tipoServizios;
    }

    public void setTipoServizios(Set<TipoServizioDTO> tipoServizios) {
        this.tipoServizios = tipoServizios;
    }
    
    public ClienteFaiDTO getClienteFai() {
      return clienteFai;
    }

    public void setClienteFai(ClienteFaiDTO clienteFai) {
      this.clienteFai = clienteFai;
    }

    public IndirizzoSpedizioneOrdiniDTO getIndirizzoDiTransito() {
      return indirizzoDiTransito;
    }

    public void setIndirizzoDiTransito(IndirizzoSpedizioneOrdiniDTO indirizzoDiTransito) {
      this.indirizzoDiTransito = indirizzoDiTransito;
    }

    public IndirizzoSpedizioneOrdiniDTO getIndirizzoDestinazioneFinale() {
      return indirizzoDestinazioneFinale;
    }

    public void setIndirizzoDestinazioneFinale(IndirizzoSpedizioneOrdiniDTO indirizzoDestinazioneFinale) {
      this.indirizzoDestinazioneFinale = indirizzoDestinazioneFinale;
    }

    public Map<String, String> getMapDispositivoRIchiesta() {
      return mapDispositivoRIchiesta;
    }

    public void setMapDispositivoRIchiesta(Map<String, String> mapDispositivoRIchiesta) {
      this.mapDispositivoRIchiesta = mapDispositivoRIchiesta;
    }

    @Override
    public String toString() {
      final int maxLen = 10;
      StringBuilder builder = new StringBuilder();
      builder.append("OrdineDTO [");
      if (id != null) {
        builder.append("id=");
        builder.append(id);
        builder.append(", ");
      }
      if (identificativo != null) {
        builder.append("identificativo=");
        builder.append(identificativo);
        builder.append(", ");
      }
      if (data != null) {
        builder.append("data=");
        builder.append(data);
        builder.append(", ");
      }
      if (tipo != null) {
        builder.append("tipo=");
        builder.append(tipo);
        builder.append(", ");
      }
      if (stato != null) {
        builder.append("stato=");
        builder.append(stato);
        builder.append(", ");
      }
      if (ultimaNota != null) {
        builder.append("ultimaNota=");
        builder.append(ultimaNota);
        builder.append(", ");
      }
      if (uuidDocumento != null) {
        builder.append("uuidDocumento=");
        builder.append(uuidDocumento);
        builder.append(", ");
      }
      if (clienteFai != null) {
        builder.append("clienteFai=");
        builder.append(clienteFai);
        builder.append(", ");
      }
      if (contrattoId != null) {
        builder.append("contrattoId=");
        builder.append(contrattoId);
        builder.append(", ");
      }
      if (contrattoCodContrattoCliente != null) {
        builder.append("contrattoCodContrattoCliente=");
        builder.append(contrattoCodContrattoCliente);
        builder.append(", ");
      }
      if (dispositivos != null) {
        builder.append("dispositivos=");
        builder.append(toString(dispositivos, maxLen));
        builder.append(", ");
      }
      if (tipoDispositivos != null) {
        builder.append("tipoDispositivos=");
        builder.append(toString(tipoDispositivos, maxLen));
        builder.append(", ");
      }
      if (tipoServizios != null) {
        builder.append("tipoServizios=");
        builder.append(toString(tipoServizios, maxLen));
        builder.append(", ");
      }
      if (indirizzoDiTransito != null) {
        builder.append("indirizzoDiTransito=");
        builder.append(indirizzoDiTransito);
        builder.append(", ");
      }
      if (indirizzoDestinazioneFinale != null) {
        builder.append("indirizzoDestinazioneFinale=");
        builder.append(indirizzoDestinazioneFinale);
      }
      builder.append("]");
      return builder.toString();
    }

    private String toString(Collection<?> collection, int maxLen) {
      StringBuilder builder = new StringBuilder();
      builder.append("[");
      int i = 0;
      for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && (i < maxLen); i++) {
        if (i > 0) {
          builder.append(", ");
        }
        builder.append(iterator.next());
      }
      builder.append("]");
      return builder.toString();
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + ((clienteFai == null) ? 0 : clienteFai.hashCode());
      result = (prime * result) + ((contrattoCodContrattoCliente == null) ? 0 : contrattoCodContrattoCliente.hashCode());
      result = (prime * result) + ((contrattoId == null) ? 0 : contrattoId.hashCode());
      result = (prime * result) + ((data == null) ? 0 : data.hashCode());
      result = (prime * result) + ((dispositivos == null) ? 0 : dispositivos.hashCode());
      result = (prime * result) + ((id == null) ? 0 : id.hashCode());
      result = (prime * result) + ((identificativo == null) ? 0 : identificativo.hashCode());
      result = (prime * result) + ((stato == null) ? 0 : stato.hashCode());
      result = (prime * result) + ((tipo == null) ? 0 : tipo.hashCode());
      result = (prime * result) + ((tipoDispositivos == null) ? 0 : tipoDispositivos.hashCode());
      result = (prime * result) + ((tipoServizios == null) ? 0 : tipoServizios.hashCode());
      result = (prime * result) + ((ultimaNota == null) ? 0 : ultimaNota.hashCode());
      result = (prime * result) + ((uuidDocumento == null) ? 0 : uuidDocumento.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      OrdineDTO other = (OrdineDTO) obj;
      if (clienteFai == null) {
        if (other.clienteFai != null) {
          return false;
        }
      } else if (!clienteFai.equals(other.clienteFai)) {
        return false;
      }
      if (contrattoCodContrattoCliente == null) {
        if (other.contrattoCodContrattoCliente != null) {
          return false;
        }
      } else if (!contrattoCodContrattoCliente.equals(other.contrattoCodContrattoCliente)) {
        return false;
      }
      if (contrattoId == null) {
        if (other.contrattoId != null) {
          return false;
        }
      } else if (!contrattoId.equals(other.contrattoId)) {
        return false;
      }
      if (data == null) {
        if (other.data != null) {
          return false;
        }
      } else if (!data.equals(other.data)) {
        return false;
      }
      if (dispositivos == null) {
        if (other.dispositivos != null) {
          return false;
        }
      } else if (!dispositivos.equals(other.dispositivos)) {
        return false;
      }
      if (id == null) {
        if (other.id != null) {
          return false;
        }
      } else if (!id.equals(other.id)) {
        return false;
      }
      if (identificativo == null) {
        if (other.identificativo != null) {
          return false;
        }
      } else if (!identificativo.equals(other.identificativo)) {
        return false;
      }
      if (stato != other.stato) {
        return false;
      }
      if (tipo != other.tipo) {
        return false;
      }
      if (tipoDispositivos == null) {
        if (other.tipoDispositivos != null) {
          return false;
        }
      } else if (!tipoDispositivos.equals(other.tipoDispositivos)) {
        return false;
      }
      if (tipoServizios == null) {
        if (other.tipoServizios != null) {
          return false;
        }
      } else if (!tipoServizios.equals(other.tipoServizios)) {
        return false;
      }
      if (ultimaNota == null) {
        if (other.ultimaNota != null) {
          return false;
        }
      } else if (!ultimaNota.equals(other.ultimaNota)) {
        return false;
      }
      if (uuidDocumento == null) {
        if (other.uuidDocumento != null) {
          return false;
        }
      } else if (!uuidDocumento.equals(other.uuidDocumento)) {
        return false;
      }
      return true;
    }
}
