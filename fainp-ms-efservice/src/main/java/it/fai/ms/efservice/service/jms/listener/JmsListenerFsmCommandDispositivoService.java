package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.DispositivoFSMChangeStatusDTO;
import it.fai.ms.efservice.service.jms.consumer.ChangeStatusDispositivoConsumer;

@Service
@Transactional
public class JmsListenerFsmCommandDispositivoService extends JmsObjectMessageListenerTemplate<DispositivoFSMChangeStatusDTO> implements JmsTopicListener {

  private final Logger                          log = LoggerFactory.getLogger(getClass());
  private final ChangeStatusDispositivoConsumer changeStatusDispositivoConsumer;

  public JmsListenerFsmCommandDispositivoService(ChangeStatusDispositivoConsumer changeStatusDispositivoConsumer) {
    this.changeStatusDispositivoConsumer = changeStatusDispositivoConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.FSM_DISPOSITIVI;
  }

  @Override
  protected void consumeMessage(DispositivoFSMChangeStatusDTO dto) {
    try {
      changeStatusDispositivoConsumer.changeStatusByDTO(dto);
    } catch (Exception e) {
      log.error("Error", e);
      throw new RuntimeException(e);
    }
  }

}