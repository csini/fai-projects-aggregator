package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.IndirizzoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Indirizzo and its DTO IndirizzoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IndirizzoMapper extends EntityMapper <IndirizzoDTO, Indirizzo> {
    
    
    default Indirizzo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.setId(id);
        return indirizzo;
    }
}
