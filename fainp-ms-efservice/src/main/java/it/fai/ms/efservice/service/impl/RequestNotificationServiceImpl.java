package it.fai.ms.efservice.service.impl;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.efservice.RequestNotificationDto;
import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.message_v2.RequestNotificationMessage;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.RequestNotificationService;

@Service
public class RequestNotificationServiceImpl implements RequestNotificationService{
  
    
  private final JmsProperties jmsProperties;
  
  private final RichiestaRepositoryExt richiestaRepositoryExt;
  
  private final Logger _log = LoggerFactory.getLogger(getClass());
  
  @Autowired
  public RequestNotificationServiceImpl(JmsProperties _jmsProperties, RichiestaRepositoryExt _richiestaRepositoryExt) {
    this.jmsProperties = _jmsProperties;
    this.richiestaRepositoryExt = _richiestaRepositoryExt;
  }

  @Override
  public void sendNotification(DeviceEventMessage deviceEventMessage) {
    
    DeviceDataDTO deviceDataDTO = deviceEventMessage.getDeviceDataDTO();
    
    if ( StringUtils.isEmpty(deviceDataDTO.getRequestId())) {
      _log.warn("Request Id is null. Skip send JMS");
      return;
    }
     
    Richiesta richiesta = richiestaRepositoryExt.findOneByIdentificativo(deviceDataDTO.getRequestId());
  
    if ( richiesta != null ) {
      
      _log.debug("Found richiesta {}", richiesta);
      RequestNotificationMessage requestNotificationMessage = new RequestNotificationMessage();
      
      RequestNotificationDto requestNotificationDto = new RequestNotificationDto();
      requestNotificationDto.setTipoRichiesta(Objects.toString(richiesta.getTipo()));
      requestNotificationDto.setStatoRichiesta(Objects.toString(richiesta.getStato())); 
      requestNotificationDto.setAssociazione(richiesta.getAssociazione());
      requestNotificationDto.setIdentificativo(richiesta.getIdentificativo());
      requestNotificationDto.setTipoDispositivo(Objects.toString(richiesta.getTipoDispositivo().getNome()));
      requestNotificationDto.setSource(deviceEventMessage.getClass().getSimpleName() + "§" + deviceEventMessage.getType());

      requestNotificationMessage.setRequestNotificationDto(requestNotificationDto);
      JmsTopicSenderUtil.publish(jmsProperties, JmsTopicNames.REQUEST_NOTIFICATION, requestNotificationMessage);
       
    }else {
      _log.warn("Request {} not found. Skipped send JMS ", deviceDataDTO.getRequestId());
    }
    
  }
  
}
