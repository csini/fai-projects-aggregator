package it.fai.ms.efservice.wizard.repository;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceType;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;

@Service
public class WizardServiceTypeRepositoryImpl implements WizardServiceTypeRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final TipoDispositivoRepository deviceTypeRepository;

  @Autowired
  public WizardServiceTypeRepositoryImpl(final TipoDispositivoRepository _deviceTypeRepository) {
    deviceTypeRepository = _deviceTypeRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardServiceType> findAllByDeviceTypeId(final WizardDeviceTypeId _wizardDeviceTypeId) {
    Optional<TipoDispositivo> optionalDeviceType = deviceTypeRepository.findAll()
                                                                       .stream()
                                                                       .filter(deviceType -> deviceType.getNome()
                                                                                                       .name()
                                                                                                       .equals(_wizardDeviceTypeId.getId()))
                                                                       .findFirst();
    final Optional<Set<WizardServiceType>> optionalWizardServiceTypes = optionalDeviceType.map(deviceType -> deviceType.getTipoServizios())
                                                                                          .map(serviceTypeStream -> serviceTypeStream.stream()
                                                                                                                                     .map(serviceType -> new WizardServiceType(new WizardServiceTypeId(serviceType.getNome()),serviceType.getOrdinamento()))
                                                                                                                                     .collect(toSet()));
    final Set<WizardServiceType> serviceTypes = optionalWizardServiceTypes.orElse(new HashSet<>());
    _log.debug("Found filtered by {}: {}", _wizardDeviceTypeId, serviceTypes);
    return serviceTypes;
  }

}
