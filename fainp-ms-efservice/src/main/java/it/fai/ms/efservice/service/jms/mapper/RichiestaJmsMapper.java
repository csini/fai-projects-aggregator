package it.fai.ms.efservice.service.jms.mapper;

import static java.util.stream.Collectors.toMap;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.fai.ms.common.dml.efservice.dto.RichiestaDMLDTO;
import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.domain.Richiesta;

public class RichiestaJmsMapper {

  public RichiestaDMLDTO toRichiestaDMLDTO(Richiesta ord) {

    return toRichiestaDMLDTO(ord, null);
  }

  private RichiestaDMLDTO toRichiestaDMLDTO(Richiesta ord, RichiestaDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyRichiestaDMLDTO(ord.getIdentificativo());
    }

    dml.setUuid(ord.getIdentificativo());
    dml.setAnomalia(ord.getAnomalia());
    dml.setAssociazione(ord.getAssociazione());
    dml.setDataModificaStato(ord.getDataModificaStato());
    dml.setDispositivoDmlUniqueIdentifier(ord.getDispositivos() != null && !ord.getDispositivos()
                                                                               .isEmpty() ? ord.getDispositivos()
                                                                                               .stream()
                                                                                               .findFirst()
                                                                                               .get() != null ? ord.getDispositivos()
                                                                                                                   .stream()
                                                                                                                   .findFirst()
                                                                                                                   .get()
                                                                                                                   .getIdentificativo()
                                                                                                              : null
                                                                                          : null);
    dml.setNumeroContratto(ord.getContratto() != null ? ord.getContratto()
                                                           .getCodContrattoCliente()
                                                      : null);
    dml.setOperazioneDisponibili(ord.getOperazioniPossibili());
    dml.setOrdineClienteDmlUniqueIdentifier(ord.getOrdineCliente() != null ? ord.getOrdineCliente()
                                                                                .getIdentificativo()
                                                                           : null);
    dml.setStatoRichiesta(ord.getStato() != null ? ord.getStato()
                                                      .name()
                                                 : null);
    dml.setTipiServizio(ord.getTipiServizioList() != null ? Arrays.asList(ord.getTipiServizioList()) : null);
    dml.setTipoDispositivo(ord.getTipoDispositivo() != null ? ord.getTipoDispositivo()
                                                                 .getNome()
                                                                 .name()
                                                            : null);
    dml.setTipoRichiesta(ord.getTipo() != null ? ord.getTipo()
                                                    .name()
                                               : null);
    dml.setUuidDocumentList(ord.getUuidDocuments());

    Map<String, String> mapDatiAggiuntivi = convertDatiAggiuntiviRichiestaToMap(ord.getDatiAggiuntivi());
    dml.setDatiAggiuntivi(mapDatiAggiuntivi);
    return dml;
  }

  public RichiestaDMLDTO toEmptyRichiestaDMLDTO(String dmlUniqueIdentifier) {
    RichiestaDMLDTO dml = new RichiestaDMLDTO();
    dml.setDmlUniqueIdentifier(dmlUniqueIdentifier);
    dml.setDmlRevisionTimestamp(Instant.now());
    return dml;
  }

  private Map<String, String> convertDatiAggiuntiviRichiestaToMap(List<DatiAggiuntiviRichiesta> datiAggiuntiviRichiesta) {
    Map<String, String> mapDatiAggiuntivi = new HashMap<String, String>();
    if (datiAggiuntiviRichiesta != null && !datiAggiuntiviRichiesta.isEmpty())
      mapDatiAggiuntivi = datiAggiuntiviRichiesta.stream()
                                                 .collect(toMap(da -> da.getChiave().name(), DatiAggiuntiviRichiesta::getValore));
    return mapDatiAggiuntivi;
  }

}
