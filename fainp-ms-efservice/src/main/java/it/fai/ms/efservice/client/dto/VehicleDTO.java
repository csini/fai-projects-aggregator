package it.fai.ms.efservice.client.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * A DTO for the Vehicle entity.
 */
public class VehicleDTO implements Serializable {

  private Long id;

  private String alimentazione;

  private Float altezza;

  private Float altezzaPrimoAsse;

  private String categoria;

  private String classeFiltroAntiparticolato;

  private String colore;

  private Float diametroCerchiPneumatici;

  private String uuidDocumento;

  private String euro;

  private LocalDate immatricolazione;

  private Float larghezza;

  private Float lunghezza;

  private Integer massa;

  private Integer massaConvoglio;

  private String modello;

  private String nome;

  private String nazione;

  private Integer numeroAssi;

  private Integer numeroAssiPienoCarico;

  private String numeroTelaio;

  private Integer portata;

  private Boolean presenzaFiltroAntiparticolato;

  private Integer tara;

  @NotEmpty
  private String targa;

  private String tipo;

  private String tipoPneumaticoAsse;

  private Boolean serviziAttivi;

  private String stato;

  private Marca marca;

  private String identificativo;

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAlimentazione() {
    return alimentazione;
  }

  public void setAlimentazione(String alimentazione) {
    this.alimentazione = alimentazione;
  }

  public Float getAltezza() {
    return altezza;
  }

  public void setAltezza(Float altezza) {
    this.altezza = altezza;
  }

  public Float getAltezzaPrimoAsse() {
    return altezzaPrimoAsse;
  }

  public void setAltezzaPrimoAsse(Float altezzaPrimoAsse) {
    this.altezzaPrimoAsse = altezzaPrimoAsse;
  }

  public String getCategoria() {
    return categoria;
  }

  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }

  public String getClasseFiltroAntiparticolato() {
    return classeFiltroAntiparticolato;
  }

  public void setClasseFiltroAntiparticolato(String classeFiltroAntiparticolato) {
    this.classeFiltroAntiparticolato = classeFiltroAntiparticolato;
  }

  public String getColore() {
    return colore;
  }

  public void setColore(String colore) {
    this.colore = colore;
  }

  public Float getDiametroCerchiPneumatici() {
    return diametroCerchiPneumatici;
  }

  public void setDiametroCerchiPneumatici(Float diametroCerchiPneumatici) {
    this.diametroCerchiPneumatici = diametroCerchiPneumatici;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }

  public void setUuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
  }

  public String getEuro() {
    return euro;
  }

  public void setEuro(String euro) {
    this.euro = euro;
  }

  public LocalDate getImmatricolazione() {
    return immatricolazione;
  }

  public void setImmatricolazione(LocalDate immatricolazione) {
    this.immatricolazione = immatricolazione;
  }

  public Float getLarghezza() {
    return larghezza;
  }

  public void setLarghezza(Float larghezza) {
    this.larghezza = larghezza;
  }

  public Float getLunghezza() {
    return lunghezza;
  }

  public void setLunghezza(Float lunghezza) {
    this.lunghezza = lunghezza;
  }

  public Integer getMassa() {
    return massa;
  }

  public void setMassa(Integer massa) {
    this.massa = massa;
  }

  public Integer getMassaConvoglio() {
    return massaConvoglio;
  }

  public void setMassaConvoglio(Integer massaConvoglio) {
    this.massaConvoglio = massaConvoglio;
  }

  public String getModello() {
    return modello;
  }

  public void setModello(String modello) {
    this.modello = modello;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNazione() {
    return nazione;
  }

  public void setNazione(String nazione) {
    this.nazione = nazione;
  }

  public Integer getNumeroAssi() {
    return numeroAssi;
  }

  public void setNumeroAssi(Integer numeroAssi) {
    this.numeroAssi = numeroAssi;
  }

  public Integer getNumeroAssiPienoCarico() {
    return numeroAssiPienoCarico;
  }

  public void setNumeroAssiPienoCarico(Integer numeroAssiPienoCarico) {
    this.numeroAssiPienoCarico = numeroAssiPienoCarico;
  }

  public String getNumeroTelaio() {
    return numeroTelaio;
  }

  public void setNumeroTelaio(String numeroTelaio) {
    this.numeroTelaio = numeroTelaio;
  }

  public Integer getPortata() {
    return portata;
  }

  public void setPortata(Integer portata) {
    this.portata = portata;
  }

  public Boolean isPresenzaFiltroAntiparticolato() {
    return presenzaFiltroAntiparticolato;
  }

  public void setPresenzaFiltroAntiparticolato(Boolean presenzaFiltroAntiparticolato) {
    this.presenzaFiltroAntiparticolato = presenzaFiltroAntiparticolato;
  }

  public Integer getTara() {
    return tara;
  }

  public void setTara(Integer tara) {
    this.tara = tara;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getTipoPneumaticoAsse() {
    return tipoPneumaticoAsse;
  }

  public void setTipoPneumaticoAsse(String tipoPneumaticoAsse) {
    this.tipoPneumaticoAsse = tipoPneumaticoAsse;
  }

  public Boolean isServiziAttivi() {
    return serviziAttivi;
  }

  public void setServiziAttivi(Boolean serviziAttivi) {
    this.serviziAttivi = serviziAttivi;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public Marca getMarca() {
    return marca;
  }

  public void setMarca(Marca marca) {
    this.marca = marca;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    VehicleDTO vehicleDTO = (VehicleDTO) o;
    if (vehicleDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), vehicleDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "VehicleDTO{" + "id=" + getId() + ", alimentazione='" + getAlimentazione() + "'" + ", altezza='" + getAltezza() + "'"
           + ", altezzaPrimoAsse='" + getAltezzaPrimoAsse() + "'" + ", categoria='" + getCategoria() + "'"
           + ", classeFiltroAntiparticolato='" + getClasseFiltroAntiparticolato() + "'" + ", colore='" + getColore() + "'"
           + ", diametroCerchiPneumatici='" + getDiametroCerchiPneumatici() + "'" + ", uuidDocumento='" + getUuidDocumento() + "'"
           + ", euro='" + getEuro() + "'" + ", immatricolazione='" + getImmatricolazione() + "'" + ", larghezza='" + getLarghezza() + "'"
           + ", lunghezza='" + getLunghezza() + "'" + ", massa='" + getMassa() + "'" + ", massaConvoglio='" + getMassaConvoglio() + "'"
           + ", modello='" + getModello() + "'" + ", nome='" + getNome() + "'" + ", nazione='" + getNazione() + "'" + ", numeroAssi='"
           + getNumeroAssi() + "'" + ", numeroAssiPienoCarico='" + getNumeroAssiPienoCarico() + "'" + ", numeroTelaio='" + getNumeroTelaio()
           + "'" + ", portata='" + getPortata() + "'" + ", presenzaFiltroAntiparticolato='" + isPresenzaFiltroAntiparticolato() + "'"
           + ", tara='" + getTara() + "'" + ", targa='" + getTarga() + "'" + ", tipo='" + getTipo() + "'" + ", tipoPneumaticoAsse='"
           + getTipoPneumaticoAsse() + "'" + ", serviziAttivi='" + isServiziAttivi() + "'" + ", stato='" + getStato() + "'" + "}";
  }
}
