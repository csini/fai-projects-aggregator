package it.fai.ms.efservice.service.fsm;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmDispositivoCacheService;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;

@Service
@Transactional
public class FsmResetService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmRichiestaCacheService fsmRichiestaCacheService;

  private final FsmDispositivoCacheService fsmDispositivoCacheService;

  private final RichiestaRepositoryExt richiestaRepo;

  private final DispositivoRepositoryExt dispositivoRepo;

  private final List<StatoRichiesta> statiFsmAccettazione = Collections.unmodifiableList(StatoRichiestaUtil.getStateOfAccettazione()
                                                                                                           .stream()
                                                                                                           .filter(s -> s != StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                                                                                           .collect(toList()));

  public FsmResetService(final FsmRichiestaCacheService _fsmRichiestaCacheService,
                         final FsmDispositivoCacheService _fsmDispositivoCacheService, final RichiestaRepositoryExt _richiestaRepo,
                         final DispositivoRepositoryExt _dispositivoRepo) {
    fsmRichiestaCacheService = _fsmRichiestaCacheService;
    fsmDispositivoCacheService = _fsmDispositivoCacheService;
    richiestaRepo = _richiestaRepo;
    dispositivoRepo = _dispositivoRepo;
  }

  public String resetStateFsm(String identificativo, StatoRichiesta stato) {
    String newContextFsm = null;
    Richiesta richiesta = richiestaRepo.findOneByIdentificativo(identificativo);
    log.info("Reset stato of Richiesta: {} to : {}", richiesta, stato);
    Long id = richiesta.getId();
    String idRichiesta = String.valueOf(id);
    if (statiFsmAccettazione.contains(stato)) {
      newContextFsm = fsmRichiestaCacheService.resetStateAndMachineId(idRichiesta, stato, FsmType.ACCETTAZIONE.fsmName());
    } else {
      newContextFsm = fsmRichiestaCacheService.resetStateFsm(idRichiesta, stato);
    }

    if (StringUtils.isNotBlank(newContextFsm)) {
      richiesta = richiestaRepo.save(richiesta.stato(stato));
      log.info("Reset state on entity: {}", richiesta);
    }

    return newContextFsm;
  }
  
  public String resetStateFsmDispositivo(final String identificativo, final StatoDispositivo stato) {
    String newContextFsm = null;
    Dispositivo device = dispositivoRepo.findOneByIdentificativo(identificativo);
    log.info("Reset stato of Device: {} to : {}", device, stato);
    Long id = device.getId();
    String idDevice = String.valueOf(id);
    newContextFsm = fsmDispositivoCacheService.resetStateFsm(idDevice, stato);

    if (StringUtils.isNotBlank(newContextFsm)) {
      device = dispositivoRepo.save(device.stato(stato));
      log.info("Reset state on entity: {}", device);
    }

    return newContextFsm;
  }

  public String getState(final String identificativo) {
    String state = null;
    Richiesta richiesta = richiestaRepo.findOneByIdentificativo(identificativo);
    if (richiesta != null) {
      log.info("Richiesta : {}", richiesta);
      Long id = richiesta.getId();
      String idRichiesta = String.valueOf(id);
      state = fsmRichiestaCacheService.getStateMachineContext(idRichiesta);
      log.info("State FSM: {}", state);
    }
    return state;
  }
  
  public String getStateFsmDevice(final String identificativo) {
    String state = null;
    Dispositivo device = dispositivoRepo.findOneByIdentificativo(identificativo);
    if (device != null) {
      log.info("Device : {}", device);
      Long id = device.getId();
      String idDevice = String.valueOf(id);
      state = fsmDispositivoCacheService.getStateMachineContext(idDevice);
      log.info("State FSM Device: {}", state);
    }
    return state;
  }

  public void putInCacheFsm(Richiesta richiesta, StateMachine<StatoRichiesta, RichiestaEvent> stateMachine) {
    stateMachine.start();
    fsmRichiestaCacheService.persist(stateMachine, String.valueOf(richiesta.getId()));
  }

}
