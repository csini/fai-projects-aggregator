package it.fai.ms.efservice.repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.domain.TipoDispositivo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the RaggruppamentoRichiesteOrdineFornitore entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaggruppamentoRichiesteOrdineFornitoreRepositoryExt extends JpaRepository<RaggruppamentoRichiesteOrdineFornitore, Long>, JpaSpecificationExecutor<RaggruppamentoRichiesteOrdineFornitore> {
  List<RaggruppamentoRichiesteOrdineFornitore> findAllByTipoDispositivo_Nome(TipoDispositivoEnum tipoDispositivo);

  //findByNomeFileRicevutoAndDataGenerazioneFileAndTipoDispositivo sembra abbia problemi con l'Instant: vedi DeviceResponseOkConsumer.saveRaggruppamentoRichiesteFornitore
//    @Query("SELECT rg FROM RaggruppamentoRichiesteOrdineFornitore rg WHERE rg.nomeFileRicevuto = :fileName AND rg.dataGenerazioneFile = :data AND rg.tipoDispositivo = :deviceType")
//    Optional<RaggruppamentoRichiesteOrdineFornitore> findByNomeFileRicevutoAndDataGenerazioneFileAndTipoDispositivo(@Param("fileName") String nomeFileRicevuto,
//                                                                                                                  @Param("data") Instant dataGenerazioneFile,
//                                                                                                                  @Param("deviceType") TipoDispositivo tipoDispositivo);

// Prova non a buon fine...
//  @Query(value = "SELECT rg.* FROM faiefservice.dbo.group_richieste_ordini rg WHERE rg.nome_file_ricevuto = :fileName AND format(rg.data_generazione_file, 'yyyy-MM-dd HH:mm:ss.fff') = :data AND rg.tipo_dispositivo_id = :deviceType", nativeQuery = true)
//  Optional<RaggruppamentoRichiesteOrdineFornitore> findByNomeFileRicevutoAndDataGenerazioneFileAndTipoDispositivo(@Param("fileName") String nomeFileRicevuto,
//                                                                                                                  @Param("data") String dataGenerazioneFileString,
//                                                                                                                  @Param("deviceType") Long tipoDispositivoId);

  @Query("SELECT rg FROM RaggruppamentoRichiesteOrdineFornitore rg WHERE rg.nomeFileRicevuto = :fileName AND rg.tipoDispositivo = :deviceType")
  List<RaggruppamentoRichiesteOrdineFornitore> findByNomeFileRicevutoAndTipoDispositivo(@Param("fileName") String nomeFileRicevuto,
                                                                                            @Param("deviceType") TipoDispositivo tipoDispositivo);
  List<RaggruppamentoRichiesteOrdineFornitore> findAllByIdIn(List<Long> ids);

}
