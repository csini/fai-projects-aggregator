package it.fai.ms.efservice.listener;

import java.util.UUID;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.jms.dml.DispositivoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;
import it.fai.ms.efservice.util.BeanUtil;

public class DispositivoEntityListener {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  // no-injection please
  DispositivoDmlSenderUtil dmlService;

  private DispositivoServiceExt deviceServiceExt;

  @PrePersist
  public void setIdentificativo(final Dispositivo dispositivo) {

    String uuidIdentificativo = UUID.randomUUID()
                                    .toString();

    if (log.isInfoEnabled()) {
      log.info("Set identificativo: {} for DISPOSITIVO: {}", uuidIdentificativo, dispositivo);
    }

    dispositivo.setIdentificativo(uuidIdentificativo);
  }

  @PostPersist
  @PostUpdate
  public void sendSaveNotificationDML(Dispositivo dispositivo) {
    if (dispositivo.getIdentificativo() == null || dispositivo.getIdentificativo()
                                                              .isEmpty()) {
      log.warn("Identificativo dispositivo is null: {}", dispositivo);
      return;
    }

    log.info("Post Persist Dispositivo: {}", dispositivo);
    if (isDeactiveStateDevice(dispositivo)) {
      if (deviceServiceExt == null) {
        deviceServiceExt = BeanUtil.getBean(DispositivoServiceExt.class);
      }
      log.debug("Change status on Services related on device: [{} - {}]", dispositivo.getIdentificativo(), dispositivo.getStato());
      deviceServiceExt.changeStatoServiceToNotActiveByDevice(dispositivo);
    }
    getDmlService().sendSaveNotification(dispositivo);
  }

  private DispositivoDmlSenderUtil getDmlService() {
    if (dmlService == null) {
      dmlService = new DispositivoDmlSenderUtil(BeanUtil.getBean(JmsProperties.class), BeanUtil.getBean(DispositivoJmsMapper.class));
    }
    return dmlService;
  }

  @PostRemove
  public void sendDeleteNotificationDML(Dispositivo dispositivo) {
    if (dispositivo.getIdentificativo() == null || dispositivo.getIdentificativo()
                                                              .isEmpty()) {
      log.warn("Identificativo dispositivo is null: {}", dispositivo);
      return;
    }

    getDmlService().sendDeleteNotification(dispositivo.getIdentificativo());
  }

  private boolean isDeactiveStateDevice(Dispositivo device) {
    return (device != null && device.getStato() != null && StatoDispositivo.getNotActiveStates()
                                                                           .contains(device.getStato())) ? true : false;
  }

}
