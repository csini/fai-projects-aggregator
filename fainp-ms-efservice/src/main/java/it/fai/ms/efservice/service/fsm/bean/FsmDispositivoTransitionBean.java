/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

/**
 * @author Luca Vassallo
 */
@WithStateMachine(id = "fsmDispositivo")
public class FsmDispositivoTransitionBean extends AbstractFsmDispositivoTransition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @StatesDispositivoOnTransition(source = StatoDispositivo.INIZIALE, target = StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE)
  public void switchFromPortaleToVerifica(StateContext<StatoDispositivo, DispositivoEvent> stateContext) {
    // Add implementation if necessary;
  }

}
