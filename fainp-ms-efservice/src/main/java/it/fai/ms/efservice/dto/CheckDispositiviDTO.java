package it.fai.ms.efservice.dto;

import java.io.Serializable;
import java.util.Map;

import it.fai.common.enumeration.TipoDispositivoEnum;

public class CheckDispositiviDTO implements Serializable {

  private static final long serialVersionUID = 5245483802032764952L;

  private Map<String, String> veicoli;

  private String codiceCliente;

  private TipoDispositivoEnum tipoDispositivo;

  public Map<String, String> getVeicoli() {
    return veicoli;
  }

  public void setVeicoli(Map<String, String> veicoli) {
    this.veicoli = veicoli;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codiceCliente == null) ? 0 : codiceCliente.hashCode());
    result = prime * result + ((tipoDispositivo == null) ? 0 : tipoDispositivo.hashCode());
    result = prime * result + ((veicoli == null) ? 0 : veicoli.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CheckDispositiviDTO other = (CheckDispositiviDTO) obj;
    if (codiceCliente == null) {
      if (other.codiceCliente != null)
        return false;
    } else if (!codiceCliente.equals(other.codiceCliente))
      return false;
    if (tipoDispositivo != other.tipoDispositivo)
      return false;
    if (veicoli == null) {
      if (other.veicoli != null)
        return false;
    } else if (!veicoli.equals(other.veicoli))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "CheckDispositiviDTO [veicoli=" + veicoli + ", codiceCliente=" + codiceCliente + ", tipoDispositivo=" + tipoDispositivo + "]";
  }

}
