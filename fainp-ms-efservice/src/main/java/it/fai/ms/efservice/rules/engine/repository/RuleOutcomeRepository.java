package it.fai.ms.efservice.rules.engine.repository;

import java.util.Optional;
import java.util.Set;

import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;

public interface RuleOutcomeRepository {

  Integer count();

  void deleteAll();

  Optional<RuleOutcome> find(RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId);

  Optional<RuleOutcome> find(RuleEngineServiceTypeId serviceTypeId, RuleEngineDeviceTypeId deviceTypeId, RuleEngineVehicleId vehicleId);

  Optional<RuleOutcome> find(RuleEngineVehicleId vehicleId);

  RuleOutcomeEntity save(RuleContext ruleContext, RuleOutcome ruleOutcome);

}
