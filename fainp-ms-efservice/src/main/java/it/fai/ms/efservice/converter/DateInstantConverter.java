package it.fai.ms.efservice.converter;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import javax.persistence.AttributeConverter;

public class DateInstantConverter implements AttributeConverter<Instant, Date> {

  @Override
  public Date convertToDatabaseColumn(final Instant _instant) {
    return Optional.ofNullable(_instant)
                   .map(instant -> Date.from(_instant))
                   .orElse(null);
  }

  @Override
  public Instant convertToEntityAttribute(final Date _date) {
    return Optional.ofNullable(_date)
                   .map(date -> _date.toInstant())
                   .orElse(null);
  }

}
