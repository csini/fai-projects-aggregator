package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_TelepassITA implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;

  public RuleEngine_Device_TelepassITA(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome,
                                       final RuleEngineServiceType _serviceTypeRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
    serviceTypeRuleContext = _serviceTypeRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("devType", deviceTypeRuleContext);
    context.put("serType", serviceTypeRuleContext);
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean careac = (serType.id.id == 'AREA_C');");
    rules.add("boolean clength = ((vehicle.type == 'AUTOBUS') || (vehicle.type != 'AUTOBUS' && vehicle.length <= 7.5));");
    rules.add("if (careac && !clength) return 'typelength';");
    rules.add("boolean cdiesel1 = (vehicle.fuelType == 'DIESEL' && vehicle.euroVehicle < 4);");
    rules.add("if (careac && cdiesel1) return 'dieseleuro';");
    rules.add("boolean cdiesel2 = (vehicle.fuelType == 'DIESEL' && (vehicle.euroVehicle == 4)  && vehicle.dieselParticulateFilter == null);");
    rules.add("if (careac && cdiesel2) return 'dieselfap';");
    rules.add("boolean cbenzina = (vehicle.fuelType == 'BENZINA' && vehicle.euroVehicle < 1);");
    rules.add("if (careac && cbenzina) return 'benzina';");
    rules.add("boolean cpremium = (serType.id.id == 'TESSERA_PREMIUM');");
    rules.add("boolean cpremiumtruck = (serType.id.id == 'TESSERA_PREMIUM_TRUCK');");
    rules.add("if (!cpremium && !cpremiumtruck) return '';");
    rules.add("boolean taranotnull = (vehicle.weight != null && vehicle.weight.loneVehicleTareWeight != null);");
    rules.add("boolean grossnotnull = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight != null);");
    rules.add("boolean trainsetnotnull = (vehicle.weight != null && vehicle.weight.trainsetGrossWeight != null);");
    rules.add("if ((cpremium || cpremiumtruck) && !taranotnull && !grossnotnull && !trainsetnotnull) return 'weightnotset';");
    rules.add("boolean taragt3500 = (vehicle.weight != null && vehicle.weight.loneVehicleTareWeight > 3500);");
    rules.add("if (cpremiumtruck && taranotnull && taragt3500) return '';");
    rules.add("if (cpremium && taranotnull && !taragt3500) return '';");
    rules.add("if (cpremiumtruck && taranotnull && !taragt3500) return 'taratruck';");
    rules.add("if (cpremium && taranotnull && taragt3500) return 'tarapremium';");
    rules.add("boolean grossgt3500 = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight > 3500);");
    rules.add("if (cpremiumtruck && grossnotnull && grossgt3500) return '';");
    rules.add("if (cpremium && grossnotnull && !grossgt3500) return '';");
    rules.add("if (cpremiumtruck && grossnotnull && !grossgt3500) return 'grosstruck';");
    rules.add("if (cpremium && grossnotnull && grossgt3500) return 'grosspremium';");
    rules.add("boolean trainsetgt3500 = (vehicle.weight != null && vehicle.weight.trainsetGrossWeight > 3500);");
    rules.add("if (cpremiumtruck && !trainsetgt3500) return 'trainsettruck';");
    rules.add("if (cpremium && trainsetgt3500) return 'trainsetpremium';");
    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);

    String res = null;
    switch (preconditionCode) {
    case TYPE_LENGTH:
      res = "Vehicle type not AUTOBUS length must be <= 7,5mt";
      break;
    case DIESEL_EURO:
      res = "Vehicle diesel euro must be > 3";
      break;
    case DIESEL_FAP:
      res = "Vehicle diesel euro 4 must have FAP";
      break;
    case BENZINA:
      res = "Vehicle benzina euro must be >= 1";
      break;
    case WEIGHT_NOT_SET:
      res = "At least one value between Vehicle tare, gross and trainset weight must be set";
      break;
    case TARA_PREMIUM_TRUCK:
      res = "Vehicle tare weight must be > 3500 Kg";
      break;
    case TARA_PREMIUM:
      res = "Vehicle tare weight must be <= 3500 Kg";
      break;
    case GROSS_PREMIUM_TRUCK:
      res = "Vehicle gross weight must be > 3500 Kg";
      break;
    case GROSS_PREMIUM:
      res = "Vehicle gross weight must be <= 3500 Kg";
      break;
    case TRAINSIENT_PREMIUM_TRUCK:
      res = "Vehicle trainset weight must be > 3500 Kg";
      break;
    case TRAINSIENT_PREMIUM:
      res = "Vehicle trainset weight must be <= 3500 Kg";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
