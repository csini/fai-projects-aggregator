/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.statemachine.annotation.OnTransition;

import it.fai.common.enumeration.StatoDispositivo;

/**
 * @author Luca Vassallo
 */
@Target(value = { ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@OnTransition
public @interface StatesDispositivoOnTransition {
  StatoDispositivo[] source() default {};

  StatoDispositivo[] target() default {};
}
