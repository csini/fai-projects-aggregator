package it.fai.ms.efservice.service.fsm.type.viacard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.viacard.FsmModificaViaCardRientroMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmRichiestaGeneric;

@Service(value = FsmModificaViaCardRientroMalfunzionamento.FSM_MOD_VIACARD_RIENTRO_MALF)
public class FsmModificaViaCardRientroMalfunzionamento extends FsmRichiestaGeneric {

  public static final String FSM_MOD_VIACARD_RIENTRO_MALF = "fsmModificaViaCardRientroMalfunzionamento";

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public FsmModificaViaCardRientroMalfunzionamento(@Qualifier(FsmModificaViaCardRientroMalfunzionamentoConfig.MOD_VIACARD_RIENTRO_MALF) StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                                   FsmRichiestaCacheService _cacheService) {
    stateMachineFactory = _stateMachineFactory;
    cacheService = _cacheService;
    fsmType = FsmType.MOD_VC_RIENTRO_MALFUNZIONAMENTO;
    deviceTypes = new TipoDispositivoEnum[] { TipoDispositivoEnum.VIACARD };
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected FsmType getOldFsm() {
    return null;
  }

}
