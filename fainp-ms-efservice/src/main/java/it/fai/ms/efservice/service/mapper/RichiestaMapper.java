package it.fai.ms.efservice.service.mapper;

import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.service.dto.RichiestaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Richiesta and its DTO RichiestaDTO.
 */
@Mapper(componentModel = "spring", uses = {ContrattoMapper.class, DispositivoMapper.class, OrdineClienteMapper.class, TipoDispositivoMapper.class, })
public interface RichiestaMapper extends EntityMapper <RichiestaDTO, Richiesta> {

    @Mapping(source = "contratto.id", target = "contrattoId")
    @Mapping(source = "contratto.codContrattoCliente", target = "contrattoCodContrattoCliente")

    @Mapping(source = "ordineCliente.id", target = "ordineClienteId")

    @Mapping(source = "tipoDispositivo.id", target = "tipoDispositivoId")
    @Mapping(source = "tipoDispositivo.nome", target = "tipoDispositivoNome")
    RichiestaDTO toDto(Richiesta richiesta); 

    @Mapping(source = "contrattoId", target = "contratto")

    @Mapping(source = "ordineClienteId", target = "ordineCliente")

    @Mapping(source = "tipoDispositivoId", target = "tipoDispositivo")
    Richiesta toEntity(RichiestaDTO richiestaDTO); 
    default Richiesta fromId(Long id) {
        if (id == null) {
            return null;
        }
        Richiesta richiesta = new Richiesta();
        richiesta.setId(id);
        return richiesta;
    }
}
