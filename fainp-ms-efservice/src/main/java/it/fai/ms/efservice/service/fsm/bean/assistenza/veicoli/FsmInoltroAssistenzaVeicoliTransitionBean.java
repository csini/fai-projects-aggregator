package it.fai.ms.efservice.service.fsm.bean.assistenza.veicoli;

import org.springframework.statemachine.annotation.WithStateMachine;
import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.assistenza.veicoli.FsmInoltroAssistenzaVeicoli;


@WithStateMachine(id = FsmInoltroAssistenzaVeicoli.FSM_INOLTRO_ASSISTENZA_VEICOLI)
public class FsmInoltroAssistenzaVeicoliTransitionBean extends AbstractFsmRichiestaTransition {
}
