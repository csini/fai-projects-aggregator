package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_TollCollect implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType  deviceTypeRuleContext;
  private RuleEngineVehicle     vehicleRuleContext;
  private RuleEngineServiceType serviceTypeRuleContext;

  public RuleEngine_Device_TollCollect(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome,
                                       final RuleEngineServiceType _serviceTypeRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
    serviceTypeRuleContext = _serviceTypeRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("devType", deviceTypeRuleContext);
    context.put("serType", serviceTypeRuleContext);
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean grossnotnull = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight != null);");
    rules.add("if (!grossnotnull) return 'grossnotset';");
    
    rules.add("boolean grossgt7500 = (vehicle.weight != null && vehicle.weight.loneVehicleGrossWeight >= 7500);");
    rules.add("if (grossnotnull && !grossgt7500) return 'grosstollcollect7500';");
    
    rules.add("boolean axes = (vehicle.axes != null);");
    rules.add("if (!axes) return 'axesnotset';");

    rules.add("boolean numberChassis = (vehicle.numberChassis != null);");
    rules.add("if (!numberChassis) return 'numberChassis';");
    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);
    String res = null;
    switch (preconditionCode) {
    case GROSS_NOT_SET:
      res = "Gross weight must be set";
      break;
    case GROSS_7500_TOLL_COLLECT:
      res = "Vehicle gross weight is less than 7500";
      break;
    case NUMBER_CHASSIS_EMPTY:
      res = "Vehicle must be set Number Chassis";
      break;
    case AXES_NOT_SET:
      res = "axes must be present";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
