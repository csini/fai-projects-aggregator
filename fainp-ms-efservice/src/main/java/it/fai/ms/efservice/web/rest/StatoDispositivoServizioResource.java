package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.StatoDispositivoServizioService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing StatoDispositivoServizio.
 */
@RestController
@RequestMapping("/api")
public class StatoDispositivoServizioResource {

    private final Logger log = LoggerFactory.getLogger(StatoDispositivoServizioResource.class);

    private static final String ENTITY_NAME = "statoDispositivoServizio";

    private final StatoDispositivoServizioService statoDispositivoServizioService;

    public StatoDispositivoServizioResource(StatoDispositivoServizioService statoDispositivoServizioService) {
        this.statoDispositivoServizioService = statoDispositivoServizioService;
    }

    /**
     * POST  /stato-dispositivo-servizios : Create a new statoDispositivoServizio.
     *
     * @param statoDispositivoServizioDTO the statoDispositivoServizioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new statoDispositivoServizioDTO, or with status 400 (Bad Request) if the statoDispositivoServizio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stato-dispositivo-servizios")
    @Timed
    public ResponseEntity<StatoDispositivoServizioDTO> createStatoDispositivoServizio(@RequestBody StatoDispositivoServizioDTO statoDispositivoServizioDTO) throws URISyntaxException {
        log.debug("REST request to save StatoDispositivoServizio : {}", statoDispositivoServizioDTO);
        if (statoDispositivoServizioDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new statoDispositivoServizio cannot already have an ID")).body(null);
        }
        StatoDispositivoServizioDTO result = statoDispositivoServizioService.save(statoDispositivoServizioDTO);
        return ResponseEntity.created(new URI("/api/stato-dispositivo-servizios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /stato-dispositivo-servizios : Updates an existing statoDispositivoServizio.
     *
     * @param statoDispositivoServizioDTO the statoDispositivoServizioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated statoDispositivoServizioDTO,
     * or with status 400 (Bad Request) if the statoDispositivoServizioDTO is not valid,
     * or with status 500 (Internal Server Error) if the statoDispositivoServizioDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stato-dispositivo-servizios")
    @Timed
    public ResponseEntity<StatoDispositivoServizioDTO> updateStatoDispositivoServizio(@RequestBody StatoDispositivoServizioDTO statoDispositivoServizioDTO) throws URISyntaxException {
        log.debug("REST request to update StatoDispositivoServizio : {}", statoDispositivoServizioDTO);
        if (statoDispositivoServizioDTO.getId() == null) {
            return createStatoDispositivoServizio(statoDispositivoServizioDTO);
        }
        StatoDispositivoServizioDTO result = statoDispositivoServizioService.save(statoDispositivoServizioDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, statoDispositivoServizioDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /stato-dispositivo-servizios : get all the statoDispositivoServizios.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of statoDispositivoServizios in body
     */
    @GetMapping("/stato-dispositivo-servizios")
    @Timed
    public List<StatoDispositivoServizioDTO> getAllStatoDispositivoServizios() {
        log.debug("REST request to get all StatoDispositivoServizios");
        return statoDispositivoServizioService.findAll();
        }

    /**
     * GET  /stato-dispositivo-servizios/:id : get the "id" statoDispositivoServizio.
     *
     * @param id the id of the statoDispositivoServizioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the statoDispositivoServizioDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stato-dispositivo-servizios/{id}")
    @Timed
    public ResponseEntity<StatoDispositivoServizioDTO> getStatoDispositivoServizio(@PathVariable Long id) {
        log.debug("REST request to get StatoDispositivoServizio : {}", id);
        StatoDispositivoServizioDTO statoDispositivoServizioDTO = statoDispositivoServizioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(statoDispositivoServizioDTO));
    }

    /**
     * DELETE  /stato-dispositivo-servizios/:id : delete the "id" statoDispositivoServizio.
     *
     * @param id the id of the statoDispositivoServizioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stato-dispositivo-servizios/{id}")
    @Timed
    public ResponseEntity<Void> deleteStatoDispositivoServizio(@PathVariable Long id) {
        log.debug("REST request to delete StatoDispositivoServizio : {}", id);
        statoDispositivoServizioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
