package it.fai.ms.efservice.service.frejus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.frejus.ConfirmTesseraDto;
import it.fai.ms.efservice.client.FrejusClient;
import it.fai.ms.efservice.dto.ResponseTessereFrejusDto;
import it.fai.ms.efservice.service.dto.ConfermaTransitoDTO;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.consumer.DeviceNotOrderableConsumer;
import it.fai.ms.efservice.service.jms.consumer.DeviceOrderedConsumer;

@Service
@Transactional
public class ConfermaTesseraFrejus {
  private static final Logger   log = LoggerFactory.getLogger(ConfermaTesseraFrejus.class);
  private DeviceOrderedConsumer ordered;

  private DeviceNotOrderableConsumer notOrderable;

  private FrejusClient frejusClient;
  private String       securityToken;

  public ConfermaTesseraFrejus(DeviceOrderedConsumer okConsumer, DeviceNotOrderableConsumer koConsumer, FrejusClient frejusClient,
                               @Value("${efservice.authorizationHeader}") String _securityToken) {
    this.ordered = okConsumer;
    this.notOrderable = koConsumer;
    this.frejusClient = frejusClient;
    securityToken = _securityToken;
  }

  public List<String> processConferme(ConfermaTransitoDTO dto) {
    List<String> serialDevices = new ArrayList<String>();
    try {
      List<ConfirmTesseraDto> list = frejusClient.processFile(securityToken, dto.getUuidDocumento(), dto.getPaese());
      for (ConfirmTesseraDto message : list) {
        ordered.consume(message.toConfirmMessage());
        serialDevices.add(message.getDeviceSerial());
      }
    } catch (IOException e) {
      log.error("errore in conferma tessere Frejus", e);
      throw new RuntimeException("errore in conferma tessere Frejus", e);
    } catch (FsmExecuteCommandException e) {
      log.error("Exception on consume message: ", e);
      throw new RuntimeException("Exception on consume message: ", e);
    }
    
    return serialDevices;
  }

  public String processConferma(ResponseTessereFrejusDto data,String paese) {
    ConfirmTesseraDto message = frejusClient.processTransito(securityToken,data,paese);
    try {
      ordered.consume(message.toConfirmMessage());
    } catch (FsmExecuteCommandException e) {
      log.error("Exception on consumer message Tessere Frejus:", e);
      throw new RuntimeException(e);
    }
    return message.getDeviceSerial();
  }



}
