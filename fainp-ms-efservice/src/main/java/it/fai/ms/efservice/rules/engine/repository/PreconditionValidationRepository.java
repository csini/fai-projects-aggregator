package it.fai.ms.efservice.rules.engine.repository;

import java.time.Instant;
import java.util.Set;

import it.fai.ms.efservice.dto.TimestampPreconditionDTO;

public interface PreconditionValidationRepository {

  void clearCache();

  TimestampPreconditionDTO get(String key);

  void put(String key, TimestampPreconditionDTO value);

  boolean isPreconditionValid(String uuidVehicle, Instant timestamp);

  void put(String key, Instant dataUltimaModifica);

}
