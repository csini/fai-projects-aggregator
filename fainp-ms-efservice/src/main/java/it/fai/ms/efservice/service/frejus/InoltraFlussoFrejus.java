package it.fai.ms.efservice.service.frejus;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.frejus.RichiestaCartaFrejusMessage;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import it.fai.ms.efservice.service.jms.producer.device.frejus.FrejusRichiestaTesseraProducer;

@Service
@Transactional
public class InoltraFlussoFrejus {

	private final Logger log = LoggerFactory.getLogger(ContrattiFrejusService.class);

	private final VehicleWizardCacheServiceExt vehicleCache;
	private final FrejusRichiestaTesseraProducer jmsProducer;

	public InoltraFlussoFrejus(VehicleWizardCacheServiceExt vehicleCache, FrejusRichiestaTesseraProducer jmsProducer) {
		super();
		this.vehicleCache = vehicleCache;
    this.jmsProducer = jmsProducer;

	}

	public void inoltra(Richiesta richiesta) {
		log.info("Invio Messaggio JMS Frejus {}", richiesta);
		try {
			RichiestaCartaFrejusMessage rfi = new RichiestaCartaFrejusMessage();
			rfi.setOrderRequestUuid(new OrderRequestUuid(richiesta.getIdentificativo()));
			rfi.setContractUuid(new ContractOrderRequest(richiesta.getContratto().getCodContrattoCliente(),
					richiesta.getContratto().getClienteFai() != null
							? richiesta.getContratto().getClienteFai().getCodiceCliente()
							: null));
			rfi.setNomeCliente(richiesta.getOrdineCliente().getClienteAssegnatario().getNomeIntestazioneTessere());
			rfi.setTarga(richiesta.getAssociazione());
			rfi.setNazioneTarga(richiesta.getCountry());
			rfi.setClasseEuro(getClasseEuro(richiesta));
			rfi.setPaeseRiferimentoIva(richiesta.getContratto().getPaeseRiferimentoIva());
			jmsProducer.sendMessage(rfi);
		} catch (RuntimeException e) {
			log.error("error in send message", e);
			throw e;
		}
	}

	private String getClasseEuro(Richiesta richiesta) {

		Optional<Dispositivo> disp = richiesta.getDispositivos().stream().findFirst();

		Optional<String> classeEuro = disp.flatMap(d -> d.getAssociazioneDispositivoVeicolos().stream()
				.filter(adv -> isSameVehicle(richiesta, adv.getUuidVeicolo())).findFirst()
				.flatMap(adv -> Optional.ofNullable(vehicleCache.getClasseEuroByUuidVehicle(adv.getUuidVeicolo()))));

		return classeEuro.orElse(null);
	}

	private boolean isSameVehicle(Richiesta richiesta, String uuidVeicolo) {
		return richiesta.getAssociazione().equals(vehicleCache.getTargaByUuidVehicle(uuidVeicolo))
				&& richiesta.getCountry().equals(vehicleCache.getNazioneByUuidVehicle(uuidVeicolo));
	}
}
