package it.fai.ms.efservice.service.jms.listener.device;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceNotAssociableConsumer;

@Service
@Transactional
public class JmsListenerDeviceNotAssociable
  extends JmsObjectMessageListenerTemplate<DeviceEventMessage>
  implements JmsQueueListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceNotAssociableConsumer consumer;

  @Autowired
  public JmsListenerDeviceNotAssociable(DeviceNotAssociableConsumer _consumer){
    consumer = _consumer;
  }

  @Override
  protected void consumeMessage(DeviceEventMessage deviceEventMessage) {
    _log.info("Received jms message {}", deviceEventMessage);
    consumer.consume(deviceEventMessage);
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_NOT_ASSOCIABLE;
  }
}
