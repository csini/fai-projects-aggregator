package it.fai.ms.efservice.web.rest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

/**
 * REST controller for managing ClienteFai.
 */
@RestController
@RequestMapping(ClienteFaiResourceExt.BASE_PATH)
public class ClienteFaiResourceExt {

  private final Logger log = LoggerFactory.getLogger(ClienteFaiResourceExt.class);

  public static final String BASE_PATH = "/api/public";

  private static final String ENTITY_NAME = "clienteFai";

  private static final String CLIENTE_FAI_BY_CODICE = "/clientefai";

  private static final String CLIENTE_FAI_BY_CODICE_FATTURAZIONE = "/clientefai/fatturazione";

  private final ClienteFaiServiceExt clienteFaiServiceExt;

  public ClienteFaiResourceExt(ClienteFaiServiceExt clienteFaiServiceExt) {
    this.clienteFaiServiceExt = clienteFaiServiceExt;
  }

  @GetMapping(CLIENTE_FAI_BY_CODICE + "/{codiceCliente}")
  @Timed
  public ResponseEntity<ClienteFaiDTO> getClienteFai(@PathVariable("codiceCliente") String codiceCliente) throws CustomException {
    log.debug("REST request to get ClienteFai by codice cliente: {}", codiceCliente);

    if (StringUtils.isBlank(codiceCliente)) {
      log.error("The cliente code of Cliente FAI requested is BLANK");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    ClienteFaiDTO clienteFaiDTO = clienteFaiServiceExt.findDTOByCodiceCliente(codiceCliente);

    if (clienteFaiDTO == null) {
      log.error("Not found Cliente Fai with cliente code {}", codiceCliente);
      throw CustomException.builder(HttpStatus.NOT_FOUND)
                           .add(Errno.NOT_FOUND_CLIENTE_FAI);
    }

    return ResponseEntity.ok(clienteFaiDTO);
  }

  @GetMapping(CLIENTE_FAI_BY_CODICE_FATTURAZIONE + "/{codiceClienteFatturazione}")
  @Timed
  public ResponseEntity<ClienteFaiDTO> getClienteFaiByCodiceFatturazione(@PathVariable("codiceClienteFatturazione") String codiceClienteFatturazione) throws CustomException {
    log.debug("REST request to get ClienteFai by codice cliente fatturazione: {}", codiceClienteFatturazione);

    if (StringUtils.isBlank(codiceClienteFatturazione)) {
      log.error("The cliente code of {} requested is BLANK", ENTITY_NAME);
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    ClienteFaiDTO clienteFaiDTO = clienteFaiServiceExt.findDTOByCodiceClienteFatturazione(codiceClienteFatturazione);

    if (clienteFaiDTO == null) {
      log.error("Not found {} with cliente code fatturazione {}", ENTITY_NAME, codiceClienteFatturazione);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.NOT_FOUND_CLIENTE_FAI);
    }

    return ResponseEntity.ok(clienteFaiDTO);
  }

}