package it.fai.ms.efservice.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DeliveryPrintDTO {
  private Instant dataSpedizione;
  private List<String> spedizioni = new ArrayList<>();
  private String lang;

  public Instant getDataSpedizione() {
    return dataSpedizione;
  }

  public void setDataSpedizione(Instant dataSpedizione) {
    this.dataSpedizione = dataSpedizione;
  }

  public List<String> getSpedizioni() {
    return spedizioni;
  }

  public void setSpedizioni(List<String> spedizioni) {
    this.spedizioni = spedizioni;
  }

  @Override
  public String toString() {
    return "DeliveryPrintRequestDTO{" +
      "dataSpedizione=" + dataSpedizione +
      ", spedizioni=" + spedizioni +
      ", lang=" + lang +
      '}';
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }
  
  
  
}
