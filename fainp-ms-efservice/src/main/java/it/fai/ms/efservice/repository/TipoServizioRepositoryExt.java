package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import it.fai.ms.efservice.domain.CategoriaServizio;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.service.dto.VistaListaServiziUtentiAppoggioDTO;

public interface TipoServizioRepositoryExt extends JpaRepository<TipoServizio, Long> {

    TipoServizio findByNome(String nome);

    List<TipoServizio> findByCategoriaServizioOrderByOrdinamento(CategoriaServizio categoriaServizio);

  @Query("select new it.fai.ms.efservice.service.dto.VistaListaServiziUtentiAppoggioDTO(count(v.nome),v.nome,v.stato) from VistaListaServiziUtenti v where v.codicecliente = :codice_cliente and v.categoriaServizioId =:categoria_servizio_id and v.chiaveFarlocca is not null and v.stato = 'ATTIVO' GROUP by v.nome,v.stato")
  List<VistaListaServiziUtentiAppoggioDTO> vistaListaServiziUtenti(@Param("categoria_servizio_id") Long categoria_servizio_id,
                                                                   @Param("codice_cliente") String codice_cliente);

  @Query("select tp from TipoServizio tp where tp.nome in :nomeTipiServizio")
  List<TipoServizio> findByNames(@Param("nomeTipiServizio") List<String> nomeTipiServizio);

  List<TipoServizio> findByCategoriaServizioOrderByNome(CategoriaServizio categoria);
}
