package it.fai.ms.efservice.service;

import it.fai.ms.efservice.service.dto.ContrattoDTO;
import java.util.List;

/**
 * Service Interface for managing Contratto.
 */
public interface ContrattoService {

    /**
     * Save a contratto.
     *
     * @param contrattoDTO the entity to save
     * @return the persisted entity
     */
    ContrattoDTO save(ContrattoDTO contrattoDTO);

    /**
     *  Get all the contrattos.
     *
     *  @return the list of entities
     */
    List<ContrattoDTO> findAll();

    /**
     *  Get the "id" contratto.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ContrattoDTO findOne(Long id);

    /**
     *  Delete the "id" contratto.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
