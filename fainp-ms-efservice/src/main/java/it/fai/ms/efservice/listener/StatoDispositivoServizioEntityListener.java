package it.fai.ms.efservice.listener;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.service.jms.dml.DispositivoDmlSenderUtil;
import it.fai.ms.efservice.service.jms.mapper.DispositivoJmsMapper;
import it.fai.ms.efservice.util.BeanUtil;

public class StatoDispositivoServizioEntityListener {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  // no-injection please
  DispositivoDmlSenderUtil dmlService;

  @PostPersist
  @PostUpdate
  public void sendSaveNotificationDML(StatoDispositivoServizio sds) {
    logger.info("StatoDispositivoServizio: {}", sds);
    Dispositivo dispositivo = sds.getDispositivo();
    logger.info("Dispositivo: {}", dispositivo);
    if (dispositivo == null || dispositivo.getIdentificativo() == null) {
      logger.debug("Identificativo Dispositivo is null, so not send");
      return;
    }
    getDmlService().sendSaveNotification(sds.getDispositivo());
  }

  @PostRemove
  public void sendDeleteNotificationDML(StatoDispositivoServizio sds) {
    logger.info("StatoDispositivoServizio: {}", sds);
    Dispositivo dispositivo = sds.getDispositivo();
    logger.info("Dispositivo: {}", dispositivo);
    if (dispositivo == null || dispositivo.getIdentificativo() == null) {
      logger.warn("Identificativo dispositivo is null...so not sent!");
      return;
    }
    getDmlService().sendDeleteNotification(dispositivo.getIdentificativo());
  }
  private DispositivoDmlSenderUtil getDmlService() {
    if (dmlService == null) {
      dmlService = new DispositivoDmlSenderUtil(BeanUtil.getBean(JmsProperties.class),BeanUtil.getBean(DispositivoJmsMapper.class));
    }
    return dmlService;
  }
}
