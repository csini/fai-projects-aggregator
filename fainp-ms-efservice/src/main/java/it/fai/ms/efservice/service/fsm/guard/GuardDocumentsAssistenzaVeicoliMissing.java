package it.fai.ms.efservice.service.fsm.guard;
 
import org.springframework.statemachine.StateContext;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardDocumentsAssistenzaVeicoliMissing extends GuardDocumentsAssistenzaVeicoliPresent {
 
  public GuardDocumentsAssistenzaVeicoliMissing(DocumentService documentService) {
    super(documentService);
  }
  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean result = super.evaluate(context);
    return !result;
  }
}
