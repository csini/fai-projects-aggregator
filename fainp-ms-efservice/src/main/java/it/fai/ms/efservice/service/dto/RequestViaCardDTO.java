package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

public class RequestViaCardDTO implements Serializable {

  private static final long serialVersionUID = 6168352212087710726L;

  private String codiceCliente;

  private int numeroViaCard;

  private int numeroTelepassIta;

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public int getNumeroViaCard() {
    return numeroViaCard;
  }

  public void setNumeroViaCard(int numeroViaCard) {
    this.numeroViaCard = numeroViaCard;
  }

  public int getNumeroTelepassIta() {
    return numeroTelepassIta;
  }

  public void setNumeroTelepassIta(int numeroTelepassIta) {
    this.numeroTelepassIta = numeroTelepassIta;
  }

  @Override
  public String toString() {
    return "RequestViaCardDTO [codiceCliente=" + codiceCliente + ", numeroViaCard=" + numeroViaCard + ", numeroTelepassIta="
           + numeroTelepassIta + "]";
  }

}
