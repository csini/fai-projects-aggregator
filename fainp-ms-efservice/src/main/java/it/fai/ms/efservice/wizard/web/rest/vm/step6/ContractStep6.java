package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;

public class ContractStep6 implements Serializable {
  private static final long serialVersionUID = -4705197078760781378L;
  
  private String tipoDispositivo;
  private Boolean contrattoAltroFornitore;
  
  public String getTipoDispositivo() {
    return tipoDispositivo;
  }
  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Boolean getContrattoAltroFornitore() {
    return contrattoAltroFornitore;
  }
  public void setContrattoAltroFornitore(Boolean contrattoAltroFornitore) {
    this.contrattoAltroFornitore = contrattoAltroFornitore;
  }
  @Override
  public String toString() {
    return "ContractStep6 [tipoDispositivo=" + tipoDispositivo + ", contrattoAltroFornitore=" + contrattoAltroFornitore + "]";
  }
}
