package it.fai.ms.efservice.client.dto;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class UserDTO {

  private boolean     activated;
  private Set<String> authorities  = new HashSet<>();
  private String      codiceAzienda;
  private String      ragioneSociale;
  private String      codiceContatto;
  private String      createdBy;
  private Instant     createdDate;
  @Email
  @Size(min = 5, max = 100)
  private String      email;
  @Size(max = 50)
  private String      firstName;
  private Long        id;
  @Size(max = 256)
  private String      imageUrl;
  @Size(min = 2, max = 5)
  private String      langKey;
  private String      lastModifiedBy;
  private Instant     lastModifiedDate;
  @Size(max = 50)
  private String      lastName;
  @NotBlank
  @Pattern(regexp = Constants.LOGIN_REGEX)
  @Size(min = 1, max = 50)
  private String      login;
  private boolean     impersonated = false;
  private Set<String> permissions;
  private String      codiceAgente;
  private String      username;

  public boolean isActivated() {
    return activated;
  }

  public void setActivated(boolean activated) {
    this.activated = activated;
  }

  public Set<String> getAuthorities() {
    return authorities;
  }

  public void setAuthorities(Set<String> authorities) {
    this.authorities = authorities;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCodiceContatto() {
    return codiceContatto;
  }

  public void setCodiceContatto(String codiceContatto) {
    this.codiceContatto = codiceContatto;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public Instant getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Instant createdDate) {
    this.createdDate = createdDate;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getLangKey() {
    return langKey;
  }

  public void setLangKey(String langKey) {
    this.langKey = langKey;
  }

  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  public Instant getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Instant lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public boolean isImpersonated() {
    return impersonated;
  }

  public void setImpersonated(boolean impersonated) {
    this.impersonated = impersonated;
  }

  public Set<String> getPermissions() {
    return permissions;
  }

  public void setPermissions(Set<String> permissions) {
    this.permissions = permissions;
  }

  public String getCodiceAgente() {
    return codiceAgente;
  }

  public void setCodiceAgente(String codiceAgente) {
    this.codiceAgente = codiceAgente;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Override
  public String toString() {
    return "UserDTO [activated=" + activated + ", authorities=" + authorities + ", codiceAzienda=" + codiceAzienda + ", ragioneSociale="
           + ragioneSociale + ", codiceContatto=" + codiceContatto + ", createdBy=" + createdBy + ", createdDate=" + createdDate
           + ", email=" + email + ", firstName=" + firstName + ", id=" + id + ", imageUrl=" + imageUrl + ", langKey=" + langKey
           + ", lastModifiedBy=" + lastModifiedBy + ", lastModifiedDate=" + lastModifiedDate + ", lastName=" + lastName + ", login=" + login
           + ", impersonated=" + impersonated + ", permissions=" + permissions + ", codiceAgente=" + codiceAgente + ", username=" + username
           + "]";
  }

}
