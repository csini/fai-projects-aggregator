package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.Set;

public class WarningRichiestaDTO implements Serializable {

  private static final long serialVersionUID = -3739679537929098760L;

  private Set<Long> idRichieste;

  private Set<String> vehiclesPlateWithoutBooklet;

  public WarningRichiestaDTO(Set<Long> _idRichieste, Set<String> _vehiclePlateWithoutBooklet) {
    idRichieste = _idRichieste;
    vehiclesPlateWithoutBooklet = _vehiclePlateWithoutBooklet;
  }

  public WarningRichiestaDTO(Set<String> _vehiclePlateWithoutBooklet) {
    vehiclesPlateWithoutBooklet = _vehiclePlateWithoutBooklet;
  }

  public Set<Long> getIdRichieste() {
    return idRichieste;
  }

  public void setIdRichieste(Set<Long> idRichieste) {
    this.idRichieste = idRichieste;
  }

  public Set<String> getVehiclesPlateWithoutBooklet() {
    return vehiclesPlateWithoutBooklet;
  }

  public void setVehiclesPlateWithoutBooklet(Set<String> vehiclesPlateWithoutBooklet) {
    this.vehiclesPlateWithoutBooklet = vehiclesPlateWithoutBooklet;
  }

}
