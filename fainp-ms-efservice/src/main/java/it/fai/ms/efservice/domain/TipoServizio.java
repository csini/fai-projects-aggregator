package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A TipoServizio.
 */
@Entity
@Table(name = "tipo_servizio")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TipoServizio implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "nome", nullable = false)
  private String nome;

  @Column(name = "descrizione")
  private String descrizione;

  @Column(name = "nome_business")
  private String nomeBusiness;

  @Column(name = "testo_promo")
  private String testoPromo;

  @Column(name = "ordinamento")
  private Long ordinamento;

  @Column(name = "documenti")
  private String documenti;

  @ManyToOne
  private TipoServizioDefault tipoServizioDefault;

  @ManyToOne
  private CategoriaServizio categoriaServizio;

  @ManyToMany(mappedBy = "tipoServizios")
  @JsonIgnore
  private Set<TipoDispositivo> tipoDispositivos = new HashSet<>();

  @Size(max = 3)
  @Column(name = "tipo_servizio_fix")
  private String tipoServizioFix;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public TipoServizio nome(String nome) {
    this.nome = nome;
    return this;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public TipoServizio descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public String getNomeBusiness() {
    return nomeBusiness;
  }

  public TipoServizio nomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
    return this;
  }

  public void setNomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
  }

  public String getTestoPromo() {
    return testoPromo;
  }

  public TipoServizio testoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
    return this;
  }

  public void setTestoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
  }

  public Long getOrdinamento() {
    return ordinamento;
  }

  public TipoServizio ordinamento(Long ordinamento) {
    this.ordinamento = ordinamento;
    return this;
  }

  public void setOrdinamento(Long ordinamento) {
    this.ordinamento = ordinamento;
  }

  public String getDocumenti() {
    return documenti;
  }

  public TipoServizio documenti(String documenti) {
    this.documenti = documenti;
    return this;
  }

  public void setDocumenti(String documenti) {
    this.documenti = documenti;
  }

  public TipoServizioDefault getTipoServizioDefault() {
    return tipoServizioDefault;
  }

  public TipoServizio tipoServizioDefault(TipoServizioDefault tipoServizioDefault) {
    this.tipoServizioDefault = tipoServizioDefault;
    return this;
  }

  public void setTipoServizioDefault(TipoServizioDefault tipoServizioDefault) {
    this.tipoServizioDefault = tipoServizioDefault;
  }

  public CategoriaServizio getCategoriaServizio() {
    return categoriaServizio;
  }

  public TipoServizio categoriaServizio(CategoriaServizio categoriaServizio) {
    this.categoriaServizio = categoriaServizio;
    return this;
  }

  public void setCategoriaServizio(CategoriaServizio categoriaServizio) {
    this.categoriaServizio = categoriaServizio;
  }

  public Set<TipoDispositivo> getTipoDispositivos() {
    return tipoDispositivos;
  }

  public TipoServizio tipoDispositivos(Set<TipoDispositivo> tipoDispositivos) {
    this.tipoDispositivos = tipoDispositivos;
    return this;
  }

  public TipoServizio addTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivos.add(tipoDispositivo);
    tipoDispositivo.getTipoServizios()
                   .add(this);
    return this;
  }

  public TipoServizio removeTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivos.remove(tipoDispositivo);
    tipoDispositivo.getTipoServizios()
                   .remove(this);
    return this;
  }

  public void setTipoDispositivos(Set<TipoDispositivo> tipoDispositivos) {
    this.tipoDispositivos = tipoDispositivos;
  }

  public String getTipoServizioFix() {
    return tipoServizioFix;
  }

  public TipoServizio tipoServizioFix(String tipoServizioFix) {
    this.tipoServizioFix = tipoServizioFix;
    return this;
  }

  public void setTipoServizioFix(String tipoServizioFix) {
    this.tipoServizioFix = tipoServizioFix;
  }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TipoServizio tipoServizio = (TipoServizio) o;
    if (tipoServizio.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), tipoServizio.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoServizio [id=");
    builder.append(id);
    builder.append(", nome=");
    builder.append(nome);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", nomeBusiness=");
    builder.append(nomeBusiness);
    builder.append(", testoPromo=");
    builder.append(testoPromo);
    builder.append(", ordinamento=");
    builder.append(ordinamento);
    builder.append(", documenti=");
    builder.append(documenti);
    builder.append(", categoriaServizio=");
    builder.append(categoriaServizio);
    builder.append(", tipoServizioFix=");
    builder.append(tipoServizioFix);
    builder.append("]");
    return builder.toString();
  }
}
