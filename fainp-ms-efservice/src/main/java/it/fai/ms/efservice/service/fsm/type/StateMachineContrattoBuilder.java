package it.fai.ms.efservice.service.fsm.type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;

@Service
public class StateMachineContrattoBuilder {

  private final Logger _log = LoggerFactory.getLogger(this.getClass());

  private StateMachineFactory<StatoContratto, ContrattoEvent> stateMachineFactory;

  @Autowired
  public StateMachineContrattoBuilder(@Qualifier("contratto") final StateMachineFactory<StatoContratto, ContrattoEvent> _stateMachineFactory) {
    stateMachineFactory = _stateMachineFactory;
  }

  public StateMachine<StatoContratto, ContrattoEvent> build() {
    StateMachine<StatoContratto, ContrattoEvent> stateMachine = stateMachineFactory.getStateMachine(FsmType.CONTRATTO.fsmName());
    _log.debug("Get StateMachine {}", stateMachine.getId());
    return stateMachine;
  }
}
