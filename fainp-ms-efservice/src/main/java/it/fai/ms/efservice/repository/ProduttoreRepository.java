package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.Produttore;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Produttore entity.
 */
@Repository
public interface ProduttoreRepository extends JpaRepository<Produttore, Long> {

  Produttore findOneByNome(String nome);

}
