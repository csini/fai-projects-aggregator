package it.fai.ms.efservice.dto;

import java.io.Serializable;
import java.time.Instant;

public class TimestampPreconditionDTO implements Serializable {

  private static final long serialVersionUID = -7843493122560930527L;

  private Instant lastModifiedDateVehicle;

  private Instant lastModifiedPrecondition;

  public TimestampPreconditionDTO(Instant lastModifiedDateVehicle) {
    this.lastModifiedDateVehicle = lastModifiedDateVehicle;
    this.lastModifiedPrecondition = Instant.now();
  }

  public Instant getLastModifiedDateVehicle() {
    return lastModifiedDateVehicle;
  }

  public void setLastModifiedDateVehicle(Instant lastModifiedDateVehicle) {
    this.lastModifiedDateVehicle = lastModifiedDateVehicle;
  }

  public Instant getLastModifiedPrecondition() {
    return lastModifiedPrecondition;
  }

  public void setLastModifiedPrecondition(Instant lastModifiedPrecondition) {
    this.lastModifiedPrecondition = lastModifiedPrecondition;
  }

  @Override
  public String toString() {
    return "TimestampPreconditionDTO [lastModifiedDateVehicle=" + lastModifiedDateVehicle + ", lastModifiedPrecondition="
           + lastModifiedPrecondition + "]";
  }

}
