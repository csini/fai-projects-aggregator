package it.fai.ms.efservice.service.fsm.util;

import java.util.HashSet;
import java.util.Set;

import it.fai.ms.efservice.domain.StatoContratto;

public class StatoContrattoUtil {

  public static Set<StatoContratto> getStates() {
    HashSet<StatoContratto> hashSet = new HashSet<StatoContratto>();
    hashSet.add(StatoContratto.ATTIVO);
    hashSet.add(StatoContratto.REVOCATO);
    hashSet.add(StatoContratto.SOSPESO);
    return hashSet;
  }

}
