package it.fai.ms.efservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "view_spedizioni_fornitore")
public class SpedizioneFornitoreView {
  @Id
  private Long id;

  @Column(name = "produttore")
  private String produttore;

  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "numero_dispositivi_spedibili")
  private Long numeroDispositiviSpedibili = 0L;

  @Column(name = "numero_dispositivi_disponibili")
  private Long numeroDispositiviDisponibili = 0L;

  @Column(name = "numero_dispositivi_da_ricevere")
  private Long numeroDispositiviDaRicevere = 0L;

  @Column(name = "targhe")
  private String targhe;

  @Column(name = "seriali")
  private String seriali;

  @Column(name = "data_rientro_dispositivo")
  private String dataRientroDispositivo;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Long getNumeroDispositiviSpedibili() {
    return numeroDispositiviSpedibili;
  }

  public void setNumeroDispositiviSpedibili(Long numeroDispositiviSpedibili) {
    this.numeroDispositiviSpedibili = numeroDispositiviSpedibili;
  }

  public Long getNumeroDispositiviDisponibili() {
    return numeroDispositiviDisponibili;
  }

  public void setNumeroDispositiviDisponibili(Long numeroDispositiviDisponibili) {
    this.numeroDispositiviDisponibili = numeroDispositiviDisponibili;
  }

  public Long getNumeroDispositiviDaRicevere() {
    return numeroDispositiviDaRicevere;
  }

  public void setNumeroDispositiviDaRicevere(Long numeroDispositiviDaRicevere) {
    this.numeroDispositiviDaRicevere = numeroDispositiviDaRicevere;
  }

  public String getTarghe() {
    return targhe;
  }

  public void setTarghe(String targhe) {
    this.targhe = targhe;
  }

  public String getSeriali() {
    return seriali;
  }

  public void setSeriali(String seriali) {
    this.seriali = seriali;
  }

  public String getDataRientroDispositivo() {
    return dataRientroDispositivo;
  }

  public void setDataRientroDispositivo(String dataRientroDispositivo) {
    this.dataRientroDispositivo = dataRientroDispositivo;
  }

  @Override
  public String toString() {
    return "SpedizioneFornitoreView{" +
      "id=" + id +
      ", produttore='" + produttore + '\'' +
      ", tipoDispositivo='" + tipoDispositivo + '\'' +
      ", numeroDispositiviSpedibili=" + numeroDispositiviSpedibili +
      ", numeroDispositiviDisponibili=" + numeroDispositiviDisponibili +
      ", numeroDispositiviDaRicevere=" + numeroDispositiviDaRicevere +
      ", targhe='" + targhe + '\'' +
      ", seriali='" + seriali + '\'' +
      ", dataRientroDispositivo='" + dataRientroDispositivo + '\'' +
      '}';
  }
}
