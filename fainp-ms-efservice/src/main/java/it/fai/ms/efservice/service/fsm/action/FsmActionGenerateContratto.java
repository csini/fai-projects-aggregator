package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class FsmActionGenerateContratto implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final GeneratorContractCodeService generatorContractCodeService;

  public FsmActionGenerateContratto(final GeneratorContractCodeService _generatorContractCodeService) {
    generatorContractCodeService = _generatorContractCodeService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;

        Contratto contract = richiesta.getContratto();
        TipoDispositivo tipoDispositivo = richiesta.getTipoDispositivo();
        log.info("Contratto {} and Tipo dispositivo {} for Richiesta {}", contract, tipoDispositivo, richiesta.getIdentificativo());
        try {
          contract = generatorContractCodeService.generateAndSaveContractCode(tipoDispositivo.getNome(), contract);
          log.info("Contratto: {}", contract);
          richiesta.setContratto(contract);
        } catch (Exception e) {
          log.error("Exception on generate codice contratto for richiesta: {}", richiesta, e);
        }
      }
    }
  }

}
