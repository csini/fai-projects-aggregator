package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.DispositivoService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Dispositivo.
 */
@RestController
@RequestMapping("/api")
public class DispositivoResource {

  private final Logger log = LoggerFactory.getLogger(DispositivoResource.class);

  private static final String ENTITY_NAME = "dispositivo";

  private final DispositivoService dispositivoService;

  public DispositivoResource(DispositivoService dispositivoService) {
    this.dispositivoService = dispositivoService;
  }

  /**
   * POST /dispositivos : Create a new dispositivo.
   *
   * @param dispositivoDTO
   *          the dispositivoDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new dispositivoDTO, or with status 400 (Bad
   *         Request) if the dispositivo has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping("/dispositivos")
  @Timed
  public ResponseEntity<DispositivoDTO> createDispositivo(@Valid @RequestBody DispositivoDTO dispositivoDTO) throws URISyntaxException {
    log.debug("REST request to save Dispositivo : {}", dispositivoDTO);
    if (dispositivoDTO.getId() != null) {
      return ResponseEntity.badRequest()
                           .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new dispositivo cannot already have an ID"))
                           .body(null);
    }
    DispositivoDTO result = dispositivoService.save(dispositivoDTO);
    return ResponseEntity.created(new URI("/api/dispositivos/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                          .toString()))
                         .body(result);
  }

  /**
   * PUT /dispositivos : Updates an existing dispositivo.
   *
   * @param dispositivoDTO
   *          the dispositivoDTO to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated dispositivoDTO, or with status 400 (Bad
   *         Request) if the dispositivoDTO is not valid, or with status 500 (Internal Server Error) if the
   *         dispositivoDTO couldn't be updated
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PutMapping("/dispositivos")
  @Timed
  public ResponseEntity<DispositivoDTO> updateDispositivo(@Valid @RequestBody DispositivoDTO dispositivoDTO) throws URISyntaxException {
    log.debug("REST request to update Dispositivo : {}", dispositivoDTO);
    if (dispositivoDTO.getId() == null) {
      return createDispositivo(dispositivoDTO);
    }
    DispositivoDTO result = dispositivoService.save(dispositivoDTO);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dispositivoDTO.getId()
                                                                                                .toString()))
                         .body(result);
  }

  /**
   * GET /dispositivos : get all the dispositivos.
   *
   * @param filter
   *          the filter of the request
   * @return the ResponseEntity with status 200 (OK) and the list of dispositivos in body
   */
  @GetMapping("/dispositivos")
  @Timed
  public List<DispositivoDTO> getAllDispositivos() {
    log.debug("REST request to get all Dispositivos");
    return dispositivoService.findAll();
  }

  /**
   * GET /dispositivos/:id : get the "id" dispositivo.
   *
   * @param id
   *          the id of the dispositivoDTO to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the dispositivoDTO, or with status 404 (Not Found)
   */
  @GetMapping("/dispositivos/{id}")
  @Timed
  public ResponseEntity<DispositivoDTO> getDispositivo(@PathVariable Long id) {
    log.debug("REST request to get Dispositivo : {}", id);
    DispositivoDTO dispositivoDTO = dispositivoService.findOne(id);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dispositivoDTO));
  }

  /**
   * DELETE /dispositivos/:id : delete the "id" dispositivo.
   *
   * @param id
   *          the id of the dispositivoDTO to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/dispositivos/{id}")
  @Timed
  public ResponseEntity<Void> deleteDispositivo(@PathVariable Long id) {
    log.debug("REST request to delete Dispositivo : {}", id);
    dispositivoService.delete(id);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                         .build();
  }
}
