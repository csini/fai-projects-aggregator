package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.BatchSize;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.listener.DispositivoEntityListener;
import org.springframework.beans.BeanUtils;

/**
 * A Dispositivo.
 */
@Entity
@EntityListeners({ DispositivoEntityListener.class })
// @Audited(withModifiedFlag = true, targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "dispositivo")
public class Dispositivo implements Serializable {

  private static final long serialVersionUID = 1L;

  public final static String NO_SERIALE_DISPOSITIVO = "NO_SERIALE";


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "seriale", nullable = false)
  private String seriale;

  @Column(name = "seriale_sostituito", nullable = true)
  private String serialeSostituito;

  @Column(name = "identificativo", nullable = false, unique = true)
  private String identificativo;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "stato", nullable = false)
  private StatoDispositivo stato;

  @Column(name = "data_spedizione")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataSpedizione;

  @Column(name = "data_prima_attivazione")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataPrimaAttivazione;

  @Column(name = "tracking_number")
  private String trackingNumber;

  @Column(name = "shipment_reference")
  private String shipmentReference;

  @Enumerated(EnumType.STRING)
  @Column(name = "modalita_spedizione")
  private ModalitaSpedizione modalitaSpedizione;

  @Enumerated(EnumType.STRING)
  @Column(name = "tipo_magazzino")
  private TipoMagazzino tipoMagazzino;

  @Column(name = "operazioni_possibili")
  private String operazioniPossibili;

  @Column(name = "data_modifica_stato")
  private Instant dataModificaStato;

  @Column(name = "data_scadenza")
  private ZonedDateTime dataScadenza;

  @Column(name = "targa_2")
  private String targa2;

  @Column(name = "targa_3")
  private String targa3;

  @Column(name = "targa_data_validita")
  private ZonedDateTime targaDataValidita;

  @Column(name = "tipo_hardware")
  private String tipoHardware;

  @ManyToOne
  private Contratto contratto;

  @OneToMany(mappedBy = "dispositivo")
  @BatchSize(size = 100)
  private Set<AssociazioneDV> associazioneDispositivoVeicolos = new HashSet<>();

  @OneToMany(mappedBy = "dispositivo")
  @BatchSize(size = 100)
  private Set<StatoDispositivoServizio> statoDispositivoServizios = new HashSet<>();

  @ManyToOne
  private TipoDispositivo tipoDispositivo;

  @OneToMany(mappedBy = "dispositivo")
  @JsonIgnore
  private Set<StoricoAssociazioneDV> storicoAssociazioneDVs;

  @ManyToMany(mappedBy = "dispositivos")
  @JsonIgnore
  private Set<Richiesta> richiestas = new HashSet<>();

  @Column(name = "note_operatore")
  private String noteOperatore;

  @Column(name = "note_tecniche")
  private String noteTecniche;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSeriale() {
    return seriale;
  }

  public Dispositivo seriale(String seriale) {
    this.seriale = seriale;
    return this;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public String getSerialeSostituito() {
    return serialeSostituito;
  }

  public void setSerialeSostituito(String serialeSostituito) {
    this.serialeSostituito = serialeSostituito;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public Dispositivo identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }

  public StatoDispositivo getStato() {
    return stato;
  }

  public Dispositivo stato(StatoDispositivo stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(StatoDispositivo stato) {
    this.stato = stato;
  }

  public Instant getDataSpedizione() {
    return dataSpedizione;
  }

  public Dispositivo dataSpedizione(Instant dataSpedizione) {
    this.dataSpedizione = dataSpedizione;
    return this;
  }

  public void setDataSpedizione(Instant dataSpedizione) {
    this.dataSpedizione = dataSpedizione;
  }

  public Instant getDataPrimaAttivazione() {
    return dataPrimaAttivazione;
  }

  public Dispositivo dataPrimaAttivazione(Instant dataPrimaAttivazione) {
    this.dataPrimaAttivazione = dataPrimaAttivazione;
    return this;
  }

  public void setDataPrimaAttivazione(Instant dataPrimaAttivazione) {
    if (this.dataPrimaAttivazione == null) {
      this.dataPrimaAttivazione = dataPrimaAttivazione;
    }
  }

  public String getTrackingNumber() {
    return trackingNumber;
  }

  public Dispositivo trackingNumber(String trackingNumber) {
    this.trackingNumber = trackingNumber;
    return this;
  }

  public void setTrackingNumber(String trackingNumber) {
    this.trackingNumber = trackingNumber;
  }

  public String getShipmentReference() {
    return shipmentReference;
  }

  public Dispositivo shipmentReference(String shipmentReference) {
    this.shipmentReference = shipmentReference;
    return this;
  }

  public void setShipmentReference(String shipmentReference) {
    this.shipmentReference = shipmentReference;
  }

  public ModalitaSpedizione getModalitaSpedizione() {
    return modalitaSpedizione;
  }

  public Dispositivo modalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
    return this;
  }

  public void setModalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
  }

  public TipoMagazzino getTipoMagazzino() {
    return tipoMagazzino;
  }

  public Dispositivo tipoMagazzino(TipoMagazzino tipoMagazzino) {
    this.tipoMagazzino = tipoMagazzino;
    return this;
  }

  public void setTipoMagazzino(TipoMagazzino tipoMagazzino) {
    this.tipoMagazzino = tipoMagazzino;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public Dispositivo operazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
    return this;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public Dispositivo dataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
    return this;
  }

  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }

  public ZonedDateTime getDataScadenza() {
    return dataScadenza;
  }

  public Dispositivo dataScadenza(ZonedDateTime dataScadenza) {
    this.dataScadenza = dataScadenza;
    return this;
  }

  public void setDataScadenza(ZonedDateTime dataScadenza) {
    this.dataScadenza = dataScadenza;
  }

  public String getTarga2() {
    return targa2;
  }

  public Dispositivo targa2(String targa2) {
    this.targa2 = targa2;
    return this;
  }

  public void setTarga2(String targa2) {
    this.targa2 = targa2;
  }

  public String getTarga3() {
    return targa3;
  }

  public Dispositivo targa3(String targa3) {
    this.targa3 = targa3;
    return this;
  }

  public void setTarga3(String targa3) {
    this.targa3 = targa3;
  }

  public ZonedDateTime getTargaDataValidita() {
    return targaDataValidita;
  }

  public Dispositivo targaDataValidita(ZonedDateTime targaDataValidita) {
    this.targaDataValidita = targaDataValidita;
    return this;
  }

  public void setTargaDataValidita(ZonedDateTime targaDataValidita) {
    this.targaDataValidita = targaDataValidita;
  }

  public String getTipoHardware() {
    return tipoHardware;
  }

  public Dispositivo tipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
    return this;
  }

  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }

  public Contratto getContratto() {
    return contratto;
  }

  public Dispositivo contratto(Contratto contratto) {
    this.contratto = contratto;
    return this;
  }

  public void setContratto(Contratto contratto) {
    this.contratto = contratto;
  }

  public Set<AssociazioneDV> getAssociazioneDispositivoVeicolos() {
    return associazioneDispositivoVeicolos;
  }

  public Dispositivo associazioneDispositivoVeicolos(Set<AssociazioneDV> associazioneDVS) {
    this.associazioneDispositivoVeicolos = associazioneDVS;
    return this;
  }

  public Dispositivo addAssociazioneDispositivoVeicolo(AssociazioneDV associazioneDV) {
    this.associazioneDispositivoVeicolos.add(associazioneDV);
    associazioneDV.setDispositivo(this);
    return this;
  }

  public Dispositivo removeAssociazioneDispositivoVeicolo(AssociazioneDV associazioneDV) {
    this.associazioneDispositivoVeicolos.remove(associazioneDV);
    associazioneDV.setDispositivo(null);
    return this;
  }

  public void setAssociazioneDispositivoVeicolos(Set<AssociazioneDV> associazioneDVS) {
    this.associazioneDispositivoVeicolos = associazioneDVS;
  }

  public Set<StatoDispositivoServizio> getStatoDispositivoServizios() {
    return statoDispositivoServizios;
  }

  public Dispositivo statoDispositivoServizios(Set<StatoDispositivoServizio> statoDispositivoServizios) {
    this.statoDispositivoServizios = statoDispositivoServizios;
    return this;
  }

  public Dispositivo addStatoDispositivoServizio(StatoDispositivoServizio statoDispositivoServizio) {
    this.statoDispositivoServizios.add(statoDispositivoServizio);
    statoDispositivoServizio.setDispositivo(this);
    return this;
  }

  public Dispositivo removeStatoDispositivoServizio(StatoDispositivoServizio statoDispositivoServizio) {
    this.statoDispositivoServizios.remove(statoDispositivoServizio);
    statoDispositivoServizio.setDispositivo(null);
    return this;
  }

  public void setStatoDispositivoServizios(Set<StatoDispositivoServizio> statoDispositivoServizios) {
    this.statoDispositivoServizios = statoDispositivoServizios;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }

  public Dispositivo tipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Set<StoricoAssociazioneDV> getStoricoAssociazioneDVs() {
    return storicoAssociazioneDVs;
  }

  public void setStoricoAssociazioneDVs(Set<StoricoAssociazioneDV> storicoAssociazioneDVs) {
    this.storicoAssociazioneDVs = storicoAssociazioneDVs;
  }

  public Set<Richiesta> getRichiestas() {
    return richiestas;
  }

  public Dispositivo richiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
    return this;
  }

  public Dispositivo addRichiesta(Richiesta richiesta) {
    this.richiestas.add(richiesta);
    richiesta.getDispositivos()
             .add(this);
    return this;
  }

  public Dispositivo removeRichiesta(Richiesta richiesta) {
    this.richiestas.remove(richiesta);
    richiesta.getDispositivos()
             .remove(this);
    return this;
  }

  public void setRichiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
  }

  public String getNoteOperatore() {
    return noteOperatore;
  }

  public void setNoteOperatore(String noteOperatore) {
    this.noteOperatore = noteOperatore;
  }

  public String getNoteTecniche() {
    return noteTecniche;
  }

  public void setNoteTecniche(String noteTecniche) {
    this.noteTecniche = noteTecniche;
  }

  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  public Dispositivo clone(){
    Dispositivo newDispositivo = new Dispositivo();
    BeanUtils.copyProperties(this, newDispositivo);
    return newDispositivo;
  }

  public Dispositivo cloneAndResetFileds(){
    return this.clone().resetFieldsAfterClone();
  }

  private Dispositivo resetFieldsAfterClone() {
    this.id = null;
    this.identificativo = null;
    this.dataPrimaAttivazione = null;
    this.noteOperatore = null;
    this.noteTecniche = null;
    this.setSeriale(NO_SERIALE_DISPOSITIVO);
    this.setStato(StatoDispositivo.INIZIALE);
    this.setDataModificaStato(Instant.now());
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dispositivo dispositivo = (Dispositivo) o;
    if (dispositivo.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), dispositivo.getId());
  }


  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "Dispositivo{" + "id=" + getId() + ", seriale='" + getSeriale() + ", serialeSostituito='" + getSerialeSostituito() + "'"
           + ", identificativo='" + getIdentificativo() + "'" + ", stato='" + getStato() + "'" 
           + "'" + ", tipoDispositivo='" + getTipoHardware() + "'" + ", dataSpedizione='" + getDataSpedizione() + "'"
           + ", dataPrimaAttivazione='" + getDataPrimaAttivazione() + "'" + ", trackingNumber='"
           + getTrackingNumber() + "'" + ", shipmentReference='" + getShipmentReference() + "'" + ", modalitaSpedizione='"
           + getModalitaSpedizione() + "'" + ", tipoMagazzino='" + getTipoMagazzino() + "'" + ", operazioniPossibili='"
           + getOperazioniPossibili() + "'" + ", dataModificaStato='" + getDataModificaStato() + "'" + ", dataScadenza='"
           + getDataScadenza() + "'" + ", targa2='" + getTarga2() + "'" + ", targa3='" + getTarga3() + "'" + ", targaDataValidita='"
           + getTargaDataValidita() + "'" + ", tipoHardware='" + getTipoHardware() + "'" + ", noteOperatore='" + getNoteOperatore() + "'"
           + ", noteTecniche='" + getNoteTecniche() + "}";
  }
  
//  @PrePersist
//  private void prePersist() {
//    if (StringUtils.isNotBlank(this.seriale)) {
//    }    
//  }
//  
//  @PreUpdate
//  private void preUpdate() {
//    if (StringUtils.isNotBlank(this.seriale)) {
//      
//    }    
//  }
  
  
}
