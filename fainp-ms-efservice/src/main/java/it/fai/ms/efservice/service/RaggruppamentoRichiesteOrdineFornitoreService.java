package it.fai.ms.efservice.service;

import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.repository.RaggruppamentoRichiesteOrdineFornitoreRepository;
import it.fai.ms.efservice.service.dto.RaggruppamentoRichiesteOrdineFornitoreDTO;
import it.fai.ms.efservice.service.mapper.RaggruppamentoRichiesteOrdineFornitoreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing RaggruppamentoRichiesteOrdineFornitore.
 */
@Service
@Transactional
public class RaggruppamentoRichiesteOrdineFornitoreService {

    private final Logger log = LoggerFactory.getLogger(RaggruppamentoRichiesteOrdineFornitoreService.class);

    private final RaggruppamentoRichiesteOrdineFornitoreRepository raggruppamentoRichiesteOrdineFornitoreRepository;

    private final RaggruppamentoRichiesteOrdineFornitoreMapper raggruppamentoRichiesteOrdineFornitoreMapper;

    public RaggruppamentoRichiesteOrdineFornitoreService(RaggruppamentoRichiesteOrdineFornitoreRepository raggruppamentoRichiesteOrdineFornitoreRepository, RaggruppamentoRichiesteOrdineFornitoreMapper raggruppamentoRichiesteOrdineFornitoreMapper) {
        this.raggruppamentoRichiesteOrdineFornitoreRepository = raggruppamentoRichiesteOrdineFornitoreRepository;
        this.raggruppamentoRichiesteOrdineFornitoreMapper = raggruppamentoRichiesteOrdineFornitoreMapper;
    }

    /**
     * Save a raggruppamentoRichiesteOrdineFornitore.
     *
     * @param raggruppamentoRichiesteOrdineFornitoreDTO the entity to save
     * @return the persisted entity
     */
    public RaggruppamentoRichiesteOrdineFornitoreDTO save(RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO) {
        log.debug("Request to save RaggruppamentoRichiesteOrdineFornitore : {}", raggruppamentoRichiesteOrdineFornitoreDTO);
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreMapper.toEntity(raggruppamentoRichiesteOrdineFornitoreDTO);
        raggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreRepository.save(raggruppamentoRichiesteOrdineFornitore);
        return raggruppamentoRichiesteOrdineFornitoreMapper.toDto(raggruppamentoRichiesteOrdineFornitore);
    }

    /**
     *  Get all the raggruppamentoRichiesteOrdineFornitores.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<RaggruppamentoRichiesteOrdineFornitoreDTO> findAll() {
        log.debug("Request to get all RaggruppamentoRichiesteOrdineFornitores");
        return raggruppamentoRichiesteOrdineFornitoreRepository.findAll().stream()
            .map(raggruppamentoRichiesteOrdineFornitoreMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one raggruppamentoRichiesteOrdineFornitore by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RaggruppamentoRichiesteOrdineFornitoreDTO findOne(Long id) {
        log.debug("Request to get RaggruppamentoRichiesteOrdineFornitore : {}", id);
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreRepository.findOne(id);
        return raggruppamentoRichiesteOrdineFornitoreMapper.toDto(raggruppamentoRichiesteOrdineFornitore);
    }

    /**
     *  Delete the  raggruppamentoRichiesteOrdineFornitore by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RaggruppamentoRichiesteOrdineFornitore : {}", id);
        raggruppamentoRichiesteOrdineFornitoreRepository.delete(id);
    }
}
