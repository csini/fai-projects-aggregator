/**
 * 
 */
package it.fai.ms.efservice.service.fsm.util;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.dto.notification.LogOperazioniPublishDTO;
import it.fai.ms.common.jms.dto.notification.TipologiaOggettoDTOEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.DispositivoService;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmSwitchStateException;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

/**
 * @author Luca Vassallo
 */
@Service
public class FsmDispositivoUtilService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private DispositivoService    dispositivoService;
  private ClienteFaiRepository  clienteFaiRepo;
  private JmsTopicSenderService senderJmsService;

  public FsmDispositivoUtilService(DispositivoService dispositivoService, ClienteFaiRepository clienteFaiRepo,
                                   JmsTopicSenderService senderJmsService) {
    super();
    this.dispositivoService = dispositivoService;
    this.clienteFaiRepo = clienteFaiRepo;
    this.senderJmsService = senderJmsService;
  }

  public boolean switchToState(Message<DispositivoEvent> msg, StatoDispositivo toState,
                               Transition<StatoDispositivo, DispositivoEvent> transition) throws FsmSwitchStateException {

    MessageHeaders headers = msg.getHeaders();
    boolean isSwitch = switchToState(headers, toState);
    if (isSwitch) {
      Trigger<StatoDispositivo, DispositivoEvent> trigger = transition.getTrigger();
      boolean isAutoTransition = false;
      if (trigger == null) {
        isAutoTransition = true;
      }

      sendNotificationByTransitionType(isAutoTransition, headers, transition);
    } else {
      return false;
    }

    return true;
  }

  private void sendNotificationByTransitionType(boolean isAutoTx, MessageHeaders headers,
                                                Transition<StatoDispositivo, DispositivoEvent> transition) {
    log.info("Invio Notifica di cambio stato " + ((isAutoTx) ? "AUTOMATICO" : "MANUALE") + ".");
    Long idDispositivo = null;
    String ultimaNota = null;
    StatoDispositivo stateFrom = null;
    StatoDispositivo stateTo = null;

    Object obj = (Object) headers.get("object");
    if (obj instanceof Dispositivo) {
      Dispositivo dispositivo = (Dispositivo) obj;
      idDispositivo = dispositivo.getId();
    }

    stateFrom = transition.getSource()
                          .getId();
    stateTo = transition.getTarget()
                        .getId();

    LocalDate now = LocalDate.now();
    if (isAutoTx) {
      sendNotification(idDispositivo, now, stateFrom, stateTo);
    } else {
      String faiOperator = SecurityUtils.getCurrentUserLogin();
      sendNotification(idDispositivo, now, stateFrom, stateTo, ultimaNota, faiOperator);
    }
  }

  private void sendNotification(long idObj, LocalDate date, StatoDispositivo stateFrom, StatoDispositivo stateTo) {
    sendNotification(idObj, date, stateFrom, stateTo, null, null);
  }

  /**
   * @author marco.murdocca
   */

  private void sendNotification(long idObj, LocalDate date, StatoDispositivo stateFrom, StatoDispositivo stateTo, String ultimaNota,
                                String operatoreFai) {
    log.info("INFO TRANSITION:\n\t- idObj: " + idObj + "\n\t- Data: " + new Date() + "\n\t- Transizione => Da: [" + stateFrom + "] A: ["
             + stateTo + "]\n\t- UltimaNota: " + ultimaNota + "\n\t- OperatoreFai: " + operatoreFai);

    DispositivoDTO dispositivo = dispositivoService.findOne(idObj);
    if (dispositivo == null) {
      log.warn("Not found Dispositivo [od: {}]", idObj);
      return;
    }

    String clienteFaiCodiceCliente = "";
    // Il cliente Fai è obbligatorio a livello di Entity Ordine;
    // final ClienteFaiDTO clienteFaiInOrdine = ordine.getClienteFai();
    // String clienteFaiCodiceCliente = clienteFaiInOrdine.getCodiceCliente();
    //
    // if (StringUtils.isBlank(clienteFaiCodiceCliente)) {
    // ClienteFai clienteFai = null;
    // Long clienteFaiId = clienteFaiInOrdine.getId();
    // if (clienteFaiId != null && clienteFaiId > 0) {
    // clienteFai = clienteFaiRepo.findOne(clienteFaiId);
    // }
    //
    // if (clienteFai == null) {
    // log.error("Error in codice cliente FAI: [{} - {}]", clienteFaiInOrdine.getId(), identificativoOrdine);
    // return;
    // } else {
    // clienteFaiCodiceCliente = clienteFai.getCodiceCliente();
    // }
    // }

    // Ordine numero <ID_ORDINE> cambia stato in <STATO_DESTINAZIONE>. Autore della modifica:
    // <SISTEMA_OPPURE_OPERATORE_FAI>. Note: <NOTE>
    StringBuilder sb = new StringBuilder("Cambio stato da " + stateFrom.name() + " ");
    sb.append("a " + stateTo.name() + ". ");
    sb.append("Autore della modifica: " + ((StringUtils.isNotBlank(operatoreFai)) ? operatoreFai : "Sistema"));
    if (ultimaNota != null) {
      sb.append(". Note: " + ultimaNota);
    }
    sb.append(". [" + idObj + "]");
    String message = sb.toString();

    LogOperazioniPublishDTO logOperationDTO = buildLogOperationDto(clienteFaiCodiceCliente, date, message, stateFrom, stateTo, idObj);
    try {
      senderJmsService.publishLogTopic(logOperationDTO);
    } catch (Exception e) {
      log.warn("JmsError: " + e);
    }
  }

  private LogOperazioniPublishDTO buildLogOperationDto(String clienteFaiCodiceCliente, LocalDate now, String msg,
                                                       StatoDispositivo stateFrom, StatoDispositivo stateTo, Long identificativo) {
    LogOperazioniPublishDTO dto = new LogOperazioniPublishDTO();

    dto.setData(now);
    dto.setIdentificativo(identificativo.toString());
    dto.setUtenteDestinatario(clienteFaiCodiceCliente);
    dto.setMessaggio(msg);
    dto.setStateFrom(stateFrom.name());
    dto.setStateTo(stateTo.name());
    dto.setTipologiaOggetto(TipologiaOggettoDTOEnum.DISPOSITIVI);

    return dto;
  }

  private boolean switchToState(Map<String, Object> headers, StatoDispositivo toState) {
    Object object = headers.get("object");
    boolean isValidEntity = validateEntity(object);
    if (isValidEntity) {

      if (object instanceof Dispositivo) {
        Dispositivo dispositivo = (Dispositivo) object;
        dispositivo.setStato(toState);
        dispositivo.setDataModificaStato(Instant.now());
        if (isStatusIn(toState, StatoDispositivo.getGreenState()) && dispositivo.getDataPrimaAttivazione() == null) {
          dispositivo.setDataPrimaAttivazione(Instant.now());
        }
      }
    } else {
      return false;
    }

    return true;
  }

  private boolean isStatusIn(StatoDispositivo stato, Set<StatoDispositivo> states) {
    if (states.contains(stato))
      return true;
    return false;
  }

  private boolean validateEntity(Object obj) {
    if (obj == null) {
      log.error("Entity to change status is null");
      return false;
    }
    return true;
  }

}
