package it.fai.ms.efservice.wizard.web.rest.vm.step6;

import java.io.Serializable;
import java.util.List;

public class DeviceStep6 implements Serializable {
  private static final long serialVersionUID = -6292486133425951545L;

  private String tipoDispositivo;
  private List<VehicleStep6> vehicles;
  
  public DeviceStep6() {}

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public List<VehicleStep6> getVehicles() {
    return vehicles;
  }

  public void setVehicles(List<VehicleStep6> vehicles) {
    this.vehicles = vehicles;
  }

  @Override
  public String toString() {
    return "DeviceStep6 [tipoDispositivo=" + tipoDispositivo + ", vehicles=" + vehicles + "]";
  }
  
 
}
