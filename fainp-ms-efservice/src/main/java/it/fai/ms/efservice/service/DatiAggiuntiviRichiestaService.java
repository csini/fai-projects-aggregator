package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.repository.DatiAggiuntiviRichiestaRepository;

@Service
@Transactional
public class DatiAggiuntiviRichiestaService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DatiAggiuntiviRichiestaRepository datiAggiuntiviRepo;

  public DatiAggiuntiviRichiestaService(final DatiAggiuntiviRichiestaRepository _datiAggiuntiviRepo) {
    datiAggiuntiviRepo = _datiAggiuntiviRepo;
  }

  public DatiAggiuntiviRichiesta save(DatiAggiuntiviRichiesta entity) {
    log.info("Persist {}: {}", DatiAggiuntiviRichiesta.class, entity);
    return datiAggiuntiviRepo.save(entity);
  }

  public DatiAggiuntiviRichiesta saveAndFlush(DatiAggiuntiviRichiesta datiAggiuntiviRichiesta) {
    DatiAggiuntiviRichiesta datAggiuntivi = save(datiAggiuntiviRichiesta);
    datiAggiuntiviRepo.flush();
    return datAggiuntivi;
  }

  public List<DatiAggiuntiviRichiesta> saveAndFlush(List<DatiAggiuntiviRichiesta> datiAggiuntivi) {
    List<DatiAggiuntiviRichiesta> datiAggiuntiviPersisted = new ArrayList<>();
    if (datiAggiuntivi != null && !datiAggiuntivi.isEmpty()) {
      datiAggiuntiviPersisted = datiAggiuntivi.stream()
                                              .map(data -> saveAndFlush(data))
                                              .collect(toList());
    }
    return datiAggiuntiviPersisted;
  }

}
