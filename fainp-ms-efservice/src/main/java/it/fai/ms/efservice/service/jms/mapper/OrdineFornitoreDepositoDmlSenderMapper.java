package it.fai.ms.efservice.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.OrdineFornitoreDepositoDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractDmlSenderMapper;
import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import it.fai.ms.efservice.domain.OrdineFornitore;

@Component
public class OrdineFornitoreDepositoDmlSenderMapper extends AbstractDmlSenderMapper<OrdineFornitoreDepositoDMLDTO, OrdineFornitore> {

  public OrdineFornitoreDepositoDmlSenderMapper() {
    super(OrdineFornitoreDepositoDMLDTO::new);
  }

  protected OrdineFornitoreDepositoDMLDTO toDMLDTO(OrdineFornitore entity, OrdineFornitoreDepositoDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyDMLDTO(String.valueOf(entity.getId()));
    }

    AnagraficaGiacenza anagraficaGiacenza = entity.getAnagraficaGiacenza();
    dml.setAlertRiordino((anagraficaGiacenza != null) ? anagraficaGiacenza.getAlertRiordino() : null);
    dml.setDataRiordino(entity.getDataRiordino());
    dml.setGiancezaMinima((anagraficaGiacenza != null) ? anagraficaGiacenza.getGiacenzaMinima() : null);
    dml.setNumDispositiviInRiordino(entity.getQuantita());
    dml.setNumDispositiviOrdinatiDaEvadere(entity.getNumeroDispositiviAcquisiti());
    dml.setOperatore(entity.getRichiedente());
    dml.setProduttore((anagraficaGiacenza != null) ? anagraficaGiacenza.getProduttoreNome() : null);
    dml.setTipoDispositivo((anagraficaGiacenza != null) ? anagraficaGiacenza.getTipoDispositivo() : null);
    dml.setTipologiaDisposizione(entity.getTipologiaDisposizione()
                                       .name());
    dml.setNote(entity.getNote());

    return dml;
  }

}
