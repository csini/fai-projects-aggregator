package it.fai.ms.efservice.service;

import java.util.List;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;

public interface RequestVehicleChangeService {

  List<Richiesta> generateModificationRequestDeviceService(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException;

}
