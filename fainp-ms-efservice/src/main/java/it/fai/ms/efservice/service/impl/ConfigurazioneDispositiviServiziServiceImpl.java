package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;
import it.fai.ms.efservice.repository.ConfigurazioneDispositiviServiziRepository;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;

/**
 * Service Implementation for managing ConfigurazioneDispositiviServizi.
 */
@Service
@Transactional
public class ConfigurazioneDispositiviServiziServiceImpl implements ConfigurazioneDispositiviServiziService {

  private final Logger log = LoggerFactory.getLogger(ConfigurazioneDispositiviServiziServiceImpl.class);

  private final ConfigurazioneDispositiviServiziRepository configurazioneDispositiviServiziRepository;

  public ConfigurazioneDispositiviServiziServiceImpl(ConfigurazioneDispositiviServiziRepository configurazioneDispositiviServiziRepository) {
    this.configurazioneDispositiviServiziRepository = configurazioneDispositiviServiziRepository;
  }

  /**
   * Save a configurazioneDispositiviServizi.
   *
   * @param configurazioneDispositiviServizi
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public ConfigurazioneDispositiviServizi save(ConfigurazioneDispositiviServizi configurazioneDispositiviServizi) {
    log.debug("Request to save ConfigurazioneDispositiviServizi : {}", configurazioneDispositiviServizi);
    return configurazioneDispositiviServiziRepository.save(configurazioneDispositiviServizi);
  }

  /**
   * Get all the configurazioneDispositiviServizis.
   *
   * @param pageable
   *          the pagination information
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public Page<ConfigurazioneDispositiviServizi> findAll(Pageable pageable) {
    log.debug("Request to get all ConfigurazioneDispositiviServizis");
    return configurazioneDispositiviServiziRepository.findAll(pageable);
  }

  /**
   * Get one configurazioneDispositiviServizi by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Override
  @Transactional(readOnly = true)
  public ConfigurazioneDispositiviServizi findOne(Long id) {
    log.debug("Request to get ConfigurazioneDispositiviServizi : {}", id);
    return configurazioneDispositiviServiziRepository.findOne(id);
  }

  /**
   * Delete the configurazioneDispositiviServizi by id.
   *
   * @param id
   *          the id of the entity
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete ConfigurazioneDispositiviServizi : {}", id);
    configurazioneDispositiviServiziRepository.delete(id);
  }

  @Override
  public Optional<ConfigurazioneDispositiviServizi> findConfigurazione(TipoDispositivoEnum tipoDispositivo, String servizio) {
    return configurazioneDispositiviServiziRepository.findConfigurazione(tipoDispositivo, servizio);
  }

  @Override
  public Optional<ConfigurazioneDispositiviServizi> findConfigurazione(TipoDispositivoEnum tipoDispositivo, String servizio,
                                                                       ModalitaGestione modalitaGestione) {
    return configurazioneDispositiviServiziRepository.findConfigurazione(tipoDispositivo, servizio, modalitaGestione);
  }

  @Override
  public Optional<ConfigurazioneDispositiviServizi> findConfigurazioneDaRemoto(TipoDispositivoEnum tipoDispositivo, String servizio) {
    log.trace("Find configurazione {} for device type {} and service {}", ModalitaGestione.DA_REMOTO, tipoDispositivo, servizio);
    Optional<ConfigurazioneDispositiviServizi> configDeviceServiceOpt = findConfigurazione(tipoDispositivo, servizio,
                                                                                           ModalitaGestione.DA_REMOTO);

    log.info("Configuration by REMOTE manage: {} for device type: {} and service: {}", configDeviceServiceOpt, tipoDispositivo, servizio);
    return configDeviceServiceOpt;
  }

  @Override
  public List<ConfigurazioneDispositiviServizi> findRemoteConfigurations(TipoDispositivoEnum deviceType) {
    log.trace("Find configurations for device type {} ", deviceType);
    List<ConfigurazioneDispositiviServizi> configs = configurazioneDispositiviServiziRepository.findAll()
                                                                                               .stream()
                                                                                               .filter(cds -> cds.getTipoDispositivo() != null
                                                                                                              && cds.getTipoDispositivo()
                                                                                                                    .getNome()
                                                                                                                    .equals(deviceType))
                                                                                               .filter(conf -> conf.getModalitaGestione()
                                                                                                                   .equals(ModalitaGestione.DA_REMOTO))
                                                                                               .collect(toList());

    log.debug("Configurations Device-Service: {}", configs);
    return configs;
  }

  @Override
  public boolean isGestibileDaRemoto(TipoDispositivoEnum tipoDispositivo, String servizio) {
    Optional<ConfigurazioneDispositiviServizi> configDeviceService = findConfigurazioneDaRemoto(tipoDispositivo, servizio);
    if (configDeviceService.isPresent()) {
      return true;
    }

    log.info("Servizio {} not config to remote manage for tipo dispositivo {}", servizio, tipoDispositivo);
    return false;
  }

  @Override
  public boolean isGestibiliDaRemoto(TipoDispositivoEnum tipoDispositivo, List<String> servizi) {
    Set<Boolean> isGestibiliDaRemoto = servizi.stream()
                                              .map(service -> isGestibileDaRemoto(tipoDispositivo, service))
                                              .collect(toSet());

    boolean isNotAllGestibiliDaRemoto = isGestibiliDaRemoto.stream()
                                                           .filter(b -> b == false)
                                                           .findFirst()
                                                           .isPresent();
    log.info("Is all service [{}] for device type {} remote manage: {}", servizi, tipoDispositivo, !isNotAllGestibiliDaRemoto);
    return !isNotAllGestibiliDaRemoto;
  }

  @Override
  public Optional<ConfigurazioneDispositiviServizi> findConfigurazioneOffline(TipoDispositivoEnum tipoDispositivo, String servizio) {
    log.trace("Find configurazione {} for device type {} and service {}", ModalitaGestione.OFFLINE, tipoDispositivo, servizio);
    Optional<ConfigurazioneDispositiviServizi> configDeviceServiceOpt = findConfigurazione(tipoDispositivo, servizio,
                                                                                           ModalitaGestione.OFFLINE);

    log.info("Configuration by OFFLINE manage: {} for device type: {} and service: {}", configDeviceServiceOpt, tipoDispositivo, servizio);
    return configDeviceServiceOpt;
  }

  @Override
  public boolean isGestibileOffline(TipoDispositivoEnum tipoDispositivo, String servizio) {
    Optional<ConfigurazioneDispositiviServizi> configDeviceService = findConfigurazioneOffline(tipoDispositivo, servizio);
    if (configDeviceService.isPresent()) {
      return true;
    }

    log.info("Servizio {} not config to managed OFFLINE for tipo dispositivo {}", servizio, tipoDispositivo);
    return false;
  }

  @Override
  public boolean isGestibiliOffline(TipoDispositivoEnum tipoDispositivo, List<String> servizi) {
    Set<Boolean> isGestibiliOffline = servizi.stream()
                                             .map(service -> isGestibileOffline(tipoDispositivo, service))
                                             .collect(toSet());

    boolean isNotAllGestibiliOffline = isGestibiliOffline.stream()
                                                         .filter(b -> b == false)
                                                         .findFirst()
                                                         .isPresent();
    log.info("Is all service [{}] for device type {} manage OFFLINE: {}", servizi, tipoDispositivo, !isNotAllGestibiliOffline);
    return !isNotAllGestibiliOffline;
  }

  @Override
  public boolean isGestibili(TipoDispositivoEnum nomeTipoDispositivo, List<String> servizi) {
    boolean isGestibiliDaRemoto = isGestibiliDaRemoto(nomeTipoDispositivo, servizi);
    boolean isGestibiliOffline = isGestibiliOffline(nomeTipoDispositivo, servizi);

    boolean result = (isGestibiliOffline || isGestibiliDaRemoto);
    log.info("Gestibili da Remoto: {} - Gestibili Offline: {} => total result {}", isGestibiliDaRemoto, isGestibiliOffline, result);
    return result;
  }
}
