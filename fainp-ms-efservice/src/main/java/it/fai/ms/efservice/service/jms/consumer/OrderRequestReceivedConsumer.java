package it.fai.ms.efservice.service.jms.consumer;

import static it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent.ACCETTATO;
import static it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent.MS_ORDRI_RISIM;
import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.efservice.message.orderrequest.OrderRequestReceivedMessage;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestDevice;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmInoltroTelepassEuropeo;

@Service
@Transactional
public class OrderRequestReceivedConsumer {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DispositivoRepositoryExt  deviceRepository;
  private final FsmDispositivo            fsmDevice;
  private final FsmInoltroTelepassEuropeo fsmInoltro;
  private final FsmRichiestaUtils         fsmUtils;

  private final RichiestaRepositoryExt    richiestaRepository;

  public OrderRequestReceivedConsumer(final RichiestaRepositoryExt _richiestaRepository, final FsmInoltroTelepassEuropeo _fsmInoltro,
                                      final DispositivoRepositoryExt _deviceRepository, final FsmDispositivo _fsmDevice,FsmRichiestaUtils         fsmUtils) {
    super();
    richiestaRepository = _richiestaRepository;
    fsmInoltro = _fsmInoltro;
    deviceRepository = _deviceRepository;
    fsmDevice = _fsmDevice;
    this.fsmUtils = fsmUtils;
  }

  public void consume(final OrderRequestReceivedMessage _orderReceivedMessage) {
    if (!_orderReceivedMessage.getDevices()
                              .isEmpty()) {
      final List<Dispositivo> devices = executeStateMachine(_orderReceivedMessage.getDevices());
      updateModel(devices);
      final Optional<Richiesta> order = executeStateMachine(_orderReceivedMessage.getOrderRequestUuid(),_orderReceivedMessage.getNote());
      updateModel(order);
    } else {
      _log.info("OrderRequestReceivedMessage not processed (empty devices list for {})", _orderReceivedMessage.getOrderRequestUuid());
    }
  }

  private Optional<Richiesta> executeStateMachine(final OrderRequestUuid _order,String note) {
    Richiesta order = null;
    final Optional<Richiesta> optionalOrder = findOrder(_order);
    if (optionalOrder.isPresent()) {
      if (fsmInoltro.isAvailableCommand(MS_ORDRI_RISIM, optionalOrder.get())) {
        try {
          fsmUtils.changeStatusToRichiesta(optionalOrder.get(), MS_ORDRI_RISIM.name(), note,null );
          fsmUtils.setOperazioniPossibiliOnRichiesta(optionalOrder.get().getIdentificativo());
//          order = fsmInoltro.executeCommandToChangeState(MS_ORDRI_RISIM, optionalOrder.get(),note);
        } catch (final Exception _e) {
          _log.warn("FinalStateMachine error {}", _e);
        }
      } else {
        _log.warn("FinalStateMachine Event {} not available for order request uuid {}", MS_ORDRI_RISIM, optionalOrder.get()
                                                                                                                     .getIdentificativo());
      }
    }
    return Optional.ofNullable(order);
  }

  private List<Dispositivo> executeStateMachine(final Set<OrderRequestDevice> _orderReceivedDevices) {
    List<Dispositivo> devices = null;
    if (_orderReceivedDevices.isEmpty()) {
      _log.warn("No devices to process");
      devices = Collections.emptyList();
    } else {
      devices = _orderReceivedDevices.stream()
                                     .filter(OrderRequestDevice::isActive)
                                     .map(orderReceivedDevice -> findDevice(orderReceivedDevice))
                                     .filter(Optional<Dispositivo>::isPresent)
                                     .map(optionalDevice -> optionalDevice.get())
                                     .filter(device -> {
                                       _log.info("Device {} [status {}]", device.getIdentificativo(), device.getStato());
                                       return device.getStato()
                                                    .equals(StatoDispositivo.INIZIALE)||
                                              device.getStato()
                                                    .equals(StatoDispositivo.INOLTRATO_AL_FORNITORE);
                                     })
                                     .map(device -> executeStateMachineForSingleDevice(device))
                                     .filter(Optional<Dispositivo>::isPresent)
                                     .map(Optional<Dispositivo>::get)
                                     .collect(toList());
    }
    return devices;
  }

  private Optional<Dispositivo> executeStateMachineForSingleDevice(final Dispositivo _device) {
    Dispositivo device = null;
    if (fsmDevice.isAvailableCommand(ACCETTATO, _device)) {
      try {
        device = fsmDevice.executeCommandToChangeState(ACCETTATO, _device);
      } catch (final Exception _e) {
        _log.warn("FinalStateMachine error {}", _e);
      }
    } else {
      _log.warn("FinalStateMachine Event {} not available for device {}", ACCETTATO, _device.getIdentificativo());
    }
    return Optional.ofNullable(device);
  }

  private Optional<Dispositivo> findDevice(final OrderRequestDevice _orderDevice) {
    final Dispositivo device = deviceRepository.findOneByIdentificativo(_orderDevice.getDeviceUuid()
                                                                                    .getUuid());
    final Optional<Dispositivo> ofNullable = Optional.ofNullable(device);
    if (!ofNullable.isPresent()) {
      _log.warn("Device not found for {}", _orderDevice);
    }
    return ofNullable;
  }

  private Optional<Richiesta> findOrder(final OrderRequestUuid _orderUuid) {
    final Richiesta richiesta = richiestaRepository.findOneByIdentificativo(_orderUuid.getUuid());
    final Optional<Richiesta> ofNullable = Optional.ofNullable(richiesta);
    if (!ofNullable.isPresent()) {
      _log.warn("Richiesta not found for {}", _orderUuid);
    }
    return ofNullable;
  }

  private void updateModel(final List<Dispositivo> devices) {
    final List<Dispositivo> savedDevices = devices.stream()
                                                  .map(device -> deviceRepository.save(device))
                                                  .collect(toList());
    savedDevices.forEach(savedDevice -> _log.info("Saved {}", savedDevice));
  }

  private void updateModel(final Optional<Richiesta> _order) {
    _order.ifPresent(order -> {
      richiestaRepository.save(order);
      _log.info("Saved {}", order);
    });
  }

}
