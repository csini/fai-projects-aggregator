package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.ProduttoreService;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.service.dto.ProduttoreDTO;
import it.fai.ms.efservice.service.mapper.ProduttoreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Produttore.
 */
@Service
@Transactional
public class ProduttoreServiceImpl implements ProduttoreService{

    private final Logger log = LoggerFactory.getLogger(ProduttoreServiceImpl.class);

    private final ProduttoreRepository produttoreRepository;

    private final ProduttoreMapper produttoreMapper;

    public ProduttoreServiceImpl(ProduttoreRepository produttoreRepository, ProduttoreMapper produttoreMapper) {
        this.produttoreRepository = produttoreRepository;
        this.produttoreMapper = produttoreMapper;
    }

    /**
     * Save a produttore.
     *
     * @param produttoreDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ProduttoreDTO save(ProduttoreDTO produttoreDTO) {
        log.debug("Request to save Produttore : {}", produttoreDTO);
        Produttore produttore = produttoreMapper.toEntity(produttoreDTO);
        produttore = produttoreRepository.save(produttore);
        return produttoreMapper.toDto(produttore);
    }

    /**
     *  Get all the produttores.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ProduttoreDTO> findAll() {
        log.debug("Request to get all Produttores");
        return produttoreRepository.findAll().stream()
            .map(produttoreMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one produttore by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ProduttoreDTO findOne(Long id) {
        log.debug("Request to get Produttore : {}", id);
        Produttore produttore = produttoreRepository.findOne(id);
        return produttoreMapper.toDto(produttore);
    }

    /**
     *  Delete the  produttore by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Produttore : {}", id);
        produttoreRepository.delete(id);
    }
}
