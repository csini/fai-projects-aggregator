package it.fai.ms.efservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.SpedizioneDispositiviClienteView;
import it.fai.ms.efservice.repository.SpedizioneDispositiviClienteViewRepositoryExt;
import it.fai.ms.efservice.service.dto.SearchSpedizioneDispositiviClienteDTO;
import it.fai.ms.efservice.service.dto.SpedizioneDispositiviClienteViewDTO;
import it.fai.ms.efservice.service.impl.VehicleWizardCacheServiceImplExt;
import it.fai.ms.efservice.service.specifications.SearchSpedizioneDispositiviClienteSpecification;

@Service
@Transactional
public class SpedizioneDispositiviClienteServiceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final SpedizioneDispositiviClienteViewRepositoryExt repositoryExt;
  
  private final VehicleWizardCacheServiceImplExt vehicleWizardCacheService;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public SpedizioneDispositiviClienteServiceExt(SpedizioneDispositiviClienteViewRepositoryExt repositoryExt, final VehicleWizardCacheServiceImplExt vehicleWizardCacheService) {
    this.repositoryExt = repositoryExt;
    this.vehicleWizardCacheService = vehicleWizardCacheService;
  }

  public Page<SpedizioneDispositiviClienteViewDTO> findAllWithFilter(SearchSpedizioneDispositiviClienteDTO dto, Pageable page) {
    Page<SpedizioneDispositiviClienteViewDTO> result = repositoryExt.findAll(new SearchSpedizioneDispositiviClienteSpecification(dto), page)
                                                                    .map(s -> toDto(s));
    return new PageImpl<>(result.getContent(), page, result.getTotalElements());
  }

  private SpedizioneDispositiviClienteViewDTO toDto(SpedizioneDispositiviClienteView spedizioneDispositiviClienteView) {
    SpedizioneDispositiviClienteViewDTO spedizioneDto = new SpedizioneDispositiviClienteViewDTO();
    spedizioneDto.setId(spedizioneDispositiviClienteView.getId());
    spedizioneDto.setCodiceAzienda(spedizioneDispositiviClienteView.getCodiceAzienda());
    spedizioneDto.setRagioneSociale(spedizioneDispositiviClienteView.getRagioneSociale());
    spedizioneDto.setNumeroDispositiviSpedibili(spedizioneDispositiviClienteView.getNumeroDispositiviSpedibili());
    return spedizioneDto;
  }

}
