package it.fai.ms.efservice.service.report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.utils.report.CsvSerializer;
import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.ClienteFai_;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Contratto_;
import it.fai.ms.efservice.domain.Produttore_;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.report.dto.AnomalieContrattiAttiviDTO;

@Service
public class ReportAnomalieContrattiService {

  private final ContrattoRepositoryExt repo;
  private final DocumentClient documentClient;
  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public ReportAnomalieContrattiService(ContrattoRepositoryExt repo,DocumentClient documentClient) {
    this.repo = repo;
    this.documentClient = documentClient;

  }

  public Page<AnomalieContrattiAttiviDTO> reportContrattiAttiviCondispositiviBloccati(Pageable pageable) {
    pageable = convertSort(pageable);
    return repo.reportContrattiAttiviConTuttiDispositiviBloccati(StatoDispositivo.getBlueState(), pageable)
      .map(this::toDto);
  }

  private Pageable convertSort(Pageable pageable) {
    if(pageable == null )return null;
    List<Order> orders = new ArrayList<>();

    Sort sort = pageable.getSort();
    if (sort != null) {
      sort.forEach((Consumer<? super Order>) o -> {
        Order orderOut = new Order(o.getDirection(), translateProperty(o.getProperty()), o.getNullHandling());
        if (o.isIgnoreCase())
          orderOut = orderOut.ignoreCase();

        orders.add(orderOut);
      });
    }
    Sort sortOut = null;
    if(!orders.isEmpty())
      sortOut = new Sort(orders);
    return new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sortOut);
  }

  private String translateProperty(String property) {
    switch (property) {
      case "contrattoNumero":
        return Contratto_.codContrattoCliente.getName();
      case "produttoreNome":
        return Contratto_.produttore.getName() + "." + Produttore_.nome.getName();
      case "codiceAzienda":
        return Contratto_.clienteFai.getName() + "." + ClienteFai_.codiceCliente.getName();
      case "ragioneSociale":
        return Contratto_.clienteFai.getName() + "." + ClienteFai_.ragioneSociale.getName();
      case "nazioneAzienda":
        return Contratto_.clienteFai.getName() + "." + ClienteFai_.paese.getName();
      default:
        throw new IllegalArgumentException();
    }

  }

  private AnomalieContrattiAttiviDTO toDto(Contratto contratto) {
    AnomalieContrattiAttiviDTO dto = new AnomalieContrattiAttiviDTO();
    dto.setContrattoNumero(contratto.getCodContrattoCliente());
    dto.setProduttoreNome(contratto.getProduttore()
      .getNome());

    ClienteFai clienteFai = contratto.getClienteFai();
    dto.setCodiceAzienda(clienteFai.getCodiceCliente());
    dto.setRagioneSociale(clienteFai.getRagioneSociale());
    dto.setNazioneAzienda(clienteFai.getPaese());

    return dto;
  }

  public AsyncJobResponse reportContrattiAttiviTuttiDispositiviFatturati(String fileName) {
    List<AnomalieContrattiAttiviDTO> reportData = pagedCollect(this::reportContrattiAttiviCondispositiviBloccati);

    StringBuilder out = new StringBuilder();
    CsvSerializer<AnomalieContrattiAttiviDTO> serializer = new CsvSerializer<>(AnomalieContrattiAttiviDTO.class);
    try {
      serializer.serialize(reportData.stream(),out);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    String documentName = documentClient.saveReportCsvFromText(jwt, fileName, out.toString());
    Map<String, String> data = Collections.singletonMap("downloadUri", DocumentClient.API_REPORT_DOWNLOAD + "/" + documentName);
    return new AsyncJobResponse(StateChangeStatusFsm.SUCCESS, data);
  }

  private <T> List<T> pagedCollect(Function<Pageable, Page<T>> p) {
    List<T> dispositivi = Lists.newArrayList();
    Pageable pageable = new PageRequest(0, 100);
    while (pageable != null) {
      Page<T> page = p.apply(pageable);
      dispositivi.addAll(page.getContent());
      pageable = page.hasNext() ? page.nextPageable() : null;
    }
    return dispositivi;
  }

}
