package it.fai.ms.efservice.wizard.web.rest.precondition;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.rules.engine.RulePrecalculator;
import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleDieselParticulateFilter;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

@RestController
@RequestMapping("/api/preconitionwizard")
public class CalculatePreconditionResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Value("${efservice.authorizationHeader}")
  private String authorizationToken;

  private final RulePrecalculator rulePrecalculator;

  private final ClienteFaiRepository clienteFaiRepository;

  private final VehicleClient vehicleClient;

  private final TipoDispositivoRepository deviceTypeRepo;

  private final TipoServizioRepository serviceTypeRepo;

  public CalculatePreconditionResource(final RulePrecalculator rulePrecalculator, final ClienteFaiRepository clienteFaiRepository,
                                       final VehicleClient vehicleClient, final TipoDispositivoRepository deviceTypeRepo,
                                       final TipoServizioRepository serviceTypeRepo) {
    this.rulePrecalculator = rulePrecalculator;
    this.clienteFaiRepository = clienteFaiRepository;
    this.vehicleClient = vehicleClient;
    this.deviceTypeRepo = deviceTypeRepo;
    this.serviceTypeRepo = serviceTypeRepo;
  }

  @Async
  @PostMapping("/calculate/all")
  public ResponseEntity<Void> calculateAllPreconditionRule(@RequestParam(value = "codiceCliente", required = false) String codiceCliente,
                                                           @RequestParam(
                                                                         value = "tipoDispositivo",
                                                                         required = false) TipoDispositivoEnum deviceType,
                                                           @RequestParam(value = "tipoServizio", required = false) String serviceType,
                                                           Pageable pageeable) {
    final long tStart = System.currentTimeMillis();
    log.info("Start calculate precondition to ALL client: {}", codiceCliente);
    List<String> clientsCode = new ArrayList<>();
    if (StringUtils.isBlank(codiceCliente)) {
      if (pageeable == null) {
        clientsCode = clienteFaiRepository.findAll()
                                          .parallelStream()
                                          .map(cf -> cf.getCodiceCliente())
                                          .collect(toList());
      } else {
        Page<ClienteFai> findAll = clienteFaiRepository.findAll(pageeable);
        if (findAll != null) {
          clientsCode = findAll.getContent()
                               .parallelStream()
                               .map(cf -> cf.getCodiceCliente())
                               .collect(toList());
        }
      }
    } else {
      clientsCode.add(codiceCliente);
    }

    clientsCode.parallelStream()
               .map(clientCode -> {
                 calculatePreconditionRule(clientCode, deviceType, serviceType);
                 return true;
               })
               .findFirst();

    log.info("Finish in {} ms", System.currentTimeMillis() - tStart);
    return ResponseEntity.ok()
                         .build();
  }

  @Async
  @PostMapping("/calculate")
  public ResponseEntity<Void> calculatePreconditionRule(@RequestParam(value = "codiceCliente", required = true) String codiceCliente,
                                                        @RequestParam(
                                                                      value = "tipoDispositivo",
                                                                      required = false) TipoDispositivoEnum deviceType,
                                                        @RequestParam(value = "tipoServizio", required = false) String serviceType) {
    final long tStart = System.currentTimeMillis();
    log.info("Start calculate precondition to client: {}", codiceCliente);
    List<VehicleDTO> vehiclesClient = vehicleClient.findAllByCodiceAzienda(authorizationToken, codiceCliente);
    if (!vehiclesClient.isEmpty()) {
      vehiclesClient.forEach(vehicle -> {
        RuleEngineVehicle _vehicleRuleContext = mapVehicleToRuleContext(vehicle);
        process(_vehicleRuleContext, deviceType, serviceType);
      });
    }

    log.info("Finish in {} ms", System.currentTimeMillis() - tStart);
    return ResponseEntity.ok()
                         .build();
  }

  private void process(RuleEngineVehicle _vehicleRuleContext, TipoDispositivoEnum deviceType, String serviceType) {
    List<TipoDispositivoEnum> deviceTypes = getDeviceType(deviceType);
    deviceTypes.forEach(device -> {
      List<String> serviceTypes = new ArrayList<>();
      if (StringUtils.isBlank(serviceType)) {
        TipoDispositivo tipoDispositivo = deviceTypeRepo.findOneByNome(device);
        serviceTypes.addAll(tipoDispositivo.getTipoServizios()
                                           .stream()
                                           .map(s -> s.getNome())
                                           .collect(toList()));
      } else {
        serviceTypes.add(serviceType);
      }
      serviceTypes.forEach(service -> {
        RuleContext ruleContext = createRuleContext(_vehicleRuleContext, device, service);
        log.info("Process RuleContext: {}", ruleContext);
        rulePrecalculator.process(ruleContext, getRuleBucket(device));
      });
    });

    RuleContext ruleContext = createRuleContext(_vehicleRuleContext);
    log.info("Process RuleContext: {}", ruleContext);
    rulePrecalculator.process(ruleContext, RuleBucket.ALL_DEVICE);
  }

  private RuleContext createRuleContext(RuleEngineVehicle _vehicleRuleContext) {
    RuleEngineDeviceType _deviceTypeRuleContext = new RuleEngineDeviceType(RuleEngineDeviceTypeId.ofWildcard());
    RuleEngineServiceType _serviceTypeRuleContext = new RuleEngineServiceType(RuleEngineServiceTypeId.ofWildcard());
    RuleContext ruleContext = new RuleContext(_serviceTypeRuleContext, _deviceTypeRuleContext, _vehicleRuleContext);
    return ruleContext;
  }

  private RuleBucket getRuleBucket(TipoDispositivoEnum device) {
    Optional<RuleBucket> optBucket = RuleBucket.getRuleBucket(device.name());
    return optBucket.isPresent() ? optBucket.get() : RuleBucket.ALL_DEVICE;
  }

  private RuleContext createRuleContext(RuleEngineVehicle _vehicleRuleContext, TipoDispositivoEnum deviceType, String serviceType) {
    RuleEngineDeviceType _deviceTypeRuleContext = new RuleEngineDeviceType(new RuleEngineDeviceTypeId(deviceType.name()));
    RuleEngineServiceType _serviceTypeRuleContext = new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceType));
    RuleContext ruleContext = new RuleContext(_serviceTypeRuleContext, _deviceTypeRuleContext, _vehicleRuleContext);
    return ruleContext;
  }

  private List<String> getServiceType(String serviceType) {
    List<String> serviceTypes = new ArrayList<>();
    if (StringUtils.isBlank(serviceType)) {
      serviceTypes.addAll(serviceTypeRepo.findAll()
                                         .stream()
                                         .map(ts -> ts.getNome())
                                         .collect(toList()));
    } else {
      serviceTypes.add(serviceType);
    }
    log.debug("ServiceTypes found: {}", serviceTypes);
    return serviceTypes;
  }

  private List<TipoDispositivoEnum> getDeviceType(TipoDispositivoEnum deviceType) {
    List<TipoDispositivoEnum> deviceTypes = new ArrayList<>();
    if (deviceType == null) {
      deviceTypes.addAll(deviceTypeRepo.findAll()
                                       .stream()
                                       .map(td -> td.getNome())
                                       .collect(toList()));
    } else {
      deviceTypes.add(deviceType);
    }
    log.debug("DeviceTypes found: {}", deviceTypes);
    return deviceTypes;
  }

  private Integer parseStringToInteger(String str) {
    Integer integer = null;
    if (StringUtils.isNotBlank(str)) {
      integer = Integer.parseInt(str);
    }

    return integer;
  }

  private RuleEngineVehicle mapVehicleToRuleContext(VehicleDTO vehicle) {
    RuleEngineVehicle ruleEngineVehicle = new RuleEngineVehicle(new RuleEngineVehicleId(vehicle.getIdentificativo()));
    ruleEngineVehicle.setAxes(vehicle.getNumeroAssi());
    ruleEngineVehicle.setCategory(vehicle.getCategoria());
    ruleEngineVehicle.setCountry(vehicle.getNazione());
    Integer filterClassDiesel = null;
    if (StringUtils.isNotBlank(vehicle.getClasseFiltroAntiparticolato())) {
      filterClassDiesel = parseStringToInteger(vehicle.getClasseFiltroAntiparticolato());
    }
    ruleEngineVehicle.setDieselParticulateFilter(new RuleEngineVehicleDieselParticulateFilter(filterClassDiesel));
    ruleEngineVehicle.setEuroClass(vehicle.getEuro());
    ruleEngineVehicle.setLicensePlate(vehicle.getTarga());
    ruleEngineVehicle.getWeight()
                     .setLoneVehicleGrossWeight(vehicle.getMassa());
    ruleEngineVehicle.getWeight()
                     .setLoneVehicleTareWeight(vehicle.getTara());
    ruleEngineVehicle.getWeight()
                     .setTrainsetGrossWeight(vehicle.getMassaConvoglio());
    ruleEngineVehicle.setType(vehicle.getTipo());
    ruleEngineVehicle.setMake(vehicle.getMarca() != null ? vehicle.getMarca()
                                                                  .getNome()
                                                         : null);
    ruleEngineVehicle.setFuelType(vehicle.getAlimentazione());
    ruleEngineVehicle.setLength(vehicle.getLunghezza());
    ruleEngineVehicle.setHeight(vehicle.getAltezza());
    ruleEngineVehicle.setPayload(vehicle.getPortata());
    ruleEngineVehicle.setWidth(vehicle.getLarghezza());
    ruleEngineVehicle.setNumeroAssiPienoCarico(vehicle.getNumeroAssiPienoCarico());
    ruleEngineVehicle.setNumberChassis(vehicle.getNumeroTelaio());
    ruleEngineVehicle.setColor(vehicle.getColore());
    ruleEngineVehicle.setModel(vehicle.getModello());
    log.debug("RuleEngineVehicle: {}", ruleEngineVehicle);
    return ruleEngineVehicle;
  }

}
