package it.fai.ms.efservice.service.fsm.guard;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardSuccessVerificaViaCard implements Guard<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(GuardSuccessVerificaViaCard.class);

  private final DispositivoRepositoryExt deviceRepositoryExt;

  public GuardSuccessVerificaViaCard(DispositivoRepositoryExt _deviceRepositoryExt) {
    deviceRepositoryExt = _deviceRepositoryExt;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean success = false;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        String codiceCliente = richiesta.getOrdineCliente()
                                        .getClienteAssegnatario()
                                        .getCodiceCliente();

        log.info("Codice Cliente {} for RIchiesta [ID: {} - Identificativo: {}]", codiceCliente, richiesta.getId(),
                 richiesta.getIdentificativo());
        Set<Dispositivo> viaCardDevice = deviceRepositoryExt.findByCodiceClienteAndTipoDispositivo(codiceCliente,
                                                                                                   TipoDispositivoEnum.VIACARD);
        int numViaCard = filterDeviceByStatoNotIs(viaCardDevice, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE).size();
        Set<Dispositivo> telepassItaDevice = deviceRepositoryExt.findByCodiceClienteAndTipoDispositivo(codiceCliente,
                                                                                                       TipoDispositivoEnum.TELEPASS_ITALIANO);
        int numTelepassIta = filterDeviceByStatoNotIs(telepassItaDevice, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE).size();

        int diffViaCardTelepassIta = numViaCard - numTelepassIta;
        log.info("Differenza tra num ViaCard {} e num Telepass ITA {} = {}", numViaCard, numTelepassIta, diffViaCardTelepassIta);
        
        boolean isAllZero = (numViaCard == 0 && numTelepassIta == 0);
        if (!isAllZero && diffViaCardTelepassIta >= 0) {
          log.info("La differenza risulta essere maggiore di 0, procedo con l'invio della richiesta a TELEPASS...");
          success = true;
        }
      }
    }

    return success;
  }

  private Set<Dispositivo> filterDeviceByStatoNotIs(Set<Dispositivo> devices, StatoDispositivo stato) {
    Set<Dispositivo> devicesFiltered = devices.stream()
                                              .filter(d -> d.getStato() != stato)
                                              .collect(toSet());
    if (devicesFiltered == null) {
      // To manage value null for size
      devicesFiltered = new HashSet<>();
    }
    log.debug("devices filtered by status no is {}: {}", stato, devicesFiltered);
    return devicesFiltered;
  }

}
