package it.fai.ms.efservice.service.impl;

import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.service.DeliveryDevicesAsyncServiceExt;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;
import it.fai.ms.efservice.web.rest.errors.Errno;

@Service
public class DeliveryDevicesAsyncServiceExtImpl implements DeliveryDevicesAsyncServiceExt {
  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AsyncJobServiceImpl asyncJobService;
  private final DeliveryDevicesService     deliveryDevicesService;

  public DeliveryDevicesAsyncServiceExtImpl(AsyncJobServiceImpl asyncJobService, DeliveryDevicesService     deliveryDevicesService) {
    this.asyncJobService = asyncJobService;
    this.deliveryDevicesService = deliveryDevicesService;
  }

  @Override
  public String getKeyCacheAndPushDefaultValue(String key) {
    return asyncJobService.generateKeyCacheAndPushValue(key, new AsyncJobResponse(StateChangeStatusFsm.PENDING, null));
  }

  @Override
  public void processAsyncDeliveryDevices(String identifierJob, DeliveryPrintDTO dto) {
    log.debug("Call async devices processing print deliveries: dto {}", dto);

    CompletableFuture.supplyAsync(() -> deliveryDevicesService.processDevices(dto))
      .exceptionally(e -> {
        log.error("Exception in Devices Delivery Resource method processAsyncDeliveryDevices: ", e);
        AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, null);
        response.add(Errno.EXCEPTION_ON_PROCESSING_PRINT);
        return response;
      })
      .thenAccept(state -> asyncJobService.save(identifierJob, state));
  }
}
