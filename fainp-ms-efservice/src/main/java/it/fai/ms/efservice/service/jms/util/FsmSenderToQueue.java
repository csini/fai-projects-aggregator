package it.fai.ms.efservice.service.jms.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.dto.DispositivoFSMChangeStatusDTO;
import it.fai.ms.common.jms.dto.telepass.TipoEventoOutbound;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.bean.telepass.TelepassEuEventiOutboundDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

public class FsmSenderToQueue {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final JmsTopicSenderService senderJmsService;

  public FsmSenderToQueue(JmsTopicSenderService _senderJmsService) {
    senderJmsService = _senderJmsService;
  }

  public JmsTopicSenderService getSenderJmsService() {
    return this.senderJmsService;
  }

  public boolean sendMessageTo(Richiesta richiesta) {
    TipoEventoOutbound tipoEventoOutBound = null;
    String identificativoRichiesta = richiesta.getIdentificativo();

    OrdineCliente ordineCliente = richiesta.getOrdineCliente();
    if (ordineCliente == null) {
      String msg = "Exception OrdineCliente related of richiesta " + identificativoRichiesta + " is NULL";
      log.error(msg);
      throw new RuntimeException(msg);
    }
    String identificativoOrdine = ordineCliente.getIdentificativo();

    String codiceClienteFai = null;
    TipoRichiesta tipo = richiesta.getTipo();
    if (tipo != null) {
      switch (tipo) {
      case NUOVO_ORDINE:
      case NUOVO_DISPOSITIVO:
        tipoEventoOutBound = getTipoEventoOutBoundByCheckContractRichiesta(richiesta);
        if (tipoEventoOutBound == TipoEventoOutbound.INOLTRO_RICHIEDI_CONTRATTO) {
          ClienteFai clienteAssegnatario = ordineCliente.getClienteAssegnatario();
          if (clienteAssegnatario != null) {
            codiceClienteFai = clienteAssegnatario.getCodiceCliente();
          } else {
            log.info("Reload cliente fai for Ordine Cliente [Identificativo: {}] before send message in queue", identificativoOrdine);
          }
        }
        break;
      default:
        log.warn("TIPO {} is not managed", tipo);
        break;
      }

      if (tipoEventoOutBound != null) {
        TelepassEuEventiOutboundDTO telepassEuEventiOutDTO = new TelepassEuEventiOutboundDTO(tipoEventoOutBound, identificativoRichiesta,
                                                                                             identificativoOrdine, codiceClienteFai);
        try {
          senderJmsService.publishTelepassEuEventiOutboundDTO(telepassEuEventiOutDTO);
        } catch (Exception e) {
          log.error("Exception JMS Service publish", e);
          throw new RuntimeException(e);
        }
      } else {
        log.warn("Not set Tipo Evento OutBound to create DTO.");
      }
    } else {
      log.warn("Not set TipoRichiesta");
    }

    return true;
  }

  private TipoEventoOutbound getTipoEventoOutBoundByCheckContractRichiesta(Richiesta richiesta) {
    TipoEventoOutbound tipoEventoOutBound = null;
    Contratto contratto = richiesta.getContratto();
    if (contratto != null) {
      String codContrattoCliente = contratto.getCodContrattoCliente();
      log.info("Codice contratto: {}", codContrattoCliente);
      if (StringUtils.isBlank(codContrattoCliente)) {
        tipoEventoOutBound = TipoEventoOutbound.INOLTRO_RICHIEDI_CONTRATTO;
      } else {
        tipoEventoOutBound = TipoEventoOutbound.INOLTRO_RICHIEDI_DISPOSITIVI;
      }
    } else {
      String msg = "The Richiesta " + richiesta + " has not contract";
      log.error(msg);
      throw new RuntimeException(msg);
    }
    return tipoEventoOutBound;
  }

  public void sendMessageForChangeStatoDispositivo(Dispositivo dispositivo, DispositivoEvent event) {
    try {
      log.info("FsmSenderToQueue::sendMessageForChangeStatoDispositivo {}","BEGIN");
      String identificativo = dispositivo.getIdentificativo();
      if (log.isDebugEnabled()) {
        log.debug("Send message to switch state for dispositivo: " + identificativo);
      }
      DispositivoFSMChangeStatusDTO dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(identificativo, event.name());
      try {
        senderJmsService.publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
      } catch (Exception e) {
        log.error("Exception JMS Service publish", e);
        throw new RuntimeException(e);
      }
    }finally{
      log.info("FsmSenderToQueue::sendMessageForChangeStatoDispositivo {}","END");
    }
  }

}
