package it.fai.ms.efservice.rules.enumeration;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

public enum RuleBucket {

  //@formatter:off

  DEVICE_TELEPASS_EU("TELEPASS_EUROPEO"),
  DEVICE_TELEPASS_EU_SAT("TELEPASS_EUROPEO_SAT"),
  DEVICE_TELEPASS_ITA("TELEPASS_ITALIANO"),
  DEVICE_TRACKYCARD("TRACKYCARD"),
  DEVICE_GOBOX("GO_BOX"),
  DEVICE_TOLL_COLLECT("TOLL_COLLECT"),
  DEVICE_FREJUS("TES_TRA_FREJUS_MONTE_BIANCO"),
  DEVICE_TESSERA_CARONTE("TESSERA_CARONTE"),
  DEVICE_DARTFORD_CROSSING("DARTFORD_CROSSING"),
  DEVICE_VIA_TOLL("VIA_TOLL"),
  DEVICE_VISPRO("VISPRO"),
  ALL_DEVICE("ALL_DEVICE")
  ;
  //@formatter:on

  private String deviceType;

  private RuleBucket(final String _deviceType) {
    deviceType = _deviceType;
  }

  public String deviceType() {
    return deviceType;
  }

  public static Set<RuleBucket> getAllDeviceTypes() {
    return Arrays.asList(RuleBucket.values())
                 .stream()
                 .filter(device -> !device.equals(RuleBucket.ALL_DEVICE))
                 .collect(toSet());
  }

  public static Optional<RuleBucket> getRuleBucket(String name) {
    return Arrays.asList(RuleBucket.values())
                 .stream()
                 .filter(device -> device.deviceType()
                                         .equals(name))
                 .findFirst();
  }

}
