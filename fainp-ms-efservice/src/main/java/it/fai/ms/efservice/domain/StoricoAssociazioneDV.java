package it.fai.ms.efservice.domain;

import javax.persistence.*;

import it.fai.ms.efservice.converter.DateInstantConverter;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A StoricoAssociazioneDV.
 */
@Entity
@Table(name = "storico_associazione_dv")
@Deprecated
/*
 * Da cancellare, lo storico è stato spostato in faiconsumi
 */
public class StoricoAssociazioneDV implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "data_inizio")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataInizio;

  @Column(name = "data_fine")
  @Convert(converter = DateInstantConverter.class)
  private Instant dataFine;

  @Column(name = "uuid_veicolo")
  private String uuidVeicolo;

  @ManyToOne
  private Dispositivo dispositivo;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Instant getDataInizio() {
    return dataInizio;
  }

  public StoricoAssociazioneDV dataInizio(Instant dataInizio) {
    this.dataInizio = dataInizio;
    return this;
  }

  public void setDataInizio(Instant dataInizio) {
    this.dataInizio = dataInizio;
  }

  public Instant getDataFine() {
    return dataFine;
  }

  public StoricoAssociazioneDV dataFine(Instant dataFine) {
    this.dataFine = dataFine;
    return this;
  }

  public void setDataFine(Instant dataFine) {
    this.dataFine = dataFine;
  }

  public String getUuidVeicolo() {
    return uuidVeicolo;
  }

  public StoricoAssociazioneDV uuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
    return this;
  }

  public void setUuidVeicolo(String uuidVeicolo) {
    this.uuidVeicolo = uuidVeicolo;
  }

  public Dispositivo getDispositivo() {
    return dispositivo;
  }

  public StoricoAssociazioneDV dispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
    return this;
  }

  public void setDispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
  }
  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoricoAssociazioneDV storicoAssociazioneDV = (StoricoAssociazioneDV) o;
    if (storicoAssociazioneDV.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), storicoAssociazioneDV.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "StoricoAssociazioneDV{" + "id=" + getId() + ", dataInizio='" + getDataInizio() + "'" + ", dataFine='" + getDataFine() + "'"
           + ", uuidVeicolo='" + getUuidVeicolo() + "'" + "}";
  }
}
