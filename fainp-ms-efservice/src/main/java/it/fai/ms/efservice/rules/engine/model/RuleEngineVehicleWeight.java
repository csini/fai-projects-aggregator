package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;

public class RuleEngineVehicleWeight implements Serializable {

  private static final long serialVersionUID = -5784140327042950424L;

  private Integer loneVehicleGrossWeight;
  private Integer loneVehicleTareWeight;
  private Integer trainsetGrossWeight;

  public RuleEngineVehicleWeight() {
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((RuleEngineVehicleWeight) _obj).getLoneVehicleGrossWeight(), loneVehicleGrossWeight)
                 && Objects.equals(((RuleEngineVehicleWeight) _obj).getLoneVehicleTareWeight(), loneVehicleTareWeight)
                 && Objects.equals(((RuleEngineVehicleWeight) _obj).getTrainsetGrossWeight(), trainsetGrossWeight);
    }
    return isEquals;
  }

  public Integer getLoneVehicleGrossWeight() {
    return loneVehicleGrossWeight;
  }

  public Integer getLoneVehicleTareWeight() {
    return loneVehicleTareWeight;
  }

  public Integer getTrainsetGrossWeight() {
    return trainsetGrossWeight;
  }

  @Override
  public int hashCode() {
    return Objects.hash(loneVehicleGrossWeight, loneVehicleTareWeight, trainsetGrossWeight);
  }

  public void setLoneVehicleGrossWeight(final Integer _loneVehicleGrossWeight) {
    loneVehicleGrossWeight = _loneVehicleGrossWeight;
  }

  public void setLoneVehicleTareWeight(final Integer _loneVehicleTareWeight) {
    loneVehicleTareWeight = _loneVehicleTareWeight;
  }

  public void setTrainsetGrossWeight(final Integer _trainsetGrossWeight) {
    trainsetGrossWeight = _trainsetGrossWeight;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleEngineVehicleWeight [loneVehicleGrossWeight=");
    builder.append(loneVehicleGrossWeight);
    builder.append(", loneVehicleTareWeight=");
    builder.append(loneVehicleTareWeight);
    builder.append(", trainsetGrossWeight=");
    builder.append(trainsetGrossWeight);
    builder.append("]");
    return builder.toString();
  }

}
