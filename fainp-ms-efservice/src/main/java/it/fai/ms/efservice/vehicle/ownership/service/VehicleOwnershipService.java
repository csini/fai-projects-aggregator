package it.fai.ms.efservice.vehicle.ownership.service;

import java.util.Set;

import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnership;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipCompanyCode;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;

public interface VehicleOwnershipService {

  Set<VehicleOwnership> getEnabledVehicles(VehicleOwnershipCompanyCode vehicleOwnershipCompanyCode, VehicleOwnershipId vehicleOwnershipId);

Set<VehicleOwnership> getEnabledVehicles(VehicleOwnershipCompanyCode _vehicleOwnershipCompanyCode,
		VehicleOwnershipId _vehicleOwnershipId, String _deviceType);

}
