package it.fai.ms.efservice.service.fsm.guard;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import org.springframework.statemachine.StateContext;

import java.util.List;


public class GuardDocumentsTollCollectMissing extends GuardDocumentsTollCollectPresent {



  public GuardDocumentsTollCollectMissing(DocumentService documentService) {
    super(documentService);
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean result = super.evaluate(context);
    return !result;
  }

}
