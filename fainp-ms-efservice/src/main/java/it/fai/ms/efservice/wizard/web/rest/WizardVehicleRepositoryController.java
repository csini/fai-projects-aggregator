package it.fai.ms.efservice.wizard.web.rest;

import java.util.Set;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.repository.WizardVehicleRepository;
import it.fai.ms.efservice.wizard.web.rest.vm.ResourceNotFoundException;

@RestController
@RequestMapping("/api/wizard/repository")
public class WizardVehicleRepositoryController {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private WizardVehicleRepository wizardVehicleRepository;

  public WizardVehicleRepositoryController(final WizardVehicleRepository _wizardVehicleRepository) {
    wizardVehicleRepository = _wizardVehicleRepository;
  }

  @GetMapping("keys/count")
  public ResponseEntity<Integer> countKeys() {
    _log.info("Getting WizardVehicles count");
    final Integer count = wizardVehicleRepository.count();
    _log.info("WizardVehicles count found: {}", count);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(count);
  }

  @GetMapping("key/{id}")
  public ResponseEntity<WizardVehicle> getVehicle(final @PathVariable(name = "id") @NotNull String _id) {
    _log.info("Getting WizardVehicle with id {}", _id);
    final WizardVehicle wizardVehicle = wizardVehicleRepository.find(_id)
                                                            .orElseThrow((ResourceNotFoundException::new));
    _log.info("WizardVehicle found: {}", wizardVehicle);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noCache())
                         .body(wizardVehicle);
  }

}
