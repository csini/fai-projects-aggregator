package it.fai.ms.efservice.service.impl;

import static it.fai.ms.efservice.domain.enumeration.StatoRichiesta.ATTESA_RISPOSTA;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;

@Service
@Transactional
public class OrderRequestServiceImpl implements OrderRequestService {

  private Logger _log = LoggerFactory.getLogger(getClass());

  private final OrdineClienteRepositoryExt companyOrderRepository;
  private final RichiestaServiceExt        richiestaServiceExt;

  @Autowired
  public OrderRequestServiceImpl(final OrdineClienteRepositoryExt _companyOrderRepository, final RichiestaServiceExt _richiestaServiceExt) {
    companyOrderRepository = _companyOrderRepository;
    richiestaServiceExt = _richiestaServiceExt;
  }

  @Override
  public Richiesta getOrderRequestsByCompanyCodeAndVehiclePlateNumber(final String _contractUuid, final String _licensePlate,
                                                                      final String _licensePlateCountry) {
    _log.info("Searching OrderRequests with a contract for contract {} and plate {}", _contractUuid,
              _licensePlateCountry + "_" + _licensePlate);

    Richiesta orderRequest = null;
    final List<Richiesta> orderRequests = richiestaServiceExt.findByContrattoAndTargaNazioneAndStato(_contractUuid, _licensePlate,
                                                                                                     _licensePlateCountry,
                                                                                                     StatoRichiesta.ACCETTATO_FORNITORE);
    if (orderRequests != null && !orderRequests.isEmpty()) {
      if (orderRequests.size() == 1) {
        _log.info(" --> Found only one order request for contract {} and plate {}", _contractUuid,
                  _licensePlateCountry + "_" + _licensePlate);
        orderRequest = orderRequests.get(0);
      } else {
        _log.info(" --> Found more than one order request for contract {} and plate {}", _contractUuid,
                  _licensePlateCountry + "_" + _licensePlate);
        _log.info(" --> Filtering by {}", TipoRichiesta.NUOVO_ORDINE);
        final Optional<Richiesta> optionalOrderRequest = orderRequests.stream()
                                                                      .filter(or -> or.getTipo()
                                                                                      .equals(TipoRichiesta.NUOVO_ORDINE))
                                                                      .findFirst();
        if (optionalOrderRequest.isPresent()) {
          _log.info(" --> Found one filtered order request for contract {} and plate {}", _contractUuid,
                    _licensePlateCountry + "_" + _licensePlate);
          orderRequest = optionalOrderRequest.get();
        } else {
          _log.info(" --> No filtered order request for contract {} and plate {}", _contractUuid,
                    _licensePlateCountry + "_" + _licensePlate);
        }
      }
    }

    return orderRequest;
  }

  @Override
  public List<Richiesta> getOrderRequestsWithoutContract(final String _companyCodeUuid) {

    _log.info("Searching OrderRequests without a contract for companyCode {} : ", _companyCodeUuid);

    final List<OrdineCliente> orders = companyOrderRepository.findByIdentificativoCliente(_companyCodeUuid);

    final List<Richiesta> orderRequests = orders.stream()
                                                .map(order -> order.getRichiestas())
                                                .flatMap(orderRequest -> orderRequest.stream())
                                                .collect(toList());

    _log.info("Richieste found: {}", (orderRequests != null) ? orderRequests.size() : "NULL");

    final List<Richiesta> orderRequestsWithoutContract = orderRequests.stream()
                                                                      .filter(orderRequest -> orderRequest.getContratto() != null
                                                                                              && StringUtils.isBlank(orderRequest.getContratto()
                                                                                                                                 .getCodContrattoCliente()))
                                                                      .filter(orderRequest -> orderRequest.getStato()
                                                                                                          .equals(ATTESA_RISPOSTA))
                                                                      .collect(toList());
    _log.info("OrderRequests without a contract (total {}) :", orderRequestsWithoutContract.size());
    orderRequestsWithoutContract.forEach(orderRequest -> _log.info("OrderRequestDecorator wo contract with status {}: {}", ATTESA_RISPOSTA,
                                                                   orderRequest));

    return orderRequestsWithoutContract;
  }

}
