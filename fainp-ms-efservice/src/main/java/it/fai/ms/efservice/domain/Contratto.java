package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.efservice.converter.DateInstantConverter;
import it.fai.ms.efservice.listener.ContrattoEntityListener;

/**
 * A Contratto.
 */
@Entity
@EntityListeners({ ContrattoEntityListener.class, })
@Audited(withModifiedFlag = true, targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
@Table(name = "contratto")
public class Contratto implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotAudited
  @Column(name = "identificativo")
  private String identificativo;

  @Column(name = "cod_contratto_cliente")
  private String codContrattoCliente;

  @NotAudited
  @OneToMany(mappedBy = "contratto")
  @JsonIgnore
  private Set<Dispositivo> dispositivo;

  @ManyToOne
  private Produttore produttore;

  @ManyToOne
  private ClienteFai clienteFai;

  @NotAudited
  @OneToMany(mappedBy = "contratto")
  @JsonIgnore
  private Set<Richiesta> richiestas = new HashSet<>();

  @NotNull
  @NotAudited
  @Enumerated(EnumType.STRING)
  @Column(name = "stato", nullable = false)
  private StatoContratto stato;

  @NotAudited
  @Column(name = "operazioni_possibili")
  private String operazioniPossibili;

  @NotAudited
  @Column(name = "paese_riferimento_iva")
  private String paeseRiferimentoIva;

  @NotNull
  @NotAudited
  @Column(name = "data_modifica_stato", nullable = false)
  @Convert(converter = DateInstantConverter.class)
  private Instant dataModificaStato;

  @NotNull
  @Column(name = "primario", nullable = false)
  private Boolean primario = Boolean.TRUE;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getIdentificativo() {
    return identificativo;
  }

  public Contratto identificativo(String identificativo) {
    this.identificativo = identificativo;
    return this;
  }

  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

  public String getCodContrattoCliente() {
    return codContrattoCliente;
  }

  public Contratto codContrattoCliente(String codContrattoCliente) {
    this.codContrattoCliente = codContrattoCliente;
    return this;
  }

  public void setCodContrattoCliente(String codContrattoCliente) {
    this.codContrattoCliente = codContrattoCliente;
  }

  public Set<Dispositivo> getDispositivo() {
    return dispositivo;
  }

  public void setDispositivo(Set<Dispositivo> dispositivo) {
    this.dispositivo = dispositivo;
  }

  public Produttore getProduttore() {
    return produttore;
  }

  public Contratto produttore(Produttore produttore) {
    this.produttore = produttore;
    return this;
  }

  public void setProduttore(Produttore produttore) {
    this.produttore = produttore;
  }

  public ClienteFai getClienteFai() {
    return clienteFai;
  }

  public Contratto clienteFai(ClienteFai clienteFai) {
    this.clienteFai = clienteFai;
    return this;
  }

  public void setClienteFai(ClienteFai clienteFai) {
    this.clienteFai = clienteFai;
  }

  public Set<Richiesta> getRichiestas() {
    return richiestas;
  }

  public Contratto richiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
    return this;
  }

  public Contratto addRichiesta(Richiesta richiesta) {
    this.richiestas.add(richiesta);
    richiesta.setContratto(this);
    return this;
  }

  public Contratto removeRichiesta(Richiesta richiesta) {
    this.richiestas.remove(richiesta);
    richiesta.setContratto(null);
    return this;
  }

  public void setRichiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
  }

  public Contratto stato(StatoContratto stato) {
    this.stato = stato;
    return this;
  }

  public StatoContratto getStato() {
    return stato;
  }

  public void setStato(StatoContratto stato) {
    this.stato = stato;
  }

  public Contratto operazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
    return this;
  }

  public String getOperazioniPossibili() {
    return operazioniPossibili;
  }

  public void setOperazioniPossibili(String operazioniPossibili) {
    this.operazioniPossibili = operazioniPossibili;
  }

  public Contratto dataModificaStato(Instant date) {
    this.dataModificaStato = date;
    return this;
  }

  public Instant getDataModificaStato() {
    return dataModificaStato;
  }

  public void setDataModificaStato(Instant date) {
    this.dataModificaStato = date;
  }

  public String getPaeseRiferimentoIva() {
	return paeseRiferimentoIva;
  }

  public void setPaeseRiferimentoIva(String paeseRiferimentoIva) {
	this.paeseRiferimentoIva = paeseRiferimentoIva;
  }

  public Contratto paeseRiferimentoIva(String paeseRiferimentoIva) {
    this.paeseRiferimentoIva = paeseRiferimentoIva;
    return this;
  }



  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  public Boolean isPrimario() {
    return primario;
  }

  public void setPrimario(Boolean primario) {
    this.primario = primario;
  }

  public Contratto primario(Boolean primario) {
    this.primario = primario;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contratto contratto = (Contratto) o;
    if (contratto.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), contratto.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return
      new StringBuilder().append("Contratto{").append("id=").append(getId()).append(", identificativo='").append(getIdentificativo()).append("'").append(", codContrattoCliente='").append(getCodContrattoCliente()).append("'").append(", stato='").append(getStato()).append("'").append(", dataModificaStato='").append(getDataModificaStato()).append("'").append(", operazioniPossibili='").append(getOperazioniPossibili()).append("'").append(", paeseRiferimentoIva='").append(getPaeseRiferimentoIva()).append("'").append(", pricipal='").append(isPrimario()).append("'").append("}").toString();
  }

}
