package it.fai.ms.efservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import io.github.jhipster.web.util.ResponseUtil;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.TipoHardwareDispositivoService;
import it.fai.ms.efservice.service.dto.TipoHardwareDispositivoDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;

/**
 * REST controller for managing TipoServizio.
 */
@RestController
@RequestMapping(TipoHardwareDispositivoResource.BASE_PATH)
public class TipoHardwareDispositivoResource {

  private final Logger log = LoggerFactory.getLogger(TipoHardwareDispositivoResource.class);

  public static final String BASE_PATH = "/api/public";

  private static final String ENTITY_NAME = "tipoHardwareDispositivo";

  private static final String TIPO_HARDWARE = "/tipo-hardware-dispositivo";

  private final TipoHardwareDispositivoService tipoHardwareDispositivoService;

  private final TipoDispositivoServiceExt tipoDispositivoService;

  public TipoHardwareDispositivoResource(final TipoHardwareDispositivoService tipoHardwareDispositivoService,
                                         final TipoDispositivoServiceExt tipoDispositivoService) {
    this.tipoHardwareDispositivoService = tipoHardwareDispositivoService;
    this.tipoDispositivoService = tipoDispositivoService;
  }

  /**
   * POST /tipo-hardware-dispositivo : Create a new TipoHardwareDispositivo.
   *
   * @param TipoHardwareDispositivoDTO
   *          the TipoHardwareDispositivoDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new tipoServizioDTO, or with status 400 (Bad
   *         Request) if the tipoServizio has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   * @throws CustomException
   */
  @PostMapping(TIPO_HARDWARE)
  @Timed
  public ResponseEntity<TipoHardwareDispositivo> createTipoHardwareDispositivo(@Valid @RequestBody TipoHardwareDispositivoDTO tipoHardwareDTO) throws URISyntaxException,
                                                                                                                                               CustomException {
    log.debug("REST request to save tipoHardwareDispositivo : {}", tipoHardwareDTO);
    if (tipoHardwareDTO.getId() != null) {
      return ResponseEntity.badRequest()
                           .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipoHardware cannot already have an ID"))
                           .body(null);
    }

    TipoDispositivo deviceType = tipoDispositivoService.findOneByNome(tipoHardwareDTO.getTipoDispositivo());
    if (deviceType == null) {
      throw CustomException.builder(HttpStatus.BAD_GATEWAY)
                           .add(Errno.NOT_FOUND_DEVICE_TYPE);
    }

    TipoHardwareDispositivo hardwareTypeDevice = new TipoHardwareDispositivo();
    hardwareTypeDevice.setTipoDispositivo(deviceType);
    hardwareTypeDevice.setHardwareType(tipoHardwareDTO.getTipoHardware());
    hardwareTypeDevice.setDefaultHardwareType(tipoHardwareDTO.isDefaultTipoHardware());
    hardwareTypeDevice.setDescrizione(tipoHardwareDTO.getDescrizione());
    hardwareTypeDevice.setAttivo(tipoHardwareDTO.isAttivo());

    TipoHardwareDispositivo hardwareTypeDeviceSaved = tipoHardwareDispositivoService.save(hardwareTypeDevice);
    return ResponseEntity.created(new URI("/api/tipo-hardware-dispositivo/" + hardwareTypeDeviceSaved.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, hardwareTypeDeviceSaved.getId()
                                                                                                           .toString()))
                         .body(hardwareTypeDeviceSaved);
  }

  /**
   * GET /tipo-hardware-dispositivo : get all the tipo-hardware-dispositivo.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of tipo-hardware-dispositivo in body
   */
  @GetMapping(TIPO_HARDWARE)
  @Timed
  public List<TipoHardwareDispositivo> getAllTipoHardwareDispositivo() {
    log.debug("REST request to get all TipoHardwareDispositivo");
    return tipoHardwareDispositivoService.findAll();
  }

  /**
   * GET /tipo-hardware-dispositivo/:id : get the "id" tipoHardwareDispositivo.
   *
   * @param id
   *          the id of the tipoHardwareDispositivo to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the TipoHardwareDispositivo, or with status 404 (Not
   *         Found)
   */
  @GetMapping(TIPO_HARDWARE + "/{id}")
  @Timed
  public ResponseEntity<TipoHardwareDispositivo> getTipoHardwareDispositivoById(@PathVariable Long id) {
    log.debug("REST request to get TipoHardwareDispositivo : {}", id);
    TipoHardwareDispositivo tipoHardwareDispositivo = tipoHardwareDispositivoService.findOne(id);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoHardwareDispositivo));
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:deviceType : get the "deviceType" tipoHardwareDispositivo.
   *
   * @param deviceType
   *          the deviceType of the tipoHardwareDispositivo to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the TipoHardwareDispositivo, or with status 404 (Not
   *         Found)
   */
  @GetMapping(TIPO_HARDWARE + "/tipo-dispositivo/{deviceType}")
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getTipoHardwareDispositivo(@PathVariable TipoDispositivoEnum deviceType) {
    log.debug("REST request to get TipoHardwareDispositivo : {}", deviceType);
    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findActiveByDeviceType(deviceType);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoHardwareDispositivos));
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:tipoDispositivoId : get the "tipoDispositivoId"
   * tipoHardwareDispositivos.
   *
   * @param tipoDispositivoId
   *          the tipoDispositivoId of the tipoHardwareDispositivos to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the tipoHardwareDispositivos, or with status 404 (Not
   *         Found)
   */
  @GetMapping(TIPO_HARDWARE + "/tipo-dispositivo-id/{tipoDispositivoId}")
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getTipoHardwareDispositivo(@PathVariable Long tipoDispositivoId) {
    log.debug("REST request to get TipoHardwareDispositivo : {}", tipoDispositivoId);
    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findActiveByDeviceTypeId(tipoDispositivoId);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoHardwareDispositivos));
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:deviceType : get the "deviceType" tipoHardwareDispositivo.
   *
   * @param deviceType
   *          the deviceType of the tipoHardwareDispositivo to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the TipoHardwareDispositivo, or with status 404 (Not
   *         Found)
   */
  @GetMapping(
              value = { TIPO_HARDWARE + "/tipo-dispositivo/{deviceType}/hw/{tipoHardware}",
                        TIPO_HARDWARE + "/tipo-dispositivo/{deviceType}/hw" })
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getAllTipoHardwareDispositivo(@PathVariable TipoDispositivoEnum deviceType,
                                                                                     @PathVariable(required = false) String tipoHardware) {
    log.debug("REST request to get TipoHardwareDispositivo : {} and tipo hardware: {}", deviceType, tipoHardware);
    List<TipoHardwareDispositivo> hardwareTypes = new ArrayList<>();
    if (StringUtils.isNotBlank(tipoHardware)) {
      hardwareTypes.add(getTipoHardwareDeviceByDeviceTypeAndHardware(deviceType, tipoHardware));
    }

    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findActiveByDeviceType(deviceType);
    populateHardwareTypesWithSkipValue(tipoHardwareDispositivos, hardwareTypes, tipoHardware);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hardwareTypes));
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:tipoDispositivoId/hw/:tipoHardware : get the "tipoDispositivoId"
   * "tipoHardware" tipoHardwareDispositivos.
   *
   * @param tipoDispositivoId
   *          the tipoDispositivoId of the tipoHardwareDispositivos to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the tipoHardwareDispositivos, or with status 404 (Not
   *         Found)
   * @throws CustomException
   */
  @GetMapping(TIPO_HARDWARE + "/tipo-dispositivo-id/{tipoDispositivoId}/hw/{tipoHardware}")
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getAllTipoHardwareDispositivo(@PathVariable Long tipoDispositivoId,
                                                                                     @PathVariable String tipoHardware) throws CustomException {
    log.debug("REST request to get TipoHardwareDispositivo : {} and tipo hardware: {}", tipoDispositivoId, tipoHardware);

    List<TipoHardwareDispositivo> hardwareTypes = new ArrayList<>();
    if (StringUtils.isNotBlank(tipoHardware)) {
      TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneById(tipoDispositivoId);
      if (tipoDispositivo != null) {
        hardwareTypes.add(getTipoHardwareDeviceByDeviceTypeAndHardware(tipoDispositivo.getNome(), tipoHardware));
      } else {
        log.error("Not found Tipo Dispositivo by id: {}", tipoDispositivoId);

        throw CustomException.builder(HttpStatus.BAD_GATEWAY)
                             .add(Errno.NOT_FOUND_DEVICE_TYPE);
      }
    }

    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findActiveByDeviceTypeId(tipoDispositivoId);
    populateHardwareTypesWithSkipValue(tipoHardwareDispositivos, hardwareTypes, tipoHardware);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hardwareTypes));
  }

  private void populateHardwareTypesWithSkipValue(List<TipoHardwareDispositivo> tipoHardwareDispositivos,
                                                  List<TipoHardwareDispositivo> hardwareTypes, String tipoHardwareToSkip) {
    if (tipoHardwareDispositivos != null && !tipoHardwareDispositivos.isEmpty()) {
      tipoHardwareDispositivos.forEach(th -> {
        if (th.getHardwareType() != null && !th.getHardwareType()
                                               .equals(tipoHardwareToSkip)) {
          hardwareTypes.add(th);
        } else {
          log.debug("Skip add tipo hardware on list...");
        }
      });
    }
  }

  private TipoHardwareDispositivo getTipoHardwareDeviceByDeviceTypeAndHardware(TipoDispositivoEnum deviceType, String tipoHardware) {
    Optional<TipoHardwareDispositivo> optTipoHardwareDevice = tipoHardwareDispositivoService.findByTipoDispositivo_nomeAndHardwareType(deviceType,
                                                                                                                                       tipoHardware);
    TipoHardwareDispositivo hardwareTypeFromDb = null;
    if (optTipoHardwareDevice.isPresent()) {
      hardwareTypeFromDb = optTipoHardwareDevice.get();
    } else {
      hardwareTypeFromDb = new TipoHardwareDispositivo();
      hardwareTypeFromDb.setDescrizione(tipoHardware);
      hardwareTypeFromDb.setHardwareType(tipoHardware);
      hardwareTypeFromDb.setDefaultHardwareType(true);
    }

    log.info("Found hardware device type: {}", hardwareTypeFromDb);
    return hardwareTypeFromDb;
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:deviceType : get the "deviceType" tipoHardwareDispositivo.
   *
   * @param deviceType
   *          the deviceType of the tipoHardwareDispositivo to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the TipoHardwareDispositivo, or with status 404 (Not
   *         Found)
   */
  @GetMapping(TIPO_HARDWARE + "/all/tipo-dispositivo/{deviceType}")
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getAllTipoHardwareDispositivo(@PathVariable TipoDispositivoEnum deviceType) {
    log.debug("REST request to get TipoHardwareDispositivo : {}", deviceType);
    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findByDeviceType(deviceType);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoHardwareDispositivos));
  }

  /**
   * GET /tipo-hardware-dispositivo/tipo-dispositivo/:tipoDispositivoId : get the "tipoDispositivoId"
   * tipoHardwareDispositivos.
   *
   * @param tipoDispositivoId
   *          the tipoDispositivoId of the tipoHardwareDispositivos to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the tipoHardwareDispositivos, or with status 404 (Not
   *         Found)
   */
  @GetMapping(TIPO_HARDWARE + "/all/tipo-dispositivo-id/{tipoDispositivoId}")
  @Timed
  public ResponseEntity<List<TipoHardwareDispositivo>> getAllTipoHardwareDispositivo(@PathVariable Long tipoDispositivoId) {
    log.debug("REST request to get TipoHardwareDispositivo : {}", tipoDispositivoId);
    List<TipoHardwareDispositivo> tipoHardwareDispositivos = tipoHardwareDispositivoService.findByDeviceTypeId(tipoDispositivoId);
    return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoHardwareDispositivos));
  }

  /**
   * DELETE /tipo-hardware-dispositivo/:id : delete the "id" tipoHardwareDispositivos.
   *
   * @param id
   *          the id of the tipoHardwareDispositivos to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping(TIPO_HARDWARE + "/{id}")
  @Timed
  public ResponseEntity<Void> deleteTipoServizio(@PathVariable Long id) {
    log.debug("REST request to delete tipoHardwareDispositivos : {}", id);
    tipoHardwareDispositivoService.delete(id);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                         .build();
  }
}
