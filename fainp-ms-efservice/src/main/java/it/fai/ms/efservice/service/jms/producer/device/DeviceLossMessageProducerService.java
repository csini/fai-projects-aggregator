package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceLossMessageProducerService {

  void deactivateDevice(Richiesta orderRequestEntity);

  void deactivatedDeviceTelepassIta(Richiesta richiesta);

}
