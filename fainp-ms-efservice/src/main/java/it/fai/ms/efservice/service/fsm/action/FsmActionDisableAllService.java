package it.fai.ms.efservice.service.fsm.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;

public class FsmActionDisableAllService implements Action<StatoRichiesta, RichiestaEvent> {

  private final Logger                                        log = LoggerFactory.getLogger(getClass());
  private final DeviceServiceActivationMessageProducerService deviceServiceActivationService;

  public FsmActionDisableAllService(final DeviceServiceActivationMessageProducerService _deviceServiceActivationService) {
    deviceServiceActivationService = _deviceServiceActivationService;
  }

  @Override
  public void execute(StateContext<StatoRichiesta, RichiestaEvent> context) {
    Object obj = context.getMessageHeader("object");
    if (obj instanceof Richiesta) {
      Richiesta richiesta = (Richiesta) obj;
      log.info("Update all StatoDispositivoServizio to Richiesta: {}", richiesta.getIdentificativo());
      deviceServiceActivationService.disableAllServiceDeviceStatus(richiesta);
    }
  }

}
