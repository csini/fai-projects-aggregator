package it.fai.ms.efservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;

@Repository
public interface TipoHardwareDispositivoRepository extends JpaRepository<TipoHardwareDispositivo, Long> {
  
  Optional<TipoHardwareDispositivo> findByTipoDispositivo_nomeAndHardwareTypeAndAttivoTrue(TipoDispositivoEnum tipoDispositivo, String tipoHardware);
  
  List<TipoHardwareDispositivo> findByTipoDispositivo_nome(TipoDispositivoEnum deviceType);

  List<TipoHardwareDispositivo> findByTipoDispositivo_id(Long tipoDispositivoId);
  
  List<TipoHardwareDispositivo> findByTipoDispositivo_nomeAndAttivoTrue(TipoDispositivoEnum deviceTypeEnum);

  List<TipoHardwareDispositivo> findByTipoDispositivo_idAndAttivoTrue(Long deviceTypeId);

}
