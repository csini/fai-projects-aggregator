package it.fai.ms.efservice.dto;

import java.io.Serializable;

public class WizardVehicleDTO implements Serializable {

  private static final long serialVersionUID = -7895104439017931691L;
  
  private String uuidVehicle;
  
  private boolean hasDevicesActive;
  
  private boolean expiring;
  
  private boolean expired;

  public String getUuidVehicle() {
    return uuidVehicle;
  }

  public void setUuidVehicle(String uuidVehicle) {
    this.uuidVehicle = uuidVehicle;
  }

  public boolean isHasDevicesActive() {
    return hasDevicesActive;
  }

  public void setHasDevicesActive(boolean hasDevicesActive) {
    this.hasDevicesActive = hasDevicesActive;
  }

  public boolean isExpiring() {
    return expiring;
  }

  public void setExpiring(boolean expiring) {
    this.expiring = expiring;
  }

  public boolean isExpired() {
    return expired;
  }

  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardVehicleDTO [uuidVehicle=");
    builder.append(uuidVehicle);
    builder.append(", hasDevicesActive=");
    builder.append(hasDevicesActive);
    builder.append(", expiring=");
    builder.append(expiring);
    builder.append(", expired=");
    builder.append(expired);
    builder.append("]");
    return builder.toString();
  }

}
