package it.fai.ms.efservice.service.jms.mapper;

import java.time.Instant;

import it.fai.ms.common.dml.efservice.dto.OrdiniClienteDMLDTO;
import it.fai.ms.efservice.domain.OrdineCliente;

public class OrdiniClienteJmsMapper {
  
  public OrdiniClienteDMLDTO toOrdineClienteDMLDTO(OrdineCliente ord) {

    return toOrdineClienteDMLDTO(ord, null);
  }

  private OrdiniClienteDMLDTO toOrdineClienteDMLDTO(OrdineCliente ord, OrdiniClienteDMLDTO dml) {

    if (dml == null) {
      dml = toEmptyOrdiniClienteDMLDTO(ord.getIdentificativo());
    }

    dml.setCodiceClienteAssegnatario(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getCodiceCliente():null);
    dml.setConsorzio(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getRaggruppamentoImpresa():null);
    dml.setConsorzioDescrizione(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getDescrizioneRaggruppamentoImpresa():null);
    dml.setCodiceClienteFatturazione(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getCodiceClienteFatturazione():null);
    dml.setCodiceAgente(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getCodiceAgente():null);
    dml.setPartitaIva(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getPartitaIva():null);

    dml.setConsumoPrevisto(ord.getPrevisioneSpesaMensile()+"");
    dml.setDataCreazione(ord.getDataCreazione());
    dml.setDataModificaStato(ord.getDataModificaStato());
    dml.setNumero(ord.getNumeroOrdine());
    dml.setRagioneSocialeAssegnatario(ord.getClienteAssegnatario()!=null?ord.getClienteAssegnatario().getRagioneSociale():null);

    dml.setRichiedente(ord.getRichiedente());
    dml.setStato(ord.getStato()!=null ? ord.getStato().name():null);
    dml.setTipo(ord.getTipo()!=null ? ord.getTipo().name() : null);

    return dml;
  }

  public OrdiniClienteDMLDTO toEmptyOrdiniClienteDMLDTO(String dmlUniqueIdentifier){
    OrdiniClienteDMLDTO dml = new OrdiniClienteDMLDTO();
    dml.setDmlUniqueIdentifier(dmlUniqueIdentifier);
    dml.setDmlRevisionTimestamp(Instant.now());
    return dml;
  }

}