package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.TipoServizioDefaultService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.TipoServizioDefaultDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoServizioDefault.
 */
@RestController
@RequestMapping("/api")
public class TipoServizioDefaultResource {

    private final Logger log = LoggerFactory.getLogger(TipoServizioDefaultResource.class);

    private static final String ENTITY_NAME = "tipoServizioDefault";

    private final TipoServizioDefaultService tipoServizioDefaultService;

    public TipoServizioDefaultResource(TipoServizioDefaultService tipoServizioDefaultService) {
        this.tipoServizioDefaultService = tipoServizioDefaultService;
    }

    /**
     * POST  /tipo-servizio-defaults : Create a new tipoServizioDefault.
     *
     * @param tipoServizioDefaultDTO the tipoServizioDefaultDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoServizioDefaultDTO, or with status 400 (Bad Request) if the tipoServizioDefault has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-servizio-defaults")
    @Timed
    public ResponseEntity<TipoServizioDefaultDTO> createTipoServizioDefault(@RequestBody TipoServizioDefaultDTO tipoServizioDefaultDTO) throws URISyntaxException {
        log.debug("REST request to save TipoServizioDefault : {}", tipoServizioDefaultDTO);
        if (tipoServizioDefaultDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tipoServizioDefault cannot already have an ID")).body(null);
        }
        TipoServizioDefaultDTO result = tipoServizioDefaultService.save(tipoServizioDefaultDTO);
        return ResponseEntity.created(new URI("/api/tipo-servizio-defaults/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-servizio-defaults : Updates an existing tipoServizioDefault.
     *
     * @param tipoServizioDefaultDTO the tipoServizioDefaultDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoServizioDefaultDTO,
     * or with status 400 (Bad Request) if the tipoServizioDefaultDTO is not valid,
     * or with status 500 (Internal Server Error) if the tipoServizioDefaultDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-servizio-defaults")
    @Timed
    public ResponseEntity<TipoServizioDefaultDTO> updateTipoServizioDefault(@RequestBody TipoServizioDefaultDTO tipoServizioDefaultDTO) throws URISyntaxException {
        log.debug("REST request to update TipoServizioDefault : {}", tipoServizioDefaultDTO);
        if (tipoServizioDefaultDTO.getId() == null) {
            return createTipoServizioDefault(tipoServizioDefaultDTO);
        }
        TipoServizioDefaultDTO result = tipoServizioDefaultService.save(tipoServizioDefaultDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoServizioDefaultDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-servizio-defaults : get all the tipoServizioDefaults.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipoServizioDefaults in body
     */
    @GetMapping("/tipo-servizio-defaults")
    @Timed
    public List<TipoServizioDefaultDTO> getAllTipoServizioDefaults() {
        log.debug("REST request to get all TipoServizioDefaults");
        return tipoServizioDefaultService.findAll();
        }

    /**
     * GET  /tipo-servizio-defaults/:id : get the "id" tipoServizioDefault.
     *
     * @param id the id of the tipoServizioDefaultDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoServizioDefaultDTO, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-servizio-defaults/{id}")
    @Timed
    public ResponseEntity<TipoServizioDefaultDTO> getTipoServizioDefault(@PathVariable Long id) {
        log.debug("REST request to get TipoServizioDefault : {}", id);
        TipoServizioDefaultDTO tipoServizioDefaultDTO = tipoServizioDefaultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoServizioDefaultDTO));
    }

    /**
     * DELETE  /tipo-servizio-defaults/:id : delete the "id" tipoServizioDefault.
     *
     * @param id the id of the tipoServizioDefaultDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-servizio-defaults/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoServizioDefault(@PathVariable Long id) {
        log.debug("REST request to delete TipoServizioDefault : {}", id);
        tipoServizioDefaultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
