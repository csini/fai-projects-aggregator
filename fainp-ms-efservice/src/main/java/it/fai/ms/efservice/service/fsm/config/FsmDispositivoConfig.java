package it.fai.ms.efservice.service.fsm.config;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoGeneric;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.listener.StateMachineMonitor;
import it.fai.ms.efservice.service.fsm.util.StatoDispositivoUtil;

@Configuration
@EnableStateMachineFactory(name = "dispositivo")
public class FsmDispositivoConfig extends EnumStateMachineConfigurerAdapter<StatoDispositivo, DispositivoEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmDispositivoConfig.class);

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoDispositivo, DispositivoEvent> config) throws Exception {
    super.configure(config);
    config.withMonitoring()
          .monitor(new StateMachineMonitor<StatoDispositivo, DispositivoEvent>());

    config.withConfiguration()
          .machineId("fsmDispositivo");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoDispositivo, DispositivoEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoDispositivo, DispositivoEvent> states) throws Exception {
    states.withStates()
          .initial(StatoDispositivo.INIZIALE, ctx -> log.trace("Initial: " + ctx.getTarget()
                                                                                .getIds()))
          .states(StatoDispositivoUtil.getAllStatoDispositivo());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoDispositivo, DispositivoEvent> transitions) throws Exception {
    log.info("Init transitions for FSM Dispositivi");
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.INOLTRATO_AL_FORNITORE, DispositivoEvent.INOLTRATO, null);
    addTransition(transitions, StatoDispositivo.INOLTRATO_AL_FORNITORE, StatoDispositivo.ACCETTATO_DA_FORNITORE, DispositivoEvent.ACCETTATO,
                  null);
    addTransition(transitions, StatoDispositivo.INOLTRATO_AL_FORNITORE, StatoDispositivo.ANNULLATO, DispositivoEvent.ANNULLAMENTO, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ACCETTATO_DA_FORNITORE, DispositivoEvent.ACCETTATO, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.IN_DEPOSITO, DispositivoEvent.TSAT_DEPOSITO, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ANNULLATO, DispositivoEvent.ANNULLAMENTO, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                  DispositivoEvent.SPEDITO_DAL_FORNITORE, null);
    addTransition(transitions, StatoDispositivo.IN_DEPOSITO, StatoDispositivo.IN_DEPOSITO_IN_ATTESA_SPEDIZIONE,
                  DispositivoEvent.TSAT_DEPOSITO_ATTESA_SPEDIZIONE, null);
    addTransition(transitions, StatoDispositivo.IN_DEPOSITO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.IN_DEPOSITO,
                  DispositivoEvent.TSAT_DEPOSITO_PER_VERIFICA, null);
    addTransition(transitions, StatoDispositivo.IN_DEPOSITO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE,
                  DispositivoEvent.TSAT_ATTIVO_ATTESA_SPEDIZIONE, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, DispositivoEvent.IN_SPEDIZIONE,
                  null);
    addTransition(transitions, StatoDispositivo.ACCETTATO_DA_FORNITORE, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE,
                  DispositivoEvent.IN_SPEDIZIONE, null);
    addTransition(transitions, StatoDispositivo.ACCETTATO_DA_FORNITORE, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                  DispositivoEvent.SPEDITO_DAL_FORNITORE, null);
    addTransition(transitions, StatoDispositivo.ACCETTATO_DA_FORNITORE, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI,
                  DispositivoEvent.SPEDITO_DA_FAI, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                  DispositivoEvent.SPEDITO_DAL_FORNITORE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.ATTIVO_SPEDITO_A_FAI,
                  DispositivoEvent.SPEDITO_A_FAI, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI,
                  DispositivoEvent.SPEDITO_DA_FAI, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.REVOCATO, DispositivoEvent.NON_SPEDITO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_A_FAI, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI,
                  DispositivoEvent.SPEDITO_DA_FAI, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_A_FAI, StatoDispositivo.REVOCATO, DispositivoEvent.REVOCA, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.REVOCATO, DispositivoEvent.NON_SPEDITO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.BLOCCATO_FURTO, DispositivoEvent.BLOCCO_FURTO,
                  null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.BLOCCATO_FURTO,
                  DispositivoEvent.BLOCCO_FURTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.BLOCCATO_SMARRIMENTO,
                  DispositivoEvent.BLOCCO_SMARRIMENTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.BLOCCATO_FURTO, DispositivoEvent.BLOCCO_FURTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.BLOCCATO_SMARRIMENTO, DispositivoEvent.BLOCCO_SMARRIMENTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.BLOCCATO_SMARRIMENTO,
                  DispositivoEvent.BLOCCO_SMARRIMENTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.BLOCCATO_NON_DICHIARATO,
                  DispositivoEvent.BLOCCO_NO_DICHIARATO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.BLOCCATO_NON_DICHIARATO,
                  DispositivoEvent.BLOCCO_NO_DICHIARATO, null);
    addTransition(transitions, StatoDispositivo.BLOCCATO_NON_DICHIARATO, StatoDispositivo.BLOCCATO_FURTO, DispositivoEvent.BLOCCO_FURTO,
                  null);
    addTransition(transitions, StatoDispositivo.BLOCCATO_NON_DICHIARATO, StatoDispositivo.BLOCCATO_SMARRIMENTO,
                  DispositivoEvent.BLOCCO_SMARRIMENTO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                  DispositivoEvent.RIENTRO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                  DispositivoEvent.RIENTRO, null);
    addTransition(transitions, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE,
                  DispositivoEvent.RIENTRO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.SOSPESO, DispositivoEvent.SOSPENSIONE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.SOSPESO, DispositivoEvent.SOSPENSIONE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.REVOCATO, DispositivoEvent.REVOCA, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.REVOCATO, DispositivoEvent.REVOCA, null);
    addTransition(transitions, StatoDispositivo.SOSPESO, StatoDispositivo.ATTIVO, DispositivoEvent.RIATTIVAZIONE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO,
                  DispositivoEvent.ATTESA_RIENTRO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO,
                  DispositivoEvent.ATTESA_RIENTRO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO,
                  DispositivoEvent.ATTESA_RIENTRO, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                  DispositivoEvent.DA_RESTITUIRE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                  DispositivoEvent.DA_RESTITUIRE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE,
                  DispositivoEvent.DA_RESTITUIRE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.ATTIVO, DispositivoEvent.ATTIVAZIONE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI, StatoDispositivo.ATTIVO, DispositivoEvent.ATTIVAZIONE, null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE, DispositivoEvent.RIENTRO,
                  null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.ANNULLATO, DispositivoEvent.ANNULLAMENTO, null);
    addAllTransactionToTarget(transitions, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE, DispositivoEvent.MU_DA_RESTITUIRE);

    addTransition(transitions, StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI,
                  DispositivoEvent.SPEDITO_DA_FAI, null);
    addTransition(transitions, StatoDispositivo.IN_PREPARAZIONE_SPEDIZIONE, StatoDispositivo.IN_DEPOSITO, DispositivoEvent.REVOCA, null);
    addTransition(transitions, StatoDispositivo.ATTIVO, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE, DispositivoEvent.DA_RESTITUIRE,
                  null);

    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ATTIVO, DispositivoEvent.ATTIVO, null);
    addTransition(transitions, StatoDispositivo.INIZIALE, StatoDispositivo.ANNULLATO_OPERATORE_FAI, DispositivoEvent.DISATTIVO, null);

    addTransition(transitions, StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE, StatoDispositivo.RIENTRATO_DISMESSO,
                  DispositivoEvent.DISMESSO, null);

    Set<StatoDispositivo> returnStates = StatoDispositivo.getStatesToReturn();
    returnStates.forEach(rs -> addTransition(transitions, rs, StatoDispositivo.RIENTRATO_DISMESSO, DispositivoEvent.DISMESSO, null));

    returnStates.forEach(rs -> addTransition(transitions, rs, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE,
                                             DispositivoEvent.RIENTRO, null));

    Set<StatoDispositivo> revocableStates = StatoDispositivo.getRevocableStates();
    log.debug("Revocable states: {}", revocableStates);
    revocableStates.forEach(sd -> addTransition(transitions, sd, StatoDispositivo.REVOCATO, DispositivoEvent.REVOCA_DA_CONTRATTO, null));
  }

  /**
   * Use to add all manual transitions that from all state without RIENTRATO_RESTITUITO_AL_FORNITORE and
   * RIENTRATO_DA_RESTITUIRE_AL_FORNITORE to RIENTRATO_DA_RESTITUIRE_AL_FORNITORE with command event MU_DA_RESTITUIRE;
   * This transitions are used by BackOffice;
   * 
   * @param transitions
   * @param target
   * @param event
   */
  private void addAllTransactionToTarget(StateMachineTransitionConfigurer<StatoDispositivo, DispositivoEvent> transitions,
                                         StatoDispositivo target, DispositivoEvent event) {
    StatoDispositivoUtil.getAllStatoDispositivo()
                        .stream()
                        .filter(source -> source != target && source != StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE)
                        .map(source -> {
                          try {
                            log.debug("Add transition -> Source {} - Target {} - Event {}", source, target, event);
                            transitions.withExternal()
                                       .source(source)
                                       .target(target)
                                       .event(event)
                                       .action(new FsmActionDispositivoGeneric());
                          } catch (Exception e) {
                            log.error("Execption on add transition", e);
                            throw new RuntimeException(e);
                          }
                          return source;
                        })
                        .collect(toSet());
  }

  private void addTransition(StateMachineTransitionConfigurer<StatoDispositivo, DispositivoEvent> transitions, StatoDispositivo source,
                             StatoDispositivo target, DispositivoEvent event, Action<StatoDispositivo, DispositivoEvent> action) {
    try {
      if (action == null) {
        log.debug("Instance generic Dispositivo Action");
        action = new FsmActionDispositivoGeneric();
      }
      log.debug("Add transition -> Source {} - Target {} - Event {} - Action {}", source, target, event, action);
      if (event == null) {
        transitions.withExternal()
                   .source(source)
                   .target(target)
                   .action(action);
      } else {
        transitions.withExternal()
                   .source(source)
                   .target(target)
                   .event(event)
                   .action(action);
      }
    } catch (Exception e) {
      log.error("Execption on add transition", e);
      throw new RuntimeException(e);
    }

  }

}
