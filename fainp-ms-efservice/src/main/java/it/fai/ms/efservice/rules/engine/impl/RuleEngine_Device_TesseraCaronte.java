package it.fai.ms.efservice.rules.engine.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.WizardPreconditionCode;
import it.fai.ms.efservice.rules.engine.RuleEngine;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_TesseraCaronte implements RuleEngine {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private RuleEngineDeviceType deviceTypeRuleContext;
  private RuleEngineVehicle    vehicleRuleContext;

  public RuleEngine_Device_TesseraCaronte(final RuleEngineDeviceType _deviceTypeRuleIncome, final RuleEngineVehicle _vehicleRuleIncome) {
    deviceTypeRuleContext = _deviceTypeRuleIncome;
    vehicleRuleContext = _vehicleRuleIncome;
  }

  @Override
  public RuleOutcome executeRule() {
    RuleOutcome ruleOutcome = null;

    final String expression = String.join("", buildRules());
    final Map<String, Object> context = new HashMap<>();
    context.put("devType", deviceTypeRuleContext);
    context.put("vehicle", vehicleRuleContext);
    String eval = MVEL.eval(expression, context, String.class);
    if (eval.equals("")) {
      ruleOutcome = new RuleOutcome();
    } else {
      ruleOutcome = new RuleOutcome(new RuleOutcome.RuleFailure(eval, decode(eval)));
    }
    _log.debug("Rule executed [Expression: {} - Context: {} => RuleOutcome: {}]", expression, context, ruleOutcome);
    return ruleOutcome;
  }

  private List<String> buildRules() {
    List<String> rules = new LinkedList<>();
    rules.add("boolean deviceType = (devType.id.id == '" + TipoDispositivoEnum.TESSERA_CARONTE.name() + "');");
    rules.add("if (deviceType) return 'tesseraCaronteNotActivable';");
    rules.add("return ''");
    return rules;
  }

  private String decode(final String _code) {
    WizardPreconditionCode preconditionCode = WizardPreconditionCode.get(_code);

    String res = null;
    switch (preconditionCode) {
    case TESSERA_CARONTE_NOT_ACTIVABLE:
      res = "Tessera Caronte Device is not orderable";
      break;
    default:
      res = "n.a.";
      break;
    }
    return res;
  }

}
