package it.fai.ms.efservice.wizard.service;

import java.util.Set;

import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.step3.WizardStep3Matrix;

public interface Step3Service {

  WizardStep3Matrix buildMatrix(WizardServiceTypeId wizardServiceTypeId, Set<WizardVehicleId> mapToWizardVehicleIds);

}
