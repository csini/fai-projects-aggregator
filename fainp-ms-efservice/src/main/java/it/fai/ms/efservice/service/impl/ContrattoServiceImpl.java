package it.fai.ms.efservice.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.ContrattoService;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.mapper.ContrattoMapper;

@Service
@Transactional
public class ContrattoServiceImpl implements ContrattoService {

  private final Logger log = LoggerFactory.getLogger(ContrattoServiceImpl.class);

  private final ContrattoRepositoryExt contrattoRepository;
  private final ContrattoMapper        contrattoMapper;

  public ContrattoServiceImpl(ContrattoRepositoryExt _contrattoRepository, ContrattoMapper contrattoMapper) {
    contrattoRepository = _contrattoRepository;
    this.contrattoMapper = contrattoMapper;
  }

  /**
   * Save a contratto.
   *
   * @param contrattoDTO
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public ContrattoDTO save(ContrattoDTO contrattoDTO) {
    log.debug("Request to save Contratto : {}", contrattoDTO);
    Contratto contratto = contrattoMapper.toEntity(contrattoDTO);
    contratto = contrattoRepository.save(contratto);
    return contrattoMapper.toDto(contratto);
  }

  /**
   * Get all the contrattos.
   *
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public List<ContrattoDTO> findAll() {
    log.debug("Request to get all Contrattos");
    return contrattoRepository.findAll()
                              .stream()
                              .map(contrattoMapper::toDto)
                              .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get one contratto by id.
   *
   * @param id
   *          the id of the entity
   * @return the entity
   */
  @Override
  @Transactional(readOnly = true)
  public ContrattoDTO findOne(Long id) {
    log.debug("Request to get Contratto : {}", id);
    Contratto contratto = contrattoRepository.findOne(id);
    return contrattoMapper.toDto(contratto);
  }

  /**
   * Delete the contratto by id.
   *
   * @param id
   *          the id of the entity
   */
  @Override
  public void delete(Long id) {
    log.debug("Request to delete Contratto : {}", id);
    contrattoRepository.delete(id);
  }
}
