package it.fai.ms.efservice.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.repository.ProduttoreRepository;

/**
 * Service Interface for managing Produttore.
 */
@Service
@Transactional(readOnly = true)
public class ProduttoreServiceExt {
  
  private final ProduttoreRepository repository;
  
  public ProduttoreServiceExt(ProduttoreRepository repository){
    this.repository = repository;
  }

   public Produttore findById(Long id){
     return repository.findOne(id);
   }
}
