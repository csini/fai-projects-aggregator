package it.fai.ms.efservice.service.jms.listener.contract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.message.contract.ContractCreatedMessage;
import it.fai.ms.efservice.service.jms.consumer.ContractCreatedConsumer;

@Service
@Transactional
public class JmsContractCreationFailedListener extends JmsObjectMessageListenerTemplate<ContractCreatedMessage> implements JmsTopicListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final ContractCreatedConsumer contractCreatedConsumer;

  @Autowired
  public JmsContractCreationFailedListener(final ContractCreatedConsumer _contractCreatedConsumer) throws Exception {
    contractCreatedConsumer = _contractCreatedConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.CONTRACT_SUSPENDED;
  }

  @Override
  protected void consumeMessage(ContractCreatedMessage contractCreatedMessage) {

    try {
      _log.info("Received jms message for contract suspended {}", contractCreatedMessage);
      contractCreatedConsumer.consume(contractCreatedMessage);
    } catch (final Exception _e) {
      _log.error("Error consuming message {}", contractCreatedMessage, _e);
      throw new RuntimeException(_e);
    }

  }
}
