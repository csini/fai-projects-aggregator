package it.fai.ms.efservice.service.fsm.config.frejus;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmCreateNewRequestIfCOnSostituzione;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardFurto;
import it.fai.ms.efservice.service.fsm.guard.GuardSmarrimento;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmMalfunzionamentoFrejusSostConfig.MOD_FREJUS_MALF_SOST)
public class FsmMalfunzionamentoFrejusSostConfig extends FsmRichiestaConfig {

  public static final String MOD_FREJUS_MALF_SOST = "MalfunzionamentoFrejusSost";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FsmSenderToQueue senderFsmService;

  public FsmMalfunzionamentoFrejusSostConfig(final FsmSenderToQueue _senderFsmService) {
    senderFsmService = _senderFsmService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MALFUNZIONAMENTO_FREJUS.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfMalfunzionamentoFrejus();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
      .source(StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS)
      .target(StatoRichiesta.ATTIVO)
      .event(RichiestaEvent.INITIAL)
      .action(new FsmActionGeneric())
      .and()
      .withExternal()// AUTOGUARD
      .source(StatoRichiesta.ATTIVO)
      .target(StatoRichiesta.FURTO)
      .guard(new GuardFurto())
      .action(new FsmActionChangeStatoDispositivo(senderFsmService, DispositivoEvent.BLOCCO_FURTO))
      .and()
      .withExternal()// AUTOGUARD
      .source(StatoRichiesta.ATTIVO)
      .target(StatoRichiesta.SMARRIMENTO)
      .guard(new GuardSmarrimento())
      .action(new FsmActionChangeStatoDispositivo(senderFsmService, DispositivoEvent.BLOCCO_SMARRIMENTO))
      .and()
      .withExternal()// AUTO
      .source(StatoRichiesta.ATTIVO)
      .target(StatoRichiesta.MALFUNZIONAMENTO)
      .action(new FsmActionChangeStatoDispositivo(senderFsmService, DispositivoEvent.BLOCCO_NO_DICHIARATO))
      .action(new FsmActionGeneric())
      .and()
      .withExternal()// AUTO
      .source(StatoRichiesta.FURTO)
      .target(StatoRichiesta.DA_INOLTRARE)
      .action(new FsmCreateNewRequestIfCOnSostituzione(senderFsmService))
      .and()
      .withExternal()// AUTO
      .source(StatoRichiesta.SMARRIMENTO)
      .target(StatoRichiesta.DA_INOLTRARE)
      .action(new FsmCreateNewRequestIfCOnSostituzione(senderFsmService))
      .and()
      .withExternal()// AUTO
      .source(StatoRichiesta.MALFUNZIONAMENTO)
      .target(StatoRichiesta.DA_INOLTRARE)
      .action(new FsmCreateNewRequestIfCOnSostituzione(senderFsmService))
      .and()
      .withExternal()// MANUAL
      .event(RichiestaEvent.MU_INOLTRATO)
      .source(StatoRichiesta.DA_INOLTRARE)
      .target(StatoRichiesta.EVASO)
      .action(new FsmActionGeneric())
      .and()
      .withExternal()// MANUAL
      .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
      .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
      .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
      .action(new FsmActionVerificaRientroDispositivo(senderFsmService))
      .and()
      .withExternal()
      .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
      .target(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
      .action(new FsmActionGeneric())
      .and()
      .withExternal()// MANUAL
      .source(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
      .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
      .event(RichiestaEvent.MU_LETTERA_VETTURA)
      .action(new FsmActionRientratoDispositivo(senderFsmService))
    ;
  }

}
