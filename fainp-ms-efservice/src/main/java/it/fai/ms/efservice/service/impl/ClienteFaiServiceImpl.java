package it.fai.ms.efservice.service.impl;

import it.fai.ms.efservice.service.ClienteFaiService;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import it.fai.ms.efservice.service.mapper.ClienteFaiMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ClienteFai.
 */
@Service
@Transactional
public class ClienteFaiServiceImpl implements ClienteFaiService {

    private final Logger log = LoggerFactory.getLogger(ClienteFaiServiceImpl.class);

    private final ClienteFaiRepository clienteFaiRepository;

    private final ClienteFaiMapper clienteFaiMapper;

    public ClienteFaiServiceImpl(ClienteFaiRepository clienteFaiRepository, ClienteFaiMapper clienteFaiMapper) {
        this.clienteFaiRepository = clienteFaiRepository;
        this.clienteFaiMapper = clienteFaiMapper;
    }

    /**
     * Save a clienteFai.
     *
     * @param clienteFaiDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ClienteFaiDTO save(ClienteFaiDTO clienteFaiDTO) {
        log.debug("Request to save ClienteFai : {}", clienteFaiDTO);
        ClienteFai clienteFai = clienteFaiMapper.toEntity(clienteFaiDTO);
        clienteFai = clienteFaiRepository.save(clienteFai);
        return clienteFaiMapper.toDto(clienteFai);
    }

    /**
     * Get all the clienteFais.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ClienteFaiDTO> findAll() {
        log.debug("Request to get all ClienteFais");
        return clienteFaiRepository.findAll().stream()
            .map(clienteFaiMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one clienteFai by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ClienteFaiDTO findOne(Long id) {
        log.debug("Request to get ClienteFai : {}", id);
        ClienteFai clienteFai = clienteFaiRepository.findOne(id);
        return clienteFaiMapper.toDto(clienteFai);
    }

    /**
     * Delete the clienteFai by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClienteFai : {}", id);
        clienteFaiRepository.delete(id);
    }
}
