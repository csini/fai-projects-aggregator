package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.OrdineClienteDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OrdineCliente.
 */
@RestController
@RequestMapping("/api")
public class OrdineClienteResource {

    private final Logger log = LoggerFactory.getLogger(OrdineClienteResource.class);

    private static final String ENTITY_NAME = "ordineCliente";

    private final OrdineClienteService ordineClienteService;

    public OrdineClienteResource(OrdineClienteService ordineClienteService) {
        this.ordineClienteService = ordineClienteService;
    }

    /**
     * POST  /ordine-clientes : Create a new ordineCliente.
     *
     * @param ordineClienteDTO the ordineClienteDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ordineClienteDTO, or with status 400 (Bad Request) if the ordineCliente has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ordine-clientes")
    @Timed
    public ResponseEntity<OrdineClienteDTO> createOrdineCliente(@RequestBody OrdineClienteDTO ordineClienteDTO) throws URISyntaxException {
        log.debug("REST request to save OrdineCliente : {}", ordineClienteDTO);
        if (ordineClienteDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new ordineCliente cannot already have an ID")).body(null);
        }
        OrdineClienteDTO result = ordineClienteService.save(ordineClienteDTO);
        return ResponseEntity.created(new URI("/api/ordine-clientes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ordine-clientes : Updates an existing ordineCliente.
     *
     * @param ordineClienteDTO the ordineClienteDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ordineClienteDTO,
     * or with status 400 (Bad Request) if the ordineClienteDTO is not valid,
     * or with status 500 (Internal Server Error) if the ordineClienteDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ordine-clientes")
    @Timed
    public ResponseEntity<OrdineClienteDTO> updateOrdineCliente(@RequestBody OrdineClienteDTO ordineClienteDTO) throws URISyntaxException {
        log.debug("REST request to update OrdineCliente : {}", ordineClienteDTO);
        if (ordineClienteDTO.getId() == null) {
            return createOrdineCliente(ordineClienteDTO);
        }
        OrdineClienteDTO result = ordineClienteService.save(ordineClienteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ordineClienteDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ordine-clientes : get all the ordineClientes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of ordineClientes in body
     */
    @GetMapping("/ordine-clientes")
    @Timed
    public List<OrdineClienteDTO> getAllOrdineClientes() {
        log.debug("REST request to get all OrdineClientes");
        return ordineClienteService.findAll();
        }

    /**
     * GET  /ordine-clientes/:id : get the "id" ordineCliente.
     *
     * @param id the id of the ordineClienteDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ordineClienteDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ordine-clientes/{id}")
    @Timed
    public ResponseEntity<OrdineClienteDTO> getOrdineCliente(@PathVariable Long id) {
        log.debug("REST request to get OrdineCliente : {}", id);
        OrdineClienteDTO ordineClienteDTO = ordineClienteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ordineClienteDTO));
    }

    /**
     * DELETE  /ordine-clientes/:id : delete the "id" ordineCliente.
     *
     * @param id the id of the ordineClienteDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ordine-clientes/{id}")
    @Timed
    public ResponseEntity<Void> deleteOrdineCliente(@PathVariable Long id) {
        log.debug("REST request to delete OrdineCliente : {}", id);
        ordineClienteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
