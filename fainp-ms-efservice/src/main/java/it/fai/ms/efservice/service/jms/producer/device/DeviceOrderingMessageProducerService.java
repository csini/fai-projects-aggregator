package it.fai.ms.efservice.service.jms.producer.device;

import it.fai.ms.efservice.domain.Richiesta;

public interface DeviceOrderingMessageProducerService {

  void deviceOrdering(Richiesta richiesta);

}
