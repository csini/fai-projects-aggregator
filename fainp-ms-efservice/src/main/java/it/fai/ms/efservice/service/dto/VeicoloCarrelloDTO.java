package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.List;

public class VeicoloCarrelloDTO implements Serializable {

  private static final long serialVersionUID = -3166019861151088325L;

  private String uuid;

  private List<String> nomeTipiServizio;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public List<String> getNomeTipiServizio() {
    return nomeTipiServizio;
  }

  public void setNomeTipiServizio(List<String> nomeTipiServizio) {
    this.nomeTipiServizio = nomeTipiServizio;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("VeicoloCarrelloDTO [");
    sb.append("UUID=")
      .append(uuid);
    sb.append(", nomeTipiServizio=")
      .append(nomeTipiServizio);
    sb.append("]");
    return sb.toString();
  }

}