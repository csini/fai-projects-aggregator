package it.fai.ms.efservice.service.impl;

import java.util.List;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.TelepassEuGiustificativoAggiuntoDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.service.RichiestaTemporaryBackofficeService;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.mapper.RichiestaMapper;

/**
 * Service Implementation for managing Richiesta.
 */
@Service
@Transactional
public class RichiestaTemporaryBackofficeServiceImpl implements RichiestaTemporaryBackofficeService {

  private final Logger log = LoggerFactory.getLogger(RichiestaTemporaryBackofficeServiceImpl.class);

  private final RichiestaService      richiestaService;
  private final RichiestaMapper       richiestaMapper;
  private final JmsTopicSenderService jmsTopicSenderService;
  private final FsmFactory            fsmFactory;

  private AbstractFsmRichiesta fsm = null;

  public RichiestaTemporaryBackofficeServiceImpl(RichiestaService richiestaService, RichiestaMapper richiestaMapper,
                                                 JmsTopicSenderService jmsTopicSenderService, FsmFactory fsmFactory) {
    this.richiestaService = richiestaService;
    this.richiestaMapper = richiestaMapper;
    this.jmsTopicSenderService = jmsTopicSenderService;
    this.fsmFactory = fsmFactory;
  }

  @Override
  public RichiestaDTO saveRichiestaForTemporayBackoffice(RichiestaDTO richiestaDTO) throws Exception {
    RichiestaDTO precedente = richiestaService.findOne(richiestaDTO.getId());
    TipoRichiesta tipoRichiesta = richiestaDTO.getTipo();
    StatoRichiesta statoPrecedente = precedente.getStato();
    StatoRichiesta statoAttuale = richiestaDTO.getStato();
    log.info("Stato precedente = " + statoPrecedente.name());
    log.info("Stato attuale = " + statoAttuale.name());

    if (statoPrecedente == statoAttuale) {
      log.info("CASO 1 - Lo stato non è stato cambiato");
      log.info("Lo stato precedente è uguale a quello attuale. Procedo con il semplice salvataggio dell'entità...");
      richiestaDTO = richiestaService.save(richiestaDTO);
      log.info("Richiesta salvato");
      return richiestaDTO;
    } else {
      log.info("CASO 2 - Lo stato è stato cambiato");
      boolean statoPossibile = isStatoPossibile(statoPrecedente, statoAttuale, tipoRichiesta);
      log.info("statoPossibile = " + statoPossibile);
      if (!statoPossibile) {
        String errorMsg = "Non è prevista la transizione di stato da " + statoPrecedente + " a " + statoAttuale;
        log.info("CASO 2A - Stato di destinazione non possibile, sollevo eccezione. " + errorMsg);
        throw new Exception(errorMsg);
      } else {
        log.info("CASO 2B - Lo stato di destinazione è possibile, quindi procedo con le valutazioni");
        boolean manualeSistema = isStatoManualeSistema(statoPrecedente, statoAttuale, tipoRichiesta);
        log.info("manualeSistema = " + manualeSistema);
        if (manualeSistema) {
          log.info("Caso 2B1 - La transizione è MANUAL SISTEMA");
          // MANUALE SISTEMA
          // (1) imposto stato ordine a precedente
          // (2) invio messaggio JMS opportuno
          richiestaDTO.setStato(statoPrecedente);
          log.info("Ho reimpostato lo stato dell'ordine a " + statoPrecedente + " prima di inviare il messaggio JMS");
          RichiestaEvent richiestaEvent = getRichiestaEvent(statoPrecedente, statoAttuale, tipoRichiesta);
          log.info("richiestaEvent = " + richiestaEvent);
          if (richiestaEvent == RichiestaEvent.MS_CTRRI) {
            log.info("Invio messaggio JMS per la transizione CTRRI");
          } else if (richiestaEvent == RichiestaEvent.MS_RIPETI_INVIO) {
            log.info("Invio messaggio JMS per la transizione RIPETI INVIO");
            try {
              jmsTopicSenderService.publishRipetiInvioMessage(richiestaDTO);
            } catch (Exception e) {
              log.error("Errore durante la pubblicazione del messaggio JMS", e);
              throw new Exception(e.getMessage());
            }
          } else if (richiestaEvent == RichiestaEvent.MS_ORDRI_RISIM) {
            log.error("CODE REMOVED by Demis");
          } else if (richiestaEvent == RichiestaEvent.MS_OBUNO) {
            log.error("CODE REMOVED by Demis");
          } else if (richiestaEvent == RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE) {
            TelepassEuGiustificativoAggiuntoDTO telepassEuGiustificativoAggiuntoDTO = new TelepassEuGiustificativoAggiuntoDTO();
            telepassEuGiustificativoAggiuntoDTO.setIdentificativoRichiesta(richiestaDTO.getIdentificativo());
            jmsTopicSenderService.publishGiustificativoMessage(telepassEuGiustificativoAggiuntoDTO);
          }
        } else {
          log.info("Caso 2B2 - La transizione è MANUAL UTENTE");
          // MANUALE UTENTE
          // (1) imposto stato ordine a precedente
          // (2) chiamo macchina per switch
          // (3) salvo ordine
          richiestaDTO.setStato(statoPrecedente);
          log.info("Ho reimpostato lo stato dell'ordine a " + statoPrecedente + " prima di effettuare il salvataggio");
          Richiesta ordineToSwitch = richiestaMapper.toEntity(richiestaDTO);
          log.info("Ho recuperato l'entity dal DTO -> " + ordineToSwitch);
          RichiestaEvent richiestaEvent = getRichiestaEvent(statoPrecedente, statoAttuale, tipoRichiesta);
          log.info("richiestaEvent = " + richiestaEvent);
          try {
            log.info("Eseguo il cambio stato...");
            ordineToSwitch = switchStato(ordineToSwitch, richiestaEvent);
            log.info("Ho effettuato il cambio stato -> " + ordineToSwitch);
          } catch (Exception e) {
            log.error("Errore", e);
            throw new Exception("Errore effettuando il passaggio di stato da " + statoPrecedente + " a " + statoAttuale);
          }
          richiestaDTO = richiestaMapper.toDto(ordineToSwitch);
          log.info("Ho recuperato il DTO dall'entity -> " + richiestaDTO);
          richiestaDTO = richiestaService.save(richiestaDTO);
          log.info("Richiesta salvato");
          return richiestaDTO;
        }
      }
    }

    return richiestaDTO;
  }

  private boolean isStatoPossibile(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale, TipoRichiesta tipoRichiesta) {
    fsm = getFsmByStatoPrecedente(statoPrecedente, tipoRichiesta);

    if (fsm == null) {
      return false;
    }

    return fsm.isStatoPossibile(statoPrecedente, statoAttuale);
  }

  private AbstractFsmRichiesta getFsmByStatoPrecedente(StatoRichiesta statoPrecedente, TipoRichiesta tipoRichiesta) {
    log.info("Retrieve FSM by Stato Precedente (di partenza): " + statoPrecedente + " for Tipo Richiesta: " + tipoRichiesta.name());
    if (StatoRichiestaUtil.getStateOfAccettazione()
                          .contains(statoPrecedente)) {
      log.info("Use Fsm Accettazione");
      return fsmFactory.getFsm(FsmCommand.CMD_IN_ACCETTAZIONE);
    }

    if (StatoRichiestaUtil.getStateOfInoltroTE()
                          .contains(statoPrecedente)) {
      log.info("Use Fsm Inoltro TE");
      return fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE);
    }

    if (StatoRichiestaUtil.getStateOfModificaTeFurtoSmarrimento()
                          .contains(statoPrecedente)) {
      log.info("Use Fsm Furto/smarrimento con o senza sostituzione");
      return fsmFactory.getFsm(FsmCommand.CMD_FURTO_NO_SOST_TE);
    }

    if (StatoRichiestaUtil.getStateOfTeRientroMalfunzionamento()
                          .contains(statoPrecedente)) {
      if (tipoRichiesta.name() == TipoRichiesta.MALFUNZIONAMENTO.name()) {
        log.info("Use Fsm Rientro/Malfunzionamento senza sostituzione");
        return fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE);
      }
    }

    if (StatoRichiestaUtil.getStateOfTeVarTargaRitargatoMalfSostituzione()
                          .contains(statoPrecedente)) {
      if (tipoRichiesta.name() == TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE.name()
          || tipoRichiesta.name() == TipoRichiesta.MEZZO_RITARGATO.name()
          || tipoRichiesta.name() == TipoRichiesta.VARIAZIONE_TARGA.name()) {
        log.info("Use Fsm Variazione Targa/Mezzo ritargato/Malfunzionamento con sostituzione");
        return fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TE);
      }
    }

    if (StatoRichiestaUtil.getStateOfRescRiattSospensione()
                          .contains(statoPrecedente)) {
      log.info("Use Fsm Rescissione/sospensione/riattivazione");
      return fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TE);
    }

    log.info("Not found FSM to use");
    return null;
  }

  private boolean isStatoManualeSistema(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale, TipoRichiesta tipoRichiesta) {
    fsm = getFsmByStatoPrecedente(statoPrecedente, tipoRichiesta);

    if (fsm == null) {
      return false;
    }

    boolean statoManualeSistema = fsm.isStatoManualeSistema(statoPrecedente, statoAttuale);
    log.info("Passaggio di stato is manual sistema: " + statoManualeSistema);
    return statoManualeSistema;
  }

  private RichiestaEvent getRichiestaEvent(StatoRichiesta statoPrecedente, StatoRichiesta statoAttuale, TipoRichiesta tipoRichiesta) {
    fsm = getFsmByStatoPrecedente(statoPrecedente, tipoRichiesta);

    if (fsm == null) {
      return null;
    }

    return fsm.getRichiestaEvent(statoPrecedente, statoAttuale);
  }

  private Richiesta switchStato(Richiesta ordine, RichiestaEvent richiestaEvent) throws Exception {
    List<String> userAvailableCommand = fsm.getUserAvailableCommand(ordine);
    boolean find = false;
    for (String availableCommandStr : userAvailableCommand) {
      log.info("AvailableCommand: " + availableCommandStr + " equals to " + richiestaEvent.name());
      if (richiestaEvent.name()
                        .equals(availableCommandStr)) {
        log.info(availableCommandStr + " is user command");
        find = true;
      }
    }

    if (find) {
      String notaUtente = "Cambio di stato eseguito da backoffice in data " + Instant.now();
      ordine = fsm.executeCommandToChangeState(richiestaEvent, ordine, notaUtente);
    } else {
      ordine = fsm.executeCommandToChangeState(richiestaEvent, ordine);
    }

    return ordine;
  }

}
