package it.fai.ms.efservice.service.fsm.config.fai;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.config.FsmModificaSimpleConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaGoBoxConfig.MODIFICA_GO_BOX)
public class FsmModificaGoBoxConfig extends FsmModificaSimpleConfig {

  public static final String MODIFICA_GO_BOX = "modificaGoBox";

  private final Logger log = LoggerFactory.getLogger(getClass());

  public FsmModificaGoBoxConfig(FsmSenderToQueue fsmSenderToQueue) {
    super(fsmSenderToQueue);
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  protected String getMachineId() {
    return FsmType.MODIFICA_GOBOX.fsmName();
  }

  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_GOBOX;
  }

  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfModificaGoBox();
  }

}
