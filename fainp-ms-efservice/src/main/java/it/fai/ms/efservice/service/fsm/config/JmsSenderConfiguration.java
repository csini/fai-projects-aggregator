package it.fai.ms.efservice.service.fsm.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
public class JmsSenderConfiguration {

  private Logger _log = LoggerFactory.getLogger(getClass());

  @PostConstruct
  public void init() {
    _log.debug("FsmSenderToQueue Configuration conf");
  }

  @Bean
  public FsmSenderToQueue newSenderFsmUtil(JmsTopicSenderService jmsTopicSenderService) {
    FsmSenderToQueue senderFsmUtil = new FsmSenderToQueue(jmsTopicSenderService);
    return senderFsmUtil;
  }
}
