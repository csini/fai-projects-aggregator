package it.fai.ms.efservice.web.rest;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.service.DeliveryAsyncServiceExt;
import it.fai.ms.efservice.service.DeliveryDevicesAsyncServiceExt;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;

@RestController
@RequestMapping(SpedizioniResourceExt.BASE_PATH)
public class SpedizioniResourceExt {
  private final Logger log = LoggerFactory.getLogger(SpedizioniResourceExt.class);

  public static final String BASE_PATH          = "/api/public";
  public static final String DELIVERY           = "/delivery";
  public static final String DELIVERY_PRINT     = DELIVERY + "/print";
  public static final String DELIVERY_PRINT_ALL = DELIVERY + "/printAll";

  public static final String DELIVERY_DEVICES_PRINT     = DELIVERY + "/devices/print";
  public static final String DELIVERY_DEVICES_PRINT_ALL = DELIVERY + "/devices/printAll";

  public static final String DELIVERY_PRINT_PRODUTTORE     = DELIVERY + "/printProduttore";
  public static final String DELIVERY_PRINT_PRODUTTORE_ALL = DELIVERY + "/printProduttoreAll";
  public static final String DELIVERY_POLLING              = DELIVERY + "/polling";

  private static final String KEY_JOB                           = "job";
  private static final String PRINT_DELIVERY_KEY_JOB            = "JOB_DELIVERY_PRINT";
  private static final String PRINT_DELIVERY_DEVICE_KEY_JOB            = "JOB_DELIVERY_DEVICE_PRINT";
  private static final String PRINT_DELIVERY_PRODUTTORE_KEY_JOB = "JOB_DELIVERY_PRINT_PRODUTTORE";

  private final DeliveryAsyncServiceExt        deliveryAsyncService;
  private final DeliveryDevicesAsyncServiceExt deliveryDevicesAsyncService;
  private final AsyncPollingJobServiceImpl     asyncPollingJobService;

  public SpedizioniResourceExt(DeliveryAsyncServiceExt deliveryAsyncService, AsyncPollingJobServiceImpl asyncPollingJobService,
                               final DeliveryDevicesAsyncServiceExt deliveryDevicesAsyncService) {
    this.deliveryAsyncService = deliveryAsyncService;
    this.asyncPollingJobService = asyncPollingJobService;
    this.deliveryDevicesAsyncService = deliveryDevicesAsyncService;
  }

  @PostMapping({ DELIVERY_PRINT, DELIVERY_PRINT_ALL })
  @Timed
  public ResponseEntity deliveryPrint(@RequestBody DeliveryPrintDTO dto) throws Exception {
    log.debug("REST print a delivery with dto : {}", dto);
    String identifierJob = deliveryAsyncService.getKeyCacheAndPushDefaultValue(PRINT_DELIVERY_KEY_JOB);

    deliveryAsyncService.processAsyncDelivery(identifierJob, dto);

    return ResponseEntity.ok(Collections.singletonMap(KEY_JOB, identifierJob));
  }

  @PostMapping({ DELIVERY_PRINT_PRODUTTORE, DELIVERY_PRINT_PRODUTTORE_ALL })
  @Timed
  public ResponseEntity deliveryPrintProduttore(@RequestBody DeliveryPrintDTO dto) throws Exception {
    log.debug("REST print a delivery with dto : {}", dto);
    String identifierJob = deliveryAsyncService.getKeyCacheAndPushDefaultValue(PRINT_DELIVERY_PRODUTTORE_KEY_JOB);

    deliveryAsyncService.processAsyncDeliveryProduttore(identifierJob, dto);

    return ResponseEntity.ok(Collections.singletonMap(KEY_JOB, identifierJob));
  }

  @PostMapping({ DELIVERY_DEVICES_PRINT, DELIVERY_DEVICES_PRINT_ALL })
  @Timed
  public ResponseEntity deliveryDevicesPrint(@RequestBody DeliveryPrintDTO dto) throws Exception {
    log.debug("REST print a delivery with dto : {}", dto);
    String identifierJob = deliveryAsyncService.getKeyCacheAndPushDefaultValue(PRINT_DELIVERY_DEVICE_KEY_JOB);

    deliveryDevicesAsyncService.processAsyncDeliveryDevices(identifierJob, dto);

    return ResponseEntity.ok(Collections.singletonMap(KEY_JOB, identifierJob));
  }

  @GetMapping(DELIVERY_POLLING + "/{identificativo}")
  @Timed
  public ResponseEntity<AsyncJobResponse> pollingDelivery(@PathVariable String identificativo) {
    log.debug("Rest request for polling print delivery {}", identificativo);
    AsyncJobResponse ouput = (AsyncJobResponse) asyncPollingJobService.getValue(identificativo);
    log.info("Response polling is {}", ouput);
    return ResponseEntity.ok(ouput);
  }
}
