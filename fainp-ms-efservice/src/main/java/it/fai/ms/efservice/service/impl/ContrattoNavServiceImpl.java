package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.message.contract.ContractClosingMessage;
import it.fai.ms.common.jms.efservice.message.contract.ContractRestoringMessage;
import it.fai.ms.common.jms.efservice.message.contract.ContractSuspensionMessage;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.service.ContrattoNavService;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;

@Service
@Transactional
public class ContrattoNavServiceImpl implements ContrattoNavService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final static String CODE_ANOMALY_CONTRACT = "FSM_CTR001";

  private final static String PRODUTTORE_TELEPASS = "TELEPASS";

  private final static String PRODUTTORE_FREJUS = "TES_TRA_FREJUS_MONTE_BIANCO";

  private final static List<String> suspendableProducer = new ArrayList<>(Arrays.asList(PRODUTTORE_TELEPASS, PRODUTTORE_FREJUS));

  private final ContrattoServiceExt contrattoService;

  private final FsmDispositivo fsmDevice;

  private final NotificationService notificationService;

  private final JmsProperties jmsProperties;

  public ContrattoNavServiceImpl(final ContrattoServiceExt _contrattoService, final FsmDispositivo _fsmDevice,
                                 final NotificationService _notificationService, final JmsProperties _jmsProperties) {
    contrattoService = _contrattoService;
    fsmDevice = _fsmDevice;
    notificationService = _notificationService;
    jmsProperties = _jmsProperties;
  }

  @Override
  public List<ContrattoNavDTO> findContractsToNav(String codiceCliente) {
    List<Contratto> contracts = (StringUtils.isNotBlank(codiceCliente)) ? contrattoService.findByCodiceCliente(codiceCliente)
                                                                        : contrattoService.findAll();
    List<ContrattoNavDTO> contractsDTO = contracts.parallelStream()
                                                  .map(c -> mapContractToNavDto(c))
                                                  .collect(toList());
    log.debug("Found {} contracts", (contractsDTO != null) ? contractsDTO.size() : 0);
    return contractsDTO;
  }

  @Override
  public Contratto changeStatusContractByNav(String identificativo, ContrattoEvent event) throws Exception {
    Optional<Contratto> contractOpt = contrattoService.findByIdentificativo(identificativo);
    Contratto contract = new Contratto();
    if (contractOpt.isPresent()) {
      contract = contractOpt.get();
      boolean isAvailable = contrattoService.isAvailableCommandFsm(contract, event);
      if (isAvailable) {
        contract = contrattoService.fsmChangeStatus(contract, event);
        try {
          if (isRevokeEvent(event)) {
            revokeDevices(contract);
          }
          sendAnomalyOrEventToProduttore(contract, event);
        } catch (Exception e) {
          throw new Exception(e);
        }
      }
    } else {
      throw new Exception("Command [" + event + "] is not available to contract: " + contract);
    }
    return contract;
  }

  @Override
  public StateChangeStatusFsm changeAsyncStatusContractByNav(Set<String> identifierContracts, ContrattoEvent event) {
    identifierContracts.forEach(identifier -> {
      try {
        changeStatusContractByNav(identifier, event);
      } catch (Exception exp) {
        throw new RuntimeException(exp);
      }
    });
    return StateChangeStatusFsm.SUCCESS;
  }

  private void sendAnomalyOrEventToProduttore(Contratto contract, ContrattoEvent event) {
    boolean isTelepass = isProductor(contract, PRODUTTORE_TELEPASS);
    boolean isFrejus = isProductor(contract, PRODUTTORE_FREJUS);
    if (isTelepass) {
      sendEventToTelepass(contract, event);
    } else if (isFrejus) {
      sendAnomalyFrejus(contract, event);
    } else {
      log.warn("Skip because produttore is not {} or {}", PRODUTTORE_TELEPASS, PRODUTTORE_FREJUS);
    }
  }

  private void sendAnomalyFrejus(Contratto contract, ContrattoEvent event) {
    Map<String, Object> parameters = createParametersToSendAnomaly(contract, event);
    notificationService.notify(CODE_ANOMALY_CONTRACT, parameters);
  }

  private Map<String, Object> createParametersToSendAnomaly(Contratto contract, ContrattoEvent event) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("contract_code", contract.getCodContrattoCliente());
    String codiceCliente = contract.getClienteFai() != null ? contract.getClienteFai()
                                                                      .getCodiceCliente()
                                                            : "";
    parameters.put("client_code", codiceCliente);
    String actionEvent = convertEventToNotifyAction(event);
    parameters.put("action", actionEvent);
    String produttoreNome = contract.getProduttore() != null ? contract.getProduttore()
                                                                       .getNome()
                                                             : "";
    parameters.put("producerName", produttoreNome);
    return parameters;
  }

  private String convertEventToNotifyAction(ContrattoEvent event) {
    String actionEvent = "";
    switch (event) {
    case MS_RIATTIVA:
      actionEvent = "RIPRISTINATO";
      break;
    case MU_REVOCA:
      actionEvent = "REVOCATO";
      break;
    case MS_SOSPENDI:
      actionEvent = "SOSPESO";
      break;
    default:
      break;
    }
    return actionEvent;
  }

  private boolean isRevokeEvent(ContrattoEvent event) {
    return event.equals(ContrattoEvent.MU_REVOCA);
  }

  private void revokeDevices(Contratto contract) {
    Set<Dispositivo> devices = contract.getDispositivo();
    devices.forEach(device -> {
      try {
        StatoDispositivo statoAttuale = device.getStato();
        Dispositivo newDevice = fsmDevice.executeCommandToChangeState(DispositivoEvent.REVOCA_DA_CONTRATTO, device);
        if (isNotEqualState(newDevice.getStato(), statoAttuale)) {
          log.debug("Change status on device: {}", newDevice);
        }
      } catch (Exception e) {
        log.error("Exception on change status to device: {}", device, e);
        throw new RuntimeException(e);
      }
    });
  }

  private boolean isNotEqualState(StatoDispositivo newState, StatoDispositivo actualState) {
    return !newState.equals(actualState);
  }

  private void sendEventToTelepass(Contratto contract, ContrattoEvent event) {
    log.info("Send contract {} to Telepass to event : {}", contract, event);
    String codContrattoCliente = contract.getCodContrattoCliente();
    String codiceCliente = contract.getClienteFai() != null ? contract.getClienteFai()
                                                                      .getCodiceCliente()
                                                            : null;
    if (StringUtils.isNotBlank(codContrattoCliente)) {
      ContractOrderRequest contractUuid = new ContractOrderRequest(codContrattoCliente, codiceCliente);
      switch (event) {
      case MS_SOSPENDI:
        log.info("Contract {} send suspension message to TELEPASS  contract", codContrattoCliente);
        publishContractEventMessage(JmsTopicNames.CONTRACT_SUSPENSION, new ContractSuspensionMessage(contractUuid));
        break;
      case MS_RIATTIVA:
        log.info("Contract {} send restoring message to TELEPASS  contract", codContrattoCliente);
        publishContractEventMessage(JmsTopicNames.CONTRACT_RESTORING, new ContractRestoringMessage(contractUuid));
        break;
      case MU_REVOCA:
        log.info("Contract {} send revoke/closing message to TELEPASS  contract", codContrattoCliente);
        publishContractEventMessage(JmsTopicNames.CONTRACT_CLOSING, new ContractClosingMessage(contractUuid));
        break;
      }
    } else {
      log.info("Skip send Contract to TELEPASS because contract hasn't CONTRACT CODE to event: {}", event);
    }
  }

  private void publishContractEventMessage(JmsTopicNames topic, Serializable message) {
    JmsTopicSenderUtil.publish(jmsProperties, topic, message);
  }

  private boolean isProductor(Contratto contract, String productorName) {
    if (contract != null && contract.getProduttore() != null && contract.getProduttore()
                                                                        .getNome()
                                                                        .equalsIgnoreCase(productorName)) {
      return true;
    }
    return false;
  }

  private ContrattoNavDTO mapContractToNavDto(Contratto c) {
    ContrattoNavDTO contractNav = new ContrattoNavDTO().identificativo(c.getIdentificativo())
                                                       .codiceContratto(c.getCodContrattoCliente())
                                                       .codiceCliente("")
                                                       .produttore("")
                                                       .stato(c.getStato()
                                                               .name())
                                                       .sospendibile(false);
    ClienteFai clienteFai = c.getClienteFai();
    if (clienteFai != null) {
      contractNav.setCodiceCliente(clienteFai.getCodiceCliente());
    }

    Produttore produttore = c.getProduttore();
    if (produttore != null) {
      String produttoreName = produttore.getNome();
      contractNav.setProduttore(produttoreName);
      if (suspendableProducer.contains(produttoreName)) {
        contractNav.setSospendibile(true);
      }
    }
    return contractNav;
  }

}
