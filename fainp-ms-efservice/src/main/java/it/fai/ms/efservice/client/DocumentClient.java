package it.fai.ms.efservice.client;

import static it.fai.ms.efservice.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import com.codahale.metrics.annotation.Timed;

import io.swagger.annotations.ApiParam;
import it.fai.ms.common.dml.document.DeliveryPrintProduttoreRequestDTO;
import it.fai.ms.common.dml.document.DeliveryPrintRequestDTO;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;

@FeignClient(name = "faidocument", decode404 = true)
public interface DocumentClient {

  static final String BASE_PATH                           = "/api";
  static final String READ_CSV                            = "/public/readcsv";
  static final String DELIVERY_PRINT                      = "/delivery/print";
  static final String DELIVERY_PRINT_PRODUTTORE           = "/delivery/printProduttore";
  static final String READ_CSV_COLUMN                     = BASE_PATH + READ_CSV + "/column";
  static final String READ_CSV_DISPOSITIVI_DEPOSITO       = BASE_PATH + READ_CSV + "/dispositivideposito";
  static final String API_CSV_DISPOSITIVI_DEPOSITO        = BASE_PATH + READ_CSV_DISPOSITIVI_DEPOSITO;
  static final String READ_CSV_DISPOSITIVI_DEPOSITIO_LIST = READ_CSV + "/dispositividepositoList";
  static final String API_CSV_DISPOSITIVI_DEPOSITO_LIST   = BASE_PATH + READ_CSV_DISPOSITIVI_DEPOSITIO_LIST;
  static final String API_DELIVERY_PRINT                  = BASE_PATH + DELIVERY_PRINT;
  static final String API_DELIVERY_PRINT_PRODUTTORE       = BASE_PATH + DELIVERY_PRINT_PRODUTTORE;
  static final String FIND_DOCUMENT                       = BASE_PATH + "/public/findLastDocument";
  static final String GET_DOCUMENTS_BY_UUID_AND_TYPE      = BASE_PATH + "/public/documents/uuid-type";

  String                     REPORT_BASE         = "/report";
  static final String        API_REPORT_SAVE     = BASE_PATH + REPORT_BASE + "/saveCSV";
  public static final String API_REPORT_DOWNLOAD = REPORT_BASE + "/download";

  @GetMapping(API_CSV_DISPOSITIVI_DEPOSITO + "/{uuidDocumento}")
  @Timed
  Map<String, String> getMapFromFile1(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                      @PathVariable("uuidDocumento") String uuidDocumento);

  @GetMapping(API_CSV_DISPOSITIVI_DEPOSITO_LIST + "Semicolon" + "/{uuidCsvFile}")
  @Timed
  public List<String[]> getListSerialDeviceFromCSVSemicolon(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                                            @ApiParam(
                                                                      value = "Identificativo documento",
                                                                      required = true) @PathVariable("uuidCsvFile") String uuidCsvFile);

  @GetMapping(API_CSV_DISPOSITIVI_DEPOSITO_LIST + "/{uuidCsvFile}")
  @Timed
  public List<String[]> getListFromFile(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                        @ApiParam(
                                                  value = "Identificativo documento",
                                                  required = true) @PathVariable("uuidCsvFile") String uuidCsvFile);

  @GetMapping(READ_CSV_COLUMN + "/{uuidDocum1ento}/{columnIndex}")
  @Timed
  List<String> getColumnListFromFileCsv(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                        @PathVariable("uuidDocumento") String uuidDocumento,
                                        @PathVariable("columnIndex") Integer columnIndex);

  @PostMapping(API_DELIVERY_PRINT)
  @Timed
  Map<String, String> printDelivery(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                    @RequestBody DeliveryPrintRequestDTO dto);

  @PostMapping(API_DELIVERY_PRINT_PRODUTTORE)
  @Timed
  Map<String, String> printDeliveryProduttore(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                              @RequestBody DeliveryPrintProduttoreRequestDTO dto);

  @GetMapping(FIND_DOCUMENT + "/{codiceAzienda}/{tipologiaDocumento}")
  @Timed
  DocumentoDTO findLastDocument(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                @PathVariable("codiceAzienda") String codiceAzienda,
                                @PathVariable("tipologiaDocumento") String tipologiaDocumento, @RequestParam("targa") String targa,
                                @RequestParam("targaNazione") String targaNazione);

  @GetMapping(GET_DOCUMENTS_BY_UUID_AND_TYPE + "/{uuid}/{tipo}")
  @Timed
  DocumentoDTO findDocumentoByIdentificativoAndTipo(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                                    @PathVariable("uuid") String uuid, @PathVariable("tipo") String tipo);

  public static final String API_REPORT_SAVE_TEXT = BASE_PATH + REPORT_BASE + "/saveCSVText";

  @GetMapping(API_REPORT_SAVE_TEXT + "/{fileName}")
  @Timed
  public String saveReportCsvFromText(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                      @PathVariable("fileName") String fileName, @RequestBody String csv);

}
