package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.efservice.service.ViaCardServiceExt;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

/**
 * REST controller for managing ViaCard.
 */
@RestController
@RequestMapping("/api/public")
public class ViaCardResourceExt {

  private final Logger log = LoggerFactory.getLogger(ViaCardResourceExt.class);

  private final static String VIACARD_NECESSARY = "/viacardnecessary";

  private final ViaCardServiceExt viaCardServiceExt;

  public ViaCardResourceExt(final ViaCardServiceExt _viaCardServiceExt) {
    viaCardServiceExt = _viaCardServiceExt;
  }

  /**
   * GET /viacardnecessary
   * 
   * @throws CustomException
   */
  @GetMapping(VIACARD_NECESSARY + "/{codiceCliente}/{numViaCard}/{numTelepassIta}")
  @Timed
  public ResponseEntity<Long> getNumberOfViaCardNecessary(@PathVariable String codiceCliente, @PathVariable long numViaCard,
                                                          @PathVariable long numTelepassIta) throws URISyntaxException, CustomException {
    long numOfViaCardNecesary = 0;
    log.debug("Request for number of ViaCard necessary for Order by: CodiceCliente: {} - numViaCard: {} - numTelepassIta: {}",
              codiceCliente, numViaCard, numTelepassIta);
    if (StringUtils.isBlank(codiceCliente) || numViaCard < 0 || numTelepassIta < 0) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    log.info("Richiesta effettuata per il codice cliente: {}", codiceCliente);
    numOfViaCardNecesary = viaCardServiceExt.getNumberOfViaCardNecessaryForOrder(codiceCliente, numViaCard, numTelepassIta);
    log.info("Request response: {} for client code {}", numOfViaCardNecesary, codiceCliente);
    return ResponseEntity.ok(numOfViaCardNecesary);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
