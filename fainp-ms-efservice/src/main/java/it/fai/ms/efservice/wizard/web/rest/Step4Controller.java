package it.fai.ms.efservice.wizard.web.rest;

import static it.fai.ms.efservice.wizard.model.WizardVehicleId.mapToWizardVehicleIds;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.dto.WizardVehicleDTO;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixVehicle;
import it.fai.ms.efservice.wizard.service.Step4Service;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixDeviceType;
import it.fai.ms.efservice.wizard.web.rest.vm.step4.MatrixStep4;
import it.fai.ms.efservice.wizard.web.rest.vm.step4.MatrixStep4Vehicle;

@RestController
@RequestMapping("/api/wizard/step4")
public class Step4Controller {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private Step4Service step4Service;

  private enum Possesso {
    IN_SCADENZA, SCADUTO;
  }

  @Autowired
  public Step4Controller(final Step4Service _step4Service) {
    step4Service = _step4Service;
  }

  @PostMapping("matrix/{serviceId}/{codiceCliente}")
  public ResponseEntity<MatrixStep4> getMatrix(final @PathVariable(name = "serviceId") @NotNull String _serviceId,
                                               final @RequestBody @NotNull Set<String> _vehicleIds,
                                               final @PathVariable(name = "codiceCliente") @NotNull String codiceCliente) {

    _log.info("Creating matrix for serviceId {} and vehicleIds {} - CodiceCliente: {}", _serviceId, _vehicleIds, codiceCliente);

    final MatrixStep4 matrix = convertWizardStep4Matrix(step4Service.buildMatrix(new WizardServiceTypeId(_serviceId),
                                                                                 mapToWizardVehicleIds(_vehicleIds), codiceCliente));

    _log.info("Matrix created (total items: {})", matrix.getContent()
                                                        .size());
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(matrix);
  }

  @PostMapping("/completematrix/{codiceCliente}")
  public ResponseEntity<MatrixStep4> postCompleteMatrix(final @RequestBody @NotNull List<WizardVehicleDTO> wizardVehiclesDTO,
                                                        final @PathVariable(name = "codiceCliente") @NotNull String codiceCliente) {
    return this.getCompleteMatrix(wizardVehiclesDTO, codiceCliente);
  }

  @GetMapping("/completematrix/{codiceCliente}")
  public ResponseEntity<MatrixStep4> getCompleteMatrix(@ApiParam(
                                                                 value = "uuids",
                                                                 required = true) @RequestParam(
                                                                                                required = true) List<WizardVehicleDTO> wizardVehiclesDTO,
                                                       final @PathVariable(name = "codiceCliente") @NotNull String codiceCliente) {
    _log.debug("Creating matrix for serviceId and WzardVehiclesDTO {} - codiceCliente: {}", wizardVehiclesDTO, codiceCliente);
    final long time1 = System.currentTimeMillis();

    final MatrixStep4 matrix = convertWizardStep4Matrix(step4Service.buildCompleteMatrix(wizardVehiclesDTO, codiceCliente));
    _log.info("Matrix created (total items: {} in {} ms)", matrix.getContent()
                                                                 .size(),
              System.currentTimeMillis() - time1);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.noStore())
                         .body(matrix);
  }

  private MatrixStep4 convertWizardStep4Matrix(final WizardStep4Matrix _wizardStep4Matrix) {
    final MatrixStep4 matrix = new MatrixStep4();
    final AtomicInteger sequence = newSequence();

    final List<MatrixStep4Vehicle> matrixStep4Vehicles = _wizardStep4Matrix.getVehicles()
                                                                           .stream()
                                                                           .map(wizardStep4MatrixVehicle -> mapToMatrixStep4Vehicle(sequence,
                                                                                                                                    wizardStep4MatrixVehicle))
                                                                           .collect(toList());

    matrix.getHeader()
          .addAll(extractHeaders(_wizardStep4Matrix.getVehicles()));
    matrix.getContent()
          .addAll(matrixStep4Vehicles);
    return matrix;
  }

  private Set<String> extractHeaders(final Set<WizardStep4MatrixVehicle> _wizardStep4MatrixVehicles) {
    return _wizardStep4MatrixVehicles.stream()
                                     .map(wizardStep4MatrixVehicle -> wizardStep4MatrixVehicle.getDeviceTypes()
                                                                                              .values()
                                                                                              .stream()
                                                                                              .map(wizardStep4MatrixDeviceType -> wizardStep4MatrixDeviceType.getId())
                                                                                              .collect(toList()))
                                     .flatMap(Collection::stream)
                                     .distinct()
                                     .sorted(TipoDispositivoEnum.ordinamentoDispositiviComparator())
                                     .collect(toCollection(LinkedHashSet::new));
  }

  private AtomicInteger newSequence() {
    return new AtomicInteger(1);
  }

  public MatrixStep4Vehicle mapToMatrixStep4Vehicle(AtomicInteger _sequence, WizardStep4MatrixVehicle _wizardStep4MatrixVehicle) {
    final MatrixStep4Vehicle matrixStep4Vehicle = new MatrixStep4Vehicle(_wizardStep4MatrixVehicle.getUuid());
    matrixStep4Vehicle.setEuro(_wizardStep4MatrixVehicle.getEuroClass());
    String licensePlate = _wizardStep4MatrixVehicle.getLicensePlate();
    matrixStep4Vehicle.setTarga(licensePlate);
    matrixStep4Vehicle.setTipo(_wizardStep4MatrixVehicle.getType());
    matrixStep4Vehicle.setTargaNazione(_wizardStep4MatrixVehicle.getTargaNazione());

    if (isExpiredAndHasDevicesActive(_wizardStep4MatrixVehicle)) {
      _log.debug("Vehicle {} ownership is EXPIRED and has devices active", licensePlate);
      matrixStep4Vehicle.setPossesso(Possesso.SCADUTO.name());
    } else if (isExpiringAndHasDevicesActive(_wizardStep4MatrixVehicle)) {
      _log.debug("Vehicle {} ownership is EXPIRING and has devices active", licensePlate);
      matrixStep4Vehicle.setPossesso(Possesso.IN_SCADENZA.name());
    }

    matrixStep4Vehicle.getDispositivi()
                      .putAll(mapToMatrixDeviceTypes(_sequence, _wizardStep4MatrixVehicle));
    setAnomalyAndErrors(matrixStep4Vehicle);
    return matrixStep4Vehicle;
  }

  private boolean isExpiringAndHasDevicesActive(WizardStep4MatrixVehicle _wizardStep4MatrixVehicle) {
    return _wizardStep4MatrixVehicle.isExpiring() && _wizardStep4MatrixVehicle.isHasDevicesActive();
  }

  private boolean isExpiredAndHasDevicesActive(WizardStep4MatrixVehicle _wizardStep4MatrixVehicle) {
    return _wizardStep4MatrixVehicle.isExpired() && _wizardStep4MatrixVehicle.isHasDevicesActive();

  }

  private Map<String, MatrixDeviceType> mapToMatrixDeviceTypes(final AtomicInteger _sequence,
                                                               final WizardStep4MatrixVehicle _wizardStep4MatrixVehicle) {
    return _wizardStep4MatrixVehicle.getDeviceTypes()
                                    .values()
                                    .stream()
                                    .collect(toMap(wizardStep4MatrixDeviceType -> wizardStep4MatrixDeviceType.getId(),
                                                   wizardStep4MatrixDeviceType -> {
                                                     final MatrixDeviceType matrixDeviceType = new MatrixDeviceType(wizardStep4MatrixDeviceType.getId());
                                                     matrixDeviceType.setState(wizardStep4MatrixDeviceType.getActivability()
                                                                                                          .getActivabilityState()
                                                                                                          .name());
                                                     matrixDeviceType.setItemKey(String.valueOf(_sequence.getAndIncrement()));
                                                     if (wizardStep4MatrixDeviceType.isNotActivable()) {
                                                       Optional.ofNullable(wizardStep4MatrixDeviceType.getActivability()
                                                                                                      .getActivabilityFailures())
                                                               .ifPresent(allFailures -> {
                                                                 allFailures.stream()
                                                                            .map(f -> new MatrixActivabilityFailure(f.getCode(),
                                                                                                                    f.getMess()))
                                                                            .forEach(failure -> {
                                                                              matrixDeviceType.addFailure(failure);
                                                                            });
                                                               });
                                                     }
                                                     return matrixDeviceType;
                                                   }));
  }

  private void setAnomalyAndErrors(MatrixStep4Vehicle matrixStep4Vehicle) {
    Map<String, MatrixDeviceType> dispositivi = matrixStep4Vehicle.getDispositivi();
    Set<String> keySet = dispositivi.keySet();
    keySet.stream()
          .map(key -> dispositivi.get(key))
          .filter(mtx -> mtx != null && ActivabilityState.VEHICLE_ANOMALY.name()
                                                                         .equals(mtx.getStato()))
          .findFirst()
          .ifPresent(mtx -> {
            mtx.getErrors()
               .stream()
               .findFirst()
               .ifPresent(matrixActivabilityFailure -> {
                 matrixStep4Vehicle.setAnomaly(true);
                 matrixStep4Vehicle.addFailure(matrixActivabilityFailure);
               });
          });
  }

}
