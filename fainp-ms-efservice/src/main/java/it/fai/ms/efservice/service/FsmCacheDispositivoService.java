package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.FsmCacheDispositivo;
import it.fai.ms.efservice.repository.FsmCacheDispositivoRepository;

@Service
@Transactional
public class FsmCacheDispositivoService {

  private final Logger log = LoggerFactory.getLogger(getClass());
  
  private FsmCacheDispositivoRepository fsmRepo;
  
  public FsmCacheDispositivoService(final FsmCacheDispositivoRepository fsmRepo) {
    this.fsmRepo = fsmRepo;
  }
  
  public void put(Long key, String json) {
    log.debug("Put key [{}] and value [{}] on DB", key, json);
    FsmCacheDispositivo fsmCache = new FsmCacheDispositivo(key, json);
    Optional<FsmCacheDispositivo> optFsmCache = find(key);
    if(optFsmCache.isPresent()) {
      log.debug("New object on DB");
      fsmCache = optFsmCache.get();
      fsmCache.setData(json);
    }
    fsmRepo.save(fsmCache);
  }
  
  public void put(FsmCacheDispositivo fsmCache) {
    fsmRepo.save(fsmCache);
  }
  
  public void putAll(List<FsmCacheDispositivo> fsmCaches) {
    fsmRepo.save(fsmCaches);
  }
  
  public String get(Long key) {
    log.debug("Get key [{}] from DB");
    String json = null;
    Optional<FsmCacheDispositivo> optFsmCache = find(key);
    if(optFsmCache.isPresent()) {
      json = optFsmCache.get().getData();
    }
    log.debug("Found value [{}] with key [{}] from DB", json, key);
    return json;
  }
  
  private Optional<FsmCacheDispositivo> find(Long key) {
    log.debug("Find {} with key {}", FsmCacheDispositivo.class.getSimpleName(), key);
    return Optional.ofNullable(fsmRepo.findOne(key));
  }
  
  public void remove(Long key) {
    if(StringUtils.isNotBlank(get(key))) {
      log.info("Delete key: {}", key);
      fsmRepo.delete(key);
    } else {
      log.debug("Not found row with key {} to delete", key);
    }
  }
  
  public void removeAll() {
    log.info("Delete all...");
    fsmRepo.deleteAll();
  }
  
}
