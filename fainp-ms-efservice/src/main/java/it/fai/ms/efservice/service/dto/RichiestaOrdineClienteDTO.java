package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;

public class RichiestaOrdineClienteDTO implements Serializable {

  private static final long serialVersionUID = 6248354659687952329L;

  private Long id;

  private StatoRichiesta stato;

  public RichiestaOrdineClienteDTO() {
  }

  public RichiestaOrdineClienteDTO(Long id, StatoRichiesta stato) {
    this.id = id;
    this.stato = stato;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public StatoRichiesta getStato() {
    return stato;
  }

  public void setStato(StatoRichiesta stato) {
    this.stato = stato;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RichiestaOrdineClienteDTO [id=");
    builder.append(id);
    builder.append(", stato=");
    builder.append(stato);
    builder.append("]");
    return builder.toString();
  }

}
