package it.fai.ms.efservice.service.fsm.guard;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.listener.events.DispositivoDepositoOutEvent;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardTelepassItaInDepositoOk implements Guard<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(GuardTelepassItaInDepositoOk.class);

  private final static String TIPO_HARDWARE_TO_NOT_SELECT = "AS";

  private final ManageDevicesInStorageService deviceInStorageService;

  private final ApplicationEventPublisher applicationEventPublisher;

  public GuardTelepassItaInDepositoOk(
    final ManageDevicesInStorageService _deviceInStorageService,
    final ApplicationEventPublisher applicationEventPublisher
  ) {
    deviceInStorageService = _deviceInStorageService;
    this.applicationEventPublisher = applicationEventPublisher;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {

    boolean success = false;
    Optional<Dispositivo> deviceLockOpt = deviceInStorageService.findTop1ByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(StatoDispositivo.IN_DEPOSITO,
                                                                                                                          TipoDispositivoEnum.TELEPASS_ITALIANO, TIPO_HARDWARE_TO_NOT_SELECT);

    if (deviceLockOpt.isPresent()) {
      Dispositivo deviceLock = deviceLockOpt.get();
      log.info("FIND DISPOSITIVO IN DEPOSITO: {}", deviceLock);
      Dispositivo device = deviceLock;

      String serialeDispositivoInDeposito = device.getSeriale();
      Message<RichiestaEvent> message = context.getMessage();
      if (message != null) {
        MessageHeaders headers = message.getHeaders();
        Object object = headers.get("object");
        if (object instanceof Richiesta) {
          Richiesta richiesta = (Richiesta) object;
          try {
            String identificativoRichiesta = richiesta.getIdentificativo();
            log.info("Identificativo Richiesta: {}", identificativoRichiesta);
            Dispositivo dispositivo = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst()
                                               .get();
            log.debug("Dispositivo Richiesta: {}", dispositivo);
            String identificativoDevice = dispositivo.getIdentificativo();

            log.debug("Remove StatoDispositivoServizio");
            deviceInStorageService.removeStatoDispositivoServizios(device);
            log.debug("Remove AssociazioneDispositivoVeicolo");
            deviceInStorageService.removeAssociazioneDvs(device);
            log.debug("Salvo il dispositivo che era in deposito......");
            device = deviceInStorageService.save(device.contratto(null));
            log.debug("Flush device");
            deviceInStorageService.flushDevice();
            log.info("Saved device [ {} ] to DE-LOCK", device);

            log.info("Cancello il dispositivo: {}", device);
            deviceInStorageService.delete(device);

            log.debug("flush device deleted");
            deviceInStorageService.flushDevice();

            dispositivo = deviceInStorageService.save(dispositivo.seriale(serialeDispositivoInDeposito));
            applicationEventPublisher.publishEvent(new DispositivoDepositoOutEvent(this, dispositivo.getTipoDispositivo()));
            log.info("Saved SERIALE {} on dispositivo {} related on Richiesta {}", serialeDispositivoInDeposito, identificativoDevice,
                     identificativoRichiesta);
          } catch (Exception e) {
            log.error("Exception on manage device in depositivo for richiesta Telepass Italiano: ", e);
            throw new RuntimeException(e);
          }
        } else {
          throw new RuntimeException("Richiesta can not be null");
        }
      } else {
        throw new RuntimeException("Message can not be null");
      }

      success = true;
    }else
      log.info("DISPOSITIVO IN DEPOSITO NOT FOUND: {} HARDARE TYPE NOT  {}",TipoDispositivoEnum.TELEPASS_ITALIANO,TIPO_HARDWARE_TO_NOT_SELECT);

    return success;
  }
}
