package it.fai.ms.efservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ModificationOperationsDeviceType;

@Repository
public interface ModificationOperationsDeviceTypeRepository extends JpaRepository<ModificationOperationsDeviceType, Long> {

  Optional<ModificationOperationsDeviceType> findByDeviceType(TipoDispositivoEnum deviceType);

}
