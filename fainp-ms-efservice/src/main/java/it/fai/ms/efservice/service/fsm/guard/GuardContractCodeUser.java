package it.fai.ms.efservice.service.fsm.guard;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.ContrattoRichiestaException;

public class GuardContractCodeUser implements Guard<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean result = false;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        Contratto contratto = richiesta.getContratto();
        if (contratto == null) {
          throw new ContrattoRichiestaException("Not found contract related on Richiesta ID: " + richiesta.getId() + " - Identificativo: "
                                                + richiesta.getIdentificativo());
        }

        String codContrattoCliente = contratto.getCodContrattoCliente();
        log.debug("Codice for contratto ID {}: {}", contratto.getId(), codContrattoCliente);
        if (StringUtils.isNotBlank(codContrattoCliente)) {
          log.info("Codice for contratto {} is: {}", contratto, codContrattoCliente);
          result = true;
        }
      }
    }

    log.info("Result: {}", result);
    return result;
  }

}
