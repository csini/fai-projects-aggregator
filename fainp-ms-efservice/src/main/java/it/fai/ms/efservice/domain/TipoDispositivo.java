package it.fai.ms.efservice.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.enumeration.ClassificazioneTipoDisp;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;

/**
 * A TipoDispositivo.
 */
@Entity
@Table(name = "tipo_dispositivo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TipoDispositivo implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "nome_business")
  private String nomeBusiness;

  @Column(name = "testo_promo")
  private String testoPromo;

  @Column(name = "descrizione")
  private String descrizione;

  @NotNull
  @Column(name = "multiservizio", nullable = false)
  private Boolean multiservizio;

  @NotNull
  @Column(name = "virtuale", nullable = false)
  private Boolean virtuale;

  @Enumerated(EnumType.STRING)
  @Column(name = "classificazione")
  private ClassificazioneTipoDisp classificazione;

  @Enumerated(EnumType.STRING)
  @Column(name = "modalita_spedizione")
  private ModalitaSpedizione modalitaSpedizione;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "nome")
  private TipoDispositivoEnum nome;

  @OneToOne
  @JoinColumn(unique = true)
  private Indirizzo indirizzoRientro;

  @OneToMany(mappedBy = "tipoDispositivo")
  @JsonIgnore
  private Set<TipoServizioDefault> tipoServizioDefaults = new HashSet<>();

  @ManyToOne
  private Produttore produttore;

  @ManyToMany
  @JoinTable(
             name = "tipo_dispositivo_tipo_servizio",
             joinColumns = @JoinColumn(name = "tipo_dispositivos_id", referencedColumnName = "id"),
             inverseJoinColumns = @JoinColumn(name = "tipo_servizios_id", referencedColumnName = "id"))
  private Set<TipoServizio> tipoServizios = new HashSet<>();

  @OneToMany(mappedBy = "tipoDispositivo")
  @JsonIgnore
  private Set<Dispositivo> dispositivos = new HashSet<>();

  @Column(name = "hidden")
  @JsonIgnore
  private Boolean hidden;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomeBusiness() {
    return nomeBusiness;
  }

  public TipoDispositivo nomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
    return this;
  }

  public void setNomeBusiness(String nomeBusiness) {
    this.nomeBusiness = nomeBusiness;
  }

  public String getTestoPromo() {
    return testoPromo;
  }

  public TipoDispositivo testoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
    return this;
  }

  public void setTestoPromo(String testoPromo) {
    this.testoPromo = testoPromo;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public TipoDispositivo descrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public Boolean isMultiservizio() {
    return multiservizio;
  }

  public TipoDispositivo multiservizio(Boolean multiservizio) {
    this.multiservizio = multiservizio;
    return this;
  }

  public void setMultiservizio(Boolean multiservizio) {
    this.multiservizio = multiservizio;
  }

  public Boolean isVirtuale() {
    return virtuale;
  }

  public TipoDispositivo virtuale(Boolean virtuale) {
    this.virtuale = virtuale;
    return this;
  }

  public void setVirtuale(Boolean virtuale) {
    this.virtuale = virtuale;
  }

  public ClassificazioneTipoDisp getClassificazione() {
    return classificazione;
  }

  public TipoDispositivo classificazione(ClassificazioneTipoDisp classificazione) {
    this.classificazione = classificazione;
    return this;
  }

  public void setClassificazione(ClassificazioneTipoDisp classificazione) {
    this.classificazione = classificazione;
  }

  public ModalitaSpedizione getModalitaSpedizione() {
    return modalitaSpedizione;
  }

  public TipoDispositivo modalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
    return this;
  }

  public void setModalitaSpedizione(ModalitaSpedizione modalitaSpedizione) {
    this.modalitaSpedizione = modalitaSpedizione;
  }

  public TipoDispositivoEnum getNome() {
    return nome;
  }

  public TipoDispositivo nome(TipoDispositivoEnum nome) {
    this.nome = nome;
    return this;
  }

  public void setNome(TipoDispositivoEnum nome) {
    this.nome = nome;
  }

  public Indirizzo getIndirizzoRientro() {
    return indirizzoRientro;
  }

  public TipoDispositivo indirizzoRientro(Indirizzo indirizzo) {
    this.indirizzoRientro = indirizzo;
    return this;
  }

  public void setIndirizzoRientro(Indirizzo indirizzo) {
    this.indirizzoRientro = indirizzo;
  }

  public Set<TipoServizioDefault> getTipoServizioDefaults() {
    return tipoServizioDefaults;
  }

  public TipoDispositivo tipoServizioDefaults(Set<TipoServizioDefault> tipoServizioDefaults) {
    this.tipoServizioDefaults = tipoServizioDefaults;
    return this;
  }

  public TipoDispositivo addTipoServizioDefault(TipoServizioDefault tipoServizioDefault) {
    this.tipoServizioDefaults.add(tipoServizioDefault);
    tipoServizioDefault.setTipoDispositivo(this);
    return this;
  }

  public TipoDispositivo removeTipoServizioDefault(TipoServizioDefault tipoServizioDefault) {
    this.tipoServizioDefaults.remove(tipoServizioDefault);
    tipoServizioDefault.setTipoDispositivo(null);
    return this;
  }

  public void setTipoServizioDefaults(Set<TipoServizioDefault> tipoServizioDefaults) {
    this.tipoServizioDefaults = tipoServizioDefaults;
  }

  public Produttore getProduttore() {
    return produttore;
  }

  public TipoDispositivo produttore(Produttore produttore) {
    this.produttore = produttore;
    return this;
  }

  public void setProduttore(Produttore produttore) {
    this.produttore = produttore;
  }

  public Set<TipoServizio> getTipoServizios() {
    return tipoServizios;
  }

  public TipoDispositivo tipoServizios(Set<TipoServizio> tipoServizios) {
    this.tipoServizios = tipoServizios;
    return this;
  }

  public TipoDispositivo addTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizios.add(tipoServizio);
    tipoServizio.getTipoDispositivos()
                .add(this);
    return this;
  }

  public TipoDispositivo removeTipoServizio(TipoServizio tipoServizio) {
    this.tipoServizios.remove(tipoServizio);
    tipoServizio.getTipoDispositivos()
                .remove(this);
    return this;
  }

  public void setTipoServizios(Set<TipoServizio> tipoServizios) {
    this.tipoServizios = tipoServizios;
  }

  public Set<Dispositivo> getDispositivos() {
    return dispositivos;
  }

  public TipoDispositivo dispositivos(Set<Dispositivo> dispositivos) {
    this.dispositivos = dispositivos;
    return this;
  }

  public TipoDispositivo addDispositivo(Dispositivo dispositivo) {
    this.dispositivos.add(dispositivo);
    dispositivo.setTipoDispositivo(this);
    return this;
  }

  public TipoDispositivo removeDispositivo(Dispositivo dispositivo) {
    this.dispositivos.remove(dispositivo);
    dispositivo.setTipoDispositivo(null);
    return this;
  }

  public void setDispositivos(Set<Dispositivo> dispositivos) {
    this.dispositivos = dispositivos;
  }

  public boolean isHidden() {
    if (this.hidden == null) {
      return false;
    }
    return this.hidden;
  }

  public TipoDispositivo hidden(Boolean hidden) {
    if (hidden == null) {
      hidden = false;
    }
    this.hidden = hidden;
    return this;
  }

  public void setHidden(Boolean hidden) {
    if (hidden == null) {
      hidden = false;
    }
    this.hidden = hidden;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TipoDispositivo tipoDispositivo = (TipoDispositivo) o;
    if (tipoDispositivo.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), tipoDispositivo.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoDispositivo [id=");
    builder.append(id);
    builder.append(", nome=");
    builder.append(nome);
    builder.append(", multiservizio=");
    builder.append(multiservizio);
    builder.append(", nomeBusiness=");
    builder.append(nomeBusiness);
    builder.append(", modalitaSpedizione=");
    builder.append(modalitaSpedizione);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", virtuale=");
    builder.append(virtuale);
    builder.append(", testoPromo=");
    builder.append(testoPromo);
    builder.append(", classificazione=");
    builder.append(classificazione);
    builder.append(", indirizzoRientro=");
    builder.append(indirizzoRientro);
    builder.append(", produttore=");
    builder.append(produttore);
    builder.append(", hidden=");
    builder.append(hidden);
    builder.append("]");
    return builder.toString();
  }
}
