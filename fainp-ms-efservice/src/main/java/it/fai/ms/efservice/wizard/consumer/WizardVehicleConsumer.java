package it.fai.ms.efservice.wizard.consumer;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

public interface WizardVehicleConsumer extends VehicleConsumer {

  @Override
  void consume(VehicleMessage newMessage);

}
