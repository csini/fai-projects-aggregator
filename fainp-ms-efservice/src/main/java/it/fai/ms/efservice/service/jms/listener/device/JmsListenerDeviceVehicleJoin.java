package it.fai.ms.efservice.service.jms.listener.device;

import java.util.List;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.efservice.message.device.DeviceVehicleJoinMessage;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;

@Service(JmsListenerDeviceVehicleJoin.QUALIFIER)
@Transactional
public class JmsListenerDeviceVehicleJoin implements MessageListener {

  public final static String QUALIFIER = "jmsListenerDeviceVehicleJoin";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final AssociazioneDVRepository associazioneDVRepository;
  private final DispositivoRepositoryExt dispositivoRepository;

  public JmsListenerDeviceVehicleJoin(AssociazioneDVRepository _associazioneDVRepository, DispositivoRepositoryExt _dispositivoRepository) {
    associazioneDVRepository = _associazioneDVRepository;
    dispositivoRepository = _dispositivoRepository;
  }

  @Override
  public void onMessage(Message message) {
	  DeviceVehicleJoinMessage dto = null;
    try {
      dto = (DeviceVehicleJoinMessage) ((ObjectMessage)message).getObject();

      Dispositivo dispositivo = null;
      if (dto.getDeviceUuid() != null && dto.getDeviceUuid().getUuid() != null) {
        dispositivo = dispositivoRepository.findOneByIdentificativo(dto.getDeviceUuid().getUuid());
      }
      
      if (dispositivo == null) {
        _log.warn("Device not found : {}", dto.getDeviceUuid().getUuid());
        return;
      }
      
      List<AssociazioneDV> associazioneDVs = associazioneDVRepository.findByDispositivoOrderByDataDescIdDesc(dispositivo);
      if (associazioneDVs.isEmpty()) {
    	AssociazioneDV associazioneDV = new AssociazioneDV();
        associazioneDV.setData(dto.getTimestamp());
        associazioneDV.setDispositivo(dispositivo);
        associazioneDV.setUuidVeicolo(dto.getVehicleUuid()!=null ? dto.getVehicleUuid().getUuid() : null);
        
        associazioneDV = associazioneDVRepository.saveAndFlush(associazioneDV);
        _log.debug("Join device to vehicle : {}", associazioneDV);
      } else {
    	boolean isFirst = true;
    	for (AssociazioneDV associazioneDV : associazioneDVs) {
    		if(isFirst) {
    			isFirst = false;
    			
    	        associazioneDV.setData(dto.getTimestamp());
    	        associazioneDV.setUuidVeicolo(dto.getVehicleUuid()!=null ? dto.getVehicleUuid().getUuid() : null);
    	        
    	        _log.debug("Join device to vehicle new : {}", associazioneDV);
    	        
    	        associazioneDV = associazioneDVRepository.saveAndFlush(associazioneDV);
    		}else {
    			_log.warn("Deleting multiple associazioneDV: {}",associazioneDV);
    			associazioneDVRepository.delete(associazioneDV);
    		}
		}
      }

      if (!dispositivo.getTipoMagazzino().equals(TipoMagazzino.NONE)) {
        dispositivo.setTipoMagazzino(TipoMagazzino.NONE);
        dispositivoRepository.saveAndFlush(dispositivo);
      }      
    } catch (Exception e) {
        _log.error("Error processing "+dto, e);
    }

  }

}
