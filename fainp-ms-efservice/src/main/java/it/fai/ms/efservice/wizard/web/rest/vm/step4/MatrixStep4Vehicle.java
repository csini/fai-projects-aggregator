package it.fai.ms.efservice.wizard.web.rest.vm.step4;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.efservice.wizard.web.rest.vm.MatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.web.rest.vm.MatrixDeviceType;

public class MatrixStep4Vehicle implements Serializable, Comparable<MatrixStep4Vehicle> {

  private static final long serialVersionUID = 6242074382589590805L;

  private Map<String, MatrixDeviceType>   dispositivi = new LinkedHashMap<>();
  private String                          euro;
  private String                          identificativo;
  private String                          targa;
  private String                          tipo;
  private String                          targaNazione;
  private List<MatrixActivabilityFailure> errors;
  private boolean                         anomaly     = false;
  private String                          possesso;

  public MatrixStep4Vehicle(final String _id) {
    identificativo = _id;
  }

  @Override
  public int compareTo(final MatrixStep4Vehicle _obj) {
    String targaNazioneToCompare = targa + targaNazione;
    return targaNazioneToCompare.compareTo(_obj.getTarga() + _obj.getTargaNazione());
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((MatrixStep4Vehicle) _obj).getTarga(), targa)
             && Objects.equals(((MatrixStep4Vehicle) _obj).getTargaNazione(), targaNazione);
    }
    return isEquals;
  }

  @JsonProperty(value = "dispositivi")
  public Map<String, MatrixDeviceType> getDispositivi() {
    return dispositivi;
  }

  @JsonProperty(value = "euro")
  public String getEuro() {
    return euro;
  }

  @JsonProperty(value = "identificativo")
  public String getIdentificativo() {
    return identificativo;
  }

  @JsonProperty(value = "targa")
  public String getTarga() {
    return targa;
  }

  @JsonProperty(value = "tipo")
  public String getTipo() {
    return tipo;
  }

  @Override
  public int hashCode() {
    return Objects.hash(targa, targaNazione);
  }

  public void setEuro(String _euro) {
    euro = _euro;
  }

  public void setTarga(final String _targa) {
    targa = _targa.toUpperCase(Locale.ROOT);
  }

  public void setTipo(final String _tipo) {
    tipo = _tipo;
  }

  public String getTargaNazione() {
    return targaNazione;
  }

  public void setTargaNazione(String targaNazione) {
    this.targaNazione = targaNazione;
  }

  public boolean isAnomaly() {
    return anomaly;
  }

  public void setAnomaly(boolean anomaly) {
    this.anomaly = anomaly;
  }

  public void addFailure(final MatrixActivabilityFailure _failure) {
    Optional.ofNullable(_failure)
            .ifPresent(failure -> {
              errors = Optional.ofNullable(errors)
                               .orElse(new LinkedList<>());
              errors.add(_failure);
            });
  }

  @JsonInclude(Include.NON_NULL)
  public List<MatrixActivabilityFailure> getErrors() {
    return errors;
  }

  public String getPossesso() {
    return possesso;
  }

  public void setPossesso(String possesso) {
    this.possesso = possesso;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("MatrixStep4Vehicle [devices=");
    builder.append(this.dispositivi);
    builder.append(",euro=");
    builder.append(this.euro);
    builder.append(",identificativo=");
    builder.append(this.identificativo);
    builder.append(",targa=");
    builder.append(this.targa);
    builder.append(",type=");
    builder.append(this.tipo);
    builder.append(",targaNazione=");
    builder.append(this.targaNazione);
    builder.append(",anomaly=");
    builder.append(this.anomaly);
    builder.append(",errors=");
    builder.append(this.errors);
    builder.append(",possesso=");
    builder.append(this.possesso);
    builder.append("]");
    return builder.toString();
  }

}
