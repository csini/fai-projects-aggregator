package it.fai.ms.efservice.asyncjob;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;;

@Configuration
public class AsyncJobCacheConfigFactory {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  @Autowired
  private CacheManager cacheManager;

  @Bean(name = "async_job")
  public Cache buildCache() {
    Cache cache = cacheManager.getCache("async_job_cache_hazelcast");
    _log.info("Created cache: {}", cache);
    return cache;
  }

}