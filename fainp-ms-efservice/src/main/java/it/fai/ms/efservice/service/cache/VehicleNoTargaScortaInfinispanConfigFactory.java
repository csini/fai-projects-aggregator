package it.fai.ms.efservice.service.cache;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.jdbc.DatabaseType;
import org.infinispan.persistence.jdbc.configuration.JdbcStringBasedStoreConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.config.ApplicationProperties;

@Service
public class VehicleNoTargaScortaInfinispanConfigFactory {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private EmbeddedCacheManager cacheManager;

  public VehicleNoTargaScortaInfinispanConfigFactory(@Value("${spring.datasource.driver}") String _driverClass,
                                                     @Value("${spring.datasource.url}") String _connectionUrl,
                                                     @Value("${spring.datasource.username}") String _username,
                                                     @Value("${spring.datasource.password}") String _password,
                                                     ApplicationProperties _applicationProperties) {

    ConfigurationBuilder builder = new ConfigurationBuilder();
    builder.jmxStatistics()
           .disable()
           .clustering()
           .cacheMode(CacheMode.LOCAL)
           .persistence()
           .passivation(false)
           .addStore(JdbcStringBasedStoreConfigurationBuilder.class)
           .dialect(DatabaseType.SQL_SERVER)
           .fetchPersistentState(false)
           .ignoreModifications(false)
           .shared(true)
           .preload(false)
           .table()
           .dropOnExit(false)
           .createOnStart(true)
           .tableNamePrefix("infinispan_vehicle")
           .idColumnName("ID_KEY")
           .idColumnType("VARCHAR(255)")
           .dataColumnName("DATA")
           .dataColumnType("VARBINARY(MAX)")
           .timestampColumnName("TIMESTAMP")
           .timestampColumnType("VARCHAR(255)")
           .connectionPool()
           .connectionUrl(_connectionUrl)
           .username(_username)
           .driverClass(_driverClass)
           .password(_password)
           .expiration()
           .lifespan(-1)
           .maxIdle(-1)
           .memory()
           .evictionStrategy(EvictionStrategy.REMOVE)
           .evictionType(EvictionType.COUNT)
           .size(100)
           .validate();

    if (_applicationProperties.getInfinispan()
                              .isClusterEnabled()) {
      cacheManager = new DefaultCacheManager(GlobalConfigurationBuilder.defaultClusteredBuilder()
                                                                       .defaultCacheName("defaultCache")
                                                                       .globalJmxStatistics()
                                                                       .cacheManagerName(getClass().getSimpleName())
                                                                       .allowDuplicateDomains(true)
                                                                       .transport()
                                                                       .defaultTransport()
                                                                       .addProperty("configurationFile",
                                                                                    "default-configs/default-jgroups-udp.xml")
                                                                       .build(),
                                             builder.build());
    } else {
      cacheManager = new DefaultCacheManager(new GlobalConfigurationBuilder().nonClusteredDefault()
                                                                             .defaultCacheName("defaultCache")
                                                                             .globalJmxStatistics()
                                                                             .cacheManagerName(getClass().getSimpleName())
                                                                             .allowDuplicateDomains(true)
                                                                             .build(),
                                             builder.build());
    }

    _log.info("Created cache manager, status is {}", cacheManager != null ? cacheManager.getStatus() : "n.a.");
  }

  public Cache<String, String> buildCache(final String _cacheName) {
    final Cache<String, String> cache = cacheManager.getCache(_cacheName);
    _log.info("Created cache: {}", cache);
    return cache;
  }

}
