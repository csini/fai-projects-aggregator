package it.fai.ms.efservice.wizard.model;

import java.io.Serializable;
import java.util.Objects;

public class WizardServiceId implements Serializable {

  private static final long serialVersionUID = 3418763910788617900L;

  private String id;

  public WizardServiceId(final String _id) {
    id = _id;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      isEquals = Objects.equals(((WizardServiceId) _obj).getId(), id);
    }
    return isEquals;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WizardServiceId [id=");
    builder.append(this.id);
    builder.append("]");
    return builder.toString();
  }

}
