package it.fai.ms.efservice.service.fsm.type.dartfordcrossing;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@Service(value = "fsmInoltroDartFordCrossing")
public class FsmInoltroDartFordCrossing extends AbstractFsmRichiesta {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory;

  private FsmRichiestaCacheService cacheService;

  private final FsmType fsmType;

  private final TipoDispositivoEnum[] deviceType;

  /**
   * @param stateMachineFactory
   * @param cacheService
   */
  public FsmInoltroDartFordCrossing(@Qualifier("inoltroDartFordCrossing") StateMachineFactory<StatoRichiesta, RichiestaEvent> _stateMachineFactory,
                                    FsmRichiestaCacheService _cacheService) {
    this.stateMachineFactory = _stateMachineFactory;
    this.cacheService = _cacheService;
    fsmType = FsmType.INOLTRO_DARTFORD_CROSSING;
    deviceType = new TipoDispositivoEnum[] { TipoDispositivoEnum.DARTFORD_CROSSING };
  }

  @Override
  public StateMachine<StatoRichiesta, RichiestaEvent> getStateMachine() {
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = stateMachineFactory.getStateMachine(fsmType.fsmName());
    log.debug("Get state machine {}", stateMachine.getId());
    return stateMachine;
  }

  @Override
  public Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta, String nota) {
    long tStart = System.currentTimeMillis();
    Long richiestaId = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    StatoRichiesta statoAttuale = richiesta.getStato();
    log.debug("Switch state by command {} to Richiesta [ID: {} - Identificativo: {}] with actual state: {}", command, richiestaId,
              identificativo, statoAttuale);

    if (!isOneOfType(richiesta, deviceType)) {
      log.warn("Richiesta: {} non gestibile tramite questa FSM...", identificativo);
      return richiesta;
    }

    boolean availableCommand = isAvailableCommand(command, richiesta);
    if (availableCommand) {

      StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = getStateMachine();
      stateMachine.start();
      String fsmKey = String.valueOf(richiestaId);
      StateMachine<StatoRichiesta, RichiestaEvent> stateMachineFromStored = retrieveFsmByCache(fsmKey);
      if (stateMachineFromStored != null) {
        stateMachine = stateMachineFromStored;
      }

      if (command != null) {

        sendEventToFsm(stateMachine, command, richiesta, nota);
        log.info("Send command: {} to Richiesta [ID: {} - Identificativo: {}]", command, richiestaId, identificativo);
        
        cacheService.persist(stateMachine, fsmKey);
        richiesta = getUserAvailableCommandAndSetOnRichiesta(richiesta);
      } else {
        log.warn("Enum command (RichiestaEvent) is {} ", command);
      }
    } else {
      String messageError = String.format("Command %s is not available because Richiesta [ID: %s - Identificativo: %s] state is %s",
                                          command, richiestaId, identificativo, statoAttuale);
      log.error(messageError);
      throw new RuntimeException(messageError);
    }

    log.debug("=> FINISH switch state with command: {} to Richiesta [ID: {} - Identificativo: {}] in {} ms", command, richiestaId,
              identificativo, (System.currentTimeMillis() - tStart));
    return richiesta;
  }

  private boolean isOneOfType(Richiesta richiesta, TipoDispositivoEnum... deviceType2) {
    for (TipoDispositivoEnum tipoDispositivoEnum : deviceType2) {
      if (isEqualType(richiesta, tipoDispositivoEnum))
        return true;
    }
    return false;
  }

  private StateMachine<StatoRichiesta, RichiestaEvent> retrieveFsmByCache(String fsmKey) {
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isNotBlank(jsonContext)) {
        String oldFsm = FsmType.ACCETTAZIONE.fsmName();
        if (jsonContext.contains(oldFsm)) {
          log.info("Reset Fsm Accettazione to Fsm Inoltro TrackyCard");
          cacheService.resetIdMachineFromAnotherIdMachineByContext(jsonContext, fsmKey, fsmType.fsmName());
        }

        log.debug("Restore state machine for FsmKey {}", fsmKey);
        return cacheService.restore(getStateMachine(), fsmKey);
      }
    }

    return null;
  }

}
