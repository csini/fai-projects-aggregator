package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.efservice.dto.OrdineFornitoreDepositoDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlSender;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.service.jms.mapper.OrdineFornitoreDepositoDmlSenderMapper;

public class OrdineFornitoreDepositoDmlSenderUtil
  extends AbstractMappableDmlSender<OrdineFornitoreDepositoDMLDTO, OrdineFornitore> {

  static final JmsTopicNames TOPIC_SAVE = JmsTopicNames.DML_ORDINEFORNITOREDEPOSITO_SAVE;
  static final JmsTopicNames TOPIC_DELETE = null;

  public OrdineFornitoreDepositoDmlSenderUtil(JmsProperties jmsProperties, OrdineFornitoreDepositoDmlSenderMapper mapper) {
    super(jmsProperties, mapper);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

}
