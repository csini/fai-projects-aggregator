package it.fai.ms.efservice.service.dto;

import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.web.rest.errors.PayloadError;

import java.io.Serializable;

/**
 * @deprecated Use AsyncJobResponse instead
 */
@Deprecated
public class CreatedTransitProcessAsyncDTO extends PayloadError {
  private StateChangeStatusFsm state;
  private Object warnings;

  public CreatedTransitProcessAsyncDTO(){};

  public CreatedTransitProcessAsyncDTO(StateChangeStatusFsm state, Object warnings) {
    this.state = state;
    this.warnings = warnings;
  }

  public StateChangeStatusFsm getState() {
    return state;
  }

  public void setState(StateChangeStatusFsm state) {
    this.state = state;
  }

  public Object getWarnings() {
    return warnings;
  }

  public void setWarnings(Object warnings) {
    this.warnings = warnings;
  }

  @Override
  public String toString() {
    return "CreatedTransitProcessAsyncDTO{" +
      "state=" + state +
      ", warnings=" + warnings +
      ", errors=" + getErrors() +
      '}';
  }
}
