package it.fai.ms.efservice.service.fsm.enumeration;

import it.fai.ms.efservice.service.fsm.type.caronte.FsmModificaTesseraCaronte;
import it.fai.ms.efservice.service.fsm.type.frejus.FsmMalfunzionamentoFrejusSost;
import it.fai.ms.efservice.service.fsm.type.frejus.FsmModificaFrejus;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmInoltroGranSanBernardo;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmModificaGranSanBernardoFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.type.gransanbernardo.FsmModificaGranSanBernardoVarTargaMalfunzionamento;
import it.fai.ms.efservice.service.fsm.type.hgv.FsmInoltroHGV;
import it.fai.ms.efservice.service.fsm.type.libert.FsmInoltroLiberT;
import it.fai.ms.efservice.service.fsm.type.libert.FsmModificaLiberTFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.type.libert.FsmModificaLiberTVarTargaMalfunzionamento;
import it.fai.ms.efservice.service.fsm.type.libert.FsmRientroLiberT;
import it.fai.ms.efservice.service.fsm.type.localizzatoresat.FsmModificaLocalizzatoreSatMalfunzionamento;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmInoltroViaToll;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmModificaViaTollFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmModificaViaTollVarTargaMalfunzionamento;
import it.fai.ms.efservice.service.fsm.type.vispro.FsmInoltroVispro;

public enum FsmType {

  //@formatter:off
  ACCETTAZIONE("fsmAccettazione"),
  INOLTRO_TE("fsmInoltroTE"),
  INOLTRO_TI("fsmInoltroTI"),
  INOLTRO_VIACARD("fsmInoltroViaCard"),
  INOLTRO_TSAT("fsmInoltroTSAT"),
  MOD_TE_FURTO_SMARRIMENTO("fsmModificaTeFurtoSmarrimento"),
  MOD_TE_SAT_FURTO_SMARRIMENTO("fsmModificaTeSatFurtoSmarrimento"),
  MOD_TSAT_FURTO_SMARRIMENTO("fsmModificaTSatFurtoSmarrimento"),
  MOD_TE_VARIAZIONE_TARGA("fsmModificaTeVarTarga"),
  MOD_TE_SAT_VARIAZIONE_TARGA("fsmModificaTeSatVarTarga"),
  MOD_TE_RIENTRO_MALFUNZIONAMENTO("fsmModificaTeRientroMalfunzionamento"),
  MOD_TE_RESC_RIATT_SOSPENSIONE("fsmModificaTeRescRiattSospeso"),
  DISPOSITIVO("fsmDispositivo"),
  MOD_TSAT_RIENTRO_MALFUNZIONAMENTO("fsmModificaTSatRientroMalfunzionamento"),
  MOD_TSAT_VARIAZIONE_TARGA("fsmModificaTSatVarTarga"),
  CONTRATTO("fsmContratto"),
  MOD_TE_SAT_RIENTRO_MALFUNZIONAMENTO("fsmModificaTeSatRientroMalfunzionamento"),
  MOD_TE_SAT_MALFUNZIONAMENTO_SOST("fsmModificaTeSatMalfunzionamentoSost"),
  MOD_TI_VARIAZIONE_TARGA("fsmModificaTItaVarTarga"),
  MOD_TI_RIENTRO_MALFUNZIONAMENTO("fsmModificaTItaRientroMalfunzionamento"),
  MOD_TI_FURTO_SMARRIMENTO("fsmModificaTItaFurtoSmarrimento"),
  MOD_VC_VARIAZIONE_TARGA("fsmModificaViaCardMalfunzionamentoSost"),
  MOD_VC_FURTO_SMARRIMENTO("fsmModificaViaCardFurtoSmarrimento"),
  MOD_VC_RIENTRO_MALFUNZIONAMENTO("fsmModificaViaCardRientroMalfunzionamento"),
  INOLTRO_TRACKYCARD("fsmInoltroTrackyCard"),
  MOD_TRACKY_FURTO_SMARRIMENTO("fsmModificaTrackyCardFurtoSmarrimento"),
  MOD_TRACKY_VARIAZIONE_TARGA("fsmModificaTrackyCardVarTarga"),
  MOD_TRACKY_RIENTRO_MALFUNZIONAMENTO("fsmModificaTrackyCardRientroMalfunzionamento"),
  INOLTRO_GOBOX("fsmInoltroGoBox"),
  MODIFICA_GOBOX("fsmModificaGoBox"),
  INOLTRO_TOLL_COLLECT("fsmInoltroTollCollect"),
  INOLTRO_TOLL2GO("fsmInoltroToll2Go"),
  INOLTRO_FEJUS("fsmInoltroFrejus"),
  MODIFICA_TOLL_COLLECT("fsmModificaTollCollect"),
  MODIFICA_FREJUS(FsmModificaFrejus.FSM_MODIFICA_FREJUS),
  MALFUNZIONAMENTO_FREJUS(FsmMalfunzionamentoFrejusSost.FSM_NAME),
  MODIFICA_TESSERA_CARONTE(FsmModificaTesseraCaronte.FSM_MODIFICA_TESSERA_CARONTE),
  INOLTRO_DARTFORD_CROSSING("fsmInoltroDartFordCrossing"),
  INOLTRO_HGV(FsmInoltroHGV.FSM_INOLTRO_HGV),
  MOD_DARTFORD_CROSSING_VARAZIONE_TARGA("fsmModificaDartFordCrossingVarTarga"),
  INOLTRO_GRAN_SAN_BERNARDO(FsmInoltroGranSanBernardo.FSM_INOLTRO_GRAN_SAN_BERNARDO),
  MOD_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO(FsmModificaGranSanBernardoFurtoSmarrimento.FSM_MODIFICA_GRAN_SAN_BERNARDO_FURTO_SMARRIMENTO),
  MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO(FsmModificaGranSanBernardoVarTargaMalfunzionamento.FSM_MOD_GRAN_SAN_BERNARDO_VAR_TARGA_MALFUNZIONAMENTO),
  INOLTRO_LIBER_T(FsmInoltroLiberT.FSM_INOLTRO_LIBER_T),
  MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO(FsmModificaLiberTVarTargaMalfunzionamento.FSM_MOD_LIBER_T_VAR_TARGA_MALFUNZIONAMENTO),
  MOD_LIBER_T_FURTO_SMARRIMENTO(FsmModificaLiberTFurtoSmarrimento.FSM_MODIFICA_LIBER_T_FURTO_SMARRIMENTO),
  MOD_LIBER_T_RIENTRO(FsmRientroLiberT.FSM_MODIFICA_LIBER_T_RIENTRO),

  INOLTRO_VIATOLL(FsmInoltroViaToll.FSM_INOLTRO_VIATOLL),
  MOD_VIATOLL_FURTO_SMARRIMENTO(FsmModificaViaTollFurtoSmarrimento.FSM_MODIFICA_VIATOLL_FURTO_SMARRIMENTO),
  MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO(FsmModificaViaTollVarTargaMalfunzionamento.FSM_MOD_VIATOLL_VAR_TARGA_MALFUNZIONAMENTO),

  INOLTRO_VISPRO(FsmInoltroVispro.FSM_INOLTRO_VISPRO),
  INOLTRO_ASSISTENZA_VEICOLI("fsmInoltroAssistenzaVeicoli"),
  MOD_ASSISTENZA_VEICOLI_VARIAZIONE_TARGA("fsmModificaAssistenzaVeicoliVarTarga"),
  FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO(FsmModificaLocalizzatoreSatMalfunzionamento.FSM_MOD_LOCALIZZATORE_SAT_MALFUNZIONAMENTO),
  ;
  //@formatter:on

  private String fsm;

  FsmType(String fsm) {
    this.fsm = fsm;
  }

  public String fsmName() {
    return fsm;
  }

}
