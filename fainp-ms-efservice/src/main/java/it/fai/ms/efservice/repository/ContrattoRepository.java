package it.fai.ms.efservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;

/**
 * Spring Data JPA repository for the Contratto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContrattoRepository extends JpaRepository<Contratto, Long> {

  @Query("select distinct c from ClienteFai a, Contratto c " + " where a.id = c.clienteFai.id"
      + " and a.codiceCliente = :codiceAzienda")
  List<Contratto> findAllByCodiceAzienda(@Param("codiceAzienda") String codiceAzienda);

  Contratto findFirstByCodContrattoCliente(String idContratto);

  List<Contratto> findByProduttoreAndClienteFai(Produttore produttore, ClienteFai clienteFai);

}
