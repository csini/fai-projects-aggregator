package it.fai.ms.efservice.service.jms.producer.device;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ObuType;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceActivationMessage;
import it.fai.ms.common.jms.efservice.message.device.DeviceServiceDeactivationMessage;
import it.fai.ms.common.jms.efservice.message.device.ObuDeviceInfo;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.StatoDispositivoServizioService;

@Service
@Transactional
public class DeviceServiceActivationMessageProducerServiceImpl implements DeviceServiceActivationMessageProducerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final StatoDispositivoServizioService sdsService;

  private final JmsProperties jmsProperties;

  @Autowired
  public DeviceServiceActivationMessageProducerServiceImpl(final StatoDispositivoServizioService _sdsService,
                                                           final JmsProperties _jmsProperties) {
    sdsService = _sdsService;
    jmsProperties = _jmsProperties;
  }

  @Override
  public void activateDevice(final Richiesta _orderRequestEntity) {

    TipoDispositivo tipoDispositivo = _orderRequestEntity.getTipoDispositivo();
    TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
    switch (deviceType) {
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      activationServiceTelepassEu(_orderRequestEntity);
      break;

    case TRACKYCARD:
      _log.info("Activation service is manage by DML Device....");
      break;
    default:
      _log.warn("Device type {} not manage to send Activation service message....", deviceType);
      break;
    }

  }

  @Override
  public void deactivateDevice(final Richiesta _orderRequestEntity) {
    TipoDispositivo tipoDispositivo = _orderRequestEntity.getTipoDispositivo();
    TipoDispositivoEnum deviceType = tipoDispositivo.getNome();
    switch (deviceType) {
    case TELEPASS_EUROPEO:
    case TELEPASS_EUROPEO_SAT:
      deActivationServiceTelepassEu(_orderRequestEntity);
      break;

    case TRACKYCARD:
      _log.info("DeActivation service is manage by DML Device....");
      break;
    default:
      _log.warn("Device type {} not manage to send DeActivation service message....", deviceType);
      break;
    }

  }

  private void activationServiceTelepassEu(final Richiesta _orderRequestEntity) {
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(_orderRequestEntity);

    DeviceServiceActivationMessage deviceServiceActivationMessage = mapToForActivation(orderRequest);
    deviceServiceActivationMessage.getServices()
                                  .addAll(DeviceService.fromDeviceServiceNames(orderRequest.findActiveServicesNames()));

    _log.info("Message produced : {}", deviceServiceActivationMessage);
    sendOnTopic(JmsTopicNames.SERVICE_ACTIVATION, deviceServiceActivationMessage);
  }

  private void deActivationServiceTelepassEu(Richiesta _orderRequestEntity) {
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(_orderRequestEntity);

    DeviceServiceDeactivationMessage deviceServiceDeactivationMessage = mapToForDeactivation(orderRequest);
    deviceServiceDeactivationMessage.getServices()
                                    .addAll(DeviceService.fromDeviceServiceNames(orderRequest.findNotActiveServicesNames()));

    _log.info("Message produced : {}", deviceServiceDeactivationMessage);
    sendOnTopic(JmsTopicNames.SERVICE_DEACTIVATION, deviceServiceDeactivationMessage);
  }

  private DeviceServiceActivationMessage mapToForActivation(final OrderRequestDecorator _orderRequest) {
    return new DeviceServiceActivationMessage(new ContractOrderRequest(_orderRequest.findNumeroContract(),
                                                                       _orderRequest.findCodiceCliente()),
                                              new ObuDeviceInfo(new ObuNumber(_orderRequest.getDeviceSerialId()),
                                                                ObuType.fromString(_orderRequest.getHardwareDeviceType())));
  }

  private DeviceServiceDeactivationMessage mapToForDeactivation(final OrderRequestDecorator _orderRequest) {
    return new DeviceServiceDeactivationMessage(new ContractOrderRequest(_orderRequest.findNumeroContract(),
                                                                         _orderRequest.findCodiceCliente()),
                                                new ObuDeviceInfo(new ObuNumber(_orderRequest.getDeviceSerialId()),
                                                                  ObuType.fromString(_orderRequest.getHardwareDeviceType())));
  }

  private void sendOnTopic(JmsTopicNames topic, Serializable dto) {
    try {
      JmsTopicSenderUtil.publish(jmsProperties, topic, dto);
    } catch (final Exception _e) {
      _log.error("Message {} not published on topic {}", dto, topic, _e);
    }
  }

  @Override
  public void updateAllServiceDeviceStatus(Richiesta richiesta) {
    Optional<Dispositivo> optDevice = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst();
    if (optDevice.isPresent()) {
      Dispositivo device = optDevice.get();
      device.getStatoDispositivoServizios()
            .stream()
            .map(sds -> {
              return updateSdsStatus(sds);
            })
            .collect(toSet());
    } else {
      _log.error("Richiesta {} hasn't device associated", richiesta.getIdentificativo());
    }
  }

  @Override
  public void updateDeviceServiceStatus(final Richiesta richiesta) {
    TipoRichiesta tipo = richiesta.getTipo();

    switch (tipo) {
    case ATTIVAZIONE_SERVIZIO:
      updateSdsState(richiesta, StatoDS.IN_ATTIVAZIONE);
      break;

    case DISATTIVAZIONE_SERVIZIO:
      updateSdsState(richiesta, StatoDS.IN_DISATTIVAZIONE);
      break;

    case NUOVO_ORDINE:
      updateAllSdsStatus(richiesta);
      break;

    default:
      _log.info("Not manage type of request: {}", tipo);
      break;
    }
  }

  private void updateSdsState(final Richiesta richiesta, StatoDS statoSds) {
    String nomeServizio = richiesta.getNomeServizio();

    Optional<Dispositivo> optDevice = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst();
    if (optDevice.isPresent()) {
      Dispositivo dispositivo = optDevice.get();
      Set<StatoDispositivoServizio> deviceServices = dispositivo.getStatoDispositivoServizios();
      deviceServices.stream()
                    .filter(sds -> sds.getStato() == statoSds)
                    .filter(sds -> sds.getTipoServizio() != null && sds.getTipoServizio()
                                                                       .getNome()
                                                                       .equalsIgnoreCase(nomeServizio))
                    .map(sds -> {
                      return updateSdsStatus(sds);
                    })
                    .collect(toSet());
    }

  }

  private void updateAllSdsStatus(final Richiesta richiesta) {
    Optional<Dispositivo> optDevice = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst();
    if (optDevice.isPresent()) {
      Dispositivo dispositivo = optDevice.get();
      Set<StatoDispositivoServizio> deviceServices = dispositivo.getStatoDispositivoServizios();
      deviceServices.stream()
                    .map(sds -> {
                      return updateSdsStatus(sds);
                    })
                    .collect(toSet());
    }
  }

  private StatoDispositivoServizio updateSdsStatus(StatoDispositivoServizio sds) {
    StatoDS stato = sds.getStato();
    StatoDS newState = stato; // so it's safe
    switch (stato) {
    case IN_ATTIVAZIONE:
      newState = StatoDS.ATTIVO;
      break;
    case IN_DISATTIVAZIONE:
      newState = StatoDS.NON_ATTIVO;
      break;
    default:
      _log.warn("Not change status on SDS");
      break;
    }
    _log.debug("Change status of SDS {} to {}", sds, newState);
    Instant data = Instant.now();
    switch (newState) {
    case ATTIVO:
      sds.setDataAttivazione(data);
      break;
    case NON_ATTIVO:
      sds.setDataDisattivazione(data);
      break;

    default:
      _log.debug("Not set data....");
      break;
    }
    sds = sdsService.save(sds.stato(newState));
    return sds;
  }

  @Override
  public void disableAllServiceDeviceStatus(Richiesta richiesta) {
    Optional<Dispositivo> optDevice = richiesta.getDispositivos()
                                               .stream()
                                               .findFirst();
    if (optDevice.isPresent()) {
      Dispositivo dispositivo = optDevice.get();
      Set<StatoDispositivoServizio> deviceServices = dispositivo.getStatoDispositivoServizios();
      deviceServices.stream()
                    .map(sds -> {
                      sds.setStato(StatoDS.IN_DISATTIVAZIONE);
                      return updateSdsStatus(sds);
                    })
                    .collect(toSet());
    }

  }

}
