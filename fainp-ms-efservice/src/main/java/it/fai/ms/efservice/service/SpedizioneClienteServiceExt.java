package it.fai.ms.efservice.service;

import it.fai.ms.efservice.client.AnagaziendeClient;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.anagazienda.ValidazioneCliente;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.SpedizioneClienteView;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.SpedizioneClienteViewRepositoryExt;
import it.fai.ms.efservice.service.dto.SearchSpedizioneClienteDTO;
import it.fai.ms.efservice.service.dto.SpedizioneClienteViewDTO;
import it.fai.ms.efservice.service.specifications.SearchSpedizioneClienteSpecification;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SpedizioneClienteServiceExt {
  private final Logger log = LoggerFactory.getLogger(getClass());


  private final SpedizioneClienteViewRepositoryExt repositoryExt;
  private final AssociazioneDVRepository associazioneDV;
  private final VehicleClient vehicleClient;
  private final DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService;
  private final AnagaziendeClient anagaziendeClient;

  @Value("${efservice.authorizationHeader}")
  private String jwt;

  public SpedizioneClienteServiceExt(
    SpedizioneClienteViewRepositoryExt repositoryExt,
    AssociazioneDVRepository associazioneDV,
    VehicleClient vehicleClient,
    DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService,
    AnagaziendeClient anagaziendeClient
  ) {
    this.repositoryExt = repositoryExt;
    this.associazioneDV = associazioneDV;
    this.vehicleClient = vehicleClient;
    this.dispositiviDaSpedireCacheService = dispositiviDaSpedireCacheService;
    this.anagaziendeClient = anagaziendeClient;
  }

  public Page<SpedizioneClienteViewDTO> findAllWithFilter(SearchSpedizioneClienteDTO dto, Pageable page) {

    SearchSpedizioneClienteSpecification searchSpeficification = new SearchSpedizioneClienteSpecification(dto);

    Page<SpedizioneClienteView> spedizioniPage = repositoryExt
      .findAll(searchSpeficification, page);

    List<String> codiciClentiSpedizioni = spedizioniPage
      .getContent().stream()
      .map(
        SpedizioneClienteView::getCodiceAzienda
      ).collect(Collectors.toList());

    Map<String, ValidazioneCliente> validazioniClientiMap = new HashMap<>();

    if(!codiciClentiSpedizioni.isEmpty()) {
      validazioniClientiMap = Optional.ofNullable(
        anagaziendeClient.validaAzienda(jwt, codiciClentiSpedizioni)
        .getValidazioni()).orElse(Collections.emptyList()
      )
        .stream()
        .collect(Collectors.toMap(ValidazioneCliente::getCodiceCliente, v -> v));
    }
    
    
    this.log.info("Elenco codiciClienteSpedizione {}", codiciClentiSpedizioni);
    
    final Map<String, ValidazioneCliente> clientIssuesMap = validazioniClientiMap;
    
    this.log.info("validazioniClientiMap {}", validazioniClientiMap);
    

    Page<SpedizioneClienteViewDTO> result = spedizioniPage
      .map(s -> toDto(s, clientIssuesMap));

    return new PageImpl<>(
      result.getContent(),
      page,
      result.getTotalElements()
    );
  }

  private List<PayloadError.Error> calculateCRMIssues(ValidazioneCliente validazioneCliente) {
    List<PayloadError.Error> errors = new ArrayList<>();
    if( validazioneCliente != null ) {
      if(BooleanUtils.isNotTrue(validazioneCliente.isModalitaPagamento())){
        errors.add(PayloadError.buildError(Errno.CRM_INVALID_MODALITA_PAGAMENTO));
      }
      if(BooleanUtils.isNotTrue(validazioneCliente.isFideiussioni())){
        errors.add(PayloadError.buildError(Errno.CRM_INVALID_FIDIUSSIONE));
      }
      if(BooleanUtils.isNotTrue(validazioneCliente.isCartaIdentitaLegRapp())){
        errors.add(PayloadError.buildError(Errno.CRM_INVALID_DOCUMENTO));
      }
      if(BooleanUtils.isNotTrue(validazioneCliente.isLicenzaValida())){
        errors.add(PayloadError.buildError(Errno.CRM_INVALID_LICENZA));
      }
    }

    return errors;
  }

  private SpedizioneClienteViewDTO toDto(
    SpedizioneClienteView spedizioneClienteView,
    Map<String, ValidazioneCliente> validazioniClientiMap
  ){
    SpedizioneClienteViewDTO spedizioneDto = new SpedizioneClienteViewDTO();
    spedizioneDto.setId(spedizioneClienteView.getId());
    spedizioneDto.setCodiceAzienda(spedizioneClienteView.getCodiceAzienda());
    spedizioneDto.setRagioneSociale(spedizioneClienteView.getRagioneSociale());
    spedizioneDto.setNumeroDispositiviDaRicevere(spedizioneClienteView.getNumeroDispositiviDaRicevere());
    spedizioneDto.setNumeroDispositiviDisponibili(spedizioneClienteView.getNumeroDispositiviDisponibili());
    spedizioneDto.setNumeroDispositiviSpedibili(spedizioneClienteView.getNumeroDispositiviSpedibili());

    spedizioneDto.addAllAnomalia(
      calculateAnomaliaLibretto(spedizioneClienteView.getCodiceAzienda(), spedizioneDto)
    );

    spedizioneDto.addAllAnomaliaCRM(
      calculateCRMIssues(
        validazioniClientiMap.get(spedizioneClienteView.getCodiceAzienda())
      )
    );

    boolean blockSpedizione = isCrmBlocked(validazioniClientiMap.get(spedizioneClienteView.getCodiceAzienda())) ||
      spedizioneDto.getNumeroDispositiviSpedibili() == 0;

    if (blockSpedizione) {
      spedizioneDto.setChecked(false);
      spedizioneDto.setDisabled(true);
    }

    return spedizioneDto;
  }

  private Boolean isCrmBlocked(ValidazioneCliente validazioneCliente) {
    return validazioneCliente != null &&
      BooleanUtils.isNotTrue(validazioneCliente.isAttivo()) &&
      BooleanUtils.isNotTrue(validazioneCliente.isForzLetSped());
  }

  private List<PayloadError.Error> calculateAnomaliaLibretto(String codiceAzienda, SpedizioneClienteViewDTO dto){

    log.debug("calculating Anomalia Libretto for codAzienda: {}", codiceAzienda);

    List<String> uuids = associazioneDV.findAllWithRichiestaStatoAndCodiceAzienda(
      StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI,
      codiceAzienda
    ).stream().filter(
      assoDV ->
        BooleanUtils.isNotTrue(dispositiviDaSpedireCacheService.get(assoDV.getDispositivo().getIdentificativo()))
    ).map(AssociazioneDV::getUuidVeicolo).collect(Collectors.toList());

    if (uuids != null && !uuids.isEmpty()) {
      log.debug("found {} uuids vehicles for codAzienda: {}", uuids.size() , codiceAzienda);
      List<VehicleDTO> issues = vehicleClient.findVehicleWithoutBooklet(
        jwt,
        uuids
      );
      log.debug("found {} issues with for codAzienda: {}", uuids.size() , codiceAzienda);
      if (issues != null && !issues.isEmpty()) {
        dto.setNumeroDispositiviSpedibili(
         dto.getNumeroDispositiviSpedibili() - issues.size()
        );
        return issues.stream().map(
          vehicleDTO ->  PayloadError.buildError(Errno.DELIVERY_VEICOLO_SENZA_LIBRETTO)
        ).collect(Collectors.toList());
      }
    }
    log.debug("NO vehicles found for codAzienda: {}", codiceAzienda);

    return null;
  }
}
