/**
 * 
 */
package it.fai.ms.efservice.service.fsm.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Luca Vassallo
 * @param <S>
 * @param <T>
 */
@Service
public class StateMachineContextSerializer<S, E> {

  public String serialize(StateMachineContext<S, E> obj) {

    ObjectMapper objMap = new ObjectMapper();
    String jsonInString = null;
    try {
      jsonInString = objMap.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    return jsonInString;
  }

  @SuppressWarnings("unchecked")
  public StateMachineContext<S, E> deSerialize(String json) {
    DefaultStateMachineContext<S, E> defaultStateMachineContext = null;

    ObjectMapper objMap = new ObjectMapper();
    Object obj = null;
    try {
      obj = objMap.readValue(json, Object.class);

      if (obj instanceof HashMap<?, ?>) {
        HashMap<String, Object> map = (HashMap<String, Object>) obj;

        Object object = map.get("historyStates");
        Map<S, S> mapping = new HashMap<>();
        if (object instanceof Map<?, ?>) {
          mapping = (Map<S, S>) object;
        }

        ExtendedState extendedState = new DefaultExtendedState();
        object = map.get("extendedState");
        if (object instanceof HashMap<?, ?>) {
          HashMap<String, Object> mapExtendedState = (HashMap<String, Object>) object;
          Object variables = mapExtendedState.get("variables");
          extendedState.getVariables()
                       .putAll((Map<? extends Object, ? extends Object>) variables);
        }

        defaultStateMachineContext = new DefaultStateMachineContext<S, E>((List<StateMachineContext<S, E>>) map.get("childs"),
                                                                          (S) map.get("state"), null, null, extendedState, mapping,
                                                                          (String) map.get("id"));
      }
    } catch (JsonParseException e) {
      // TODO manage exception
      e.printStackTrace();
    } catch (JsonMappingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return defaultStateMachineContext;
  }

}
