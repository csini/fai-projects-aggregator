package it.fai.ms.efservice.service.fsm.config.assistenza.veicoli;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.action.FsmActionGenerateContratto;
import it.fai.ms.efservice.service.fsm.action.FsmActionChangeStatusDispositivo; 
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsAssistenzaVeicoliMissing;
import it.fai.ms.efservice.service.fsm.guard.GuardDocumentsAssistenzaVeicoliPresent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;
 
@Configuration                     
@EnableStateMachineFactory(name = FsmInoltroAssistenzaVeicoliConfig.INOLTRO_ASSISTENZA_VEICOLI)
public class FsmInoltroAssistenzaVeicoliConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_ASSISTENZA_VEICOLI = "inoltroAssistenzaVeicoli"; 
  private final Logger log = LoggerFactory.getLogger(getClass()); 
 
  private final FsmSenderToQueue fsmSenderToQueue; 
  private final ManageDeviceSentModeService  manageDeviceSentModeService;
  private final GeneratorContractCodeService generatorContractCodeService;  
  private final DocumentService documentService;
  
  
  
  public FsmInoltroAssistenzaVeicoliConfig(final GeneratorContractCodeService generatorContractCodeService, 
                                           final FsmSenderToQueue _senderFsmService, 
                                           DocumentService documentService,
                                           final ManageDeviceSentModeService _manageDeviceSentModeService 
                                           ) {
    super();
    this.generatorContractCodeService = generatorContractCodeService;
    this.fsmSenderToQueue =  _senderFsmService;
    this.documentService = documentService;
    this.manageDeviceSentModeService = _manageDeviceSentModeService;
    // senderFsmService = _senderFsmService; 
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_ASSISTENZA_VEICOLI.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroAssistenzaVeicoli();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
       
    transitions.withExternal()// MANUAL
    .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .action(new FsmActionGeneric()) 
    .and()
    .withExternal()// AUTO
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .action(new FsmActionGenerateContratto(generatorContractCodeService))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .target(StatoRichiesta.ATTIVO_EVASO)
    .guard(new GuardDocumentsAssistenzaVeicoliPresent(documentService))
    .action(new FsmActionChangeStatusDispositivo(fsmSenderToQueue, DispositivoEvent.SPEDITO_DAL_FORNITORE))
    .and()
    .withExternal()// AUTOGUARD
    .source(StatoRichiesta.CONTROLLO_DOCUMENTI)
    .target(StatoRichiesta.INCOMPLETO_TECNICO)
    .guard(new GuardDocumentsAssistenzaVeicoliMissing(documentService))
    .action(new FsmActionGeneric())
    .and()
    .withExternal()// MANUAL
    .source(StatoRichiesta.INCOMPLETO_TECNICO)
    .target(StatoRichiesta.CONTROLLO_DOCUMENTI) 
    .event(RichiestaEvent.MU_DOCUMENTO_CARICATO)   
    .action(new FsmActionGeneric());  
  }
} 
