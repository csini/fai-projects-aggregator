package it.fai.ms.efservice.service.fsm.config.telepass.sat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionAttivoRientro;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionRientratoDispositivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionVerificaRientroDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardDispositivoDaSpedireTE;
import it.fai.ms.efservice.service.fsm.guard.GuardSendDeviceReturnMessage;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = "ModTSatRientroMalfunzionamento")
public class FsmModificaTSatRientroMalfunzionamentoConfig extends EnumStateMachineConfigurerAdapter<StatoRichiesta, RichiestaEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTSatRientroMalfunzionamentoConfig.class);

  private final FsmSenderToQueue senderFsmUtil;

  private final DeviceReturnMessageProducerService deviceReturnMessageService;

  public FsmModificaTSatRientroMalfunzionamentoConfig(FsmSenderToQueue senderFsmUtil,
                                                      DeviceReturnMessageProducerService _deviceReturnMessageService) {
    this.senderFsmUtil = senderFsmUtil;
    deviceReturnMessageService = _deviceReturnMessageService;
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoRichiesta, RichiestaEvent> config) throws Exception {
    super.configure(config);
    config.withConfiguration()
          .machineId("fsmModificaTSatRientroMalfunzionamento");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoRichiesta, RichiestaEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoRichiesta, RichiestaEvent> states) throws Exception {
    states.withStates()
          .initial(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT, ctx -> log.trace("Initial: " + ctx.getTarget()
                                                                                              .getIds()))
          .states(StatoRichiestaUtil.getStateOfTSatRientroMalfunzionamento());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.MALFUNZIONAMENTO)
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.MALFUNZIONAMENTO)
               .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
//               .action(new FsmActionAttivoRientro(senderFsmUtil))
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.ANNULLATO_UTENTE)
               .event(RichiestaEvent.MU_ANNULLAMENTO_UTENTE)
               .action(new FsmActionGeneric()) // TODO
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE)
               .action(new FsmActionGeneric()) // TODO
               .and()
               .withExternal()// MANUALGUARD
               .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
               .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
               .guard(new GuardDispositivoDaSpedireTE())
               .action(new FsmActionVerificaRientroDispositivo(senderFsmUtil))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
               .target(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO)
               .guard(new GuardSendDeviceReturnMessage(deviceReturnMessageService)) //FIXME-fsm non è una guard, lancia un messaggio
//               .action(new FsmActionRientratoDispositivo(senderFsmUtil)) //FIXME-fsm ho commentato questo e lasciato il successivo: va bene?
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO)
               .target(StatoRichiesta.EVASO_CON_RICONSEGNA)
               .event(RichiestaEvent.MU_LETTERA_VETTURA)
               .action(new FsmActionRientratoDispositivo(senderFsmUtil));
  }

}
