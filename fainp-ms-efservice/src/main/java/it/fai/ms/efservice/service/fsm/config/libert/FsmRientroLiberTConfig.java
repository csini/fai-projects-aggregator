package it.fai.ms.efservice.service.fsm.config.libert;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.action.FsmActionMoveDispositivoToMagazzino;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@Configuration
@EnableStateMachineFactory(name = FsmRientroLiberTConfig.RIENTRO_LIBER_T)
public class FsmRientroLiberTConfig extends  FsmRichiestaConfig{
  public static final String RIENTRO_LIBER_T =  "rientroLiberT";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ManageDevicesInStorageService deviceInStorageService;


  public FsmRientroLiberTConfig(ManageDevicesInStorageService deviceInStorageService) {
    super();
    this.deviceInStorageService = deviceInStorageService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_LIBER_T_RIENTRO.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {

    return StatoRichiesta.ATTIVO_PER_RIENTRO_MAGAZZINO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    HashSet<StatoRichiesta> states = new HashSet<StatoRichiesta>();
    states.add(StatoRichiesta.ATTIVO_PER_RIENTRO_MAGAZZINO);
    states.add(StatoRichiesta.ACCETTATO);
    states.add(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    states.add(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    states.add(StatoRichiesta.EVASO);
    return states;
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()
    .source(StatoRichiesta.ATTIVO_PER_RIENTRO_MAGAZZINO)
    .target(StatoRichiesta.ACCETTATO)
    .event(RichiestaEvent.INITIAL)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()
    .source(StatoRichiesta.ACCETTATO)
    .target(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
    .action(new FsmActionGeneric())
    .and()
    .withExternal()
    .source(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
    .target(StatoRichiesta.DISPOSITIVO_RIENTRATO)
    .event(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO)
    .action(new FsmActionMoveDispositivoToMagazzino(deviceInStorageService))
    .and()
    .withExternal()
    .source(StatoRichiesta.DISPOSITIVO_RIENTRATO)
    .target(StatoRichiesta.EVASO)
    .action(new FsmActionGeneric())
    ;

  }
}
