package it.fai.ms.efservice.service.dto;

import java.io.Serializable;
import java.util.HashMap;

public class datiAggiuntiviCarrelloDTO implements Serializable {

  private static final long serialVersionUID = -4367213693867945880L;

  private String identificativo;

  private HashMap<String, Object> datiAggiuntivi;

  public String getIdentificativo() {
    return identificativo;
  }

  public void setIdentificativo(String uuidVehicle) {
    this.identificativo = uuidVehicle;
  }

  public HashMap<String, Object> getDatiAggiuntivi() {
    return datiAggiuntivi;
  }

  public void setDatiAggiuntivi(HashMap<String, Object> datiAggiuntivi) {
    this.datiAggiuntivi = datiAggiuntivi;
  }

  @Override
  public String toString() {
    return "datiAggiuntiviCarrelloDTO [identificativo=" + identificativo + ", datiAggiuntivi=" + datiAggiuntivi + "]";
  }

}
