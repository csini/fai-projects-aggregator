package it.fai.ms.efservice.web.rest.vm;

import java.io.Serializable;
import java.time.Instant;

import it.fai.ms.efservice.domain.StatoContratto;

public class ContrattoRequestExt implements Serializable {

  private static final long serialVersionUID = 1L;

  private String codiceContratto;
  private StatoContratto stato;
  private Instant dataModificaStato;
  private String codiceCliente;
  private Long idProduttore;
  private String nazioneFatturazione;

  public String getCodiceContratto() {
    return codiceContratto;
  }
  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }
  public StatoContratto getStato() {
    return stato;
  }
  public void setStato(StatoContratto stato) {
    this.stato = stato;
  }
  public Instant getDataModificaStato() {
    return dataModificaStato;
  }
  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }
  public String getCodiceCliente() {
    return codiceCliente;
  }
  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }
  public Long getIdProduttore() {
    return idProduttore;
  }
  public void setIdProduttore(Long idProduttore) {
    this.idProduttore = idProduttore;
  }
  public String getNazioneFatturazione() {
    return nazioneFatturazione;
  }
  public void setNazioneFatturazione(String nazioneFatturazione) {
    this.nazioneFatturazione = nazioneFatturazione;
  }
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("ContrattoRequestExt [codiceContratto=")
        .append(codiceContratto)
        .append(", stato=")
        .append(stato)
        .append(", dataModificaStato=")
        .append(dataModificaStato)
        .append(", codiceCliente=")
        .append(codiceCliente)
        .append(", idProduttore=")
        .append(idProduttore)
        .append(", nazioneFatturazione=")
        .append(nazioneFatturazione)
        .append("]");
    return builder.toString();
  }

}
