package it.fai.ms.efservice.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.RichiestaModificaServiceExt;
import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.async.CreationOrdineAsyncService;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.CreatedRichiestasAsyncDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.OrdineDTO;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;
import it.fai.ms.efservice.web.rest.errors.PayloadError;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Richiesta.
 */
@RestController
@RequestMapping(RichiestaResourceExt.BASE_PATH)
public class RichiestaResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String BASE_PATH = "/api/public";

  public static final String ORDINE = "/richiesta";

  public static final String CONTRATTO_FOR_ORDINE = "/contrattoforrichiesta";

  public static final String MODIFICA_ORDINE = "/modificarichiesta";

  public static final String POLLING_ORDINE = ORDINE + "/polling";

  public static final String LINK_FILE_TO_ORDER = "/linkFileToRichiesta";

  public static final String GET_ORDINE = "/getrichiesta";

  public static final String GET_ORDINE_BY_RICHIESTE = "/getordinebyrichieste";

  private final static long SLEEP_SECOND = 2000L;

  // public static final String ACTIVE_CONTRACT = "/contratto/attiva";
  // public static final String API_ACTIVE_CONTRACT = BASE_PATH + ACTIVE_CONTRACT;

  private static final String ENTITY_NAME = "richiesta";

  private final RichiestaServiceExt         richiestaServiceExt;
  private final RichiestaService            richiestaService;
  private final CreationOrdineAsyncService  creationOrdineAsyncService;
  private final AsyncPollingJobServiceImpl  asyncPollingJobService;
  private final RichiestaModificaServiceExt richiestaModificaService;

  public RichiestaResourceExt(RichiestaServiceExt richiestaServiceExt, RichiestaService richiestaService,
                              CreationOrdineAsyncService _creationOrdineAsyncService, AsyncPollingJobServiceImpl _asyncJobService,
                              RichiestaModificaServiceExt _richiestaModificaService) {
    this.richiestaServiceExt = richiestaServiceExt;
    this.richiestaService = richiestaService;
    this.creationOrdineAsyncService = _creationOrdineAsyncService;
    this.asyncPollingJobService = _asyncJobService;
    this.richiestaModificaService = _richiestaModificaService;
  }

  @PutMapping(ORDINE + "/save")
  @Timed
  public ResponseEntity<RichiestaDTO> updateRichiesta(@Valid @RequestBody RichiestaDTO richiestaDTO) throws URISyntaxException, Exception {
    log.debug("REST request to update Richiesta : {}", richiestaDTO);
    if (richiestaDTO.getId() == null) {
      RichiestaDTO result = richiestaService.save(richiestaDTO);
      return ResponseEntity.created(new URI("/api/richiestas/" + result.getId()))
                           .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                            .toString()))
                           .body(result);
    }
    RichiestaDTO result = richiestaService.save(richiestaDTO);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, richiestaDTO.getId()
                                                                                              .toString()))
                         .body(result);
  }

  /**
   * POST /richiesta : Create a new richiesta.
   *
   * @param carrelloDTO
   *          the richiestaDTO to create
   * @return the ResponseEntity with status 201 (Created) and with body the new List<Long>, or with status 500 (Bad
   *         Request) if the richiesta has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   * @throws CustomException
   */
  @PostMapping(ORDINE)
  @Timed
  public ResponseEntity<Map<String, String>> createRichiestaByCarrelloDto(@Valid @RequestBody CarrelloDTO carrelloDTO) throws URISyntaxException,
                                                                                                                       CustomException {
    log.debug("REST request to create Richiesta by CarrelloDTO : {}", carrelloDTO);

    if (!checkValidityCarrelloDTO(carrelloDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String currentUserLogin = SecurityUtils.getCurrentUserLogin();
    carrelloDTO.setRichiedente(currentUserLogin);

    String uuidKeyCreationRichiestas = creationOrdineAsyncService.getKeyCacheAndPushDefaultValue();
    log.info("Created Async Job to generate Ordine and Richieste by CarrelloDTO with identifier {}", uuidKeyCreationRichiestas);
    creationOrdineAsyncService.generateOrdineAsyncByCarrelloDTO(uuidKeyCreationRichiestas, carrelloDTO);
    Map<String, String> mapResponse = new HashMap<>();
    mapResponse.put("richiesta", uuidKeyCreationRichiestas);
    return ResponseEntity.ok(mapResponse);
  }

  @GetMapping(POLLING_ORDINE + "/{identificativo}")
  public ResponseEntity<CreatedRichiestasAsyncDTO> pollingRichiesta(@PathVariable @NotNull String identificativo) {
    CreatedRichiestasAsyncDTO createdRichiestasAsyncDTO = (CreatedRichiestasAsyncDTO) asyncPollingJobService.getValue(identificativo);
    return ResponseEntity.ok(createdRichiestasAsyncDTO);
  }

  @PutMapping(CONTRATTO_FOR_ORDINE + "/{codiceContratto}/{identificativoRichiesta}")
  @Timed
  public ResponseEntity<Richiesta> createContractForRichiesta(@PathVariable String codiceContratto,
                                                              @PathVariable String identificativoRichiesta) throws CustomException {

    Richiesta richiesta = null;
    if (!checkValidityParamsContractRichiesta(codiceContratto, identificativoRichiesta)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    try {
      richiesta = richiestaServiceExt.associateContrattoToRichiesta(codiceContratto, identificativoRichiesta);
    } catch (Exception e) {
      log.error("Exception", e);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_AT_CREATE_CONTRACT);
    }

    if (richiesta == null) {
      log.error("Richiesta is null");
      throw CustomException.builder(HttpStatus.CONFLICT)
                           .add(Errno.VALIDATION_FAILED);
    }

    return ResponseEntity.ok(richiesta);
  }

  @GetMapping(GET_ORDINE + "/{identificativo}")
  @Timed
  public ResponseEntity<RichiestaDTO> getRichiestaByIdentificativo(@PathVariable String identificativo) throws CustomException {

    if (StringUtils.isBlank(identificativo)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    RichiestaDTO richiestaDTO = null;
    try {
      richiestaDTO = richiestaServiceExt.getRichiestaByIdentificativo(identificativo, true);
    } catch (Exception e) {
      log.error("Exception", e);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.NOT_FOUND_ORDER);
    }

    return ResponseEntity.ok(richiestaDTO);
  }

  //FIXME aggiungere chiamata WS telepass
  @PostMapping(MODIFICA_ORDINE)
  @Timed
  public ResponseEntity<List<Long>> createRichiestaDiModifica(@Valid @RequestBody CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException, InterruptedException
  {
    log.debug("Request to create RichiestaDiModifica by Carrello: {}", carrelloDTO);
    TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
    if (!checkValidityCarrello(carrelloDTO)) {
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

    String currentUserLogin = SecurityUtils.getCurrentUserLogin();
    carrelloDTO.setRichiedente(currentUserLogin);

    List<Long> resultSet = null;
    try {
      switch (tipoOperazioneModifica){
        case ASSOCIA_TARGA: {
          log.debug("TipoOperazioneModifica = ASSOCIA_TARGA: ");
          resultSet = richiestaModificaService.associaTarga(carrelloDTO.getTipoDispositivo(), carrelloDTO.getDispositivi(), carrelloDTO.getNewVeicolo());
        }
        break;
        case RICHIESTA_PIN: {
          log.debug("TipoOperazioneModifica = RICHIESTA_PIN: ");
          resultSet = richiestaModificaService.richiestaPin(carrelloDTO.getTipoDispositivo(), carrelloDTO.getCodiceAzienda(), carrelloDTO.getDispositivi());
        }
        break;
        default: {
          log.debug("TipoOperazioneModifica = : " + carrelloDTO.getTipoOperazioneModifica());
          resultSet = richiestaModificaService.createRichiestaModifica(carrelloDTO);
        }
      }

    } catch (CustomException e) {
      throw e;
    } catch (Exception e) {

      if (TipoRichiesta.ASSOCIA_TARGA.equals(tipoOperazioneModifica)) {
        log.error("AssociaTarga Exception", e);
        throw CustomException.builder(HttpStatus.BAD_REQUEST).add(Errno.FAILED_ASSOCIATION);


      } else if (TipoRichiesta.RICHIESTA_PIN.equals(tipoOperazioneModifica)) {
        log.error("RichiestaPin Exception", e);
        throw CustomException.builder(HttpStatus.BAD_REQUEST).add(Errno.FAILED_TRACKYCARD_PIN_REQUEST);


 	  } else {
        log.error("CreateRichiestaDiModifica Exception", e);
        throw CustomException.builder(HttpStatus.BAD_REQUEST).add(Errno.EXCEPTION_AT_CREATE_ORDERS);
      }

    }

    waitStandardChangeStatusDevice();
    return ResponseEntity.ok(resultSet);
  }

  @PostMapping(LINK_FILE_TO_ORDER)
  @Timed
  public ResponseEntity<String> linkFileToRichiesta(@RequestBody CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException {
    if (carrelloDTO.getIdRichiestaOperazioneModifica() == null) {
      log.error("idRichiestaOperazioneModifica is null");
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.ORDER_NULL);
    }
    if (carrelloDTO.getDocumento()
                   .isEmpty()) {
      log.error("documento uuid is null");
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_AT_CREATE_ORDERS);
    }

    try {
      RichiestaDTO richiesta = richiestaService.findOne(carrelloDTO.getIdRichiestaOperazioneModifica());
      richiesta.setUuidDocumento(carrelloDTO.getDocumento());
      RichiestaDTO richiestaSaved = richiestaService.save(richiesta);
      richiestaServiceExt.sendMessageForGiustificativoFurtoSmarrimento(richiestaSaved);
    } catch (Exception e) {
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.UPLOAD_DOCUMENT_ON_ORDER);
    }
    return ResponseEntity.ok(null);
  }

  @GetMapping(GET_ORDINE_BY_RICHIESTE + "/{richiesta}")
  @Timed
  public ResponseEntity<List<OrdineDTO>> getOrdineByIdentificatoRichiesta(@PathVariable String richiesta) throws CustomException {
    if (StringUtils.isBlank(richiesta)) {
      log.error("Richiesta is NULL or EMPTY");
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.LIST_RICHIESTE_NULL);
    }

    List<OrdineDTO> ordiniDTO;
    try {
      ordiniDTO = richiestaServiceExt.generateOrdineDTOByRichieste(new ArrayList<>(Arrays.asList(richiesta)));
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw CustomException.builder(HttpStatus.NOT_FOUND)
                           .add(Errno.EXCEPTION_ON_GENERATE_ORDER_DTO);
    }
    if (ordiniDTO == null) {
      log.error("Not generate ORDINI DTO for richieste {}", richiesta);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_ON_GENERATE_ORDER_DTO);
    }

    return ResponseEntity.ok(ordiniDTO);
  }

  @PostMapping(GET_ORDINE_BY_RICHIESTE)
  @Timed
  public ResponseEntity<List<OrdineDTO>> getOrdineByIdentificatoRichieste(@RequestBody List<String> richieste) throws CustomException {
    if (richieste == null) {
      log.error("Richieste is NULL");
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.LIST_RICHIESTE_NULL);
    }

    List<OrdineDTO> ordiniDTO;
    try {
      ordiniDTO = richiestaServiceExt.generateOrdineDTOByRichieste(richieste);
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw CustomException.builder(HttpStatus.NOT_FOUND)
                           .add(Errno.EXCEPTION_ON_GENERATE_ORDER_DTO);
    }
    if (ordiniDTO == null) {
      log.error("Not generate ORDINI DTO for richieste {}", richieste);
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.EXCEPTION_ON_GENERATE_ORDER_DTO);
    }

    return ResponseEntity.ok(ordiniDTO);
  }

  private boolean checkValidityCarrello(CarrelloModificaRichiestaDTO carrelloDTO) {
    if (carrelloDTO == null) {
      log.error("Carrello modifica richiesta is null");
      return false;
    }

    TipoRichiesta tipoOperazioneModifica = carrelloDTO.getTipoOperazioneModifica();
    if (tipoOperazioneModifica == null) {
      log.error("Tipo Operazione Modifica is null");
      return false;
    }

    boolean isValid = true;

    String codiceAzienda = carrelloDTO.getCodiceAzienda();
    if (StringUtils.isBlank(codiceAzienda)) {
      log.error("Codice azienda is null or empty string");
      isValid = false;
    }

    switch (tipoOperazioneModifica) {
    case FURTO:
    case FURTO_CON_SOSTITUZIONE:
    case SMARRIMENTO:
    case SMARRIMENTO_CON_SOSTITUZIONE:
    case MALFUNZIONAMENTO:
    case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
    case DISATTIVAZIONE:
      isValid = checkDevices(carrelloDTO) && checkDeviceType(carrelloDTO);
      break;

    case MEZZO_RITARGATO:
      isValid = checkVehicle(carrelloDTO) && checkPlate(carrelloDTO) && checkCountryPlate(carrelloDTO);
      break;
    case VARIAZIONE_TARGA:
      isValid = checkVehicle(carrelloDTO) && checkNewVehicle(carrelloDTO) ;
      break;
    case ASSOCIA_TARGA:
      isValid = checkNewVehicle(carrelloDTO) && checkIdentifierDevice(carrelloDTO.getDispositivi()) && checkDeviceType(carrelloDTO)
                && checkDeviceType(carrelloDTO, TipoDispositivoEnum.VIACARD) || checkDeviceType(carrelloDTO, TipoDispositivoEnum.TELEPASS_ITALIANO);
      break;
    case RICHIESTA_PIN:
        isValid = checkIdentifierDevice(carrelloDTO.getDispositivi()) && checkDeviceType(carrelloDTO, TipoDispositivoEnum.TRACKYCARD);
        break;
    default:
      log.error("Type of modify is not supported: " + tipoOperazioneModifica.name());
      isValid = false;
      break;
    }

    log.info("Precondition Cart is : {}", isValid);
    return isValid;
  }

  private boolean checkDevices(CarrelloModificaRichiestaDTO carrelloDTO) {
    List<String> devices = carrelloDTO.getDispositivi();
    if (devices == null || (devices != null && devices.isEmpty())) {
      logError("Devices are null/empty for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private void logError(String errorMessage, CarrelloModificaRichiestaDTO carrelloDTO) {
    log.error(errorMessage, carrelloDTO.getTipoOperazioneModifica() != null ? carrelloDTO.getTipoOperazioneModifica()
                                                                                         .name()
                                                                            : null);
  }

  private boolean checkCountryPlate(CarrelloModificaRichiestaDTO carrelloDTO) {
    String nazione = carrelloDTO.getNazione();
    if (StringUtils.isBlank(nazione)) {
      logError("Nazione is null/empty for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private boolean checkPlate(CarrelloModificaRichiestaDTO carrelloDTO) {
    String plate = carrelloDTO.getTarga();
    if (StringUtils.isBlank(plate)) {
      logError("Plate is null/empty for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private boolean checkVehicle(CarrelloModificaRichiestaDTO carrelloDTO) {
    String veicolo = carrelloDTO.getVeicolo();
    if (StringUtils.isBlank(veicolo)) {
      logError("UUID Veicolo is null/empty for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private boolean checkDeviceType(CarrelloModificaRichiestaDTO carrelloDTO, TipoDispositivoEnum deviceType) {
    boolean isValid = checkDeviceType(carrelloDTO);
    if (isValid) {
      TipoDispositivoEnum tipoDispositivo = carrelloDTO.getTipoDispositivo();
      if (isNotEqualDeviceType(tipoDispositivo, deviceType)) {
        log.error("Tipo Dispositivo Param is: {} and it is not equal to: {}", tipoDispositivo, deviceType);
        return false;
      }
      return true;
    }
    return isValid;
  }

  private boolean isNotEqualDeviceType(TipoDispositivoEnum tipoDispositivo, TipoDispositivoEnum deviceType) {
    if (tipoDispositivo.equals(deviceType))
      return false;
    return true;
  }

  private boolean checkDeviceType(CarrelloModificaRichiestaDTO carrelloDTO) {
    TipoDispositivoEnum tipoDispositivo = carrelloDTO.getTipoDispositivo();
    if (tipoDispositivo == null) {
      logError("Tipo Dispositivo is null for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private boolean checkIdentifierDevice(List<String> dispositivi) {
    Optional<String> identifierDevice = dispositivi.stream()
                                                   .findFirst();
    if (!identifierDevice.isPresent()) {
      log.error("Not found identifier device");
      return false;
    }
    return true;
  }

  private boolean checkNewVehicle(CarrelloModificaRichiestaDTO carrelloDTO) {
    String veicoloDaAssociare = carrelloDTO.getNewVeicolo();
    if (StringUtils.isBlank(veicoloDaAssociare)) {
      logError("UUID New Veicolo is null or empty for modify: {}", carrelloDTO);
      return false;
    }
    return true;
  }

  private boolean checkValidityParamsContractRichiesta(String codiceContratto, String identificativoRichiesta) {
    if (StringUtils.isBlank(codiceContratto) || StringUtils.isBlank(identificativoRichiesta)) {
      log.error("Validation Failed: Is Blank => Codice contratto [ {} ] or Identificativo Richiesta [ {} ].", codiceContratto,
                identificativoRichiesta);

      return false;
    }
    return true;
  }

  private boolean checkValidityCarrelloDTO(CarrelloDTO carrelloDTO) {
    if (carrelloDTO == null) {
      return false;
    }

    String tipoServizio = carrelloDTO.getTipoServizio();
    List<String> uuidVeicoli = carrelloDTO.getUuidVeicoli();
    List<DispositivoCarrelloDTO> dispositivi = carrelloDTO.getDispositivi();
    if ((tipoServizio == null) || (uuidVeicoli == null) || (dispositivi == null)) {
      log.error("Validation FAILED: Is null => TipoServizio: [ {} ] or uuidVeicoli [ {} ] or Dispositivi [ {} ].", tipoServizio,
                uuidVeicoli, dispositivi);

      return false;
    }

    if ((carrelloDTO.getUuidVeicoli() != null) && uuidVeicoli.isEmpty()) {
      log.error("Validation CarrelloDTO => Veicoli are EMPTY");
      return false;
    }

    if ((dispositivi != null) && dispositivi.isEmpty()) {
      log.error("Validation CarrelloDTO => Dispositivi are EMPTY");
      return false;
    }

    return true;
  }

  private void waitStandardChangeStatusDevice() throws InterruptedException {
    // Add trhead sleep to attempt change status on device
    Thread.sleep(SLEEP_SECOND);
  }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
                         .body(ex.getPayloadError());
  }

}
