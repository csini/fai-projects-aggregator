package it.fai.ms.efservice.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.efservice.DispositivoDTO;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.repository.dispositivo.AssegnaDispositiviDaDepositoService;
import it.fai.ms.efservice.repository.dispositivo.TipoDispositivoAssegnabile;
import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.MailActivatedDeviceService;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

@RestController
@RequestMapping("/api/deposito/assegnazione")
public class AssegnazioneDispositivoDaDeposito {

  private final Logger log = LoggerFactory.getLogger(AssegnazioneDispositivoDaDeposito.class);


  AssegnaDispositiviDaDepositoService assegnazioneService;
  MailActivatedDeviceService mailservice;
  private final DocumentClient                    documentClient;
  private final TipoDispositivoRepository          tipoDipositivoRepo;

  private static final String PREFIX_TOKEN = "Bearer ";


  @Inject
  public AssegnazioneDispositivoDaDeposito(AssegnaDispositiviDaDepositoService assegnazioneService, MailActivatedDeviceService jmsService, final DocumentClient _documentClient,TipoDispositivoRepository          _tipoDipositivoRepo) {
    this.assegnazioneService     = assegnazioneService;
    this.mailservice = jmsService;
    documentClient               = _documentClient;
    tipoDipositivoRepo = _tipoDipositivoRepo;
  }

  public static class AssegnazioneDispositivoDaDepositoParameter {
    public String                     codiceCliente;
    public String                       idContratto;
    public TipoDispositivoAssegnabile tipoDispositivo;
    public Integer                    quantita;
    public String                     lang;

    public AssegnazioneDispositivoDaDepositoParameter() {}
    public AssegnazioneDispositivoDaDepositoParameter(String codiceCliente, String idContratto, TipoDispositivoAssegnabile tipoDispositivo,
                                                      Integer quantita, String lang) {
      this.codiceCliente   = codiceCliente;
      this.idContratto     = idContratto;
      this.tipoDispositivo = tipoDispositivo;
      this.quantita        = quantita;
      this.lang            = lang;
    }
  }

  @PostMapping("/assegnazione")
  @Transactional
  public ResponseEntity<Void> assegnazioneDispositivoDaDeposito(@RequestBody AssegnazioneDispositivoDaDepositoParameter parameterObject) {

    if (assegnazioneService.countDeviceInDeposito(parameterObject.tipoDispositivo) < parameterObject.quantita) {
      return ResponseEntity.notFound().build();
    }

    List<DispositivoDTO> dispositiviAssegnati = assegnazioneService.assegnaDispositivi(parameterObject.tipoDispositivo, parameterObject.quantita, parameterObject.idContratto);

    mailservice.sendAssociationFromDeposito(parameterObject.codiceCliente, parameterObject.tipoDispositivo, parameterObject.quantita, parameterObject.lang, dispositiviAssegnati);

    if(dispositiviAssegnati.size() !=  parameterObject.quantita) {
      return ResponseEntity.notFound().build();
    }

    return ResponseEntity.ok().build();
  }

  @PostMapping("/assegnazione/{tipoDispositivo}/file/{fileUuid}")
  @Timed
  public ResponseEntity<List<String>> assegnazioneDispositivoDaDepositoByFile(@PathVariable(value = "tipoDispositivo",required = true) TipoDispositivoEnum tipoDispositivoEnum, @PathVariable(value = "fileUuid",required = true) String identificativoDocumento) throws URISyntaxException,
                                                                                                                                           CustomException {
    log.debug("REST request to associate Dispositivo from deposito by file: {}", identificativoDocumento);

    if (StringUtils.isBlank(identificativoDocumento)) {
      log.error("Identificativo documento is BLANK");
      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
                           .add(Errno.VALIDATION_FAILED);
    }

	TipoDispositivo tipoDispositivo = tipoDipositivoRepo.findOneByNome(tipoDispositivoEnum);
	if(tipoDispositivo == null) {
		log.error("tipoDispositivo not valid {}",tipoDispositivoEnum);
	      throw CustomException.builder(HttpStatus.PRECONDITION_FAILED)
	                           .add(Errno.VALIDATION_FAILED);
	}

    String token = PREFIX_TOKEN + SecurityUtils.getCurrentUserJWT();
    List<String[]> csvRow = documentClient.getListSerialDeviceFromCSVSemicolon(token, identificativoDocumento);

    log.debug("Processing {} rows",csvRow.size());
    List<String> errors = new ArrayList<String>();
    int i = 0;
    for (String[] cells : csvRow) {
    	i++;
    	if(cells.length < 2) {
    		errors.add("La riga "+i+" non è valida: "+Arrays.toString(cells));
    		break;
    	}
    	String error = assegnazioneService.assegnaDispositivi(tipoDispositivo, cells[0], cells[1]);
    	if(error != null) {
    		errors.add("La riga "+i+" non è valida perchè "+error+": "+Arrays.toString(cells));
    	}
	}

    return ResponseEntity.ok(errors);
  }

  @GetMapping("/dispositivi")
  String[] getDispositiviAssegnabili() {
    return Arrays.stream(TipoDispositivoAssegnabile.values()).map(t->t.name()).collect(Collectors.toList()).toArray(new String[] {});
  }

  @GetMapping("/dispositiviDaFile")
  String[] getDispositiviAssegnabiliDaFile() {
    return Arrays.asList(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR.name(),TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT.name(),TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO.name()).toArray(new String[3]);
  }

}
