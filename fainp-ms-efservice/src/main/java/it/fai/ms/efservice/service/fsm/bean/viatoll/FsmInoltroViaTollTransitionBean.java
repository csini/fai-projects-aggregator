package it.fai.ms.efservice.service.fsm.bean.viatoll;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.viatoll.FsmInoltroViaToll;

@WithStateMachine(id = FsmInoltroViaToll.FSM_INOLTRO_VIATOLL)
public class FsmInoltroViaTollTransitionBean extends AbstractFsmRichiestaTransition {
}
