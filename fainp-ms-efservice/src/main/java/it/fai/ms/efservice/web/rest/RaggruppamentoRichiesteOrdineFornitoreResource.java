package it.fai.ms.efservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import it.fai.ms.efservice.service.RaggruppamentoRichiesteOrdineFornitoreService;
import it.fai.ms.efservice.web.rest.util.HeaderUtil;
import it.fai.ms.efservice.service.dto.RaggruppamentoRichiesteOrdineFornitoreDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RaggruppamentoRichiesteOrdineFornitore.
 */
@RestController
@RequestMapping("/api")
public class RaggruppamentoRichiesteOrdineFornitoreResource {

    private final Logger log = LoggerFactory.getLogger(RaggruppamentoRichiesteOrdineFornitoreResource.class);

    private static final String ENTITY_NAME = "raggruppamentoRichiesteOrdineFornitore";

    private final RaggruppamentoRichiesteOrdineFornitoreService raggruppamentoRichiesteOrdineFornitoreService;

    public RaggruppamentoRichiesteOrdineFornitoreResource(RaggruppamentoRichiesteOrdineFornitoreService raggruppamentoRichiesteOrdineFornitoreService) {
        this.raggruppamentoRichiesteOrdineFornitoreService = raggruppamentoRichiesteOrdineFornitoreService;
    }

    /**
     * POST  /raggruppamento-richieste-ordine-fornitores : Create a new raggruppamentoRichiesteOrdineFornitore.
     *
     * @param raggruppamentoRichiesteOrdineFornitoreDTO the raggruppamentoRichiesteOrdineFornitoreDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new raggruppamentoRichiesteOrdineFornitoreDTO, or with status 400 (Bad Request) if the raggruppamentoRichiesteOrdineFornitore has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/raggruppamento-richieste-ordine-fornitores")
    @Timed
    public ResponseEntity<RaggruppamentoRichiesteOrdineFornitoreDTO> createRaggruppamentoRichiesteOrdineFornitore(@RequestBody RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO) throws URISyntaxException {
        log.debug("REST request to save RaggruppamentoRichiesteOrdineFornitore : {}", raggruppamentoRichiesteOrdineFornitoreDTO);
        if (raggruppamentoRichiesteOrdineFornitoreDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new raggruppamentoRichiesteOrdineFornitore cannot already have an ID")).body(null);
        }
        RaggruppamentoRichiesteOrdineFornitoreDTO result = raggruppamentoRichiesteOrdineFornitoreService.save(raggruppamentoRichiesteOrdineFornitoreDTO);
        return ResponseEntity.created(new URI("/api/raggruppamento-richieste-ordine-fornitores/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /raggruppamento-richieste-ordine-fornitores : Updates an existing raggruppamentoRichiesteOrdineFornitore.
     *
     * @param raggruppamentoRichiesteOrdineFornitoreDTO the raggruppamentoRichiesteOrdineFornitoreDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated raggruppamentoRichiesteOrdineFornitoreDTO,
     * or with status 400 (Bad Request) if the raggruppamentoRichiesteOrdineFornitoreDTO is not valid,
     * or with status 500 (Internal Server Error) if the raggruppamentoRichiesteOrdineFornitoreDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/raggruppamento-richieste-ordine-fornitores")
    @Timed
    public ResponseEntity<RaggruppamentoRichiesteOrdineFornitoreDTO> updateRaggruppamentoRichiesteOrdineFornitore(@RequestBody RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO) throws URISyntaxException {
        log.debug("REST request to update RaggruppamentoRichiesteOrdineFornitore : {}", raggruppamentoRichiesteOrdineFornitoreDTO);
        if (raggruppamentoRichiesteOrdineFornitoreDTO.getId() == null) {
            return createRaggruppamentoRichiesteOrdineFornitore(raggruppamentoRichiesteOrdineFornitoreDTO);
        }
        RaggruppamentoRichiesteOrdineFornitoreDTO result = raggruppamentoRichiesteOrdineFornitoreService.save(raggruppamentoRichiesteOrdineFornitoreDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, raggruppamentoRichiesteOrdineFornitoreDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /raggruppamento-richieste-ordine-fornitores : get all the raggruppamentoRichiesteOrdineFornitores.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of raggruppamentoRichiesteOrdineFornitores in body
     */
    @GetMapping("/raggruppamento-richieste-ordine-fornitores")
    @Timed
    public List<RaggruppamentoRichiesteOrdineFornitoreDTO> getAllRaggruppamentoRichiesteOrdineFornitores() {
        log.debug("REST request to get all RaggruppamentoRichiesteOrdineFornitores");
        return raggruppamentoRichiesteOrdineFornitoreService.findAll();
        }

    /**
     * GET  /raggruppamento-richieste-ordine-fornitores/:id : get the "id" raggruppamentoRichiesteOrdineFornitore.
     *
     * @param id the id of the raggruppamentoRichiesteOrdineFornitoreDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the raggruppamentoRichiesteOrdineFornitoreDTO, or with status 404 (Not Found)
     */
    @GetMapping("/raggruppamento-richieste-ordine-fornitores/{id}")
    @Timed
    public ResponseEntity<RaggruppamentoRichiesteOrdineFornitoreDTO> getRaggruppamentoRichiesteOrdineFornitore(@PathVariable Long id) {
        log.debug("REST request to get RaggruppamentoRichiesteOrdineFornitore : {}", id);
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO = raggruppamentoRichiesteOrdineFornitoreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(raggruppamentoRichiesteOrdineFornitoreDTO));
    }

    /**
     * DELETE  /raggruppamento-richieste-ordine-fornitores/:id : delete the "id" raggruppamentoRichiesteOrdineFornitore.
     *
     * @param id the id of the raggruppamentoRichiesteOrdineFornitoreDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/raggruppamento-richieste-ordine-fornitores/{id}")
    @Timed
    public ResponseEntity<Void> deleteRaggruppamentoRichiesteOrdineFornitore(@PathVariable Long id) {
        log.debug("REST request to delete RaggruppamentoRichiesteOrdineFornitore : {}", id);
        raggruppamentoRichiesteOrdineFornitoreService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
