package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Set;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.OrdineClienteCacheDTO;

public interface OrdineClienteServiceExt {

  /**
   * @param ordineCliente
   * @return
   */
  OrdineCliente save(OrdineCliente ordineCliente);

  /**
   * @param ordineCliente
   * @return
   */
  OrdineCliente loadClienteAssegnatarioOnOrdineCliente(OrdineCliente ordineCliente);

  /**
   * @param idOrdineCliente
   * @return
   */
  OrdineCliente loadClienteAssegnatarioOnOrdineCliente(Long idOrdineCliente);

  /**
   * @param ordineClienteId
   * @param b
   */
  OrdineCliente findById(Long ordineClienteId, boolean eager);

  /**
   * @param identificativoOrdineCliente
   * @return
   */
  OrdineCliente findByIdentificativoOrdineCliente(String identificativoOrdineCliente);

  /**
   * @return
   */
  Set<OrdineCliente> findAll();

  /**
   * @param identificativo
   * @return
   */
  Set<Richiesta> findRichiesteByIdentificativoOrdine(String identificativo);

  OrdineCliente updateNumeroOrdine(OrdineCliente ordineCliente);

  Set<OrdineClienteCacheDTO> findOrdineClienteFilteredByStatoNotIsAndNotInCache(StatoOrdineCliente stato, List<String> keysId);

  OrdineCliente createOrdineClienteByRichieste(List<Richiesta> richieste, CarrelloModificaRichiestaDTO carrelloDTO);

  void delete(OrdineCliente ordineCliente);

  OrdineCliente saveAndFlush(OrdineCliente ordineCliente);
  
  List<OrdineClienteCacheDTO> findByStatoNotIs(StatoOrdineCliente stato);

}
