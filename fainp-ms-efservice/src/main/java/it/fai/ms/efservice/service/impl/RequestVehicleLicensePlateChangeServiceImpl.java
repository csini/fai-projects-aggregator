package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.MezzoRitargatoDTO;
import it.fai.ms.common.jms.publisher.JmsTopicSenderUtil;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RequestVehicleLicensePlateChangeService;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;

@Service
@Transactional
public class RequestVehicleLicensePlateChangeServiceImpl extends RequestUtilService implements RequestVehicleLicensePlateChangeService {

  private final static JmsTopicNames TOPIC_VEHICLE = JmsTopicNames.VEHICLE;

  private final Logger log = LoggerFactory.getLogger(getClass());

  private List<TipoDispositivoEnum> particularDeviceType = new ArrayList<>();

  private List<TipoDispositivoEnum> deviceThatShouldNotGenerateRequest = new ArrayList<>();

  private final DispositivoServiceExt deviceServiceExt;

  private final AssociazioneDVServiceExt associazioneDvService;

  private final JmsProperties jmsProperties;

  public RequestVehicleLicensePlateChangeServiceImpl(final AssociazioneDVServiceExt _associazioneDvService,
                                                     final DispositivoServiceExt _deviceServiceExt,
                                                     final ClienteFaiServiceExt clienteFaiService, final JmsProperties _jmsProperties) {
    super(clienteFaiService);
    deviceServiceExt = _deviceServiceExt;
    associazioneDvService = _associazioneDvService;
    jmsProperties = _jmsProperties;
    particularDeviceType.add(TipoDispositivoEnum.VIACARD);
    deviceThatShouldNotGenerateRequest.add(TipoDispositivoEnum.TESSERA_CARONTE);
  }

  @Override
  public List<Richiesta> generateModificationRequestDeviceService(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException {
    log.info("Start creationModificationRequest by carrelloDTO: {}", carrelloDTO);
    List<Richiesta> requests = new ArrayList<>();
    OrdineCliente clientOrderForRequests = null;

    String targa = carrelloDTO.getTarga();
    String nazione = carrelloDTO.getNazione();
    try {
      List<AssociazioneDV> assDvs = findRelationsDevicesVehicles(carrelloDTO.getVeicolo(), carrelloDTO.getCodiceAzienda());
      boolean isOneDeviceInYellowState = isOneDeviceInYellowState(assDvs);
      if (isOneDeviceInYellowState) {
        throw new IllegalAccessException("There is a device with operation pending...");
      }

      List<AssociazioneDV> assDvParticulaDevice = getAssociazioniByParticularDevice(assDvs);
      updateAssociationParticularDevice(assDvParticulaDevice);

      Map<TipoDispositivoEnum, List<Dispositivo>> mapDevicesFilteredByDeviceType = new HashMap<>();
      mapDevicesFilteredByDeviceType = mapFilterToExcludeParticularDeviceAndToNotGeneratedRequest(assDvs, carrelloDTO.getDispositivi());

      requests = createRequestDefaultDeviceTypes(mapDevicesFilteredByDeviceType, carrelloDTO);
      if (requests != null && !requests.isEmpty()) {
        log.debug("Create ClientOrder....");
        clientOrderForRequests = createDefaultOrdineCliente(carrelloDTO.getCodiceAzienda());
        log.debug("Assign Targa {} and Nazione at {} requests generated", targa, nazione, requests.size());
        assignTargaNazioneVehicleOnRequestsByCarrelloDTO(requests, carrelloDTO);
      }

      log.debug("Send message to change Targa and Nazione to: {} - {}", targa, nazione);
      sendMessageToChangeTargaNazione(carrelloDTO);
    } catch (Exception e) {
      log.error("Exception on generate Requests of modification vehicle license plate: ", e);
      throw CustomException.builder(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    assignClientOrderOnRequests(requests, clientOrderForRequests);
    return requests;
  }

  private void updateAssociationParticularDevice(List<AssociazioneDV> assDvParticulaDevice) {
    assDvParticulaDevice.forEach(assDv -> {
      AssociazioneDV associazione = associazioneDvService.save(assDv.data(Instant.now()));
      log.debug("Updated data  on associazioneDV: {}", associazione);
    });

  }

  private List<AssociazioneDV> getAssociazioniByParticularDevice(List<AssociazioneDV> assDvs) {
    return assDvs.stream()
                 .filter(assDv -> assDv.getDispositivo() != null && assDv.getDispositivo()
                                                                         .getTipoDispositivo() != null
                                  && particularDeviceType.contains(assDv.getDispositivo()
                                                                        .getTipoDispositivo()
                                                                        .getNome()))
                 .collect(toList());
  }

  private boolean isOneDeviceInYellowState(List<AssociazioneDV> assDvs) {
    List<Dispositivo> devices = assDvs.stream()
                                      .map(assDv -> assDv.getDispositivo())
                                      .collect(toList());
    Optional<Dispositivo> optDevice = devices.stream()
                                             .filter(d -> StatoDispositivo.getYellowState()
                                                                          .contains(d.getStato()))
                                             .findFirst();
    if (optDevice.isPresent()) {
      log.warn("Device {} is in status with operation pending...", optDevice.get());
      return true;
    }
    return false;
  }

  private List<Richiesta> createRequestDefaultDeviceTypes(Map<TipoDispositivoEnum, List<Dispositivo>> mapDevicesByDeviceType,
                                                          CarrelloModificaRichiestaDTO carrelloDTO) {
    Set<TipoDispositivoEnum> keysMap = mapDevicesByDeviceType.keySet();
    List<Richiesta> requests = keysMap.stream()
                                      .filter(key -> !particularDeviceType.contains(key))
                                      .map(key -> filterDeviceByStatus(mapDevicesByDeviceType.get(key)))
                                      .map(devicesFiltered -> {
                                        return createRequestByDevicesAndCarrelloDTO(devicesFiltered, carrelloDTO);
                                      })
                                      .flatMap(requestsDeviceTyped -> requestsDeviceTyped.stream())
                                      .collect(toList());

    return requests;
  }

  private List<Richiesta> createRequestByDevicesAndCarrelloDTO(List<Dispositivo> devicesFiltered,
                                                               CarrelloModificaRichiestaDTO carrelloDTO) {
    return devicesFiltered.stream()
                          .map(device -> {
                            try {
                              return createDefaultRequestVehicleLicensePlate(device, carrelloDTO);
                            } catch (Exception e) {
                              throw new RuntimeException(e);
                            }
                          })
                          .collect(toList());
  }

  private List<Dispositivo> filterDeviceByStatus(List<Dispositivo> devicesTyped) {
    List<Dispositivo> devices = new ArrayList<>();
    if (devicesTyped != null && !devicesTyped.isEmpty()) {
      devices = devicesTyped.stream()
                            .filter(device -> !isDeviceStatusIn(device, StatoDispositivo.getBlueState()))
                            .map(device -> {
                              if (isDeviceStatusIn(device, StatoDispositivo.getYellowState())) {
                                try {
                                  throw new IllegalAccessException("Device " + device.getIdentificativo() + " has a pending operations...");
                                } catch (IllegalAccessException e) {
                                  throw new RuntimeException(e);
                                }
                              }
                              return device;
                            })
                            .collect(toList());
    }
    return devices;
  }

  private Richiesta createDefaultRequestVehicleLicensePlate(Dispositivo device, CarrelloModificaRichiestaDTO carrelloDTO) throws Exception {
    TipoRichiesta requestType = carrelloDTO.getTipoOperazioneModifica();
    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    Richiesta richiesta = createDefaultRequestByTypeAndDeviceType(requestType, tipoDispositivo);

    richiesta.setUuidDocumento(carrelloDTO.getDocumento());
    richiesta.setContratto(device.getContratto());
    richiesta.setStato(getStatusRequestByDeviceType(device.getTipoDispositivo()
                                                          .getNome()));
    richiesta.addDispositivo(device);

    saveDevice(device);

    return richiesta;
  }

  private void saveDevice(Dispositivo device) throws Exception {
    try {
      deviceServiceExt.save(device);
    } catch (Exception e) {
      log.error("Exception on save device: {}", device.getIdentificativo(), e);
      throw e;
    }
  }

  private List<AssociazioneDV> findRelationsDevicesVehicles(String uuidVeicolo, String codiceCliente) {
    return associazioneDvService.findByUuidVeicoloAndCodiceCliente(uuidVeicolo, codiceCliente);
  }

  private Map<TipoDispositivoEnum, List<Dispositivo>> mapFilterToExcludeParticularDeviceAndToNotGeneratedRequest(List<AssociazioneDV> assDvs,
                                                                                                                 List<String> devicesIdentifier) {
    return assDvs.stream()
                 .filter(assDv -> assDv != null && assDv.getDispositivo() != null && assDv.getDispositivo()
                                                                                          .getTipoDispositivo() != null)
                 .filter((assDv -> (isNotParticularDevice(assDv.getDispositivo()
                                                               .getTipoDispositivo()
                                                               .getNome())
                                    && isDeviceThatShouldNotGenerateRequest(assDv.getDispositivo()
                                                                                 .getTipoDispositivo()
                                                                                 .getNome()))))
                 .map(assDv -> assDv.getDispositivo())
                 .filter(d -> StatoDispositivo.getGreenState()
                                              .contains(d.getStato()))
                 .filter(d -> devicesIdentifier.contains(d.getIdentificativo()))
                 .collect(toMap(d -> d.getTipoDispositivo()
                                      .getNome(),
                                d -> new ArrayList<>(Arrays.asList(d)), (list1, list2) -> {
                                  list1.addAll(list2);
                                  return list1;
                                }, HashMap::new));
  }

  private boolean isNotParticularDevice(TipoDispositivoEnum deviceType) {
    return !particularDeviceType.contains(deviceType);
  }

  private boolean isDeviceThatShouldNotGenerateRequest(TipoDispositivoEnum deviceType) {
    return !deviceThatShouldNotGenerateRequest.contains(deviceType);
  }

  private boolean isDeviceStatusIn(Dispositivo device, Set<StatoDispositivo> states) {
    StatoDispositivo deviceStatus = device.getStato();
    if (states.contains(deviceStatus))
      return true;
    return false;
  }

  private void sendMessageToChangeTargaNazione(CarrelloModificaRichiestaDTO carrelloDTO) throws JMSException {
    MezzoRitargatoDTO mezzoRitargatoDTO = mappingToMezzoRitargatoDTO(carrelloDTO);
    log.debug("I'll send on {} the following {}: {}", TOPIC_VEHICLE.name(), mezzoRitargatoDTO.getClass(), mezzoRitargatoDTO.toString());
    JmsTopicSenderUtil.publish(jmsProperties, TOPIC_VEHICLE, mezzoRitargatoDTO);
    log.info("JMS message for {} was sent!", mezzoRitargatoDTO.getClass());
  }

  private MezzoRitargatoDTO mappingToMezzoRitargatoDTO(CarrelloModificaRichiestaDTO carrelloDTO) {
    String uuidVehicle = carrelloDTO.getVeicolo();
    String targa = carrelloDTO.getTarga();
    String nazione = carrelloDTO.getNazione();
    return new MezzoRitargatoDTO(uuidVehicle, targa, nazione);
  }

}
