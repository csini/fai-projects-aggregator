package it.fai.ms.efservice.service.async.impl;

import it.fai.ms.efservice.asyncjob.AsyncJobResponse;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.service.ProcessTransitiService;
import it.fai.ms.efservice.service.TransitiAsyncServiceExt;
import it.fai.ms.efservice.service.dto.ConfermaTransitoDTO;
import it.fai.ms.efservice.web.rest.errors.Errno;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.CompletableFuture;

@Service
public class TransitiAsyncServiceExtImpl implements TransitiAsyncServiceExt {
  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AsyncJobServiceImpl    asyncJobService;
  private final ProcessTransitiService processTransitiService;

  public TransitiAsyncServiceExtImpl(AsyncJobServiceImpl asyncJobService, final ProcessTransitiService processTransitiService) {
    this.asyncJobService = asyncJobService;
    this.processTransitiService = processTransitiService;
  }

  @Override
  public String getKeyCacheAndPushDefaultValue(String key) {
    return asyncJobService.generateKeyCacheAndPushValue(key, new AsyncJobResponse(StateChangeStatusFsm.PENDING, null));
  }

  @Override
  public void processAsyncTransiti(String identificativoJob, ConfermaTransitoDTO dto) {
    log.debug("Call async fro processing Transiti: dto {}", dto);
    CompletableFuture.supplyAsync(() -> processTransitiService.process(dto))
                     .exceptionally(e -> {
                       log.error("Exception in Transiti Resource method processAsyncTransiti: {}", e);
                       AsyncJobResponse response = new AsyncJobResponse(StateChangeStatusFsm.ERROR, null);
                       response.add(Errno.EXCEPTION_ON_PROCESSING_TRANSIT);
                       return response;
                     })
                     .thenAccept(state -> asyncJobService.save(identificativoJob, state));
  }

}
