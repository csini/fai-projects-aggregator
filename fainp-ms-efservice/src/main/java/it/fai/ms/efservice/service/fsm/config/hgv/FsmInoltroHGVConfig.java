package it.fai.ms.efservice.service.fsm.config.hgv;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.action.FsmActionGenerateContratto;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmInoltroHGVConfig.INOLTRO_HGV)
public class FsmInoltroHGVConfig extends FsmRichiestaConfig {

  public static final String INOLTRO_HGV = "inoltroHGV";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceProducerService deviceProducerService;

  // private final FsmSenderToQueue senderFsmService;

  // private final JmsProperties jmsProperties;

  private final ManageDeviceSentModeService  manageDeviceSentModeService;
  private final GeneratorContractCodeService generatorContractCodeService;

  public FsmInoltroHGVConfig(final GeneratorContractCodeService generatorContractCodeService, final FsmSenderToQueue _senderFsmService,
                             // final JmsProperties _jmsProperties,
                             final DeviceProducerService _deviceProducerService,
                             final ManageDeviceSentModeService _manageDeviceSentModeService) {
    super();
    this.generatorContractCodeService = generatorContractCodeService;
    // senderFsmService = _senderFsmService;
    // jmsProperties = _jmsProperties;
    deviceProducerService = _deviceProducerService;
    manageDeviceSentModeService = _manageDeviceSentModeService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.INOLTRO_HGV.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfInoltroHGV();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
               .target(StatoRichiesta.ACCETTATO)
               .event(RichiestaEvent.INITIAL)
               .and()
               .withExternal()// AUTO
               .source(StatoRichiesta.ACCETTATO)
               .target(StatoRichiesta.DA_INOLTRARE)
               .action(new FsmActionGenerateContratto(generatorContractCodeService))
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.INOLTRATO)
               .event(RichiestaEvent.MU_INOLTRATO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.ATTIVO_EVASO)
               .event(RichiestaEvent.MU_CONSUMO)
               .and()
               .withExternal()// MANUAL
               .source(StatoRichiesta.DA_INOLTRARE)
               .target(StatoRichiesta.ANNULLATO_OPERATORE_FAI)
               .event(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE);

  }

}
