package it.fai.ms.efservice.service.async.impl;

import java.io.Serializable;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.service.ChangeStatusFsmService;
import it.fai.ms.efservice.service.ContrattoNavService;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.FsmContrattoDTO;
import it.fai.ms.efservice.service.dto.FsmDTO;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.dto.FsmRichiesteDTO;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

@Service
public class FsmAsyncServiceImpl implements FsmAsyncService {

  private final static String FSM_CHANGE_STATUS = "FSM";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AsyncJobServiceImpl    asyncJobService;
  private final ChangeStatusFsmService changeStatusFsmService;
  private final ContrattoNavService    contrattoNavService;

  public FsmAsyncServiceImpl(final AsyncJobServiceImpl _asyncJobService, final ChangeStatusFsmService _changeStatusFsmService,
                             final ContrattoNavService _contrattoNavService) {
    asyncJobService = _asyncJobService;
    changeStatusFsmService = _changeStatusFsmService;
    contrattoNavService = _contrattoNavService;
  }

  @Override
  public String getKeyCacheAndPushDefaultValue() {
    return asyncJobService.generateKeyCacheAndPushValue(FSM_CHANGE_STATUS, StateChangeStatusFsm.PENDING);
  }

  @Override
  public String getKeyCacheAndPushDefaultValue(String key) {
    return asyncJobService.generateKeyCacheAndPushValue(key, StateChangeStatusFsm.PENDING);
  }

  @Override
  public void changeAsyncFsmStatus(String identificativoJob, FsmRichiesteDTO fsmRichiesteDTO) {
    log.debug("Call async change status for richieste: FSMchangeStatusDTO {}", fsmRichiesteDTO);
    CompletableFuture.supplyAsync(() -> {
      return changeStatusFsmService.changeStatusRichiesta(fsmRichiesteDTO);
    })
                     .exceptionally(e -> {
                       log.error("Exception in FSM Resource method changeStatusToRichieste: ", e);
                       return StateChangeStatusFsm.ERROR;
                     })
                     .thenAccept(state -> asyncJobService.save(identificativoJob, state));
  }

  @Override
  public void changeAsyncFsmContratto(String identificativoJob, FsmContrattoDTO fsmDTO) {
    log.debug("Call async change status for Contratti: FsmContrattoDTO {}", fsmDTO);
    CompletableFuture.supplyAsync(() -> {
      return changeStatusFsmService.changeFsmContrattoStatus(fsmDTO);
    })
                     .exceptionally(e -> {
                       log.error("Exception in FSM Resource method changeStatusContratto: {}", e);
                       return StateChangeStatusFsm.ERROR;
                     })
                     .thenAccept(state -> asyncJobService.save(identificativoJob, state));

  }

  @Override
  public void changeAsyncFsmDispositivo(String identificativoJob, FsmDispositivoDTO fsmDTO) {
    log.debug("Call async change status for Dispositivi: FsmDispositivoDTO {}", fsmDTO);
    CompletableFuture.supplyAsync(() -> {
      return changeStatusFsmService.changeFsmDispositivoStatus(fsmDTO);
    })
                     .exceptionally(e -> {
                       log.error("Exception in FSM Resource method changeStatusDispositivo: {}", e);
                       return StateChangeStatusFsm.ERROR;
                     })
                     .thenAccept(state -> asyncJobService.save(identificativoJob, state));

  }

  @Override
  public void changeAsyncStatusContratto(String identificativoJob, FsmDTO fsmDTO) {
    log.debug("Call async change status for Contratti: FsmContrattoDTO {}", fsmDTO);
    CompletableFuture.supplyAsync(() -> {
      return contrattoNavService.changeAsyncStatusContractByNav(fsmDTO.getIdentificativi(), ContrattoEvent.valueOf(fsmDTO.getOperazione()));
    })
                     .exceptionally(e -> {
                       log.error("Exception in FSM Resource method changeStatusContratto: {}", e);
                       return StateChangeStatusFsm.ERROR;
                     })
                     .thenAccept(state -> saveStateJob(identificativoJob, state));
  }
  
  @Override
  public void saveStateJob(String identifierJob, Serializable state) {
    asyncJobService.save(identifierJob, state);
    log.debug("Updated state to {} on job: {}", state, identifierJob);
  }

}
