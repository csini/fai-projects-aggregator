package it.fai.ms.efservice.service;

public class RichiestaWithoutDeviceTypeException extends RuntimeException {

  private static final long serialVersionUID = -8824041112903885397L;

  public RichiestaWithoutDeviceTypeException() {
  }

  public RichiestaWithoutDeviceTypeException(String _message) {
    super(_message);
  }

  public RichiestaWithoutDeviceTypeException(Throwable _cause) {
    super(_cause);
  }

  public RichiestaWithoutDeviceTypeException(String _message, Throwable _cause) {
    super(_message, _cause);
  }

}
