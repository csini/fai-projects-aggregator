package it.fai.ms.efservice.rules.engine.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import it.fai.ms.efservice.rules.engine.util.EuroClassConverter;

public class RuleEngineVehicle implements Serializable {

  private static final long serialVersionUID = -6953203496560416472L;

  public static RuleEngineVehicle as(final String _id) {
    return new RuleEngineVehicle(new RuleEngineVehicleId(_id));
  }

  private Integer                                  axes;
  private String                                   category;
  private String                                   country;
  private RuleEngineVehicleDieselParticulateFilter dieselParticulateFilter;
  private String                                   euroClass;
  private RuleEngineVehicleId                      id;
  private String                                   licensePlate;
  private String                                   make;
  private String                                   type;
  private String                                   fuelType;
  private Float                                    length;
  private Integer                                  euroVehicle;
  private Float                                    height;
  private Integer                                  payload;
  private Float                                    width;
  private Integer                                  numeroAssiPienoCarico;
  private String                                   numberChassis;
  private String                                   color;
  private String                                   model;

  private RuleEngineVehicleWeight weight;

  public RuleEngineVehicle(final RuleEngineVehicleId _id) {
    id = Optional.ofNullable(_id)
                 .orElseThrow(() -> new IllegalArgumentException("RuleEngineVehicle Id is mandatory"));
    weight = new RuleEngineVehicleWeight();
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean isEquals = false;
    if (getClass().isInstance(_obj)) {
      return Objects.equals(((RuleEngineVehicle) _obj).getId(), id) && Objects.equals(((RuleEngineVehicle) _obj).getAxes(), axes)
             && Objects.equals(((RuleEngineVehicle) _obj).getCategory(), category)
             && Objects.equals(((RuleEngineVehicle) _obj).getCountry(), country)
             && Objects.equals(((RuleEngineVehicle) _obj).getDieselParticulateFilter(), dieselParticulateFilter)
             && Objects.equals(((RuleEngineVehicle) _obj).getEuroClass(), euroClass)
             && Objects.equals(((RuleEngineVehicle) _obj).getLicensePlate(), licensePlate)
             && Objects.equals(((RuleEngineVehicle) _obj).getMake(), make) && Objects.equals(((RuleEngineVehicle) _obj).getType(), type)
             && Objects.equals(((RuleEngineVehicle) _obj).getWeight(), weight)
             && Objects.equals(((RuleEngineVehicle) _obj).getFuelType(), fuelType)
             && Objects.equals(((RuleEngineVehicle) _obj).getLength(), length)
             && Objects.equals(((RuleEngineVehicle) _obj).getModel(), model)
             && Objects.equals(((RuleEngineVehicle) _obj).getColor(), color);

    }
    return isEquals;
  }

  public Integer getAxes() {
    return axes;
  }

  public String getCategory() {
    return category;
  }

  public String getCountry() {
    return country;
  }

  public RuleEngineVehicleDieselParticulateFilter getDieselParticulateFilter() {
    return dieselParticulateFilter;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public RuleEngineVehicleId getId() {
    return id;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public String getMake() {
    return make;
  }

  public String getType() {
    return type;
  }

  public RuleEngineVehicleWeight getWeight() {
    return weight;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, axes, category, country, dieselParticulateFilter, euroClass, licensePlate, make, weight, type, fuelType, length,
                        color, model);
  }

  public void setAxes(final Integer _axies) {
    axes = _axies;
  }

  public void setCategory(final String _category) {
    category = _category;
  }

  public void setCountry(final String _country) {
    country = _country;
  }

  public void setDieselParticulateFilter(final RuleEngineVehicleDieselParticulateFilter _dieselParticulateFilter) {
    dieselParticulateFilter = _dieselParticulateFilter;
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
    setEuroVehicle(EuroClassConverter.convertEuroClassToInteger(euroClass));
  }

  public void setLicensePlate(final String _licensePlate) {
    licensePlate = _licensePlate;
  }

  public void setMake(final String _make) {
    make = _make;
  }

  public void setType(final String _type) {
    type = _type;
  }

  public String getFuelType() {
    return fuelType;
  }

  public void setFuelType(String fuelType) {
    this.fuelType = fuelType;
  }

  public Float getLength() {
    return length;
  }

  public void setLength(Float length) {
    this.length = length;
  }

  public Integer getEuroVehicle() {
    return euroVehicle;
  }

  public void setEuroVehicle(Integer euroVehicle) {
    this.euroVehicle = euroVehicle;
  }

  public Float getHeight() {
    return height;
  }

  public void setHeight(Float height) {
    this.height = height;
  }

  public Integer getPayload() {
    return payload;
  }

  public void setPayload(Integer payload) {
    this.payload = payload;
  }

  public Float getWidth() {
    return width;
  }

  public void setWidth(Float width) {
    this.width = width;
  }

  public Integer getNumeroAssiPienoCarico() {
    return numeroAssiPienoCarico;
  }

  public void setNumeroAssiPienoCarico(Integer numeroAssiPienoCarico) {
    this.numeroAssiPienoCarico = numeroAssiPienoCarico;
  }

  public String getNumberChassis() {
    return numberChassis;
  }

  public void setNumberChassis(String numberChassis) {
    this.numberChassis = numberChassis;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("RuleContextVehicle [axes=");
    builder.append(this.axes);
    builder.append(", category=");
    builder.append(this.category);
    builder.append(", country=");
    builder.append(this.country);
    builder.append(", dieselParticulateFilter=");
    builder.append(this.dieselParticulateFilter);
    builder.append(", euroClass=");
    builder.append(this.euroClass);
    builder.append(", id=");
    builder.append(this.id);
    builder.append(", licensePlate=");
    builder.append(this.licensePlate);
    builder.append(", make=");
    builder.append(this.make);
    builder.append(", type=");
    builder.append(this.type);
    builder.append(", weight=");
    builder.append(this.weight);
    builder.append(", fuelType=");
    builder.append(this.fuelType);
    builder.append(", length=");
    builder.append(this.length);
    builder.append(", height=");
    builder.append(this.height);
    builder.append(", width=");
    builder.append(this.width);
    builder.append(", payload=");
    builder.append(this.payload);
    builder.append(", numAssiPienoCarico=");
    builder.append(this.numeroAssiPienoCarico);
    builder.append(", numberChassis=");
    builder.append(this.numberChassis);
    builder.append(", model=");
    builder.append(this.model);
    builder.append(", color=");
    builder.append(this.color);
    builder.append("]");
    return builder.toString();
  }

}
