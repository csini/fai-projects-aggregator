package it.fai.ms.efservice.repository;

import it.fai.ms.efservice.domain.CategoriaServizio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data JPA repository for the CategoriaServizio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoriaServizioRepositoryExt extends JpaRepository<CategoriaServizio, Long> {
    CategoriaServizio findByNome(String nome);
}
