package it.fai.ms.efservice.service.fsm.guard;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import com.google.common.collect.ImmutableSet;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.TrackyCardGoBoxService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public class GuardCheckIfPresentTrackyCardTrue implements Guard<StatoRichiesta, RichiestaEvent> {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final TrackyCardGoBoxService trackyCardGoBoxService;

  private Set<StatoDispositivo> statiDispositiviAttivi = ImmutableSet.of(StatoDispositivo.ATTIVO_SPEDITO_A_FAI,
                                                                         StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE,
                                                                         StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);

  public GuardCheckIfPresentTrackyCardTrue(final TrackyCardGoBoxService _trackyCardGoBoxService) {
    trackyCardGoBoxService = _trackyCardGoBoxService;
  }

  @Override
  public boolean evaluate(StateContext<StatoRichiesta, RichiestaEvent> context) {
    boolean result = false;
    Message<RichiestaEvent> message = context.getMessage();
    if (message != null) {
      MessageHeaders headers = message.getHeaders();
      Object object = headers.get("object");
      if (object instanceof Richiesta) {
        Richiesta richiesta = (Richiesta) object;
        ClienteFai clienteFai = null;
        Contratto contratto = richiesta.getContratto();
        if (contratto != null) {
          clienteFai = contratto.getClienteFai();
        }
        String targa = richiesta.getAssociazione();
        String nazione = richiesta.getCountry();
        String codiceCliente = (clienteFai != null) ? clienteFai.getCodiceCliente() : null;
        Optional<Dispositivo> optDevice = trackyCardGoBoxService.findActiveDeviceByTypeAndLicensePlateAndCountry(TipoDispositivoEnum.TRACKYCARD,
                                                                                                                 statiDispositiviAttivi,
                                                                                                                 targa, nazione,
                                                                                                                 codiceCliente);

        if (optDevice.isPresent()) {
          result = true;
        }
      }
    }

    log.info("If TrackyCard present: {}", result);
    return result;
  }

}
