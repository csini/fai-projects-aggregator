package it.fai.ms.efservice.asyncjob;

public enum StateChangeStatusFsm {
  PENDING, ERROR, SUCCESS;
}
