package it.fai.ms.efservice.service.jms.dml;

import it.fai.ms.common.dml.AbstractDmlSender;
import it.fai.ms.common.dml.efservice.dto.AssociazioneDvDMLDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.service.jms.mapper.AssociazioneDvDmlSenderMapper;

public class AssociazioneDvDmlSenderUtil extends AbstractDmlSender<AssociazioneDvDMLDTO, AssociazioneDV> {

  static final JmsTopicNames TOPIC_SAVE   = JmsTopicNames.DML_ASSOCIAZIONE_DV_SAVE;
  static final JmsTopicNames TOPIC_DELETE = JmsTopicNames.DML_ASSOCIAZIONE_DV_DELETE;

  static final AssociazioneDvDmlSenderMapper mapper = new AssociazioneDvDmlSenderMapper();

  public AssociazioneDvDmlSenderUtil(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  protected JmsTopicNames getTopicSave() {
    return TOPIC_SAVE;
  }

  @Override
  protected JmsTopicNames getTopicDelete() {
    return TOPIC_DELETE;
  }

  @Override
  protected AssociazioneDvDMLDTO getSavingDmlDto(AssociazioneDV assDv) {
    AssociazioneDvDMLDTO dml = mapper.toDMLDTO(assDv);
    return dml;
  }

  @Override
  protected AssociazioneDvDMLDTO getDeletingDmlDto(String dmlUniqueIdentifier) {
    AssociazioneDvDMLDTO dto = mapper.toEmptyDMLDTO(dmlUniqueIdentifier);
    if (dto.getDmlUniqueIdentifier() == null || dto.getDmlUniqueIdentifier()
                                                   .isEmpty())
      throw new IllegalArgumentException("DmlUniqueIdentifier cannot be null");
    return dto;
  }

}
