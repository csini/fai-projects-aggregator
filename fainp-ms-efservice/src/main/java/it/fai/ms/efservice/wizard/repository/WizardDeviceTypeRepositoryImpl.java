package it.fai.ms.efservice.wizard.repository;

import static java.util.stream.Collectors.toCollection;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;

@Service
public class WizardDeviceTypeRepositoryImpl implements WizardDeviceTypeRepository {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final TipoDispositivoRepository deviceTypeRepository;

  public WizardDeviceTypeRepositoryImpl(final TipoDispositivoRepository _deviceTypeRepository) {
    deviceTypeRepository = _deviceTypeRepository;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDeviceType> findAll() {
    final long time1 = System.currentTimeMillis();
    final Set<WizardDeviceType> wizardDeviceTypes = deviceTypeRepository.findAll()
                                                                        .stream()
                                                                        .map(deviceType -> mapToWizardDeviceType(deviceType))
                                                                        .collect(toCollection(TreeSet::new));
    final long time2 = System.currentTimeMillis();
    _log.debug("Found all : {} [ms {}]", wizardDeviceTypes, time2 - time1);
    return wizardDeviceTypes;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDeviceTypeId> findAllIds() {
    final Set<WizardDeviceTypeId> wizardDeviceTypeIds = deviceTypeRepository.findNotHiddenDeviceType()
                                                                            .stream()
                                                                            .map(deviceType -> mapToWizardDeviceType(deviceType))
                                                                            .collect(toCollection(TreeSet::new))
                                                                            .stream()
                                                                            .filter(wdt -> !wdt.isHidden())
                                                                            .sequential()
                                                                            .map(deviceType -> deviceType.getId())
                                                                            .collect(toCollection(LinkedHashSet::new));
    _log.debug("Found all ids : {}", wizardDeviceTypeIds);
    return wizardDeviceTypeIds;
  }

  @Override
  @Transactional(readOnly = true)
  public Optional<WizardDeviceType> findByDeviceTypeId(final WizardDeviceTypeId _wizardDeviceTypeId) {
    Optional<WizardDeviceType> wizardDeviceType = null;
    wizardDeviceType = Optional.ofNullable(deviceTypeRepository.findOneByNome(TipoDispositivoEnum.valueOf(_wizardDeviceTypeId.getId())))
                               .map(deviceType -> mapToWizardDeviceType(deviceType));
    if (wizardDeviceType.isPresent()) {
      _log.debug("Found by {} : {}", _wizardDeviceTypeId, wizardDeviceType);
    } else {
      _log.debug("No WizardDeviceType for {}", _wizardDeviceTypeId);
    }
    return wizardDeviceType;
  }

  @Override
  @Transactional(readOnly = true)
  public Set<WizardDeviceType> findByServiceTypeId(final WizardServiceTypeId _serviceTypeId) {
    final Set<WizardDeviceType> wizardDeviceTypes = deviceTypeRepository.findAll()
                                                                        .stream()
                                                                        .filter(deviceType -> deviceType.getTipoServizios()
                                                                                                        .stream()
                                                                                                        .anyMatch(serviceType -> serviceType.getNome()
                                                                                                                                            .equalsIgnoreCase(_serviceTypeId.getId())))
                                                                        .map(deviceType -> mapToWizardDeviceType(deviceType))
                                                                        .collect(toCollection(TreeSet::new));
    _log.debug("Found by {} : {}", _serviceTypeId, wizardDeviceTypes);
    return wizardDeviceTypes;
  }

  private WizardDeviceType mapToWizardDeviceType(final TipoDispositivo _deviceType) {

    int maxNumeberService = _deviceType.isMultiservizio() ? (_deviceType.getTipoServizios() == null ? 0
                                                                                                    : _deviceType.getTipoServizios()
                                                                                                                 .size())
                                                          : 1;

    WizardDeviceType wizardDeviceType = new WizardDeviceType(new WizardDeviceTypeId(_deviceType.getNome()
                                                                                               .name(),
                                                                                    maxNumeberService));
    if (wizardDeviceType.getId()
                        .getId()
                        .equals("TELEPASS_EUROPEO")) {
      wizardDeviceType.setOrder(1);
    } else {
      wizardDeviceType.setOrder(6);
    }
    wizardDeviceType.setMultiService(_deviceType.isMultiservizio());

    wizardDeviceType.setHidden(_deviceType.isHidden());
    wizardDeviceType.setIsToBeSent(getIsToBeSent(_deviceType));
    return wizardDeviceType;
  }

  private Boolean getIsToBeSent(final TipoDispositivo _deviceType) {
    Boolean isToBeSent = null;
    if (!isTelepassItaliano(_deviceType)) {
      isToBeSent = true;
      if (isToBeNotSent(_deviceType)) {
        isToBeSent = false;
      }
    }
    return isToBeSent;
  }

  private boolean isToBeNotSent(TipoDispositivo _deviceType) {
    ModalitaSpedizione modalitaSpedizione = _deviceType.getModalitaSpedizione();
    _log.debug("Modalità spedizione to device {} is {}", _deviceType.getNome(), modalitaSpedizione);
    return ModalitaSpedizione.PUNTO_GO.equals(modalitaSpedizione);
  }

  private boolean isTelepassItaliano(final TipoDispositivo deviceType) {
    return TipoDispositivoEnum.TELEPASS_ITALIANO.equals(deviceType.getNome());
  }

}
