package it.fai.ms.efservice.service.fsm.bean.libert;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;
import it.fai.ms.efservice.service.fsm.type.libert.FsmInoltroLiberT;

@WithStateMachine(id = FsmInoltroLiberT.FSM_INOLTRO_LIBER_T)
public class FsmInoltroLiberTTransitionBean extends AbstractFsmRichiestaTransition {
}
