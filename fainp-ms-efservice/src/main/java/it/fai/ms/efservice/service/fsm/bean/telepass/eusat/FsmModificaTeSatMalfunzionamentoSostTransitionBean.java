package it.fai.ms.efservice.service.fsm.bean.telepass.eusat;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaTeSatMalfunzionamentoSost")
public class FsmModificaTeSatMalfunzionamentoSostTransitionBean extends AbstractFsmRichiestaTransition {
}