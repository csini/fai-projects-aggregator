package it.fai.ms.efservice.domain.enumeration;

/**
 * The ModalitaSpedizione enumeration.
 */
public enum ModalitaSpedizione {
  A_FAI, A_CLIENTE, A_FAI_O_CLIENTE, DA_DEPOSITO_FAI, PUNTO_GO
}
