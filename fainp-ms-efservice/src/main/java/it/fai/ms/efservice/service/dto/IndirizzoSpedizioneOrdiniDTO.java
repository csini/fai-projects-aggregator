package it.fai.ms.efservice.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the IndirizzoSpedizioneOrdini entity.
 */
public class IndirizzoSpedizioneOrdiniDTO implements Serializable {

    private Long id;

    private String via;

    private String citta;

    private String cap;

    private String provincia;

    private String paese;

    private Boolean fai;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPaese() {
        return paese;
    }

    public void setPaese(String paese) {
        this.paese = paese;
    }

    public Boolean isFai() {
        return fai;
    }

    public void setFai(Boolean fai) {
        this.fai = fai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = (IndirizzoSpedizioneOrdiniDTO) o;
        if(indirizzoSpedizioneOrdiniDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), indirizzoSpedizioneOrdiniDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IndirizzoSpedizioneOrdiniDTO{" +
            "id=" + getId() +
            ", via='" + getVia() + "'" +
            ", citta='" + getCitta() + "'" +
            ", cap='" + getCap() + "'" +
            ", provincia='" + getProvincia() + "'" +
            ", paese='" + getPaese() + "'" +
            ", fai='" + isFai() + "'" +
            "}";
    }
}
