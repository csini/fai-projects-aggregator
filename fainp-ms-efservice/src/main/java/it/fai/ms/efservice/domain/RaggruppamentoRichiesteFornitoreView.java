package it.fai.ms.efservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "view_group_richieste_ordini")
public class RaggruppamentoRichiesteFornitoreView {

  @Id
  private Long id;

  @Column(name = "nome_file_ricevuto")
  private String nomeFileRicevuto;

  @Column(name = "data_generazione_file")
  private Instant dataGenerazioneFile;

  @Column(name = "data_ultima_acquisizione")
  private Instant dataUltimaAcquisizione;

  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "numero_dispositivi_ordinati")
  private Long numeroDispositiviOrdinati;

  @Column(name = "numero_dispositivi_acquisiti")
  private Long numeroDispositiviAcquisiti;

  @Column(name = "stato_raggruppamento")
  private String stato;

  @Column(name = "produttore")
  private String produttore;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomeFileRicevuto() {
    return nomeFileRicevuto;
  }

  public void setNomeFileRicevuto(String nomeFileRicevuto) {
    this.nomeFileRicevuto = nomeFileRicevuto;
  }

  public Instant getDataGenerazioneFile() {
    return dataGenerazioneFile;
  }

  public void setDataGenerazioneFile(Instant dataGenerazioneFile) {
    this.dataGenerazioneFile = dataGenerazioneFile;
  }

  public Instant getDataUltimaAcquisizione() {
    return dataUltimaAcquisizione;
  }

  public void setDataUltimaAcquisizione(Instant dataUltimaAcquisizione) {
    this.dataUltimaAcquisizione = dataUltimaAcquisizione;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Long getNumeroDispositiviOrdinati() {
    return numeroDispositiviOrdinati;
  }

  public void setNumeroDispositiviOrdinati(Long numeroDispositiviOrdinati) {
    this.numeroDispositiviOrdinati = numeroDispositiviOrdinati;
  }

  public Long getNumeroDispositiviAcquisiti() {
    return numeroDispositiviAcquisiti;
  }

  public void setNumeroDispositiviAcquisiti(Long numeroDispositiviAcquisiti) {
    this.numeroDispositiviAcquisiti = numeroDispositiviAcquisiti;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getProduttore() {
    return produttore;
  }

  public void setProduttore(String produttore) {
    this.produttore = produttore;
  }

  @Override
  public String toString() {
    return "RaggruppamentoRichiesteFornitoreView{" +
      "id=" + id +
      ", nomeFileRicevuto='" + nomeFileRicevuto + '\'' +
      ", dataGenerazioneFile=" + dataGenerazioneFile +
      ", dataUltimaAcquisizione=" + dataUltimaAcquisizione +
      ", tipoDispositivo=" + tipoDispositivo +
      ", numeroDispositiviOrdinati=" + numeroDispositiviOrdinati +
      ", numeroDispositiviAcquisiti=" + numeroDispositiviAcquisiti +
      ", stato='" + stato + '\'' +
      ", produttore='" + produttore + '\'' +
      '}';
  }
}
