package it.fai.ms.efservice.domain.enumeration;

public enum FsmManageDeviceSent {
  TRANSITO, FAI, CLIENTE;
}
