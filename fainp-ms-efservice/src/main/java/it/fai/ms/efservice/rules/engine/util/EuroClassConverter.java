package it.fai.ms.efservice.rules.engine.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class EuroClassConverter {

  private static final Logger _log = LoggerFactory.getLogger(EuroClassConverter.class.getSimpleName());

  public enum EuroEnum {
    EURO0, EURO1, EURO2, EURO3, EURO4, EURO5, EURO6, EURO6_1A, EURO6_1B, EURO6_2C, EURO6D;
  }

  public static String convertEuroClassToMappingTelepass(final String _euroClass) {

    if (_euroClass == null || _euroClass.trim()
                                        .isEmpty()) {
      _log.warn("EuroClass parameter to convert is null or empty : fallback to 00", _euroClass);
      return "00";
    }

    try {

      final EuroEnum euroClassEnum = EuroEnum.valueOf(_euroClass);
      if (euroClassEnum == null) {
        _log.warn("EuroClassEnum {} not managed in converter : fallback to 00", euroClassEnum);
        return "00";
      }

      switch (euroClassEnum) {
      case EURO0:
        return "00";
      case EURO1:
        return "01";
      case EURO2:
        return "02";
      case EURO3:
        return "03";
      case EURO4:
        return "04";
      case EURO5:
        return "05";
      case EURO6:
        return "06";
      case EURO6_1A:
        return "06_1A";
      case EURO6_1B:
        return "06_1B";
      case EURO6_2C:
        return "06_2C";
      case EURO6D:
        return "06D";
      default:
        _log.warn("EuroClassEnum {} not managed in converter : fallback to 00", euroClassEnum);
        return "00";
      }

    } catch (@SuppressWarnings("unused") final IllegalArgumentException _e) {
      _log.warn("EuroClass parameter not mapped in EuroEnum : fallback to 00", _euroClass);
      return "00";
    }

  }

  public static int convertEuroClassToInteger(final String _euroClass) {

    if (_euroClass == null || _euroClass.trim()
                                        .isEmpty()) {
      _log.warn("EuroClass parameter to convert is null or empty : fallback to 0", _euroClass);
      return 0;
    }

    try {

      final EuroEnum euroClassEnum = EuroEnum.valueOf(_euroClass);
      if (euroClassEnum == null) {
        throw new RuntimeException("EuroClassEnum is " + euroClassEnum);
      }

      switch (euroClassEnum) {
      case EURO0:
        return 0;
      case EURO1:
        return 1;
      case EURO2:
        return 2;
      case EURO3:
        return 3;
      case EURO4:
        return 4;
      case EURO5:
        return 5;
      case EURO6:
        return 6;
      case EURO6_1A:
        return 6;
      case EURO6_1B:
        return 6;
      case EURO6_2C:
        return 6;
      case EURO6D:
        return 6;
      default:
        _log.warn("EuroClassEnum {} not managed in converter : fallback to 0", euroClassEnum);
        return 0;
      }

    } catch (@SuppressWarnings("unused") final IllegalArgumentException _e) {
      _log.warn("EuroClass parameter not mapped in EuroEnum : fallback to 0", _euroClass);
      return 0;
    }

  }

}
