package it.fai.ms.efservice.service.jms.listener.orderrequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.efservice.service.RequestNotificationService;
import it.fai.ms.efservice.service.jms.consumer.DeviceOrderedConsumer;

@Service 
public class JmsListenerDeviceOrdered
  extends JmsObjectMessageListenerTemplate<DeviceEventMessage>
  implements JmsQueueListener {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceOrderedConsumer consumer;
  
  private final RequestNotificationService requestNotificationService;

  @Autowired
  public JmsListenerDeviceOrdered(DeviceOrderedConsumer _consumer, RequestNotificationService _requestNotificationService){
    consumer = _consumer;
    this.requestNotificationService = _requestNotificationService;
  }

  @Override
  protected void consumeMessage(DeviceEventMessage eventMessage) {
    _log.info("Received jms message {}", eventMessage);
    try {
    consumer.consume(eventMessage);
   _log.debug("Message processed. Send notification");
   requestNotificationService.sendNotification(eventMessage);
    } catch (Exception e) {
      _log.error("Exception on consume message DeviceOrdered", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.DEVICE_ORDERED;
  }
}
