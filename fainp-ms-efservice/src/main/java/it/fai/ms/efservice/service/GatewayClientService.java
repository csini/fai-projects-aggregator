package it.fai.ms.efservice.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import feign.FeignException;
import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.efservice.client.FaiGatewayClient;
import it.fai.ms.efservice.client.dto.UserDTO;

@Service
@Transactional(noRollbackFor = FeignException.class)
public class GatewayClientService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final FaiGatewayClient gatewayClient;

  private String securityToken;

  public GatewayClientService(final FaiGatewayClient _gatewayClient, @Value("${efservice.authorizationHeader}") String _securityToken) {
    gatewayClient = _gatewayClient;
    securityToken = _securityToken;
  }

  public UserDTO getUserByCodiceCliente(String codiceCliente) {
    UserDTO user = null;
    if (StringUtils.isBlank(codiceCliente)) {
      return user;
    }
    String login = codiceCliente + "_" + codiceCliente;
    try {
      user = gatewayClient.getUser(securityToken, login);
    }catch (FeignException e) {
      log.error("Exception on call gateway to retrieve user: {}", codiceCliente);
    } 
    log.info("Found user by codice cliente {} : {}", codiceCliente, user);
    return user;
  }

  public AziendaDTO getAziendaByCodice(String codiceAzienda) {
    log.debug("Find azienda by codice azienda : {}", codiceAzienda);
    return StringUtils.isBlank(codiceAzienda) ? null : gatewayClient.getAziendaByCodice(securityToken, codiceAzienda);
  }

}
