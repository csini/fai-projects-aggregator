package it.fai.ms.efservice.service.impl;

import static java.util.stream.Collectors.toSet;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.LongStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.listener.events.DispositivoDepositoInEvent;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.service.DispositivoOrdineFornitoreService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreBySerialeDTO.OrdineFornitoreDispositivo;

/**
 * Service Implementation for managing DispositivoOrdineFornitore.
 */
@Service
@Transactional
public class DispositivoOrdineFornitoreServiceImpl implements DispositivoOrdineFornitoreService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DispositivoServiceExt              dispositivoServiceExt;
  private final StatoDispositivoServizioRepository sdsRepository;
  private final TipoDispositivoServiceExt          tipoDispositivoServiceExt;
  private final ApplicationEventPublisher          applicationEventPublisher;

  public DispositivoOrdineFornitoreServiceImpl(final DispositivoServiceExt _dispositivoServiceExt,
                                               final StatoDispositivoServizioRepository _sdsRepository,
                                               final TipoDispositivoServiceExt _tipoDispositivoServiceExt,
                                               ApplicationEventPublisher applicationEventPublisher) {
    dispositivoServiceExt          = _dispositivoServiceExt;
    sdsRepository                  = _sdsRepository;
    tipoDispositivoServiceExt      = _tipoDispositivoServiceExt;
    this.applicationEventPublisher = applicationEventPublisher;
  }

  @Override
  public Set<Dispositivo> createDispositivoByRange(String tipoDispositivo, String pan, String rangeStart, String rangeStop,
                                                   Set<String> dispositiviFounded) {
    TipoDispositivo   deviceType    = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.valueOf(tipoDispositivo));
    Set<TipoServizio> tipoServizios = deviceType.getTipoServizios();

    long             start                = Long.parseLong(rangeStart);
    long             stop                 = Long.parseLong(rangeStop);
    LongStream       streamRange          = LongStream.rangeClosed(start, stop);
    Set<Dispositivo> dispositiviGenerated = streamRange.mapToObj(v -> {
                                            Optional<Dispositivo> device = null;
                                            try {
                                              String           seriale    = String.valueOf(v);
                                              Optional<String> optSeriale = dispositiviFounded.stream()
                                                .filter(s -> s.equals(seriale))
                                                .findFirst();
                                              if (!optSeriale.isPresent()) {
                                                Dispositivo deviceCreated = createDispositivoInDeposito(deviceType, tipoServizios, seriale,
                                                                                                        pan, null, null);
                                                device = Optional.of(deviceCreated);
                                              } else {
                                                device = Optional.empty();
                                              }
                                            } catch (Exception e) {
                                              log.error("Exception: ", e);
                                              throw new RuntimeException(e);
                                            }
                                            return device;
                                          })
      .filter(opt -> opt.isPresent())
      .map(opt -> opt.get())
      .collect(toSet());

    log.info("Generated dispositivi: {}", dispositiviGenerated != null ? dispositiviGenerated.size() : 0);
    return dispositiviGenerated;
  }

  @Override
  public Dispositivo createDispositivoBySeriale(String tipoDispositivo, String seriale, String pan) throws Exception {
    TipoDispositivo   deviceType    = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.valueOf(tipoDispositivo));
    Set<TipoServizio> tipoServizios = deviceType.getTipoServizios();
    return createDispositivoInDeposito(deviceType, tipoServizios, seriale, pan, null, null);
  }

  @Override
  public Set<Dispositivo> createDispositiviByList(String tipoDispositivo, Collection<OrdineFornitoreDispositivo> rowList, Set<String> serialiToSkip) {
    TipoDispositivo   deviceType     = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.valueOf(tipoDispositivo));
    Set<TipoServizio> tipoServizios  = deviceType.getTipoServizios();
    Set<Dispositivo>  devicesCreated = rowList.stream()
      .filter(line -> !serialiToSkip.contains(line.getSeriale()))
      .map(line -> {
        String seriale    = line.getSeriale();
        String pan        = line.getPan();
        String tipoHardware = line.getTipoHardware();
        ZonedDateTime dataScadenza = line.getDataScadenza();
        log.info("Seriale: {} - PAN: 0{}", seriale, pan);
        Optional<Dispositivo> device;
        try {
          Dispositivo deviceCreated = createDispositivoInDeposito(deviceType, tipoServizios, seriale, pan, tipoHardware, dataScadenza);
          device = Optional.of(deviceCreated);
        } catch (Exception e) {
          log.error("Exception on creation Device", e);
          throw new RuntimeException(e);
        }
        return device;
      })
      .filter(opt -> opt.isPresent())
      .map(opt -> opt.get())
      .collect(toSet());

    log.info("Created {} device", devicesCreated != null ? devicesCreated.size() : 0);
    return devicesCreated;
  }

  @Override
  public ZonedDateTime parseDate(String string) {
    ZonedDateTime date = null;
    try {
      if (StringUtils.isNotBlank(string)) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        date = LocalDateTime.parse(string.trim(), df)
          .atZone(ZoneId.systemDefault());
      }
    } catch (Exception e) {
      try {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        date = LocalDate.parse(string.trim(), df)
          .atStartOfDay(ZoneId.systemDefault());
      } catch (Exception e1) {
        log.error("error pasing date \"{}\"", string);
      }
    }
    return date;
  }

  private Dispositivo createDispositivoInDeposito(TipoDispositivo deviceType, Set<TipoServizio> servizi, String seriale, String pan,
                                                  String tipoHardware, ZonedDateTime dataScadenza) throws Exception {
    Dispositivo dispositivoCreated = dispositivoServiceExt.createDispositivoDefault(deviceType);
    dispositivoCreated.setSeriale(seriale);
    dispositivoCreated.setStato(StatoDispositivo.IN_DEPOSITO);
    dispositivoCreated.setTipoHardware(tipoHardware);
    dispositivoCreated.setDataScadenza(dataScadenza);

    Set<StatoDispositivoServizio> createStatoDispositivoServizio = createStatoDispositivoServizio(dispositivoCreated, servizi, pan);
    dispositivoServiceExt.save(dispositivoCreated);
    sdsRepository.save(createStatoDispositivoServizio);

    applicationEventPublisher.publishEvent(new DispositivoDepositoInEvent(this, dispositivoCreated.getTipoDispositivo()));
    log.info("Created Dispositivo: {}", dispositivoCreated);
    return dispositivoCreated;
  }

  private Set<StatoDispositivoServizio> createStatoDispositivoServizio(Dispositivo createDispositivoDefault, Set<TipoServizio> tipoServizios, String pan) {
    Set<StatoDispositivoServizio> sdss = tipoServizios.stream()
      .map(s -> {
        StatoDispositivoServizio sds = new StatoDispositivoServizio().stato(StatoDS.ATTIVO)
          .dataScadenza(createDispositivoDefault.getDataScadenza() != null ? createDispositivoDefault.getDataScadenza().toInstant() : null)
          .dispositivo(createDispositivoDefault)
          .tipoServizio(s);

        sds.setPan(pan); // FIXME manage by tipoDispositivo

        return sds;
      })
      .collect(toSet());

    if (sdss.size() != tipoServizios.size()) {
      throw new RuntimeException("StatoDispositivoServizio created are minus to tipi servizio");
    }
    return sdss;
  }

  @Override
  public Optional<Dispositivo> findDeviceByTipoAndSeriale(TipoDispositivoEnum tipoDispositivo,String seriale) {
    return dispositivoServiceExt.findByTipoDispositivoAndSeriale(tipoDispositivo, seriale);
  }

}
