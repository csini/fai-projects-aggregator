package it.fai.ms.efservice.service.dto;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public class FsmDTO implements Serializable {

  private static final long serialVersionUID = 5331791464419242981L;

  private String operazione;

  private Set<String> identificativi;

  private String nota;

  public String getOperazione() {
    return operazione;
  }
  
  public FsmDTO operazione(String operazione) {
    this.operazione = operazione;
    return this;
  }

  public void setOperazione(String operazione) {
    this.operazione = operazione;
  }

  public Set<String> getIdentificativi() {
    return identificativi;
  }
  
  public String getStringIdentificativi() {
    StringBuilder sbIdentifiers = new StringBuilder();
    
    identificativi.stream()
    .map(identifierRichiesta -> {
      sbIdentifiers.append(identifierRichiesta + ", ");
      return identifierRichiesta;
    })
    .collect(toSet());
    
    return sbIdentifiers.toString();
  }
  
  public FsmDTO identificativi(Set<String> identificativi) {
    this.identificativi = identificativi;
    return this;
  }

  public void setIdentificativi(Set<String> identificativi) {
    this.identificativi = identificativi;
  }

  public String getNota() {
    return nota;
  }
  
  public FsmDTO nota(String nota) {
    this.nota = nota;
    return this;
  }

  public void setNota(String nota) {
    this.nota = nota;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Generic FSM DTO ");

    sb.append("[");
    sb.append(" operazione: ");
    sb.append(operazione);
    sb.append(", identificativi {");

    identificativi.stream()
                           .map(identifierRichiesta -> {
                             sb.append(identifierRichiesta + ", ");
                             return identifierRichiesta;
                           })
                           .collect(toSet());

    sb.append("}");
    sb.append(", nota: ");
    sb.append(nota);
    sb.append("]");

    return sb.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FsmDTO fsmDTO = (FsmDTO) o;
    return Objects.equals(operazione, fsmDTO.operazione) &&
      Objects.equals(identificativi, fsmDTO.identificativi) &&
      Objects.equals(nota, fsmDTO.nota);
  }

  @Override
  public int hashCode() {
    return Objects.hash(operazione, identificativi, nota);
  }
}
