package it.fai.ms.efservice.service.fsm.bean.fai;

import org.springframework.statemachine.annotation.WithStateMachine;

import it.fai.ms.efservice.service.fsm.bean.AbstractFsmRichiestaTransition;

@WithStateMachine(id = "fsmModificaGoBox")
public class FsmModificaGoBoxTransitionBean extends AbstractFsmRichiestaTransition {
}
