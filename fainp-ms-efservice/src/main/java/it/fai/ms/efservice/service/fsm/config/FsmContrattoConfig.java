package it.fai.ms.efservice.service.fsm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListener;
import it.fai.ms.efservice.service.fsm.util.StatoContrattoUtil;

@Configuration
@EnableStateMachineFactory(name = "contratto")
public class FsmContrattoConfig extends EnumStateMachineConfigurerAdapter<StatoContratto, ContrattoEvent> {

  private final static Logger log = LoggerFactory.getLogger(FsmContrattoConfig.class);

  public FsmContrattoConfig() {
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.config.AbstractStateMachineConfigurerAdapter#configure(org.springframework.
   * statemachine.config.builders.StateMachineConfigurationConfigurer)
   */
  @Override
  public void configure(StateMachineConfigurationConfigurer<StatoContratto, ContrattoEvent> config) throws Exception {

    super.configure(config);
    config.withConfiguration()
          .machineId("fsmContratto");
    config.withConfiguration()
          .listener(new StateMachineEventListener<StatoContratto, ContrattoEvent>());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StatoContratto, ContrattoEvent> states) throws Exception {
    states.withStates()
          .initial(StatoContratto.ATTIVO, ctx -> log.trace("Target: " + ctx.getTarget()
                                                                           .getIds()))
          .states(StatoContrattoUtil.getStates());
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StatoContratto, ContrattoEvent> transitions) throws Exception {

    transitions.withExternal()// MANUAL
               .source(StatoContratto.ATTIVO)
               .target(StatoContratto.SOSPESO)
               .event(ContrattoEvent.MS_SOSPENDI)
               .and()
               .withExternal()// MANUAL
               .source(StatoContratto.ATTIVO)
               .target(StatoContratto.REVOCATO)
               .event(ContrattoEvent.MU_REVOCA)
               .and()
               .withExternal()// MANUAL
               .source(StatoContratto.SOSPESO)
               .target(StatoContratto.REVOCATO)
               .event(ContrattoEvent.MU_REVOCA)
               .and()
               .withExternal()// MANUAL
               .source(StatoContratto.SOSPESO)
               .target(StatoContratto.ATTIVO)
               .event(ContrattoEvent.MS_RIATTIVA);
  }

}
