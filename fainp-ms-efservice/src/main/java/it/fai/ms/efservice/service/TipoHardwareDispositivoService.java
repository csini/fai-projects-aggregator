package it.fai.ms.efservice.service;

import java.util.List;
import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;

public interface TipoHardwareDispositivoService {

  TipoHardwareDispositivo findOne(Long id);

  List<TipoHardwareDispositivo> findAll();

  TipoHardwareDispositivo save(TipoHardwareDispositivo hardwareTypeDevice);

  List<TipoHardwareDispositivo> findActiveByDeviceType(TipoDispositivoEnum deviceTypeEnum);

  List<TipoHardwareDispositivo> findActiveByDeviceTypeId(Long idDeviceType);

  Optional<TipoHardwareDispositivo> findByTipoDispositivo_nomeAndHardwareType(TipoDispositivoEnum tipoDispositivo, String tipoHardware);

  void delete(Long id);

  void deleteAll();

  List<TipoHardwareDispositivo> findByDeviceType(TipoDispositivoEnum deviceType);

  List<TipoHardwareDispositivo> findByDeviceTypeId(Long tipoDispositivoId);

}
