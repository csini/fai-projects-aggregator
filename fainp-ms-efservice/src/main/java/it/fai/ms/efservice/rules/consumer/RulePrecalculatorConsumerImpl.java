package it.fai.ms.efservice.rules.consumer;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.rules.engine.RulePrecalculator;
import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleDieselParticulateFilter;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.repository.PreconditionValidationRepository;
import it.fai.ms.efservice.rules.engine.repository.RuleContextDeviceTypeRepository;
import it.fai.ms.efservice.rules.engine.repository.RuleContextServiceTypeRepository;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

@Service
public class RulePrecalculatorConsumerImpl implements RulePrecalculatorConsumer {

  private final Logger _log = LoggerFactory.getLogger(getClass().getName());

  private final static String PEDAGGI_AUSTRIA = "PEDAGGI_AUSTRIA";
  private final static String TRACKY_CARD     = "TRACKYCARD";

  private final RuleContextDeviceTypeRepository  ruleContextDeviceTypeRepository;
  private final RuleContextServiceTypeRepository ruleContextServiceTypeRepository;
  private final RulePrecalculator                rulePrecalculator;
  private final PreconditionValidationRepository preconditionValidationRepo;

  public RulePrecalculatorConsumerImpl(final RulePrecalculator _rulePrecalculator,
                                       final RuleContextServiceTypeRepository _ruleContextServiceTypeRepository,
                                       final RuleContextDeviceTypeRepository _ruleContextDeviceTypeRepository,
                                       final PreconditionValidationRepository _preconditionValidationRepo) {
    rulePrecalculator = _rulePrecalculator;
    ruleContextServiceTypeRepository = _ruleContextServiceTypeRepository;
    ruleContextDeviceTypeRepository = _ruleContextDeviceTypeRepository;
    preconditionValidationRepo = _preconditionValidationRepo;
  }

  @Override
  @Transactional
  public void consume(final VehicleMessage _newMessage) {
    _log.info("*** Consuming message: {}", _newMessage);
    process(_newMessage);
    _log.info("*** Message with uuid {} consumed", _newMessage.getUuid());
    preconditionValidationRepo.put(_newMessage.getUuid(), _newMessage.getDataUltimaModifica());
  }

  private Set<RuleEngineDeviceType> allDeviceTypes() {
    return ruleContextDeviceTypeRepository.findAll();
  }

  private Set<TipoServizio> allServiceTypes() {
    return ruleContextServiceTypeRepository.findAll();
  }

  private void executeRuleVehicleDevicesServices(final RuleEngineVehicle vehicle, RuleEngineDeviceType deviceType,
                                                 RuleEngineServiceType serviceType) {
    final RuleContext ruleContext = new RuleContext(serviceType, deviceType, vehicle);
    String deviceId = deviceType.getId()
                                .getId();
    Optional<RuleBucket> optRuleBucket = RuleBucket.getRuleBucket(deviceId);
    if (optRuleBucket.isPresent()) {
      RuleBucket ruleBucket = optRuleBucket.get();
      processRule(ruleContext, ruleBucket);
    } else {
      processRule(ruleContext, RuleBucket.ALL_DEVICE);
    }
  }

  public RuleOutcomeEntity executeRuleVehicleDevices(final RuleEngineVehicle vehicle, RuleEngineDeviceType deviceType,
                                         RuleEngineServiceType serviceType) {
    final RuleContext ruleContext = new RuleContext(serviceType, deviceType, vehicle);
    RuleBucket allDevice = RuleBucket.ALL_DEVICE;
    return processRule(ruleContext, allDevice);
  }

  private RuleOutcomeEntity processRule(RuleContext ruleContext, RuleBucket ruleId) {
    _log.debug("Process RuleContext: {} to RuleBucket: {}", ruleContext, ruleId);
    return rulePrecalculator.process(ruleContext, ruleId);
  }

  private RuleEngineVehicle mapToVehicleRuleContext(final VehicleMessage _newMessage) {
    final RuleEngineVehicle context = new RuleEngineVehicle(new RuleEngineVehicleId(_newMessage.getUuid()));
    context.setAxes(_newMessage.getAxes());
    context.setCategory(_newMessage.getCategory());
    context.setCountry(_newMessage.getCountry());
    context.setEuroClass(_newMessage.getEuroClass());
    context.setLicensePlate(_newMessage.getLicensePlate());
    context.setMake(_newMessage.getMake());
    context.setType(_newMessage.getType());
    context.getWeight()
           .setLoneVehicleGrossWeight(_newMessage.getLoneVehicleGrossWeight());
    context.getWeight()
           .setLoneVehicleTareWeight(_newMessage.getLoneVehicleTareWeight());
    context.getWeight()
           .setTrainsetGrossWeight(_newMessage.getTrainsetGrossWeight());
    context.setFuelType(_newMessage.getFuelType());
    context.setLength(_newMessage.getLength());
    if (_newMessage.getDieselParticulateFilterClass() != null) {
      Integer filterClass = parseStringToInteger(_newMessage.getDieselParticulateFilterClass());
      _log.info("Filter class parsed: {} from string: {}", filterClass, _newMessage.getDieselParticulateFilterClass());
      context.setDieselParticulateFilter(new RuleEngineVehicleDieselParticulateFilter(filterClass));
    }
    context.setHeight(_newMessage.getHeight());
    context.setWidth(_newMessage.getWidth());
    context.setPayload(_newMessage.getPayload());
    context.setNumberChassis(_newMessage.getNumberChassis());
    context.setColor(_newMessage.getColor());
    context.setModel(_newMessage.getModel());
    return context;
  }

  public Integer parseStringToInteger(String str) {
    Integer integer = null;
    if (StringUtils.isNotBlank(str)) {
      integer = Integer.parseInt(str);
    }

    return integer;
  }

  private void process(final VehicleMessage _newObjectMsg) {
    final RuleEngineVehicle ruleContextVehicle = mapToVehicleRuleContext(_newObjectMsg);

    RuleOutcomeEntity ruleOutcomeVehicle = processRuleVehicle(ruleContextVehicle);
    if(ruleOutcomeVehicle.isOutcome()) {
      processRuleVehicleDevices(ruleContextVehicle);
      processRuleVehicleDevicesServices(ruleContextVehicle);
    }
  }

  private void processRuleVehicleDevicesServices(final RuleEngineVehicle ruleContextVehicle) {
    List<String> allDeviceTypesFiltered = allDeviceTypes().stream()
                                                          .map(rd -> {
                                                            return rd.getId()
                                                                     .getId();
                                                          })
                                                          .collect(toList());
    allServiceTypes().stream()
                     .forEach(tipoServizio -> {
                       Set<TipoDispositivo> tipoDispositivos = tipoServizio.getTipoDispositivos();
                       String serviceName = tipoServizio.getNome();
                       _log.debug("Service {} managed rule....", serviceName);
                       RuleEngineServiceType serviceType = new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
                       Set<RuleEngineDeviceType> deviceTypes = mapToRuleEngineDeviceType(tipoDispositivos);
                       deviceTypes.stream()
                                  .forEach(deviceType -> {
                                    if (allDeviceTypesFiltered.contains(deviceType.getId()
                                                                                  .getId())) {
                                      executeRuleVehicleDevicesServices(ruleContextVehicle, deviceType, serviceType);
                                    }
                                  });
                     });

    addRuleDeviceTrackyCardToPedaggiAustriaService(ruleContextVehicle);

  }

  private void addRuleDeviceTrackyCardToPedaggiAustriaService(RuleEngineVehicle ruleContextVehicle) {
    executeRuleVehicleDevicesServices(ruleContextVehicle, buildRuleEngineDeviceType(TRACKY_CARD),
                                      buildRuleEngineServiceType(PEDAGGI_AUSTRIA));

  }

  private RuleEngineDeviceType buildRuleEngineDeviceType(String deviceName) {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId(deviceName));
  }

  private RuleEngineServiceType buildRuleEngineServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

  private void processRuleVehicleDevices(final RuleEngineVehicle _ruleContextVehicle) {
    allDeviceTypes().stream()
                    .forEach(deviceType -> {
                      executeRuleVehicleDevicesServices(_ruleContextVehicle, deviceType,
                                                        ruleContextServiceTypeRepository.newStarContextServiceType());
                    });
  }

  private RuleOutcomeEntity processRuleVehicle(final RuleEngineVehicle ruleContextVehicle) {
    return executeRuleVehicleDevices(ruleContextVehicle, ruleContextDeviceTypeRepository.newStarContextDeviceType(),
                              ruleContextServiceTypeRepository.newStarContextServiceType());
  }

  private Set<RuleEngineDeviceType> mapToRuleEngineDeviceType(Set<TipoDispositivo> devicesType) {
    return devicesType.stream()
                      .map(td -> new RuleEngineDeviceType(new RuleEngineDeviceTypeId(td.getNome()
                                                                                       .name())))
                      .collect(toSet());
  }

}
