package it.fai.ms.efservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RequestVehicleChangeService;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.impl.dto.RequestData;
import it.fai.ms.efservice.web.rest.errors.CustomException;
import it.fai.ms.efservice.web.rest.errors.Errno;

@Service
@Transactional
public class RequestVehicleChangeServiceImpl extends RequestUtilService implements RequestVehicleChangeService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();

  private final AssociazioneDVServiceExt associazioneDvService;

  private final DispositivoServiceExt deviceServiceExt;

  public RequestVehicleChangeServiceImpl(final AssociazioneDVServiceExt _associazioneDvService,
                                         final DispositivoServiceExt _deviceServiceExt, final ClienteFaiServiceExt _clienteFaiService) {
    super(_clienteFaiService);
    associazioneDvService = _associazioneDvService;
    deviceServiceExt = _deviceServiceExt;
    particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);
  }

  @Override
  public List<Richiesta> generateModificationRequestDeviceService(CarrelloModificaRichiestaDTO carrelloDTO) throws CustomException {
    log.info("Start creationModificationRequest by carrelloDTO: {}", carrelloDTO);

    List<Richiesta> requests = new ArrayList<>();
    try {
      log.info("{}: OLD vehicle [UUID: {}] to NEW Vehicle [UUID: {}]", carrelloDTO.getTipoOperazioneModifica(), carrelloDTO.getVeicolo(),
                carrelloDTO.getNewVeicolo());

      RequestData requestData = transformToData(carrelloDTO);

      validateRequestData(requestData);

      if (isOnlyParticularDeviceType(requestData.reqDispositivi)) {
        requestData.reqDispositivi.values()
                                  .stream()
                                  .map(d -> d.get())
                                  .forEach(assDV -> {
                                    changeVehicleUuidOnAssociazioneDV(requestData, assDV);
                                  });
      } else {

        OrdineCliente clientOrderForRequests = createDefaultOrdineCliente(carrelloDTO.getCodiceAzienda());

        requestData.reqDispositivi.values()
                                  .stream()
                                  .map(d -> d.get())
                                  .forEach(assDV -> {
                                    if (particularDeviceTypes.contains(assDV.getDispositivo()
                                                                            .getTipoDispositivo()
                                                                            .getNome())) {
                                      changeVehicleUuidOnAssociazioneDV(requestData, assDV);
                                    } else if (!StatoDispositivo.getBlueState()
                                                                .contains(assDV.getDispositivo()
                                                                               .getStato())) {
                                      Richiesta requestVehicleChange = createDefaultRequestVehicleChange(requestData, assDV);
                                      assignNewTargaNazione(requestVehicleChange, requestData.carrelloDto.getTarga(),
                                                            requestData.carrelloDto.getNazione());
                                      requests.add(requestVehicleChange);
                                    }
                                  });

        assignClientOrderOnRequests(requests, clientOrderForRequests);
      }
    } catch (CustomException e) {
      throw e;
    } catch (Exception e) {
      log.error("Exception on generation requests of Vehicle change...", e);
      throw CustomException.builder(HttpStatus.INTERNAL_SERVER_ERROR)
                           .add(Errno.EXCEPTION_AT_CREATE_ORDERS);
    }

    log.info("Requests generated: {}", requests.size());
    return requests;
  }

  private boolean isOnlyParticularDeviceType(Map<String, Optional<AssociazioneDV>> reqDispositivi) {
    Optional<TipoDispositivoEnum> optDeviceType = reqDispositivi.values()
                                                                .stream()
                                                                .filter(assDv -> !particularDeviceTypes.contains(assDv.get()
                                                                                                                      .getDispositivo()
                                                                                                                      .getTipoDispositivo()
                                                                                                                      .getNome()))
                                                                .map(assDv -> assDv.get()
                                                                                   .getDispositivo()
                                                                                   .getTipoDispositivo()
                                                                                   .getNome())
                                                                .findFirst();
    if (optDeviceType.isPresent()) {
      log.debug("Found device {} that not in {}, so do not managed only ParticularDeviceType", optDeviceType.get(), particularDeviceTypes);
    }
    return !optDeviceType.isPresent();
  }

  private Richiesta createDefaultRequestVehicleChange(RequestData requestData, AssociazioneDV assDV) {
    log.info("Create modification request by {} and {}", requestData.carrelloDto, assDV.getDispositivo());
    return createModificationRequest(requestData.carrelloDto, assDV.getDispositivo());
  }

  private void changeVehicleUuidOnAssociazioneDV(RequestData requestData, AssociazioneDV reqAssDV) {
    AssociazioneDV assDv = associazioneDvService.assignNewUuidVehicle(reqAssDV, requestData.getNewVeicolo());
    log.debug("New AssocizioneDV: {}", assDv);
  }

  private void validateRequestData(RequestData requestData) throws CustomException {
    // all device requested are valid
    if (requestData.reqDispositivi.values()
                                  .stream()
                                  .anyMatch(d -> !d.isPresent()))
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.DEVICE_NOTFOUND);
    // no requested device in pending state
    if (requestData.reqDispositivi.values()
                                  .stream()
                                  .anyMatch(d -> StatoDispositivo.getYellowState()
                                                                 .contains(d.get()
                                                                            .getDispositivo()
                                                                            .getStato())))
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.STATO_DISPOSITIVO_IS_YELLOW);
    // no active device with same type in target
    if (requestData.targetAssDvs.stream()
                                .map(a -> a.getDispositivo())
                                .filter(d -> !StatoDispositivo.getBlueState()
                                                              .contains(d.getStato()))
                                .anyMatch(targetD -> requestData.reqDispositivi.values()
                                                                               .stream()
                                                                               .anyMatch(reqD -> reqD.get()
                                                                                                     .getDispositivo()
                                                                                                     .getTipoDispositivo()
                                                                                                     .equals(targetD.getTipoDispositivo()))))
      throw CustomException.builder(HttpStatus.BAD_REQUEST)
                           .add(Errno.DUPLICATE_DEVICE);

  }

  protected RequestData transformToData(CarrelloModificaRichiestaDTO carrelloDTO) {
    RequestData requestData = new RequestData();
    requestData.carrelloDto = carrelloDTO;
    requestData.clienteFai = retrieveClienteFai(carrelloDTO.getCodiceAzienda());
    requestData.assDvs = associazioneDvService.findByUuidVeicoloAndCodiceCliente(carrelloDTO.getVeicolo(), carrelloDTO.getCodiceAzienda());

    List<String> reqDispositiviIds = carrelloDTO.getDispositivi();

    requestData.reqDispositivi = new HashMap<>();
    if (reqDispositiviIds != null) {
      reqDispositiviIds.forEach(dId -> {
        requestData.reqDispositivi.put(dId, requestData.assDvs.stream()
                                                              .filter(adv -> adv.getDispositivo() != null)
                                                              .filter(adv -> !StatoDispositivo.getBlueState()
                                                                                              .contains(adv.getDispositivo()
                                                                                                           .getStato()))
                                                              .filter(adv -> dId.equals(adv.getDispositivo()
                                                                                           .getIdentificativo()))
                                                              .findAny());
      });
    }

    requestData.targetAssDvs = associazioneDvService.findByUuidVeicoloAndCodiceCliente(carrelloDTO.getNewVeicolo(),
                                                                                       carrelloDTO.getCodiceAzienda());

    return requestData;
  }

  private void setDeviceTypeAndContractAndAddDeviceOnRequest(Richiesta richiesta, Dispositivo device) {
    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    richiesta.setTipoDispositivo(tipoDispositivo);
    richiesta.setContratto(device.getContratto());
    richiesta.addDispositivo(device);
  }

  private Richiesta createModificationRequest(CarrelloModificaRichiestaDTO carrelloDTO, Dispositivo device) {
    TipoRichiesta requestType = carrelloDTO.getTipoOperazioneModifica();
    TipoDispositivo tipoDispositivo = device.getTipoDispositivo();
    Richiesta richiesta = createDefaultRequestByTypeAndDeviceType(requestType, tipoDispositivo);

    richiesta.setUuidDocumento(carrelloDTO.getDocumento());

    StatoRichiesta statoRichiesta = getStatusRequestByDeviceType(device.getTipoDispositivo()
                                                                       .getNome());
    richiesta.setStato(statoRichiesta);

    setDeviceTypeAndContractAndAddDeviceOnRequest(richiesta, device);
    saveDevice(device);
    log.info("Modification request created: {}", richiesta);
    return richiesta;
  }

  private void saveDevice(Dispositivo device) {
    try {
      deviceServiceExt.save(device);
    } catch (RuntimeException e) {
      log.error("Failed to save Device: {}", device.getIdentificativo(), e);
      throw e;
    }
  }

}
