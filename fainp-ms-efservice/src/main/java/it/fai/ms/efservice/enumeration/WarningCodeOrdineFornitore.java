package it.fai.ms.efservice.enumeration;

public enum WarningCodeOrdineFornitore {

  SERIALE_NOT_VALID(100), SERIALE_ALREADY_EXISTS(101);

  private int codice;

  private WarningCodeOrdineFornitore(int codice) {
    this.codice = codice;
  }

  public int getCodice() {
    return codice;
  }
}
