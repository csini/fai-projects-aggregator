package it.fai.ms.efservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A RaggruppamentoRichiesteOrdineFornitore.
 */
@Entity
@Table(name = "group_richieste_ordini")
public class RaggruppamentoRichiesteOrdineFornitore implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "nome_file_ricevuto")
  private String nomeFileRicevuto;

  @Column(name = "data_generazione_file")
  private Instant dataGenerazioneFile;

  @Column(name = "data_ultima_acquisizione")
  private Instant dataUltimaAcquisizione;

  @OneToMany(mappedBy = "raggruppamentoRichiesteOrdineFornitore")
  @JsonIgnore
  private Set<Richiesta> richiestas = new HashSet<>();

  @ManyToOne
  private TipoDispositivo tipoDispositivo;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomeFileRicevuto() {
    return nomeFileRicevuto;
  }

  public RaggruppamentoRichiesteOrdineFornitore nomeFileRicevuto(String nomeFileRicevuto) {
    this.nomeFileRicevuto = nomeFileRicevuto;
    return this;
  }

  public void setNomeFileRicevuto(String nomeFileRicevuto) {
    this.nomeFileRicevuto = nomeFileRicevuto;
  }

  public Instant getDataGenerazioneFile() {
    return dataGenerazioneFile;
  }

  public RaggruppamentoRichiesteOrdineFornitore dataGenerazioneFile(Instant dataGenerazioneFile) {
    this.dataGenerazioneFile = dataGenerazioneFile;
    return this;
  }

  public void setDataGenerazioneFile(Instant dataGenerazioneFile) {
    this.dataGenerazioneFile = dataGenerazioneFile;
  }

  public Instant getDataUltimaAcquisizione() {
    return dataUltimaAcquisizione;
  }

  public RaggruppamentoRichiesteOrdineFornitore dataUltimaAcquisizione(Instant dataUltimaAcquisizione) {
    this.dataUltimaAcquisizione = dataUltimaAcquisizione;
    return this;
  }

  public void setDataUltimaAcquisizione(Instant dataUltimaAcquisizione) {
    this.dataUltimaAcquisizione = dataUltimaAcquisizione;
  }

  public Set<Richiesta> getRichiestas() {
    return richiestas;
  }

  public RaggruppamentoRichiesteOrdineFornitore richiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
    return this;
  }

  public RaggruppamentoRichiesteOrdineFornitore addRichiesta(Richiesta richiesta) {
    this.richiestas.add(richiesta);
    richiesta.setRaggruppamentoRichiesteOrdineFornitore(this);
    return this;
  }

  public RaggruppamentoRichiesteOrdineFornitore removeRichiesta(Richiesta richiesta) {
    this.richiestas.remove(richiesta);
    richiesta.setRaggruppamentoRichiesteOrdineFornitore(null);
    return this;
  }

  public void setRichiestas(Set<Richiesta> richiestas) {
    this.richiestas = richiestas;
  }

  public TipoDispositivo getTipoDispositivo() {
    return tipoDispositivo;
  }

  public RaggruppamentoRichiesteOrdineFornitore tipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(TipoDispositivo tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public Set<String> getIdentificativiRichieste() {
    if (this.richiestas != null) {
      return this.richiestas.stream()
                            .map(r -> r.getIdentificativo())
                            .collect(toSet());
    }
    return null;
  }

  public String getidentificativiRichiesteToString() {
    Set<String> identificativiRichieste = getIdentificativiRichieste();
    if (identificativiRichieste != null) {
      StringBuilder sb = new StringBuilder("[");
      for (String identificativo : identificativiRichieste) {
        sb.append(identificativo + ", ");
      }
      sb.append("]");
      return sb.toString();
    }
    return null;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = (RaggruppamentoRichiesteOrdineFornitore) o;
    if (raggruppamentoRichiesteOrdineFornitore.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), raggruppamentoRichiesteOrdineFornitore.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("RaggruppamentoRichiesteOrdineFornitore {");
    sb.append("id=" + getId());
    sb.append(", nomeFileRicevuto=" + getNomeFileRicevuto());
    sb.append(", dataGenerazioneFile=" + getDataGenerazioneFile());
    sb.append(", dataUltimaAcquizione=" + getDataUltimaAcquisizione());
    sb.append(", tipoDispositivo=" + (getTipoDispositivo() != null ? getTipoDispositivo().getNome() : null));
    sb.append(", richieste=" + getidentificativiRichiesteToString());
    sb.append("}");
    return sb.toString();
  }
}
