package it.fai.ms.efservice.service.fsm.config.telepass.eu;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoAttivo;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoRevocato;
import it.fai.ms.efservice.service.fsm.action.FsmActionDispositivoSospeso;
import it.fai.ms.efservice.service.fsm.action.FsmActionGeneric;
import it.fai.ms.efservice.service.fsm.config.FsmRichiestaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.guard.GuardInvioEvcnoTE;
import it.fai.ms.efservice.service.fsm.guard.GuardRescissioneTE;
import it.fai.ms.efservice.service.fsm.guard.GuardSospensioneTE;
import it.fai.ms.efservice.service.fsm.util.StatoRichiestaUtil;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
@EnableStateMachineFactory(name = FsmModificaTeRescRiattSospesoConfig.MOD_TE_RESC_RIATT_SOSPESO)
public class FsmModificaTeRescRiattSospesoConfig extends FsmRichiestaConfig {

  public static final String MOD_TE_RESC_RIATT_SOSPESO = "ModTeRescRiattSospeso";

  private final static Logger log = LoggerFactory.getLogger(FsmModificaTeRescRiattSospesoConfig.class);

  private final FsmSenderToQueue senderFsmService;

  public FsmModificaTeRescRiattSospesoConfig(final FsmSenderToQueue _senderFsmService) {
    senderFsmService = _senderFsmService;
  }

  @Override
  protected Logger getLogger() {
    return log;
  }

  @Override
  protected String getMachineId() {
    return FsmType.MOD_TE_RESC_RIATT_SOSPENSIONE.fsmName();
  }

  @Override
  protected StatoRichiesta getStatoRichiestaInitial() {
    return StatoRichiesta.ATTIVO_PER_MODIFICA_TE;
  }

  @Override
  protected Set<StatoRichiesta> getAvailableStates() {
    return StatoRichiestaUtil.getStateOfRescRiattSospensione();
  }

  @Override
  protected void configTransitions(StateMachineTransitionConfigurer<StatoRichiesta, RichiestaEvent> transitions) throws Exception {
    transitions.withExternal()// MANUAL
               .source(StatoRichiesta.ATTIVO_PER_MODIFICA_TE)
               .target(StatoRichiesta.ATTIVO)
               .event(RichiestaEvent.INITIAL)
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.IN_SOSPENSIONE)
               .guard(new GuardSospensioneTE())
               .action(new FsmActionDispositivoSospeso(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.ATTIVO)
               .target(StatoRichiesta.IN_RESCISSIONE)
               .guard(new GuardRescissioneTE())
               .action(new FsmActionDispositivoRevocato(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.IN_RESCISSIONE)
               .target(StatoRichiesta.CESSATO)
               .guard(new GuardInvioEvcnoTE(senderFsmService))
               .action(new FsmActionDispositivoRevocato(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.IN_SOSPENSIONE)
               .target(StatoRichiesta.SOSPESO)
               .guard(new GuardInvioEvcnoTE(senderFsmService))
//               .action(new FsmActionDispositivoSospeso(senderFsmService)) //FIXME-fsm commentato perchè mi sembrava un cambio stato di troppo...
               .action(new FsmActionGeneric())
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.SOSPESO)
               .target(StatoRichiesta.IN_RIATTIVAZIONE)
               .event(RichiestaEvent.MU_RICHIESTA_RIATTIVAZIONE)
               .action(new FsmActionDispositivoAttivo(senderFsmService))
               .and()
               .withExternal()// AUTOGUARD
               .source(StatoRichiesta.IN_RIATTIVAZIONE)
               .target(StatoRichiesta.RIATTIVATO)
               .guard(new GuardInvioEvcnoTE(senderFsmService))
               .action(new FsmActionDispositivoAttivo(senderFsmService));
  }

}
