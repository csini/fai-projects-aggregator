package it.fai.ms.efservice.service.jms.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.RichiestaNewDispositivoDTO;
import it.fai.ms.efservice.service.jms.consumer.NuovoDispositivoConsumer;

@Service
@Transactional
public class JmsListenerNuovoDispositivoService extends JmsObjectMessageListenerTemplate<RichiestaNewDispositivoDTO> implements JmsTopicListener {

  private final Logger                   log = LoggerFactory.getLogger(JmsListenerNuovoDispositivoService.class);
  private final NuovoDispositivoConsumer newDispositivoConsumer;

  public JmsListenerNuovoDispositivoService(NuovoDispositivoConsumer newDispositivoConsumer) throws Exception {
    this.newDispositivoConsumer = newDispositivoConsumer;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.REQUEST_NEW_DEVICE;
  }

  @Override
  protected void consumeMessage(RichiestaNewDispositivoDTO dto) {
    try {
      newDispositivoConsumer.createRichiestaDiSostituzione(dto);
    } catch (Exception e) {
      log.error("Error", e);
      throw new RuntimeException(e);
    }

  }

}