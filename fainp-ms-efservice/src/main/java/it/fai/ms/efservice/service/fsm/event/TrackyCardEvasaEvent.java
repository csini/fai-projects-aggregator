package it.fai.ms.efservice.service.fsm.event;

import java.io.Serializable;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;

public class TrackyCardEvasaEvent implements Serializable {

  private static final long serialVersionUID = 928586584698009240L;

  private Richiesta richiesta;

  private Dispositivo dispositivo;

  public TrackyCardEvasaEvent(Richiesta _richiesta) {
    richiesta = _richiesta;
  }

  public TrackyCardEvasaEvent(Richiesta _richiesta, Dispositivo _dispositivo) {
    richiesta = _richiesta;
    dispositivo = _dispositivo;
  }

  public Richiesta getRichiesta() {
    return richiesta;
  }

  public void setRichiesta(Richiesta richiesta) {
    this.richiesta = richiesta;
  }

  public Dispositivo getDispositivo() {
    return dispositivo;
  }

  public void setDispositivo(Dispositivo dispositivo) {
    this.dispositivo = dispositivo;
  }

  @Override
  public String toString() {
    return "TrackyCardEvasaEvent [richiesta=" + richiesta + ", dispositivo=" + dispositivo + "]";
  }

}
