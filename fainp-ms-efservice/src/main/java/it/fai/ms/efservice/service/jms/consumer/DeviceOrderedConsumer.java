package it.fai.ms.efservice.service.jms.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.telepass.DeviceDataDTO;
import it.fai.ms.common.jms.dto.telepass.DeviceEventMessage;
import it.fai.ms.common.jms.efservice.FileElaborationDetail;
import it.fai.ms.common.jms.efservice.message.device.DeviceOrderedMessage;
import it.fai.ms.common.jms.efservice.message.orderrequest.model.OrderRequestUuid;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.consumer.device.DeviceResponseOkConsumer;

@Service 
public class DeviceOrderedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DeviceResponseOkConsumer deviceResponseOkConsumer;

  public DeviceOrderedConsumer(final DeviceResponseOkConsumer _deviceResponseOkConsumer) {
    deviceResponseOkConsumer = _deviceResponseOkConsumer;
  }

  public void consume(DeviceOrderedMessage message) throws FsmExecuteCommandException {
    log.info("Manage message: {}", message);
    OrderRequestUuid orderRequestUuid = message.getOrderRequestUuid();
    String identificativoRichiesta = orderRequestUuid.getUuid();

    // Solo Trackycard, Viacard (ora gestito con DeviceEventMessage) e tessere frejus generano questo tipo di mesassggio
    // e non dovrebero mai arrivare messaggi che non contengono FileElaborationDetail
    deviceResponseOkConsumer.consumeDeviceMessageWithOtherInformations(identificativoRichiesta, message);
  }

  public void consume(DeviceEventMessage eventMessage) throws FsmExecuteCommandException {
    log.info("Manage message: {}", eventMessage);
    DeviceDataDTO deviceDataDTO = eventMessage.getDeviceDataDTO();
    String identificativoRichiesta = deviceDataDTO.getRequestId();

    deviceResponseOkConsumer.consumeDeviceMessageWithOtherInformations(identificativoRichiesta, eventMessage);
  }

}
