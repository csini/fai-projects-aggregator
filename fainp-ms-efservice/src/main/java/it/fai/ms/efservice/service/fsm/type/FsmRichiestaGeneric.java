package it.fai.ms.efservice.service.fsm.type;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

public abstract class FsmRichiestaGeneric extends AbstractFsmRichiesta {

  protected StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactory;
  protected FsmRichiestaCacheService                            cacheService;

  protected FsmType fsmType;

  protected TipoDispositivoEnum[] deviceTypes;

  protected abstract FsmType getOldFsm();

  protected abstract Logger getLogger();

  @Override
  public StateMachine<StatoRichiesta, RichiestaEvent> getStateMachine() {
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = stateMachineFactory.getStateMachine(fsmType.fsmName());
    getLogger().debug("Get state machine {}", stateMachine.getId());
    return stateMachine;
  }

  @Override
  public Richiesta executeCommandToChangeState(RichiestaEvent command, Richiesta richiesta, String nota) {
    long tStart = System.currentTimeMillis();
    Long richiestaId = richiesta.getId();
    String identificativo = richiesta.getIdentificativo();
    StatoRichiesta statoAttuale = richiesta.getStato();
    getLogger().debug("Switch state by command {} to Richiesta [ID: {} - Identificativo: {}] with actual state: {}", command, richiestaId,
                      identificativo, statoAttuale);

    if (!isOneOfType(richiesta, deviceTypes)) {
      getLogger().warn("Richiesta: {} not possible to handle in this FSM...", identificativo);
      return richiesta;
    }

    boolean availableCommand = isAvailableCommand(command, richiesta);
    if (availableCommand) {

      StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = getStateMachine();
      stateMachine.start();
      String fsmKey = String.valueOf(richiestaId);
      StateMachine<StatoRichiesta, RichiestaEvent> stateMachineFromStored = retrieveFsmByCache(fsmKey);
      if (stateMachineFromStored != null) {
        stateMachine = stateMachineFromStored;
      }

      if (command != null) {

        sendEventToFsm(stateMachine, command, richiesta, nota);
        getLogger().info("Send command: {} to Richiesta [ID: {} - Identificativo: {}]", command, richiestaId, identificativo);

        cacheService.persist(stateMachine, fsmKey);
        richiesta.setStato(stateMachine.getState().getId());
        richiesta = getUserAvailableCommandAndSetOnRichiesta(richiesta);
      } else {
        getLogger().warn("Enum command (RichiestaEvent) is {} ", command);
      }
    } else {
      // WarkAround per messaggi ripetuti (Caused by: java.lang.RuntimeException: Command RESPONSE_KO is not available because Richiesta [ID: 2574 - Identificativo: 86d02c8a-b0ec-4a0a-8600-7844789d1a0c] state is ORDINE_SOSPESO)
      if(RichiestaEvent.RESPONSE_KO.equals(command) && richiesta != null && StatoRichiesta.ORDINE_SOSPESO.equals(richiesta.getStato())) {
        getLogger().warn("Skip change status because command is {} and stato richiesta [{}] is {}", command, identificativo, richiesta.getStato());
      } else {
        String messageError = String.format("Command %s is not available because Richiesta [ID: %s - Identificativo: %s] state is %s", command, richiestaId,
                                          identificativo, statoAttuale);
        getLogger().error(messageError);
        throw new RuntimeException(messageError);
      }
    }

    getLogger().debug("=> FINISH switch state with command: {} to Richiesta [ID: {} - Identificativo: {}] in {} ms", command, richiestaId,
                      identificativo, (System.currentTimeMillis() - tStart));
    return richiesta;
  }

  private boolean isOneOfType(Richiesta richiesta, TipoDispositivoEnum... _deviceTypes) {
    for (TipoDispositivoEnum deviceType : _deviceTypes) {
      if (isEqualType(richiesta, deviceType))
        return true;
    }
    return false;
  }

  private StateMachine<StatoRichiesta, RichiestaEvent> retrieveFsmByCache(String fsmKey) {
    if (StringUtils.isNotBlank(fsmKey)) {
      String jsonContext = cacheService.getStateMachineContext(fsmKey);
      if (StringUtils.isNotBlank(jsonContext)) {
        String oldFsm = "FSM-RICHIESTA";
        if (getOldFsm() != null) {
          oldFsm = getOldFsm().fsmName();
        }
        if (jsonContext.contains(oldFsm)) {
          getLogger().info("Reset {} to {}", getOldFsm() != null ? getOldFsm().fsmName() : null, fsmType.fsmName());
          cacheService.resetIdMachineFromAnotherIdMachineByContext(jsonContext, fsmKey, fsmType.fsmName());
        }

        getLogger().debug("Restore state machine for FsmKey {}", fsmKey);
        return cacheService.restore(getStateMachine(), fsmKey);
      }
    }

    return null;
  }
}
