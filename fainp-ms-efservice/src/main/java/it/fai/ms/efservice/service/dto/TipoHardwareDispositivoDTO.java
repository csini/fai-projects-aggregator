package it.fai.ms.efservice.service.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;

/**
 * A DTO for the TipoHardwareDispositivo entity.
 */
public class TipoHardwareDispositivoDTO implements Serializable {

  private static final long serialVersionUID = -2052552965531350052L;

  private Long id;

  @NotNull
  private TipoDispositivoEnum tipoDispositivo;

  @NotNull
  private String tipoHardware;

  private boolean defaultTipoHardware;

  private String descrizione;

  private boolean attivo;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getTipoHardware() {
    return tipoHardware;
  }

  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }

  public boolean isDefaultTipoHardware() {
    return defaultTipoHardware;
  }

  public void setDefaultTipoHardware(boolean defaultTipoHardware) {
    this.defaultTipoHardware = defaultTipoHardware;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public boolean isAttivo() {
    return attivo;
  }

  public void setAttivo(boolean attivo) {
    this.attivo = attivo;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoHardwareDispositivoDTO [id=");
    builder.append(id);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", tipoHardware=");
    builder.append(tipoHardware);
    builder.append(", defaultTipoHardware=");
    builder.append(defaultTipoHardware);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", attivo=");
    builder.append(attivo);
    builder.append("]");
    return builder.toString();
  }

}
