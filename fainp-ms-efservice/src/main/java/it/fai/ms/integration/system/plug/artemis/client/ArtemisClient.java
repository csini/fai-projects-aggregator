package it.fai.ms.integration.system.plug.artemis.client;

import org.apache.activemq.artemis.jms.client.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.Destination;
import java.io.Serializable;

@Component("artemisAdaptee")
public class ArtemisClient {

  @Resource(name = "topicJmsTemplate")
  private JmsTemplate jmsTemplate;

  public void topicPublish(Destination destination, Serializable data){
    jmsTemplate.setDeliveryPersistent(true);
    jmsTemplate.convertAndSend(destination,data);
  }

  public Destination getTopicDestination(String topic){
    return new ActiveMQTopic(topic);
  }
}
