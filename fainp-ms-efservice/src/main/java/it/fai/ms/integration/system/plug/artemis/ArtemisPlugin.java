package it.fai.ms.integration.system.plug.artemis;

import it.fai.ms.integration.system.sock.core.factory.PluginFactory;
import it.fai.ms.integration.system.plug.annotations.Plugin;
import it.fai.ms.integration.system.plug.factory.AdapterFactory;

import javax.annotation.Nullable;

@Plugin(name="MessageSystemFactory")
public class ArtemisPlugin implements PluginFactory {

  Object adapter;
  Object adaptee;

  public ArtemisPlugin(Object adapter, Object adaptee) {
    this.adapter = adapter;
    this.adaptee = adaptee;
  }

  @Nullable
  @Override
  public <T> T getPlugin(Class<T> pluginClass) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
    return AdapterFactory.adapt(adaptee, pluginClass, adapter);
  }

  public Object getAdapter() {
    return adapter;
  }


  public Object getAdaptee() {
    return adaptee;
  }

}
