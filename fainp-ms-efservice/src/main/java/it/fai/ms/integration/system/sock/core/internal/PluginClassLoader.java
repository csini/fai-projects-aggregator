package it.fai.ms.integration.system.sock.core.internal;

/*-
 * #%L
 * plugface-core
 * %%
 * Copyright (C) 2017 Matteo Joliveau
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 * #L%
 */

import it.fai.ms.integration.system.plug.annotations.Plugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.util.Properties;

/**
 * Custom {@link URLClassLoader} that is used to load {@link Plugin} classes from Jar files.
 * <p>
 * It is used to give an empty set of permissions to plugins in SandboxSecurityPolicy#isPlugin(ProtectionDomain)
 * so that plugins run in a protected sandbox.
 * It also overrides the {@link URLClassLoader#getPermissions(CodeSource)} method so that
 * custom permissions specified in a <i>permissions.properties</i> file can be applied to specific plugins.
 * </p>
 */
public class PluginClassLoader extends CCLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PluginClassLoader.class);

    /**
     * The default directory separator for Unix-like operating systems
     */
    private static final char UNIX_SEPARATOR = '/';

    /**
     * The default directory separator for Windows operating systems
     */
    private static final char WINDOWS_SEPARATOR = '\\';

    /**
     * The properties containing custom permissions for plugins
     */
    private Properties permissionProperties;

    /**
     * Construct a {@link PluginClassLoader} from an single {@link URL}
     *
     * @param jarFileUrl an Jar file {@link URL}
     */
    public PluginClassLoader(URL jarFileUrl, ClassLoader parent) {
      //noinspection NamedArgsPositionMismatch
      super(new URL[]{jarFileUrl},parent);
    }

    /**
     * Construct a {@link PluginClassLoader} from an array of {@link URL}
     *
     * @param jarFileUrls the array of Jar file {@link URL}
     */
    public PluginClassLoader(URL[] jarFileUrls, ClassLoader parent) {
      super(jarFileUrls,parent);
    }


     /*
    @Override
    protected PermissionCollection getPermissions(CodeSource codesource) {
        return super.getPermissions(codesource);

        Permissions permissions = new Permissions();
        if (permissionProperties != null) {
            String pluginFileName = retrieveFileNameFromCodeSource(codesource);

            final String[] properties = new String[]{
                    "permissions." + pluginFileName + ".files",
                    "permissions." + pluginFileName + ".network",
                    "permissions." + pluginFileName + ".security",
                    "permissions." + pluginFileName + ".runtime"
            };

            String requiredPermissions;

            if (permissionProperties.isEmpty()) {
                return permissions;
            }

            for (String key : properties) {
                if (permissionProperties.containsKey(key)) {
                    requiredPermissions = permissionProperties.getProperty(key);
                    String[] slices = requiredPermissions.split(", ");

                    if (key.equals(properties[0])) {
                        for (String s : slices) {
                            String[] params = s.split(" ");
                            permissions.add(new FilePermission(params[1], params[0]));
                        }
                    } else if (key.equals(properties[1])) {
                        for (String s : slices) {
                            String[] params = s.split(" ");
                            permissions.add(new NetPermission(params[0]));
                        }
                    } else if (key.equals(properties[2])) {
                        for (String s : slices) {
                            String[] params = s.split(" ");
                            permissions.add(new SecurityPermission(params[0]));
                        }
                    } else if (key.equals(properties[3])) {
                        for (String s : slices) {
                            String[] params = s.split(" ");
                            permissions.add(new RuntimePermission(params[0]));
                        }
                    } else {
                        LOGGER.warn("{} is not a valid permission, skipping...");
                    }
                }
            }
        }
        return permissions;

    }*/


    public Properties getPermissionProperties() {
        return permissionProperties;
    }

    public void setPermissionProperties(Properties permissionProperties) {
        this.permissionProperties = permissionProperties;
    }

}
