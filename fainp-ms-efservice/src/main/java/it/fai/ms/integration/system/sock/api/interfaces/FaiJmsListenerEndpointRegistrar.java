package it.fai.ms.integration.system.sock.api.interfaces;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;

import javax.jms.MessageListener;

import org.springframework.jms.config.JmsListenerEndpointRegistrar;

public interface FaiJmsListenerEndpointRegistrar {



  public void registerDurableTopicSubscription(JmsTopicNames topicName,
                    MessageListener listener,
                    JmsListenerEndpointRegistrar registrar,
                    JmsProperties jmsProperties);

}
