package it.fai.ms.integration.system.sock.core;

/*-
 * #%L
 * PlugFace :: Spring
 * %%
 * Copyright (C) 2017 - 2018 PlugFace
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 * #L%
 */

import it.fai.ms.integration.system.sock.core.impl.DefaultPluginContext;
import it.fai.ms.integration.system.sock.core.impl.DependencyResolver;
import it.fai.ms.integration.system.sock.core.internal.AnnotationProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EntityScan( basePackages = { "it.fai.ms.integration.system.plug.artemis.config",
  "it.fai.ms.integration.system.plug.artemis.client"} )
public class SpringPluginContextAutoConfiguration implements ApplicationContextAware {
  private ApplicationContext applicationcontext;

  @Bean(name="pluginContext")
    //@ConditionalOnMissingBean
    public PluginContext pluginContext(ApplicationContext context) {
        SpringPluginContext springPluginContext = new SpringPluginContext();
        springPluginContext.setApplicationContext(context);
        return springPluginContext;
    }

    @Bean(name="pluginAnnotationProcessor")
    //@ConditionalOnMissingBean
    public AnnotationProcessor pluginAnnotationProcessor() {
        return new AnnotationProcessor();
    }

    @Bean
    //@ConditionalOnMissingBean
    public DependencyResolver pluginResolver(
      @Qualifier("pluginAnnotationProcessor") AnnotationProcessor processor) {
        return new DependencyResolver(processor);
    }

    @Bean
    @ConditionalOnMissingBean
    public PluginManager springDefaultPluginManager() {
      final DefaultPluginContext context = new DefaultPluginContext();
      final AnnotationProcessor processor = new AnnotationProcessor();
      final DependencyResolver resolver = new DependencyResolver(processor);

      return new SpringPluginManager(context, processor, resolver);
    }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationcontext = applicationContext;
  }
}
