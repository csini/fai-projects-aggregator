package it.fai.ms.integration.system.plug.artemis.config;

import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import it.fai.ms.integration.system.plug.artemis.client.ArtemisClient;
import it.fai.ms.integration.messaging.jms.Utils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.Destination;
import java.io.Serializable;

@Component(value = "artemisAdapter")
public class ArtemisAdapter implements FaiMessageSystem {

  @Resource(name = "artemisAdaptee")
  ArtemisClient adaptee;

  @Override
  public void publishMessage(String topicName, Serializable message) {

    Destination name = adaptee.getTopicDestination(Utils.TOPIC_PREFIX + "." +topicName);
    adaptee.topicPublish(name,message);
  }
}

