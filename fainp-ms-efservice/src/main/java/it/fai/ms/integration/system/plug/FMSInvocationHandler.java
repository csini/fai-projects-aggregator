package it.fai.ms.integration.system.plug;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FMSInvocationHandler implements InvocationHandler {
  private  Object adapter;
  private Object adaptee;

  public FMSInvocationHandler(Object adapter, Object adaptee) {
    this.adapter = adapter;
    this.adaptee = adaptee;
    init();
  }

  private Map<MethodIdentifier, Method> adaptedMethods =
    new HashMap<MethodIdentifier, Method>();

  // initializer block - find all methods in adapter object
  private void init()
  {
    Method[] methods = this.adapter.getClass().getDeclaredMethods();
    for (Method m : methods) {
      adaptedMethods.put(new MethodIdentifier(m), m);
    }
  }

  @Override
  public Object invoke(Object proxy, Method method,
                       Object[] args) throws Throwable {
    try {
      Method other = adaptedMethods.get(new MethodIdentifier(method));
      if (other != null) {
        return other.invoke(adapter, args);
      } else {
        return method.invoke(adaptee, args);
      }
    } catch (InvocationTargetException e) {
      throw e.getTargetException();
    }
  } //////////////Invocation handler
//////newProxyInstance


static class MethodIdentifier {
  private final String name;
  private final Class[] parameters;

  public MethodIdentifier(Method m) {
    name = m.getName();
    parameters = m.getParameterTypes();
  }

  // we can save time by assuming that we only compare against
  // other MethodIdentifier objects
  public boolean equals(Object o) {
    MethodIdentifier mid = (MethodIdentifier) o;
    return name.equals(mid.name) &&
      Arrays.equals(parameters, mid.parameters);
  }

  public int hashCode() {
    return name.hashCode();
  }
}

}
