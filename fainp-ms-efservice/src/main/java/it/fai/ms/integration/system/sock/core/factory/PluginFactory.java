package it.fai.ms.integration.system.sock.core.factory;

import javax.annotation.Nullable;

public interface PluginFactory {
  /**
   * Return an instance of the plugin matching the given type
   * given type
   *
   * @param pluginClass type the plugin must match; can be an interface or superclass. Cannot be <code>null</code>
   * @param <T>         the inferred type of the plugin
   * @return the plugin object or <code>null</code> if none exists
   */
  @Nullable
  <T> T getPlugin(Class<T> pluginClass) throws IllegalAccessException, InstantiationException, ClassNotFoundException;
}
