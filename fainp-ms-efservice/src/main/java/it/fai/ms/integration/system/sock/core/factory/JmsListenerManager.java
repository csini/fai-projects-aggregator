package it.fai.ms.integration.system.sock.core.factory;

import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;

public class JmsListenerManager implements ApplicationContextAware {
  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @Bean
  @ConditionalOnMissingBean
  public MessageSystemManager pluginContext(ApplicationContext context) {
    MessageSystemManager springPluginContext = new MessageSystemManager();
    springPluginContext.setApplicationContext(context);
    return springPluginContext;
  }
}
