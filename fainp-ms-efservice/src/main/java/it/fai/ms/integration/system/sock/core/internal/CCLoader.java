package it.fai.ms.integration.system.sock.core.internal;


//import jdk.internal.perf.PerfCounter;
import sun.misc.PerfCounter;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;

public class CCLoader extends URLClassLoader {

  private ClassLoader system;

  public CCLoader(ClassLoader parent) {
    //noinspection NamedArgsPositionMismatch
    super(new URL[0], parent);
    system = getSystemClassLoader();
  }

  public CCLoader(URL[] classpath, ClassLoader parent) {
    super(classpath, parent);
    system = getSystemClassLoader();
  }

  /**
   * IMPORTANT Method.
   * Every request for a class passes through this method. If the class is in
   * com.classloaderdemo package, we will use this classloader or else delegate the
   * request to parent classloader.
   */
  @Override
  public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
    synchronized (getClassLoadingLock(name)) {
      // First, check if the class has already been loaded
      Class<?> loadedClass = findLoadedClass(name);
      if (loadedClass == null) {
        long t0 = System.nanoTime();
        try {
          if (system != null) {
            loadedClass = system.loadClass(name);
          }
        } catch (ClassNotFoundException e) {
          // class not found in system class loader... silently skipping
        }
        try {
          if (loadedClass == null) {
            loadedClass = findClass(name);
          }
        }catch (ClassNotFoundException e) {
          // class is not found in the given urls.
          // Let's try it in parent classloader.
          // If class is still not found, then this method will throw class not found ex.
          loadedClass = super.loadClass(name, resolve);
        }

        long t1 = System.nanoTime();
        // this is the defining class loader; record the stats
         PerfCounter.getParentDelegationTime().addTime(t1 - t0);
         PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
         PerfCounter.getFindClasses().increment();

      }
      if (resolve) {
        resolveClass(loadedClass);
      }
      return loadedClass;
    }

  }

  @Override
  public URL getResource(String name) {
    URL res = null;
    if (system != null) {
      res = system.getResource(name);
    }
    if (res == null) {
      res = findResource(name);
    }
    if (res == null) {
      res = super.getResource(name);
    }
    return res;
  }

    public Class getClass (String name) throws ClassNotFoundException {
      String file = name.replace('.', File.separatorChar) + ".class";
      byte[] buffer = null;

      try {
        // This will load the byte code data from file.
        buffer = loadClassFileData(file);

        Class c = defineClass(name, buffer, 0, buffer.length);
        resolveClass(c);

        return c;
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }

      return null;
    }

    private byte[] loadClassFileData (String name) throws IOException {
      InputStream stream = getClass().getClassLoader().getResourceAsStream(name);
      int size = stream.available();
      byte[] buffer = new byte[size];
      DataInputStream in = new DataInputStream(stream);
      in.readFully(buffer);
      in.close();
      return buffer;
    }
  }
