package it.fai.ms.integration.system;

import it.fai.ms.integration.system.sock.core.factory.PluginFactory;
import it.fai.ms.integration.system.plug.artemis.ArtemisPlugin;
import it.fai.ms.integration.system.sock.core.factory.MessageSystemManager;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;

@Configuration
//@ConditionalOnClass
// ({Project.class})
//@Profile("artemis")
@PropertySource("classpath:plugin.properties")
@ComponentScan(value =
  {"it.fai.ms.integration.system.plug.artemis.client",
    "it.fai.ms.integration.system.plug.artemis.config"}
)
//@Import(value = {PluginConfig.class})
public class MessageSystemConfiguration implements ApplicationContextAware {

  @Autowired
  private static ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @Bean(name = "MessageSystemManager")


  @ConditionalOnMissingBean
  public MessageSystemManager messageSystemManager(ApplicationContext context) {
    MessageSystemManager springPluginContext = new MessageSystemManager();
    springPluginContext.setApplicationContext(this.applicationContext);
    return springPluginContext;
  }

  @Bean(name="messageSystemFactory")
  public PluginFactory messageSystemFactory(
    @Value("${plugin.message.system.adapter}") String adapter,
    @Value("${plugin.message.system.adaptee}") String adaptee) throws ClassNotFoundException {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();

    Class adapterClass = cl.loadClass(adapter);
    Class adapteeClass = cl.loadClass(adaptee);
    AutowireCapableBeanFactory beanFactory =
      applicationContext.getAutowireCapableBeanFactory();
    ArtemisPlugin ap = new ArtemisPlugin(
      beanFactory.autowire(adapterClass,
        AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,true),
      beanFactory.autowire(adapteeClass,

        AutowireCapableBeanFactory.AUTOWIRE_BY_NAME,true)
    );
    return ap;
  }
}
