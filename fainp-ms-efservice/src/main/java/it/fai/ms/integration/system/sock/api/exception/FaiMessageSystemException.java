package it.fai.ms.integration.system.sock.api.exception;

public class FaiMessageSystemException extends RuntimeException {

  public FaiMessageSystemException() {
    super();
  }

  public FaiMessageSystemException(String message) {
    super(message);
  }

  public FaiMessageSystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}

