package it.fai.ms.integration.system.sock.core;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

public class PluginSourceImpl implements PluginSource {
  @Override
  public Collection<Class<?>> load(final URI pluginUri) throws Exception {
    final ArrayList<Class<?>> plugins = new ArrayList<>();
    final Path path = Paths.get(pluginUri);
    if (!Files.exists(path)) {
      throw new IllegalArgumentException("Path " + pluginUri + " does not exist");
    }


    return null;
  }
}
