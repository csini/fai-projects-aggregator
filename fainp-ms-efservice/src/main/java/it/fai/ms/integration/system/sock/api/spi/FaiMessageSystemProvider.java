package it.fai.ms.integration.system.sock.api.spi;

import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;

public interface FaiMessageSystemProvider {
  public FaiMessageSystem get();
}
