package it.fai.ms.integration.system.plug.artemis.client;

import it.fai.ms.common.jms.DestinationResolverImpl;
import it.fai.ms.common.jms.JmsListenerErrorHandler;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.integration.messaging.jms.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashSet;
import java.util.Set;


@Component("artemisJmsListenerAdaptee")
public class ArtemisFaiJmsListenerEndpointRegistrar {

  private static final Logger log = LoggerFactory.getLogger(ArtemisFaiJmsListenerEndpointRegistrar.class);

  private static final Set<String> registeredTopic = new HashSet<>();
  private static final Set<String> registeredQueue = new HashSet<>();
  @Autowired
  @Resource(name = "cachingConnectionFactory")
  public CachingConnectionFactory cachingConnectionFactory;

  @Autowired
  @Resource(name="queueJmsTemplate")
  JmsTemplate queueJmsTemplate;
  /*
  @Autowired
  @Resource(name="topicJmsTemplate")
  JmsTemplate topicJmsTemplate;
*/


  public void registerQueueConsumer(String queueName,
                                    MessageListener listener,
                                    JmsListenerEndpointRegistrar registrar,
                                    JmsProperties jmsProperties) {

    log.info("jmsUrl = " + jmsProperties.getUrl());


    try {
      log.debug("Registering queue {}:{} ...", queueName,
        listener.getClass().getName());
      SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
      endpoint.setId(listener.getClass().getName());
      endpoint.setDestination(queueName);
      // endpoint.setConcurrency("1");
      endpoint.setMessageListener(listener);
      registrar.registerEndpoint(endpoint, jmsConsumerListenerContainerFactory(queueName, jmsProperties));
    } catch (Exception e) {
      throw new RuntimeException("ERRORE FATALE: non è stato possibile registrare la subscription " + listener.getClass()
        .getName()
        + " per la queue " + queueName, e);
    }
    registeredQueue.add(queueName);
  }

  private DefaultJmsListenerContainerFactory jmsConsumerListenerContainerFactory(String queueName,
                                                                                 JmsProperties jmsProperties) throws JMSException,
    NamingException {
    String url = jmsProperties.getUrl();
    // String port = jmsProperties.getCarbonDefaultPort();
    // String username = jmsProperties.getUserName();
    // String password = jmsProperties.getPassword();

    Assert.hasText(url, "jms.url non è presente nel application*.yml");

    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    // JmsConfigurationDTO conf = AndesJmsListenerEndpointRegistrar.jmsConfiguration(url, port, username, password);
    InitialContext ic = new InitialContext();
    QueueConnectionFactory cf = (QueueConnectionFactory) ic.lookup(jmsProperties.getCfName());

    createQueueIfNotExist(queueName, cf, ic);

    factory.setConnectionFactory(cf);
    factory.setDestinationResolver(new DestinationResolverImpl(ic));
    // factory.setConcurrency("3-10"); // Massimo un sottoscrittore alla volta per jvm
    factory.setAutoStartup(true);
    factory.setErrorHandler(new JmsListenerErrorHandler(queueName, "queue"));
    factory.setSessionTransacted(true);
    factory.setRecoveryInterval(30000L);
    return factory;
  }


  public void registerDurableTopicSubscription(String topicName, MessageListener listener,
                                               JmsListenerEndpointRegistrar registrar,
                                               JmsProperties jmsProperties) {
    String subscriptionName = listener.getClass().getName();

    int indexDollaro = subscriptionName.indexOf("$");
    if (indexDollaro > 0) {
      subscriptionName = subscriptionName.substring(0, indexDollaro);
    }

    String destination = Utils.buildTopicName(topicName);

    try {
      log.debug("Registering topic {}:{} ...", topicName, subscriptionName);
      SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
      endpoint.setId(subscriptionName);
      endpoint.setDestination(destination);
      endpoint.setSubscription(subscriptionName);
      endpoint.setMessageListener(listener);
      registrar.registerEndpoint(endpoint,
        jmsDurableTopicListenerContainerFactory(topicName, subscriptionName,
          jmsProperties));
    } catch (Exception e) {
      throw new RuntimeException("ERRORE FATALE: non è stato possibile registrare la subscription " + listener.getClass().getName() + " per la topic " + topicName, e);
    }

    registeredTopic.add(topicName);
  }

  private DefaultJmsListenerContainerFactory jmsDurableTopicListenerContainerFactory(String topicName, String subscriptionName, JmsProperties jmsProperties) throws JMSException, NamingException {
    String url = jmsProperties.getUrl();
    Assert.hasText(url, "jms.url non è presente nel application*.yml");
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    //InitialContext ic = initialTopicContext(jmsProperties, topicName);
    InitialContext ic = new InitialContext();
    //ConnectionFactory cf = (TopicConnectionFactory)cachingConnectionFactory.getTargetConnectionFactory();
    createDurableSubscriptionIfNotExist(topicName, subscriptionName, cachingConnectionFactory, ic);
    factory.setConnectionFactory(cachingConnectionFactory);
    factory.setDestinationResolver(new DestinationResolverImpl(ic));
    factory.setAutoStartup(true);
    factory.setSubscriptionDurable(true);
    factory.setErrorHandler(new JmsListenerErrorHandler("topic", topicName));
    factory.setSessionTransacted(true);
    factory.setRecoveryInterval(30000L);

    return factory;
  }

  private void createDurableSubscriptionIfNotExist(String topicName,
                                                   String subscriptionName,
                                                   TopicConnectionFactory topicConnectionFactory,
                                                   InitialContext ctx) throws JMSException, NamingException {
    Topic topic = (Topic) ctx.lookup(topicName);

    try (TopicConnection topicConnection = topicConnectionFactory.createTopicConnection();
         TopicSession topicSession = topicConnection.createTopicSession(false, QueueSession.AUTO_ACKNOWLEDGE);
         TopicSubscriber topicSubscriber = topicSession.createDurableSubscriber(topic, subscriptionName)) {
      topicConnection.start();
      //log.info("Created Durable Subscription {} for topic {}", subscriptionName, topic);
    }
  }

  private void createQueueIfNotExist(String queueName, QueueConnectionFactory cf, InitialContext ic) {
    // TODO Auto-generated method stub

  }

}
