package it.fai.ms.integration.system.plug.factory;

import it.fai.ms.integration.system.plug.FMSInvocationHandler;
import it.fai.ms.integration.system.sock.core.internal.PluginClassLoader;

import java.lang.reflect.Proxy;
import java.net.URI;
import java.net.URL;

public class AdapterFactory {
  public static <T> T adapt(final String adapteeClassName,
                            final Class<T> target,
                            final String adapterClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    PluginClassLoader pcl = new PluginClassLoader(new URL[]{},
      Thread.currentThread().getContextClassLoader());
    //ClassLoader cl = target.getClassLoader();
    Class adapteeClass = pcl.loadClass(adapteeClassName);
    Class adapterClass = pcl.loadClass(adapterClassName);

    return (T) Proxy.newProxyInstance(
      pcl,
      new Class[]{target},
      new FMSInvocationHandler(adapterClass.newInstance(),adapteeClass.newInstance())
    );
  }

  public static <T> T adapt(final Object adaptee,
                            final Class<T> target,
                            final Object adapter) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

    return (T) Proxy.newProxyInstance(
      target.getClassLoader(),
      new Class[]{target},
      new FMSInvocationHandler(adapter,adaptee)
    );
  }
}
