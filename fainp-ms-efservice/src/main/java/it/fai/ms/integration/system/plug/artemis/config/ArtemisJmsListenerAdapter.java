package it.fai.ms.integration.system.plug.artemis.config;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.integration.system.plug.artemis.client.ArtemisFaiJmsListenerEndpointRegistrar;
import it.fai.ms.integration.system.sock.api.interfaces.FaiJmsListenerEndpointRegistrar;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.MessageListener;

@Component(value = "artemisListenerAdapter")
public class ArtemisJmsListenerAdapter implements FaiJmsListenerEndpointRegistrar {

  @Resource(name = "artemisJmsListenerAdaptee")
  ArtemisFaiJmsListenerEndpointRegistrar jmsListenerAdaptee;

  @Override
  public void registerDurableTopicSubscription(JmsTopicNames topicName,
          MessageListener listener,
          JmsListenerEndpointRegistrar registrar,
          JmsProperties jmsProperties) {
    //noinspection NamedArgsPositionMismatch
    jmsListenerAdaptee.registerDurableTopicSubscription(
      topicName.name(), listener, registrar, jmsProperties);

  }
}
