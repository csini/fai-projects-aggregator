package it.fai.ms.integration.messaging.jms;

public class Utils {
  public static final String TOPIC_PREFIX= "topic";

  public static String buildTopicName(String topicName){
    StringBuilder sb = new StringBuilder(TOPIC_PREFIX);
    sb.append(".").append(topicName);
    return sb.toString();
  }
}
