package it.fai.ms.integration.system.sock.core.factory;

import it.fai.ms.integration.system.sock.api.interfaces.FaiJmsListenerEndpointRegistrar;
import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class MessageSystemManager implements ApplicationContextAware {
  @Autowired
  private ApplicationContext context;


  @Resource(name="messageSystemFactory")
  @Autowired
  public PluginFactory messageSystemFactory ;//= (PluginFactory) context.getBean("messageSystemFactory");

  public FaiMessageSystem loadMessageSystem() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
    return messageSystemFactory.getPlugin(FaiMessageSystem.class);
  }

  @Resource(name="jmsListenerFactory")
  public PluginFactory jmsListenerFactory;

  public FaiJmsListenerEndpointRegistrar loadJmsListener() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
    return jmsListenerFactory.getPlugin(FaiJmsListenerEndpointRegistrar.class);
  }

  public void setApplicationContext(ApplicationContext context) {
    this.context = context;
  }
}
