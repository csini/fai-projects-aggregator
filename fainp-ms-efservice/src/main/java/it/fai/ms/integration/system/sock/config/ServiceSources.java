package it.fai.ms.integration.system.sock.config;

import it.fai.ms.integration.system.sock.core.internal.CCLoader;
import it.fai.ms.integration.system.sock.core.internal.PluginClassLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:config/application-kafka.yml")
public class ServiceSources {

  @Value("${spring.plugin.message.system.pluginpath}")
   private String pluginpath;

  /**
   * Load plugins from JAR files located at the given {@link URI}
   *
   * @param pluginUri the {@link URI} to the directory where the JAR files are located
   * @return a list of loaded {@link Class} objects, never null
   */
  @Bean(name = "pluginClassLoader")
  public ClassLoader jarSource() throws IOException {
    URI pluginUri = URI.create(pluginpath);
    final ArrayList<Class<?>> plugins = new ArrayList<>();
    final Path path = Paths.get(pluginUri);
    if (!Files.exists(path)) {
      throw new IllegalArgumentException("Path " + pluginUri + " does not exist");
    }
    if (Files.isDirectory(path)) {
      throw new IllegalArgumentException("Path " + pluginUri + " is  a directory");
    }
    final Map<Path, URL> jarUrls = new HashMap<>();

        jarUrls.put(path, path.toUri().toURL());

    ClassLoader tc =Thread.currentThread().getContextClassLoader();
    //final PluginClassLoader cl = new PluginClassLoader(jarUrls.values().toArray(new URL[]{}));
    final CCLoader cl = new CCLoader(jarUrls.values().toArray(new URL[]{}),
     tc );

    return tc;
  }

  //To resolve ${} in @Value
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }
}
