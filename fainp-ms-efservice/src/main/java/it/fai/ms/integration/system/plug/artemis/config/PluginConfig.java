package it.fai.ms.integration.system.plug.artemis.config;

import it.fai.ms.integration.system.sock.core.factory.PluginFactory;

import it.fai.ms.integration.system.plug.artemis.ArtemisPlugin;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan
@PropertySource("classpath:plugin.properties")
//@ConfigurationProperties(prefix = "spring.plugin")
public class PluginConfig implements ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Bean(name="messageSystemFactory")
  public PluginFactory messageSystemFactory(
    @Value("${plugin.message.system.adapter}") String adapter,
    @Value("${plugin.message.system.adaptee}") String adaptee) throws ClassNotFoundException {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();

    Class adapterClass = cl.loadClass(adapter);
    Class adapteeClass = cl.loadClass(adaptee);
    AutowireCapableBeanFactory beanFactory =
      applicationContext.getAutowireCapableBeanFactory();
    ArtemisPlugin ap = new ArtemisPlugin(
    beanFactory.autowire(adapterClass,
      AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,true),
      beanFactory.autowire(adapteeClass,

        AutowireCapableBeanFactory.AUTOWIRE_BY_NAME,true)
    );
    return ap;
  }

  @Bean(name="jmsListenerFactory")
  public PluginFactory jmsListenerFactory(
    @Value("${plugin.jms.listener.adapter}") String adapter,
    @Value("${plugin.jms.listener.adaptee}") String adaptee) throws ClassNotFoundException {
    ClassLoader cl = Thread.currentThread().getContextClassLoader();

    Class adapterClass = cl.loadClass(adapter);
    Class adapteeClass = cl.loadClass(adaptee);
    AutowireCapableBeanFactory beanFactory =
      applicationContext.getAutowireCapableBeanFactory();
    return  new ArtemisPlugin(
      beanFactory.autowire(adapterClass,
        AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,true),
      beanFactory.autowire(adapteeClass,
        AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE,true)
    );

  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
