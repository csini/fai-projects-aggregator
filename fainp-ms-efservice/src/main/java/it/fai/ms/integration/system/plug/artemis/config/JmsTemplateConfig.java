package it.fai.ms.integration.system.plug.artemis.config;

import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.api.jms.ActiveMQJMSClient;
import org.apache.activemq.artemis.api.jms.JMSFactoryType;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQQueueConnectionFactory;
import org.apache.activemq.artemis.jms.client.ActiveMQTopicConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.util.Assert;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

@Configuration
//@Profile("artemis")
@ConfigurationProperties(prefix = "spring.artemis")
public class JmsTemplateConfig {
  Logger log  = LoggerFactory.getLogger(this.getClass());

  //@Value("${broker-url}")
  private String brokerUrl = "tcp://localhost:61616?jms.useAsyncSend=true";

  //@Value("${spring.artemis.host}")
  private String host = "tcp://localhost:61616";

  @Bean(name = "TopicConnectionFactory")
  public ConnectionFactory senderConnectionFactory() throws JMSException {
    //ActiveMQConnectionFactory activeMQConnectionFactory =
    //new ActiveMQConnectionFactory();
    //activeMQConnectionFactory.setBrokerURL(brokerUrl);

    TransportConfiguration transportConfiguration = new TransportConfiguration(NettyConnectorFactory.class.getName());

    ActiveMQTopicConnectionFactory cf = (ActiveMQTopicConnectionFactory) ActiveMQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.TOPIC_CF, transportConfiguration);
    Assert.notNull(brokerUrl,"Broker URL is null");
    cf.setBrokerURL(brokerUrl);

    return cf;
  }

  @Bean(name = "queueConnectionFactory")
  public ConnectionFactory queueConnectionFactory() throws JMSException {
    //ActiveMQConnectionFactory activeMQConnectionFactory =
    //new ActiveMQConnectionFactory();
    //activeMQConnectionFactory.setBrokerURL(brokerUrl);

    TransportConfiguration transportConfiguration = new TransportConfiguration(NettyConnectorFactory.class.getName());

    ActiveMQQueueConnectionFactory cf = (ActiveMQQueueConnectionFactory) ActiveMQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.QUEUE_CF, transportConfiguration);
    Assert.notNull(brokerUrl,"Broker URL is null");
    cf.setBrokerURL(brokerUrl);

    return cf;
  }

  @Bean(name = "cachingConnectionFactory")
  public CachingConnectionFactory cachingConnectionFactory(@Qualifier("TopicConnectionFactory") ConnectionFactory connectionFactory) throws JMSException {
    CachingConnectionFactory cachingConnectionFactory =
      new CachingConnectionFactory(connectionFactory);
    cachingConnectionFactory.setSessionCacheSize(10);
    return cachingConnectionFactory;
  }

  @Bean(name = "queueCachingConnectionFactory")
  public CachingConnectionFactory queueCachingConnectionFactory(@Qualifier("queueConnectionFactory") ConnectionFactory queueConnectionFactory) throws JMSException {
    CachingConnectionFactory cachingConnectionFactory =
      new CachingConnectionFactory(queueConnectionFactory);
    cachingConnectionFactory.setSessionCacheSize(10);
    return cachingConnectionFactory;
  }

  @Bean(name = "topicJmsTemplate")
  public JmsTemplate topicJmsTemplate(@Qualifier("cachingConnectionFactory") CachingConnectionFactory cachingConnectionFactory) {
    JmsTemplate jmsTemplate =
      new JmsTemplate(cachingConnectionFactory);
    //jmsTemplate.setDefaultDestination(orderDestination());
    // jmsTemplate.setReceiveTimeout(5000);

    return jmsTemplate;
  }

  @Bean(name="queueJmsTemplate")
  public JmsTemplate queueJmsTemplate(@Qualifier("queueCachingConnectionFactory") CachingConnectionFactory cachingConnectionFactory) {
    JmsTemplate jmsTemplate =
      new JmsTemplate(cachingConnectionFactory);

    return jmsTemplate;
  }

  public String getBrokerUrl() {
    return brokerUrl;
  }

  public void setBrokerUrl(String brokerUrl) {
    this.brokerUrl = brokerUrl;
  }
}
