package it.fai.ms.integration.system.sock.api.interfaces;

import it.fai.ms.integration.system.sock.api.exception.FaiMessageSystemException;

import java.io.Serializable;

public interface FaiMessageSystem {
  public void publishMessage(String topicName, Serializable message)
    throws FaiMessageSystemException;
}
