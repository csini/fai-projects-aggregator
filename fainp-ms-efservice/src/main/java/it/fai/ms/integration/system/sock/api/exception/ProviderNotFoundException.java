package it.fai.ms.integration.system.sock.api.exception;

public class ProviderNotFoundException extends RuntimeException {

  public ProviderNotFoundException() {
    super();
  }

  public ProviderNotFoundException(String message) {
    super(message);
  }

}

