package it.fai.ms.integration.system.sock.core.factory;

/*-
 * #%L
 * PlugFace :: Core
 * %%
 * Copyright (C) 2017 - 2018 PlugFace
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 * #L%
 */

import it.fai.ms.integration.system.sock.core.PluginContext;
import it.fai.ms.integration.system.sock.core.PluginManager;
import it.fai.ms.integration.system.sock.core.impl.DefaultPluginContext;
import it.fai.ms.integration.system.sock.core.impl.DefaultPluginManager;
import it.fai.ms.integration.system.sock.core.impl.DependencyResolver;
import it.fai.ms.integration.system.sock.core.internal.AnnotationProcessor;

public class PluginManagers {

    /**
     * A default {@link PluginManager} configured with internal dependencies for {@link PluginContext}, {@link AnnotationProcessor} and {@link DependencyResolver}
     *
     * @return a fully configured {@link DefaultPluginManager}
     */
    public static PluginManager defaultPluginManager() {
        final DefaultPluginContext context = new DefaultPluginContext();
        final AnnotationProcessor processor = new AnnotationProcessor();
        final DependencyResolver resolver = new DependencyResolver(processor);
        return newPluginManager(context, processor, resolver);
    }

    public static PluginManager newPluginManager(PluginContext context, AnnotationProcessor processor, DependencyResolver resolver) {
        return new DefaultPluginManager(context, processor, resolver);
    }
}
