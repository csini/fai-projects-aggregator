package it.fai.ms.efservice.wizard.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5MatrixServiceType;
import it.fai.ms.efservice.wizard.model.matrix.step5.WizardStep5MatrixVehicle;
import it.fai.ms.efservice.wizard.service.Step5Service;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc(webClientEnabled = true, webDriverEnabled = false)
public class Step5ControllerTest {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private MockMvc         mockMvc;
  private Step5Controller step5Controller;
  private Step5Service    step5Service = mock(Step5Service.class);

  @Before
  public void setUp() throws Exception {
    step5Controller = spy(new Step5Controller(step5Service));
    mockMvc = MockMvcBuilders.standaloneSetup(step5Controller)
                             .build();
  }

  @Test
  public void testGetMatrix() throws Exception {
    /*
     * given(step5Service.buildMatrixList(newWizardDeviceTypeId(), newVehicleIds().stream() .map(itm -> new
     * WizardVehicleId(itm)) .collect(toSet()))).willReturn(newStep5WizardMatrix());
     */

    // FIXME
    given(step5Service.buildMatrixList(anySet())).willReturn(newStep5WizardMatrixList());

    MvcResult mvcResult = mockMvc.perform(newRequestBuilder())
                                 .andReturn();

    String contentType = mvcResult.getRequest()
                                  .getContentType();
    assertThat(contentType).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());

    String cacheHeader = mvcResult.getResponse()
                                  .getHeader("Cache-control");
    assertThat(cacheHeader).contains("no-store");

    String json = mvcResult.getResponse()
                           .getContentAsString();

    _log.info("Returned json: {}", json);
    JSONAssert.assertEquals(JsonUtils.readFile("step5.json"), json, false);

  }

  private List<WizardStep5Matrix> newStep5WizardMatrixList() {
    return Arrays.asList(newStep5WizardMatrix());
  }

  @Test
  public void testGetCompleteMatrix() throws Exception {
    // FIXME
    given(step5Service.buildMatrixList(anySet())).willReturn(newStep5WizardMatrixList());

    MvcResult mvcResult = mockMvc.perform(newRequestBuilderCompleteMatrix())
                                 .andReturn();

    String contentType = mvcResult.getRequest()
                                  .getContentType();
    assertThat(contentType).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());

    String cacheHeader = mvcResult.getResponse()
                                  .getHeader("Cache-control");
    assertThat(cacheHeader).contains("no-store");

    String json = mvcResult.getResponse()
                           .getContentAsString();

    _log.info("Returned json: {}", json);
    JSONAssert.assertEquals(JsonUtils.readFile("step5.json"), json, false);

  }

  private String newRequestBody() {
    String json = null;
    try {
      json = JsonUtils.readFile("step5req.json");
    } catch (IOException e) {
      e.printStackTrace();
    }
    return json;
  }

  private RequestBuilder newRequestBuilder() {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/wizard/step5/matrix/::serviceId::")
                                                          .content(newRequestBody())
                                                          .contentType(MediaType.APPLICATION_JSON_UTF8)
                                                          .accept(MediaType.APPLICATION_JSON_UTF8);
    return requestBuilder;
  }

  private RequestBuilder newRequestBuilderCompleteMatrix() {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/wizard/step5/matrix/all")
                                                          .content(newRequestBody())
                                                          .contentType(MediaType.APPLICATION_JSON_UTF8)
                                                          .accept(MediaType.APPLICATION_JSON_UTF8);
    return requestBuilder;
  }

  private WizardStep5Matrix newStep5WizardMatrix() {
    WizardStep5Matrix wizardStep5MatrixValue = new WizardStep5Matrix(newWizardDeviceTypeId().getId());

    WizardStep5MatrixVehicle vehicle1 = new WizardStep5MatrixVehicle("::vehicleId1::");
    vehicle1.setEuroClass("::euroClass1::");
    vehicle1.setLicensePlate("::licensePlate1::");
    vehicle1.setType("::type1::");
    WizardStep5MatrixServiceType serviceType1 = new WizardStep5MatrixServiceType("::serviceType1::");
    serviceType1.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle1.getServiceTypes()
            .put("::serviceType1::", serviceType1);
    WizardStep5MatrixServiceType serviceType2 = new WizardStep5MatrixServiceType("::serviceType2::");
    serviceType2.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle1.getServiceTypes()
            .put("::serviceType2::", serviceType2);
    WizardStep5MatrixServiceType serviceType3 = new WizardStep5MatrixServiceType("::serviceType3::");
    serviceType3.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle1.getServiceTypes()
            .put("::serviceType3::", serviceType3);
    WizardStep5MatrixServiceType serviceType4 = new WizardStep5MatrixServiceType("::serviceType4::");
    serviceType4.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle1.getServiceTypes()
            .put("::serviceType4::", serviceType4);

    WizardStep5MatrixVehicle vehicle2 = new WizardStep5MatrixVehicle("::vehicleId2::");
    vehicle2.setEuroClass("::euroClass2::");
    vehicle2.setLicensePlate("::licensePlate2::");
    vehicle2.setType("::type2::");

    WizardStep5MatrixServiceType serviceType5 = new WizardStep5MatrixServiceType("::serviceType5::");
    serviceType5.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle2.getServiceTypes()
            .put("::serviceType5::", serviceType5);
    WizardStep5MatrixServiceType serviceType6 = new WizardStep5MatrixServiceType("::serviceType6::");
    serviceType6.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle2.getServiceTypes()
            .put("::serviceType6::", serviceType6);
    WizardStep5MatrixServiceType serviceType7 = new WizardStep5MatrixServiceType("::serviceType7::");
    serviceType7.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle2.getServiceTypes()
            .put("::serviceType7::", serviceType7);
    WizardStep5MatrixServiceType serviceType8 = new WizardStep5MatrixServiceType("::serviceType8::");
    serviceType8.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    vehicle2.getServiceTypes()
            .put("::serviceType8::", serviceType8);

    wizardStep5MatrixValue.getVehicles()
                          .add(vehicle1);
    wizardStep5MatrixValue.getVehicles()
                          .add(vehicle2);

    return wizardStep5MatrixValue;
  }

  private Set<String> newVehicleIds() {
    return new LinkedHashSet<>(Arrays.asList("::vehicleId1::", "::vehicleId2::"));
  }

  private WizardDeviceTypeId newWizardDeviceTypeId() {
    return new WizardDeviceTypeId("::deviceTypeId::");
  }

}
