package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.TipoHardwareDispositivoService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoHardwareDispositivoServiceImplTest {

  @Autowired
  private TipoHardwareDispositivoService hardwareTypeDeviceService;

  @Autowired
  private TipoDispositivoRepository deviceTypeRepo;

  private Long                    idDeviceType;
  private TipoDispositivoEnum     deviceTypeName;
  private TipoHardwareDispositivo entity;
  private final String            hardwareType = "AF";
  TipoDispositivo tipoDispositivo = null;

  @Before
  public void setUp() throws Exception {

    hardwareTypeDeviceService.deleteAll();
    tipoDispositivo = deviceTypeRepo.findOne(2L);
    idDeviceType = tipoDispositivo.getId();
    deviceTypeName = tipoDispositivo.getNome();

    entity = new TipoHardwareDispositivo();
    entity.setHardwareType(hardwareType);
    entity.setTipoDispositivo(tipoDispositivo);
    entity.setDefaultHardwareType(true);
    entity.setDescrizione("Description");
    entity.setAttivo(true);

    hardwareTypeDeviceService.save(entity);
  }

  @After
  public void delete() throws Exception {
    hardwareTypeDeviceService.deleteAll();
  }

  @Test
  public void saveMoreEntityWithSameDeviceType() {
    TipoDispositivo tipoDispositivo = deviceTypeRepo.findOneByNome(TipoDispositivoEnum.TRACKYCARD);
    
    entity = new TipoHardwareDispositivo();
    entity.setHardwareType(hardwareType);
    entity.setTipoDispositivo(tipoDispositivo);
    entity.setDefaultHardwareType(true);
    entity.setDescrizione("Description");
    entity.setAttivo(true);
    hardwareTypeDeviceService.save(entity);
    
    entity = new TipoHardwareDispositivo();
    entity.setHardwareType(hardwareType);
    entity.setTipoDispositivo(tipoDispositivo);
    entity.setDefaultHardwareType(true);
    entity.setDescrizione("Description");
    entity.setAttivo(true);
    hardwareTypeDeviceService.save(entity);
  }

  @Test
  public void findByEnum() {
    List<TipoHardwareDispositivo> list = hardwareTypeDeviceService.findActiveByDeviceType(deviceTypeName);
    assertThat(list).isNotNull();
    assertThat(list).isNotEmpty();
    assertThat(list).hasSize(1);
    
    entity = new TipoHardwareDispositivo();
    entity.setHardwareType(hardwareType);
    entity.setTipoDispositivo(tipoDispositivo);
    entity.setDefaultHardwareType(false);
    entity.setDescrizione("Description");
    entity.setAttivo(false);
    hardwareTypeDeviceService.save(entity);
    
    list = hardwareTypeDeviceService.findActiveByDeviceType(deviceTypeName);
    assertThat(list).isNotNull();
    assertThat(list).isNotEmpty();
    assertThat(list).hasSize(1);

    list = hardwareTypeDeviceService.findActiveByDeviceType(TipoDispositivoEnum.VIA_TOLL);
    assertThat(list).isNotNull();
    assertThat(list).isEmpty();
  }

  @Test
  public void findByDeviceTypeId() {
    List<TipoHardwareDispositivo> list = hardwareTypeDeviceService.findActiveByDeviceTypeId(idDeviceType);
    assertThat(list).isNotNull();
    assertThat(list).isNotEmpty();
    assertThat(list).hasSize(1);
    
    entity = new TipoHardwareDispositivo();
    entity.setHardwareType(hardwareType);
    entity.setTipoDispositivo(tipoDispositivo);
    entity.setDefaultHardwareType(false);
    entity.setDescrizione("Description");
    entity.setAttivo(false);
    hardwareTypeDeviceService.save(entity);
    
    list = hardwareTypeDeviceService.findActiveByDeviceTypeId(idDeviceType);
    assertThat(list).isNotNull();
    assertThat(list).isNotEmpty();
    assertThat(list).hasSize(1);

    list = hardwareTypeDeviceService.findActiveByDeviceTypeId(10L);
    assertThat(list).isNotNull();
    assertThat(list).isEmpty();
  }

}
