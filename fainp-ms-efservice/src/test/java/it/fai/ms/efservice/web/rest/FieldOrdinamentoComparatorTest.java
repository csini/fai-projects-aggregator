package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;

public class FieldOrdinamentoComparatorTest {

	@Test
	public void testCompare() {
		List<CategoriaServizioDTO> lista = createCategoriaServizioDtoList();

		Collections.sort(lista, new FieldOrdinamentoComparator());

		List<Long> collect = lista.stream().map(x -> x.getOrdinamento()).collect(Collectors.toList());
		assertThat(collect).isSorted();
	}

	private List<CategoriaServizioDTO> createCategoriaServizioDtoList() {
		List<CategoriaServizioDTO> lista = new LinkedList<>();
		CategoriaServizioDTO categoriaServizio1 = new CategoriaServizioDTO();
		CategoriaServizioDTO categoriaServizio2 = new CategoriaServizioDTO();
		CategoriaServizioDTO categoriaServizio3 = new CategoriaServizioDTO();
		categoriaServizio1.setOrdinamento(5L);
		categoriaServizio2.setOrdinamento(1L);
		categoriaServizio3.setOrdinamento(3L);
		lista.add(categoriaServizio1);
		lista.add(categoriaServizio2);
		lista.add(categoriaServizio3);
		return lista;
	}
}
