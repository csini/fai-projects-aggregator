package it.fai.ms.efservice.config;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractCommonTest {

  protected static String RandomEmail() {
    return RandomStringUtils.random(17, true, true) + "@" + RandomStringUtils.random(10, true, false) + "."
        + RandomStringUtils.random(2, true, false);
  }

  protected static String RandomString() {
    return RandomStringUtils.random(20, true, true).toUpperCase();
  }

  protected static String RandomNumeric() {
    return RandomStringUtils.randomNumeric(10);
  }

  protected static String RandomLicense() {
    return (RandomStringUtils.randomAlphabetic(2) + RandomStringUtils.randomNumeric(5) + RandomStringUtils.randomAlphabetic(2)).toUpperCase();
  }

  protected static String RandomString(int count) {
    return RandomStringUtils.random(count, true, true).toUpperCase();
  }

  protected static String RandomUUID() {
    return UUID.randomUUID()
        .toString();
  }  

  protected static void logToJson(Object obj) throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    final String jsonInString = mapper.writeValueAsString(obj);
    System.out.println(jsonInString);    
  }

  protected static String RandomNumeric(int count) {
    return RandomStringUtils.randomNumeric(count);
  }

  protected static String randomElement(String... strings) {
    List<String> givenList = Arrays.asList(strings);
    Random rand = new Random();
    return givenList.get(rand.nextInt(givenList.size()));
  }  
  
  protected static Double RandomDouble() {
    Random r = new Random();
    return 0.1 + (10 - 0.1) * r.nextDouble();    
  }

}
