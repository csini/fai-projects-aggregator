package it.fai.ms.efservice.service.jms.listener.device;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;

import javax.jms.ObjectMessage;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.jms.efservice.message_v2.device.DispositivoUUID;
import it.fai.ms.common.jms.efservice.message_v2.device.StatoDispositivoServizioUpdateMessage;
import it.fai.ms.common.jms.efservice.message_v2.device.StatoDispositivoServizioUpdateMessage.StatoDS;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.AbstractCommonTest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore
public class JmsListenerStatoDispositivoServizioUpdateMessageExtTest extends AbstractCommonTest {

  @Autowired
  JmsListenerStatoDispositivoServizioUpdateMessage listener;
  
  @Test
  public void onMessage() throws Exception {
    Instant dataAttivazione = Instant.now();
    DispositivoUUID dispositivoUUID = new DispositivoUUID("ABCD");    
    StatoDispositivoServizioUpdateMessage dto = new StatoDispositivoServizioUpdateMessage(dispositivoUUID, "TESSERA_PREMIUM_TRUCK", dataAttivazione);
    dto.setStato(StatoDS.ATTIVO);
    
    ObjectMessage objectMessage = mock(ObjectMessage.class);
    when(objectMessage.getObject()).thenReturn(dto);
    
    listener.onMessage(objectMessage);    

    dto.setStato(StatoDS.NON_ATTIVO);
    listener.onMessage(objectMessage);    
  }
  
}
