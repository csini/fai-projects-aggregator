package it.fai.ms.efservice.service.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import org.jgroups.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.dto.RichiestaOrdineClienteDTO;

@RunWith(MockitoJUnitRunner.class)
public class ChangeStatusOrdineClienteServiceImplTest {

  private static final String IDENTIFICATIVO_ORDINE = "::identificativoOrdine::";

  private OrdineClienteServiceExt ordineClienteServiceMock = mock(OrdineClienteServiceExt.class);

  private ChangeStatusOrdineClienteServiceImpl changeStatusOrdineClienteService;

  private RichiestaRepositoryExt richiestaRepo = mock(RichiestaRepositoryExt.class);

  private OrdineCliente ordineCliente;

  @Before
  public void setUp() {
    changeStatusOrdineClienteService = new ChangeStatusOrdineClienteServiceImpl(ordineClienteServiceMock, richiestaRepo);
    ordineCliente = newOrdineCliente();
  }

  @Test
  public void OrdineCompletedTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.ATTIVO_EVASO));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.EVASO_CON_RICONSEGNA));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.EVASO_CON_DENUNCIA));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.COMPLETATO));
  }

  @Test
  public void OrdineSospesoLavParzialmenteTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.SOSPESA));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.INOLTRATO));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.SOSPESO_E_LAVORATO_PARZIALMENTE));
  }

  @Test
  public void OrdineSospesoTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.SOSPESA));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.INOLTRATO));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.INOLTRATO));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.SOSPESO));
  }

  @Test
  public void OrdineRifiutatoTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.RICHIESTA_RIFIUTATA));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.RICHIESTA_RIFIUTATA));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.RICHIESTA_RIFIUTATA));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.RIFIUTATO));
  }

  @Test
  public void OrdineAccettatoParzialeTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.INOLTRATO));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.RICHIESTA_RIFIUTATA));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.ACCETTATO_PARZIALE));
  }

  @Test
  public void OrdineAccettatoTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.ACCETTATO_FORNITORE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.FURTO));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.ACCETTATO));
  }

  @Test
  public void OrdineLavoratoParzialeTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.ACCETTATO_FORNITORE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock).save(ordineCliente.stato(StatoOrdineCliente.LAVORATO_PARZIALMENTE));
  }

  @Test
  public void OrdineDaEvadereTest() {
    Set<RichiestaOrdineClienteDTO> richiestas = new HashSet<>();
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    richiestas.add(new RichiestaOrdineClienteDTO(System.currentTimeMillis(), StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE));
    when(ordineClienteServiceMock.findByIdentificativoOrdineCliente(IDENTIFICATIVO_ORDINE)).thenReturn(ordineCliente);
    when(richiestaRepo.findStatoRichiestaByIdentificativoOrdine(IDENTIFICATIVO_ORDINE)).thenReturn(richiestas);
    changeStatusOrdineClienteService.calculateAndChangeStatus(IDENTIFICATIVO_ORDINE);

    verify(ordineClienteServiceMock, never()).save(ordineCliente.stato(StatoOrdineCliente.LAVORATO_PARZIALMENTE));
  }

  private Richiesta newRichiesta() {
    return new Richiesta().identificativo(UUID.randomUUID()
                                              .toString())
                          .tipo(TipoRichiesta.NUOVO_ORDINE)
                          .dataModificaStato(Instant.now());
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().identificativo(IDENTIFICATIVO_ORDINE)
                              .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                              .stato(StatoOrdineCliente.DA_EVADERE);
  }

}
