package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.TrackyCardClient;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;
import it.fai.ms.efservice.service.frejus.ConfermaTesseraFrejus;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.service.fsm.util.FsmByTipoDispositivoService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DeliveryServiceTest {

  static String  ACAR         = "0014113";
  static String  SIMEONI      = "0046348";

  DeliveryService deliveryService;

  DeliveryDeviceService deliveryDeviceService = mock(DeliveryDeviceService.class);

  DeliveryPrintDTO deliveryPrintRequestDTO;

  private FsmRichiestaUtils      fsmRichiestaUtils;
  private ProcessTransitiService transitiService;

  @Autowired
  private FsmByTipoDispositivoService _fsmDeviceTypeService;

  private VehicleClient                    vehicleClient                    = mock(VehicleClient.class);
  private DispositivoRepositoryExt         dispositivoRepository            = mock(DispositivoRepositoryExt.class);
  private RichiestaRepositoryExt           richiestaRepository              = mock(RichiestaRepositoryExt.class);
  private DispositiviDaSpedireCacheService dispositiviDaSpedireCacheService = mock(DispositiviDaSpedireCacheService.class);
  private DocumentClient                   documentClient                   = mock(DocumentClient.class);
  private DispositivoRepositoryExt         dispositivoRepo                  = mock(DispositivoRepositoryExt.class);
  private DispositivoServiceExt            dispositivoServiceExt            = mock(DispositivoServiceExt.class);

  private ConfermaTesseraFrejus                            frejusClient          = mock(ConfermaTesseraFrejus.class);
  private RaggruppamentoRichiesteOrdineFornitoreServiceExt raggruppamentoService = mock(RaggruppamentoRichiesteOrdineFornitoreServiceExt.class);
  private TrackyCardClient                                 trackyCardClient      = mock(TrackyCardClient.class);
  private ClienteFaiRepository                             clienteFaiRepo        = mock(ClienteFaiRepository.class);

  private RichiestaRepository     _richiestaRepo           = mock(RichiestaRepository.class);
  private OrdineClienteServiceExt _ordineClienteServiceExt = mock(OrdineClienteServiceExt.class);
  
  private FsmDispositivo fsmDevice = mock(FsmDispositivo.class);

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    fsmRichiestaUtils = new FsmRichiestaUtils(_fsmDeviceTypeService, _richiestaRepo, richiestaRepository, _ordineClienteServiceExt);

    transitiService = new ProcessTransitiService(raggruppamentoService, trackyCardClient, documentClient, dispositivoServiceExt,
                                                 dispositivoRepo, clienteFaiRepo, fsmRichiestaUtils, frejusClient);

    deliveryService = new DeliveryService(documentClient, transitiService, richiestaRepository, dispositivoRepository,
                                          dispositiviDaSpedireCacheService, vehicleClient, deliveryDeviceService, fsmDevice);
  }

  @Test
  public void printProduttoreException() {
    List<Richiesta> richieste = newRichieste();
    
    List<Dispositivo> devices = richieste.parallelStream().flatMap(r -> r.getDispositivos().stream()).collect(toList());
    
    Dispositivo d = new Dispositivo().stato(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE);
    d.setSeriale("seriale" + System.currentTimeMillis());
    
    devices.add(d);
    
    List<StatoDispositivo> states = new ArrayList<>();
    states.add(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE);
    when(dispositivoRepository.findByStatoIn(states)).thenReturn(devices);

    Instant now = Instant.now();
    DeliveryPrintDTO dto = new DeliveryPrintDTO();
    dto.setSpedizioni(null);
    dto.setDataSpedizione(now);
    dto.setLang("IT");
    try {
    deliveryService.processProduttore(dto);
    } catch (Exception e) {
      assertThat(e).isNotNull();
    }
    verifyZeroInteractions(deliveryDeviceService);
  }

  private List<Richiesta> newRichieste() {
    List<Richiesta> request = new ArrayList<>();
    for (int num = 1; num < 5; num++) {
      Richiesta r = new Richiesta().identificativo("identifier-" + num)
                                   .stato(StatoRichiesta.DISPOSITIVO_DISATTIVATO)
                                   .tipo(TipoRichiesta.MALFUNZIONAMENTO)
                                   .tipoDispositivo(newTipoDispositivo(num));
      ;
      Dispositivo d = new Dispositivo().stato(StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE);
      d.setSeriale("seriale" + num);

      Contratto contract = new Contratto();
      contract.setCodContrattoCliente("contratto-" + num);
      if (num == 4) {
        contract.setCodContrattoCliente("contratto-" + 1);
      }
      d.setContratto(contract);

      AssociazioneDV adv = new AssociazioneDV();
      adv.setDispositivo(d);
      adv.setUuidVeicolo("vehicle" + num);
      Set<AssociazioneDV> associazioneDVS = new HashSet<>();
      associazioneDVS.add(adv);
      d.setAssociazioneDispositivoVeicolos(associazioneDVS);

      Set<Richiesta> richiestas = new HashSet<>();
      d.setRichiestas(richiestas);

      Set<Dispositivo> dispositivos = new HashSet<>();
      d.setTipoDispositivo(newTipoDispositivo(num));
      ClienteFai newClienteFai = newClienteFai(d.getTipoDispositivo());
      contract.setClienteFai(newClienteFai);
      d.setContratto(contract);
      dispositivos.add(d);

      r.setDispositivos(dispositivos);
      request.add(r);
    }
    return request;
  }

  private ClienteFai newClienteFai(TipoDispositivo deviceType) {
    ClienteFai cf = new ClienteFai();
    switch (deviceType.getNome()) {
    case TELEPASS_EUROPEO:
      cf.setRagioneSociale("::SERVICE TRASPORTI SRL::");
      break;
    case TRACKYCARD:
      cf.setRagioneSociale("::ASAM SRL::");
      break;

    default:
      cf.setRagioneSociale("::DEFAULT SPA::");
      break;
    }
    return cf;
  }

  private TipoDispositivo newTipoDispositivo(int num) {
    TipoDispositivo td = new TipoDispositivo();
    TipoDispositivoEnum deviceType = (num % 2 == 0) ? TipoDispositivoEnum.TELEPASS_EUROPEO : TipoDispositivoEnum.TRACKYCARD;
    td.setNome(deviceType);
    td.nomeBusiness(deviceType.name());
    Set<TipoServizio> tipoServizios = new HashSet<>();
    if (num % 2 == 0) {
      tipoServizios.add(newTipoServizio("SERV1"));
      tipoServizios.add(newTipoServizio("SERV2"));
      tipoServizios.add(newTipoServizio("SERV3"));
      tipoServizios.add(newTipoServizio("SERV4"));
    } else {
      tipoServizios.add(newTipoServizio("SERV1"));
      tipoServizios.add(newTipoServizio("SERV3"));
    }
    td.setTipoServizios(tipoServizios);
    td.setProduttore(newProduttore(deviceType));
    return td;
  }

  private Produttore newProduttore(TipoDispositivoEnum deviceType) {
    Produttore p = new Produttore();
    switch (deviceType) {
    case TELEPASS_EUROPEO:
      p.setCodiceFornitoreNav("F_TELEPASS");
      p.setNome("TELEPASS");
      p.setId(1L);
      break;
    case TRACKYCARD:
      p.setCodiceFornitoreNav("F_FAISERVICE");
      p.setNome("FAISERVICE");
      p.setId(2L);
      break;

    default:
      p.setCodiceFornitoreNav("P_NOT_FOUND");
      p.setNome("NOT_FOUND");
      p.setId(8890L);
      break;
    }
    return p;
  }

  private TipoServizio newTipoServizio(String nome) {
    TipoServizio ts = new TipoServizio();
    ts.nome(nome);
    ts.nomeBusiness(nome + "_");
    return ts;
  }

  static String RandomEmail() {
    return RandomStringUtils.random(10, true, true) + "@" + RandomStringUtils.random(8, true, false) + "."
           + RandomStringUtils.random(3, true, false);
  }

  static String RandomString() {
    return RandomStringUtils.random(10, true, true);
  }

  static String RandomString(int count) {
    return RandomStringUtils.random(count, true, true);
  }

  static String RandomUUID() {
    return UUID.randomUUID()
               .toString();
  }

  static void logToJson(Object obj) throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    final String jsonInString = mapper.writeValueAsString(obj);
    System.out.println(jsonInString);
  }

}
