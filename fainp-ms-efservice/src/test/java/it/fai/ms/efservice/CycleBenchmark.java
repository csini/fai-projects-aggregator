package it.fai.ms.efservice;

import org.junit.Test;

import java.util.*;


public class CycleBenchmark {

  List<Integer> intList = Arrays.asList(1,2,3,4,5,6,7,8,9,0);

  @Test
  public void test_on_cycle(){
    System.out.println("Testing 10000000 of elements");
    long t0 = System.currentTimeMillis();
    for (long i = 0; i < 10000000; i++) {
      Collections.sort(new LinkedList(intList));
      Collections.sort(new LinkedList(intList));
    }
    long t1 = System.currentTimeMillis();
    System.out.println("Done in "+ (t1 - t0) +"milliseconds");
  }

  @Test
  public void test_two_cycles(){
    System.out.println("Testing 10000000 of elements");
    long t0 = System.currentTimeMillis();
    for (long i = 0; i < 10000000; i++) {
      Collections.sort(new LinkedList(intList));
    }
    for (long i = 0; i < 10000000; i++) {
      Collections.sort(new LinkedList(intList));
    }
    long t1 = System.currentTimeMillis();
    System.out.println("Done in "+ (t1 - t0) +"milliseconds");
  }


}
