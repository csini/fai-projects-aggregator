package it.fai.ms.efservice.service.fsm.type.gransanbernardo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.ManageDevicesInStorageService;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.gransanbernardo.FsmInoltroGranSanBernardoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroGranSanBernardoTest {
  
  @Configuration
  public static class GranSanBernardoInoltroConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());
    
    private static FsmSenderToQueue senderFsmService = mock(FsmSenderToQueue.class);
    private static ManageDevicesInStorageService deviceInStorageServiceExt = mock(ManageDevicesInStorageService.class);
    private static GeneratorContractCodeService generatorContractCodeService = mock(GeneratorContractCodeService.class);
    
    @Autowired
    private ManageDeviceSentModeService manageDeviceSentModeService;

    @Autowired
    private DeviceProducerService deviceProducerService;
    
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    
    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroGranSanBernardoConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmInoltroGranSanBernardoConfig newGranSanBernardoInoltroConfiguration() {
      FsmInoltroGranSanBernardoConfig fsmInoltroGranSanBernardo = new FsmInoltroGranSanBernardoConfig(senderFsmService, deviceInStorageServiceExt, manageDeviceSentModeService, applicationEventPublisher, deviceProducerService, generatorContractCodeService);
      _log.info("Created FsmInoltroGranSanBernardoConfig for test {}", fsmInoltroGranSanBernardo);
      return fsmInoltroGranSanBernardo;
    }
  }

  private FsmInoltroGranSanBernardo fsmInoltroGranSanBernardo;

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiesta = null;
  private Dispositivo dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
    fsmInoltroGranSanBernardo = (FsmInoltroGranSanBernardo) fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_GRAN_SAN_BERNARDO);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }
  
  private Dispositivo newDispositivo() {
    Dispositivo d = new Dispositivo();
    
    d.setStato(StatoDispositivo.IN_DEPOSITO);
    d.setTipoDispositivo(newTipoDispositivo());
    d.setId(1L);
    
    return d;
  }
  
  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo d = new TipoDispositivo();
    
    d.setId(10000001L);
    d.setModalitaSpedizione(ModalitaSpedizione.DA_DEPOSITO_FAI);
    d.setNome(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);
    
    return d;
  }

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = newDispositivo();
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(newTipoDispositivo())
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmInoltroGranSanBernardo_attivo_evaso() throws Exception {
    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta, false);

    if(richiesta.getStato().equals(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)) {
      richiesta = fsmInoltroGranSanBernardo.executeCommandToChangeState(RichiestaEvent.MU_SPEDIZIONE_COMPLETA, richiesta);
      assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);      
    }
  }

  @Test
  public void FsmInoltroGranSanBernardo_concluso_non_spedito() throws Exception {
    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta, false);
    
    if(richiesta.getStato().equals(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI)) {
      richiesta = fsmInoltroGranSanBernardo.executeCommandToChangeState(RichiestaEvent.MU_CHIUSURA_ORDINE, richiesta);
      assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.CONCLUSO_NON_SPEDITO);      
    }
  }
  
  @Test
  public void FsmInoltroGranSanBernardo_mu_dispositivo_in_deposito() throws Exception {
    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta, true);
    
    if(richiesta.getStato().equals(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO)) {
      richiesta = fsmInoltroGranSanBernardo.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_IN_DEPOSITO, richiesta);
      assertThat(richiesta.getStato()).isIn(StatoRichiesta.SOSPESO_ATTESA_DEPOSITO, StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);      
    }
  }

  @Test
  public void FsmInoltroGranSanBernardo_not_available_command() throws Exception {
    try {
    richiesta = fsmInoltroGranSanBernardo.executeCommandToChangeState(RichiestaEvent.MU_CHIUSURA_ORDINE, richiesta);
    } catch(Exception e) {
      assertThat(e.getMessage()).startsWith("Command MU_CHIUSURA_ORDINE");
    }

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
  }

  private void switchStateAccettatoProntoPerInoltroToDaInoltrare(Richiesta richiesta, boolean sospesoAttesaDeposito) throws Exception {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");
    
    when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt.findDispositiviByStateAndType(StatoDispositivo.IN_DEPOSITO, TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO)).thenReturn(sospesoAttesaDeposito?null:Collections.singleton(newDispositivo()));
    when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt.findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo.IN_DEPOSITO, TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO)).thenReturn(Optional.of(newDispositivo()));
    when(GranSanBernardoInoltroConfiguration.generatorContractCodeService.generateAndSaveContractCode(any(TipoDispositivoEnum.class), any(Contratto.class))).thenReturn(newContratto(newClienteFai));
    
    Mockito.doNothing().when(GranSanBernardoInoltroConfiguration.senderFsmService).sendMessageForChangeStatoDispositivo(any(Dispositivo.class), any(DispositivoEvent.class));
    
    Mockito.doNothing().when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt).removeStatoDispositivoServizios(any(Dispositivo.class));
    Mockito.doNothing().when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt).removeAssociazioneDvs(any(Dispositivo.class));
    when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt.save(any(Dispositivo.class))).thenReturn(newDispositivo());
    Mockito.doNothing().when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt).flushDevice();
    Mockito.doNothing().when(GranSanBernardoInoltroConfiguration.deviceInStorageServiceExt).delete(any(Dispositivo.class));
    
    richiesta = fsmInoltroGranSanBernardo.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isIn(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI, StatoRichiesta.SOSPESO_ATTESA_DEPOSITO);
  }

}
