package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.client.AnagaziendeClient;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.client.dto.IndirizzoSpedizioneDTO;
import it.fai.ms.efservice.config.AbstractCommonTest;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.enumeration.StateCreationRichiesta;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ContrattoRepository;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RichiestaModificaServiceExt;
import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.async.CreationOrdineAsyncService;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.impl.IndirizzoSpedizioneOrdiniServiceImplExt;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the RichiestaResource REST controller.
 *
 * @see RichiestaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, RichiestaResourceExtTest.RichiestaResourceConfiguration.class })
public class RichiestaResourceExtTest extends AbstractCommonTest {

  @Configuration
  public static class RichiestaResourceConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    @Autowired
    private IndirizzoSpedizioneOrdiniRepository repositoryIndirizzi;

    private AnagaziendeClient anagAziendeMock = mock(AnagaziendeClient.class);

    @PostConstruct
    public void init() {
      IndirizziDiSpedizioneDTO indirizziDTO = new IndirizziDiSpedizioneDTO();
      indirizziDTO.setFinale(newIndirizzoSpedizioneDTO());
      indirizziDTO.setTransito(newIndirizzoSpedizioneDTO());
      when(anagAziendeMock.getIndirizzoSpedizioneClienteFai(anyString(), anyString())).thenReturn(indirizziDTO);
      _log.debug("Test configuration for AnagaziendeClient");
    }

    private IndirizzoSpedizioneDTO newIndirizzoSpedizioneDTO() {
      IndirizzoSpedizioneDTO indirizzoSpedizioneDTO = new IndirizzoSpedizioneDTO();

      String randString = UUID.randomUUID()
                              .toString();
      indirizzoSpedizioneDTO.setVia(randString);
      indirizzoSpedizioneDTO.setCitta(randString);
      indirizzoSpedizioneDTO.setCap(randString);
      indirizzoSpedizioneDTO.setProvincia(randString);
      indirizzoSpedizioneDTO.setPaese(randString);
      indirizzoSpedizioneDTO.setDestinatario(randString);
      indirizzoSpedizioneDTO.setFulladdress(randString);
      indirizzoSpedizioneDTO.indirizzoFai(false);

      return indirizzoSpedizioneDTO;
    }

    @Bean
    @Primary
    public IndirizzoSpedizioneOrdiniServiceImplExt newIndirizzoSpedizioneOrdiniServiceExt() {
      IndirizzoSpedizioneOrdiniServiceImplExt indirizzoSpedizioneServiceExt = new IndirizzoSpedizioneOrdiniServiceImplExt(repositoryIndirizzi,
                                                                                                                          anagAziendeMock,
                                                                                                                          "token");
      _log.info("Created IndirizzoSpedizioneOrdiniServiceImplExt for test {}", indirizzoSpedizioneServiceExt);
      return indirizzoSpedizioneServiceExt;
    }
  }

  private static final String CODICE_CLIENTE_FAI = "0046348";

  private static final String tipoDispositivoDefault = "TELEPASS_EUROPEO";

  private static final String PEDAGGIO_ITALIA     = "PEDAGGI_ITALIA";
  private static final String PEDAGGIO_POLONIA    = "PEDAGGI_POLONIA";
  private static final String PEDAGGIO_FRANCIA    = "PEDAGGI_FRANCIA";
  private static final String PEDAGGIO_PORTOGALLO = "PEDAGGI_PORTOGALLO";

  private static final String PEDAGGIO_AUSTRIA = "PEDAGGI_AUSTRIA";
  private static final String LAVAGGI          = "LAVAGGI";
  private static final String CARBURANTI       = "CARBURANTI";

  private static final String tipoServizioDefault = PEDAGGIO_ITALIA;

  private static final String veicolo1 = "AA000AA";
  // private static final String veicolo2 = "AB001AA";
  private static final String veicolo3 = "AC002AA";
  // private static final String veicolo4 = "AD003AA";
  private static final String veicolo5 = "AE004AA";
  private static final String veicolo6 = "AF005AA";

  private static final String PATH_API_CREATE_ORDER = "/api/public/richiesta";

  private static final String PATH_API_CREATE_CONTRATTO = "/api/public/contrattoforrichiesta";

  private static final String PATH_API_GET_ORDINE = "/api/public/getrichiesta";

  private static final String PATH_API_GET_ORDINE_BY_RICHIESTE = "/api/public/getordinebyrichieste";

  private static final String API_POLLING = "/api/public/richiesta/polling";

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private RichiestaServiceExt richiestaServiceExt;

  @Autowired
  private RichiestaService richiestaService;

  @Autowired
  private ContrattoRepository contrattoRepository;

  @Autowired
  private CreationOrdineAsyncService creationOrdineAsyncService;

  @Autowired
  private AsyncPollingJobServiceImpl asyncJobService;

  @Autowired
  private RichiestaModificaServiceExt richiestaModificaService;

  @Autowired
  private AssociazioneDVRepository associazioneDvRepo;

  @Autowired
  private DispositivoServiceExt dispositivoServiceExt;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  private MockMvc restRichiestaMockMvc;

  CarrelloDTO carrello;

  List<Long> idOrderCreated = new ArrayList<>();

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    List<AssociazioneDV> findAll = associazioneDvRepo.findAll();
    findAll.stream()
           .map(a -> {
             a = associazioneDvRepo.saveAndFlush(a.dispositivo(null));
             associazioneDvRepo.delete(a.getId());
             return true;
           })
           .collect(toSet());

    final RichiestaResourceExt richiestaResourceExt = new RichiestaResourceExt(richiestaServiceExt, richiestaService,
                                                                               creationOrdineAsyncService, asyncJobService,
                                                                               richiestaModificaService);
    this.restRichiestaMockMvc = MockMvcBuilders.standaloneSetup(richiestaResourceExt)
                                               .alwaysDo(MockMvcResultHandlers.print())
                                               .setCustomArgumentResolvers(pageableArgumentResolver)
                                               .setControllerAdvice(exceptionTranslator)
                                               .setMessageConverters(jacksonMessageConverter)
                                               .build();

    carrello = new CarrelloDTO();
    carrello.setCodiceAzienda(CODICE_CLIENTE_FAI);
    HashMap<String, String> datiAggiuntivi = new HashMap<>();
    datiAggiuntivi.put("costo", "1500");
    carrello.setDatiAggiuntivi(datiAggiuntivi);
    carrello.setTipoServizio(tipoServizioDefault);
    carrello.setUuidVeicoli(createUuidVeicoli(new ArrayList<String>(Arrays.asList(veicolo1, veicolo3, veicolo5))));
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(veicolo1, new ArrayList<String>(Arrays.asList(PEDAGGIO_FRANCIA, PEDAGGIO_POLONIA)));
    mappaVeicoli.put(veicolo3, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA, PEDAGGIO_PORTOGALLO)));
    mappaVeicoli.put(veicolo6, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrello.setDispositivi(createDispositivi(tipoDispositivoDefault, mappaDispositiviVeicoli));
  }

  @After
  @Transactional
  public void delete() {

    String codiceAzienda = "0046276";
    List<Contratto> findAll = contrattoRepository.findAll();
    for (Contratto contratto : findAll) {
      if (contratto.getClienteFai()
                   .getCodiceCliente()
                   .equalsIgnoreCase(codiceAzienda)) {
        Set<Dispositivo> dispositiviByContratto = dispositivoServiceExt.findDispositiviByCodiceContratto(contratto.getCodContrattoCliente());
        dispositiviByContratto.stream()
                              .map(d -> {
                                try {
                                  return dispositivoServiceExt.saveAndFlush(d.contratto(null));
                                } catch (Exception e) {
                                  throw new RuntimeException(e);
                                }
                              })
                              .collect(toSet());
        List<Richiesta> richiesteByContratto = richiestaServiceExt.findByCodiceId(contratto.getId());
        richiesteByContratto.stream()
                            .map(r -> {
                              return richiestaServiceExt.saveAndFlush(r.contratto(null));
                            })
                            .collect(toSet());

        contratto.setDispositivo(null);
        contratto = contrattoRepository.saveAndFlush(contratto);
        contrattoRepository.delete(contratto);
      }
    }

    List<Richiesta> richiestaList = richiestaRepositoryExt.findAll();
    for (Richiesta richiesta : richiestaList) {
      richiestaService.delete(richiesta.getId());
    }
  }

  @Test
  @Transactional
  public void generateRichiesteByCarrello() throws Exception {
    List<Richiesta> richieste = richiestaServiceExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrello);
    assertThat(richieste).isNotNull();
    assertThat(richieste.size()).isGreaterThanOrEqualTo(1);

    richieste.stream()
             .map(r -> {
               richiestaService.delete(r.getId());
               return true;
             })
             .collect(toSet());
    ;
  }

  @Test
  @Transactional
  public void generateRichiestaLumesiaCardPedaggiAustriaByCarrello() throws Exception {
    HashMap<Integer, List<String>> mappaServizi = new HashMap<>();
    mappaServizi.put(0, new ArrayList<String>(Arrays.asList(PEDAGGIO_AUSTRIA, CARBURANTI, LAVAGGI)));
    mappaServizi.put(1, new ArrayList<String>(Arrays.asList(PEDAGGIO_AUSTRIA, CARBURANTI)));
    mappaServizi.put(2, new ArrayList<String>(Arrays.asList(CARBURANTI, LAVAGGI)));
    
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(veicolo1, mappaServizi.get(0));
    mappaVeicoli.put(veicolo3, mappaServizi.get(1));
    mappaVeicoli.put(veicolo6, mappaServizi.get(2));
    
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrello.setDispositivi(createDispositivi(TipoDispositivoEnum.TRACKYCARD.name(), mappaDispositiviVeicoli));

    List<Richiesta> richieste = richiestaServiceExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrello);
    
    assertThat(richieste).isNotNull();
    assertThat(richieste.size()).isGreaterThanOrEqualTo(1);
    
    List<Dispositivo> devices = richieste.stream().map(r -> r.getDispositivos().stream().findFirst().get()).collect(toList());
    System.out.println("Devices size= " + devices.size());
    int count = 1;
    for (Dispositivo device : devices) {
      Set<StatoDispositivoServizio> statoDispositivoServizios = device.getStatoDispositivoServizios();
      assertThat(statoDispositivoServizios).isNotEmpty();
      
      List<String> servizi = statoDispositivoServizios.stream().map(sds -> sds.getTipoServizio().getNome()).collect(toList());
      System.out.println(servizi);
      assertThat(servizi).containsAll(mappaServizi.get(count));
      count--;
    }

    richieste.stream()
             .map(r -> {
               richiestaService.delete(r.getId());
               return true;
             })
             .collect(toSet());
  }

  private void createRichiestaByCarrelloDTO() throws Exception {
    String identificativoOrdine = "";
    int databaseSizeBeforeCreate = richiestaRepositoryExt.findAll()
                                                         .size();

    MvcResult result = restRichiestaMockMvc.perform(post(PATH_API_CREATE_ORDER).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                               .content(TestUtil.convertObjectToJsonBytes(carrello)))
                                           .andExpect(status().isOk())
                                           .andReturn();

    Map<String, Object> resultMap = getResultMap(result);
    identificativoOrdine = (String) resultMap.get("richiesta");

    pollingRichiesta(identificativoOrdine);

    // Validate the Richiesta in the database
    List<Richiesta> richiestaList = findAllRichiesteNewTransaction();
    int sizeOrdini = richiestaList.size();
    assertThat(sizeOrdini).isGreaterThan(databaseSizeBeforeCreate);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private List<Richiesta> findAllRichiesteNewTransaction() {
    List<Richiesta> richiestaList = richiestaRepositoryExt.findAll();
    return richiestaList;
  }

  @Test
  public void test() throws Exception {
    createRichiestaByCarrelloDTO();
    assertTrue(true);
  }

  @Test
  @Transactional
  public void createRichiestaDispositivoByCarrelloDTO() throws Exception {
    MvcResult result = restRichiestaMockMvc.perform(post(PATH_API_CREATE_ORDER).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                               .content(TestUtil.convertObjectToJsonBytes(carrello)))
                                           .andExpect(status().isOk())
                                           .andReturn();

    Map<String, Object> resultMap = getResultMap(result);
    String identificativoOrdine = (String) resultMap.get("richiesta");

    pollingRichiesta(identificativoOrdine);

    int numDispositivi = 2;
    List<Richiesta> richiestaList = richiestaRepositoryExt.findAll();

    Richiesta richiesta = null;
    for (int i = 1; i <= numDispositivi; i++) {
      richiesta = richiestaList.get(richiestaList.size() - i);
      Set<Dispositivo> dispositivos = richiesta.getDispositivos();
      assertThat(dispositivos).hasSize(1);
    }

    RichiestaDTO richiestaDTO = richiestaService.findOne(richiesta.getId());
    assertThat(richiestaDTO.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
  }

  @Test
  @Transactional
  public void createRichiestaByCarrelloDTO_001() throws Exception {
    // Generate exception validation on CarrelloDTO;

    carrello = new CarrelloDTO();

    restRichiestaMockMvc.perform(post(PATH_API_CREATE_ORDER).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                            .content(TestUtil.convertObjectToJsonBytes(carrello)))
                        .andExpect(status().is4xxClientError());

  }

  @Test
  public void createRichiestaByCarrelloDTO_002() throws Exception {
    // Generate exception generation richiesta by carrelloDTO;
    List<DispositivoCarrelloDTO> dispositivi = carrello.getDispositivi();
    for (DispositivoCarrelloDTO dispositivoCarrelloDTO : dispositivi) {
      // Set Servizio not available
      dispositivoCarrelloDTO.setTipoDispositivo("TELEPASS");
    }
    carrello.setDispositivi(dispositivi);

    MvcResult returnValue = restRichiestaMockMvc.perform(post(PATH_API_CREATE_ORDER).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                    .content(TestUtil.convertObjectToJsonBytes(carrello)))
                                                .andExpect(status().is2xxSuccessful())
                                                .andReturn();

    Map<String, Object> resultMap = getResultMap(returnValue);
    String identificativo = (String) resultMap.get("richiesta");
    pollingRichiestaState(identificativo, StateCreationRichiesta.ERROR);
  }

  @Test
  public void createContrattoForRichiesta_001() throws Exception {
    int richiestaBeforeTest = richiestaRepositoryExt.findAll()
                                                    .size();
    // codice azienda relativo a "A & G TRASPORTI SRL"
    carrello.setCodiceAzienda("0046276");
    createRichiestaByCarrelloDTO();

    String codiceContratto = UUID.randomUUID()
                                 .toString();

    Richiesta richiesta = findAllRichiesteNewTransaction().get(richiestaBeforeTest + 1);
    restRichiestaMockMvc.perform(put(PATH_API_CREATE_CONTRATTO + "/" + codiceContratto + "/" + richiesta.getIdentificativo()))
                        .andExpect(status().is2xxSuccessful());

    List<Contratto> contratti = findAllContratto();

    Contratto contratto = contratti.get(contratti.size() - 1);
    String codContrattoCliente = contratto.getCodContrattoCliente();
    assertThat(codContrattoCliente).isEqualTo(codiceContratto);

    int richiesteAfterTest = findAllRichiesteNewTransaction().size();
    int numRichiesteCreated = richiesteAfterTest - richiestaBeforeTest;
    System.out.println("Numero richieste create: " + numRichiesteCreated);
    for (int i = 0; i < numRichiesteCreated; i++) {
      Richiesta r = findAllRichiesteNewTransaction().get(richiestaBeforeTest + i);
      saveRichiestaWithNewTransaction(r.contratto(null));
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private List<Contratto> findAllContratto() {
    return contrattoRepository.findAll();
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private Richiesta saveRichiestaWithNewTransaction(Richiesta richiesta) {
    return richiestaServiceExt.saveAndFlush(richiesta);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private void saveAndDeleteContrattoWithNewTransaction(Contratto contratto) {
    contratto.setDispositivo(null);
    contratto = contrattoRepository.save(contratto.richiestas(null));
    contrattoRepository.delete(contratto);
  }

  @Test
  public void getRichiestaApiTest() throws Exception {
    createRichiestaByCarrelloDTO();

    List<Richiesta> richiestaList = richiestaRepositoryExt.findAll();
    Richiesta richiesta = richiestaList.get(richiestaList.size() - 1);

    String identificativo = richiesta.getIdentificativo();

    MvcResult result = restRichiestaMockMvc.perform(get(PATH_API_GET_ORDINE + "/" + identificativo))
                                           .andExpect(status().is2xxSuccessful())
                                           .andReturn();

    Map<String, Object> resultMap = getResultMap(result);
    String identificativoRichiesta = (String) resultMap.get("identificativo");
    assertThat(identificativoRichiesta).isEqualTo(identificativo);

    richiestaService.delete(richiesta.getId());
  }

  @Test
  public void getOrdineDTOByRichieste() throws Exception {
    createRichiestaByCarrelloDTO();

    List<Richiesta> richiestaList = findAllRichiesteNewTransaction();
    int size = richiestaList.size();
    Richiesta richiesta = richiestaList.get(size - 1);
    String identificativo = richiesta.getIdentificativo();
    MvcResult result = restRichiestaMockMvc.perform(get(PATH_API_GET_ORDINE_BY_RICHIESTE + "/" + identificativo))
                                           .andExpect(status().isOk())
                                           .andReturn();

    assertThat(result).isNotNull();
  }

  private List<DispositivoCarrelloDTO> createDispositivi(String tipoDispositivo,
                                                         HashMap<Integer, HashMap<String, List<String>>> mapDispositivi) {
    List<DispositivoCarrelloDTO> dispositivi = new ArrayList<>();

    Set<Integer> keySet = mapDispositivi.keySet();
    Iterator<Integer> iterator = keySet.iterator();

    while ((iterator != null) && iterator.hasNext()) {
      Integer keyDispositivo = iterator.next();
      HashMap<String, List<String>> veicoliCarrello = mapDispositivi.get(keyDispositivo);
      DispositivoCarrelloDTO dispositivoCarrello = new DispositivoCarrelloDTO();
      dispositivoCarrello.setTipoDispositivo(tipoDispositivo);
      dispositivoCarrello.setVeicoli(createVeicoliCarrello(veicoliCarrello));
      dispositivi.add(dispositivoCarrello);
    }

    return dispositivi;
  }

  private List<VeicoloCarrelloDTO> createVeicoliCarrello(HashMap<String, List<String>> map) {
    List<VeicoloCarrelloDTO> listVeicolo = new ArrayList<>();
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while ((iterator != null) && iterator.hasNext()) {
      String keyVeicolo = iterator.next();
      List<String> listServizi = map.get(keyVeicolo);
      VeicoloCarrelloDTO veicolo = new VeicoloCarrelloDTO();
      veicolo.setUuid(keyVeicolo);
      veicolo.setNomeTipiServizio(createNomeServizio(listServizi));
      listVeicolo.add(veicolo);
    }
    return listVeicolo;
  }

  private List<String> createNomeServizio(List<String> servizi) {
    List<String> nomeServizio = new ArrayList<>();

    for (String servizio : servizi) {
      nomeServizio.add(servizio);
    }
    return nomeServizio;
  }

  private List<String> createUuidVeicoli(List<String> listVeicoli) {
    List<String> uuidVeicoli = new ArrayList<>();
    for (String veicolo : listVeicoli) {
      uuidVeicoli.add(veicolo);
    }
    return uuidVeicoli;
  }

  private Map<String, Object> getResultMap(MvcResult returnValue) throws JsonProcessingException, IOException {
    String responseValue = returnValue.getResponse()
                                      .getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    JsonNode json = mapper.readTree(responseValue);
    @SuppressWarnings("unchecked")
    Map<String, Object> resultMap = mapper.convertValue(json, Map.class);
    return resultMap;
  }

  private void pollingRichiesta(String identificativoOrdine) throws Exception {

    boolean created = false;
    int numTentativi = 5;
    int count = 0;

    while (!created) {
      System.out.println("Count: " + count + " - Polling: " + identificativoOrdine);
      Thread.sleep(800);
      MvcResult resultPolling = restRichiestaMockMvc.perform(get(API_POLLING + "/" + identificativoOrdine))
                                                    .andExpect(status().isOk())
                                                    .andReturn();
      Map<String, Object> resultMap = getResultMap(resultPolling);
      String statoRichiesta = (String) resultMap.get("state");
      StateCreationRichiesta state = StateCreationRichiesta.valueOf(statoRichiesta);
      switch (state) {
      case SUCCESS:
        created = true;
        break;

      case PENDING:
        System.out.println("State Creation Richiesta [" + identificativoOrdine + "]:" + state.name());
        break;
      default:
        throw new Exception("State Creation Richiesta is in ERROR....");
      }

      if (count >= numTentativi) {
        System.out.println("Last state creation: " + state);
        throw new Exception("Numero tentativi superato. Deve esserci stato un errore sulla creazione delle richieste.");
      }

      count++;
    }

  }

  private void pollingRichiestaState(String identificativoOrdine, StateCreationRichiesta stateExpected) throws Exception {
    boolean created = false;
    int numTentativi = 10;
    int count = 0;
    Map<String, Object> resultMap = new HashMap<>();
    while (!created) {
      Thread.sleep(1000);
      MvcResult resultPolling = restRichiestaMockMvc.perform(get(API_POLLING + "/" + identificativoOrdine))
                                                    .andExpect(status().isOk())
                                                    .andReturn();
      resultMap = getResultMap(resultPolling);
      String statoRichiesta = (String) resultMap.get("state");
      StateCreationRichiesta state = StateCreationRichiesta.valueOf(statoRichiesta);
      if (state == stateExpected) {
        created = true;
      } else {
        System.out.println("State Creation Richiesta: " + state.name());
      }

      if (count >= numTentativi) {
        throw new Exception("Numero tentativi superato. Deve esserci stato un errore sulla creazione delle richieste.");
      }
      count++;
    }
  }

}
