package it.fai.ms.efservice.service.impl;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Richiesta;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class OrderRequestServiceImplTestIT {

  @Autowired
  private OrderRequestService orderRequestService;

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testGetOrderRequestsWithoutContract() {
    final List<Richiesta> orderRequests = orderRequestService.getOrderRequestsWithoutContract("0086736");
    System.out.println(orderRequests);
  }

}
