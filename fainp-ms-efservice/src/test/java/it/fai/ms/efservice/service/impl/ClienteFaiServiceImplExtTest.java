package it.fai.ms.efservice.service.impl;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ClienteFaiServiceImplExtTest {
  
  //SIMEONI TRASPORTI SRL
  private static final String CODICE_AZIENDA = "0046348";
  
  @Autowired
  private ClienteFaiServiceExt clienteFaiServiceExt;

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void test() {
    Set<Dispositivo> dispositiviTelepassEu = clienteFaiServiceExt.findDispositivoByClienteAndTipoDispositivo(CODICE_AZIENDA, TipoDispositivoEnum.TELEPASS_EUROPEO);
    
    System.out.println(dispositiviTelepassEu.size());
    
  }

}
