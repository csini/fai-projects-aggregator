package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.FsmManageDeviceSent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ManageDeviceSentModeServiceTest {

  @Autowired
  private ManageDeviceSentModeService serviceDeviceSent;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  private Richiesta r;

  @Before
  public void setUp() {
    OrdineCliente oc = new OrdineCliente().destinazioneFai(false);
    r = new Richiesta().ordineCliente(oc);
  }

  @Test
  public void deviceSentModeTelepassItaTest() {
    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TELEPASS_ITALIANO);
    r.setTipoDispositivo(tipoDispositivo);
    boolean toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.FAI);
    assertThat(toBeSentAt).isTrue();
  }

  @Test
  public void deviceSentModeTelepassEuTest() {
    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    r.setTipoDispositivo(tipoDispositivo);
    boolean toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.FAI);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isTrue();

    r.getOrdineCliente()
     .setDestinazioneFai(true);
    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isTrue();
  }

  @Test
  public void deviceSentModeTelepassEuSatTest() {
    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT);
    r.setTipoDispositivo(tipoDispositivo);
    boolean toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.FAI);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isTrue();

    r.getOrdineCliente()
     .setDestinazioneFai(true);
    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isTrue();
  }

  @Test
  public void deviceSentModeViaCardTest() {
    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.VIACARD);
    r.setTipoDispositivo(tipoDispositivo);

    boolean toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.FAI);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isTrue();
  }

  @Test
  public void deviceSentModeTrackyCardTest() {
    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TRACKYCARD);
    r.setTipoDispositivo(tipoDispositivo);

    boolean toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.FAI);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.CLIENTE);
    assertThat(toBeSentAt).isFalse();

    toBeSentAt = serviceDeviceSent.isToBeSentAt(r, FsmManageDeviceSent.TRANSITO);
    assertThat(toBeSentAt).isTrue();
  }

}
