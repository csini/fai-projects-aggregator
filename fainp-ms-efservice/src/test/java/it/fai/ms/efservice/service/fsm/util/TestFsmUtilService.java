/**
 * 
 */
package it.fai.ms.efservice.service.fsm.util;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.security.SecurityUtils;
import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.testdomain.TestFSM;

/**
 * @author Luca Vassallo
 */
@Service
public class TestFsmUtilService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public boolean switchToState(Message<EventTest> msg, StateTest toState, Transition<StateTest, EventTest> transition) {
    // Collection<Transition<String, String>> transitions = stateMachine.getTransitions();

    MessageHeaders headers = msg.getHeaders();
    boolean isSwitch = switchToState(headers, toState);
    if (isSwitch) {
      Trigger<StateTest, EventTest> trigger = transition.getTrigger();
      boolean isAutoTransition = false;
      if (trigger == null) {
        isAutoTransition = true;
      }

      sendNotificationByTransitionType(isAutoTransition, headers, transition);
    } else {
      return false;
    }

    return true;
  }

  private void sendNotificationByTransitionType(boolean isAutoTx, MessageHeaders headers, Transition<StateTest, EventTest> transition) {
    log.info("Invio Notifica di cambio stato " + ((isAutoTx) ? "AUTOMATICO" : "MANUALE") + ".");
    Integer idObj = null;
    String ultimaNota = null;
    StateTest stateFrom = null;
    StateTest stateTo = null;
    // TODO manca l'identificativo dell'operatore FAI;

    Object object = headers.get("itemTest");
    if (object instanceof TestFSM) {
      TestFSM objTest = (TestFSM) object;

      idObj = objTest.getId();
      ultimaNota = objTest.getUltimaNota();
      stateFrom = transition.getSource()
                            .getId();
      stateTo = transition.getTarget()
                          .getId();
    }

    if (isAutoTx) {
      sendNotification(idObj, new Date(), stateFrom, stateTo);
    } else {
      String faiOperator = SecurityUtils.getCurrentUserLogin();
      sendNotification(idObj, new Date(), stateFrom, stateTo, ultimaNota, faiOperator);
    }
  }

  private void sendNotification(Integer idObj, Date date, StateTest stateFrom, StateTest stateTo) {
    sendNotification(idObj, date, stateFrom, stateTo, null, null);
  }

  private void sendNotification(Integer idObj, Date date, StateTest stateFrom, StateTest stateTo, String ultimaNota, String operatoreFai) {
    log.info("INFO TRANSITION:\n\t- idObj: " + idObj + "\n\t- Data: " + new Date() + "\n\t- Transizione => Da: [" + stateFrom.name()
             + "] A: [" + stateTo.name() + "]\n\t- UltimaNota: " + ultimaNota + "\n\t- OperatoreFai: " + operatoreFai);
  }

  private boolean switchToState(Map<String, Object> headers, StateTest toState) {
    Object object = headers.get("itemTest");
    boolean isValidEntity = validateEntity(object);
    if (isValidEntity) {
      if (object instanceof TestFSM) {
        TestFSM entityFsm = (TestFSM) object;
        entityFsm.setStato(toState.name());
        setUltimaNota(headers, entityFsm);
      } else {
        log.error("Object class is " + object.getClass() + " is not " + TestFSM.class.getName());
        return false;
      }
    } else {
      return false;
    }

    return true;
  }

  private void setUltimaNota(Map<String, Object> headers, TestFSM entityFsm) {
    Object object = headers.get("nota");
    if (object != null) {
      if (object instanceof String) {
        String nota = (String) object;
        entityFsm.storeUltimaNota(nota);
      }
    } else {
      log.error("Not set Ultima Nota");
    }

  }

  private boolean validateEntity(Object obj) {
    if (obj == null) {
      log.error("Entity to change status is null");
      return false;
    }
    return true;
  }

}
