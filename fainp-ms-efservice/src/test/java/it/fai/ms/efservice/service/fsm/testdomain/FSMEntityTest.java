package it.fai.ms.efservice.service.fsm.testdomain;

public interface FSMEntityTest {

  public abstract void setIdentificativo(String identificativo);

  public abstract String getIdentificativo();

  public abstract Enum<?> findFsmStatus();

  public abstract void storeFsmStatus(Enum<?> fsmStatus);

  public abstract void storeUltimaNota(String ultimaNota);

}
