package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class GeneratorContractCodeServiceTest {

  private final String DEFAULT_CLIENT_CODE = "1288";

  private ContrattoRepositoryExt repo = mock(ContrattoRepositoryExt.class);

  private GeneratorContractCodeService service;

  private Contratto contract;

  private ClienteFai clienteFai;

  @Before
  public void setUp() {
    service = new GeneratorContractCodeService(repo);
    contract = new Contratto();
    clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente(DEFAULT_CLIENT_CODE);
    contract.setClienteFai(clienteFai);
  }

  @Test
  public void generateContractTrackyCard() {
    String contractCode = contract.getClienteFai()
                                  .getCodiceCliente();
    contractCode = addZeroLeftPad(contractCode);
    String generatedContractCode = service.generateContractCode(TipoDispositivoEnum.TRACKYCARD, contract);
    assertThat(generatedContractCode).isEqualTo(contractCode);
  }

  @Test(expected = RuntimeException.class)
  public void generateContractTrackyCardFailed() {
    contract.getClienteFai()
            .setCodiceCliente("123456789");
    String contractCode = contract.getClienteFai()
                                  .getCodiceCliente();
    contractCode = addZeroLeftPad(contractCode);
    String generatedContractCode = service.generateContractCode(TipoDispositivoEnum.TRACKYCARD, contract);
    assertThat(generatedContractCode).isEqualTo(contractCode);
  }

  @Test
  public void generateContractGoBox() {
    String contractCode = contract.getClienteFai()
                                  .getCodiceCliente();
    contractCode = addZeroLeftPad(contractCode);
    String generatedContractCode = service.generateContractCode(TipoDispositivoEnum.GO_BOX, contract);
    assertThat(generatedContractCode).isEqualTo(contractCode);
  }

  @Test(expected = RuntimeException.class)
  public void generateContractGoBoxFailed() {
    contract.getClienteFai()
            .setCodiceCliente("123456789");
    String contractCode = contract.getClienteFai()
                                  .getCodiceCliente();
    contractCode = addZeroLeftPad(contractCode);
    String generatedContractCode = service.generateContractCode(TipoDispositivoEnum.GO_BOX, contract);
    assertThat(generatedContractCode).isEqualTo(contractCode);
  }

  @Test
  public void generateContractDeviceTypeNotFound() {
    String contractCode = contract.getClienteFai()
                                  .getCodiceCliente();
    contractCode = addZeroLeftPad(contractCode);
    String generatedContractCode = service.generateContractCode(TipoDispositivoEnum.TELEPASS_EUROPEO, contract);
    assertThat(generatedContractCode).isNotEqualTo(contractCode);
    assertThat(generatedContractCode).isNull();
  }

  private String addZeroLeftPad(String contractCode) {
    int length = contractCode.length();
    if (contractCode.length() < 8) {
      int diff = 8 - length;
      for (int i = 0; i < diff; i++) {
        contractCode = "0" + contractCode;
      }
    }
    return contractCode;
  }

}
