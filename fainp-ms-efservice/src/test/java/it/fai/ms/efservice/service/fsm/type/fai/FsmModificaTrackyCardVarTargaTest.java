package it.fai.ms.efservice.service.fsm.type.fai;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.fai.FsmModificaTrackyCardVarTargaConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceServiceActivationMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmModificaTrackyCardVarTargaTest {

  @Configuration
  public static class TrackyCardVarTargaConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FsmSenderToQueue fsmSender;

    private static DeviceServiceActivationMessageProducerService deviceServiceActivationService = mock(DeviceServiceActivationMessageProducerService.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmModificaTrackyCardVarTargaConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmModificaTrackyCardVarTargaConfig newTrackyCardVarTargaConfiguration() {
      FsmModificaTrackyCardVarTargaConfig fsmTrackyCard = new FsmModificaTrackyCardVarTargaConfig(fsmSender,
                                                                                                  deviceServiceActivationService);
      _log.info("Created {} for test {}", fsmTrackyCard.getClass()
                                                       .getSimpleName(),
                fsmTrackyCard);
      return fsmTrackyCard;
    }
  }

  @Autowired
  private GeneratorContractCodeService generatorContractCode;

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta       richiesta;
  private Contratto       contratto;
  private ClienteFai      clienteFai;
  private TipoDispositivo deviceType;
  private Produttore      produttore;
  private OrdineCliente   ordine;

  private AbstractFsmRichiesta fsm;

  @Before
  public void setUp() {
    cache.clearCache();

    String formattedDate = LocalDateTime.now()
                                        .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    String identificativo = "request-" + formattedDate;
    Long idRichiesta = Long.parseLong(formattedDate);

    newDeviceType();
    newProduttore();
    newClienteFai();
    newContratto();
    newOrdineCliente(formattedDate);

    newRichiesta(idRichiesta, identificativo);
  }

  @Test
  @Transactional
  public void variazioneTarga_test() throws Exception {
    setTypeRequestAndState(richiesta, TipoRichiesta.VARIAZIONE_TARGA, StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    executeSimpleTest();
  }

  @Test
  @Transactional
  public void mezzoRitargato_test() throws Exception {
    setTypeRequestAndState(richiesta, TipoRichiesta.MEZZO_RITARGATO, StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    executeCompleteTest();
  }

  @Test
  @Transactional
  public void malfunzionamentoConSostituzione_test() throws Exception {
    setTypeRequestAndState(richiesta, TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE, StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY);
    executeCompleteTest();
  }

  private void executeSimpleTest() throws Exception {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TRACKY);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta).isNotNull();
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, richiesta);
    assertThat(richiesta).isNotNull();
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
  }

  private void executeCompleteTest() throws Exception {
    executeSimpleTest();

    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_LETTERA_VETTURA, richiesta);
    assertThat(richiesta).isNotNull();
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  private void newDeviceType() {
    deviceType = new TipoDispositivo();
    deviceType.setNome(TipoDispositivoEnum.TRACKYCARD);
    deviceType.setId(88L);
  }

  private void newProduttore() {
    produttore = new Produttore();
    produttore.setNome("FAI");
    produttore.addTipoDispositivo(deviceType);
  }

  private void newClienteFai() {
    clienteFai = new ClienteFai().codiceCliente("codcli");
  }

  private void newContratto() {
    contratto = new Contratto().dataModificaStato(Instant.now())
                               .produttore(produttore)
                               .clienteFai(clienteFai);

    String generatedContractCode = generatorContractCode.generateContractCode(deviceType.getNome(), contratto);
    contratto.setCodContrattoCliente(generatedContractCode);
  }

  private Dispositivo newDispositivo(String identificativo) {
    Dispositivo device = new Dispositivo().dataModificaStato(Instant.now())
                                          .seriale(identificativo)
                                          .stato(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
    return device;
  }

  private void newOrdineCliente(String formattedDate) {
    ordine = new OrdineCliente().clienteAssegnatario(clienteFai)
                                .identificativo("ORD-" + formattedDate)
                                .numeroOrdine(formattedDate + "-1")
                                .stato(StatoOrdineCliente.DA_EVADERE);

  }

  private void newRichiesta(Long idRichiesta, String identificativo) {
    richiesta = new Richiesta();
    richiesta.identificativo(identificativo)
             .tipo(TipoRichiesta.NUOVO_ORDINE)
             .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_TRACKY)
             .data(Instant.now())
             .dataModificaStato(Instant.now())
             .tipoDispositivo(deviceType)
             .contratto(contratto)
             .ordineCliente(ordine);
    Dispositivo device = newDispositivo(identificativo).contratto(contratto);
    richiesta.addDispositivo(device);
    richiesta.setId(idRichiesta);
  }

  private void setTypeRequestAndState(Richiesta richiesta2, TipoRichiesta requestType, StatoRichiesta stato) {
    richiesta.setTipo(requestType);
    richiesta.setStato(stato);
    richiesta.setDataModificaStato(Instant.now());
  }

}
