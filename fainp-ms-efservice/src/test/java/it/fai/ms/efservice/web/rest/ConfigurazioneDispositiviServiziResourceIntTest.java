package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ConfigurazioneDispositiviServizi;
import it.fai.ms.efservice.domain.enumeration.ModalitaGestione;
import it.fai.ms.efservice.repository.ConfigurazioneDispositiviServiziRepository;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziQueryService;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ConfigurazioneDispositiviServiziResource REST controller.
 *
 * @see ConfigurazioneDispositiviServiziResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ConfigurazioneDispositiviServiziResourceIntTest {

  private static final ModalitaGestione DEFAULT_MODALITA_GESTIONE = ModalitaGestione.DA_REMOTO;
  private static final ModalitaGestione UPDATED_MODALITA_GESTIONE = ModalitaGestione.OFFLINE;

  @Autowired
  private ConfigurazioneDispositiviServiziRepository configurazioneDispositiviServiziRepository;

  @Autowired
  private ConfigurazioneDispositiviServiziService configurazioneDispositiviServiziService;

  @Autowired
  private ConfigurazioneDispositiviServiziQueryService configurazioneDispositiviServiziQueryService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restConfigurazioneDispositiviServiziMockMvc;

  private ConfigurazioneDispositiviServizi configurazioneDispositiviServizi;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final ConfigurazioneDispositiviServiziResource configurazioneDispositiviServiziResource = new ConfigurazioneDispositiviServiziResource(configurazioneDispositiviServiziService,
                                                                                                                                           configurazioneDispositiviServiziQueryService);
    this.restConfigurazioneDispositiviServiziMockMvc = MockMvcBuilders.standaloneSetup(configurazioneDispositiviServiziResource)
                                                                      .setCustomArgumentResolvers(pageableArgumentResolver)
                                                                      .setControllerAdvice(exceptionTranslator)
                                                                      .setMessageConverters(jacksonMessageConverter)
                                                                      .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static ConfigurazioneDispositiviServizi createEntity(EntityManager em) {
    ConfigurazioneDispositiviServizi configurazioneDispositiviServizi = new ConfigurazioneDispositiviServizi().modalitaGestione(DEFAULT_MODALITA_GESTIONE);
    return configurazioneDispositiviServizi;
  }

  @Before
  public void initTest() {
    configurazioneDispositiviServizi = createEntity(em);
  }

  @Test
  @Transactional
  public void createConfigurazioneDispositiviServizi() throws Exception {
    int databaseSizeBeforeCreate = configurazioneDispositiviServiziRepository.findAll()
                                                                             .size();

    // Create the ConfigurazioneDispositiviServizi
    restConfigurazioneDispositiviServiziMockMvc.perform(post("/api/configurazione-dispositivi-servizis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                                        .content(TestUtil.convertObjectToJsonBytes(configurazioneDispositiviServizi)))
                                               .andExpect(status().isCreated());

    // Validate the ConfigurazioneDispositiviServizi in the database
    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeCreate + 1);
    ConfigurazioneDispositiviServizi testConfigurazioneDispositiviServizi = configurazioneDispositiviServiziList.get(configurazioneDispositiviServiziList.size()
                                                                                                                     - 1);
    assertThat(testConfigurazioneDispositiviServizi.getModalitaGestione()).isEqualTo(DEFAULT_MODALITA_GESTIONE);
  }

  @Test
  @Transactional
  public void createConfigurazioneDispositiviServiziWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = configurazioneDispositiviServiziRepository.findAll()
                                                                             .size();

    // Create the ConfigurazioneDispositiviServizi with an existing ID
    configurazioneDispositiviServizi.setId(1L);

    // An entity with an existing ID cannot be created, so this API call must fail
    restConfigurazioneDispositiviServiziMockMvc.perform(post("/api/configurazione-dispositivi-servizis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                                        .content(TestUtil.convertObjectToJsonBytes(configurazioneDispositiviServizi)))
                                               .andExpect(status().isBadRequest());

    // Validate the ConfigurazioneDispositiviServizi in the database
    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void checkModalitaGestioneIsRequired() throws Exception {
    int databaseSizeBeforeTest = configurazioneDispositiviServiziRepository.findAll()
                                                                           .size();
    // set the field null
    configurazioneDispositiviServizi.setModalitaGestione(null);

    // Create the ConfigurazioneDispositiviServizi, which fails.

    restConfigurazioneDispositiviServiziMockMvc.perform(post("/api/configurazione-dispositivi-servizis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                                        .content(TestUtil.convertObjectToJsonBytes(configurazioneDispositiviServizi)))
                                               .andExpect(status().isBadRequest());

    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllConfigurazioneDispositiviServizis() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziRepository.saveAndFlush(configurazioneDispositiviServizi);

    // Get all the configurazioneDispositiviServiziList
    restConfigurazioneDispositiviServiziMockMvc.perform(get("/api/configurazione-dispositivi-servizis?sort=id,desc"))
                                               .andExpect(status().isOk())
                                               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                               .andExpect(jsonPath("$.[*].id").value(hasItem(configurazioneDispositiviServizi.getId()
                                                                                                                             .intValue())))
                                               .andExpect(jsonPath("$.[*].modalitaGestione").value(hasItem(DEFAULT_MODALITA_GESTIONE.toString())));
  }

  @Test
  @Transactional
  public void getConfigurazioneDispositiviServizi() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziRepository.saveAndFlush(configurazioneDispositiviServizi);

    // Get the configurazioneDispositiviServizi
    restConfigurazioneDispositiviServiziMockMvc.perform(get("/api/configurazione-dispositivi-servizis/{id}",
                                                            configurazioneDispositiviServizi.getId()))
                                               .andExpect(status().isOk())
                                               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                               .andExpect(jsonPath("$.id").value(configurazioneDispositiviServizi.getId()
                                                                                                                 .intValue()))
                                               .andExpect(jsonPath("$.modalitaGestione").value(DEFAULT_MODALITA_GESTIONE.toString()));
  }

  @Test
  @Transactional
  public void getAllConfigurazioneDispositiviServizisByModalitaGestioneIsEqualToSomething() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziRepository.saveAndFlush(configurazioneDispositiviServizi);

    // Get all the configurazioneDispositiviServiziList where modalitaGestione equals to DEFAULT_MODALITA_GESTIONE
    defaultConfigurazioneDispositiviServiziShouldBeFound("modalitaGestione.equals=" + DEFAULT_MODALITA_GESTIONE);

  }

  @Test
  @Transactional
  public void getAllConfigurazioneDispositiviServizisByModalitaGestioneIsInShouldWork() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziRepository.saveAndFlush(configurazioneDispositiviServizi);

    // Get all the configurazioneDispositiviServiziList where modalitaGestione in DEFAULT_MODALITA_GESTIONE or
    // UPDATED_MODALITA_GESTIONE
    defaultConfigurazioneDispositiviServiziShouldBeFound("modalitaGestione.in=" + DEFAULT_MODALITA_GESTIONE + ","
                                                         + UPDATED_MODALITA_GESTIONE);

  }

  @Test
  @Transactional
  public void getAllConfigurazioneDispositiviServizisByModalitaGestioneIsNullOrNotNull() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziRepository.saveAndFlush(configurazioneDispositiviServizi);

    // Get all the configurazioneDispositiviServiziList where modalitaGestione is not null
    defaultConfigurazioneDispositiviServiziShouldBeFound("modalitaGestione.specified=true");

    // Get all the configurazioneDispositiviServiziList where modalitaGestione is null
    defaultConfigurazioneDispositiviServiziShouldNotBeFound("modalitaGestione.specified=false");
  }

  /**
   * Executes the search, and checks that the default entity is returned
   */
  private void defaultConfigurazioneDispositiviServiziShouldBeFound(String filter) throws Exception {
    restConfigurazioneDispositiviServiziMockMvc.perform(get("/api/configurazione-dispositivi-servizis?sort=id,desc&" + filter))
                                               .andExpect(status().isOk())
                                               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                               .andExpect(jsonPath("$.[*].id").value(hasItem(configurazioneDispositiviServizi.getId()
                                                                                                                             .intValue())))
                                               .andExpect(jsonPath("$.[*].modalitaGestione").value(hasItem(DEFAULT_MODALITA_GESTIONE.toString())));
  }

  /**
   * Executes the search, and checks that the default entity is not returned
   */
  private void defaultConfigurazioneDispositiviServiziShouldNotBeFound(String filter) throws Exception {
    restConfigurazioneDispositiviServiziMockMvc.perform(get("/api/configurazione-dispositivi-servizis?sort=id,desc&" + filter))
                                               .andExpect(status().isOk())
                                               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                               .andExpect(jsonPath("$").isArray())
                                               .andExpect(jsonPath("$").isEmpty());
  }

  @Test
  @Transactional
  public void getNonExistingConfigurazioneDispositiviServizi() throws Exception {
    // Get the configurazioneDispositiviServizi
    restConfigurazioneDispositiviServiziMockMvc.perform(get("/api/configurazione-dispositivi-servizis/{id}", Long.MAX_VALUE))
                                               .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateConfigurazioneDispositiviServizi() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziService.save(configurazioneDispositiviServizi);

    int databaseSizeBeforeUpdate = configurazioneDispositiviServiziRepository.findAll()
                                                                             .size();

    // Update the configurazioneDispositiviServizi
    ConfigurazioneDispositiviServizi updatedConfigurazioneDispositiviServizi = configurazioneDispositiviServiziRepository.findOne(configurazioneDispositiviServizi.getId());
    updatedConfigurazioneDispositiviServizi.modalitaGestione(UPDATED_MODALITA_GESTIONE);

    restConfigurazioneDispositiviServiziMockMvc.perform(put("/api/configurazione-dispositivi-servizis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                                       .content(TestUtil.convertObjectToJsonBytes(updatedConfigurazioneDispositiviServizi)))
                                               .andExpect(status().isOk());

    // Validate the ConfigurazioneDispositiviServizi in the database
    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeUpdate);
    ConfigurazioneDispositiviServizi testConfigurazioneDispositiviServizi = configurazioneDispositiviServiziList.get(configurazioneDispositiviServiziList.size()
                                                                                                                     - 1);
    assertThat(testConfigurazioneDispositiviServizi.getModalitaGestione()).isEqualTo(UPDATED_MODALITA_GESTIONE);
  }

  @Test
  @Transactional
  public void updateNonExistingConfigurazioneDispositiviServizi() throws Exception {
    int databaseSizeBeforeUpdate = configurazioneDispositiviServiziRepository.findAll()
                                                                             .size();

    // Create the ConfigurazioneDispositiviServizi

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restConfigurazioneDispositiviServiziMockMvc.perform(put("/api/configurazione-dispositivi-servizis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                                       .content(TestUtil.convertObjectToJsonBytes(configurazioneDispositiviServizi)))
                                               .andExpect(status().isCreated());

    // Validate the ConfigurazioneDispositiviServizi in the database
    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteConfigurazioneDispositiviServizi() throws Exception {
    // Initialize the database
    configurazioneDispositiviServiziService.save(configurazioneDispositiviServizi);

    int databaseSizeBeforeDelete = configurazioneDispositiviServiziRepository.findAll()
                                                                             .size();

    // Get the configurazioneDispositiviServizi
    restConfigurazioneDispositiviServiziMockMvc.perform(delete("/api/configurazione-dispositivi-servizis/{id}",
                                                               configurazioneDispositiviServizi.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                                               .andExpect(status().isOk());

    // Validate the database is empty
    List<ConfigurazioneDispositiviServizi> configurazioneDispositiviServiziList = configurazioneDispositiviServiziRepository.findAll();
    assertThat(configurazioneDispositiviServiziList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(ConfigurazioneDispositiviServizi.class);
    ConfigurazioneDispositiviServizi configurazioneDispositiviServizi1 = new ConfigurazioneDispositiviServizi();
    configurazioneDispositiviServizi1.setId(1L);
    ConfigurazioneDispositiviServizi configurazioneDispositiviServizi2 = new ConfigurazioneDispositiviServizi();
    configurazioneDispositiviServizi2.setId(configurazioneDispositiviServizi1.getId());
    assertThat(configurazioneDispositiviServizi1).isEqualTo(configurazioneDispositiviServizi2);
    configurazioneDispositiviServizi2.setId(2L);
    assertThat(configurazioneDispositiviServizi1).isNotEqualTo(configurazioneDispositiviServizi2);
    configurazioneDispositiviServizi1.setId(null);
    assertThat(configurazioneDispositiviServizi1).isNotEqualTo(configurazioneDispositiviServizi2);
  }
}
