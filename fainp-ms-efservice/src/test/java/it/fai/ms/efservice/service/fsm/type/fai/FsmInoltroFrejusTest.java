package it.fai.ms.efservice.service.fsm.type.fai;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.verification.VerificationModeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.FsmManageDeviceSent;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.frejus.InoltraFlussoFrejus;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.frejus.FsmInoltroFrejusConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroFrejusTest {

	static final FsmSenderToQueue fsmSender = mock(FsmSenderToQueue.class);

	static final ManageDeviceSentModeService managedDeviceSentModeService = mock(ManageDeviceSentModeService.class);

	static final DeviceProducerService deviceProducerService = mock(DeviceProducerService.class);

	static final InoltraFlussoFrejus inoltroFlussoFrejus = mock(InoltraFlussoFrejus.class);

	static final GeneratorContractCodeService contractGenerator = mock(GeneratorContractCodeService.class);
	@Configuration
	public static class FC {

		private Logger _log = LoggerFactory.getLogger(getClass());

		@PostConstruct
		public void init() {
			_log.debug("Test configuration for {}", FsmInoltroFrejusConfig.class.getSimpleName());
		}

		@Bean
		@Primary
		public FsmInoltroFrejusConfig newFrejusInoltroConfiguration() {
			FsmInoltroFrejusConfig fsmInoltroTrackyCard = new FsmInoltroFrejusConfig(contractGenerator, inoltroFlussoFrejus, fsmSender,
					managedDeviceSentModeService, deviceProducerService);
			_log.info("Created FsmInoltroFrejusConfig for test {}", fsmInoltroTrackyCard);
			return fsmInoltroTrackyCard;
		}
	}

	@Autowired
	private FsmFactory fsmFactory;

	@Autowired
	private FsmRichiestaCacheService cache;

	private Richiesta richiesta = null;

	private Dispositivo dispositivo;

	@Before // Set-up
	public void setUp() throws Exception {
		cache.clearCache();

		String uuidData = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
		richiesta = newMockRichiesta(uuidData);
		reset(inoltroFlussoFrejus);
	}


	@Test
	public void contractResponseKoAnnullatoOperatore() throws Exception {
//    ClienteFai newClienteFai = newClienteFai();
		AbstractFsmRichiesta fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_FREJUS);

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta, null);

		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INOLTRATO);
		verify(inoltroFlussoFrejus).inoltra(richiesta);
		verifyNoMoreIntyeraction();

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.RESPONSE_KO, richiesta, null);
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ORDINE_SOSPESO);
		verifyNoMoreIntyeraction();

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE, richiesta, null);
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
		verifyNoMoreIntyeraction();

	}

	@Test
	@Ignore
	public void contractResponseKoRipetuto() throws Exception {
//    ClienteFai newClienteFai = newClienteFai();
		AbstractFsmRichiesta fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_FREJUS);

		initializeManagedDeviceSentModeService(FsmManageDeviceSent.TRANSITO);


		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta, null);



		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INOLTRATO);
		verify(inoltroFlussoFrejus, times(1)).inoltra(richiesta);
		verifyNoMoreIntyeraction();

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.RESPONSE_KO, richiesta, null);
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ORDINE_SOSPESO);
		verifyNoMoreIntyeraction();

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_RIPETI_INVIO, richiesta, null);
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INOLTRATO);
		verify(inoltroFlussoFrejus, times(2)).inoltra(richiesta);
		verifyNoMoreIntyeraction();




		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.RESPONSE_OK, richiesta, null);
		verify(managedDeviceSentModeService, VerificationModeFactory.atLeastOnce()).isToBeSentAt(eq(richiesta), any());
		{
		ArgumentCaptor<DispositivoEvent> sentMessages = ArgumentCaptor.forClass(DispositivoEvent.class);
		verify(fsmSender, times(1)).sendMessageForChangeStatoDispositivo(eq(dispositivo),sentMessages.capture());
		assertThat(sentMessages.getAllValues()).containsExactly(DispositivoEvent.IN_SPEDIZIONE);
		}
		verifyNoMoreIntyeraction();
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTESA_DISPOSITIVO_DA_FORNITORE);

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_SPEDIZIONE_FAI, richiesta, null);
		{
			ArgumentCaptor<DispositivoEvent> sentMessages = ArgumentCaptor.forClass(DispositivoEvent.class);
			verify(fsmSender, times(2)).sendMessageForChangeStatoDispositivo(eq(dispositivo),sentMessages.capture());
			assertThat(sentMessages.getAllValues()).containsExactly(DispositivoEvent.IN_SPEDIZIONE,DispositivoEvent.SPEDITO_A_FAI);
		}
		verifyNoMoreIntyeraction();
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);

		richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_SPEDIZIONE_COMPLETA, richiesta, null);
		verify(deviceProducerService, times(1)).updateDeviceServiceStatus(richiesta);
		{
			ArgumentCaptor<DispositivoEvent> sentMessages = ArgumentCaptor.forClass(DispositivoEvent.class);
			verify(fsmSender, times(3)).sendMessageForChangeStatoDispositivo(eq(dispositivo),sentMessages.capture());
			assertThat(sentMessages.getAllValues()).containsExactly(DispositivoEvent.IN_SPEDIZIONE,DispositivoEvent.SPEDITO_A_FAI,DispositivoEvent.SPEDITO_DA_FAI);
		}
		verifyNoMoreIntyeraction();
		assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);

	}

	private void initializeManagedDeviceSentModeService(final FsmManageDeviceSent fmds) {
		when(managedDeviceSentModeService.isToBeSentAt(richiesta, fmds)).thenReturn(true);
		when(managedDeviceSentModeService.isToBeSentAt(argThat(r -> r == richiesta), argThat(d -> d != fmds)))
				.thenReturn(false);
		when(managedDeviceSentModeService.isToBeSentAt(richiesta, null)).thenThrow(NullPointerException.class);
	}

	private void verifyNoMoreIntyeraction() {
		verifyNoMoreInteractions(inoltroFlussoFrejus, fsmSender, managedDeviceSentModeService, deviceProducerService);

	}

//  @Test
//  @Transactional
//  public void contractCodeNotGenerated() throws Exception {
//    when(FrejusInoltroConfiguration.generatorContractCode.generateAndSaveContractCode(TipoDispositivoEnum.TRACKYCARD,
//                                                                                          richiesta.getContratto())).thenReturn(richiesta.getContratto()
//                                                                                                                                         .codContrattoCliente(null));
//    AbstractFsmRichiesta fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_FREJUS);
//    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta, null);
//    StatoRichiesta stato = richiesta.getStato();
//    assertThat(stato).isEqualTo(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
//  }
//
	private Richiesta newMockRichiesta(String uuidData) {
		ClienteFai newClienteFai = newClienteFai();

		OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
				.numeroOrdine("NUOVOORDINE-" + uuidData).tipo(TipoOrdineCliente.NUOVO_ORDINE)
				.stato(StatoOrdineCliente.DA_EVADERE);

		dispositivo = new Dispositivo();
		Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
				.stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
				.tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO)
						.modalitaSpedizione(ModalitaSpedizione.A_FAI))
				.addDispositivo(dispositivo).contratto(newContratto(newClienteFai)).data(Instant.now())
				.dataModificaStato(Instant.now()).ordineCliente(oc).associazione("::targa::").country("::country::");

		richiesta.setId(Long.valueOf(uuidData));
		richiesta.setIdentificativo("identifier-" + uuidData + "-request");
		return richiesta;
	}

	private Contratto newContratto(ClienteFai clienteFai) {
		Contratto c = new Contratto().produttore(new Produttore()).clienteFai(clienteFai).stato(StatoContratto.ATTIVO)
				.dataModificaStato(Instant.now());
		c.setId(1L);
		return c;
	}

	private ClienteFai newClienteFai() {
		return new ClienteFai().codiceCliente("codcli");
	}

}
