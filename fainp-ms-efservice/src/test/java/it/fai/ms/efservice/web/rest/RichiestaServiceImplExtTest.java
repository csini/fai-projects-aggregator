package it.fai.ms.efservice.web.rest;

import static it.fai.common.enumeration.TipoDispositivoEnum.TOLL_COLLECT;
import static it.fai.common.enumeration.TipoDispositivoEnum.TRACKYCARD;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.assertj.core.util.Sets;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.KeyDatiAggiuntivi;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TypeClassValue;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepository;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.DatiAggiuntiviRichiestaService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.impl.RichiestaServiceImplExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
@Transactional
public class RichiestaServiceImplExtTest {
  private static final String CODICE_CLIENTE_FAI = "0046348";

  private static final String PEDAGGIO_ITALIA   = "PEDAGGI_ITALIA";
  private static final String PEDAGGIO_GERMANIA = "PEDAGGI_GERMANIA";
  private static final String PEDAGGIO_AUSTRIA  = "PEDAGGI_AUSTRIA";
  private static final String PEDAGGIO_SLOVENIA = "PEDAGGI_SLOVENIA";

  private static final String veicolo1 = "AA000AA";
  private static final String veicolo2 = "AB001AA";
  private static final String veicolo3 = "AC002AA";
  private static final String veicolo4 = "AD003AA";
  private static final String veicolo5 = "AE004AA";
  private static final String veicolo6 = "AF005AA";

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private ContrattoRepository contrattoRepository;

  @Autowired
  private ContrattoServiceExt contrattoServiceExt;

  @SpyBean
  private DispositivoServiceExt dispositivoServiceExt;

  @Autowired
  private OrdineClienteRepositoryExt ordineClienteRepositoryExt;

  @Autowired
  private RichiestaServiceImplExt richiestaServiceImplExt;

  @Autowired
  private ClienteFaiRepository clienteFaiRepository;

  @Autowired
  private DatiAggiuntiviRichiestaService datiAggiuntiviService;

  private List<Long> ordiniIdsBefore = new ArrayList<>();

  private List<Long> richiesteIdsBefore = new ArrayList<>();

  private Contratto contrattoTrackycard;

  @Before
  public void init() {
    // store all the requests before the test
    List<OrdineCliente> existingOrders = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI);
    List<Richiesta> existingRichieste = existingOrders.stream()
                                                      .flatMap(o -> o.getRichiestas()
                                                                     .stream())
                                                      .collect(toList());
    ordiniIdsBefore = existingOrders.stream()
                                    .map(OrdineCliente::getId)
                                    .collect(toList());
    richiesteIdsBefore = existingRichieste.stream()
                                          .map(Richiesta::getId)
                                          .collect(toList());
    TipoDispositivo trackyCard = dispositivoServiceExt.searchTipoDispositivoByNome(TipoDispositivoEnum.TRACKYCARD.name());
    ClienteFai cliente = clienteFaiRepository.findOneByCodiceCliente(CODICE_CLIENTE_FAI);
    Optional<Contratto> contractOpt = contrattoServiceExt.findContrattoByProduttoreAndCodiceAzienda(trackyCard.getProduttore(),
                                                                                                    CODICE_CLIENTE_FAI);
    if (!contractOpt.isPresent()) {
      contrattoTrackycard = contrattoServiceExt.createContrattoByProduttoreAndClienteFai(trackyCard.getProduttore(), cliente);
    } else {
      contrattoTrackycard = contractOpt.get();
    }
  }

  @After
  public void cleanup() {
    // diff the new requests with the previous ones.
    List<OrdineCliente> orders = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI)
                                                           .stream()
                                                           .filter(ordineCliente -> !ordiniIdsBefore.contains(ordineCliente.getId()))
                                                           .collect(toList());
    List<Richiesta> richieste = orders.stream()
                                      .flatMap(o -> o.getRichiestas()
                                                     .stream())
                                      .collect(toList());
    // delete the newest requests and orders
    richiestaRepositoryExt.delete(richieste.stream()
                                           .filter(r -> !richiesteIdsBefore.contains(r.getId()))
                                           .collect(toList()));
    ordineClienteRepositoryExt.delete(orders);
    contrattoRepository.delete(contrattoTrackycard);

  }

  @Test
  @Transactional
  public void createRichiestaWithDatiAggiuntivi() throws Exception {
    TipoDispositivo hgvDevice = dispositivoServiceExt.searchTipoDispositivoByNome(TipoDispositivoEnum.HGV.name());
    Richiesta r1 = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                  .tipoDispositivo(hgvDevice)
                                  .data(Instant.now())
                                  .stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    Richiesta r2 = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                  .tipoDispositivo(hgvDevice)
                                  .data(Instant.now())
                                  .stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    DatiAggiuntiviRichiesta da1r1 = new DatiAggiuntiviRichiesta().chiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_DA)
                                                                 .valore(sdf.format(new Date()))
                                                                 .type(TypeClassValue.STRING);
    DatiAggiuntiviRichiesta da2r1 = new DatiAggiuntiviRichiesta().chiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_A)
                                                                 .valore(sdf.format(new Date()))
                                                                 .type(TypeClassValue.STRING);
    DatiAggiuntiviRichiesta da1r2 = new DatiAggiuntiviRichiesta().chiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_DA)
                                                                 .valore(sdf.format(new Date()))
                                                                 .type(TypeClassValue.STRING);
    DatiAggiuntiviRichiesta da2r2 = new DatiAggiuntiviRichiesta().chiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_A)
                                                                 .valore(sdf.format(new Date()))
                                                                 .type(TypeClassValue.STRING);

    da1r1.setRichiesta(r1);
    da2r1.setRichiesta(r1);
    r1.addDatiAggiuntivi(da1r1);
    r1.addDatiAggiuntivi(da2r1);

    da1r2.setRichiesta(r2);
    da2r2.setRichiesta(r2);
    r2.addDatiAggiuntivi(da1r2);
    r2.addDatiAggiuntivi(da2r2);

    List<DatiAggiuntiviRichiesta> datiAggiuntivi = r1.getDatiAggiuntivi();
    Richiesta saved = richiestaServiceImplExt.saveAndFlush(r1);
    datiAggiuntiviService.saveAndFlush(datiAggiuntivi);
    List<DatiAggiuntiviRichiesta> datiAggiuntiviPersisted = saved.getDatiAggiuntivi();
    assertThat(datiAggiuntiviPersisted).hasSize(2);
  }

  @Test
  @Transactional
  @Ignore(value = "Skip to remove service PEDAGGI AUSTRIA to TOLL-COLLECT")
  public void createRichiestaTollCollectWithPedaggiAustria_trackyCardAttiva() throws Exception {
    /*
     * si richiede un dispositivo toll_collect su 3 veicoli - veicolo1: ha solo pedaggi germania - veicolo3: ha pedaggi
     * austria e germania e non ha nessuna trackycard sul veicolo - veicolo5: ha pedaggi austria e germania ma ha già
     * una trackycard sul veicolo Risultato Atteso: - 4 richieste - 3 richieste toll collect su veicoli 1,3,5 - 1
     * richieta trackycard su veicolo 3
     */

    List<OrdineCliente> existingOrders = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI);
    int existingOrdersSize = existingOrders.size();
    CarrelloDTO carrelloDTO = new CarrelloDTO();
    carrelloDTO.setCodiceAzienda(CODICE_CLIENTE_FAI);
    HashMap<String, String> datiAggiuntivi = new HashMap<>();
    datiAggiuntivi.put("costo", "1500");
    carrelloDTO.setDatiAggiuntivi(datiAggiuntivi);
    carrelloDTO.setTipoServizio(PEDAGGIO_GERMANIA);
    // uuid vehicle = targa to int (fakeTargaToUUID)
    List<VehicleDTO> expectedVehicles = Arrays.asList(buildStubVehicleDTO(veicolo1), buildStubVehicleDTO(veicolo3),
                                                      buildStubVehicleDTO(veicolo5));
    List<String> uuidVeichles = expectedVehicles.stream()
                                                .map(VehicleDTO::getIdentificativo)
                                                .collect(toList());
    carrelloDTO.setUuidVeicoli(uuidVeichles);
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(uuidVeichles.get(0), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA)));
    mappaVeicoli.put(uuidVeichles.get(1), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA, PEDAGGIO_AUSTRIA)));
    mappaVeicoli.put(uuidVeichles.get(2), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA, PEDAGGIO_AUSTRIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrelloDTO.setDispositivi(createDispositivi(TOLL_COLLECT.name(), mappaDispositiviVeicoli));

    // mock veicolo5 ha una trackycard attiva
    when(dispositivoServiceExt.checkDispositivoExists(ArgumentMatchers.eq(CODICE_CLIENTE_FAI),
                                                      ArgumentMatchers.eq(TipoDispositivoEnum.TRACKYCARD),
                                                      ArgumentMatchers.anySetOf(String.class))).thenReturn(Sets.newHashSet(Arrays.asList(fakeTargaToUUID(veicolo3))));
    List<Richiesta> createdRichieste = richiestaServiceImplExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrelloDTO);
    ArgumentCaptor<Set<String>> captor = ArgumentCaptor.forClass(Set.class);
    verify(dispositivoServiceExt).checkDispositivoExists(ArgumentMatchers.eq(CODICE_CLIENTE_FAI),
                                                         ArgumentMatchers.eq(TipoDispositivoEnum.TRACKYCARD), captor.capture());
    assertThat(captor.getValue()).containsExactlyInAnyOrder(fakeTargaToUUID(veicolo3), fakeTargaToUUID(veicolo5));
    List<OrdineCliente> ordineCliente = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI);
    // un ordine creato
    assertThat(ordineCliente).hasSize(existingOrdersSize + 1);
    OrdineCliente lastOrder = ordineCliente.get(ordineCliente.size() - 1);
    // l'ordine contiene 4 richieste
    assertThat(lastOrder.getRichiestas()).hasSize(4);
    assertThat(createdRichieste).hasSize(4);
    // si controlla sia il risultato restituito dal servizio sia quello presente a db
    // solo veicolo 3 ha trackycard
    List<Richiesta> richiesteTrackyCard = createdRichieste.stream()
                                                          .filter(r -> r.getTipoDispositivo()
                                                                        .getNome()
                                                                        .equals(TipoDispositivoEnum.TRACKYCARD))
                                                          .collect(toList());
    assertThat(richiesteTrackyCard).hasSize(1);
    assertThat(richiesteTrackyCard.get(0)
                                  .getDispositivos()
                                  .stream()
                                  .findFirst()
                                  .get()
                                  .getContratto()).isNotNull()
                                                  .matches(c -> c.getId()
                                                                 .equals(contrattoTrackycard.getId()));
    assertThat(richiesteTrackyCard.stream()
                                  .map(r -> r.getDispositivos()
                                             .stream()
                                             .findFirst()
                                             .get()
                                             .getAssociazioneDispositivoVeicolos()
                                             .stream()
                                             .findFirst()
                                             .get())
                                  .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                                  .collect(toList())).containsExactlyInAnyOrder(fakeTargaToUUID(veicolo3));
    assertThat(lastOrder.getRichiestas()
                        .stream()
                        .filter(r -> r.getTipoDispositivo()
                                      .getNome()
                                      .equals(TipoDispositivoEnum.TRACKYCARD))
                        .map(r -> r.getDispositivos()
                                   .stream()
                                   .findFirst()
                                   .get()
                                   .getAssociazioneDispositivoVeicolos()
                                   .stream()
                                   .findFirst()
                                   .get())
                        .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                        .collect(toList())).hasSize(1)
                                           .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo3));
    // veicoli 1,3,6 con toll collect
    assertThat(createdRichieste.stream()
                               .filter(r -> r.getTipoDispositivo()
                                             .getNome()
                                             .equals(TipoDispositivoEnum.TOLL_COLLECT))
                               .map(r -> r.getDispositivos()
                                          .stream()
                                          .findFirst()
                                          .get()
                                          .getAssociazioneDispositivoVeicolos()
                                          .stream()
                                          .findFirst()
                                          .get())
                               .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                               .collect(toList())).hasSize(3)
                                                  .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo1), fakeTargaToUUID(veicolo3),
                                                                             fakeTargaToUUID(veicolo5));
    assertThat(lastOrder.getRichiestas()
                        .stream()
                        .filter(r -> r.getTipoDispositivo()
                                      .getNome()
                                      .equals(TipoDispositivoEnum.TOLL_COLLECT))
                        .map(r -> r.getDispositivos()
                                   .stream()
                                   .findFirst()
                                   .get()
                                   .getAssociazioneDispositivoVeicolos()
                                   .stream()
                                   .findFirst()
                                   .get())
                        .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                        .collect(toList())).hasSize(3)
                                           .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo1), fakeTargaToUUID(veicolo3),
                                                                      fakeTargaToUUID(veicolo5));

  }

  private Contratto stubContratto(String codiceClient, StatoContratto statoContratto, String codiceContratto, Long idContratto) {

    Contratto contratto = new Contratto().clienteFai(clienteFaiRepository.findOneByCodiceCliente(codiceClient))
                                         .stato(statoContratto)
                                         .codContrattoCliente(codiceContratto);
    contratto.setId(idContratto);
    return contratto;
  }

  @Test
  @Transactional
  @Ignore(value = "Skip to remove service PEDAGGI AUSTRIA to TOLL-COLLECT")
  public void createRichiestaTollCollectWithPedaggiAustria_TrackycardInCarrello() throws Exception {
    /*
     * si richiede un dispositivo toll_collect su 3 veicoli - veicolo1: ha solo pedaggi germania - veicolo3: ha pedaggi
     * austria e germania ma ha una trackycard in carrello - veicolo5: ha pedaggi austria e germania e nessun'altra
     * trackycard si richiede un dispositivo trackycard su 1 veicolo 3 per pedaggi slovenia Risultato Atteso: - 5
     * richieste = - 3 richieste toll collect su veicoli 1,3,5 - 1 richieta trackycard su veicolo 3 relativo a pedaggi
     * slovenia - 1 richiesta trackycard su veicolo 5 relativo a pedaggi austria
     */
    List<OrdineCliente> existingOrders = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI);
    int existingOrdersSize = existingOrders.size();
    CarrelloDTO carrelloDTO = new CarrelloDTO();
    carrelloDTO.setCodiceAzienda(CODICE_CLIENTE_FAI);
    HashMap<String, String> datiAggiuntivi = new HashMap<>();
    datiAggiuntivi.put("costo", "1500");
    carrelloDTO.setDatiAggiuntivi(datiAggiuntivi);
    carrelloDTO.setTipoServizio(PEDAGGIO_GERMANIA);
    // uuid vehicle = targa to int (fakeTargaToUUID)
    List<VehicleDTO> expectedVehicles = Arrays.asList(buildStubVehicleDTO(veicolo1), buildStubVehicleDTO(veicolo3),
                                                      buildStubVehicleDTO(veicolo5));
    /*
     * 65654848486565 : AA000AA 65674848506565 : AC002AA 65694848526565 : AE004AA
     */
    List<String> uuidVeichles = expectedVehicles.stream()
                                                .map(VehicleDTO::getIdentificativo)
                                                .collect(toList());
    carrelloDTO.setUuidVeicoli(uuidVeichles);
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(uuidVeichles.get(0), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA)));
    mappaVeicoli.put(uuidVeichles.get(1), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA, PEDAGGIO_AUSTRIA)));
    mappaVeicoli.put(uuidVeichles.get(2), new ArrayList<String>(Arrays.asList(PEDAGGIO_GERMANIA, PEDAGGIO_AUSTRIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrelloDTO.setDispositivi(new ArrayList<>());
    carrelloDTO.getDispositivi()
               .addAll(createDispositivi(TOLL_COLLECT.name(), mappaDispositiviVeicoli));
    mappaDispositiviVeicoli = new HashMap<>();
    mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(uuidVeichles.get(1), Arrays.asList(PEDAGGIO_SLOVENIA));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrelloDTO.getDispositivi()
               .addAll(createDispositivi(TRACKYCARD.name(), mappaDispositiviVeicoli));
    TipoDispositivo trackyCard = dispositivoServiceExt.searchTipoDispositivoByNome(TipoDispositivoEnum.TRACKYCARD.name());

    when(dispositivoServiceExt.checkDispositivoExists(ArgumentMatchers.eq(CODICE_CLIENTE_FAI),
                                                      ArgumentMatchers.eq(TipoDispositivoEnum.TRACKYCARD),
                                                      ArgumentMatchers.anySetOf(String.class))).thenReturn(new HashSet<>(Arrays.asList(fakeTargaToUUID(veicolo3), fakeTargaToUUID(veicolo5))));
    List<Richiesta> createdRichiete = richiestaServiceImplExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrelloDTO);

    // non c'è nessun contratto con trackycard quindi deve crearne uno

    List<OrdineCliente> ordineCliente = ordineClienteRepositoryExt.findByIdentificativoCliente(CODICE_CLIENTE_FAI);
    assertThat(ordineCliente).hasSize(existingOrdersSize + 1);
    OrdineCliente lastOrder = ordineCliente.get(ordineCliente.size() - 1);
    assertThat(lastOrder.getRichiestas()).hasSize(5);
    assertThat(createdRichiete).hasSize(5);
    List<Richiesta> trackyCardRequests = createdRichiete.stream()
                                                        .filter(r -> r.getTipoDispositivo()
                                                                      .getNome()
                                                                      .equals(TipoDispositivoEnum.TRACKYCARD))
                                                        .collect(toList());
    assertThat(trackyCardRequests).hasSize(2);

    assertThat(trackyCardRequests.stream()
                                 .flatMap(richiesta -> richiesta.getDispositivos()
                                                                .stream())
                                 .map(d -> d.getContratto())).allMatch(contratto -> contratto.getId()
                                                                                             .equals(contrattoTrackycard.getId()));

    // trackycard su veicolo 5
    assertThat(trackyCardRequests.stream()
                                 .map(r -> r.getDispositivos()
                                            .stream()
                                            .findFirst()
                                            .get()
                                            .getAssociazioneDispositivoVeicolos()
                                            .stream()
                                            .findFirst()
                                            .get())
                                 .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                                 .collect(toList())).containsExactlyInAnyOrder(fakeTargaToUUID(veicolo3), fakeTargaToUUID(veicolo5));
    // sul veicolo 5 la trackycard ha attivo solo servizi PEDAGGIO_SLOVENIA
    List<Richiesta> richiestaTrackycardVeicolo5 = trackyCardRequests.stream()
                                                                    .filter(r -> r.getDispositivos()
                                                                                  .stream()
                                                                                  .findFirst()
                                                                                  .get()
                                                                                  .getAssociazioneDispositivoVeicolos()
                                                                                  .stream()
                                                                                  .findFirst()
                                                                                  .get()
                                                                                  .getUuidVeicolo()
                                                                                  .equals(fakeTargaToUUID(veicolo5)))
                                                                    .collect(toList());
    assertThat(richiestaTrackycardVeicolo5).hasSize(1);
    assertThat(richiestaTrackycardVeicolo5.get(0)
                                          .getDispositivos()
                                          .stream()
                                          .flatMap(d -> d.getStatoDispositivoServizios()
                                                         .stream())
                                          .map(StatoDispositivoServizio::getTipoServizio)
                                          .map(TipoServizio::getNome)
                                          .collect(toList())).contains(PEDAGGIO_AUSTRIA);
    List<Richiesta> richiestaTrackycardVeicolo3 = trackyCardRequests.stream()
                                                                    .filter(r -> r.getDispositivos()
                                                                                  .stream()
                                                                                  .findFirst()
                                                                                  .get()
                                                                                  .getAssociazioneDispositivoVeicolos()
                                                                                  .stream()
                                                                                  .findFirst()
                                                                                  .get()
                                                                                  .getUuidVeicolo()
                                                                                  .equals(fakeTargaToUUID(veicolo3)))
                                                                    .collect(toList());
    assertThat(richiestaTrackycardVeicolo3).hasSize(1);
    assertThat(richiestaTrackycardVeicolo3.get(0)
                                          .getDispositivos()
                                          .stream()
                                          .flatMap(d -> d.getStatoDispositivoServizios()
                                                         .stream())
                                          .map(StatoDispositivoServizio::getTipoServizio)
                                          .map(TipoServizio::getNome)
                                          .collect(toList())).doesNotContain(PEDAGGIO_AUSTRIA);

    assertThat(lastOrder.getRichiestas()
                        .stream()
                        .filter(r -> r.getTipoDispositivo()
                                      .getNome()
                                      .equals(TipoDispositivoEnum.TRACKYCARD))
                        .map(r -> r.getDispositivos()
                                   .stream()
                                   .findFirst()
                                   .get()
                                   .getAssociazioneDispositivoVeicolos()
                                   .stream()
                                   .findFirst()
                                   .get())
                        .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                        .collect(toList())).hasSize(2)
                                           .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo3), fakeTargaToUUID(veicolo5));

    assertThat(createdRichiete.stream()
                              .filter(r -> r.getTipoDispositivo()
                                            .getNome()
                                            .equals(TipoDispositivoEnum.TOLL_COLLECT))
                              .map(r -> r.getDispositivos()
                                         .stream()
                                         .findFirst()
                                         .get()
                                         .getAssociazioneDispositivoVeicolos()
                                         .stream()
                                         .findFirst()
                                         .get())
                              .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                              .collect(toList())).hasSize(3)
                                                 .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo1), fakeTargaToUUID(veicolo3),
                                                                            fakeTargaToUUID(veicolo5));

    // verify that veichle 1,3,6 has toll collect
    assertThat(lastOrder.getRichiestas()
                        .stream()
                        .filter(r -> r.getTipoDispositivo()
                                      .getNome()
                                      .equals(TipoDispositivoEnum.TOLL_COLLECT))
                        .map(r -> r.getDispositivos()
                                   .stream()
                                   .findFirst()
                                   .get()
                                   .getAssociazioneDispositivoVeicolos()
                                   .stream()
                                   .findFirst()
                                   .get())
                        .map(associazioneDV -> associazioneDV.getUuidVeicolo())
                        .collect(toList())).hasSize(3)
                                           .containsExactlyInAnyOrder(fakeTargaToUUID(veicolo1), fakeTargaToUUID(veicolo3),
                                                                      fakeTargaToUUID(veicolo5));

  }

  private List<DispositivoCarrelloDTO> createDispositivi(String tipoDispositivo,
                                                         HashMap<Integer, HashMap<String, List<String>>> mapDispositivi) {
    List<DispositivoCarrelloDTO> dispositivi = new ArrayList<>();

    Set<Integer> keySet = mapDispositivi.keySet();
    Iterator<Integer> iterator = keySet.iterator();

    while ((iterator != null) && iterator.hasNext()) {
      Integer keyDispositivo = iterator.next();
      HashMap<String, List<String>> veicoliCarrello = mapDispositivi.get(keyDispositivo);
      DispositivoCarrelloDTO dispositivoCarrello = new DispositivoCarrelloDTO();
      dispositivoCarrello.setTipoDispositivo(tipoDispositivo);
      dispositivoCarrello.setVeicoli(createVeicoliCarrello(veicoliCarrello));
      dispositivi.add(dispositivoCarrello);
    }

    return dispositivi;
  }

  private List<VeicoloCarrelloDTO> createVeicoliCarrello(HashMap<String, List<String>> map) {
    List<VeicoloCarrelloDTO> listVeicolo = new ArrayList<>();
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while ((iterator != null) && iterator.hasNext()) {
      String keyVeicolo = iterator.next();
      List<String> listServizi = map.get(keyVeicolo);
      VeicoloCarrelloDTO veicolo = new VeicoloCarrelloDTO();
      veicolo.setUuid(keyVeicolo);
      veicolo.setNomeTipiServizio(createNomeServizio(listServizi));
      listVeicolo.add(veicolo);
    }
    return listVeicolo;
  }

  private List<String> createNomeServizio(List<String> servizi) {
    List<String> nomeServizio = new ArrayList<>();

    for (String servizio : servizi) {
      nomeServizio.add(servizio);
    }
    return nomeServizio;
  }

  private List<String> createUuidVeicoli(List<String> listVeicoli) {
    List<String> uuidVeicoli = new ArrayList<>();
    for (String veicolo : listVeicoli) {
      uuidVeicoli.add(veicolo);
    }
    return uuidVeicoli;
  }

  private Map<String, Object> getResultMap(MvcResult returnValue) throws JsonProcessingException, IOException {
    String responseValue = returnValue.getResponse()
                                      .getContentAsString();
    ObjectMapper mapper = new ObjectMapper();
    JsonNode json = mapper.readTree(responseValue);
    @SuppressWarnings("unchecked")
    Map<String, Object> resultMap = mapper.convertValue(json, Map.class);
    return resultMap;
  }

  private String fakeTargaToUUID(String targa) {
    StringBuffer sb = new StringBuffer();
    for (char c : targa.toCharArray()) {
      int number = (int) c;
      sb.append(number);
    }
    return sb.toString();
  }

  private VehicleDTO buildStubVehicleDTO(String targa) {
    VehicleDTO vehicleDTO = new VehicleDTO();
    vehicleDTO.setTarga(targa);
    vehicleDTO.setIdentificativo(fakeTargaToUUID(targa));
    return vehicleDTO;
  }

}
