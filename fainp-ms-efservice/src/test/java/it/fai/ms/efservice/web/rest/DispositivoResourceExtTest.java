package it.fai.ms.efservice.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.AbstractCommonTest;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DispositivoResourceExtTest extends AbstractCommonTest {

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired DispositivoResourceExt dispositivoResource;

  MockMvc mockMvc;

  @Test
  public void testGetDispositiviAttiviTarghe() throws Exception {
    LocalDate localDate = LocalDate.now();
    
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(DispositivoResourceExt.API_ACTIVE_DEVICES_WITH_PLATE);
    builder.queryParam("dateStart", localDate);

    mockMvc.perform(get(builder.build().toString())
                    .contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isOk());    
  }

  @Before
  public void setUp() throws Exception {
    mockMvc = MockMvcBuilders.standaloneSetup(dispositivoResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();    
  }

  @Test
  public void testCheckDevicesNotFound() throws Exception {
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(DispositivoResourceExt.API_DEVICES_NOT_FOUND + "/VIACARD");

    List<String> serials = Arrays.asList("105215", RandomString(), RandomString());
    
    mockMvc.perform(post(builder.build().toString())
                    .contentType(TestUtil.APPLICATION_JSON_UTF8)
                    .content(TestUtil.convertObjectToJsonBytes(serials)))
    .andExpect(status().isOk());    
  }

}
