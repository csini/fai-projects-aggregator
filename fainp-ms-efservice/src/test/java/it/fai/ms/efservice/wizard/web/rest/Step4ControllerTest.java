package it.fai.ms.efservice.wizard.web.rest;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivabilityFailure;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixDeviceType;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixVehicle;
import it.fai.ms.efservice.wizard.service.Step4Service;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc(webClientEnabled = true, webDriverEnabled = false)
public class Step4ControllerTest {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private MockMvc         mockMvc;
  private Step4Controller step4Controller;
  private Step4Service    step4Service = mock(Step4Service.class);

  @Before
  public void setUp() throws Exception {
    step4Controller = spy(new Step4Controller(step4Service));
    mockMvc = MockMvcBuilders.standaloneSetup(step4Controller)
                             .setMessageConverters(new MappingJackson2HttpMessageConverter())
                             .build();
  }

  @Test
  public void testGetMatrix() throws Exception {
    given(step4Service.buildMatrix(newWizardServiceTypeId(), newVehicleIds().stream()
                                                                            .map(itm -> new WizardVehicleId(itm))
                                                                            .collect(toSet()), "0046348")).willReturn(newStep4WizardMatrix());

    MvcResult mvcResult = mockMvc.perform(newRequestBuilder("0046348"))
                                 .andReturn();

    String contentType = mvcResult.getRequest()
                                  .getContentType();
    assertThat(contentType).isEqualTo(MediaType.APPLICATION_JSON_UTF8.toString());

    String cacheHeader = mvcResult.getResponse()
                                  .getHeader("Cache-control");
    assertThat(cacheHeader).contains("no-store");

    String json = mvcResult.getResponse()
                           .getContentAsString();
    _log.info("Returned json: {}", json);
    JSONAssert.assertEquals(JsonUtils.readFile("step4.json"), json, false);

    verify(step4Controller).getMatrix("::serviceId::", newVehicleIds(), "0046348");

    verify(step4Service).buildMatrix(newWizardServiceTypeId(), newVehicleIds().stream()
                                                                              .map(itm -> new WizardVehicleId(itm))
                                                                              .collect(toSet()), "0046348");
  }

  private String newRequestBody() {
    String json = null;
    try {
      json = JsonUtils.readFile("step4req.json");
    } catch (IOException e) {
      e.printStackTrace();
    }
    return json;
  }

  private RequestBuilder newRequestBuilder(String codiceCliente) {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/wizard/step4/matrix/::serviceId::/" + codiceCliente)
                                                          .content(newRequestBody())
                                                          .contentType(MediaType.APPLICATION_JSON_UTF8)
                                                          .accept(MediaType.APPLICATION_JSON_UTF8);
    return requestBuilder;
  }

  private WizardStep4Matrix newStep4WizardMatrix() {
    WizardStep4Matrix wizardStep4Matrix = new WizardStep4Matrix("::serviceId::");

    WizardStep4MatrixVehicle WizardStep4MatrixVehicle1 = new WizardStep4MatrixVehicle("::vehicleId1::");
    WizardStep4MatrixVehicle1.setEuroClass("euro_::1::");
    WizardStep4MatrixVehicle1.setLicensePlate("targa_::1::");
    WizardStep4MatrixVehicle1.setType("::tipo1::");
    WizardStep4MatrixDeviceType WizardStep4MatrixDeviceType1 = new WizardStep4MatrixDeviceType("::deviceTypeId1::");
    WizardStep4MatrixDeviceType1.setActivability(new WizardMatrixActivability(ActivabilityState.ANOMALY));
    WizardStep4MatrixDeviceType1.getActivability()
                                .addFailure(new WizardMatrixActivabilityFailure(1L, "::mess1::"));
    WizardStep4MatrixVehicle1.getDeviceTypes()
                             .put(WizardStep4MatrixDeviceType1.getId(), WizardStep4MatrixDeviceType1);
    WizardStep4MatrixDeviceType wizardStep4MatrixDeviceType2 = new WizardStep4MatrixDeviceType("::deviceTypeId2::");
    wizardStep4MatrixDeviceType2.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVE));
    WizardStep4MatrixVehicle1.getDeviceTypes()
                             .put(wizardStep4MatrixDeviceType2.getId(), wizardStep4MatrixDeviceType2);

    WizardStep4MatrixVehicle wizardStep4MatrixVehicle2 = new WizardStep4MatrixVehicle("::vehicleId2::");
    wizardStep4MatrixVehicle2.setEuroClass("euro_::2::");
    wizardStep4MatrixVehicle2.setLicensePlate("targa_::2::");
    wizardStep4MatrixVehicle2.setType("::tipo2::");
    WizardStep4MatrixDeviceType wizardStep4MatrixDeviceType3 = new WizardStep4MatrixDeviceType("::deviceTypeId3::");
    wizardStep4MatrixDeviceType3.setActivability(new WizardMatrixActivability(ActivabilityState.NOT_ACTIVABLE));
    wizardStep4MatrixDeviceType3.getActivability()
                                .addFailure(new WizardMatrixActivabilityFailure(2L, "::mess2::"));
    wizardStep4MatrixVehicle2.getDeviceTypes()
                             .put(WizardStep4MatrixDeviceType1.getId(), wizardStep4MatrixDeviceType3);
    WizardStep4MatrixDeviceType wizardStep4MatrixDeviceType4 = new WizardStep4MatrixDeviceType("::deviceTypeId4::");
    wizardStep4MatrixDeviceType4.setActivability(new WizardMatrixActivability(ActivabilityState.ONGOING_REQUEST));
    wizardStep4MatrixVehicle2.getDeviceTypes()
                             .put(wizardStep4MatrixDeviceType2.getId(), wizardStep4MatrixDeviceType4);

    wizardStep4Matrix.getVehicles()
                     .add(WizardStep4MatrixVehicle1);
    wizardStep4Matrix.getVehicles()
                     .add(wizardStep4MatrixVehicle2);
    return wizardStep4Matrix;
  }

  private Set<String> newVehicleIds() {
    return new LinkedHashSet<>(Arrays.asList("::vehicleId1::", "::vehicleId2::"));
  }

  private WizardServiceTypeId newWizardServiceTypeId() {
    return new WizardServiceTypeId("::serviceId::");
  }

}
