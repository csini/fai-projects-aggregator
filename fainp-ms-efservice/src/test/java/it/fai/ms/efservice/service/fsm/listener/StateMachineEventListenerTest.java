/**
 * 
 */
package it.fai.ms.efservice.service.fsm.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;

/**
 * @author Luca Vassallo
 */
public class StateMachineEventListenerTest extends StateMachineListenerAdapter<StateTest, EventTest> {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public void stateChanged(State<StateTest, EventTest> from, State<StateTest, EventTest> to) {
    if (logger.isDebugEnabled()) {
      logger.debug("StateMachine Listener => StateChanged: From: " + from.getId() + " - TO: " + to.getId());
    }
  }

  @Override
  public void stateEntered(State<StateTest, EventTest> state) {
  }

  @Override
  public void stateExited(State<StateTest, EventTest> state) {
  }

  @Override
  public void transition(Transition<StateTest, EventTest> transition) {
  }

  @Override
  public void transitionStarted(Transition<StateTest, EventTest> transition) {
  }

  @Override
  public void transitionEnded(Transition<StateTest, EventTest> transition) {
  }

  @Override
  public void stateMachineStarted(StateMachine<StateTest, EventTest> stateMachine) {
  }

  @Override
  public void stateMachineStopped(StateMachine<StateTest, EventTest> stateMachine) {
  }

  @Override
  public void eventNotAccepted(Message<EventTest> event) {
    if (logger.isWarnEnabled()) {
      logger.warn("StateMachine Listener => Event Not Accepted: EVENT " + ((event != null) ? event.getPayload()
                                                                                                  .name()
                                                                                           : null));
    }
  }

  @Override
  public void extendedStateChanged(Object key, Object value) {
  }

  @Override
  public void stateMachineError(StateMachine<StateTest, EventTest> stateMachine, Exception exception) {
  }

  @Override
  public void stateContext(StateContext<StateTest, EventTest> stateContext) {
    if (logger.isDebugEnabled()) {
      logger.debug("StateMachine Listener => StateContext: " + stateContext.toString());
    }
  }
}
