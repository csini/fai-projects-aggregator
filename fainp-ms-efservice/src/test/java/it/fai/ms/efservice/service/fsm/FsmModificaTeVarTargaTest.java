/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeVarTarga;

/**
 * @author Luca Vassallo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore("Skipped => Fix to use Mockito")
public class FsmModificaTeVarTargaTest {

  @Autowired
  private FsmModificaTeVarTarga fsmModificaTeVarTarga;

  private Richiesta entityTest;

  private String notaUtente = "NOTA UTENTE....";

  @Before // Set-up
  public void setUp() throws Exception {

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    entityTest = new Richiesta();
    entityTest.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    int randId = (int) (Math.random() * Integer.MAX_VALUE);
    entityTest.setId((long) randId);
  }

  @Test
  public void FsmModificaTeVarTarga_001_01() throws Exception {

    entityTest.setTipo(TipoRichiesta.VARIAZIONE_TARGA);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_001_02() throws Exception {

    entityTest.setTipo(TipoRichiesta.MEZZO_RITARGATO);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_001_03() throws Exception {

    entityTest.setTipo(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_002_01() throws Exception {

    entityTest.setTipo(TipoRichiesta.VARIAZIONE_TARGA);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  @Test
  public void FsmModificaTeVarTarga_002_02() throws Exception {

    entityTest.setTipo(TipoRichiesta.MEZZO_RITARGATO);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);

  }

  @Test
  public void FsmModificaTeVarTarga_002_03() throws Exception {

    entityTest.setTipo(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  private void assertChangeStateFsmTe(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsmModificaTeVarTarga.isAvailableCommand(event, entityTest);
    assertTrue(availableCommand);

    fsmModificaTeVarTarga.executeCommandToChangeState(event, entityTest, notaUtente);
    StatoRichiesta fsmStatus = entityTest.getStato();
    assertEquals(state.name(), fsmStatus.name());
  }

}
