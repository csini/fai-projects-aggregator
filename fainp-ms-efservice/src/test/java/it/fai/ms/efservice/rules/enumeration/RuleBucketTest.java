package it.fai.ms.efservice.rules.enumeration;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;

public class RuleBucketTest {

  @Test
  public void testTrue() {
    String test = "TELEPASS_EUROPEO";
    Optional<RuleBucket> ruleBucketOpt = RuleBucket.getRuleBucket(test);
    assertThat(ruleBucketOpt.isPresent()).isTrue();
  }

  @Test
  public void testFalse() {
    String test = "::device-test::";
    Optional<RuleBucket> ruleBucketOpt = RuleBucket.getRuleBucket(test);
    assertThat(ruleBucketOpt.isPresent()).isFalse();
  }

}
