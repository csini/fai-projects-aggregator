package it.fai.ms.efservice.service.fsm.type.assistenza.veicoli;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroAssistenzaVeicoliTest {

  private FsmInoltroAssistenzaVeicoli fsmInoltroAssistenzaVeicoli;

  @MockBean
  DocumentService documentService;

  @Autowired
  private FsmFactory               fsmFactory;
  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiesta = null;
  private Dispositivo dispositivo;

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = new Dispositivo();
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.ASSISTENZA_VEICOLI)
                                                                               .modalitaSpedizione(ModalitaSpedizione.A_FAI))
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();
    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
    fsmInoltroAssistenzaVeicoli = (FsmInoltroAssistenzaVeicoli) fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_ASSISTENZA_VEICOLI);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  @Test
  public void switchStateControlloDocumentiIncompletoTecnico() throws Exception {
    TipoDispositivo tipoDispositivo = new TipoDispositivo().nome(TipoDispositivoEnum.ASSISTENZA_VEICOLI);
    richiesta.setTipoDispositivo(tipoDispositivo);
    richiesta = fsmInoltroAssistenzaVeicoli.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INCOMPLETO_TECNICO);
  }

  @Test
  public void switchStateControlloDocumentiAttivoEvaso() throws Exception {
    TipoDispositivo tipoDispositivo = new TipoDispositivo().nome(TipoDispositivoEnum.ASSISTENZA_VEICOLI);
    DocumentoDTO documentoDTO = new DocumentoDTO();
    documentoDTO.setIdentificativo("ID_" + System.currentTimeMillis());
    Mockito.when(documentService.findLastDocument(richiesta.getContratto()
                                                           .getClienteFai()
                                                           .getCodiceCliente(),
                                                  DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                  TipoDispositivoEnum.ASSISTENZA_VEICOLI, null, null))
           .thenReturn(Optional.of(documentoDTO));
    richiesta.setTipoDispositivo(tipoDispositivo);
    richiesta = fsmInoltroAssistenzaVeicoli.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void switchStateIncompletoTecnicoControlloDocumentiKO() throws Exception {
    switchStateControlloDocumentiIncompletoTecnico();

    DocumentoDTO documentoDTO = null;
    Mockito.when(documentService.findLastDocument(richiesta.getContratto()
                                                           .getClienteFai()
                                                           .getCodiceCliente(),
                                                  DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                  TipoDispositivoEnum.ASSISTENZA_VEICOLI, null, null))
           .thenReturn(Optional.ofNullable(documentoDTO));

    richiesta = fsmInoltroAssistenzaVeicoli.executeCommandToChangeState(RichiestaEvent.MU_DOCUMENTO_CARICATO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INCOMPLETO_TECNICO);
  }

  @Test
  public void switchStateIncompletoTecnicoControlloDocumentiOK() throws Exception {
    switchStateControlloDocumentiIncompletoTecnico();
    DocumentoDTO documentoDTO = new DocumentoDTO();
    documentoDTO.setIdentificativo("ID_" + System.currentTimeMillis());
    Mockito.when(documentService.findLastDocument(richiesta.getContratto()
                                                           .getClienteFai()
                                                           .getCodiceCliente(),
                                                  DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                  TipoDispositivoEnum.ASSISTENZA_VEICOLI, null, null))
           .thenReturn(Optional.of(documentoDTO));

    richiesta = fsmInoltroAssistenzaVeicoli.executeCommandToChangeState(RichiestaEvent.MU_DOCUMENTO_CARICATO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }
}
