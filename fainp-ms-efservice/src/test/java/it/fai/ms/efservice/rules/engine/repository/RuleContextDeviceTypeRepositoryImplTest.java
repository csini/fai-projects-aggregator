package it.fai.ms.efservice.rules.engine.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;

@RunWith(MockitoJUnitRunner.class)
public class RuleContextDeviceTypeRepositoryImplTest {

  @Mock
  private TipoDispositivoRepository deviceTypeRepository;

  private RuleContextDeviceTypeRepositoryImpl ruleContextDeviceTypeRepository;

  @Before
  public void setUp() throws Exception {
    ruleContextDeviceTypeRepository = new RuleContextDeviceTypeRepositoryImpl(deviceTypeRepository);
  }

  @Test
  public void testFindAll() {
    given(deviceTypeRepository.findAll()).willReturn(newTipoDispositivoList());

    Set<RuleEngineDeviceType> allRuleContextDeviceTypes = ruleContextDeviceTypeRepository.findAll();

    verify(deviceTypeRepository).findAll();
    assertThat(allRuleContextDeviceTypes).containsExactlyInAnyOrder(new RuleEngineDeviceType(new RuleEngineDeviceTypeId("TELEPASS_EUROPEO")),
                                                                    new RuleEngineDeviceType(new RuleEngineDeviceTypeId("TELEPASS_ITALIANO")));
  }

  @Test
  public void testStar() {
    RuleEngineDeviceType contextDeviceType = ruleContextDeviceTypeRepository.newStarContextDeviceType();

    verifyNoMoreInteractions(deviceTypeRepository);
    assertThat(contextDeviceType.getId()).isEqualTo(RuleEngineDeviceTypeId.ofWildcard());
  }

  private List<TipoDispositivo> newTipoDispositivoList() {
    List<TipoDispositivo> deviceTypes = new LinkedList<>();
    TipoDispositivo deviceType1 = new TipoDispositivo();
    deviceType1.setNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    deviceTypes.add(deviceType1);
    TipoDispositivo deviceType2 = new TipoDispositivo();
    deviceType2.setNome(TipoDispositivoEnum.TELEPASS_ITALIANO);
    deviceTypes.add(deviceType2);
    return deviceTypes;
  }

}
