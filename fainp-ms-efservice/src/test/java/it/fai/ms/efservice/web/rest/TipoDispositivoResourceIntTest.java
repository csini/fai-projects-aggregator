package it.fai.ms.efservice.web.rest;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.TipoDispositivoService;
import it.fai.ms.efservice.service.dto.TipoDispositivoDTO;
import it.fai.ms.efservice.service.mapper.TipoDispositivoMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.ClassificazioneTipoDisp;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
/**
 * Test class for the TipoDispositivoResource REST controller.
 *
 * @see TipoDispositivoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoDispositivoResourceIntTest {

    private static final String DEFAULT_NOME_BUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_NOME_BUSINESS = "BBBBBBBBBB";

    private static final String DEFAULT_TESTO_PROMO = "AAAAAAAAAA";
    private static final String UPDATED_TESTO_PROMO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MULTISERVIZIO = false;
    private static final Boolean UPDATED_MULTISERVIZIO = true;

    private static final Boolean DEFAULT_VIRTUALE = false;
    private static final Boolean UPDATED_VIRTUALE = true;

    private static final ClassificazioneTipoDisp DEFAULT_CLASSIFICAZIONE = ClassificazioneTipoDisp.TESSERA;
    private static final ClassificazioneTipoDisp UPDATED_CLASSIFICAZIONE = ClassificazioneTipoDisp.TESSERA_VIRTUALE;

    private static final ModalitaSpedizione DEFAULT_MODALITA_SPEDIZIONE = ModalitaSpedizione.A_FAI;
    private static final ModalitaSpedizione UPDATED_MODALITA_SPEDIZIONE = ModalitaSpedizione.A_CLIENTE;

    private static final TipoDispositivoEnum DEFAULT_NOME = TipoDispositivoEnum.DARS_GO;
    private static final TipoDispositivoEnum UPDATED_NOME = TipoDispositivoEnum.GO_BOX;

    @Autowired
    private TipoDispositivoRepository tipoDispositivoRepository;

    @Autowired
    private TipoDispositivoMapper tipoDispositivoMapper;

    @Autowired
    private TipoDispositivoService tipoDispositivoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoDispositivoMockMvc;

    private TipoDispositivo tipoDispositivo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoDispositivoResource tipoDispositivoResource = new TipoDispositivoResource(tipoDispositivoService);
        this.restTipoDispositivoMockMvc = MockMvcBuilders.standaloneSetup(tipoDispositivoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoDispositivo createEntity(EntityManager em) {
        TipoDispositivo tipoDispositivo = new TipoDispositivo()
            .nomeBusiness(DEFAULT_NOME_BUSINESS)
            .testoPromo(DEFAULT_TESTO_PROMO)
            .descrizione(DEFAULT_DESCRIZIONE)
            .multiservizio(DEFAULT_MULTISERVIZIO)
            .virtuale(DEFAULT_VIRTUALE)
            .classificazione(DEFAULT_CLASSIFICAZIONE)
            .modalitaSpedizione(DEFAULT_MODALITA_SPEDIZIONE)
            .nome(DEFAULT_NOME);
        return tipoDispositivo;
    }

    @Before
    public void initTest() {
        tipoDispositivo = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoDispositivo() throws Exception {
        int databaseSizeBeforeCreate = tipoDispositivoRepository.findAll().size();

        // Create the TipoDispositivo
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(tipoDispositivo);
        restTipoDispositivoMockMvc.perform(post("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoDispositivo in the database
        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeCreate + 1);
        TipoDispositivo testTipoDispositivo = tipoDispositivoList.get(tipoDispositivoList.size() - 1);
        assertThat(testTipoDispositivo.getNomeBusiness()).isEqualTo(DEFAULT_NOME_BUSINESS);
        assertThat(testTipoDispositivo.getTestoPromo()).isEqualTo(DEFAULT_TESTO_PROMO);
        assertThat(testTipoDispositivo.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoDispositivo.isMultiservizio()).isEqualTo(DEFAULT_MULTISERVIZIO);
        assertThat(testTipoDispositivo.isVirtuale()).isEqualTo(DEFAULT_VIRTUALE);
        assertThat(testTipoDispositivo.getClassificazione()).isEqualTo(DEFAULT_CLASSIFICAZIONE);
        assertThat(testTipoDispositivo.getModalitaSpedizione()).isEqualTo(DEFAULT_MODALITA_SPEDIZIONE);
        assertThat(testTipoDispositivo.getNome()).isEqualTo(DEFAULT_NOME);
    }

    @Test
    @Transactional
    public void createTipoDispositivoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoDispositivoRepository.findAll().size();

        // Create the TipoDispositivo with an existing ID
        tipoDispositivo.setId(1L);
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(tipoDispositivo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoDispositivoMockMvc.perform(post("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TipoDispositivo in the database
        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMultiservizioIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoDispositivoRepository.findAll().size();
        // set the field null
        tipoDispositivo.setMultiservizio(null);

        // Create the TipoDispositivo, which fails.
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(tipoDispositivo);

        restTipoDispositivoMockMvc.perform(post("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isBadRequest());

        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVirtualeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoDispositivoRepository.findAll().size();
        // set the field null
        tipoDispositivo.setVirtuale(null);

        // Create the TipoDispositivo, which fails.
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(tipoDispositivo);

        restTipoDispositivoMockMvc.perform(post("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isBadRequest());

        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoDispositivos() throws Exception {
        // Initialize the database
        tipoDispositivoRepository.saveAndFlush(tipoDispositivo);

        // Get all the tipoDispositivoList
        restTipoDispositivoMockMvc.perform(get("/api/tipo-dispositivos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoDispositivo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeBusiness").value(hasItem(DEFAULT_NOME_BUSINESS.toString())))
            .andExpect(jsonPath("$.[*].testoPromo").value(hasItem(DEFAULT_TESTO_PROMO.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].multiservizio").value(hasItem(DEFAULT_MULTISERVIZIO.booleanValue())))
            .andExpect(jsonPath("$.[*].virtuale").value(hasItem(DEFAULT_VIRTUALE.booleanValue())))
            .andExpect(jsonPath("$.[*].classificazione").value(hasItem(DEFAULT_CLASSIFICAZIONE.toString())))
            .andExpect(jsonPath("$.[*].modalitaSpedizione").value(hasItem(DEFAULT_MODALITA_SPEDIZIONE.toString())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())));
    }

    @Test
    @Transactional
    public void getTipoDispositivo() throws Exception {
        // Initialize the database
        tipoDispositivoRepository.saveAndFlush(tipoDispositivo);

        // Get the tipoDispositivo
        restTipoDispositivoMockMvc.perform(get("/api/tipo-dispositivos/{id}", tipoDispositivo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoDispositivo.getId().intValue()))
            .andExpect(jsonPath("$.nomeBusiness").value(DEFAULT_NOME_BUSINESS.toString()))
            .andExpect(jsonPath("$.testoPromo").value(DEFAULT_TESTO_PROMO.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.multiservizio").value(DEFAULT_MULTISERVIZIO.booleanValue()))
            .andExpect(jsonPath("$.virtuale").value(DEFAULT_VIRTUALE.booleanValue()))
            .andExpect(jsonPath("$.classificazione").value(DEFAULT_CLASSIFICAZIONE.toString()))
            .andExpect(jsonPath("$.modalitaSpedizione").value(DEFAULT_MODALITA_SPEDIZIONE.toString()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTipoDispositivo() throws Exception {
        // Get the tipoDispositivo
        restTipoDispositivoMockMvc.perform(get("/api/tipo-dispositivos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoDispositivo() throws Exception {
        // Initialize the database
        tipoDispositivoRepository.saveAndFlush(tipoDispositivo);
        int databaseSizeBeforeUpdate = tipoDispositivoRepository.findAll().size();

        // Update the tipoDispositivo
        TipoDispositivo updatedTipoDispositivo = tipoDispositivoRepository.findOne(tipoDispositivo.getId());
        updatedTipoDispositivo
            .nomeBusiness(UPDATED_NOME_BUSINESS)
            .testoPromo(UPDATED_TESTO_PROMO)
            .descrizione(UPDATED_DESCRIZIONE)
            .multiservizio(UPDATED_MULTISERVIZIO)
            .virtuale(UPDATED_VIRTUALE)
            .classificazione(UPDATED_CLASSIFICAZIONE)
            .modalitaSpedizione(UPDATED_MODALITA_SPEDIZIONE)
            .nome(UPDATED_NOME);
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(updatedTipoDispositivo);

        restTipoDispositivoMockMvc.perform(put("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isOk());

        // Validate the TipoDispositivo in the database
        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeUpdate);
        TipoDispositivo testTipoDispositivo = tipoDispositivoList.get(tipoDispositivoList.size() - 1);
        assertThat(testTipoDispositivo.getNomeBusiness()).isEqualTo(UPDATED_NOME_BUSINESS);
        assertThat(testTipoDispositivo.getTestoPromo()).isEqualTo(UPDATED_TESTO_PROMO);
        assertThat(testTipoDispositivo.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoDispositivo.isMultiservizio()).isEqualTo(UPDATED_MULTISERVIZIO);
        assertThat(testTipoDispositivo.isVirtuale()).isEqualTo(UPDATED_VIRTUALE);
        assertThat(testTipoDispositivo.getClassificazione()).isEqualTo(UPDATED_CLASSIFICAZIONE);
        assertThat(testTipoDispositivo.getModalitaSpedizione()).isEqualTo(UPDATED_MODALITA_SPEDIZIONE);
        assertThat(testTipoDispositivo.getNome()).isEqualTo(UPDATED_NOME);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = tipoDispositivoRepository.findAll().size();

        // Create the TipoDispositivo
        TipoDispositivoDTO tipoDispositivoDTO = tipoDispositivoMapper.toDto(tipoDispositivo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoDispositivoMockMvc.perform(put("/api/tipo-dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoDispositivoDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoDispositivo in the database
        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoDispositivo() throws Exception {
        // Initialize the database
        tipoDispositivoRepository.saveAndFlush(tipoDispositivo);
        int databaseSizeBeforeDelete = tipoDispositivoRepository.findAll().size();

        // Get the tipoDispositivo
        restTipoDispositivoMockMvc.perform(delete("/api/tipo-dispositivos/{id}", tipoDispositivo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TipoDispositivo> tipoDispositivoList = tipoDispositivoRepository.findAll();
        assertThat(tipoDispositivoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoDispositivo.class);
        TipoDispositivo tipoDispositivo1 = new TipoDispositivo();
        tipoDispositivo1.setId(1L);
        TipoDispositivo tipoDispositivo2 = new TipoDispositivo();
        tipoDispositivo2.setId(tipoDispositivo1.getId());
        assertThat(tipoDispositivo1).isEqualTo(tipoDispositivo2);
        tipoDispositivo2.setId(2L);
        assertThat(tipoDispositivo1).isNotEqualTo(tipoDispositivo2);
        tipoDispositivo1.setId(null);
        assertThat(tipoDispositivo1).isNotEqualTo(tipoDispositivo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoDispositivoDTO.class);
        TipoDispositivoDTO tipoDispositivoDTO1 = new TipoDispositivoDTO();
        tipoDispositivoDTO1.setId(1L);
        TipoDispositivoDTO tipoDispositivoDTO2 = new TipoDispositivoDTO();
        assertThat(tipoDispositivoDTO1).isNotEqualTo(tipoDispositivoDTO2);
        tipoDispositivoDTO2.setId(tipoDispositivoDTO1.getId());
        assertThat(tipoDispositivoDTO1).isEqualTo(tipoDispositivoDTO2);
        tipoDispositivoDTO2.setId(2L);
        assertThat(tipoDispositivoDTO1).isNotEqualTo(tipoDispositivoDTO2);
        tipoDispositivoDTO1.setId(null);
        assertThat(tipoDispositivoDTO1).isNotEqualTo(tipoDispositivoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tipoDispositivoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tipoDispositivoMapper.fromId(null)).isNull();
    }
}
