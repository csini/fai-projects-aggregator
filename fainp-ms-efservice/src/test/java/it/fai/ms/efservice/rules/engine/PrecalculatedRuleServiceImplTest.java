package it.fai.ms.efservice.rules.engine;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;

@RunWith(MockitoJUnitRunner.class)
public class PrecalculatedRuleServiceImplTest {

  @Mock
  private RuleOutcomeRepository ruleOutcomeRepository;

  private RuleEngineServiceTypeId serviceTypeId = new RuleEngineServiceTypeId("::serviceTypeId::");
  private RuleEngineDeviceTypeId  deviceTypeId  = new RuleEngineDeviceTypeId("::deviceTypeId::");
  private RuleEngineVehicleId     vehicleId_1   = new RuleEngineVehicleId("::vehicleId_1::");
  private RuleEngineVehicleId     vehicleId_2   = new RuleEngineVehicleId("::vehicleId_2::");

  @Captor
  private ArgumentCaptor<RuleEngineServiceTypeId> captorForServiceTypeId;
  @Captor
  private ArgumentCaptor<RuleEngineDeviceTypeId>  captorForDeviceTypeId;
  @Captor
  private ArgumentCaptor<RuleEngineVehicleId>     captorForVehicleId;

  @Before
  public void setUp() throws Exception {
    when(ruleOutcomeRepository.find(serviceTypeId, deviceTypeId, vehicleId_1)).thenReturn(Optional.ofNullable(new RuleOutcome()));
    when(ruleOutcomeRepository.find(serviceTypeId, deviceTypeId,
                                    vehicleId_2)).thenReturn(Optional.ofNullable(new RuleOutcome(new RuleFailure("::code::", "::mess::"))));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void whenRuleExecutionResultIsPositive() {
    PrecalculatedRuleServiceImpl precalculatedRulesService = new PrecalculatedRuleServiceImpl(ruleOutcomeRepository);
    Optional<RuleOutcome> ruleOutcome = precalculatedRulesService.execute(serviceTypeId, deviceTypeId, vehicleId_1);

    verify(ruleOutcomeRepository).find(captorForServiceTypeId.capture(), captorForDeviceTypeId.capture(), captorForVehicleId.capture());
    assertThat(captorForServiceTypeId.getValue()).isEqualTo(new RuleEngineServiceTypeId("::serviceTypeId::"));
    assertThat(captorForDeviceTypeId.getValue()).isEqualTo(new RuleEngineDeviceTypeId("::deviceTypeId::"));
    assertThat(captorForVehicleId.getValue()).isEqualTo(new RuleEngineVehicleId("::vehicleId_1::"));

    assertThat(ruleOutcome.get()
                          .getOutcome()).isTrue();
    assertThat(ruleOutcome.get()
                          .getFailure()).isEmpty();
  }

  @Test
  public void whenRuleExecutionResultIsNegative() {
    PrecalculatedRuleServiceImpl precalculatedRulesService = new PrecalculatedRuleServiceImpl(ruleOutcomeRepository);
    Optional<RuleOutcome> ruleOutcome = precalculatedRulesService.execute(serviceTypeId, deviceTypeId, vehicleId_2);

    verify(ruleOutcomeRepository).find(captorForServiceTypeId.capture(), captorForDeviceTypeId.capture(), captorForVehicleId.capture());
    assertThat(captorForServiceTypeId.getValue()).isEqualTo(new RuleEngineServiceTypeId("::serviceTypeId::"));
    assertThat(captorForDeviceTypeId.getValue()).isEqualTo(new RuleEngineDeviceTypeId("::deviceTypeId::"));
    assertThat(captorForVehicleId.getValue()).isEqualTo(new RuleEngineVehicleId("::vehicleId_2::"));

    assertThat(ruleOutcome.get()
                          .getOutcome()).isFalse();
    assertThat(ruleOutcome.get()
                          .getFailure()).hasValue(new RuleOutcome.RuleFailure("::code::", "::mess::"));
  }

}
