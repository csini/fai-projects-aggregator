package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_TrackyCardTest {

  @Test
  public void testServiceNotIsSloveniaAndSvizzera() {
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle(),
                                                               newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testServiceIsSloveniaFailed() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SLOVENIA");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle(),
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight are not set or less/equal than 3500");
  }

  @Test
  public void testServiceIsSloveniaAndGrossGreaterThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SLOVENIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle,
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testServiceIsSloveniaAndGrossLessOrEqualThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SLOVENIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle,
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight are not set or less/equal than 3500");
  }

  @Test
  public void testServiceIsSvizzeraFailed() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SVIZZERA");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle(),
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight are not set or less/equal than 3500");
  }

  @Test
  public void testServiceIsSvizzeraAndGrossGreaterThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SVIZZERA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle,
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testServiceIsSvizzeraAndGrossLessOrEqualThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_SVIZZERA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TrackyCard(newRuleContextDeviceType(), newRuleContextVehicle,
                                                               newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight are not set or less/equal than 3500");
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType() {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId("::serviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

}
