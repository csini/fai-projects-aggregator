/**
 * 
 */
package it.fai.ms.efservice.service.fsm.testdomain;

import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;

/**
 * @author Luca Vassallo
 */
public class TestFSM implements FSMEntityTest {

  private int id;

  private String identificativo;

  private String campo1 = "";

  private String campo2 = "";

  private String stato;

  private String ultimaNota;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCampo1() {
    return campo1;
  }

  public void setCampo1(String campo1) {
    this.campo1 = campo1;
  }

  public String getCampo2() {
    return campo2;
  }

  public void setCampo2(String campo2) {
    this.campo2 = campo2;
  }

  private String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getUltimaNota() {
    return ultimaNota;
  }

  @Override
  public Enum<?> findFsmStatus() {
    return StateTest.valueOf(getStato());
  }

  @Override
  public void storeFsmStatus(Enum<?> fsmStatus) {
    this.stato = fsmStatus.name();
  }

  @Override
  public void storeUltimaNota(String ultimaNota) {
    this.ultimaNota = ultimaNota;
  }

  @Override
  public String getIdentificativo() {
    return this.identificativo;
  }

  @Override
  public void setIdentificativo(String identificativo) {
    this.identificativo = identificativo;
  }

}
