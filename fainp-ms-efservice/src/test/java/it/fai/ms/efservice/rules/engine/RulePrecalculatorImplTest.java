package it.fai.ms.efservice.rules.engine;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;
import it.fai.ms.efservice.rules.enumeration.RuleBucket;

@RunWith(MockitoJUnitRunner.class)
public class RulePrecalculatorImplTest {

  private RulePrecalculatorImpl rulePrecalculator;

  @Spy
  private RuleEngineFactory     mockruleEngineFactory;
  @Mock
  private RuleOutcomeRepository mockRuleOutcomeRepository;
  @Mock
  private RuleEngine            mockRuleEngine;

  @Before
  public void setUp() throws Exception {
    rulePrecalculator = new RulePrecalculatorImpl(mockruleEngineFactory, mockRuleOutcomeRepository);
  }

  @Test
  public void testProcess() {

    RuleContext ruleContext = new RuleContext(new RuleEngineServiceType(new RuleEngineServiceTypeId("::serviceId::")),
                                              new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::")),
                                              new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::")));

    when(mockruleEngineFactory.newRuleEngine(ruleContext, RuleBucket.DEVICE_TELEPASS_EU)).thenReturn(mockRuleEngine);
    when(mockRuleEngine.executeRule()).thenReturn(new RuleOutcome());

    rulePrecalculator.process(ruleContext, RuleBucket.DEVICE_TELEPASS_EU);

    verify(mockruleEngineFactory).newRuleEngine(ruleContext, RuleBucket.DEVICE_TELEPASS_EU);
    verifyNoMoreInteractions(mockruleEngineFactory);

    verify(mockRuleEngine).executeRule();
    verifyNoMoreInteractions(mockRuleEngine);

    verify(mockRuleOutcomeRepository).save(ruleContext, new RuleOutcome());
    verifyNoMoreInteractions(mockRuleOutcomeRepository);
  }

}
