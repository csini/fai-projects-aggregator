package it.fai.ms.efservice.service.jms;

import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.telepass.TipoEventoOutbound;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.fsm.bean.telepass.TelepassEuEventiOutboundDTO;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;
import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import it.fai.ms.integration.system.sock.config.FaiMessageSystemLoader;
import it.fai.ms.integration.system.sock.config.ServiceSources;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.mockito.Mockito.*;
@RunWith(SpringRunner.class)
@ComponentScan( basePackages = { "it.fai.ms.efservice",
  "it.fai.common.notification.domain"})
@SpringBootTest(
  classes = {JmsConfiguration.class,  ServiceSources.class,
    FaiMessageSystemLoader.class})
@Import({FsmSenderToQueue.class, JmsTopicSenderService.class})
public class JmsTopicSenderServiceTest {
  @Mock
  Logger log;
  @Inject
  JmsProperties jmsProperties;
  @Inject
  OrdineClienteServiceExt ordineClienteServiceExt;
  //@Inject
  FaiMessageSystem faiMessageSystem;
  @Inject
  JmsTopicSenderService jmsTopicSenderService;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testPublishLogTopic() throws Exception {
    jmsTopicSenderService.publishLogTopic(null);
  }

  @Test
  public void testPublishTelepassEuEventiOutboundDTO() throws Exception {
    when(ordineClienteServiceExt.findByIdentificativoOrdineCliente(anyString())).thenReturn(new OrdineCliente());

    jmsTopicSenderService.publishTelepassEuEventiOutboundDTO(new TelepassEuEventiOutboundDTO(TipoEventoOutbound.INOLTRO_RICHIEDI_CONTRATTO, "identificativoRichiesta", "identificativoOrdineCliente", "codiceClienteFai"));
  }

  @Test
  public void testPublishRipetiInvioMessage() throws Exception {
    when(ordineClienteServiceExt.loadClienteAssegnatarioOnOrdineCliente(anyLong())).thenReturn(new OrdineCliente());

    jmsTopicSenderService.publishRipetiInvioMessage(new RichiestaDTO());
  }

  @Test
  public void testPublishFsmCommandDispositivoMessage() throws Exception {
    jmsTopicSenderService.publishFsmCommandDispositivoMessage(null);
  }

  @Test
  public void testPublishTelepassEuNuovoDispositivoMessage() throws Exception {
    jmsTopicSenderService.publishTelepassEuNuovoDispositivoMessage(null);
  }

  @Test
  public void testPublishMezzoRitargatoMessage() throws Exception {
    jmsTopicSenderService.publishMezzoRitargatoMessage(null);
  }

  @Test
  public void testPublishDenunciaAllineamentoMessage() throws Exception {
    jmsTopicSenderService.publishDenunciaAllineamentoMessage(null);
  }

  @Test
  public void testPublishGiustificativoMessage() throws Exception {
    jmsTopicSenderService.publishGiustificativoMessage(null);
  }

  @Test
  public void testPublishDispositiviDaSpedireMessage() throws Exception {
    jmsTopicSenderService.publishDispositiviDaSpedireMessage(null);
  }

  @Test
  public void testPublishRequestNewDispositivoMessage() throws Exception {
    jmsTopicSenderService.publishRequestNewDispositivoMessage(null);
  }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
