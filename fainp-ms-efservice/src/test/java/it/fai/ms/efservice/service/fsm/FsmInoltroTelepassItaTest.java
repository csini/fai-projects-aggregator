package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmAccettazione;
import it.fai.ms.efservice.service.fsm.type.telepass.ita.FsmInoltroTelepassItaliano;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroTelepassItaTest {

  private final static String CODICE_AZIENDA = "0073935";

  @Autowired
  private FsmInoltroTelepassItaliano fsmInoltroTi;

  @Autowired
  private FsmAccettazione fsmAccettazione;

  @Autowired
  private RichiestaRepositoryExt richiestaRepo;

  @Autowired
  private FsmRichiestaCacheService cache;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  @Autowired
  private DispositivoServiceExt dispositivoService;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepo;

  @Autowired
  private ContrattoServiceExt contrattoService;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  private Richiesta richiesta;

  private Dispositivo dispositivo;

  private TipoDispositivo tipoDispositivo;

  private Contratto contract;

  private boolean isNewContract = false;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache(String.valueOf(977871L));

    tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TELEPASS_ITALIANO);
    dispositivo = new Dispositivo().stato(StatoDispositivo.IN_DEPOSITO)
                                   .dataModificaStato(Instant.now())
                                   .tipoDispositivo(tipoDispositivo)
                                   .seriale(UUID.randomUUID()
                                                .toString());
    dispositivo = dispositivoService.save(dispositivo);

    richiesta = newRichiesta();
  }

  @After
  public void afterTest() {
    if (isNewContract) {
      contrattoService.delete(contract);
    }

    if (dispositivo != null) {
      Dispositivo deviceFound = dispositivoRepo.findOne(dispositivo.getId());
      if (deviceFound != null) {
        dispositivoRepo.delete(deviceFound);
      }
    }
    if (richiesta != null) {
      richiestaRepo.delete(richiesta);
    }
  }

  @Test
  @Transactional
  @Ignore
  public void FsmInoltroTelepassIta_001_test() throws Exception {
    richiesta = fsmAccettazione.executeCommandToChangeState(RichiestaEvent.MU_ACCETTA_RICHIESTA, richiesta);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.ACCETTATO);

    richiesta = fsmInoltroTi.executeCommandToChangeState(RichiestaEvent.CHECK_VIACARD, richiesta);
    stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.INCOMPLETO_TECNICO_NO_VC);
  }

  @Test
  @Transactional
  @Ignore
  public void FsmInoltroTelepassIta_002_test() throws Exception {
    FsmInoltroTelepassIta_001_test();

    TipoDispositivo tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.VIACARD);
    Dispositivo dispositivo = new Dispositivo().stato(StatoDispositivo.ATTIVO)
                                               .dataModificaStato(Instant.now())
                                               .tipoDispositivo(tipoDispositivo)
                                               .seriale(UUID.randomUUID()
                                                            .toString());
    dispositivo.setContratto(contract);
    dispositivo = dispositivoService.save(dispositivo);

    richiesta = fsmInoltroTi.executeCommandToChangeState(RichiestaEvent.CHECK_VIACARD, richiesta);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.IN_ATTESA_INOLTRO);

    if (dispositivo != null) {
      dispositivoRepo.delete(dispositivo);
    }
  }

  // ****** Utils method ************

  private Richiesta newRichiesta() {
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                                         .data(Instant.now())
                                         .contratto(newContratto())
                                         .ordineCliente(newOrdineCliente());
    richiesta.setId(977871L);
    Dispositivo newDispositivo = newDispositivo();
    richiesta.addDispositivo(newDispositivo);
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().clienteAssegnatario(newClienteFai());
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }

  private Contratto newContratto() {
    Produttore produttore = tipoDispositivo.getProduttore();
    Optional<Contratto> optContratto = contrattoService.findContrattoByProduttoreAndCodiceAzienda(produttore, CODICE_AZIENDA);

    if (!optContratto.isPresent()) {
      contract = contrattoService.createContrattoByProduttoreAndClienteFai(produttore, newClienteFai());
      isNewContract = true;
    } else {
      contract = optContratto.get();
    }

    return contract;
  }

  private Dispositivo newDispositivo() {
    Dispositivo disp = new Dispositivo().stato(StatoDispositivo.INIZIALE)
                                        .dataModificaStato(Instant.now())
                                        .tipoDispositivo(tipoDispositivo)
                                        .seriale("ABCD-1234");
    return disp;
  }

}