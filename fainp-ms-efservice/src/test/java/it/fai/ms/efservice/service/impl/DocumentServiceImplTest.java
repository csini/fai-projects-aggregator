package it.fai.ms.efservice.service.impl;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.service.DocumentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceImplTest {

  private static final String EXPECTED_AUTH_TOKEN = "token1234";
  private DocumentServiceImpl documentService;

  @Mock
  private DocumentClient documentClient;

  @Before
  public void init(){
    documentService = new DocumentServiceImpl();
    documentService.setDocumentClient(documentClient);
    documentService.setAuthorizationHeader(EXPECTED_AUTH_TOKEN);
  }

  @Test
  public void getDocumentRichiestaContrattoFound(){
    String expectedCodiceAzienda = "100586";

    DocumentService.TipologiaDocumento expectedTipologiaDocumento = DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO;
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("tipoDispositivo","TOLL_COLLECT");
    DocumentoDTO expectedDocumentDTO = new DocumentoDTO();
    expectedDocumentDTO.setCliente("cliente1234");
    expectedDocumentDTO.setId(1l);
    when(documentClient.findLastDocument(EXPECTED_AUTH_TOKEN,expectedCodiceAzienda, "RICHIESTA_CONTRATTO",null,null))
      .thenReturn(expectedDocumentDTO);
    Optional<DocumentoDTO> documentoDTOOptional = documentService
      .findLastDocument(
      expectedCodiceAzienda,
        expectedTipologiaDocumento,
      TipoDispositivoEnum.TOLL_COLLECT,
      null,
      null);
    assertThat(documentoDTOOptional).isPresent();
    assertThat(documentoDTOOptional.get()).isEqualTo(expectedDocumentDTO);
  }
  @Test
  public void getDocumentRichiestaContrattoNotFound(){
    String expectedCodiceAzienda = "100586";

    DocumentService.TipologiaDocumento expectedTipologiaDocumento = DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO;
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("tipoDispositivo","TOLL_COLLECT");
    DocumentoDTO expectedDocumentDTO = new DocumentoDTO();
    expectedDocumentDTO.setCliente("cliente1234");
    expectedDocumentDTO.setId(1l);
    when(documentClient.findLastDocument(EXPECTED_AUTH_TOKEN,expectedCodiceAzienda, "RICHIESTA_CONTRATTO",null,null))
      .thenReturn(null);
    Optional<DocumentoDTO> documentoDTOOptional = documentService
      .findLastDocument(
        expectedCodiceAzienda,
        expectedTipologiaDocumento,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,
        null);
    assertThat(documentoDTOOptional).isNotPresent();
  }
  @Test
  public void getDocumentRichiestaDispostivoFound(){
    String expectedCodiceAzienda = "100586";

    DocumentService.TipologiaDocumento expectedTipologiaDocumento = DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO;
    String expectedTarga = "123456";
    String expectedTargaNazione = "12";
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("tipoDispositivo","TOLL_COLLECT");
    queryParams.put("targa", expectedTarga);
    queryParams.put("targaNazione", expectedTargaNazione);
    DocumentoDTO expectedDocumentDTO = new DocumentoDTO();
    expectedDocumentDTO.setCliente("cliente1234");
    expectedDocumentDTO.setId(1l);
    when(documentClient.findLastDocument(EXPECTED_AUTH_TOKEN,expectedCodiceAzienda, "RICHIESTA_DISPOSITIVO",expectedTarga,expectedTargaNazione))
      .thenReturn(expectedDocumentDTO);
    Optional<DocumentoDTO> documentoDTOOptional = documentService
      .findLastDocument(
        expectedCodiceAzienda,
        expectedTipologiaDocumento,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,
        expectedTargaNazione);
    assertThat(documentoDTOOptional).isPresent();
    assertThat(documentoDTOOptional.get()).isEqualTo(expectedDocumentDTO);
  }
  @Test
  public void getDocumentRichiestaDispostivoNotFound(){
    String expectedCodiceAzienda = "100586";

    DocumentService.TipologiaDocumento expectedTipologiaDocumento = DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO;
    String expectedTarga = "123456";
    String expectedTargaNazione = "12";
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("tipoDispositivo","TOLL_COLLECT");
    queryParams.put("targa", expectedTarga);
    queryParams.put("targaNazione", expectedTargaNazione);
    DocumentoDTO expectedDocumentDTO = new DocumentoDTO();
    expectedDocumentDTO.setCliente("cliente1234");
    expectedDocumentDTO.setId(1l);
    when(documentClient.findLastDocument(EXPECTED_AUTH_TOKEN,expectedCodiceAzienda, "RICHIESTA_DISPOSITIVO",expectedTarga,expectedTargaNazione))
      .thenReturn(null);
    Optional<DocumentoDTO> documentoDTOOptional = documentService
      .findLastDocument(
        expectedCodiceAzienda,
        expectedTipologiaDocumento,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,
        expectedTargaNazione);
    assertThat(documentoDTOOptional).isNotPresent();
  }

}
