package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.client.dto.VehicleDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.web.rest.errors.CustomException;

public class RichiestaModificaServiceImplExtTest {

  private RichiestaModificaServiceImplExt richiestaModificaServiceImpl;

  private RichiestaModificaGenerationServiceImpl richiestaModificaGeneratorService;

  private ClienteFaiServiceExt clienteFaiService;

  private DispositivoServiceExt deviceServiceExt;

  private AssociazioneDVServiceExt associazioneDvService;

  private RichiestaServiceExt     _richiestaServiceExt  = mock(RichiestaServiceExt.class);
  private OrdineClienteServiceExt _ordineClienteService = mock(OrdineClienteServiceExt.class);
  private VehicleClient           _vehicleClient        = mock(VehicleClient.class);

  private DeviceAssociationService deviceAssociationService = mock(DeviceAssociationService.class);

  private final RequestPinService requestPinService = mock(RequestPinService.class);

  private JmsProperties mockJmsProperties = mock(JmsProperties.class);

  private String device1 = "device_1";
  private String device2 = "device_2";

  @Before
  public void setUp() throws Exception {

    clienteFaiService = mock(ClienteFaiServiceExt.class);
    deviceServiceExt = mock(DispositivoServiceExt.class);
    RequestTheftLossMalfunctionServiceImpl _requestTheftLossMalfunctionService = new RequestTheftLossMalfunctionServiceImpl(deviceServiceExt,
                                                                                                                            clienteFaiService);
    associazioneDvService = mock(AssociazioneDVServiceExt.class);
    RequestVehicleChangeServiceImpl _requestVehicleChangeService = new RequestVehicleChangeServiceImpl(associazioneDvService,
                                                                                                       deviceServiceExt, clienteFaiService);
    RequestVehicleLicensePlateChangeServiceImpl _requestVehicleLicensePlateChangeService = new RequestVehicleLicensePlateChangeServiceImpl(associazioneDvService,
                                                                                                                                           deviceServiceExt,
                                                                                                                                           clienteFaiService,
                                                                                                                                           mockJmsProperties);
    richiestaModificaGeneratorService = new RichiestaModificaGenerationServiceImpl(_requestTheftLossMalfunctionService,
                                                                                   _requestVehicleLicensePlateChangeService,
                                                                                   _requestVehicleChangeService, deviceAssociationService,
                                                                                   requestPinService);

    richiestaModificaServiceImpl = new RichiestaModificaServiceImplExt(_richiestaServiceExt, _vehicleClient,
                                                                       richiestaModificaGeneratorService, _ordineClienteService,
                                                                       deviceServiceExt);
  }

  @Test
  public void testFurtoTrackyCard() throws Exception {

    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.FURTO;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.TRACKYCARD;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);

    when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(new Dispositivo().identificativo(device1)
                                                                                        .stato(StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE)
                                                                                        .tipoDispositivo(newTipoDispositivo(deviceTypes)));

    List<Richiesta> requests = richiestaModificaGeneratorService.generateRichieste(carrelloDTO);
    assertThat(requests).hasSize(1);

  }

  @Test
  public void testVariazioneTargaTrackyCard() throws Exception {

    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.VARIAZIONE_TARGA;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.TRACKYCARD;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);

    when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(newDispositivo(deviceTypes));

    List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();
    particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);

    when(associazioneDvService.findAndFilterActiveStatusAndDeviceTypeByVehicleAndClientCode("uuidVeicolo", "::codCliente::",
                                                                                            particularDeviceTypes)).thenReturn(null);

    AssociazioneDV assDV = newAssociazioneDV(TipoDispositivoEnum.TRACKYCARD);
    List<AssociazioneDV> assDvs = new ArrayList<>();
    assDvs.add(assDV);
    when(associazioneDvService.findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::")).thenReturn(assDvs);

    List<Richiesta> requests = richiestaModificaGeneratorService.generateRichieste(carrelloDTO);

    verify(deviceServiceExt, times(0)).findOneByIdentificativo(device1);

    verify(associazioneDvService, times(1)).findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::");

    assertThat(requests).hasSize(1);
  }

  @Test(expected = CustomException.class)
  public void testVariazioneTargaCreateZeroRequest() throws Exception {

    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.VARIAZIONE_TARGA;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.VIACARD;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);

    when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(newDispositivo(deviceTypes));

    List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();
    particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);

    AssociazioneDV assDV = newAssociazioneDV(TipoDispositivoEnum.VIACARD);
    List<AssociazioneDV> assDvs = new ArrayList<>();
    assDvs.add(assDV);
    when(associazioneDvService.findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::")).thenReturn(new ArrayList<>());

    List<Richiesta> requests = richiestaModificaGeneratorService.generateRichieste(carrelloDTO);
    assertThat(requests).hasSize(0);
  }

  @Test
  public void testVariazioneTargaViaCard() throws Exception {
    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.VARIAZIONE_TARGA;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.VIACARD;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);

    when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(newDispositivo(deviceTypes));

    List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();
    particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);

    when(clienteFaiService.findByCodiceCliente("::codCliente::")).thenReturn(new ClienteFai());

    AssociazioneDV assDV = newAssociazioneDV(TipoDispositivoEnum.VIACARD);
    when(associazioneDvService.findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::")).thenReturn(Arrays.asList(assDV));

    when(associazioneDvService.assignNewUuidVehicle(assDV,
                                                    carrelloDTO.getNewVeicolo())).thenReturn(assDV.uuidVeicolo(carrelloDTO.getNewVeicolo()));

    List<Richiesta> requests = richiestaModificaGeneratorService.generateRichieste(carrelloDTO);
    assertThat(requests).hasSize(0);

    String newVeicolo = carrelloDTO.getNewVeicolo();
    verify(associazioneDvService).assignNewUuidVehicle(assDV, newVeicolo);
  }

  @Test
  public void testVariazioneTargaTelepassEuropeoSat() throws Exception {
    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.VARIAZIONE_TARGA;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.TELEPASS_EUROPEO_SAT;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);

    VehicleDTO vehicleDTO = new VehicleDTO();
    vehicleDTO.setTarga("::targaNew::");
    vehicleDTO.setNazione("::nazioneNew::");
    when(_vehicleClient.findVehicleByUUID(Mockito.any(), Mockito.eq(carrelloDTO.getNewVeicolo()))).thenReturn(vehicleDTO);

    VehicleDTO vehicleOld = new VehicleDTO();
    vehicleOld.setTarga("OLD");
    vehicleOld.setNazione("IT_OLD");
    when(_vehicleClient.findVehicleByUUID(Mockito.any(), Mockito.eq(carrelloDTO.getVeicolo()))).thenReturn(vehicleOld);

    // when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(newDispositivo(deviceTypes).identificativo(device1));
    //
    // List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();
    // particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);

    when(clienteFaiService.findByCodiceCliente("::codCliente::")).thenReturn(new ClienteFai());

    AssociazioneDV assDV = newAssociazioneDV(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT);
    assDV.getDispositivo()
         .identificativo(device1)
         .stato(StatoDispositivo.ATTIVO);
    when(associazioneDvService.findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::")).thenReturn(Arrays.asList(assDV));
    //
    // when(associazioneDvService.assignNewUuidVehicle(assDV,
    // carrelloDTO.getNewVeicolo())).thenReturn(assDV.uuidVeicolo(carrelloDTO.getNewVeicolo()));
    List<Long> ids = richiestaModificaServiceImpl.createRichiestaModifica(carrelloDTO);

    assertThat(ids).hasSize(0); // this is zero beacuse save is mokkato

    verify(_vehicleClient, times(2)).findVehicleByUUID(Mockito.any(), Mockito.eq(carrelloDTO.getVeicolo()));
    verify(_vehicleClient).findVehicleByUUID(Mockito.any(), Mockito.eq(carrelloDTO.getNewVeicolo()));
  }

  @Test
  public void testVariazioneTargaViaCardMulti() throws Exception {
    TipoRichiesta tipoOperazioneModifica = TipoRichiesta.VARIAZIONE_TARGA;
    TipoDispositivoEnum deviceTypes = TipoDispositivoEnum.VIACARD;
    CarrelloModificaRichiestaDTO carrelloDTO = newCarrelloModificaDTO(tipoOperazioneModifica, deviceTypes);
    carrelloDTO.addDispositivo(device2);

    when(deviceServiceExt.findOneByIdentificativo(device1)).thenReturn(newDispositivo(deviceTypes));
    when(deviceServiceExt.findOneByIdentificativo(device2)).thenReturn(newDispositivo(deviceTypes).identificativo(device2));

    List<TipoDispositivoEnum> particularDeviceTypes = new ArrayList<>();
    particularDeviceTypes.add(TipoDispositivoEnum.VIACARD);

    when(clienteFaiService.findByCodiceCliente("::codCliente::")).thenReturn(new ClienteFai());

    AssociazioneDV assDV = newAssociazioneDV(TipoDispositivoEnum.VIACARD);
    AssociazioneDV assDV2 = newAssociazioneDV(TipoDispositivoEnum.VIACARD);
    assDV2.getDispositivo()
          .identificativo(device2);
    when(associazioneDvService.findByUuidVeicoloAndCodiceCliente("uuidVeicolo", "::codCliente::")).thenReturn(Arrays.asList(assDV, assDV2));

    when(associazioneDvService.assignNewUuidVehicle(assDV,
                                                    carrelloDTO.getNewVeicolo())).thenReturn(assDV.uuidVeicolo(carrelloDTO.getNewVeicolo()));

    when(associazioneDvService.assignNewUuidVehicle(assDV2,
                                                    carrelloDTO.getNewVeicolo())).thenReturn(assDV2.uuidVeicolo(carrelloDTO.getNewVeicolo()));

    List<Richiesta> requests = richiestaModificaGeneratorService.generateRichieste(carrelloDTO);
    assertThat(requests).hasSize(0);

    String newVeicolo = carrelloDTO.getNewVeicolo();
    verify(associazioneDvService).assignNewUuidVehicle(assDV, newVeicolo);
    verify(associazioneDvService).assignNewUuidVehicle(assDV2, newVeicolo);
  }

  private AssociazioneDV newAssociazioneDV(TipoDispositivoEnum deviceTypes) {
    return new AssociazioneDV().data(Instant.now())
                               .dispositivo(newDispositivo(deviceTypes))
                               .uuidVeicolo("uuidVehicle");
  }

  private Dispositivo newDispositivo(TipoDispositivoEnum deviceTypes) {
    return new Dispositivo().identificativo(device1)
                            .stato(StatoDispositivo.ATTIVO_SPEDITO_DAL_FORNITORE)
                            .tipoDispositivo(newTipoDispositivo(deviceTypes));
  }

  private TipoDispositivo newTipoDispositivo(TipoDispositivoEnum deviceTypes) {
    return new TipoDispositivo().nome(deviceTypes);
  }

  private CarrelloModificaRichiestaDTO newCarrelloModificaDTO(TipoRichiesta tipoOperazioneModifica, TipoDispositivoEnum deviceTypes) {
    CarrelloModificaRichiestaDTO cartDTO = new CarrelloModificaRichiestaDTO();
    cartDTO.setTipoOperazioneModifica(tipoOperazioneModifica);
    cartDTO.setTipoDispositivo(deviceTypes);

    cartDTO.setCodiceAzienda("::codCliente::");
    List<String> devices = new ArrayList<>();
    devices.add(device1);
    cartDTO.setDispositivi(devices);
    cartDTO.setDocumento("documento");
    cartDTO.setNazione("IT");
    cartDTO.setRichiedente("::richiedente::");
    cartDTO.setVeicolo("uuidVeicolo");
    cartDTO.setNewVeicolo("newUuidVehicle");
    return cartDTO;
  }

}
