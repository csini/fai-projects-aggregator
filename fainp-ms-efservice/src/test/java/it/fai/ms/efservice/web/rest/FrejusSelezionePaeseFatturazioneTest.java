package it.fai.ms.efservice.web.rest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.service.frejus.ContrattiFrejusService;
import it.fai.ms.efservice.web.rest.FrejusSelezionePaeseFatturazione.NazioneFatturazione;

public class FrejusSelezionePaeseFatturazioneTest {

	private ContrattiFrejusService contrattiService = mock(ContrattiFrejusService.class);
	private MockMvc subject;

	@Before
	public void setup() {
		subject = MockMvcBuilders.standaloneSetup(new FrejusSelezionePaeseFatturazione(contrattiService)).build();
	}

	@Test
	public void testSaveNazioneFatturazione() throws Exception {
		when(contrattiService.saveNazioneFatturazione("az1", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO, "IT"))
				.thenReturn(Optional.of("IT"));

		subject.perform(post("/api/public/nazioneFatturazione/az1/" + TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(new NazioneFatturazione("IT")))).andExpect(status().is(200))
				.andExpect(content().json("{nazioneFatturazione:\"IT\"}"));
	}

	@Test
	public void testChangeNazioneFatturazione() throws Exception {
		when(contrattiService.changeNazioneFatturazione("az1", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO, "IT"))
				.thenReturn(Optional.of("IT"));

		subject.perform(put("/api/public/nazioneFatturazione/az1/" + TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(new NazioneFatturazione("IT")))).andExpect(status().is(200))
				.andExpect(content().json("{nazioneFatturazione:\"IT\"}"));
	}

	@Test
	public void testGetNazioneFatturazione() throws Exception {
		when(contrattiService.getNazioneFatturazione("az1", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
				.thenReturn(Optional.of("IT"));

		subject.perform(get("/api/public/nazioneFatturazione/az1/" + TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
				.andExpect(status().is(200)).andExpect(content().json("{nazioneFatturazione:\"IT\"}"));
	}

	@Test
	public void testGetNazioneFatturazioneEmpty() throws Exception {
		when(contrattiService.getNazioneFatturazione("az1", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
				.thenReturn(Optional.empty());

		subject.perform(get("/api/public/nazioneFatturazione/az1/" + TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
				.andExpect(status().is(404));
	}

}
