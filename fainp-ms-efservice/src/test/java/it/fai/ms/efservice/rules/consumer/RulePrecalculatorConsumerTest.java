package it.fai.ms.efservice.rules.consumer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class RulePrecalculatorConsumerTest {

  @Autowired
  private RulePrecalculatorConsumer consumer;

  @Autowired
  private RuleOutcomeRepository repository;

  @Test
  public void test() {
    VehicleMessage vMsg = new VehicleMessage();
    String vehicleUUID = "::vehicleUUID::";
    vMsg.setUuid("::vehicleUUID::");
    vMsg.setLicensePlate("::targa::");
    vMsg.setCountry("::country::");
    vMsg.setMake("::make::");
    vMsg.setType("::typeVehicle::");
    vMsg.setEuroClass("EURO1");
    consumer.consume(vMsg);

    assertCondition(vehicleUUID);
    assertCondition(vehicleUUID, "TELEPASS_EUROPEO");
    assertCondition(vehicleUUID, "TELEPASS_ITALIANO");
    assertCondition(vehicleUUID, "TELEPASS_EUROPEO", "PEDAGGI_ITALIA");
    assertCondition(vehicleUUID, "TELEPASS_ITALIANO", "PEDAGGI_ITALIA");
  }

  private void assertCondition(String vehicle, String deviceType, String serviceType) {
    Optional<RuleOutcome> ruleOutcomeOpt = repository.find(new RuleEngineServiceTypeId(serviceType), new RuleEngineDeviceTypeId(deviceType),
                                                           new RuleEngineVehicleId(vehicle));
    assertThat(ruleOutcomeOpt.isPresent()).isTrue();
    RuleOutcome ruleOutcome = ruleOutcomeOpt.get();
    if (ruleOutcome.getOutcome() == true) {
      assertThat(true).isTrue();
    } else {
      assertThat(ruleOutcome.getFailure()
                            .isPresent()).isTrue();
    }
  }

  private void assertCondition(String vehicle, String deviceType) {
    Optional<RuleOutcome> ruleOutcomeOpt = repository.find(new RuleEngineDeviceTypeId(deviceType), new RuleEngineVehicleId(vehicle));
    assertThat(ruleOutcomeOpt.isPresent()).isTrue();
    RuleOutcome ruleOutcome = ruleOutcomeOpt.get();
    if (ruleOutcome.getOutcome() == true) {
      assertThat(true).isTrue();
    } else {
      assertThat(ruleOutcome.getFailure()
                            .isPresent()).isTrue();
    }
  }

  private void assertCondition(String vehicle) {
    Optional<RuleOutcome> ruleOutcomeOpt = repository.find(new RuleEngineVehicleId(vehicle));
    assertThat(ruleOutcomeOpt.isPresent()).isTrue();
  }

}
