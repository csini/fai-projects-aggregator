package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssociazioneDVResourceIntExtTest {
  
  private Logger log = LoggerFactory.getLogger(getClass());

  private final static String NOME_PRODUTTORE = "TELEPASS";
  private final static String CODICE_AZIENDA = "0046348";

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private AssociazioneDVResourceExt associazioneDVResource;

  @Autowired
  private ContrattoRepositoryExt contrattoRepo;

  private MockMvc   mockMvc;
  private MvcResult mvcResult;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepository;

  @Autowired
  private AssociazioneDVRepository associazioneDVRepository;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  private Contratto contratto;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    this.mockMvc = MockMvcBuilders.standaloneSetup(associazioneDVResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();
  }

  @Test
  public void getDispositivByVehicle_failed() throws Exception {
    mockMvc.perform(get(AssociazioneDVResourceExt.API_DISPOSITIVI_BY_VEHICLE).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void getDispositivByVehicle_success() throws Exception {
    List<Contratto> contracts = contrattoRepo.findByNomeProduttoreAndCodiceAzienda(NOME_PRODUTTORE, CODICE_AZIENDA);
    Optional<Contratto> contrattoOpt = Optional.ofNullable((contracts != null && !contracts.isEmpty())?contracts.get(0):null);
    contratto = contrattoOpt.get();
    TipoDispositivo tipoDispositivo1 = tipoDispositivoRepository.findOne(1L);
    TipoDispositivo tipoDispositivo2 = tipoDispositivoRepository.findOne(2L);

    Dispositivo dispositivo1 = new Dispositivo();
    dispositivo1.setContratto(contratto);
    dispositivo1.setSeriale(UUID.randomUUID()
                            .toString());
    dispositivo1.setStato(StatoDispositivo.INIZIALE);
    dispositivo1.setTipoDispositivo(tipoDispositivo1);
    dispositivo1 = dispositivoRepository.save(dispositivo1);

    Dispositivo dispositivo2 = new Dispositivo();
    dispositivo2.setContratto(contratto);
    dispositivo2.setSeriale(UUID.randomUUID()
                            .toString());
    dispositivo2.setStato(StatoDispositivo.SOSPESO);
    dispositivo2.setTipoDispositivo(tipoDispositivo2);
    dispositivo2 = dispositivoRepository.save(dispositivo2);

    long size_before = associazioneDVRepository.count();

    String uuid = UUID.randomUUID()
        .toString();

    AssociazioneDV associazioneDV1 = new AssociazioneDV();
    associazioneDV1.setUuidVeicolo(uuid);
    associazioneDV1.setDispositivo(dispositivo1);
    associazioneDV1 = associazioneDVRepository.save(associazioneDV1);

    AssociazioneDV associazioneDV2 = new AssociazioneDV();
    associazioneDV2.setUuidVeicolo(uuid);
    associazioneDV2.setDispositivo(dispositivo2);
    associazioneDV2 = associazioneDVRepository.save(associazioneDV2);
    
    dispositivo1.getAssociazioneDispositivoVeicolos().stream().map(p -> {
      log.debug("AssDV: {}", p);
      return p;
      }).collect(toSet());
    dispositivo2.getAssociazioneDispositivoVeicolos().stream().map(p -> {
      log.debug("AssDV: {}", p);
      return p;
      }).collect(toSet());
    
    List<AssociazioneDV> findByUuidVeicolo = associazioneDVRepository.findByUuidVeicolo(uuid);

    long size_after = associazioneDVRepository.count();

    assertThat(size_before + 2).isEqualTo(size_after);
    String apiUrl = AssociazioneDVResourceExt.API_DISPOSITIVI_BY_VEHICLE + "/"
        + associazioneDV1.getUuidVeicolo() + "/" + CODICE_AZIENDA;
    mvcResult = mockMvc.perform(get(apiUrl).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].stato").value(hasItem(dispositivo1.getStato()
                                                         .toString())))
        .andExpect(jsonPath("$.[*].seriale").value(hasItem(dispositivo1.getSeriale())))
        .andExpect(jsonPath("$.[*].stato").value(hasItem(dispositivo2.getStato()
                                                         .toString())))
        .andExpect(jsonPath("$.[*].seriale").value(hasItem(dispositivo2.getSeriale())))
        .andReturn();

    String size = mvcResult.getResponse()
        .getHeader("X-size");
    assertThat(Integer.valueOf(size)).isEqualTo(2);

    associazioneDVRepository.delete(associazioneDV1);
    dispositivoRepository.delete(dispositivo1);
    associazioneDVRepository.delete(associazioneDV2);
    dispositivoRepository.delete(dispositivo2);

    size_after = associazioneDVRepository.count();
    assertThat(size_before).isEqualTo(size_after);
  }
  
  @Test
  @Transactional
  public void testPostLoad() {
    AssociazioneDV associazioneDV = associazioneDVRepository.findAll().get(0);
    String oldUuidVeicolo = associazioneDV.getUuidVeicolo();
    String newUuidVeicolo = "TEST" + System.currentTimeMillis();

    log.info("Loaded....");

    assertThat(associazioneDV.getOldUuidVeicolo()).isNotBlank();

    associazioneDV.setUuidVeicolo(newUuidVeicolo);
    AssociazioneDV returnedAssociazioneDv = associazioneDVRepository.save(associazioneDV);
    associazioneDVRepository.flush();

    assertThat(associazioneDV.getUuidVeicolo()).isNotEqualTo(oldUuidVeicolo);
    assertThat(associazioneDV.getOldUuidVeicolo()).isNotEqualTo(oldUuidVeicolo);
    assertThat(associazioneDV.getUuidVeicolo()).isEqualTo(newUuidVeicolo);

    assertThat(returnedAssociazioneDv.getUuidVeicolo()).isNotEqualTo(oldUuidVeicolo);
    assertThat(returnedAssociazioneDv.getOldUuidVeicolo()).isNotEqualTo(oldUuidVeicolo);
    assertThat(returnedAssociazioneDv.getUuidVeicolo()).isEqualTo(newUuidVeicolo);

  }

  @Test
  public void getDispositivByVehicle_success_empty_response() throws Exception {
    mvcResult = mockMvc.perform(get(AssociazioneDVResourceExt.API_DISPOSITIVI_BY_VEHICLE + "/" + UUID.randomUUID()
    .toString() + "/" + UUID.randomUUID().toString()).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andReturn();

    String size = mvcResult.getResponse()
        .getHeader("X-size");
    assertThat(Integer.valueOf(size)).isEqualTo(0);
  }

  @Test
  public void getAssociazioneVeicoliCounter_test() throws Exception {
    List<String> uuids = new ArrayList<String>();
    uuids.add(RandomString());
    uuids.add(RandomString());

    mvcResult = mockMvc.perform(post(AssociazioneDVResourceExt.API_DISPOSITIVI_VEHICLES_COUNTER)
                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                .content(TestUtil.convertObjectToJsonBytes(uuids)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andReturn();

    String size = mvcResult.getResponse().getHeader("X-size");
    assertThat(Integer.valueOf(size)).isEqualTo(0);
  }

  static String RandomString() {
    return RandomString(20);
  }

  static String RandomString(int length) {
    return RandomStringUtils.random(length, true, true);
  }

  static String RandomUUID() {
    return UUID.randomUUID().toString();
  }

}
