package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ManageDevicesInStorageServiceTest {

  private static String hwDeviceType = "AS";

  @Autowired
  ManageDevicesInStorageService manageDevicesInStorageService;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepo;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepo;

  private Dispositivo dispositivo1;
  private Dispositivo dispositivo2;

  @Before
  public void setUp() {
    TipoDispositivo telepassIta = tipoDispositivoRepo.findOne(1L);

    dispositivo1 = new Dispositivo().seriale("TEST-QUERY1")
                                    .dataModificaStato(Instant.now())
                                    .stato(StatoDispositivo.IN_DEPOSITO)
                                    .modalitaSpedizione(ModalitaSpedizione.A_FAI)
                                    .tipoDispositivo(telepassIta)
                                    .tipoHardware(null);
    dispositivoRepo.save(dispositivo1);

    dispositivo2 = new Dispositivo().seriale("TEST-QUERY2")
                                    .dataModificaStato(Instant.now())
                                    .stato(StatoDispositivo.IN_DEPOSITO)
                                    .modalitaSpedizione(ModalitaSpedizione.A_FAI)
                                    .tipoDispositivo(telepassIta)
                                    .tipoHardware("AS");
    dispositivoRepo.save(dispositivo2);
  }

  @After
  public void afterTest() {
    dispositivoRepo.delete(dispositivo1);
    dispositivoRepo.delete(dispositivo2);
  }

  @Test
  @Transactional
  public void findTop1ByStatoAndTipoDispositivo_nomeAndTipoHardwareNot() {
    Optional<Dispositivo> res = manageDevicesInStorageService.findTop1ByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(
      StatoDispositivo.IN_DEPOSITO, TipoDispositivoEnum.TELEPASS_ITALIANO, hwDeviceType);

    assertThat(res.isPresent())
      .isTrue();
  }

}
