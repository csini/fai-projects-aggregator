package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmAccettazione;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmInoltroTelepassEuropeo;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeFurtoSmarrimento;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeRientroMalfunzionamento;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeVarTarga;
import it.fai.ms.efservice.service.fsm.type.telepass.sat.FsmInoltroTelepassSat;

/**
 * Test class for the TipoServizioDefaultResource REST controller.
 *
 * @see TipoServizioDefaultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmFactoryTest {

  @Autowired
  private FsmFactory fsmFactory;

  private AbstractFsmRichiesta fsm;

  @Before
  public void setup() {

  }

  @Before
  public void initTest() {

  }

  @Test
  public void resolveFsm() throws Exception {

    fsm = fsmFactory.getFsm(FsmCommand.CMD_IN_ACCETTAZIONE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmAccettazione.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmInoltroTelepassEuropeo.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    
    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TSAT);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmInoltroTelepassSat.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_FURTO_NO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeFurtoSmarrimento.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_SMARRIMENTO_NO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeFurtoSmarrimento.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_FURTO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeFurtoSmarrimento.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_SMARRIMENTO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeFurtoSmarrimento.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeVarTarga.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_MEZZO_RITARGATO_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeVarTarga.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeVarTarga.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

    fsm = fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE);
    assertThat(fsm.getClass()
                  .getName()).isEqualTo(FsmModificaTeRientroMalfunzionamento.class.getName());
    assertInitialStateofFsm(fsm, StatoRichiesta.ATTIVO_PER_MODIFICA_TE);

  }

  private void assertInitialStateofFsm(AbstractFsmRichiesta abstractFsm, StatoRichiesta stato) {
    StateMachine<StatoRichiesta, RichiestaEvent> stateMachine = abstractFsm.getStateMachine();
    State<StatoRichiesta, RichiestaEvent> initialState = stateMachine.getInitialState();
    StatoRichiesta statoIniziale = initialState.getId();
    assertThat(statoIniziale).isEqualTo(stato);
  }

}
