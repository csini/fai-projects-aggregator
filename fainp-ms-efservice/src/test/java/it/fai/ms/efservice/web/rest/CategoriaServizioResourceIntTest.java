package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.CategoriaServizio;
import it.fai.ms.efservice.repository.CategoriaServizioRepository;
import it.fai.ms.efservice.service.CategoriaServizioService;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;
import it.fai.ms.efservice.service.mapper.CategoriaServizioMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CategoriaServizioResource REST controller.
 *
 * @see CategoriaServizioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class CategoriaServizioResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_BUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_NOME_BUSINESS = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_TESTO_PROMO = "AAAAAAAAAA";
    private static final String UPDATED_TESTO_PROMO = "BBBBBBBBBB";

    private static final Long DEFAULT_ORDINAMENTO = 1L;
    private static final Long UPDATED_ORDINAMENTO = 2L;

    private static final Boolean DEFAULT_DISABILITATO = false;
    private static final Boolean UPDATED_DISABILITATO = true;

    @Autowired
    private CategoriaServizioRepository categoriaServizioRepository;

    @Autowired
    private CategoriaServizioMapper categoriaServizioMapper;

    @Autowired
    private CategoriaServizioService categoriaServizioService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCategoriaServizioMockMvc;

    private CategoriaServizio categoriaServizio;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CategoriaServizioResource categoriaServizioResource = new CategoriaServizioResource(categoriaServizioService);
        this.restCategoriaServizioMockMvc = MockMvcBuilders.standaloneSetup(categoriaServizioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoriaServizio createEntity(EntityManager em) {
        CategoriaServizio categoriaServizio = new CategoriaServizio()
            .nome(DEFAULT_NOME)
            .nomeBusiness(DEFAULT_NOME_BUSINESS)
            .descrizione(DEFAULT_DESCRIZIONE)
            .testoPromo(DEFAULT_TESTO_PROMO)
            .ordinamento(DEFAULT_ORDINAMENTO)
            .disabilitato(DEFAULT_DISABILITATO);
        return categoriaServizio;
    }

    @Before
    public void initTest() {
        categoriaServizio = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategoriaServizio() throws Exception {
        int databaseSizeBeforeCreate = categoriaServizioRepository.findAll().size();

        // Create the CategoriaServizio
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioMapper.toDto(categoriaServizio);
        restCategoriaServizioMockMvc.perform(post("/api/categoria-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoriaServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the CategoriaServizio in the database
        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeCreate + 1);
        CategoriaServizio testCategoriaServizio = categoriaServizioList.get(categoriaServizioList.size() - 1);
        assertThat(testCategoriaServizio.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testCategoriaServizio.getNomeBusiness()).isEqualTo(DEFAULT_NOME_BUSINESS);
        assertThat(testCategoriaServizio.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testCategoriaServizio.getTestoPromo()).isEqualTo(DEFAULT_TESTO_PROMO);
        assertThat(testCategoriaServizio.getOrdinamento()).isEqualTo(DEFAULT_ORDINAMENTO);
        assertThat(testCategoriaServizio.isDisabilitato()).isEqualTo(DEFAULT_DISABILITATO);
    }

    @Test
    @Transactional
    public void createCategoriaServizioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoriaServizioRepository.findAll().size();

        // Create the CategoriaServizio with an existing ID
        categoriaServizio.setId(1L);
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioMapper.toDto(categoriaServizio);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoriaServizioMockMvc.perform(post("/api/categoria-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoriaServizioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CategoriaServizio in the database
        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoriaServizioRepository.findAll().size();
        // set the field null
        categoriaServizio.setNome(null);

        // Create the CategoriaServizio, which fails.
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioMapper.toDto(categoriaServizio);

        restCategoriaServizioMockMvc.perform(post("/api/categoria-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoriaServizioDTO)))
            .andExpect(status().isBadRequest());

        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCategoriaServizios() throws Exception {
        // Initialize the database
        categoriaServizioRepository.saveAndFlush(categoriaServizio);

        // Get all the categoriaServizioList
        restCategoriaServizioMockMvc.perform(get("/api/categoria-servizios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoriaServizio.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].nomeBusiness").value(hasItem(DEFAULT_NOME_BUSINESS.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].testoPromo").value(hasItem(DEFAULT_TESTO_PROMO.toString())))
            .andExpect(jsonPath("$.[*].ordinamento").value(hasItem(DEFAULT_ORDINAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].disabilitato").value(hasItem(DEFAULT_DISABILITATO.booleanValue())));
    }

    @Test
    @Transactional
    public void getCategoriaServizio() throws Exception {
        // Initialize the database
        categoriaServizioRepository.saveAndFlush(categoriaServizio);

        // Get the categoriaServizio
        restCategoriaServizioMockMvc.perform(get("/api/categoria-servizios/{id}", categoriaServizio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(categoriaServizio.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.nomeBusiness").value(DEFAULT_NOME_BUSINESS.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.testoPromo").value(DEFAULT_TESTO_PROMO.toString()))
            .andExpect(jsonPath("$.ordinamento").value(DEFAULT_ORDINAMENTO.intValue()))
            .andExpect(jsonPath("$.disabilitato").value(DEFAULT_DISABILITATO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCategoriaServizio() throws Exception {
        // Get the categoriaServizio
        restCategoriaServizioMockMvc.perform(get("/api/categoria-servizios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategoriaServizio() throws Exception {
        // Initialize the database
        categoriaServizioRepository.saveAndFlush(categoriaServizio);
        int databaseSizeBeforeUpdate = categoriaServizioRepository.findAll().size();

        // Update the categoriaServizio
        CategoriaServizio updatedCategoriaServizio = categoriaServizioRepository.findOne(categoriaServizio.getId());
        updatedCategoriaServizio
            .nome(UPDATED_NOME)
            .nomeBusiness(UPDATED_NOME_BUSINESS)
            .descrizione(UPDATED_DESCRIZIONE)
            .testoPromo(UPDATED_TESTO_PROMO)
            .ordinamento(UPDATED_ORDINAMENTO)
            .disabilitato(UPDATED_DISABILITATO);
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioMapper.toDto(updatedCategoriaServizio);

        restCategoriaServizioMockMvc.perform(put("/api/categoria-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoriaServizioDTO)))
            .andExpect(status().isOk());

        // Validate the CategoriaServizio in the database
        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeUpdate);
        CategoriaServizio testCategoriaServizio = categoriaServizioList.get(categoriaServizioList.size() - 1);
        assertThat(testCategoriaServizio.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testCategoriaServizio.getNomeBusiness()).isEqualTo(UPDATED_NOME_BUSINESS);
        assertThat(testCategoriaServizio.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testCategoriaServizio.getTestoPromo()).isEqualTo(UPDATED_TESTO_PROMO);
        assertThat(testCategoriaServizio.getOrdinamento()).isEqualTo(UPDATED_ORDINAMENTO);
        assertThat(testCategoriaServizio.isDisabilitato()).isEqualTo(UPDATED_DISABILITATO);
    }

    @Test
    @Transactional
    public void updateNonExistingCategoriaServizio() throws Exception {
        int databaseSizeBeforeUpdate = categoriaServizioRepository.findAll().size();

        // Create the CategoriaServizio
        CategoriaServizioDTO categoriaServizioDTO = categoriaServizioMapper.toDto(categoriaServizio);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCategoriaServizioMockMvc.perform(put("/api/categoria-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoriaServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the CategoriaServizio in the database
        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCategoriaServizio() throws Exception {
        // Initialize the database
        categoriaServizioRepository.saveAndFlush(categoriaServizio);
        int databaseSizeBeforeDelete = categoriaServizioRepository.findAll().size();

        // Get the categoriaServizio
        restCategoriaServizioMockMvc.perform(delete("/api/categoria-servizios/{id}", categoriaServizio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CategoriaServizio> categoriaServizioList = categoriaServizioRepository.findAll();
        assertThat(categoriaServizioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoriaServizio.class);
        CategoriaServizio categoriaServizio1 = new CategoriaServizio();
        categoriaServizio1.setId(1L);
        CategoriaServizio categoriaServizio2 = new CategoriaServizio();
        categoriaServizio2.setId(categoriaServizio1.getId());
        assertThat(categoriaServizio1).isEqualTo(categoriaServizio2);
        categoriaServizio2.setId(2L);
        assertThat(categoriaServizio1).isNotEqualTo(categoriaServizio2);
        categoriaServizio1.setId(null);
        assertThat(categoriaServizio1).isNotEqualTo(categoriaServizio2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoriaServizioDTO.class);
        CategoriaServizioDTO categoriaServizioDTO1 = new CategoriaServizioDTO();
        categoriaServizioDTO1.setId(1L);
        CategoriaServizioDTO categoriaServizioDTO2 = new CategoriaServizioDTO();
        assertThat(categoriaServizioDTO1).isNotEqualTo(categoriaServizioDTO2);
        categoriaServizioDTO2.setId(categoriaServizioDTO1.getId());
        assertThat(categoriaServizioDTO1).isEqualTo(categoriaServizioDTO2);
        categoriaServizioDTO2.setId(2L);
        assertThat(categoriaServizioDTO1).isNotEqualTo(categoriaServizioDTO2);
        categoriaServizioDTO1.setId(null);
        assertThat(categoriaServizioDTO1).isNotEqualTo(categoriaServizioDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(categoriaServizioMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(categoriaServizioMapper.fromId(null)).isNull();
    }
}
