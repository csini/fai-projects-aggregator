package it.fai.ms.efservice.service.fsm.type.localizzatoresat;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.localizzatoresat.FsmModificaLocalizzatoreSatMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceReturnMessageProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaLocalizzatoreSatMalfunzionamentoTest {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Configuration
  public static class LocalizzatoreSatMalfunzionamentoConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    private static FsmSenderToQueue                  senderFsmService           = mock(FsmSenderToQueue.class);
    private final DeviceReturnMessageProducerService deviceReturnMessageService = mock(DeviceReturnMessageProducerService.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmModificaLocalizzatoreSatMalfunzionamentoConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmModificaLocalizzatoreSatMalfunzionamentoConfig newLocalizzatoreSatMalfunzionamentoConfiguration() {
      FsmModificaLocalizzatoreSatMalfunzionamentoConfig fsmModificaLocalizzatoreSatMalfunzionamento = new FsmModificaLocalizzatoreSatMalfunzionamentoConfig(senderFsmService,
                                                                                                                                                            deviceReturnMessageService);
      _log.info("Created FsmModificaLocalizzatoreSatMalfunzionamentoConfig for test {}", fsmModificaLocalizzatoreSatMalfunzionamento);
      return fsmModificaLocalizzatoreSatMalfunzionamento;
    }
  }

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;
  private Richiesta                richiestaMalfunzionamento = null;
  private Dispositivo              dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    richiestaMalfunzionamento = newMockRichiesta(TipoRichiesta.MALFUNZIONAMENTO);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private Dispositivo newDispositivo() {
    Dispositivo d = new Dispositivo();

    d.setStato(StatoDispositivo.ATTIVO_IN_ATTESA_RIENTRO);
    d.setTipoDispositivo(newTipoDispositivo());
    d.setId(1L);

    return d;
  }

  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo d = new TipoDispositivo();

    d.setId(10000001L);
    d.setModalitaSpedizione(ModalitaSpedizione.A_FAI);
    d.setNome(TipoDispositivoEnum.LOCALIZZAZIONE_SATELITTARE);

    return d;
  }

  private Richiesta newMockRichiesta(TipoRichiesta tipoRichiesta) {
    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = newDispositivo();

    Richiesta richiesta = new Richiesta().tipo(tipoRichiesta)
                                         .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)
                                         .tipoDispositivo(newTipoDispositivo())
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmModificaViaTollMalfunzionamento_verifica_rientro_dispositivo_annullamento_operatore() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    }
    FsmModificaLocalizzatoreSatMalfunzionamento fsmModificaLocalizzatoreSatMalfunzionamento = getFsm(richiestaMalfunzionamento);
    richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE,
                                                                                                        richiestaMalfunzionamento);
    assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
  }

  @Test
  public void FsmModificaViaTollMalfunzionamento_verifica_rientro_dispositivo_dispositivo_rientrato() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    }
    FsmModificaLocalizzatoreSatMalfunzionamento fsmModificaLocalizzatoreSatMalfunzionamento = getFsm(richiestaMalfunzionamento);
    richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                        richiestaMalfunzionamento);
    assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
  }

  @Test
  public void FsmModificaViaTollMalfunzionamento_dispositivo_disattivato() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    }
    FsmModificaLocalizzatoreSatMalfunzionamento fsmModificaLocalizzatoreSatMalfunzionamento = getFsm(richiestaMalfunzionamento);
    richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                        richiestaMalfunzionamento);

    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)) {

      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);
    }
    richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.RESPONSE_OK,
                                                                                                        richiestaMalfunzionamento);
    assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
  }

  @Test
  public void FsmModificaViaTollMalfunzionamento_evaso_con_riconsegna() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    }
    FsmModificaLocalizzatoreSatMalfunzionamento fsmModificaLocalizzatoreSatMalfunzionamento = getFsm(richiestaMalfunzionamento);

    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LOCALIZZATORE_SAT)) {

      richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                          richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_RIENTRATO);
    }

    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO)) {

      richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.RESPONSE_OK,
                                                                                                          richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
    }

    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.DISPOSITIVO_DISATTIVATO)) {

      richiestaMalfunzionamento = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_LETTERA_VETTURA,
                                                                                                          richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
    }

  }

  private void sendInitialCommandToFSM(Richiesta richiesta) throws Exception {
    Mockito.doNothing()
           .when(LocalizzatoreSatMalfunzionamentoConfiguration.senderFsmService)
           .sendMessageForChangeStatoDispositivo(any(Dispositivo.class), any(DispositivoEvent.class));

    FsmModificaLocalizzatoreSatMalfunzionamento fsmModificaLocalizzatoreSatMalfunzionamento = getFsm(richiesta);

    richiesta = fsmModificaLocalizzatoreSatMalfunzionamento.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  }

  private FsmModificaLocalizzatoreSatMalfunzionamento getFsm(Richiesta richiesta) {
    FsmCommand fsmCommand = null;
    switch (richiesta.getTipo()) {
    case MALFUNZIONAMENTO:
      fsmCommand = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_LOCALIZZATORE_SAT;
      break;
    default:
      log.error("Tipologia Richiesta [" + richiesta.getTipo()
                                                   .name()
                + "] is not managed");
      break;
    }

    return (FsmModificaLocalizzatoreSatMalfunzionamento) fsmFactory.getFsm(fsmCommand);
  }

}
