package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.ClienteFaiService;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import it.fai.ms.efservice.service.mapper.ClienteFaiMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.StatoAzienda;
/**
 * Test class for the ClienteFaiResource REST controller.
 *
 * @see ClienteFaiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ClienteFaiResourceIntTest {

    private static final String DEFAULT_CODICE_CLIENTE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_CLIENTE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final StatoAzienda DEFAULT_STATO = StatoAzienda.ATTIVO;
    private static final StatoAzienda UPDATED_STATO = StatoAzienda.SOSPESO;

    private static final Long DEFAULT_NUMERO_ACCOUNT = 1L;
    private static final Long UPDATED_NUMERO_ACCOUNT = 2L;

    private static final String DEFAULT_RAGIONE_SOCIALE = "AAAAAAAAAA";
    private static final String UPDATED_RAGIONE_SOCIALE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_FISCALE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_FISCALE = "BBBBBBBBBB";

    private static final String DEFAULT_PARTITA_IVA = "AAAAAAAAAA";
    private static final String UPDATED_PARTITA_IVA = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_AGENTE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_AGENTE = "BBBBBBBBBB";

    private static final String DEFAULT_RAGGRUPPAMENTO_IMPRESA = "AAAAAAAAAA";
    private static final String UPDATED_RAGGRUPPAMENTO_IMPRESA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CONSORZIO_PADRE = false;
    private static final Boolean UPDATED_CONSORZIO_PADRE = true;

    private static final String DEFAULT_VIA = "AAAAAAAAAA";
    private static final String UPDATED_VIA = "BBBBBBBBBB";

    private static final String DEFAULT_CITTA = "AAAAAAAAAA";
    private static final String UPDATED_CITTA = "BBBBBBBBBB";

    private static final String DEFAULT_CAP = "AAAAAAAAAA";
    private static final String UPDATED_CAP = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
    private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

    private static final String DEFAULT_PAESE = "AA";
    private static final String UPDATED_PAESE = "BB";

    private static final String DEFAULT_EMAIL_LEGALE_RAPPRESENTANTE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_LEGALE_RAPPRESENTANTE = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_LEGALE_RAPPRESENTANTE = "AAAAAAAAAA";
    private static final String UPDATED_NOME_LEGALE_RAPPRESENTANTE = "BBBBBBBBBB";

    private static final String DEFAULT_COGNOME_LEGALE_RAPPRESENTANTE = "AAAAAAAAAA";
    private static final String UPDATED_COGNOME_LEGALE_RAPPRESENTANTE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE = "AAAAAAAAAA";
    private static final String UPDATED_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_CLIENTE_FATTURAZIONE = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_CLIENTE_FATTURAZIONE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DML_REVISION_TIMESTAMP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DML_REVISION_TIMESTAMP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ClienteFaiRepository clienteFaiRepository;

    @Autowired
    private ClienteFaiMapper clienteFaiMapper;

    @Autowired
    private ClienteFaiService clienteFaiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClienteFaiMockMvc;

    private ClienteFai clienteFai;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClienteFaiResource clienteFaiResource = new ClienteFaiResource(clienteFaiService);
        this.restClienteFaiMockMvc = MockMvcBuilders.standaloneSetup(clienteFaiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClienteFai createEntity(EntityManager em) {
        ClienteFai clienteFai = new ClienteFai()
            .codiceCliente(DEFAULT_CODICE_CLIENTE)
            .email(DEFAULT_EMAIL)
            .stato(DEFAULT_STATO)
            .numeroAccount(DEFAULT_NUMERO_ACCOUNT)
            .ragioneSociale(DEFAULT_RAGIONE_SOCIALE)
            .codiceFiscale(DEFAULT_CODICE_FISCALE)
            .partitaIva(DEFAULT_PARTITA_IVA)
            .codiceAgente(DEFAULT_CODICE_AGENTE)
            .raggruppamentoImpresa(DEFAULT_RAGGRUPPAMENTO_IMPRESA)
            .descrizioneRaggruppamentoImpresa(DEFAULT_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA)
            .consorzioPadre(DEFAULT_CONSORZIO_PADRE)
            .via(DEFAULT_VIA)
            .citta(DEFAULT_CITTA)
            .cap(DEFAULT_CAP)
            .provincia(DEFAULT_PROVINCIA)
            .paese(DEFAULT_PAESE)
            .emailLegaleRappresentante(DEFAULT_EMAIL_LEGALE_RAPPRESENTANTE)
            .nomeLegaleRappresentante(DEFAULT_NOME_LEGALE_RAPPRESENTANTE)
            .cognomeLegaleRappresentante(DEFAULT_COGNOME_LEGALE_RAPPRESENTANTE)
            .contattoTelefonicoLegaleRappresentante(DEFAULT_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE)
            .codiceClienteFatturazione(DEFAULT_CODICE_CLIENTE_FATTURAZIONE)
            .dmlRevisionTimestamp(DEFAULT_DML_REVISION_TIMESTAMP);
        return clienteFai;
    }

    @Before
    public void initTest() {
        clienteFai = createEntity(em);
    }

    @Test
    @Transactional
    public void createClienteFai() throws Exception {
        int databaseSizeBeforeCreate = clienteFaiRepository.findAll().size();

        // Create the ClienteFai
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);
        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isCreated());

        // Validate the ClienteFai in the database
        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeCreate + 1);
        ClienteFai testClienteFai = clienteFaiList.get(clienteFaiList.size() - 1);
        assertThat(testClienteFai.getCodiceCliente()).isEqualTo(DEFAULT_CODICE_CLIENTE);
        assertThat(testClienteFai.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testClienteFai.getStato()).isEqualTo(DEFAULT_STATO);
        assertThat(testClienteFai.getNumeroAccount()).isEqualTo(DEFAULT_NUMERO_ACCOUNT);
        assertThat(testClienteFai.getRagioneSociale()).isEqualTo(DEFAULT_RAGIONE_SOCIALE);
        assertThat(testClienteFai.getCodiceFiscale()).isEqualTo(DEFAULT_CODICE_FISCALE);
        assertThat(testClienteFai.getPartitaIva()).isEqualTo(DEFAULT_PARTITA_IVA);
        assertThat(testClienteFai.getCodiceAgente()).isEqualTo(DEFAULT_CODICE_AGENTE);
        assertThat(testClienteFai.getRaggruppamentoImpresa()).isEqualTo(DEFAULT_RAGGRUPPAMENTO_IMPRESA);
        assertThat(testClienteFai.getDescrizioneRaggruppamentoImpresa()).isEqualTo(DEFAULT_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA);
        assertThat(testClienteFai.isConsorzioPadre()).isEqualTo(DEFAULT_CONSORZIO_PADRE);
        assertThat(testClienteFai.getVia()).isEqualTo(DEFAULT_VIA);
        assertThat(testClienteFai.getCitta()).isEqualTo(DEFAULT_CITTA);
        assertThat(testClienteFai.getCap()).isEqualTo(DEFAULT_CAP);
        assertThat(testClienteFai.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
        assertThat(testClienteFai.getPaese()).isEqualTo(DEFAULT_PAESE);
        assertThat(testClienteFai.getEmailLegaleRappresentante()).isEqualTo(DEFAULT_EMAIL_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getNomeLegaleRappresentante()).isEqualTo(DEFAULT_NOME_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getCognomeLegaleRappresentante()).isEqualTo(DEFAULT_COGNOME_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getContattoTelefonicoLegaleRappresentante()).isEqualTo(DEFAULT_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getCodiceClienteFatturazione()).isEqualTo(DEFAULT_CODICE_CLIENTE_FATTURAZIONE);
        assertThat(testClienteFai.getDmlRevisionTimestamp()).isEqualTo(DEFAULT_DML_REVISION_TIMESTAMP);
    }

    @Test
    @Transactional
    public void createClienteFaiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clienteFaiRepository.findAll().size();

        // Create the ClienteFai with an existing ID
        clienteFai.setId(1L);
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClienteFai in the database
        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodiceClienteIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteFaiRepository.findAll().size();
        // set the field null
        clienteFai.setCodiceCliente(null);

        // Create the ClienteFai, which fails.
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isBadRequest());

        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkViaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteFaiRepository.findAll().size();
        // set the field null
        clienteFai.setVia(null);

        // Create the ClienteFai, which fails.
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isBadRequest());

        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCittaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteFaiRepository.findAll().size();
        // set the field null
        clienteFai.setCitta(null);

        // Create the ClienteFai, which fails.
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isBadRequest());

        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDmlRevisionTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteFaiRepository.findAll().size();
        // set the field null
        clienteFai.setDmlRevisionTimestamp(null);

        // Create the ClienteFai, which fails.
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        restClienteFaiMockMvc.perform(post("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isBadRequest());

        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClienteFais() throws Exception {
        // Initialize the database
        clienteFaiRepository.saveAndFlush(clienteFai);

        // Get all the clienteFaiList
        restClienteFaiMockMvc.perform(get("/api/cliente-fais?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clienteFai.getId().intValue())))
            .andExpect(jsonPath("$.[*].codiceCliente").value(hasItem(DEFAULT_CODICE_CLIENTE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
            .andExpect(jsonPath("$.[*].numeroAccount").value(hasItem(DEFAULT_NUMERO_ACCOUNT.intValue())))
            .andExpect(jsonPath("$.[*].ragioneSociale").value(hasItem(DEFAULT_RAGIONE_SOCIALE.toString())))
            .andExpect(jsonPath("$.[*].codiceFiscale").value(hasItem(DEFAULT_CODICE_FISCALE.toString())))
            .andExpect(jsonPath("$.[*].partitaIva").value(hasItem(DEFAULT_PARTITA_IVA.toString())))
            .andExpect(jsonPath("$.[*].codiceAgente").value(hasItem(DEFAULT_CODICE_AGENTE.toString())))
            .andExpect(jsonPath("$.[*].raggruppamentoImpresa").value(hasItem(DEFAULT_RAGGRUPPAMENTO_IMPRESA.toString())))
            .andExpect(jsonPath("$.[*].descrizioneRaggruppamentoImpresa").value(hasItem(DEFAULT_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA.toString())))
            .andExpect(jsonPath("$.[*].consorzioPadre").value(hasItem(DEFAULT_CONSORZIO_PADRE.booleanValue())))
            .andExpect(jsonPath("$.[*].via").value(hasItem(DEFAULT_VIA.toString())))
            .andExpect(jsonPath("$.[*].citta").value(hasItem(DEFAULT_CITTA.toString())))
            .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
            .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
            .andExpect(jsonPath("$.[*].paese").value(hasItem(DEFAULT_PAESE.toString())))
            .andExpect(jsonPath("$.[*].emailLegaleRappresentante").value(hasItem(DEFAULT_EMAIL_LEGALE_RAPPRESENTANTE.toString())))
            .andExpect(jsonPath("$.[*].nomeLegaleRappresentante").value(hasItem(DEFAULT_NOME_LEGALE_RAPPRESENTANTE.toString())))
            .andExpect(jsonPath("$.[*].cognomeLegaleRappresentante").value(hasItem(DEFAULT_COGNOME_LEGALE_RAPPRESENTANTE.toString())))
            .andExpect(jsonPath("$.[*].contattoTelefonicoLegaleRappresentante").value(hasItem(DEFAULT_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE.toString())))
            .andExpect(jsonPath("$.[*].codiceClienteFatturazione").value(hasItem(DEFAULT_CODICE_CLIENTE_FATTURAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dmlRevisionTimestamp").value(hasItem(DEFAULT_DML_REVISION_TIMESTAMP.toString())));
    }

    @Test
    @Transactional
    public void getClienteFai() throws Exception {
        // Initialize the database
        clienteFaiRepository.saveAndFlush(clienteFai);

        // Get the clienteFai
        restClienteFaiMockMvc.perform(get("/api/cliente-fais/{id}", clienteFai.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clienteFai.getId().intValue()))
            .andExpect(jsonPath("$.codiceCliente").value(DEFAULT_CODICE_CLIENTE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
            .andExpect(jsonPath("$.numeroAccount").value(DEFAULT_NUMERO_ACCOUNT.intValue()))
            .andExpect(jsonPath("$.ragioneSociale").value(DEFAULT_RAGIONE_SOCIALE.toString()))
            .andExpect(jsonPath("$.codiceFiscale").value(DEFAULT_CODICE_FISCALE.toString()))
            .andExpect(jsonPath("$.partitaIva").value(DEFAULT_PARTITA_IVA.toString()))
            .andExpect(jsonPath("$.codiceAgente").value(DEFAULT_CODICE_AGENTE.toString()))
            .andExpect(jsonPath("$.raggruppamentoImpresa").value(DEFAULT_RAGGRUPPAMENTO_IMPRESA.toString()))
            .andExpect(jsonPath("$.descrizioneRaggruppamentoImpresa").value(DEFAULT_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA.toString()))
            .andExpect(jsonPath("$.consorzioPadre").value(DEFAULT_CONSORZIO_PADRE.booleanValue()))
            .andExpect(jsonPath("$.via").value(DEFAULT_VIA.toString()))
            .andExpect(jsonPath("$.citta").value(DEFAULT_CITTA.toString()))
            .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
            .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
            .andExpect(jsonPath("$.paese").value(DEFAULT_PAESE.toString()))
            .andExpect(jsonPath("$.emailLegaleRappresentante").value(DEFAULT_EMAIL_LEGALE_RAPPRESENTANTE.toString()))
            .andExpect(jsonPath("$.nomeLegaleRappresentante").value(DEFAULT_NOME_LEGALE_RAPPRESENTANTE.toString()))
            .andExpect(jsonPath("$.cognomeLegaleRappresentante").value(DEFAULT_COGNOME_LEGALE_RAPPRESENTANTE.toString()))
            .andExpect(jsonPath("$.contattoTelefonicoLegaleRappresentante").value(DEFAULT_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE.toString()))
            .andExpect(jsonPath("$.codiceClienteFatturazione").value(DEFAULT_CODICE_CLIENTE_FATTURAZIONE.toString()))
            .andExpect(jsonPath("$.dmlRevisionTimestamp").value(DEFAULT_DML_REVISION_TIMESTAMP.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClienteFai() throws Exception {
        // Get the clienteFai
        restClienteFaiMockMvc.perform(get("/api/cliente-fais/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClienteFai() throws Exception {
        // Initialize the database
        clienteFaiRepository.saveAndFlush(clienteFai);
        int databaseSizeBeforeUpdate = clienteFaiRepository.findAll().size();

        // Update the clienteFai
        ClienteFai updatedClienteFai = clienteFaiRepository.findOne(clienteFai.getId());
        // Disconnect from session so that the updates on updatedClienteFai are not directly saved in db
        em.detach(updatedClienteFai);
        updatedClienteFai
            .codiceCliente(UPDATED_CODICE_CLIENTE)
            .email(UPDATED_EMAIL)
            .stato(UPDATED_STATO)
            .numeroAccount(UPDATED_NUMERO_ACCOUNT)
            .ragioneSociale(UPDATED_RAGIONE_SOCIALE)
            .codiceFiscale(UPDATED_CODICE_FISCALE)
            .partitaIva(UPDATED_PARTITA_IVA)
            .codiceAgente(UPDATED_CODICE_AGENTE)
            .raggruppamentoImpresa(UPDATED_RAGGRUPPAMENTO_IMPRESA)
            .descrizioneRaggruppamentoImpresa(UPDATED_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA)
            .consorzioPadre(UPDATED_CONSORZIO_PADRE)
            .via(UPDATED_VIA)
            .citta(UPDATED_CITTA)
            .cap(UPDATED_CAP)
            .provincia(UPDATED_PROVINCIA)
            .paese(UPDATED_PAESE)
            .emailLegaleRappresentante(UPDATED_EMAIL_LEGALE_RAPPRESENTANTE)
            .nomeLegaleRappresentante(UPDATED_NOME_LEGALE_RAPPRESENTANTE)
            .cognomeLegaleRappresentante(UPDATED_COGNOME_LEGALE_RAPPRESENTANTE)
            .contattoTelefonicoLegaleRappresentante(UPDATED_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE)
            .codiceClienteFatturazione(UPDATED_CODICE_CLIENTE_FATTURAZIONE)
            .dmlRevisionTimestamp(UPDATED_DML_REVISION_TIMESTAMP);
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(updatedClienteFai);

        restClienteFaiMockMvc.perform(put("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isOk());

        // Validate the ClienteFai in the database
        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeUpdate);
        ClienteFai testClienteFai = clienteFaiList.get(clienteFaiList.size() - 1);
        assertThat(testClienteFai.getCodiceCliente()).isEqualTo(UPDATED_CODICE_CLIENTE);
        assertThat(testClienteFai.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testClienteFai.getStato()).isEqualTo(UPDATED_STATO);
        assertThat(testClienteFai.getNumeroAccount()).isEqualTo(UPDATED_NUMERO_ACCOUNT);
        assertThat(testClienteFai.getRagioneSociale()).isEqualTo(UPDATED_RAGIONE_SOCIALE);
        assertThat(testClienteFai.getCodiceFiscale()).isEqualTo(UPDATED_CODICE_FISCALE);
        assertThat(testClienteFai.getPartitaIva()).isEqualTo(UPDATED_PARTITA_IVA);
        assertThat(testClienteFai.getCodiceAgente()).isEqualTo(UPDATED_CODICE_AGENTE);
        assertThat(testClienteFai.getRaggruppamentoImpresa()).isEqualTo(UPDATED_RAGGRUPPAMENTO_IMPRESA);
        assertThat(testClienteFai.getDescrizioneRaggruppamentoImpresa()).isEqualTo(UPDATED_DESCRIZIONE_RAGGRUPPAMENTO_IMPRESA);
        assertThat(testClienteFai.isConsorzioPadre()).isEqualTo(UPDATED_CONSORZIO_PADRE);
        assertThat(testClienteFai.getVia()).isEqualTo(UPDATED_VIA);
        assertThat(testClienteFai.getCitta()).isEqualTo(UPDATED_CITTA);
        assertThat(testClienteFai.getCap()).isEqualTo(UPDATED_CAP);
        assertThat(testClienteFai.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
        assertThat(testClienteFai.getPaese()).isEqualTo(UPDATED_PAESE);
        assertThat(testClienteFai.getEmailLegaleRappresentante()).isEqualTo(UPDATED_EMAIL_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getNomeLegaleRappresentante()).isEqualTo(UPDATED_NOME_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getCognomeLegaleRappresentante()).isEqualTo(UPDATED_COGNOME_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getContattoTelefonicoLegaleRappresentante()).isEqualTo(UPDATED_CONTATTO_TELEFONICO_LEGALE_RAPPRESENTANTE);
        assertThat(testClienteFai.getCodiceClienteFatturazione()).isEqualTo(UPDATED_CODICE_CLIENTE_FATTURAZIONE);
        assertThat(testClienteFai.getDmlRevisionTimestamp()).isEqualTo(UPDATED_DML_REVISION_TIMESTAMP);
    }

    @Test
    @Transactional
    public void updateNonExistingClienteFai() throws Exception {
        int databaseSizeBeforeUpdate = clienteFaiRepository.findAll().size();

        // Create the ClienteFai
        ClienteFaiDTO clienteFaiDTO = clienteFaiMapper.toDto(clienteFai);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClienteFaiMockMvc.perform(put("/api/cliente-fais")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clienteFaiDTO)))
            .andExpect(status().isCreated());

        // Validate the ClienteFai in the database
        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClienteFai() throws Exception {
        // Initialize the database
        clienteFaiRepository.saveAndFlush(clienteFai);
        int databaseSizeBeforeDelete = clienteFaiRepository.findAll().size();

        // Get the clienteFai
        restClienteFaiMockMvc.perform(delete("/api/cliente-fais/{id}", clienteFai.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClienteFai> clienteFaiList = clienteFaiRepository.findAll();
        assertThat(clienteFaiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClienteFai.class);
        ClienteFai clienteFai1 = new ClienteFai();
        clienteFai1.setId(1L);
        ClienteFai clienteFai2 = new ClienteFai();
        clienteFai2.setId(clienteFai1.getId());
        assertThat(clienteFai1).isEqualTo(clienteFai2);
        clienteFai2.setId(2L);
        assertThat(clienteFai1).isNotEqualTo(clienteFai2);
        clienteFai1.setId(null);
        assertThat(clienteFai1).isNotEqualTo(clienteFai2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClienteFaiDTO.class);
        ClienteFaiDTO clienteFaiDTO1 = new ClienteFaiDTO();
        clienteFaiDTO1.setId(1L);
        ClienteFaiDTO clienteFaiDTO2 = new ClienteFaiDTO();
        assertThat(clienteFaiDTO1).isNotEqualTo(clienteFaiDTO2);
        clienteFaiDTO2.setId(clienteFaiDTO1.getId());
        assertThat(clienteFaiDTO1).isEqualTo(clienteFaiDTO2);
        clienteFaiDTO2.setId(2L);
        assertThat(clienteFaiDTO1).isNotEqualTo(clienteFaiDTO2);
        clienteFaiDTO1.setId(null);
        assertThat(clienteFaiDTO1).isNotEqualTo(clienteFaiDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clienteFaiMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clienteFaiMapper.fromId(null)).isNull();
    }
}
