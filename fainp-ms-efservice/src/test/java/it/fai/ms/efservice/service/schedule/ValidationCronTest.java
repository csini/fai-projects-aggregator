package it.fai.ms.efservice.service.schedule;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.ApplicationProperties;
import it.fai.ms.efservice.config.ApplicationProperties.Scheduler;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class ValidationCronTest {

  @Autowired
  private ApplicationProperties _applicationProperties;

  @Test
  public void testCronValid() {
    Scheduler cronScheduler = _applicationProperties.getScheduler();

    assertThat(cronScheduler).isNotNull();
    String cacheordini = cronScheduler.getCacheordini();
    CronSequenceGenerator.isValidExpression(cacheordini);

    String ordiniCron = cronScheduler.getOrdinicron();
    CronSequenceGenerator.isValidExpression(ordiniCron);

    String forcedevicetosend = cronScheduler.getForcedevicetosend();
    CronSequenceGenerator.isValidExpression(forcedevicetosend);
  }

}
