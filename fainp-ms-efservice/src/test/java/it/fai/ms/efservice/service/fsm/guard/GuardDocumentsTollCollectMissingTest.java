package it.fai.ms.efservice.service.fsm.guard;

import feign.FeignException;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.ContrattoRichiestaException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.statemachine.StateContext;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GuardDocumentsTollCollectMissingTest {

  private GuardDocumentsTollCollectMissing guard;

  @Mock
  private DocumentService documentService;

  @Before
  public void init(){
    guard = new GuardDocumentsTollCollectMissing(documentService);
  }

  //1
  @Test
  public void evaluateTrueWhenDocDispositivoMissingContrattoMissingDocContrattoMissing(){
    /*

      Non c'è il documento del dispositivo.
      Non c'è il contratto e non c'è nessun documento contratto associato
     */
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, null);
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto missing
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null)).thenReturn(Optional.empty());
    //doc dispositivo missing
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.empty());
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isTrue();
    assertThat(richiesta.getUuidDocuments()).hasSize(0);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }

  //2
  @Test
  public void evaluateTrueWhenDocDispositivoMissingContrattoPresentDocContrattoMissing(){
    /*

      Non c'è il documento del dispositivo.
      Non c'è il contratto e c'è un documento relativo al contratto
     */
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, "1412412");
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);
    //doc dispositivo missing
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.empty());
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isTrue();
    assertThat(richiesta.getUuidDocuments()).hasSize(0);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }

  //3
  @Test
  public void evaluateTrueWhenDocDispositivoMissingCodContrattoPresent(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, "1241241");
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto present
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);

    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isTrue();
    assertThat(richiesta.getUuidDocuments()).hasSize(0);

    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }

  //4
  public void evaluateTrueWhenDocDispositivoMissingCodContrattoMissingDocContrattoPresent(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, null);
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto present
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null))
      .thenReturn(Optional.of(expectedDocument));
    //doc dispositivo present
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.empty());
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isTrue();
    assertThat(richiesta.getUuidDocuments()).hasSize(1);
    assertThat(richiesta.getUuidDocuments()).containsExactlyInAnyOrder(idDoc1);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }

  //5
  public void evaluateTrueWhenDocDispositivoPresentCodContrattoMissingDocContrattoMissing(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, null);
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto present
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null))
      .thenReturn(Optional.empty());
    //doc dispositivo present
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.of(expectedDocument2));
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isFalse();
    assertThat(richiesta.getUuidDocuments()).hasSize(1);
    assertThat(richiesta.getUuidDocuments()).containsExactlyInAnyOrder(idDoc2);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }
  //6
  @Test
  public void evaluateFalseWhenDocumentiDispAndDocContrattoPresent(){
    /*
      Pur non essendoci il codice contratto esiste un documento per la richiesta contratto.
      Il documento relativo al dispositivo è presente.
      Entrambi i documenti vanno aggiunti alla richiesta in modo che possano essere validati
     */
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, "15214512512");
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto present
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);

    //doc dispositivo present
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.of(expectedDocument2));
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isFalse();
    assertThat(richiesta.getUuidDocuments()).hasSize(1);
    assertThat(richiesta.getUuidDocuments()).containsExactlyInAnyOrder(idDoc2);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }

  //7
  public void evaluateFalseWhenDocumentDispPresentCodContrattoMissingDocContrattoPresent(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, null);
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    //doc contratto present
    DocumentoDTO expectedDocument = new DocumentoDTO();
    expectedDocument.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument.setIdentificativo(idDoc1);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null))
      .thenReturn(Optional.of(expectedDocument));
    //doc dispositivo present
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.of(expectedDocument2));
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isFalse();
    assertThat(richiesta.getUuidDocuments()).hasSize(2);
    assertThat(richiesta.getUuidDocuments()).containsExactlyInAnyOrder(idDoc1,idDoc2);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
        TipoDispositivoEnum.TOLL_COLLECT,
        null,null);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }
  //8
  @Test
  public void evaluateFalseWhenCodContrattoDocDispositivoPresent(){
    /*
      Codice contratto presente quindi il contratto può considerarsi validato.
      Di conseguenza controlla solo il documento del dispositivo è presente, ed è ok
     */
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, "141241241");
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    DocumentoDTO expectedDocument1 = new DocumentoDTO();
    expectedDocument1.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument1.setIdentificativo(idDoc1);
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    expectedDocument1.setId(2l);
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);

    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenReturn(Optional.of(expectedDocument2));
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isFalse();
    assertThat(richiesta.getUuidDocuments()).hasSize(1).contains(idDoc2);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);

  }


  @Test
  public void expectedExceptionContrattoMissing(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    String expectedCodiceContratto="1191241412";
    DocumentoDTO expectedDocument1 = new DocumentoDTO();
    expectedDocument1.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument1.setIdentificativo(idDoc1);
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    expectedDocument1.setId(2l);
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, expectedCodiceContratto);
    richiesta.setContratto(null);
    richiesta.setUuidDocuments(idDoc1);
    richiesta.setId(1l);
    richiesta.setIdentificativo("idRichiesta1");
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    Assertions.assertThatExceptionOfType(ContrattoRichiestaException.class).isThrownBy(()->{
      guard.evaluate(context);
    }).matches(e->e.getMessage().equals("Not found contract related on Richiesta ID: " + richiesta.getId() + " - Identificativo: " +richiesta.getIdentificativo()));
    verifyNoMoreInteractions(documentService);
  }

  @Test
  public void evaluateTrueIfExceptionInDocumentService(){
    String expectedCodiceCliente = "fai12314";
    String expectedTarga = "sa124141";
    String expectedTargaNazione = "it";
    String expectedCodiceContratto="1191241412";
    DocumentoDTO expectedDocument1 = new DocumentoDTO();
    expectedDocument1.setId(1l);
    String idDoc1 = "idDoc1";
    expectedDocument1.setIdentificativo(idDoc1);
    DocumentoDTO expectedDocument2 = new DocumentoDTO();
    expectedDocument1.setId(2l);
    String idDoc2 = "idDoc2";
    expectedDocument2.setIdentificativo(idDoc2);
    Richiesta richiesta = stubRichiesta(expectedCodiceCliente,expectedTarga, expectedTargaNazione, expectedCodiceContratto);
    MessageHeaders headers = mock(MessageHeaders.class);
    Message<RichiestaEvent> message = (Message<RichiestaEvent>) mock(Message.class);
    StateContext<StatoRichiesta, RichiestaEvent> context = (StateContext<StatoRichiesta,RichiestaEvent>)mock(StateContext.class);
    when(headers.get("object")).thenReturn(richiesta);
    when(message.getHeaders()).thenReturn(
      headers
    );
    when(context.getMessage()).thenReturn(
      message
    );
    when(documentService
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione)).thenThrow(mock(FeignException.class));
    boolean evaluateResult = guard.evaluate(context);
    assertThat(evaluateResult).isTrue();
    assertThat(richiesta.getUuidDocuments()).hasSize(0);
    verify(documentService)
      .findLastDocument(
        expectedCodiceCliente,
        DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
        TipoDispositivoEnum.TOLL_COLLECT,
        expectedTarga,expectedTargaNazione);
  }


  private Richiesta stubRichiesta(String expectedCodiceCliente, String expectedTarga, String expectedTargaNazione, String expectedCodContratto){
    Richiesta richiesta = new Richiesta()
      .contratto(new Contratto().codContrattoCliente(expectedCodContratto).clienteFai(new ClienteFai().codiceCliente(expectedCodiceCliente)))
      .associazione(expectedTarga)
      .country(expectedTargaNazione);
    return richiesta;
  }
}
