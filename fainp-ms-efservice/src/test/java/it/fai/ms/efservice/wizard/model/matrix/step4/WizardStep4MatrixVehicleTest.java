package it.fai.ms.efservice.wizard.model.matrix.step4;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.service.Step4Service;
import it.fai.ms.efservice.wizard.web.rest.vm.step4.MatrixStep4;
import it.fai.ms.efservice.wizard.web.rest.vm.step4.MatrixStep4Vehicle;

public class WizardStep4MatrixVehicleTest {

  Set<WizardStep4MatrixVehicle> wizardStep4MatrixVehicles;

  @Before
  public void setUp() throws Exception {
    wizardStep4MatrixVehicles = new LinkedHashSet<>();

    WizardStep4MatrixVehicle v1 = createWizardDevice("ES");
    WizardStep4MatrixVehicle v2 = createWizardDevice("EE");
    WizardStep4MatrixVehicle v3 = createWizardDevice("ES");
    WizardStep4MatrixVehicle v4 = createWizardDevice("EE");

    wizardStep4MatrixVehicles.add(v1);
    wizardStep4MatrixVehicles.add(v2);
    wizardStep4MatrixVehicles.add(v3);
    wizardStep4MatrixVehicles.add(v4);
  }

  private static WizardStep4MatrixVehicle createWizardDevice(String targaNazione) {
    // Testing equals on license plate nation.
    WizardStep4MatrixVehicle vehicle = new WizardStep4MatrixVehicle("bbb9db2b-222e-4ba4-8c3b-1eefb23d0446");
    vehicle.setType("TRATTORE");
    vehicle.setEuroClass("EURO5");
    vehicle.setLicensePlate("0674HTF");
    vehicle.setTargaNazione(targaNazione);

    WizardStep4MatrixDeviceType device1 = new WizardStep4MatrixDeviceType("TELEPASS_EUROPEO_SAT");
    device1.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVE));
    device1.setActivationDate(Instant.EPOCH);
    device1.setSeriale("NO_SERIALE");

    vehicle.getDeviceTypes()
           .put("", device1);

    return vehicle;
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEqualAndHashcode() {
    assertThat(wizardStep4MatrixVehicles).hasSize(2);
  }

  @Test
  public void testEqualAndHashCodeMatrixStep4Vehicle() {
    final MatrixStep4 matrix = new MatrixStep4();
    final AtomicInteger sequence = new AtomicInteger(1);

    List<MatrixStep4Vehicle> matrixVehicles = wizardStep4MatrixVehicles.stream()
                                                                       .map(wv -> mapToMatrixVehicle(sequence, wv))
                                                                       .collect(toList());

    assertThat(matrixVehicles).hasSize(2);

    matrix.getContent()
          .addAll(matrixVehicles);
    assertThat(matrix.getContent()).hasSize(2);
  }

  private MatrixStep4Vehicle mapToMatrixVehicle(AtomicInteger sequence, WizardStep4MatrixVehicle wv) {
    MatrixStep4Vehicle matrixVehicle = new MatrixStep4Vehicle(wv.getUuid());
    matrixVehicle.setTarga(wv.getLicensePlate());
    matrixVehicle.setTargaNazione(wv.getTargaNazione());
    matrixVehicle.setTipo(wv.getType());
    matrixVehicle.setEuro(wv.getEuroClass());
    matrixVehicle.setAnomaly(false);
    return matrixVehicle;
  }

}
