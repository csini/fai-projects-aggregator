package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_VisproTest {
  
  @Test
  public void testVehicleEuroClassGreaterThan2AndHeightGreaterThan3AndGrossLessEqualThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setEuroClass("EURO3");
    newRuleContextVehicle.setHeight(3.01f);
    newRuleContextVehicle.getWeight().setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_Vispro(newRuleContextDeviceType(), newRuleContextVehicle,
                                                           newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight are not set or less/equal than 3500");
  }
  
  @Test
  public void testVehicleEuroClassGreaterThan2AndHeightGreaterThan3AndGrossGreaterThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setEuroClass("EURO3");
    newRuleContextVehicle.setHeight(3.01f);
    newRuleContextVehicle.getWeight().setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_Vispro(newRuleContextDeviceType(), newRuleContextVehicle,
                                                           newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }
  
  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

}
