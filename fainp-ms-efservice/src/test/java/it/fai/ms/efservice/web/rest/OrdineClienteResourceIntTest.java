package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.repository.OrdineClienteRepository;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.service.dto.OrdineClienteDTO;
import it.fai.ms.efservice.service.mapper.OrdineClienteMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;

/**
 * Test class for the OrdineClienteResource REST controller.
 *
 * @see OrdineClienteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class OrdineClienteResourceIntTest {

  private static final Integer DEFAULT_PREVISIONE_SPESA_MENSILE = 1;
  private static final Integer UPDATED_PREVISIONE_SPESA_MENSILE = 2;

  private static final String DEFAULT_UUID_VEICOLO = "AAAAAAAAAA";
  private static final String UPDATED_UUID_VEICOLO = "BBBBBBBBBB";

  private static final Boolean DEFAULT_DESTINAZIONE_FAI = false;
  private static final Boolean UPDATED_DESTINAZIONE_FAI = true;

  private static final String DEFAULT_NUMERO_ORDINE = "AAAAAAAAAA";
  private static final String UPDATED_NUMERO_ORDINE = "BBBBBBBBBB";

  private static final StatoOrdineCliente DEFAULT_STATO = StatoOrdineCliente.DA_EVADERE;
  private static final StatoOrdineCliente UPDATED_STATO = StatoOrdineCliente.LAVORATO_PARZIALMENTE;

  private static final Instant DEFAULT_DATA_MODIFICA_STATO = Instant.ofEpochMilli(0L);
  private static final Instant UPDATED_DATA_MODIFICA_STATO = Instant.now()
                                                                    .truncatedTo(ChronoUnit.MILLIS);

  private static final Instant DEFAULT_DATA_CREAZIONE = Instant.ofEpochMilli(0L);
  private static final Instant UPDATED_DATA_CREAZIONE = Instant.now()
                                                               .truncatedTo(ChronoUnit.MILLIS);

  private static final String DEFAULT_RICHIEDENTE = "AAAAAAAAAA";
  private static final String UPDATED_RICHIEDENTE = "BBBBBBBBBB";

  private static final TipoOrdineCliente DEFAULT_TIPO = TipoOrdineCliente.NUOVO_ORDINE;
  private static final TipoOrdineCliente UPDATED_TIPO = TipoOrdineCliente.VARIAZIONE;

  private static IndirizzoSpedizioneOrdini indirizzoTransito;

  private static IndirizzoSpedizioneOrdini indirizzoDestFinale;

  @Autowired
  private OrdineClienteRepository ordineClienteRepository;

  @Autowired
  private OrdineClienteMapper ordineClienteMapper;

  @Autowired
  private OrdineClienteService ordineClienteService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  @Autowired
  private IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepo;

  private MockMvc restOrdineClienteMockMvc;

  private OrdineCliente ordineCliente;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final OrdineClienteResource ordineClienteResource = new OrdineClienteResource(ordineClienteService);
    this.restOrdineClienteMockMvc = MockMvcBuilders.standaloneSetup(ordineClienteResource)
                                                   .setCustomArgumentResolvers(pageableArgumentResolver)
                                                   .setControllerAdvice(exceptionTranslator)
                                                   .setMessageConverters(jacksonMessageConverter)
                                                   .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static OrdineCliente createEntity(EntityManager em) {
    OrdineCliente ordineCliente = new OrdineCliente().previsioneSpesaMensile(DEFAULT_PREVISIONE_SPESA_MENSILE)
                                                     .uuidVeicolo(DEFAULT_UUID_VEICOLO)
                                                     .destinazioneFai(DEFAULT_DESTINAZIONE_FAI)
                                                     .numeroOrdine(DEFAULT_NUMERO_ORDINE)
                                                     .stato(DEFAULT_STATO)
                                                     .dataModificaStato(DEFAULT_DATA_MODIFICA_STATO)
                                                     .dataCreazione(DEFAULT_DATA_CREAZIONE)
                                                     .richiedente(DEFAULT_RICHIEDENTE)
                                                     .tipo(DEFAULT_TIPO)
                                                     .indirizzoDiTransito(indirizzoTransito)
                                                     .indirizzoDestinazioneFinale(indirizzoDestFinale);
    return ordineCliente;
  }

  @Before
  public void initTest() {
    indirizzoTransito = indirizzoSpedizioneOrdiniRepo.save(new IndirizzoSpedizioneOrdini());
    indirizzoDestFinale = indirizzoSpedizioneOrdiniRepo.save(new IndirizzoSpedizioneOrdini());
    ordineCliente = createEntity(em);
  }

  @Test
  @Transactional
  public void createOrdineCliente() throws Exception {
    int databaseSizeBeforeCreate = ordineClienteRepository.findAll()
                                                          .size();

    // Create the OrdineCliente
    OrdineClienteDTO ordineClienteDTO = ordineClienteMapper.toDto(ordineCliente);
    restOrdineClienteMockMvc.perform(post("/api/ordine-clientes").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                 .content(TestUtil.convertObjectToJsonBytes(ordineClienteDTO)))
                            .andExpect(status().isCreated());

    // Validate the OrdineCliente in the database
    List<OrdineCliente> ordineClienteList = ordineClienteRepository.findAll();
    assertThat(ordineClienteList).hasSize(databaseSizeBeforeCreate + 1);
    OrdineCliente testOrdineCliente = ordineClienteList.get(ordineClienteList.size() - 1);
    assertThat(testOrdineCliente.getPrevisioneSpesaMensile()).isEqualTo(DEFAULT_PREVISIONE_SPESA_MENSILE);
    assertThat(testOrdineCliente.getUuidVeicolo()).isEqualTo(DEFAULT_UUID_VEICOLO);
    assertThat(testOrdineCliente.isDestinazioneFai()).isEqualTo(DEFAULT_DESTINAZIONE_FAI);
    assertThat(testOrdineCliente.getNumeroOrdine()).isEqualTo(DEFAULT_NUMERO_ORDINE);
    assertThat(testOrdineCliente.getStato()).isEqualTo(DEFAULT_STATO);
    assertThat(testOrdineCliente.getDataModificaStato()).isEqualTo(DEFAULT_DATA_MODIFICA_STATO);
    assertThat(testOrdineCliente.getDataCreazione()).isEqualTo(DEFAULT_DATA_CREAZIONE);
    assertThat(testOrdineCliente.getRichiedente()).isEqualTo(DEFAULT_RICHIEDENTE);
    assertThat(testOrdineCliente.getTipo()).isEqualTo(DEFAULT_TIPO);
  }

  @Test
  @Transactional
  public void createOrdineClienteWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = ordineClienteRepository.findAll()
                                                          .size();

    // Create the OrdineCliente with an existing ID
    ordineCliente.setId(1L);
    OrdineClienteDTO ordineClienteDTO = ordineClienteMapper.toDto(ordineCliente);

    // An entity with an existing ID cannot be created, so this API call must fail
    restOrdineClienteMockMvc.perform(post("/api/ordine-clientes").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                 .content(TestUtil.convertObjectToJsonBytes(ordineClienteDTO)))
                            .andExpect(status().isBadRequest());

    // Validate the OrdineCliente in the database
    List<OrdineCliente> ordineClienteList = ordineClienteRepository.findAll();
    assertThat(ordineClienteList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void getAllOrdineClientes() throws Exception {
    // Initialize the database
    ordineClienteRepository.saveAndFlush(ordineCliente);

    // Get all the ordineClienteList
    restOrdineClienteMockMvc.perform(get("/api/ordine-clientes?sort=id,desc"))
                            .andExpect(status().isOk())
                            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .andExpect(jsonPath("$.[*].id").value(hasItem(ordineCliente.getId()
                                                                                       .intValue())))
                            .andExpect(jsonPath("$.[*].previsioneSpesaMensile").value(hasItem(DEFAULT_PREVISIONE_SPESA_MENSILE)))
                            .andExpect(jsonPath("$.[*].uuidVeicolo").value(hasItem(DEFAULT_UUID_VEICOLO.toString())))
                            .andExpect(jsonPath("$.[*].destinazioneFai").value(hasItem(DEFAULT_DESTINAZIONE_FAI.booleanValue())))
                            .andExpect(jsonPath("$.[*].numeroOrdine").value(hasItem(DEFAULT_NUMERO_ORDINE.toString())))
                            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
                            .andExpect(jsonPath("$.[*].dataModificaStato").value(hasItem(DEFAULT_DATA_MODIFICA_STATO.toString())))
                            .andExpect(jsonPath("$.[*].dataCreazione").value(hasItem(DEFAULT_DATA_CREAZIONE.toString())))
                            .andExpect(jsonPath("$.[*].richiedente").value(hasItem(DEFAULT_RICHIEDENTE.toString())))
                            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())));
  }

  @Test
  @Transactional
  public void getOrdineCliente() throws Exception {
    // Initialize the database
    ordineClienteRepository.saveAndFlush(ordineCliente);

    // Get the ordineCliente
    restOrdineClienteMockMvc.perform(get("/api/ordine-clientes/{id}", ordineCliente.getId()))
                            .andExpect(status().isOk())
                            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .andExpect(jsonPath("$.id").value(ordineCliente.getId()
                                                                           .intValue()))
                            .andExpect(jsonPath("$.previsioneSpesaMensile").value(DEFAULT_PREVISIONE_SPESA_MENSILE))
                            .andExpect(jsonPath("$.uuidVeicolo").value(DEFAULT_UUID_VEICOLO.toString()))
                            .andExpect(jsonPath("$.destinazioneFai").value(DEFAULT_DESTINAZIONE_FAI.booleanValue()))
                            .andExpect(jsonPath("$.numeroOrdine").value(DEFAULT_NUMERO_ORDINE.toString()))
                            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
                            .andExpect(jsonPath("$.dataModificaStato").value(DEFAULT_DATA_MODIFICA_STATO.toString()))
                            .andExpect(jsonPath("$.dataCreazione").value(DEFAULT_DATA_CREAZIONE.toString()))
                            .andExpect(jsonPath("$.richiedente").value(DEFAULT_RICHIEDENTE.toString()))
                            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingOrdineCliente() throws Exception {
    // Get the ordineCliente
    restOrdineClienteMockMvc.perform(get("/api/ordine-clientes/{id}", Long.MAX_VALUE))
                            .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateOrdineCliente() throws Exception {
    // Initialize the database
    ordineClienteRepository.saveAndFlush(ordineCliente);
    int databaseSizeBeforeUpdate = ordineClienteRepository.findAll()
                                                          .size();

    // Update the ordineCliente
    OrdineCliente updatedOrdineCliente = ordineClienteRepository.findOne(ordineCliente.getId());
    updatedOrdineCliente.previsioneSpesaMensile(UPDATED_PREVISIONE_SPESA_MENSILE)
                        .uuidVeicolo(UPDATED_UUID_VEICOLO)
                        .destinazioneFai(UPDATED_DESTINAZIONE_FAI)
                        .numeroOrdine(UPDATED_NUMERO_ORDINE)
                        .stato(UPDATED_STATO)
                        .dataModificaStato(UPDATED_DATA_MODIFICA_STATO)
                        .dataCreazione(UPDATED_DATA_CREAZIONE)
                        .richiedente(UPDATED_RICHIEDENTE)
                        .tipo(UPDATED_TIPO);
    OrdineClienteDTO ordineClienteDTO = ordineClienteMapper.toDto(updatedOrdineCliente);

    restOrdineClienteMockMvc.perform(put("/api/ordine-clientes").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                .content(TestUtil.convertObjectToJsonBytes(ordineClienteDTO)))
                            .andExpect(status().isOk());

    // Validate the OrdineCliente in the database
    List<OrdineCliente> ordineClienteList = ordineClienteRepository.findAll();
    assertThat(ordineClienteList).hasSize(databaseSizeBeforeUpdate);
    OrdineCliente testOrdineCliente = ordineClienteList.get(ordineClienteList.size() - 1);
    assertThat(testOrdineCliente.getPrevisioneSpesaMensile()).isEqualTo(UPDATED_PREVISIONE_SPESA_MENSILE);
    assertThat(testOrdineCliente.getUuidVeicolo()).isEqualTo(UPDATED_UUID_VEICOLO);
    assertThat(testOrdineCliente.isDestinazioneFai()).isEqualTo(UPDATED_DESTINAZIONE_FAI);
    assertThat(testOrdineCliente.getNumeroOrdine()).isEqualTo(UPDATED_NUMERO_ORDINE);
    assertThat(testOrdineCliente.getStato()).isEqualTo(UPDATED_STATO);
    assertThat(testOrdineCliente.getDataModificaStato()).isEqualTo(UPDATED_DATA_MODIFICA_STATO);
    assertThat(testOrdineCliente.getDataCreazione()).isEqualTo(UPDATED_DATA_CREAZIONE);
    assertThat(testOrdineCliente.getRichiedente()).isEqualTo(UPDATED_RICHIEDENTE);
    assertThat(testOrdineCliente.getTipo()).isEqualTo(UPDATED_TIPO);
  }

  @Test
  @Transactional
  public void updateNonExistingOrdineCliente() throws Exception {
    int databaseSizeBeforeUpdate = ordineClienteRepository.findAll()
                                                          .size();

    // Create the OrdineCliente
    OrdineClienteDTO ordineClienteDTO = ordineClienteMapper.toDto(ordineCliente);

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restOrdineClienteMockMvc.perform(put("/api/ordine-clientes").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                .content(TestUtil.convertObjectToJsonBytes(ordineClienteDTO)))
                            .andExpect(status().isCreated());

    // Validate the OrdineCliente in the database
    List<OrdineCliente> ordineClienteList = ordineClienteRepository.findAll();
    assertThat(ordineClienteList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteOrdineCliente() throws Exception {
    // Initialize the database
    ordineClienteRepository.saveAndFlush(ordineCliente);
    int databaseSizeBeforeDelete = ordineClienteRepository.findAll()
                                                          .size();

    // Get the ordineCliente
    restOrdineClienteMockMvc.perform(delete("/api/ordine-clientes/{id}", ordineCliente.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                            .andExpect(status().isOk());

    // Validate the database is empty
    List<OrdineCliente> ordineClienteList = ordineClienteRepository.findAll();
    assertThat(ordineClienteList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(OrdineCliente.class);
    OrdineCliente ordineCliente1 = new OrdineCliente();
    ordineCliente1.setId(1L);
    OrdineCliente ordineCliente2 = new OrdineCliente();
    ordineCliente2.setId(ordineCliente1.getId());
    assertThat(ordineCliente1).isEqualTo(ordineCliente2);
    ordineCliente2.setId(2L);
    assertThat(ordineCliente1).isNotEqualTo(ordineCliente2);
    ordineCliente1.setId(null);
    assertThat(ordineCliente1).isNotEqualTo(ordineCliente2);
  }

  @Test
  @Transactional
  public void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(OrdineClienteDTO.class);
    OrdineClienteDTO ordineClienteDTO1 = new OrdineClienteDTO();
    ordineClienteDTO1.setId(1L);
    OrdineClienteDTO ordineClienteDTO2 = new OrdineClienteDTO();
    assertThat(ordineClienteDTO1).isNotEqualTo(ordineClienteDTO2);
    ordineClienteDTO2.setId(ordineClienteDTO1.getId());
    assertThat(ordineClienteDTO1).isEqualTo(ordineClienteDTO2);
    ordineClienteDTO2.setId(2L);
    assertThat(ordineClienteDTO1).isNotEqualTo(ordineClienteDTO2);
    ordineClienteDTO1.setId(null);
    assertThat(ordineClienteDTO1).isNotEqualTo(ordineClienteDTO2);
  }

  @Test
  @Transactional
  public void testEntityFromId() {
    assertThat(ordineClienteMapper.fromId(42L)
                                  .getId()).isEqualTo(42);
    assertThat(ordineClienteMapper.fromId(null)).isNull();
  }
}
