package it.fai.ms.efservice.service.fsm.type.dartfordcrossing;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroDartFordCrossingTest {

  private FsmInoltroDartFordCrossing fsmInoltroDartFordCrossing;

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiesta = null;
  private Dispositivo dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
    fsmInoltroDartFordCrossing = (FsmInoltroDartFordCrossing) fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_DARTFORD_CROSSING);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = new Dispositivo();
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.DARTFORD_CROSSING)
                                                                               .modalitaSpedizione(ModalitaSpedizione.A_FAI))
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmInoltroDartFordCrossing_attivo_evaso() throws Exception {
    TipoDispositivo tipoDispositivo = new TipoDispositivo().nome(TipoDispositivoEnum.DARTFORD_CROSSING);
    richiesta.setTipoDispositivo(tipoDispositivo);

    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta);

    richiesta = fsmInoltroDartFordCrossing.executeCommandToChangeState(RichiestaEvent.MU_INOLTRATO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void FsmInoltroDartFordCrossing_annullato_operatore() throws Exception {
    TipoDispositivo tipoDispositivo = new TipoDispositivo().nome(TipoDispositivoEnum.DARTFORD_CROSSING);
    richiesta.setTipoDispositivo(tipoDispositivo);

    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta);

    richiesta = fsmInoltroDartFordCrossing.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
  }

  @Test
  public void FsmInoltroDartFordCrossing_not_available_command() throws Exception {
    try {
      richiesta = fsmInoltroDartFordCrossing.executeCommandToChangeState(RichiestaEvent.MU_INOLTRATO, richiesta);
    } catch (Exception e) {
      assertThat(e.getMessage()).startsWith("Command MU_INOLTRATO");
    }
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
  }

  private void switchStateAccettatoProntoPerInoltroToDaInoltrare(Richiesta richiesta) throws Exception {
    richiesta = fsmInoltroDartFordCrossing.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.DA_INOLTRARE);
  }

}
