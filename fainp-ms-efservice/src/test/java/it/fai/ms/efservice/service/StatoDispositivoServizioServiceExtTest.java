package it.fai.ms.efservice.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class StatoDispositivoServizioServiceExtTest {

  @Autowired
  StatoDispositivoServizioServiceExt service;
  
  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testFindbyDispositivo() throws Exception {
    Dispositivo device = new Dispositivo();
    device.setId(new Long(801));
    service.findbyDispositivo(device);
  }

}
