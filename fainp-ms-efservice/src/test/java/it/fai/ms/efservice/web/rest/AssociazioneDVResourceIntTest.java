package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.service.AssociazioneDVService;
import it.fai.ms.efservice.service.dto.AssociazioneDVDTO;
import it.fai.ms.efservice.service.mapper.AssociazioneDVMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AssociazioneDVResource REST controller.
 *
 * @see AssociazioneDVResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class AssociazioneDVResourceIntTest {

    private static final Instant DEFAULT_DATA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UUID_VEICOLO = "AAAAAAAAAA";
    private static final String UPDATED_UUID_VEICOLO = "BBBBBBBBBB";

    @Autowired
    private AssociazioneDVRepository associazioneDVRepository;

    @Autowired
    private AssociazioneDVMapper associazioneDVMapper;

    @Autowired
    private AssociazioneDVService associazioneDVService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAssociazioneDVMockMvc;

    private AssociazioneDV associazioneDV;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AssociazioneDVResource associazioneDVResource = new AssociazioneDVResource(associazioneDVService);
        this.restAssociazioneDVMockMvc = MockMvcBuilders.standaloneSetup(associazioneDVResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssociazioneDV createEntity(EntityManager em) {
        AssociazioneDV associazioneDV = new AssociazioneDV()
            .data(DEFAULT_DATA)
            .uuidVeicolo(DEFAULT_UUID_VEICOLO);
        return associazioneDV;
    }

    @Before
    public void initTest() {
        associazioneDV = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssociazioneDV() throws Exception {
        int databaseSizeBeforeCreate = associazioneDVRepository.findAll().size();

        // Create the AssociazioneDV
        AssociazioneDVDTO associazioneDVDTO = associazioneDVMapper.toDto(associazioneDV);
        restAssociazioneDVMockMvc.perform(post("/api/associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(associazioneDVDTO)))
            .andExpect(status().isCreated());

        // Validate the AssociazioneDV in the database
        List<AssociazioneDV> associazioneDVList = associazioneDVRepository.findAll();
        assertThat(associazioneDVList).hasSize(databaseSizeBeforeCreate + 1);
        AssociazioneDV testAssociazioneDV = associazioneDVList.get(associazioneDVList.size() - 1);
        assertThat(testAssociazioneDV.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testAssociazioneDV.getUuidVeicolo()).isEqualTo(DEFAULT_UUID_VEICOLO);
    }

    @Test
    @Transactional
    public void createAssociazioneDVWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = associazioneDVRepository.findAll().size();

        // Create the AssociazioneDV with an existing ID
        associazioneDV.setId(1L);
        AssociazioneDVDTO associazioneDVDTO = associazioneDVMapper.toDto(associazioneDV);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssociazioneDVMockMvc.perform(post("/api/associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(associazioneDVDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AssociazioneDV in the database
        List<AssociazioneDV> associazioneDVList = associazioneDVRepository.findAll();
        assertThat(associazioneDVList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAssociazioneDVS() throws Exception {
        // Initialize the database
        associazioneDVRepository.saveAndFlush(associazioneDV);

        // Get all the associazioneDVList
        restAssociazioneDVMockMvc.perform(get("/api/associazione-dvs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(associazioneDV.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].uuidVeicolo").value(hasItem(DEFAULT_UUID_VEICOLO.toString())));
    }

    @Test
    @Transactional
    public void getAssociazioneDV() throws Exception {
        // Initialize the database
        associazioneDVRepository.saveAndFlush(associazioneDV);

        // Get the associazioneDV
        restAssociazioneDVMockMvc.perform(get("/api/associazione-dvs/{id}", associazioneDV.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(associazioneDV.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
            .andExpect(jsonPath("$.uuidVeicolo").value(DEFAULT_UUID_VEICOLO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAssociazioneDV() throws Exception {
        // Get the associazioneDV
        restAssociazioneDVMockMvc.perform(get("/api/associazione-dvs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssociazioneDV() throws Exception {
        // Initialize the database
        associazioneDVRepository.saveAndFlush(associazioneDV);
        int databaseSizeBeforeUpdate = associazioneDVRepository.findAll().size();

        // Update the associazioneDV
        AssociazioneDV updatedAssociazioneDV = associazioneDVRepository.findOne(associazioneDV.getId());
        updatedAssociazioneDV
            .data(UPDATED_DATA)
            .uuidVeicolo(UPDATED_UUID_VEICOLO);
        AssociazioneDVDTO associazioneDVDTO = associazioneDVMapper.toDto(updatedAssociazioneDV);

        restAssociazioneDVMockMvc.perform(put("/api/associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(associazioneDVDTO)))
            .andExpect(status().isOk());

        // Validate the AssociazioneDV in the database
        List<AssociazioneDV> associazioneDVList = associazioneDVRepository.findAll();
        assertThat(associazioneDVList).hasSize(databaseSizeBeforeUpdate);
        AssociazioneDV testAssociazioneDV = associazioneDVList.get(associazioneDVList.size() - 1);
        assertThat(testAssociazioneDV.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testAssociazioneDV.getUuidVeicolo()).isEqualTo(UPDATED_UUID_VEICOLO);
    }

    @Test
    @Transactional
    public void updateNonExistingAssociazioneDV() throws Exception {
        int databaseSizeBeforeUpdate = associazioneDVRepository.findAll().size();

        // Create the AssociazioneDV
        AssociazioneDVDTO associazioneDVDTO = associazioneDVMapper.toDto(associazioneDV);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAssociazioneDVMockMvc.perform(put("/api/associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(associazioneDVDTO)))
            .andExpect(status().isCreated());

        // Validate the AssociazioneDV in the database
        List<AssociazioneDV> associazioneDVList = associazioneDVRepository.findAll();
        assertThat(associazioneDVList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAssociazioneDV() throws Exception {
        // Initialize the database
        associazioneDVRepository.saveAndFlush(associazioneDV);
        int databaseSizeBeforeDelete = associazioneDVRepository.findAll().size();

        // Get the associazioneDV
        restAssociazioneDVMockMvc.perform(delete("/api/associazione-dvs/{id}", associazioneDV.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AssociazioneDV> associazioneDVList = associazioneDVRepository.findAll();
        assertThat(associazioneDVList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssociazioneDV.class);
        AssociazioneDV associazioneDV1 = new AssociazioneDV();
        associazioneDV1.setId(1L);
        AssociazioneDV associazioneDV2 = new AssociazioneDV();
        associazioneDV2.setId(associazioneDV1.getId());
        assertThat(associazioneDV1).isEqualTo(associazioneDV2);
        associazioneDV2.setId(2L);
        assertThat(associazioneDV1).isNotEqualTo(associazioneDV2);
        associazioneDV1.setId(null);
        assertThat(associazioneDV1).isNotEqualTo(associazioneDV2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssociazioneDVDTO.class);
        AssociazioneDVDTO associazioneDVDTO1 = new AssociazioneDVDTO();
        associazioneDVDTO1.setId(1L);
        AssociazioneDVDTO associazioneDVDTO2 = new AssociazioneDVDTO();
        assertThat(associazioneDVDTO1).isNotEqualTo(associazioneDVDTO2);
        associazioneDVDTO2.setId(associazioneDVDTO1.getId());
        assertThat(associazioneDVDTO1).isEqualTo(associazioneDVDTO2);
        associazioneDVDTO2.setId(2L);
        assertThat(associazioneDVDTO1).isNotEqualTo(associazioneDVDTO2);
        associazioneDVDTO1.setId(null);
        assertThat(associazioneDVDTO1).isNotEqualTo(associazioneDVDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(associazioneDVMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(associazioneDVMapper.fromId(null)).isNull();
    }
}
