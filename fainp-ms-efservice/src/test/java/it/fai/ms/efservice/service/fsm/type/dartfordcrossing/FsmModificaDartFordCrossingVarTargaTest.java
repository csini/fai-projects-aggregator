package it.fai.ms.efservice.service.fsm.type.dartfordcrossing;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaDartFordCrossingVarTargaTest {

  @Autowired
  private FsmRichiestaCacheService cacheService;

  @Autowired
  private FsmFactory fsmFactory;

  private AbstractFsmRichiesta fsm;

  private Richiesta richiesta;

  @Before
  public void setUp() throws Exception {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_DARTFORD_CROSSING);
    richiesta = newRichiesta(TipoRichiesta.VARIAZIONE_TARGA);
  }

  public void destroy() {
    cacheService.clearCache();
  }

  @Test
  public void testFsmFromStateAttivoPerModificaDartFordCrossingVarTargaToStateIniziale() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.INIZIALE);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INIZIALE);
  }

  @Test
  public void testFsmFromStateAttivoPerModificaDartFordCrossingVarTargaToStateAttivoEvaso() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.ATTIVO_EVASO);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);

  }

  @Test
  public void testFsmFromStateAttivoPerModificaDartFordCrossingVarTargaToStateOrdineSospeso() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.ORDINE_SOSPESO);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_SOSPENDI, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ORDINE_SOSPESO);
  }

  @Test
  public void testFsmFromStateAttivoPerModificaDartFordCrossingVarTargaToStateRichiestaRifiutata() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.RICHIESTA_RIFIUTATA);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_RIFIUTATA_RICHIESTA, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.RICHIESTA_RIFIUTATA);
  }

  @Test
  public void testFsmFromOrdineSospesoToStateAttivoEvaso() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.ORDINE_SOSPESO);
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.ATTIVO_EVASO);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void testFsmFromOrdineSospesoToStateRichiestaRifiutata() throws Exception {
    richiesta = changeRequestStatusTo(richiesta, StatoRichiesta.ORDINE_SOSPESO);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_RIFIUTATA_RICHIESTA, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.RICHIESTA_RIFIUTATA);
  }

  private Richiesta changeRequestStatusTo(Richiesta request, StatoRichiesta stateTo) throws Exception {
    switch (stateTo) {
    case RICHIESTA_RIFIUTATA:
      if (request.getStato()
                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING)) {
        request = changeRequestStatusTo(request, StatoRichiesta.INIZIALE);
      }
      request = fsm.executeCommandToChangeState(RichiestaEvent.MU_RIFIUTATA_RICHIESTA, request);
      break;

    case ORDINE_SOSPESO:
      if (request.getStato()
                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING)) {
        request = changeRequestStatusTo(request, StatoRichiesta.INIZIALE);
      }
      request = fsm.executeCommandToChangeState(RichiestaEvent.MU_SOSPENDI, request);
      break;

    case ATTIVO_EVASO:
      if (request.getStato()
                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING)) {
        request = changeRequestStatusTo(request, StatoRichiesta.INIZIALE);
      }
      request = fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, request);
      break;

    case INIZIALE:
      request = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, request);
      break;

    default:
      throw new IllegalArgumentException("Not manage change status to reuqest " + request + " into the state " + stateTo);
    }
    return request;
  }

  private Richiesta newRichiesta(TipoRichiesta requestType) {
    Richiesta richiesta = new Richiesta().tipo(requestType)
                                         .identificativo(RandomStringUtils.random(25))
                                         .data(Instant.now())
                                         .tipoDispositivo(newTipoDispositivo())
                                         .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_DARTFORD_CROSSING)
                                         .ordineCliente(newOrdineCliente());

    richiesta.setId(RandomUtils.nextLong());
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai())
                                          .numeroOrdine(RandomStringUtils.random(8));
    return oc;
  }

  private ClienteFai newClienteFai() {
    ClienteFai cf = new ClienteFai().codiceCliente("::codCliente::");
    return cf;
  }

  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo deviceType = new TipoDispositivo().nome(TipoDispositivoEnum.DARTFORD_CROSSING);
    return deviceType;
  }

}
