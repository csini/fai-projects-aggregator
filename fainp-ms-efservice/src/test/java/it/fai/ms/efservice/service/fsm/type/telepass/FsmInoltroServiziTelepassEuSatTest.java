package it.fai.ms.efservice.service.fsm.type.telepass;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.ServiceUuid;
import it.fai.ms.common.jms.efservice.message.device.model.DeviceService;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.producer.device.OrderRequestDecorator;

public class FsmInoltroServiziTelepassEuSatTest {

  private Richiesta request;

  private final String ITALIA     = "PEDAGGI_ITALIA";
  private final String FRANCIA    = "PEDAGGI_FRANCIA";
  private final String SPAGNA     = "PEDAGGI_SPAGNA";
  private final String PORTOGALLO = "PEDAGGI_PORTOGALLO";
  private final String GERMANIA   = "PEDAGGI_GERMANIA";

  private List<String> tipoServizios = Arrays.asList(ITALIA, SPAGNA, PORTOGALLO, FRANCIA, GERMANIA);

  private List<String> tipoServiziosToActivated = Arrays.asList(ServiceUuid.fromServiceTypeName(ITALIA)
                                                                           .getUuid(),
                                                                ServiceUuid.fromServiceTypeName(SPAGNA)
                                                                           .getUuid(),
                                                                ServiceUuid.fromServiceTypeName(PORTOGALLO)
                                                                           .getUuid());

  private List<String> tipoServiziosToDeactivated = Arrays.asList(ServiceUuid.fromServiceTypeName(GERMANIA)
                                                                             .getUuid(),
                                                                  ServiceUuid.fromServiceTypeName(FRANCIA)
                                                                             .getUuid());

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testAttivazioneServizio() throws FsmExecuteCommandException {
    request = newRichiesta(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT, TipoRichiesta.ATTIVAZIONE_SERVIZIO);
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);

    Set<DeviceService> deviceServices = DeviceService.fromDeviceServiceNames(orderRequest.findActiveServicesNames());
    List<String> services = deviceServices.stream()
                                          .map(ds -> ds.getServiceUuid()
                                                       .getUuid())
                                          .collect(toList());

    assertThat(tipoServiziosToActivated).containsExactlyElementsOf(services);
  }

  @Test
  public void testDisattivazioneServizio() throws FsmExecuteCommandException {
    request = newRichiesta(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT, TipoRichiesta.DISATTIVAZIONE_SERVIZIO);
    final OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);
    Set<DeviceService> deviceServices = DeviceService.fromDeviceServiceNames(orderRequest.findNotActiveServicesNames());

    List<String> services = deviceServices.stream()
                                          .map(ds -> ds.getServiceUuid()
                                                       .getUuid())
                                          .collect(toList());
    assertThat(tipoServiziosToDeactivated).containsExactlyElementsOf(services);

  }

  private Richiesta newRichiesta(TipoDispositivoEnum deviceType, TipoRichiesta tipoRichiesta) {
    Dispositivo device = newDispositivo(deviceType);
    Richiesta r = new Richiesta();
    r.associazione("::targa::")
     .addDispositivo(device)
     .tipoDispositivo(device.getTipoDispositivo())
     .tipo(tipoRichiesta);
    return r;
  }

  private Dispositivo newDispositivo(TipoDispositivoEnum deviceType) {
    Dispositivo d = new Dispositivo();
    d.tipoDispositivo(new TipoDispositivo().nome(deviceType));
    d.statoDispositivoServizios(newStatoDispositivoServizios());
    d.setContratto(newContratto());
    return d;
  }

  private Contratto newContratto() {
    Contratto c = new Contratto();
    c.setClienteFai(newClienteFai());
    c.setCodContrattoCliente("::ContractCode::");
    c.setProduttore(newProduttore());
    return c;
  }

  private Produttore newProduttore() {
    return new Produttore().nome("TELEPASS");
  }

  private ClienteFai newClienteFai() {
    return new ClienteFai().codiceCliente("::codiceCliente::");
  }

  private Set<StatoDispositivoServizio> newStatoDispositivoServizios() {
    Set<StatoDispositivoServizio> sdss = new HashSet<>();
    tipoServizios.forEach(ts -> {
      StatoDispositivoServizio sds = new StatoDispositivoServizio();
      TipoServizio tipoServizio = new TipoServizio();
      tipoServizio.setNome(ts);
      sds.setTipoServizio(tipoServizio);
      sds.stato((ts.equalsIgnoreCase(GERMANIA) ? StatoDS.NON_ATTIVO
                                               : (ts.equalsIgnoreCase(FRANCIA) ? StatoDS.IN_DISATTIVAZIONE
                                                                               : (ts.equalsIgnoreCase(PORTOGALLO) ? StatoDS.IN_ATTIVAZIONE
                                                                                                                  : StatoDS.ATTIVO))));

      sdss.add(sds);
    });

    return sdss;
  }

}
