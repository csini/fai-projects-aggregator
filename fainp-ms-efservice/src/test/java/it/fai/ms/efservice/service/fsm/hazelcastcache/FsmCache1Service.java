/**
 * 
 */
package it.fai.ms.efservice.service.fsm.hazelcastcache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.ExtendedState;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.access.StateMachineFunction;
import org.springframework.statemachine.region.Region;
import org.springframework.statemachine.state.AbstractState;
import org.springframework.statemachine.state.HistoryPseudoState;
import org.springframework.statemachine.state.PseudoState;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.AbstractStateMachine;
import org.springframework.statemachine.support.DefaultExtendedState;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Component;

import it.fai.ms.efservice.service.fsm.cacheconfig.FsmInfinispanConfigFactory;
import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.util.TestStateMachineContextSerializer;

/**
 * @author Luca Vassallo
 */
@Component
public class FsmCache1Service {

  private static Logger log = LoggerFactory.getLogger(FsmCache1Service.class);

  @Autowired
  private TestStateMachineContextSerializer serializer;

  @Autowired
  private FsmInfinispanConfigFactory infinispanConfig;

  private Cache<String, String> cache;

  @PostConstruct
  public void init() {
    if (log.isDebugEnabled()) {
      log.debug("Init service FSM_CACHE_1...");
    }
    cache = infinispanConfig.buildCache("fsm1");
  }

  @PreDestroy
  public void preDestroy() {
    if (log.isDebugEnabled()) {
      log.debug("Closing cache manager");
    }
    EmbeddedCacheManager cacheManager = cache.getCacheManager();
    cacheManager.stop();
  }

  public void putStateMachineContext(String key, String json) {
    if (log.isDebugEnabled()) {
      log.debug("Put in map [Key: " + key + " String: " + json + "]");
    }
    cache.put(key, json);
  }

  public String getStateMachineContext(String key) {
    if (log.isDebugEnabled()) {
      log.debug("Get object from Map with Key: " + key + "");
    }

    String json = cache.get(key);
    return json;
  }

  public void clearCache() {
    if (log.isDebugEnabled()) {
      log.debug("Clear cache " + ((cache != null) ? cache.getName() : ""));
    }
    cache.clear();
  }

  public final void persist(StateMachine<StateTest, EventTest> stateMachine, String identifier) throws Exception {

    StateMachineContext<StateTest, EventTest> buildStateMachineContext = buildStateMachineContext(stateMachine);
    String objSerialized = serializer.serialize(buildStateMachineContext);
    putStateMachineContext(identifier, objSerialized);
  }

  @SuppressWarnings("unchecked")
  public final StateMachine<StateTest, EventTest> restore(StateMachine<StateTest, EventTest> stateMachine,
                                                          String identifier) throws Exception {
    String json = getStateMachineContext(identifier);
    if (StringUtils.isBlank(json)) {
      return stateMachine;
    }
    Object deSerialize = serializer.deSerialize(json);
    StateMachineContext<StateTest, EventTest> smc = null;
    if (deSerialize instanceof StateMachineContext<?, ?>) {
      smc = (StateMachineContext<StateTest, EventTest>) deSerialize;
    }

    if (smc == null) {
      return stateMachine;
    }

    final StateMachineContext<StateTest, EventTest> context = smc;
    stateMachine.stop();
    stateMachine.getStateMachineAccessor()
                .doWithAllRegions(new StateMachineFunction<StateMachineAccess<StateTest, EventTest>>() {

                  public void apply(StateMachineAccess<StateTest, EventTest> function) {
                    function.resetStateMachine(context);
                  }

                });
    stateMachine.start();
    return stateMachine;
  }

  protected StateMachineContext<StateTest, EventTest> buildStateMachineContext(StateMachine<StateTest, EventTest> stateMachine) {
    ExtendedState extendedState = new DefaultExtendedState();
    extendedState.getVariables()
                 .putAll(stateMachine.getExtendedState()
                                     .getVariables());

    ArrayList<StateMachineContext<StateTest, EventTest>> childs = new ArrayList<StateMachineContext<StateTest, EventTest>>();
    StateTest id = null;
    State<StateTest, EventTest> state = stateMachine.getState();
    if (state.isSubmachineState()) {
      id = getDeepState(state);
    } else if (state.isOrthogonal()) {
      Collection<Region<StateTest, EventTest>> regions = ((AbstractState<StateTest, EventTest>) state).getRegions();
      for (Region<StateTest, EventTest> r : regions) {
        StateMachine<StateTest, EventTest> rsm = (StateMachine<StateTest, EventTest>) r;
        childs.add(buildStateMachineContext(rsm));
      }
      id = state.getId();
    } else {
      id = state.getId();
    }

    // building history state mappings
    Map<StateTest, StateTest> historyStates = new HashMap<StateTest, StateTest>();
    PseudoState<StateTest, EventTest> historyState = ((AbstractStateMachine<StateTest, EventTest>) stateMachine).getHistoryState();
    if (historyState != null) {
      historyStates.put(null, ((HistoryPseudoState<StateTest, EventTest>) historyState).getState()
                                                                                       .getId());
    }
    Collection<State<StateTest, EventTest>> states = stateMachine.getStates();
    for (State<StateTest, EventTest> ss : states) {
      if (ss.isSubmachineState()) {
        StateMachine<StateTest, EventTest> submachine = ((AbstractState<StateTest, EventTest>) ss).getSubmachine();
        PseudoState<StateTest, EventTest> ps = ((AbstractStateMachine<StateTest, EventTest>) submachine).getHistoryState();
        if (ps != null) {
          State<StateTest, EventTest> pss = ((HistoryPseudoState<StateTest, EventTest>) ps).getState();
          if (pss != null) {
            historyStates.put(ss.getId(), pss.getId());
          }
        }
      }
    }
    return new DefaultStateMachineContext<StateTest, EventTest>(childs, id, null, null, extendedState, historyStates, stateMachine.getId());
  }

  private StateTest getDeepState(State<StateTest, EventTest> state) {
    Collection<StateTest> ids1 = state.getIds();
    StateTest[] ids2 = (StateTest[]) ids1.toArray();
    // TODO: can this be empty as then we'd get error?
    return ids2[ids2.length - 1];
  }

}
