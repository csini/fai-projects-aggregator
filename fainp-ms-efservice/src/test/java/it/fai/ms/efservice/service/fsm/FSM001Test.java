/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestContextManager;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.testdomain.TestFSM;

/**
 * @author Luca Vassallo
 */
@RunWith(Parameterized.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore("Skipped => Fix to use Mockito")
public class FSM001Test {

  @Autowired
  private TestFiniteStateMachineService<TestFSM> fsmTest;

  private TestContextManager manager;

  @Before // Set-up
  public void setUp() throws Exception {
    this.manager = new TestContextManager(getClass());
    this.manager.prepareTestInstance(this);

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);
  }

  @Parameter(value = 0)
  public String campo1;

  @Parameter(value = 1)
  public Enum<?> transition1;

  @Parameter(value = 2)
  public String n1;

  @Parameter(value = 3)
  public Enum<?> expected1;

  @Parameter(value = 4)
  public String expTransition1;

  @Parameter(value = 5)
  public Enum<?> transition2;

  @Parameter(value = 6)
  public String n2;

  @Parameter(value = 7)
  public Enum<?> expected2;

  @Parameter(value = 8)
  public String expTransition2;

  @Parameters
  public static Collection<Object[]> data() {
    Collection<Object[]> transition = new ArrayList<>();
    transition.add(new Object[] { "DISPARI", EventTest.T1, "NOTA T1", StateTest.C, "T1T3", EventTest.T5, "NOTA T5", StateTest.D,
                                  "T1T3T5" });
    transition.add(new Object[] { "DISPARI", EventTest.T2, "NOTA T2", StateTest.C, "T2", EventTest.T5, "NOTA T5", StateTest.D, "T2T5" });
    transition.add(new Object[] { "PARI", EventTest.T1, "NOTA T1", StateTest.D, "T1T4", null, null, null, null });

    return transition;
  }

  /**
   * Test001, 002 e 003 parametrizzato; Test001 => A -> t1 -> B -> t3 -> C -> t5 -> D Test002 => A -> t2 -> C -> t5 -> D
   * Test003 => A -> t1 -> B -> t4 -> D
   * 
   * @throws Exception
   */
  @Test
  public void FsmTest001_002_003() throws Exception {
    TestFSM entityTest = new TestFSM();
    entityTest.setIdentificativo(UUID.randomUUID()
                                     .toString());
    entityTest.setStato(StateTest.A.name());
    entityTest.setCampo1(campo1);

    boolean isAvailable = fsmTest.isAvailableCommand(transition1, entityTest);
    assertTrue(isAvailable);
    String notaUtente = n1;
    fsmTest.executeCommandToChangeState(transition1, entityTest, notaUtente);
    Enum<?> actualState = entityTest.findFsmStatus();
    assertEquals(expected1, actualState);
    assertEquals(expTransition1, entityTest.getCampo2());
    assertEquals(notaUtente, entityTest.getUltimaNota());

    boolean valid = validateParameters();
    if (valid) {
      isAvailable = fsmTest.isAvailableCommand(transition2, entityTest);
      assertTrue(isAvailable);

      notaUtente = n2;
      fsmTest.executeCommandToChangeState(transition2, entityTest, notaUtente);
      actualState = entityTest.findFsmStatus();
      assertEquals(expected2, actualState);
      assertEquals(expTransition2, entityTest.getCampo2());
      assertEquals(notaUtente, entityTest.getUltimaNota());
    }

  }

  @Test
  public void FsmTestNotDefaultInitialState() throws Exception {
    TestFSM entityTest = new TestFSM();
    entityTest.setStato(StateTest.A.name());
    String uuidString = UUID.randomUUID()
                            .toString();
    entityTest.setIdentificativo(uuidString);
    entityTest.setCampo1("A");

    fsmTest.executeCommandToChangeState(EventTest.T2, entityTest, "NOTA NOT DEFAULT STATE");
    Enum<?> status = entityTest.findFsmStatus();
    assertEquals(StateTest.C, status);
    assertEquals("T2", entityTest.getCampo2());

    TestFSM entityTest2 = new TestFSM();
    entityTest2.setId(1);
    entityTest2.setIdentificativo(uuidString);
    entityTest2.setStato(StateTest.C.name());
    entityTest2.setCampo1("CA");

    fsmTest.executeCommandToChangeState(EventTest.T5, entityTest2);
    Enum<?> findFsmStatus = entityTest2.findFsmStatus();
    assertEquals(StateTest.D, findFsmStatus);
    assertEquals("T5", entityTest2.getCampo2());
  }

  @Test
  public void FsmTestAutoTransition() {
    assertFalse(fsmTest.isAutoTransition(EventTest.T1));
    assertFalse(fsmTest.isAutoTransition(EventTest.T2));
    assertFalse(fsmTest.isAutoTransition(EventTest.T5));
    assertTrue(fsmTest.isAutoTransition(EventTest.T3));
    assertTrue(fsmTest.isAutoTransition(EventTest.T4));
  }

  private boolean validateParameters() {
    if (transition2 == null || n2 == null || expected2 == null || expTransition2 == null) {
      return false;
    }
    return true;
  }

}
