package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.TipoServizioDefault;
import it.fai.ms.efservice.repository.TipoServizioDefaultRepository;
import it.fai.ms.efservice.service.TipoServizioDefaultService;
import it.fai.ms.efservice.service.dto.TipoServizioDefaultDTO;
import it.fai.ms.efservice.service.mapper.TipoServizioDefaultMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoServizioDefaultResource REST controller.
 *
 * @see TipoServizioDefaultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoServizioDefaultResourceIntTest {

    @Autowired
    private TipoServizioDefaultRepository tipoServizioDefaultRepository;

    @Autowired
    private TipoServizioDefaultMapper tipoServizioDefaultMapper;

    @Autowired
    private TipoServizioDefaultService tipoServizioDefaultService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoServizioDefaultMockMvc;

    private TipoServizioDefault tipoServizioDefault;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoServizioDefaultResource tipoServizioDefaultResource = new TipoServizioDefaultResource(tipoServizioDefaultService);
        this.restTipoServizioDefaultMockMvc = MockMvcBuilders.standaloneSetup(tipoServizioDefaultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoServizioDefault createEntity(EntityManager em) {
        TipoServizioDefault tipoServizioDefault = new TipoServizioDefault();
        return tipoServizioDefault;
    }

    @Before
    public void initTest() {
        tipoServizioDefault = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoServizioDefault() throws Exception {
        int databaseSizeBeforeCreate = tipoServizioDefaultRepository.findAll().size();

        // Create the TipoServizioDefault
        TipoServizioDefaultDTO tipoServizioDefaultDTO = tipoServizioDefaultMapper.toDto(tipoServizioDefault);
        restTipoServizioDefaultMockMvc.perform(post("/api/tipo-servizio-defaults")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDefaultDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoServizioDefault in the database
        List<TipoServizioDefault> tipoServizioDefaultList = tipoServizioDefaultRepository.findAll();
        assertThat(tipoServizioDefaultList).hasSize(databaseSizeBeforeCreate + 1);
        TipoServizioDefault testTipoServizioDefault = tipoServizioDefaultList.get(tipoServizioDefaultList.size() - 1);
    }

    @Test
    @Transactional
    public void createTipoServizioDefaultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoServizioDefaultRepository.findAll().size();

        // Create the TipoServizioDefault with an existing ID
        tipoServizioDefault.setId(1L);
        TipoServizioDefaultDTO tipoServizioDefaultDTO = tipoServizioDefaultMapper.toDto(tipoServizioDefault);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoServizioDefaultMockMvc.perform(post("/api/tipo-servizio-defaults")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDefaultDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TipoServizioDefault in the database
        List<TipoServizioDefault> tipoServizioDefaultList = tipoServizioDefaultRepository.findAll();
        assertThat(tipoServizioDefaultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTipoServizioDefaults() throws Exception {
        // Initialize the database
        tipoServizioDefaultRepository.saveAndFlush(tipoServizioDefault);

        // Get all the tipoServizioDefaultList
        restTipoServizioDefaultMockMvc.perform(get("/api/tipo-servizio-defaults?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoServizioDefault.getId().intValue())));
    }

    @Test
    @Transactional
    public void getTipoServizioDefault() throws Exception {
        // Initialize the database
        tipoServizioDefaultRepository.saveAndFlush(tipoServizioDefault);

        // Get the tipoServizioDefault
        restTipoServizioDefaultMockMvc.perform(get("/api/tipo-servizio-defaults/{id}", tipoServizioDefault.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoServizioDefault.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTipoServizioDefault() throws Exception {
        // Get the tipoServizioDefault
        restTipoServizioDefaultMockMvc.perform(get("/api/tipo-servizio-defaults/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoServizioDefault() throws Exception {
        // Initialize the database
        tipoServizioDefaultRepository.saveAndFlush(tipoServizioDefault);
        int databaseSizeBeforeUpdate = tipoServizioDefaultRepository.findAll().size();

        // Update the tipoServizioDefault
        TipoServizioDefault updatedTipoServizioDefault = tipoServizioDefaultRepository.findOne(tipoServizioDefault.getId());
        TipoServizioDefaultDTO tipoServizioDefaultDTO = tipoServizioDefaultMapper.toDto(updatedTipoServizioDefault);

        restTipoServizioDefaultMockMvc.perform(put("/api/tipo-servizio-defaults")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDefaultDTO)))
            .andExpect(status().isOk());

        // Validate the TipoServizioDefault in the database
        List<TipoServizioDefault> tipoServizioDefaultList = tipoServizioDefaultRepository.findAll();
        assertThat(tipoServizioDefaultList).hasSize(databaseSizeBeforeUpdate);
        TipoServizioDefault testTipoServizioDefault = tipoServizioDefaultList.get(tipoServizioDefaultList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoServizioDefault() throws Exception {
        int databaseSizeBeforeUpdate = tipoServizioDefaultRepository.findAll().size();

        // Create the TipoServizioDefault
        TipoServizioDefaultDTO tipoServizioDefaultDTO = tipoServizioDefaultMapper.toDto(tipoServizioDefault);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoServizioDefaultMockMvc.perform(put("/api/tipo-servizio-defaults")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDefaultDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoServizioDefault in the database
        List<TipoServizioDefault> tipoServizioDefaultList = tipoServizioDefaultRepository.findAll();
        assertThat(tipoServizioDefaultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoServizioDefault() throws Exception {
        // Initialize the database
        tipoServizioDefaultRepository.saveAndFlush(tipoServizioDefault);
        int databaseSizeBeforeDelete = tipoServizioDefaultRepository.findAll().size();

        // Get the tipoServizioDefault
        restTipoServizioDefaultMockMvc.perform(delete("/api/tipo-servizio-defaults/{id}", tipoServizioDefault.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TipoServizioDefault> tipoServizioDefaultList = tipoServizioDefaultRepository.findAll();
        assertThat(tipoServizioDefaultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoServizioDefault.class);
        TipoServizioDefault tipoServizioDefault1 = new TipoServizioDefault();
        tipoServizioDefault1.setId(1L);
        TipoServizioDefault tipoServizioDefault2 = new TipoServizioDefault();
        tipoServizioDefault2.setId(tipoServizioDefault1.getId());
        assertThat(tipoServizioDefault1).isEqualTo(tipoServizioDefault2);
        tipoServizioDefault2.setId(2L);
        assertThat(tipoServizioDefault1).isNotEqualTo(tipoServizioDefault2);
        tipoServizioDefault1.setId(null);
        assertThat(tipoServizioDefault1).isNotEqualTo(tipoServizioDefault2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoServizioDefaultDTO.class);
        TipoServizioDefaultDTO tipoServizioDefaultDTO1 = new TipoServizioDefaultDTO();
        tipoServizioDefaultDTO1.setId(1L);
        TipoServizioDefaultDTO tipoServizioDefaultDTO2 = new TipoServizioDefaultDTO();
        assertThat(tipoServizioDefaultDTO1).isNotEqualTo(tipoServizioDefaultDTO2);
        tipoServizioDefaultDTO2.setId(tipoServizioDefaultDTO1.getId());
        assertThat(tipoServizioDefaultDTO1).isEqualTo(tipoServizioDefaultDTO2);
        tipoServizioDefaultDTO2.setId(2L);
        assertThat(tipoServizioDefaultDTO1).isNotEqualTo(tipoServizioDefaultDTO2);
        tipoServizioDefaultDTO1.setId(null);
        assertThat(tipoServizioDefaultDTO1).isNotEqualTo(tipoServizioDefaultDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tipoServizioDefaultMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tipoServizioDefaultMapper.fromId(null)).isNull();
    }
}
