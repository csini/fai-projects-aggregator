package it.fai.ms.efservice.service.fsm;

import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FsmTestUtils {
  private FsmTestUtils(){}


  public static Transition<StatoRichiesta, RichiestaEvent> newMockTransition(StatoRichiesta from, RichiestaEvent event, StatoRichiesta to) {
    Transition<StatoRichiesta, RichiestaEvent> mockTransition = mock(Transition.class);
    State<StatoRichiesta, RichiestaEvent> mockStateFrom = mock(State.class);
    State<StatoRichiesta, RichiestaEvent> mockStateTo = mock(State.class);
    Trigger<StatoRichiesta, RichiestaEvent> mockTrigger = mock(Trigger.class);

    when(mockStateFrom.getId()).thenReturn(from);
    when(mockStateTo.getId()).thenReturn(to);
    when(mockTrigger.getEvent()).thenReturn(event);
    when(mockTransition.getSource()).thenReturn(mockStateFrom);
    when(mockTransition.getTarget()).thenReturn(mockStateTo);
    if (event == null) {
      when(mockTransition.getTrigger()).thenReturn(null);
    } else {
      when(mockTransition.getTrigger()).thenReturn(mockTrigger);
    }
    return mockTransition;
  }

}
