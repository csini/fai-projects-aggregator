package it.fai.ms.efservice.service.impl;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.telepass.TelepassITResponseDTO;
import it.fai.ms.efservice.client.TelepassClient;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.AssociazioneDVServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.VehicleWizardCacheServiceExt;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;


public class DeviceAssociationServiceTest {

  private final DispositivoServiceExt deviceService = mock(DispositivoServiceExt.class);
  private final VehicleWizardCacheServiceExt vehicleWizardCacheServiceExt = mock(VehicleWizardCacheServiceExt.class);
  private final AssociazioneDVServiceExt associazioneService = mock(AssociazioneDVServiceExt.class);

  private final String authorizationToken = "fake_token";
  private final TelepassClient telepassClient = mock(TelepassClient.class);

  private DeviceAssociationService deviceAssociationService;

  @Before
  public void setUp() throws Exception {
    this.deviceAssociationService =
      new DeviceAssociationService(
        deviceService,
        vehicleWizardCacheServiceExt,
        associazioneService,
        telepassClient,
        authorizationToken);

    given(deviceService.findOneByIdentificativo(any()))
      .willReturn(new Dispositivo().contratto(new Contratto().codContrattoCliente("::codContrattoCliente::")));

    given(deviceService.save(any()))
      .will(invocationOnMock -> invocationOnMock.getArgument(0));

    given(associazioneService.save(any()))
      .will(invocationOnMock -> invocationOnMock.getArgument(0));
  }

  @Test
  public void given_any_device_BUT_telepass_italiano_device_should_change_targa() throws Exception {

    List<Long> associazioneDvIds = deviceAssociationService.generateAssociationDeviceVehicle(
      TipoDispositivoEnum.CONTRATTI_FERALPINA, "::idDevice::", "::idVeicolo::");

    then(telepassClient)
      .shouldHaveZeroInteractions();

    then(deviceService)
      .should()
      .save(any());

    then(associazioneService)
      .should()
      .save(any());

  }


  @Test
  public void given_telepass_italiano_device_when_WS_response_OK_should_change_targa() throws Exception {

    given(telepassClient.devicePlateInsert(any(), any()))
      .willReturn(new TelepassITResponseDTO(TelepassITResponseDTO.OUTCOME.OK, "OOKK"));

    List<Long> associazioneDvIds = deviceAssociationService.generateAssociationDeviceVehicle(
      TipoDispositivoEnum.TELEPASS_ITALIANO, "::idDevice::", "::idVeicolo::");

    then(telepassClient)
      .should()
      .devicePlateInsert(any(), any());

    then(deviceService)
      .should()
      .save(any());

    then(associazioneService)
      .should()
      .save(any());
  }

  @Test(expected = RuntimeException.class)
  public void given_telepass_italiano_device_when_WS_response_ERROR_should_NOT_change_targa() throws Exception {
    given(telepassClient.devicePlateInsert(any(), any()))
      .willReturn(new TelepassITResponseDTO(TelepassITResponseDTO.OUTCOME.ERROR, "This is an exception to test behaviour"));

    try {
      List<Long> associazioneDvIds = deviceAssociationService.generateAssociationDeviceVehicle(
        TipoDispositivoEnum.TELEPASS_ITALIANO, "::idDevice::", "::idVeicolo::");

      then(telepassClient)
        .should()
        .devicePlateInsert(any(), any());

      then(deviceService)
        .shouldHaveZeroInteractions();

      then(associazioneService)
        .shouldHaveZeroInteractions();
    } catch (Exception e){
      throw e;
    }
  }

  @Test(expected = RuntimeException.class)
  public void given_telepass_italiano_device_when_WS_gives_exception_should_NOT_change_targa() throws Exception {
    given(telepassClient.devicePlateInsert(any(), any()))
      .willThrow(new RuntimeException("This is an exception to test behaviour"));

    try {
      List<Long> associazioneDvIds = deviceAssociationService.generateAssociationDeviceVehicle(
        TipoDispositivoEnum.TELEPASS_ITALIANO, "::idDevice::", "::idVeicolo::");

      then(telepassClient)
        .should()
        .devicePlateInsert(any(), any());

      then(deviceService)
        .shouldHaveZeroInteractions();

      then(associazioneService)
        .shouldHaveZeroInteractions();
    } catch (Exception e){
      throw e;
    }

  }

}
