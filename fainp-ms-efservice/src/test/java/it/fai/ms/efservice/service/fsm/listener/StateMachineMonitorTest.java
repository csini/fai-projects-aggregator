/**
 * 
 */
package it.fai.ms.efservice.service.fsm.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.monitor.AbstractStateMachineMonitor;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;

/**
 * @author Luca Vassallo
 */
public class StateMachineMonitorTest extends AbstractStateMachineMonitor<StateTest, EventTest> {

  private final static Logger log = LoggerFactory.getLogger(StateMachineMonitor.class);

  /*
   * (non-Javadoc)
   * @see
   * org.springframework.statemachine.monitor.AbstractStateMachineMonitor#transition(org.springframework.statemachine.
   * StateMachine, org.springframework.statemachine.transition.Transition, long)
   */
  @Override
  public void transition(StateMachine<StateTest, EventTest> stateMachine, Transition<StateTest, EventTest> transition, long duration) {
    super.transition(stateMachine, transition, duration);
    if (log.isDebugEnabled()) {
      log.debug("Transition: " + transition + " duration: " + duration);
    }
  }

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.monitor.AbstractStateMachineMonitor#action(org.springframework.statemachine.
   * StateMachine, org.springframework.statemachine.action.Action, long)
   */
  @Override
  public void action(StateMachine<StateTest, EventTest> stateMachine, Action<StateTest, EventTest> action, long duration) {
    super.action(stateMachine, action, duration);
    if (log.isDebugEnabled()) {
      log.debug("Action: " + action + " duration: " + duration);
    }
  }

}
