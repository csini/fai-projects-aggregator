package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.TipoMagazzino;
import it.fai.ms.efservice.domain.enumeration.TipologiaAssociazione;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.CarrelloModificaRichiestaDTO;
import it.fai.ms.efservice.service.dto.DeviceAssignationDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.impl.RichiestaServiceImpl;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore("Skip Test")
public class DispositivoResourceIntExtTest {

  private static final String CODICE_CLIENTE_FAI = "0046276";

  private static final String tipoDispositivoDefault = "TELEPASS_EUROPEO";

  private static final String PEDAGGIO_ITALIA     = "PEDAGGI_ITALIA";
  private static final String PEDAGGIO_POLONIA    = "PEDAGGI_POLONIA";
  private static final String PEDAGGIO_FRANCIA    = "PEDAGGI_FRANCIA";
  private static final String PEDAGGIO_PORTOGALLO = "PEDAGGI_PORTOGALLO";

  private static final String tipoServizioDefault = PEDAGGIO_ITALIA;

  private static final String veicolo1 = "AA000AA";
  private static final String veicolo2 = "AB001AA";
  private static final String veicolo3 = "AC002AA";
  private static final String veicolo4 = "AD003AA";
  private static final String veicolo5 = "AE004AA";
  private static final String veicolo6 = "AF005AA";

  private final Logger log = LoggerFactory.getLogger(DispositivoResourceIntExtTest.class);

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private DispositivoResourceExt dispositivoResource;

  @Autowired
  private RichiestaResourceExt richiestaResource;

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private RichiestaServiceImpl richiestaService;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepository;

  @Autowired
  private ContrattoRepositoryExt contrattoRepository;

  @Autowired
  private ClienteFaiRepository clienteFaiRepository;

  @Autowired
  private ProduttoreRepository produttoreRepository;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  @Autowired
  private AssociazioneDVRepository associazioneDVRepository;

  private MockMvc mockMvc;

  private MockMvc restRichiestaMockMvc;

  private CarrelloDTO carrello;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    this.mockMvc = MockMvcBuilders.standaloneSetup(dispositivoResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();

    this.restRichiestaMockMvc = MockMvcBuilders.standaloneSetup(richiestaResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();

    carrello = new CarrelloDTO();
    carrello.setCodiceAzienda(CODICE_CLIENTE_FAI);
    HashMap<String, String> datiAggiuntiviMap = new HashMap<>();
    datiAggiuntiviMap.put("costo", "1534");
    carrello.setDatiAggiuntivi(datiAggiuntiviMap);
    carrello.setTipoServizio(tipoServizioDefault);
    carrello.setUuidVeicoli(createUuidVeicoli(new ArrayList<String>(Arrays.asList(veicolo1, veicolo3, veicolo5))));
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(veicolo1, new ArrayList<String>(Arrays.asList(PEDAGGIO_FRANCIA, PEDAGGIO_POLONIA)));
    mappaVeicoli.put(veicolo3, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA, PEDAGGIO_PORTOGALLO)));
    mappaVeicoli.put(veicolo6, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrello.setDispositivi(createDispositivi(tipoDispositivoDefault, mappaDispositiviVeicoli));
  }

//  @Test
//  public void deviceAssignation() throws Exception {
//    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(DispositivoResourceExt.API_DEVICE_ASSIGNATION);
//
//    DeviceAssignationDTO dto = new DeviceAssignationDTO()
//        .codiceCliente("1234")
//        .contratto("098765")
//        .quantita(1L)
//        .tipoDispositivo(TipoDispositivoEnum.TELEPASS_EUROPEO);
//
//    MvcResult mvcResult = mockMvc.perform(post(builder.build().toString())
//                                          .contentType(TestUtil.APPLICATION_JSON_UTF8)
//                                          .content(TestUtil.convertObjectToJsonBytes(dto)))
//        .andExpect(status().isOk())
//        .andReturn();
//  }

  @Test
  public void getDispositiviByRichiesta_failed() throws Exception {
    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_BY_ORDINE).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound());
  }

  @Test
  public void getDispositiviByRichiesta_success() throws Exception {
    String identificativo;

    MvcResult mvcResult = restRichiestaMockMvc.perform(post("/api/public/richiesta").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                       .content(TestUtil.convertObjectToJsonBytes(carrello)))
        .andExpect(status().isOk())
        .andReturn();

    Richiesta richiesta = richiestaRepositoryExt.findFirstByOrderByIdDesc();

    assertThat(richiesta).isNotNull();

    identificativo = richiesta.getIdentificativo();
    richiesta = richiestaRepositoryExt.findOneByIdentificativoWithEagerRelationships(identificativo);

    mvcResult = mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_BY_ORDINE + "/"
        + identificativo).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andReturn();

    String size = mvcResult.getResponse()
        .getHeader("X-rows");
    assertThat(richiesta.getDispositivos()
               .size()).isEqualTo(new Integer(size));

    for (Dispositivo dispositivo : richiesta.getDispositivos()) {
      assertThat(dispositivo.getSeriale()).isNotBlank();
      assertThat(dispositivo.getStato()).isEqualTo(StatoDispositivo.INIZIALE);
    }

    richiestaService.delete(richiesta.getId());
  }

  @Test
  public void getDispositiviScorta_failed() throws Exception {
    String codiceAzienda = " ";

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_SCORTA).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound())
    .andReturn();

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_SCORTA + "/").contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound())
    .andReturn();

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_SCORTA + "/" + codiceAzienda).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isPreconditionFailed())
    .andReturn();
  }

  @Test
  public void getDispositiviScorta_success() throws Exception {
    String codiceAzienda = RandomStringUtils.random(7, true, true);
    String seriale = RandomStringUtils.random(17, true, true);

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente(codiceAzienda);
    clienteFai.setCitta(RandomStringUtils.random(7, true, true));
    clienteFai.setVia(RandomStringUtils.random(7, true, true));
    clienteFai.setDmlRevisionTimestamp(Instant.now());

    clienteFai = clienteFaiRepository.save(clienteFai);

    Produttore produttore = new Produttore();
    produttore.setCodiceFornitoreNav(RandomStringUtils.random(7, true, true));
    produttore.setNome(RandomStringUtils.random(7, true, true));

    produttore = produttoreRepository.save(produttore);

    Contratto contratto = new Contratto().stato(StatoContratto.ATTIVO)
        .dataModificaStato(Instant.now());
    contratto.setClienteFai(clienteFai);
    contratto.setProduttore(produttore);
    contratto.setCodContrattoCliente(RandomStringUtils.random(7, true, true));

    contratto = contrattoRepository.save(contratto);

    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOne(1L);

    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setContratto(contratto);
    dispositivo.setSeriale(seriale);
    dispositivo.setTipoMagazzino(TipoMagazzino.SCORTA);
    dispositivo.setIdentificativo(RandomStringUtils.random(17, true, true));
    dispositivo.setTipoDispositivo(tipoDispositivo);
    dispositivo.setStato(StatoDispositivo.ATTIVO);

    dispositivo = dispositivoRepository.save(dispositivo);

    MvcResult mvcResult = mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_SCORTA + "/"
        + codiceAzienda).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[*].seriale").value(hasItem(seriale)))
        .andExpect(jsonPath("$.[*].tipoMagazzino").value(hasItem(TipoMagazzino.SCORTA.toString())))
        .andReturn();

    assertThat(mvcResult.getResponse()
               .getHeader("X-rows")).isEqualTo("1");

    dispositivoRepository.delete(dispositivo);
    contrattoRepository.delete(contratto);
    produttoreRepository.delete(produttore);
    clienteFaiRepository.delete(clienteFai);
  }

  @Test
  public void getDispositiviNotarga_failed() throws Exception {
    String codiceAzienda = " ";

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_NO_TARGA).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound())
    .andReturn();

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_NO_TARGA + "/").contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isNotFound())
    .andReturn();

    mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_NO_TARGA + "/" + codiceAzienda).contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isPreconditionFailed())
    .andReturn();
  }

  @Test
  public void getDispositiviNotarga_success() throws Exception {
    String codiceAzienda = RandomStringUtils.random(7, true, true);
    String identificativo = RandomStringUtils.random(17, true, true);
    String seriale = RandomStringUtils.random(17, true, true);

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente(codiceAzienda);
    clienteFai.setCitta(RandomStringUtils.random(7, true, true));
    clienteFai.setVia(RandomStringUtils.random(7, true, true));
    clienteFai.setDmlRevisionTimestamp(Instant.now());

    clienteFai = clienteFaiRepository.save(clienteFai);

    Produttore produttore = new Produttore();
    produttore.setCodiceFornitoreNav(RandomStringUtils.random(7, true, true));
    produttore.setNome(RandomStringUtils.random(7, true, true));

    produttore = produttoreRepository.save(produttore);

    Contratto contratto = new Contratto().stato(StatoContratto.ATTIVO)
        .dataModificaStato(Instant.now());
    contratto.setClienteFai(clienteFai);
    contratto.setProduttore(produttore);
    contratto.setCodContrattoCliente(RandomStringUtils.random(7, true, true));

    contratto = contrattoRepository.save(contratto);

    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOne(1L);

    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setContratto(contratto);
    dispositivo.setSeriale(seriale);
    dispositivo.setTipoMagazzino(TipoMagazzino.NOTARGA);
    dispositivo.setIdentificativo(identificativo);
    dispositivo.setTipoDispositivo(tipoDispositivo);
    dispositivo.setStato(StatoDispositivo.ATTIVO);

    dispositivo = dispositivoRepository.save(dispositivo);

    MvcResult mvcResult = mockMvc.perform(get(DispositivoResourceExt.API_DISPOSITIVI_NO_TARGA + "/"
        + codiceAzienda).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[*].seriale").value(hasItem(seriale)))
        .andExpect(jsonPath("$.[*].tipoMagazzino").value(hasItem(TipoMagazzino.NOTARGA.toString())))
        .andReturn();

    assertThat(mvcResult.getResponse()
               .getHeader("X-rows")).isEqualTo("1");

    dispositivoRepository.delete(dispositivo);
    contrattoRepository.delete(contratto);
    produttoreRepository.delete(produttore);
    clienteFaiRepository.delete(clienteFai);
  }

  @Test
  public void getDispositiviByVehicleUuid_test() throws Exception {
    String codiceAzienda = RandomStringUtils.random(7, true, true);
    String identificativo = RandomStringUtils.random(17, true, true);
    String seriale = RandomStringUtils.random(17, true, true);
    String uuidVeicolo = RandomStringUtils.random(17, true, true);

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente(codiceAzienda);
    clienteFai.setCitta(RandomStringUtils.random(7, true, true));
    clienteFai.setVia(RandomStringUtils.random(7, true, true));
    clienteFai.setDmlRevisionTimestamp(Instant.now());

    clienteFai = clienteFaiRepository.save(clienteFai);

    Produttore produttore = new Produttore();
    produttore.setCodiceFornitoreNav(RandomStringUtils.random(7, true, true));
    produttore.setNome(RandomStringUtils.random(7, true, true));

    produttore = produttoreRepository.save(produttore);

    Contratto contratto = new Contratto().stato(StatoContratto.ATTIVO)
        .dataModificaStato(Instant.now());
    contratto.setClienteFai(clienteFai);
    contratto.setProduttore(produttore);
    contratto.setCodContrattoCliente(RandomStringUtils.random(7, true, true));

    contratto = contrattoRepository.save(contratto);

    //    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOne(1L);
    TipoDispositivo tipoDispositivo = new TipoDispositivo();
    tipoDispositivo.setId(1L);

    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setContratto(contratto);
    dispositivo.setSeriale(seriale);
    dispositivo.setTipoMagazzino(TipoMagazzino.NOTARGA);
    dispositivo.setIdentificativo(identificativo);
    dispositivo.setTipoDispositivo(tipoDispositivo);
    dispositivo.setStato(StatoDispositivo.ATTIVO);

    dispositivo = dispositivoRepository.save(dispositivo);

    AssociazioneDV associazioneDV = new AssociazioneDV();
    associazioneDV.setDispositivo(dispositivo);
    associazioneDV.setUuidVeicolo(uuidVeicolo);

    associazioneDV = associazioneDVRepository.save(associazioneDV);

    MvcResult mvcResult = mockMvc.perform(get(DispositivoResourceExt.API_DEVICES_BY_VEHICLE_UUID + "/"
        + uuidVeicolo).contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.[*].seriale").value(hasItem(seriale)))
        .andExpect(jsonPath("$.[*].tipoMagazzino").value(hasItem(TipoMagazzino.NOTARGA.toString())))
        .andReturn();

    assertThat(mvcResult.getResponse()
               .getHeader("X-rows")).isEqualTo("1");

    associazioneDVRepository.delete(associazioneDV);
    dispositivoRepository.delete(dispositivo);
    contrattoRepository.delete(contratto);
    produttoreRepository.delete(produttore);
    clienteFaiRepository.delete(clienteFai);
  }

  private static List<DispositivoCarrelloDTO> createDispositivi(String tipoDispositivo,
                                                                HashMap<Integer, HashMap<String, List<String>>> mapDispositivi) {
    List<DispositivoCarrelloDTO> dispositivi = new ArrayList<>();

    Set<Integer> keySet = mapDispositivi.keySet();
    Iterator<Integer> iterator = keySet.iterator();

    while (iterator != null && iterator.hasNext()) {
      Integer keyDispositivo = iterator.next();
      HashMap<String, List<String>> veicoliCarrello = mapDispositivi.get(keyDispositivo);
      DispositivoCarrelloDTO dispositivoCarrello = new DispositivoCarrelloDTO();
      dispositivoCarrello.setTipoDispositivo(tipoDispositivo);
      dispositivoCarrello.setVeicoli(createVeicoliCarrello(veicoliCarrello));
      dispositivi.add(dispositivoCarrello);
    }

    return dispositivi;
  }

  private static List<VeicoloCarrelloDTO> createVeicoliCarrello(HashMap<String, List<String>> map) {
    List<VeicoloCarrelloDTO> listVeicolo = new ArrayList<>();
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while (iterator != null && iterator.hasNext()) {
      String keyVeicolo = iterator.next();
      List<String> listServizi = map.get(keyVeicolo);
      VeicoloCarrelloDTO veicolo = new VeicoloCarrelloDTO();
      veicolo.setUuid(keyVeicolo);
      veicolo.setNomeTipiServizio(createNomeServizio(listServizi));
      listVeicolo.add(veicolo);
    }
    return listVeicolo;
  }

  private static List<String> createNomeServizio(List<String> servizi) {
    List<String> nomeServizio = new ArrayList<>();

    for (String servizio : servizi) {
      nomeServizio.add(servizio);
    }
    return nomeServizio;
  }

  private static List<String> createUuidVeicoli(List<String> listVeicoli) {
    List<String> uuidVeicoli = new ArrayList<>();
    for (String veicolo : listVeicoli) {
      uuidVeicoli.add(veicolo);
    }
    return uuidVeicoli;
  }

  @Test
  public void associazioneScorta() throws Exception {

    Dispositivo dispositivo = DispositivoResourceIntTest.createEntity(null);
    dispositivo.setTipoMagazzino(TipoMagazzino.SCORTA);
    dispositivoRepository.save(dispositivo);

    CarrelloModificaRichiestaDTO carrelloModificaRichiestaDTO = new CarrelloModificaRichiestaDTO();
    carrelloModificaRichiestaDTO.setDispositivi(Collections.singletonList(dispositivo.getIdentificativo()));
    carrelloModificaRichiestaDTO.setNewVeicolo("AAANEW_VEICOLO");

    mockMvc.perform(post(DispositivoResourceExt.API_DISPOSITIVI_ASSOCIAZIONE_DV + "/"
        + TipologiaAssociazione.SCORTA).contentType(TestUtil.APPLICATION_JSON_UTF8)
                    .content(TestUtil.convertObjectToJsonBytes(carrelloModificaRichiestaDTO)))
    .andExpect(status().isOk());

    assertThat(dispositivo.getTipoMagazzino()).isNull();
    assertThat(dispositivo.getAssociazioneDispositivoVeicolos()).hasSize(1);
  }

}
