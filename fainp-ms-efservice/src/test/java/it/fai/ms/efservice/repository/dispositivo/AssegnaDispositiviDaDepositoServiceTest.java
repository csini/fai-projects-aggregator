package it.fai.ms.efservice.repository.dispositivo;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionTemplate;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.repository.TipoServizioRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)

public class AssegnaDispositiviDaDepositoServiceTest {

  private static final TransactionDefinition REQUIRES_NEW_TRANSACTION = new DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

  @Autowired
  AssegnaDispositiviDaDepositoService subject;

  @Autowired
  DispositivoRepository dispositivoRepo;

  @Autowired
  TipoDispositivoRepository tipoDispositivoRepo;

  @Autowired
  TipoServizioRepository tipoServizioRepo;

  @Autowired
  ClienteFaiRepository clienteRepo;

  @Autowired
  ProduttoreRepository produttoreRepo;

  @Autowired
  EntityManager em;

  @Autowired
  PlatformTransactionManager transactionManager;

  Contratto contratto;

  TipoDispositivo tipoDispositivo;

  StatoDispositivoServizio statoDispositivo;

  // Produttore produttore;

  Dispositivo dispositivo;

  Dispositivo dispositivoScaduto;

  @Test
  public void testAssegnaDispositiviSenzaServizio() {
    new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(status -> {
      tipoDispositivo = tipoDispositivoRepo.findOneByNome(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT);
      ClienteFai clienteFai = clienteRepo.findOne(16l);

      Produttore produttore = produttoreRepo.findOne(15l);

      dispositivo = new Dispositivo().tipoDispositivo(tipoDispositivo)
                                     .stato(StatoDispositivo.IN_DEPOSITO)
                                     .seriale("123456")
                                     .dataScadenza(ZonedDateTime.now()
                                                                .plus(1, ChronoUnit.MONTHS));
      em.persist(dispositivo);

      dispositivoScaduto = new Dispositivo().tipoDispositivo(tipoDispositivo)
                                            .stato(StatoDispositivo.IN_DEPOSITO)
                                            .seriale("000001")
                                            .dataScadenza(ZonedDateTime.now());

      em.persist(dispositivoScaduto);

      contratto = new Contratto().clienteFai(clienteFai)
                                 .produttore(produttore)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());

      em.persist(contratto);

      //
      assertThat(contratto).isNotNull();
      List<Dispositivo> assegnaDispositivi = subject.assegnaDispositiviInternal(TipoDispositivoAssegnabile.BUONI_FREJUS_MONTE_BIANCO_IT, 2,
                                                                                contratto.getCodContrattoCliente());

      assertThat(assegnaDispositivi.size()).isEqualTo(1);
      Dispositivo dispositivo = assegnaDispositivi.get(0);
      assertThat(dispositivo.getContratto()).isEqualTo(contratto);
      assertThat(dispositivo.getContratto()
                            .getClienteFai()
                            .getId()).isEqualTo(16l);

      assertThat(dispositivo.getStato()).isEqualTo(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
      assertThat(dispositivo.getSeriale()).isEqualTo("123456");
      assertThat(dispositivo.getDataScadenza()
                            .isAfter(ZonedDateTime.now()));

      assertThat(dispositivo.getStatoDispositivoServizios()).isNotEmpty()
                                                            .anySatisfy(sds -> {
                                                              assertThat(sds.getStato()).isEqualTo(StatoDS.ATTIVO);
                                                            });
      statoDispositivo = dispositivo.getStatoDispositivoServizios()
                                    .iterator()
                                    .next();
      return null;
    });
  }

  @Test
  public void testAssegnaDispositiviConServizio() {
     new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(status -> {
    tipoDispositivo = tipoDispositivoRepo.findOneByNome(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR);
    TipoServizio tipoServizio = tipoServizioRepo.findOne(1l);

    assertThat(tipoDispositivo).isNotNull();
    ClienteFai clienteFai = clienteRepo.findOne(16l);

    Produttore produttore = produttoreRepo.findOne(15l);

    dispositivo = new Dispositivo().tipoDispositivo(tipoDispositivo)
                                   .stato(StatoDispositivo.IN_DEPOSITO)
                                   .seriale("00002")
                                   .dataScadenza(ZonedDateTime.now()
                                                              .plus(1, ChronoUnit.MONTHS));

    Set<StatoDispositivoServizio> set = new HashSet<>(1);
    statoDispositivo = new StatoDispositivoServizio();
    statoDispositivo.setStato(StatoDS.NON_ATTIVO);
    statoDispositivo.setDispositivo(dispositivo);
    statoDispositivo.setTipoServizio(tipoServizio);

    set.add(statoDispositivo);
    dispositivo.setStatoDispositivoServizios(set);
    em.persist(statoDispositivo);
    em.persist(dispositivo);

    contratto = new Contratto().clienteFai(clienteFai)
                               .produttore(produttore)
                               .stato(StatoContratto.ATTIVO)
                               .dataModificaStato(Instant.now());

    em.persist(contratto);


    assertThat(contratto).isNotNull();
    List<Dispositivo> assegnaDispositivi = subject.assegnaDispositiviInternal(TipoDispositivoAssegnabile.BUONI_FREJUS_MONTE_BIANCO_FR, 2,
                                                                              contratto.getCodContrattoCliente());

    assertThat(assegnaDispositivi.size()).isEqualTo(1);
    Dispositivo dispositivo = assegnaDispositivi.get(0);
    assertThat(dispositivo.getContratto()).isEqualTo(contratto);
    assertThat(dispositivo.getContratto()
                          .getClienteFai()
                          .getId()).isEqualTo(16l);

    assertThat(dispositivo.getStato()).isEqualTo(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
    assertThat(dispositivo.getSeriale()).isEqualTo("00002");
    assertThat(dispositivo.getDataScadenza()
                          .isAfter(ZonedDateTime.now()));

    assertThat(dispositivo.getStatoDispositivoServizios()
                          .size()).isEqualTo(1);
    StatoDispositivoServizio statoServizio = dispositivo.getStatoDispositivoServizios()
                                                        .iterator()
                                                        .next();
    assertThat(statoServizio.getId()).isEqualTo(statoDispositivo.getId());
     return null;
     });
  }

  @After
  public void tearDown() {
    new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(status -> {
      delete(contratto);
      em.remove(em.find(statoDispositivo.getClass(), statoDispositivo.getId()));
      em.remove(em.find(dispositivo.getClass(), dispositivo.getId()));
      if (dispositivoScaduto != null)
        em.remove(em.find(dispositivoScaduto.getClass(), dispositivoScaduto.getId()));
      return null;
    });
  }

  private void delete(Contratto contratto) {
    if (contratto != null)
      em.remove(em.find(contratto.getClass(), contratto.getId()));
  }

}
