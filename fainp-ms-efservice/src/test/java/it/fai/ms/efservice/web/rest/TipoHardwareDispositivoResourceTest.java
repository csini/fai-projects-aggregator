package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoHardwareDispositivo;
import it.fai.ms.efservice.repository.TipoHardwareDispositivoRepository;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.TipoHardwareDispositivoService;
import it.fai.ms.efservice.service.dto.TipoHardwareDispositivoDTO;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the TipoDispositivoResource REST controller.
 *
 * @see TipoDispositivoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoHardwareDispositivoResourceTest {

  private static final String DEFAULT_HARDWARE_TYPE = "AS";

  private static final String HARDWARE_TYPE_1 = "AT";

  private static final String HARDWARE_TYPE_2 = "AA";

  private static final TipoDispositivoEnum DEFAULT_DEVICE = TipoDispositivoEnum.TELEPASS_ITALIANO;

  @Autowired
  private TipoHardwareDispositivoRepository tipoHardwareDispositivoRepo;

  @Autowired
  private TipoHardwareDispositivoService tipoHardwareDispositivoService;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restTipoHardwareDispositivoMockMvc;

  private static TipoDispositivo tipoDispositivo;

  private TipoHardwareDispositivo hardwareType;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final TipoHardwareDispositivoResource tipoHardwareDispositivoResource = new TipoHardwareDispositivoResource(tipoHardwareDispositivoService,
                                                                                                                tipoDispositivoService);
    this.restTipoHardwareDispositivoMockMvc = MockMvcBuilders.standaloneSetup(tipoHardwareDispositivoResource)
                                                             .setCustomArgumentResolvers(pageableArgumentResolver)
                                                             .setControllerAdvice(exceptionTranslator)
                                                             .setMessageConverters(jacksonMessageConverter)
                                                             .build();

    tipoDispositivo = tipoDispositivoService.findOneByNome(DEFAULT_DEVICE);
  }

  public static TipoHardwareDispositivo createEntity(EntityManager em, String hw, boolean defaultValue, boolean active) {
    TipoHardwareDispositivo tipoHardwareDispositivo = new TipoHardwareDispositivo().tipoDispositivo(tipoDispositivo)
                                                                                   .hardwareType(hw)
                                                                                   .descrizione("HW-" + System.currentTimeMillis())
                                                                                   .defaultHardwareType(defaultValue)
                                                                                   .attivo(active);
    return tipoHardwareDispositivo;
  }

  @Before
  public void initTest() {
    tipoHardwareDispositivoService.deleteAll();
    // hardwareType = createEntity(em, "description", true, true);
  }

  @Test
  @Transactional
  public void createTipoDispositivo() throws Exception {
    int databaseSizeBeforeCreate = tipoHardwareDispositivoRepo.findAll()
                                                              .size();

    String descrizione = "HW - Type";
    TipoHardwareDispositivoDTO hardwareTypeDTO = new TipoHardwareDispositivoDTO();
    hardwareTypeDTO.setDefaultTipoHardware(false);
    hardwareTypeDTO.setTipoDispositivo(DEFAULT_DEVICE);
    hardwareTypeDTO.setTipoHardware(DEFAULT_HARDWARE_TYPE);
    hardwareTypeDTO.setDescrizione(descrizione);
    hardwareTypeDTO.setAttivo(true);
    restTipoHardwareDispositivoMockMvc.perform(post("/api/public/tipo-hardware-dispositivo").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                            .content(TestUtil.convertObjectToJsonBytes(hardwareTypeDTO)))
                                      .andExpect(status().isCreated());

    List<TipoHardwareDispositivo> tipoHardwareDispositivoList = tipoHardwareDispositivoRepo.findAll();
    assertThat(tipoHardwareDispositivoList).hasSize(databaseSizeBeforeCreate + 1);
    TipoHardwareDispositivo testTipoDispositivo = tipoHardwareDispositivoList.get(tipoHardwareDispositivoList.size() - 1);
    assertThat(testTipoDispositivo.getTipoDispositivo()
                                  .equals(tipoDispositivo));
    assertThat(testTipoDispositivo.getHardwareType()
                                  .equals(DEFAULT_HARDWARE_TYPE));
    assertThat(testTipoDispositivo.getDescrizione()
                                  .equals(descrizione));
    assertThat(testTipoDispositivo.isAttivo()).isTrue();
    assertThat(testTipoDispositivo.isDefaultHardwareType()).isFalse();
  }

  @Test
  @Transactional
  public void findByTipoDispositivo() throws Exception {
    tipoHardwareDispositivoRepo.save(createEntity(em, DEFAULT_HARDWARE_TYPE, true, true));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_1, true, false));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_2, false, true));

    MvcResult result = restTipoHardwareDispositivoMockMvc.perform(get("/api/public/tipo-hardware-dispositivo/tipo-dispositivo/"
                                                                      + DEFAULT_DEVICE).contentType(TestUtil.APPLICATION_JSON_UTF8))
                                                         .andExpect(status().isOk())
                                                         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                                         .andReturn();

    String response = result.getResponse()
                            .getContentAsString();
    System.out.println(response);
    int count = StringUtils.countMatches(response, "hardwareType");
    assertThat(count).isEqualTo(2);
  }

  @Test
  @Transactional
  public void findByTipoDispositivoId() throws Exception {
    tipoHardwareDispositivoRepo.save(createEntity(em, DEFAULT_HARDWARE_TYPE, true, true));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_1, true, false));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_2, false, true));
    
    Long idTipoDispositivo = tipoDispositivo.getId();
    MvcResult result = restTipoHardwareDispositivoMockMvc.perform(get("/api/public/tipo-hardware-dispositivo/tipo-dispositivo-id/"
                                                                      + idTipoDispositivo).contentType(TestUtil.APPLICATION_JSON_UTF8))
                                                         .andExpect(status().isOk())
                                                         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                                         .andReturn();

    String response = result.getResponse()
                            .getContentAsString();
    System.out.println(response);
    int count = StringUtils.countMatches(response, "hardwareType");
    assertThat(count).isEqualTo(2);
  }

  @Test
  @Transactional
  public void findByTipoDispositivoAndHardware() throws Exception {

    tipoHardwareDispositivoRepo.save(createEntity(em, DEFAULT_HARDWARE_TYPE, true, true));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_1, true, false));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_2, false, true));

    MvcResult result = restTipoHardwareDispositivoMockMvc.perform(get("/api/public/tipo-hardware-dispositivo/tipo-dispositivo/"
                                                                      + DEFAULT_DEVICE + "/hw/" + DEFAULT_HARDWARE_TYPE)
                                                                                                                        .contentType(TestUtil.APPLICATION_JSON_UTF8))
                                                         .andExpect(status().isOk())
                                                         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                                         .andReturn();

    String response = result.getResponse()
                            .getContentAsString();
    System.out.println(response);
    int count = StringUtils.countMatches(response, "hardwareType");
    assertThat(count).isEqualTo(2);
  }

  @Test
  @Transactional
  public void findByTipoDispositivoIdAndHardware() throws Exception {

    tipoHardwareDispositivoRepo.save(createEntity(em, DEFAULT_HARDWARE_TYPE, true, true));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_1, true, false));
    tipoHardwareDispositivoRepo.save(createEntity(em, HARDWARE_TYPE_2, false, true));

    Long idTipoDispositivo = tipoDispositivo.getId();
    MvcResult result = restTipoHardwareDispositivoMockMvc.perform(get("/api/public/tipo-hardware-dispositivo/tipo-dispositivo-id/"
                                                                      + idTipoDispositivo + "/hw/" + "HARDWARE_TYPE_NEW")
                                                                                                                         .contentType(TestUtil.APPLICATION_JSON_UTF8))
                                                         .andExpect(status().isOk())
                                                         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                                         .andReturn();

    String response = result.getResponse()
                            .getContentAsString();
    System.out.println(response);
    int count = StringUtils.countMatches(response, "hardwareType");
    assertThat(count).isEqualTo(3);
  }

}
