package it.fai.ms.efservice.service.fsm.type.viatoll;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaViaTollFurtoSmarrimentoTest {
  private final static String CODICE_AZIENDA = "0073935";

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtils;

  @Autowired
  private RichiestaRepositoryExt richiestaRepo;

  @Autowired
  private FsmRichiestaCacheService cache;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepo;

  @Autowired
  private ContrattoServiceExt contrattoService;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  private Richiesta richiesta;

  private Dispositivo dispositivo;

  private TipoDispositivo tipoDispositivo;

  private Contratto contract;

  private boolean isNewContract = false;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache(String.valueOf(977871L));

    tipoDispositivo = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.VIA_TOLL);
    richiesta = newRichiesta();
    richiesta.setTipoDispositivo(tipoDispositivo);
  }

  @After
  public void afterTest() {
    if (isNewContract) {
      contrattoService.delete(contract);
    }

    if (dispositivo != null) {
      Dispositivo deviceFound = dispositivoRepo.findOne(dispositivo.getId());
      if (deviceFound != null) {
        dispositivoRepo.delete(deviceFound);
      }
    }
    if (richiesta != null) {
      richiestaRepo.delete(richiesta);
    }
  }

  @Test
  @Transactional
  public void FsmFurtoViaTollectTest() throws Exception {
    richiesta.setTipo(TipoRichiesta.FURTO);
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL);
    AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
    String idMachine = fsm.getStateMachine()
                          .getId();

    assertThat(idMachine).isEqualTo(FsmType.MOD_VIATOLL_FURTO_SMARRIMENTO.fsmName());

    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA);
  }

  @Test
  @Transactional
  public void FsmSmarrimentoViaTollTest() throws Exception {
    richiesta.setTipo(TipoRichiesta.SMARRIMENTO);
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_VIATOLL);
    AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
    String idMachine = fsm.getStateMachine()
                          .getId();

    assertThat(idMachine).isEqualTo(FsmType.MOD_VIATOLL_FURTO_SMARRIMENTO.fsmName());

    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA);
  }

  // @Test
  // @Transactional
  // public void FsmMalfSostituzione() throws Exception {
  // richiesta.setTipo(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
  // richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_VC);
  // AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
  // String idMachine = fsm.getStateMachine()
  // .getId();
  //
  // assertThat(idMachine).isEqualTo(FsmType.MOD_VC_VARIAZIONE_TARGA.fsmName());
  //
  // richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  // StatoRichiesta stato = richiesta.getStato();
  // assertThat(stato).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  // }

  // @Test
  // @Transactional
  // public void FsmMalfunzionamento() throws Exception {
  // richiesta.setTipo(TipoRichiesta.MALFUNZIONAMENTO);
  // richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_VC);
  // AbstractFsmRichiesta fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
  // String idMachine = fsm.getStateMachine()
  // .getId();
  //
  // assertThat(idMachine).isEqualTo(FsmType.MOD_VC_RIENTRO_MALFUNZIONAMENTO.fsmName());
  //
  // richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  // StatoRichiesta stato = richiesta.getStato();
  // assertThat(stato).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  // }

  // ****** Utils method ************
  private Richiesta newRichiesta() {
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .data(Instant.now())
                                         .contratto(newContratto())
                                         .ordineCliente(newOrdineCliente());
    richiesta.setId(977871L);
    Dispositivo newDispositivo = newDispositivo();
    richiesta.addDispositivo(newDispositivo);
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().clienteAssegnatario(newClienteFai());
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }

  private Contratto newContratto() {
    Produttore produttore = tipoDispositivo.getProduttore();
    Optional<Contratto> optContratto = contrattoService.findContrattoByProduttoreAndCodiceAzienda(produttore, CODICE_AZIENDA);

    if (!optContratto.isPresent()) {
      contract = contrattoService.createContrattoByProduttoreAndClienteFai(produttore, newClienteFai());
      isNewContract = true;
    } else {
      contract = optContratto.get();
    }

    return contract;
  }

  private Dispositivo newDispositivo() {
    Dispositivo disp = new Dispositivo().stato(StatoDispositivo.INIZIALE)
                                        .dataModificaStato(Instant.now())
                                        .tipoDispositivo(tipoDispositivo)
                                        .seriale("ABCD-1234");
    return disp;
  }

}
