package it.fai.ms.efservice.service.fsm.type.fai;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.GeneratorContractCodeService;
import it.fai.ms.efservice.service.ManageDeviceSentModeService;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.fai.FsmInoltroTrackyCardConfig;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroTrackyCardTest {

  @Configuration
  public static class TrackyCardInoltroConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    private static GeneratorContractCodeService generatorContractCode = mock(GeneratorContractCodeService.class);

    @Autowired
    private FsmSenderToQueue fsmSender;

    @Autowired
    private ManageDeviceSentModeService manageDeviceSentMode;

    @Autowired
    private DeviceProducerService deviceProducerService;

    @Autowired
    private JmsProperties jmsProperties;

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroTrackyCardConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmInoltroTrackyCardConfig newTrackyCardInoltroConfiguration() {
      FsmInoltroTrackyCardConfig fsmInoltroTrackyCard = new FsmInoltroTrackyCardConfig(generatorContractCode, fsmSender,
                                                                                       deviceProducerService, manageDeviceSentMode,
                                                                                       jmsProperties);
      _log.info("Created FsmInoltroTrackyCardConfig for test {}", fsmInoltroTrackyCard);
      return fsmInoltroTrackyCard;
    }
  }

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta richiesta = null;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
  }

  @Test
  public void contractCodeGenerated() throws Exception {
    ClienteFai newClienteFai = newClienteFai();
    when(TrackyCardInoltroConfiguration.generatorContractCode.generateAndSaveContractCode(TipoDispositivoEnum.TRACKYCARD,
                                                                                          richiesta.getContratto())).thenReturn(newContratto(newClienteFai).codContrattoCliente("GENERATED"));
    AbstractFsmRichiesta fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TRACKY);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta, null);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.INOLTRATO);
  }

  @Test
  @Transactional
  public void contractCodeNotGenerated() throws Exception {
    when(TrackyCardInoltroConfiguration.generatorContractCode.generateAndSaveContractCode(TipoDispositivoEnum.TRACKYCARD,
                                                                                          richiesta.getContratto())).thenReturn(richiesta.getContratto()
                                                                                                                                         .codContrattoCliente(null));
    AbstractFsmRichiesta fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TRACKY);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta, null);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
  }

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = newClienteFai();
    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TRACKYCARD))
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::")
                                         .dispositivos(newDispositivos(TipoDispositivoEnum.TRACKYCARD, newContratto(newClienteFai)));
    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  private Set<Dispositivo> newDispositivos(TipoDispositivoEnum trackycard, Contratto contratto) {
    Set<Dispositivo> dispositivos = new HashSet<>();
    Dispositivo device = new Dispositivo();
    device.setTipoDispositivo(new TipoDispositivo().nome(trackycard));
    device.setSeriale("SN_" + System.currentTimeMillis());
    device.setContratto(contratto);
    dispositivos.add(device);
    return dispositivos;
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private ClienteFai newClienteFai() {
    return new ClienteFai().codiceCliente("codcli");
  }

}
