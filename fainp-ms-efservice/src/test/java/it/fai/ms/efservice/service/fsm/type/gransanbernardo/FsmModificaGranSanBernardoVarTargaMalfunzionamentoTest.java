package it.fai.ms.efservice.service.fsm.type.gransanbernardo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.gransanbernardo.FsmInoltroGranSanBernardoConfig;
import it.fai.ms.efservice.service.fsm.config.gransanbernardo.FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaGranSanBernardoVarTargaMalfunzionamentoTest {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Configuration
  public static class GranSanBernardoModificaVarTargaMalfunzionamentoConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    private static FsmSenderToQueue senderFsmService = mock(FsmSenderToQueue.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroGranSanBernardoConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig newGranSanBernardoModificaVarTargaMalfunzionamentoConfiguration() {
      FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig fsmModificaGranSanBernardoVarTargaMalfunzionamento = new FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig(senderFsmService);
      _log.info("Created FsmModificaGranSanBernardoVarTargaMalfunzionamentoConfig for test {}",
                fsmModificaGranSanBernardoVarTargaMalfunzionamento);
      return fsmModificaGranSanBernardoVarTargaMalfunzionamento;
    }
  }

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiestaVariazioneTarga                 = null;
  private Richiesta   richiestaMezzoRitargato                  = null;
  private Richiesta   richiestaMalfunzionamentoConSostituzione = null;
  private Richiesta   richiestaMalfunzionamento                = null;
  private Dispositivo dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    richiestaVariazioneTarga = newMockRichiesta(TipoRichiesta.VARIAZIONE_TARGA);
    richiestaMezzoRitargato = newMockRichiesta(TipoRichiesta.MEZZO_RITARGATO);
    richiestaMalfunzionamentoConSostituzione = newMockRichiesta(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    richiestaMalfunzionamento = newMockRichiesta(TipoRichiesta.MALFUNZIONAMENTO);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private Dispositivo newDispositivo() {
    Dispositivo d = new Dispositivo();

    d.setStato(StatoDispositivo.IN_DEPOSITO);
    d.setTipoDispositivo(newTipoDispositivo());
    d.setId(1L);

    return d;
  }

  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo d = new TipoDispositivo();

    d.setId(10000001L);
    d.setModalitaSpedizione(ModalitaSpedizione.DA_DEPOSITO_FAI);
    d.setNome(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);

    return d;
  }

  private Richiesta newMockRichiesta(TipoRichiesta tipoRichiesta) {
    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = newDispositivo();

    Richiesta richiesta = new Richiesta().tipo(tipoRichiesta)
                                         .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)
                                         .tipoDispositivo(newTipoDispositivo())
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmInoltroGranSanBernardoVariazioneTarga_attivoPerModifica_inAttesaDispositivo() throws Exception {
    if (richiestaVariazioneTarga.getStato()
                                .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaVariazioneTarga);
      assertThat(richiestaVariazioneTarga.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
  }

  @Test
  public void FsmInoltroGranSanBernardoMezzoRitargato_attivoPerModifica_inAttesaDispositivo() throws Exception {
    if (richiestaMezzoRitargato.getStato()
                               .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMezzoRitargato);
      assertThat(richiestaMezzoRitargato.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
  }

  @Test
  public void FsmInoltroGranSanBernardoMalfunzionamentoConSostituzione_attivoPerModifica_inAttesaDispositivo() throws Exception {
    if (richiestaMalfunzionamentoConSostituzione.getStato()
                                                .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMalfunzionamentoConSostituzione);
      assertThat(richiestaMalfunzionamentoConSostituzione.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
  }

  @Test
  public void FsmInoltroGranSanBernardoMalfunzionamento_attivoPerModifica_inAttesaDispositivo() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
  }

  @Test
  public void FsmInoltroGranSanBernardoVariazioneTarga_attivoPerModifica_evasoConRiconsegna() throws Exception {
    if (richiestaVariazioneTarga.getStato()
                                .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaVariazioneTarga);
      assertThat(richiestaVariazioneTarga.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }

    FsmModificaGranSanBernardoVarTargaMalfunzionamento fsmModificaGranSanBernardoVarTargaMalfunzionamento = getFsm(richiestaVariazioneTarga);
    richiestaVariazioneTarga = fsmModificaGranSanBernardoVarTargaMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                              richiestaVariazioneTarga);
    assertThat(richiestaVariazioneTarga.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  @Test
  public void FsmInoltroGranSanBernardoMezzoRitargato_attivoPerModifica_evasoConRiconsegna() throws Exception {
    if (richiestaMezzoRitargato.getStato()
                               .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMezzoRitargato);
      assertThat(richiestaMezzoRitargato.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
    FsmModificaGranSanBernardoVarTargaMalfunzionamento fsmModificaGranSanBernardoVarTargaMalfunzionamento = getFsm(richiestaMezzoRitargato);
    richiestaMezzoRitargato = fsmModificaGranSanBernardoVarTargaMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                             richiestaMezzoRitargato);
    assertThat(richiestaMezzoRitargato.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  @Test
  public void FsmInoltroGranSanBernardoMalfunzionamentoConSostituzione_attivoPerModifica_evasoConRiconsegna() throws Exception {
    if (richiestaMalfunzionamentoConSostituzione.getStato()
                                                .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMalfunzionamentoConSostituzione);
      assertThat(richiestaMalfunzionamentoConSostituzione.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
    FsmModificaGranSanBernardoVarTargaMalfunzionamento fsmModificaGranSanBernardoVarTargaMalfunzionamento = getFsm(richiestaMalfunzionamentoConSostituzione);
    richiestaMalfunzionamentoConSostituzione = fsmModificaGranSanBernardoVarTargaMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                                              richiestaMalfunzionamentoConSostituzione);
    assertThat(richiestaMalfunzionamentoConSostituzione.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  @Test
  public void FsmInoltroGranSanBernardoMalfunzionamento_attivoPerModifica_evasoConRiconsegna() throws Exception {
    if (richiestaMalfunzionamento.getStato()
                                 .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_GRAN_SANBERNARDO)) {
      sendInitialCommandToFSM(richiestaMalfunzionamento);
      assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISPOSITIVO);
    }
    FsmModificaGranSanBernardoVarTargaMalfunzionamento fsmModificaGranSanBernardoVarTargaMalfunzionamento = getFsm(richiestaMalfunzionamento);
    richiestaMalfunzionamento = fsmModificaGranSanBernardoVarTargaMalfunzionamento.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO,
                                                                                                               richiestaMalfunzionamento);
    assertThat(richiestaMalfunzionamento.getStato()).isEqualTo(StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  private void sendInitialCommandToFSM(Richiesta richiesta) throws Exception {
    Mockito.doNothing()
           .when(GranSanBernardoModificaVarTargaMalfunzionamentoConfiguration.senderFsmService)
           .sendMessageForChangeStatoDispositivo(any(Dispositivo.class), any(DispositivoEvent.class));

    FsmModificaGranSanBernardoVarTargaMalfunzionamento fsmModificaGranSanBernardoVarTargaMalfunzionamento = getFsm(richiesta);

    richiesta = fsmModificaGranSanBernardoVarTargaMalfunzionamento.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  }

  private FsmModificaGranSanBernardoVarTargaMalfunzionamento getFsm(Richiesta richiesta) {
    FsmCommand fsmCommand = null;
    switch (richiesta.getTipo()) {
    case VARIAZIONE_TARGA:
      fsmCommand = FsmCommand.CMD_VARIAZIONE_TARGA_GRAN_SANBERNARDO;
      break;
    case MEZZO_RITARGATO:
      fsmCommand = FsmCommand.CMD_MEZZO_RITARGATO_GRAN_SANBERNARDO;
      break;
    case MALFUNZIONAMENTO_CON_SOSTITUZIONE:
      fsmCommand = FsmCommand.CMD_MALFUNZIONAMENTO_SOST_GRAN_SANBERNARDO;
      break;
    case MALFUNZIONAMENTO:
      fsmCommand = FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_GRAN_SANBERNARDO;
      break;
    default:
      log.error("Tipologia Richiesta [" + richiesta.getTipo()
                                                   .name()
                + "] is not managed");
      break;
    }

    return (FsmModificaGranSanBernardoVarTargaMalfunzionamento) fsmFactory.getFsm(fsmCommand);
  }

}
