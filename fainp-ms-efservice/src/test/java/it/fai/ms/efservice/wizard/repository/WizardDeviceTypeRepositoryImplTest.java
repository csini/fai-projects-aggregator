package it.fai.ms.efservice.wizard.repository;

import static it.fai.common.enumeration.TipoDispositivoEnum.LIBER_T;
import static it.fai.common.enumeration.TipoDispositivoEnum.TELEPASS_EUROPEO;
import static it.fai.common.enumeration.TipoDispositivoEnum.TELEPASS_ITALIANO;
import static it.fai.common.enumeration.TipoDispositivoEnum.VIACARD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;

@RunWith(MockitoJUnitRunner.class)
public class WizardDeviceTypeRepositoryImplTest {

  @Mock
  private TipoDispositivoRepository deviceTypeRepository;

  private WizardDeviceTypeRepositoryImpl wizardDeviceTypeRepository;

  @Before
  public void setUp() throws Exception {
    wizardDeviceTypeRepository = new WizardDeviceTypeRepositoryImpl(deviceTypeRepository);
  }

  @Test
  public void testFindAll() {

    given(deviceTypeRepository.findAll()).willReturn(newTipoDispositivoList());

    Set<WizardDeviceType> allWizardDeviceTypes = wizardDeviceTypeRepository.findAll();

    then(deviceTypeRepository).should()
                              .findAll();
    then(deviceTypeRepository).shouldHaveNoMoreInteractions();

    WizardDeviceType wizardDeviceType1 = newWizardDeviceType("TELEPASS_EUROPEO");
    wizardDeviceType1.setOrder(1);
    WizardDeviceType wizardDeviceType2 = newWizardDeviceType("TELEPASS_ITALIANO");
    wizardDeviceType2.setOrder(6);
    WizardDeviceType wizardDeviceType4 = newWizardDeviceType("VIACARD");
    wizardDeviceType4.setOrder(6);
    WizardDeviceType wizardDeviceType5 = newWizardDeviceType("LIBER_T");
    wizardDeviceType5.setOrder(6);

    assertThat(allWizardDeviceTypes).containsExactly(wizardDeviceType1, wizardDeviceType5, wizardDeviceType2,
                                                     wizardDeviceType4);
  }

  @Test
  public void testFindAllIds() {

    given(deviceTypeRepository.findNotHiddenDeviceType()).willReturn(newTipoDispositivoList());

    Set<WizardDeviceTypeId> allWizardDeviceTypeIds = wizardDeviceTypeRepository.findAllIds();

    verify(deviceTypeRepository).findNotHiddenDeviceType();
    verifyNoMoreInteractions(deviceTypeRepository);
    
    WizardDeviceTypeId wizardDeviceTypeId1 = new WizardDeviceTypeId("TELEPASS_EUROPEO");
    WizardDeviceTypeId wizardDeviceTypeId2 = new WizardDeviceTypeId("TELEPASS_ITALIANO");
    WizardDeviceTypeId wizardDeviceTypeId4 = new WizardDeviceTypeId("VIACARD");
    WizardDeviceTypeId wizardDeviceTypeId5 = new WizardDeviceTypeId("LIBER_T");

    assertThat(allWizardDeviceTypeIds).containsExactly(wizardDeviceTypeId1, wizardDeviceTypeId5, wizardDeviceTypeId2,
                                                       wizardDeviceTypeId4);
  }

  @Test
  public void testFindByServiceTypeId() {

    given(deviceTypeRepository.findAll()).willReturn(newTipoDispositivoList());

    Set<WizardDeviceType> allWizardDeviceTypes = wizardDeviceTypeRepository.findByServiceTypeId(new WizardServiceTypeId("::serviceTypeName1::"));

    then(deviceTypeRepository).should()
                              .findAll();
    then(deviceTypeRepository).shouldHaveNoMoreInteractions();

    WizardDeviceType wizardDeviceType1 = newWizardDeviceType("TELEPASS_EUROPEO");
    wizardDeviceType1.setOrder(1);
    WizardDeviceType wizardDeviceType2 = newWizardDeviceType("LIBER_T");
    wizardDeviceType2.setOrder(6);

    assertThat(allWizardDeviceTypes).containsExactly(wizardDeviceType1, wizardDeviceType2);
  }

  @Test
  public void testFindByDeviceTypeId() {

    given(deviceTypeRepository.findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO)).willReturn(newTipoDispositivo(TELEPASS_EUROPEO, true,
                                                                                                                  1L, "1"));

    Optional<WizardDeviceType> optionalWizardDeviceType = wizardDeviceTypeRepository.findByDeviceTypeId(new WizardDeviceTypeId("TELEPASS_EUROPEO"));

    then(deviceTypeRepository).should()
                              .findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    then(deviceTypeRepository).shouldHaveNoMoreInteractions();

    WizardDeviceType wizardDeviceType = newWizardDeviceType("TELEPASS_EUROPEO");
    wizardDeviceType.setOrder(1);

    assertThat(optionalWizardDeviceType).contains(wizardDeviceType);
  }

  private TipoDispositivo newTipoDispositivo(final TipoDispositivoEnum _name, final boolean _multiservice, final long _id,
                                             final String _tipoServizioId) {
    TipoDispositivo deviceType = new TipoDispositivo();
    deviceType.setNome(_name);
    deviceType.setMultiservizio(_multiservice);
    deviceType.setId(_id);
    deviceType.addTipoServizio(newTipoServizio(_tipoServizioId));
    return deviceType;
  }

  private List<TipoDispositivo> newTipoDispositivoList() {
    List<TipoDispositivo> deviceTypes = new LinkedList<>();
    deviceTypes.add(newTipoDispositivo(LIBER_T, true, 5L, "1"));
    deviceTypes.add(newTipoDispositivo(TELEPASS_EUROPEO, true, 1L, "1"));
    deviceTypes.add(newTipoDispositivo(TELEPASS_ITALIANO, true, 2L, "2"));
    deviceTypes.add(newTipoDispositivo(VIACARD, false, 4L, "4"));
    return deviceTypes;
  }

  private TipoServizio newTipoServizio(final String _id) {
    TipoServizio tipoServizio = new TipoServizio();
    tipoServizio.setNome("::serviceTypeName" + _id + "::");
    return tipoServizio;
  }

  private WizardDeviceType newWizardDeviceType(final String _id) {
    return new WizardDeviceType(new WizardDeviceTypeId(_id));
  }

}
