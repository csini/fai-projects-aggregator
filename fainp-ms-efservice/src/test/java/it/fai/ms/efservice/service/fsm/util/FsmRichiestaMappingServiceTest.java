package it.fai.ms.efservice.service.fsm.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.fsm.FsmRichiestaMappingService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmRichiestaMappingServiceTest {

  @Autowired
  private FsmRichiestaMappingService mapFsm;

  private static int expectedNumFsmImplemented = 52;

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void numberOfOrderStateMachineInMapTest() {
    int countStateMachine = mapFsm.countStateMachine();
    assertThat(countStateMachine).isEqualTo(expectedNumFsmImplemented);
  }

}
