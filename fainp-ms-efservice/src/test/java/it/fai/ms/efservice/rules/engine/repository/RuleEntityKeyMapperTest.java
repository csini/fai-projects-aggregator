package it.fai.ms.efservice.rules.engine.repository;

import static it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKeyMapper.SEPARATOR;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKeyMapper;

public class RuleEntityKeyMapperTest {

  private RuleEntityKeyMapper ruleEntityKeyMapper;

  @Before
  public void setUp() throws Exception {
    ruleEntityKeyMapper = new RuleEntityKeyMapper();
  }

  @Test
  public void testGetKeyMapping() {
    assertThat(ruleEntityKeyMapper.getKeyMapping("service" + SEPARATOR + "device" + SEPARATOR + "vehicle")).isEqualTo(newRuleEntityKey());
  }

  @Test
  public void testGetStringMapping() {
    assertThat(ruleEntityKeyMapper.getStringMapping(newRuleEntityKey())).isEqualTo("service" + SEPARATOR + "device" + SEPARATOR
                                                                                   + "vehicle");
  }

  @Test
  public void testIsSupportedType() {
    assertThat(ruleEntityKeyMapper.isSupportedType(RuleEntityKey.class)).isTrue();
  }

  private RuleEntityKey newRuleEntityKey() {
    RuleEntityKey ruleEntityKey = new RuleEntityKey();
    ruleEntityKey.setDeviceTypeId("device");
    ruleEntityKey.setServiceTypeId("service");
    ruleEntityKey.setVehicleId("vehicle");
    return ruleEntityKey;
  }

}
