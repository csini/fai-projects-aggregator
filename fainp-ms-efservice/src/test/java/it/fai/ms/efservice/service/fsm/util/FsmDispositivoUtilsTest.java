package it.fai.ms.efservice.service.fsm.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.impl.DispositivoServiceImpl;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
// @Ignore
public class FsmDispositivoUtilsTest {

  @Autowired
  private ClienteFaiRepository clienteFaiRepoMock;

  @Autowired
  private JmsTopicSenderService senderJmsMock;

  @Autowired
  private DispositivoServiceImpl dispositivoService;
  
  @Autowired
  private DispositivoServiceExt dispositivoServiceExt;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepositoryExt;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  private FsmDispositivoUtilService fsmDispositivoUtil;

  private Dispositivo dispositivo;

  private String SERIALE        = RandomStringUtils.random(15, true, true);
  private String IDENTIFICATIVO = RandomStringUtils.random(15, true, true);

  @Before
  public void setUp() throws Exception {
    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    dispositivo = new Dispositivo();
    dispositivo.setTipoDispositivo(tipoDispositivoRepository.getOne(1L));
    dispositivo.setStato(StatoDispositivo.INIZIALE);
    dispositivo.setSeriale(SERIALE);
    dispositivo.setModalitaSpedizione(ModalitaSpedizione.A_FAI);
    dispositivo.setIdentificativo(IDENTIFICATIVO);

    dispositivo = dispositivoServiceExt.save(dispositivo);

    fsmDispositivoUtil = new FsmDispositivoUtilService(dispositivoService, clienteFaiRepoMock, senderJmsMock);
  }

  @After
  public void after() {
    dispositivoRepositoryExt.delete(dispositivo);
  }

  @Test
  @Transactional
  public void switchState() throws Exception {
    assertThat(switchStateTest(StatoDispositivo.INIZIALE, DispositivoEvent.ACCETTATO,
                               StatoDispositivo.ACCETTATO_DA_FORNITORE)).isEqualTo(true);
    assertThat(switchStateTest(StatoDispositivo.ACCETTATO_DA_FORNITORE, DispositivoEvent.SPEDITO_A_FAI,
                               StatoDispositivo.ATTIVO_SPEDITO_A_FAI)).isEqualTo(true);
    assertThat(switchStateTest(StatoDispositivo.ATTIVO_SPEDITO_A_FAI, DispositivoEvent.ATTIVAZIONE,
                               StatoDispositivo.ATTIVO)).isEqualTo(true);
    // assertThat(switch_state(StatoDispositivo.INIZIALE, DispositivoEvent.ATTIVAZIONE,
    // StatoDispositivo.ACCETTATO_DA_FORNITORE)).isEqualTo(true);
  }

  private boolean switchStateTest(StatoDispositivo fromState, DispositivoEvent dispositivoEvent,
                                  StatoDispositivo toState) throws Exception {
    Transition<StatoDispositivo, DispositivoEvent> transition = newMockTransition(fromState, dispositivoEvent, toState);
    Message<DispositivoEvent> msg = mock(Message.class);
    MessageHeaders msgHeaders = mock(MessageHeaders.class);

    dispositivo.setStato(fromState);

    when(msg.getHeaders()).thenReturn(msgHeaders);
    when(msgHeaders.get("object")).thenReturn(dispositivo);
    when(msgHeaders.get("nota")).thenReturn("::NOTA_UTENTE::");

    return fsmDispositivoUtil.switchToState(msg, toState, transition);
  }

  private Transition<StatoDispositivo, DispositivoEvent> newMockTransition(StatoDispositivo from, DispositivoEvent event,
                                                                           StatoDispositivo to) {
    Transition<StatoDispositivo, DispositivoEvent> mockTransition = mock(Transition.class);
    State<StatoDispositivo, DispositivoEvent> mockStateFrom = mock(State.class);
    State<StatoDispositivo, DispositivoEvent> mockStateTo = mock(State.class);
    Trigger<StatoDispositivo, DispositivoEvent> mockTrigger = mock(Trigger.class);

    when(mockStateFrom.getId()).thenReturn(from);
    when(mockStateTo.getId()).thenReturn(to);
    when(mockTrigger.getEvent()).thenReturn(event);
    when(mockTransition.getSource()).thenReturn(mockStateFrom);
    when(mockTransition.getTarget()).thenReturn(mockStateTo);
    if (event != null) {
      when(mockTransition.getTrigger()).thenReturn(mockTrigger);
    }
    return mockTransition;
  }

}
