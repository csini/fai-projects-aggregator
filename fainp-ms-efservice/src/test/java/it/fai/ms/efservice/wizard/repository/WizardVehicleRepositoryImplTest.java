package it.fai.ms.efservice.wizard.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.infinispan.Cache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

@RunWith(MockitoJUnitRunner.class)
public class WizardVehicleRepositoryImplTest {

  @Mock
  private Cache<String, WizardVehicleEntity> cache;
  private WizardVehicleRepositoryImpl        wizardVehicleRepository;

  @Before
  public void setUp() throws Exception {
    wizardVehicleRepository = new WizardVehicleRepositoryImpl(cache);
  }

  @Test
  public void testCount() {
    wizardVehicleRepository.count();

    verify(cache).size();
    verifyNoMoreInteractions(cache);
  }

  @Test
  public void testFindAWizardVehicle() {

    given(cache.get("::vehicleUuid::")).willReturn(newWizardVehicleEntity());

    final Optional<WizardVehicle> optionalWizardVehicle = wizardVehicleRepository.find("::vehicleUuid::");

    verify(cache).get("::vehicleUuid::");
    verifyNoMoreInteractions(cache);

    assertThat(optionalWizardVehicle).isPresent();
    assertThat(optionalWizardVehicle).contains(newWizardVehicle());
  }

  @Test
  public void testFindAWizardVehicleIdNotPresent() {
    final Optional<WizardVehicle> optionalWizardVehicle = wizardVehicleRepository.find("::uuid_not_present::");

    verify(cache).get("::uuid_not_present::");
    verifyNoMoreInteractions(cache);

    assertThat(optionalWizardVehicle).isEmpty();
  }

  @Test
  public void testSave() {
    wizardVehicleRepository.save(newWizardVehicleWithDeviceTypeIds());

    verify(cache).put("::vehicleUuid::", newWizardVehicleEntity());
    verifyNoMoreInteractions(cache);
  }

  private Set<WizardDeviceTypeId> newDeviceTypeIds() {
    Set<WizardDeviceTypeId> deviceTypeIds = new HashSet<>();
    deviceTypeIds.add(new WizardDeviceTypeId("::wizardDeviceTypeId1::"));
    deviceTypeIds.add(new WizardDeviceTypeId("::wizardDeviceTypeId2::"));
    return deviceTypeIds;
  }

  private WizardVehicle newWizardVehicle() {
    WizardVehicle wizardVehicle = new WizardVehicle(new WizardVehicleId("::vehicleUuid::"));
    wizardVehicle.setEuroClass("::euroClass::");
    wizardVehicle.setLicensePlate("::licensePlate::");
    wizardVehicle.setState("::state::");
    wizardVehicle.setType("::type::");
    return wizardVehicle;
  }

  private WizardVehicleEntity newWizardVehicleEntity() {
    WizardVehicleEntity wizardVehicleEntity = new WizardVehicleEntity("::vehicleUuid::");
    wizardVehicleEntity.setEuroClass("::euroClass::");
    wizardVehicleEntity.setLicensePlate("::licensePlate::");
    wizardVehicleEntity.setState("::state::");
    wizardVehicleEntity.setType("::type::");
    return wizardVehicleEntity;
  }

  private WizardVehicle newWizardVehicleWithDeviceTypeIds() {
    WizardVehicle wizardVehicle = new WizardVehicle(new WizardVehicleId("::vehicleUuid::"));
    wizardVehicle.setDeviceTypeIds(newDeviceTypeIds());
    wizardVehicle.setEuroClass("::euroClass::");
    wizardVehicle.setLicensePlate("::licensePlate::");
    wizardVehicle.setState("::state::");
    wizardVehicle.setType("::type::");
    return wizardVehicle;
  }

}
