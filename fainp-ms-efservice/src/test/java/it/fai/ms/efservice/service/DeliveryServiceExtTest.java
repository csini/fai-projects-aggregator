package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.dml.document.ContrattiDTO;
import it.fai.ms.common.dml.document.DeliveryDTO;
import it.fai.ms.common.dml.document.DeliveryPrintProduttoreRequestDTO;
import it.fai.ms.common.dml.document.DeliveryPrintRequestDTO;
import it.fai.ms.common.dml.document.DeliveryProduttoreDTO;
import it.fai.ms.common.dml.document.DispositivoDTO;
import it.fai.ms.common.dml.document.ServizioDTO;
import it.fai.ms.common.dml.document.TipiDispositivo;
import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.common.jms.dto.efservice.DeliveryCompanyDTO;
import it.fai.ms.efservice.client.AnagaziendeClientService;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.service.dto.DeliveryPrintDTO;
import it.fai.ms.efservice.service.impl.ClienteFaiServiceImplExt;
import it.fai.ms.efservice.wizard.repository.entity.WizardVehicleEntity;

public class DeliveryServiceExtTest {

  static String ACAR    = "0014113";
  static String SIMEONI = "0046348";

  DeliveryDeviceService deliveryDeviceService;

  DeliveryPrintDTO deliveryPrintRequestDTO;

  private AnagaziendeClientService           anagaziendeClientServiceMock        = mock(AnagaziendeClientService.class);
  private ClienteFaiServiceImplExt           clienteFaiServiceMock               = mock(ClienteFaiServiceImplExt.class);
  private DispositivoServiceExt              dispositivoServiceMock              = mock(DispositivoServiceExt.class);
  private Map<String, WizardVehicleEntity>   cacheVehicleWizardMock              = mock(Map.class);
  private StatoDispositivoServizioServiceExt statoDispositivoServizioServiceMock = mock(StatoDispositivoServizioServiceExt.class);
  private GatewayClientService               gatewayClientServiceMock            = mock(GatewayClientService.class);

  private String customerCode = "::codiceCliente::";

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    deliveryDeviceService = new DeliveryDeviceService(anagaziendeClientServiceMock, clienteFaiServiceMock, dispositivoServiceMock,
                                                      cacheVehicleWizardMock, statoDispositivoServizioServiceMock,
                                                      gatewayClientServiceMock);
  }

  @Test
  public void testDeliveryPrint() {
    DeliveryPrintRequestDTO printRequest = new DeliveryPrintRequestDTO();
    List<Richiesta> richieste = newRichieste();
    IndirizziDiSpedizioneDTO indirizzo = new IndirizziDiSpedizioneDTO();
    when(clienteFaiServiceMock.findByCodiceCliente(customerCode)).thenReturn(newClienteFai());
    when(clienteFaiServiceMock.findByRaggruppamentoImpresa("0001")).thenReturn(newClienteFaiConsorzio());
    when(anagaziendeClientServiceMock.getIndirizzoSpedizioneClienteFai(customerCode)).thenReturn(indirizzo);
    when(gatewayClientServiceMock.getAziendaByCodice(any())).thenReturn(newAziendaDTO());
    when(cacheVehicleWizardMock.get(any())).thenReturn(newWizardVehicle());
    OngoingStubbing<List<StatoDispositivoServizio>> stubbing = when(statoDispositivoServizioServiceMock.findbyDispositivo(any()));
    stubbing.thenReturn(newStatoDispositivos());

    DeliveryDTO deliveryDtoByRequestsCustomer = deliveryDeviceService.getDeliveryDtoByRequestsCustomer(customerCode, richieste,
                                                                                                       printRequest);

    DeliveryCompanyDTO company = deliveryDtoByRequestsCustomer.getCompany();
    assertThat(company).isNotNull();
    assertThat(company.getLang()).isNotNull();

    List<TipiDispositivo> tipiDispositivo = deliveryDtoByRequestsCustomer.getTipiDispositivo();
    assertThat(tipiDispositivo).isNotNull();
    assertThat(tipiDispositivo).isNotEmpty();
    assertThat(tipiDispositivo).hasSize(2);

    TipiDispositivo tdTrackyCard = tipiDispositivo.get(0);
    assertThat(tdTrackyCard).isNotNull();
    assertThat(tdTrackyCard.getCodice()).isEqualTo(TipoDispositivoEnum.TRACKYCARD.name());

    List<ContrattiDTO> contrattiTracky = tdTrackyCard.getContratti();
    assertThat(contrattiTracky).isNotNull();
    assertThat(contrattiTracky).hasSize(2);

    for (ContrattiDTO contrattiDTO : contrattiTracky) {
      List<DispositivoDTO> dispositivi = contrattiDTO.getDispositivi();
      assertThat(dispositivi).isNotNull();
      assertThat(dispositivi).hasSize(1);

      for (DispositivoDTO dispositivo : dispositivi) {
        assertThat(dispositivo.getSeriale()).isNotNull();
        List<ServizioDTO> servizi = dispositivo.getServizi();
        assertThat(servizi).isNotNull();
        assertThat(servizi).hasSize(2);

        for (ServizioDTO serviceDTO : servizi) {
          assertThat(serviceDTO.getNome()).isNotNull();
          assertThat(serviceDTO.getAbilitato()).isNotNull();
        }
      }
    }

    TipiDispositivo tdTelepass = tipiDispositivo.get(1);
    assertThat(tdTelepass).isNotNull();
    assertThat(tdTelepass.getCodice()).isEqualTo(TipoDispositivoEnum.TELEPASS_EUROPEO.name());

    List<ContrattiDTO> contrattiTelepass = tdTelepass.getContratti();
    assertThat(contrattiTelepass).isNotNull();
    assertThat(contrattiTelepass).hasSize(2);
    for (ContrattiDTO contrattiDTO : contrattiTelepass) {
      List<DispositivoDTO> dispositivi = contrattiDTO.getDispositivi();
      assertThat(dispositivi).isNotNull();
      assertThat(dispositivi).hasSize(1);

      for (DispositivoDTO dispositivo : dispositivi) {
        assertThat(dispositivo.getSeriale()).isNotNull();
        List<ServizioDTO> servizi = dispositivo.getServizi();
        assertThat(servizi).isNotNull();
        assertThat(servizi).hasSize(4);

        for (ServizioDTO serviceDTO : servizi) {
          assertThat(serviceDTO.getNome()).isNotNull();
          assertThat(serviceDTO.getAbilitato()).isNotNull();
        }
      }
    }

    assertThat(printRequest).isNotNull();
    assertThat(printRequest.getCompanyGroups()).isNotNull();
    assertThat(printRequest.getCompanyGroups()).isNotEmpty();
  }

  @Test
  public void printProduttore() {
    List<Richiesta> richieste = newRichieste();

    when(cacheVehicleWizardMock.get(any())).thenReturn(newWizardVehicle());

    DeliveryPrintProduttoreRequestDTO deliveryPrintProduttoreRequestDTO = new DeliveryPrintProduttoreRequestDTO();
    LocalDate dateDelivery = LocalDate.now();
    deliveryPrintProduttoreRequestDTO.setDeliveryDate(dateDelivery);
    deliveryDeviceService.getDeliveryDtoByProduttore(richieste.stream()
                                                              .flatMap(r -> r.getDispositivos()
                                                                             .stream())
                                                              .collect(toList()),
                                                     deliveryPrintProduttoreRequestDTO);

    assertThat(deliveryPrintProduttoreRequestDTO).isNotNull();
    assertThat(deliveryPrintProduttoreRequestDTO.getDeliveryDate()).isNotNull()
                                                                   .isEqualTo(dateDelivery);

    List<DeliveryProduttoreDTO> deliveryProduttore = deliveryPrintProduttoreRequestDTO.getDeliveryProduttore();
    assertThat(deliveryProduttore).isNotNull()
                                  .isNotEmpty()
                                  .hasSize(2)
                                  .extracting(d -> tuple(d.getProduttore()
                                                          .getProduttoreName(),
                                                         d.getProduttore()
                                                          .getProduttoreCodeNav()))
                                  .contains(tuple("TELEPASS", "F_TELEPASS"), tuple("FAISERVICE", "F_FAISERVICE"));
    assertThat(deliveryProduttore.get(0)
                                 .getDevice()).isNotNull()
                                              .isNotEmpty()
                                              .hasSize(2);
    assertThat(deliveryProduttore.get(1)
                                 .getDevice()).isNotNull()
                                              .isNotEmpty()
                                              .hasSize(2);
  }

  private List<ClienteFai> newClienteFaiConsorzio() {
    List<ClienteFai> list = new ArrayList<>();
    ClienteFai newClienteFai = newClienteFai();
    list.add(newClienteFai);
    ClienteFai newClienteFai2 = newClienteFai().consorzioPadre(true)
                                               .codiceCliente("consorzio");
    list.add(newClienteFai2);
    return list;
  }

  private WizardVehicleEntity newWizardVehicle() {
    WizardVehicleEntity vehicle = new WizardVehicleEntity(UUID.randomUUID()
                                                              .toString());
    vehicle.setLicensePlate("AA000AA");
    vehicle.setEuroClass("CLASSE EURO");
    vehicle.setCountry("::country::");
    return vehicle;
  }

  private List<StatoDispositivoServizio> newStatoDispositivos() {
    List<StatoDispositivoServizio> list = new ArrayList<>();
    for (int i = 1; i < 4; i++) {
      StatoDispositivoServizio sds = new StatoDispositivoServizio();
      sds.setTipoServizio(newTipoServizio("SERV" + i));
      sds.stato(StatoDS.IN_ATTIVAZIONE);
      list.add(sds);
    }
    return list;
  }

  private AziendaDTO newAziendaDTO() {
    AziendaDTO azienda = new AziendaDTO();
    azienda.setPaeseNascitaLegaleRappresentante("IT");
    return azienda;
  }

  private ClienteFai newClienteFai() {
    ClienteFai cliente = new ClienteFai();
    cliente.via("Via")
           .cap("CAP")
           .raggruppamentoImpresa("0001")
           .consorzioPadre(false)
           .codiceCliente(customerCode);
    return cliente;
  }

  private List<Richiesta> newRichieste() {
    List<Richiesta> request = new ArrayList<>();
    for (int num = 1; num < 5; num++) {
      Richiesta r = new Richiesta();
      Dispositivo d = new Dispositivo();
      d.setSeriale("seriale" + num);

      Contratto contract = new Contratto();
      contract.setCodContrattoCliente("contratto-" + num);
      if (num == 4) {
        contract.setCodContrattoCliente("contratto-" + 1);
      }
      d.setContratto(contract);

      AssociazioneDV adv = new AssociazioneDV();
      adv.setDispositivo(d);
      adv.setUuidVeicolo("vehicle" + num);
      Set<AssociazioneDV> associazioneDVS = new HashSet<>();
      associazioneDVS.add(adv);
      d.setAssociazioneDispositivoVeicolos(associazioneDVS);

      Set<Richiesta> richiestas = new HashSet<>();
      d.setRichiestas(richiestas);

      Set<Dispositivo> dispositivos = new HashSet<>();
      d.setTipoDispositivo(newTipoDispositivo(num));
      ClienteFai newClienteFai = newClienteFai(d.getTipoDispositivo());
      contract.setClienteFai(newClienteFai);
      d.setContratto(contract);
      dispositivos.add(d);

      r.setDispositivos(dispositivos);
      request.add(r);
    }
    return request;
  }

  private ClienteFai newClienteFai(TipoDispositivo deviceType) {
    ClienteFai cf = new ClienteFai();
    switch (deviceType.getNome()) {
    case TELEPASS_EUROPEO:
      cf.setRagioneSociale("::SERVICE TRASPORTI SRL::");
      break;
    case TRACKYCARD:
      cf.setRagioneSociale("::ASAM SRL::");
      break;

    default:
      cf.setRagioneSociale("::DEFAULT SPA::");
      break;
    }
    return cf;
  }

  private TipoDispositivo newTipoDispositivo(int num) {
    TipoDispositivo td = new TipoDispositivo();
    TipoDispositivoEnum deviceType = (num % 2 == 0) ? TipoDispositivoEnum.TELEPASS_EUROPEO : TipoDispositivoEnum.TRACKYCARD;
    td.setNome(deviceType);
    td.nomeBusiness(deviceType.name());
    Set<TipoServizio> tipoServizios = new HashSet<>();
    if (num % 2 == 0) {
      tipoServizios.add(newTipoServizio("SERV1"));
      tipoServizios.add(newTipoServizio("SERV2"));
      tipoServizios.add(newTipoServizio("SERV3"));
      tipoServizios.add(newTipoServizio("SERV4"));
    } else {
      tipoServizios.add(newTipoServizio("SERV1"));
      tipoServizios.add(newTipoServizio("SERV3"));
    }
    td.setTipoServizios(tipoServizios);
    td.setProduttore(newProduttore(deviceType));
    return td;
  }

  private Produttore newProduttore(TipoDispositivoEnum deviceType) {
    Produttore p = new Produttore();
    switch (deviceType) {
    case TELEPASS_EUROPEO:
      p.setCodiceFornitoreNav("F_TELEPASS");
      p.setNome("TELEPASS");
      p.setId(1L);
      break;
    case TRACKYCARD:
      p.setCodiceFornitoreNav("F_FAISERVICE");
      p.setNome("FAISERVICE");
      p.setId(2L);
      break;

    default:
      p.setCodiceFornitoreNav("P_NOT_FOUND");
      p.setNome("NOT_FOUND");
      p.setId(8890L);
      break;
    }
    return p;
  }

  private TipoServizio newTipoServizio(String nome) {
    TipoServizio ts = new TipoServizio();
    ts.nome(nome);
    ts.nomeBusiness(nome + "_");
    return ts;
  }

  static String RandomEmail() {
    return RandomStringUtils.random(10, true, true) + "@" + RandomStringUtils.random(8, true, false) + "."
           + RandomStringUtils.random(3, true, false);
  }

  static String RandomString() {
    return RandomStringUtils.random(10, true, true);
  }

  static String RandomString(int count) {
    return RandomStringUtils.random(count, true, true);
  }

  static String RandomUUID() {
    return UUID.randomUUID()
               .toString();
  }

  static void logToJson(Object obj) throws Exception {
    final ObjectMapper mapper = new ObjectMapper();
    final String jsonInString = mapper.writeValueAsString(obj);
    System.out.println(jsonInString);
  }

}
