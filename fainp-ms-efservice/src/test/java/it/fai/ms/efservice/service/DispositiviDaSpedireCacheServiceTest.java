package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DispositiviDaSpedireCacheServiceTest {
  
  private final static int num = 5;

  @Autowired
  private DispositiviDaSpedireCacheService deviceToSendServiceCache;

  private List<String> identificativiDispositivi;

  @Before
  public void setUp() {
    // Pulisco i valori rimasti in cache;
    deviceToSendServiceCache.clearCache();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss.sss");
    for (int i = 0; i < num; i++) {
      if (identificativiDispositivi == null) {
        identificativiDispositivi = new ArrayList<>();
      }
      String dateFormatted = sdf.format(new Date());
      String identificativo = i + "-DEVICE-" + dateFormatted;
      identificativiDispositivi.add(identificativo);
    }
  }
  
  @After
  public void afterTest() {
    deviceToSendServiceCache.clearCache();
  }

  @Test
  public void test() {
    deviceToSendServiceCache.putAllKeys(identificativiDispositivi);

    Set<String> allKeys = deviceToSendServiceCache.getAllKeys();
    assertThat(allKeys).hasSize(num);

    for (String keys : allKeys) {
      Boolean bool = deviceToSendServiceCache.get(keys);
      assertThat(bool).isTrue();
    }
  }

}
