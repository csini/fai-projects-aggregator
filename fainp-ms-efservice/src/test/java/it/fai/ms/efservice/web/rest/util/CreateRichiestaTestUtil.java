package it.fai.ms.efservice.web.rest.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.mapper.RichiestaMapper;

@Service
@Transactional
public class CreateRichiestaTestUtil {

  private final static Logger log = LoggerFactory.getLogger(CreateRichiestaTestUtil.class);

  private static final String CODICE_CLIENTE_FAI = "0046276";

  private static final String tipoDispositivoDefault = "TELEPASS_EUROPEO";

  private static final String PEDAGGIO_ITALIA     = "PEDAGGI_ITALIA";
  private static final String PEDAGGIO_POLONIA    = "PEDAGGI_POLONIA";
  private static final String PEDAGGIO_FRANCIA    = "PEDAGGI_FRANCIA";
  private static final String PEDAGGIO_PORTOGALLO = "PEDAGGI_PORTOGALLO";

  private static final String tipoServizioDefault = PEDAGGIO_ITALIA;

  private static final String veicolo1 = "AA000AA";
  private static final String veicolo2 = "AB001AA";
  private static final String veicolo3 = "AC002AA";
  private static final String veicolo4 = "AD003AA";
  private static final String veicolo5 = "AE004AA";
  private static final String veicolo6 = "AF005AA";

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private RichiestaServiceExt richiestaServiceExt;

  @Autowired
  private RichiestaMapper richiestaMapper;

  CarrelloDTO carrello;

  public RichiestaDTO mappingNewRichiesta() throws IOException, Exception {
    Richiesta richiesta = createRichiestaByCarrelloDTO(null);
    return richiestaMapper.toDto(richiesta);
  }

  public Richiesta createRichiestaByCarrelloDTO() throws IOException, Exception {
    return createRichiestaByCarrelloDTO(null);
  }

  public Richiesta createRichiestaByCarrelloDTO(String codiceCliente) throws IOException, Exception {

    carrello = new CarrelloDTO();
    carrello.setCodiceAzienda((StringUtils.isNotBlank(codiceCliente)) ? codiceCliente : CODICE_CLIENTE_FAI);

    HashMap<String, String> datiAggiuntivi = new HashMap<>();
    datiAggiuntivi.put("costo", "1500");
    carrello.setDatiAggiuntivi(datiAggiuntivi);

    carrello.setTipoServizio(tipoServizioDefault);
    carrello.setUuidVeicoli(createUuidVeicoli(new ArrayList<String>(Arrays.asList(veicolo1, veicolo3, veicolo5))));
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(veicolo1, new ArrayList<String>(Arrays.asList(PEDAGGIO_FRANCIA, PEDAGGIO_POLONIA)));
    mappaVeicoli.put(veicolo3, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA, PEDAGGIO_PORTOGALLO)));
    mappaVeicoli.put(veicolo6, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrello.setDispositivi(createDispositivi(tipoDispositivoDefault, mappaDispositiviVeicoli));

    List<Richiesta> richieste = richiestaServiceExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrello);
    int size = richieste.size();
    if (size != 1) {
      return null;
    }

    Richiesta richiesta = richiestaRepositoryExt.findOne(richieste.get(0)
                                                                  .getId());
    if (richiesta != null) {
      log.info("Richiesta id: " + richiesta.getId() + " - identificativo: " + richiesta.getIdentificativo());
    }
    return richiesta;
  }

  private List<DispositivoCarrelloDTO> createDispositivi(String tipoDispositivo,
                                                         HashMap<Integer, HashMap<String, List<String>>> mapDispositivi) {
    List<DispositivoCarrelloDTO> dispositivi = new ArrayList<>();

    Set<Integer> keySet = mapDispositivi.keySet();
    Iterator<Integer> iterator = keySet.iterator();

    while (iterator != null && iterator.hasNext()) {
      Integer keyDispositivo = iterator.next();
      HashMap<String, List<String>> veicoliCarrello = mapDispositivi.get(keyDispositivo);
      DispositivoCarrelloDTO dispositivoCarrello = new DispositivoCarrelloDTO();
      dispositivoCarrello.setTipoDispositivo(tipoDispositivo);
      dispositivoCarrello.setVeicoli(createVeicoliCarrello(veicoliCarrello));
      dispositivi.add(dispositivoCarrello);
    }

    return dispositivi;
  }

  private List<VeicoloCarrelloDTO> createVeicoliCarrello(HashMap<String, List<String>> map) {
    List<VeicoloCarrelloDTO> listVeicolo = new ArrayList<>();
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while (iterator != null && iterator.hasNext()) {
      String keyVeicolo = iterator.next();
      List<String> listServizi = map.get(keyVeicolo);
      VeicoloCarrelloDTO veicolo = new VeicoloCarrelloDTO();
      veicolo.setUuid(keyVeicolo);
      veicolo.setNomeTipiServizio(createNomeServizio(listServizi));
      listVeicolo.add(veicolo);
    }
    return listVeicolo;
  }

  private List<String> createNomeServizio(List<String> servizi) {
    List<String> nomeServizio = new ArrayList<>();

    for (String servizio : servizi) {
      nomeServizio.add(servizio);
    }
    return nomeServizio;
  }

  private List<String> createUuidVeicoli(List<String> listVeicoli) {
    List<String> uuidVeicoli = new ArrayList<>();
    for (String veicolo : listVeicoli) {
      uuidVeicoli.add(veicolo);
    }
    return uuidVeicoli;
  }
}
