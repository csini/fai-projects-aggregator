package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.utils.MessageSourceServiceExt;

import it.fai.ms.efservice.FaiefserviceApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class SubjectMailTest {

  private String subjectActivetedDeviceDefault_1 = "[FAISERVICE] - Il dispositivo ";
  private String subjectActivetedDeviceDefault_2 = " disponibile per il ritiro";
  private String subjectActivetedDeviceEN_1      = "[FAISERVICE] - Device ";
  private String subjectActivetedDeviceEN_2      = " is ready to be picked up";
  private String subjectActivetedDeviceDE_1      = "[FAISERVICE] - Ger";
  private String subjectActivetedDeviceDE_2      = "r jeden Gast";
  private String subjectAssignedDeviceDefault_1  = "Assegnati ";
  private String subjectAssignedDeviceDefault_2  = " al cliente ";
  private String subjectAssignedDeviceEN_1       = " Devices assigned ";
  private String subjectAssignedDeviceEN_2       = " to customer: ";
  private String subjectAssignedDeviceDE_1       = "Weisen Sie ";
  private String subjectAssignedDeviceDE_2       = " zu.";

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void activatedDeviceSubject_notFoundLanguage() {
    checkValidationSubject("not-found-language", MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, subjectActivetedDeviceDefault_1,
                           subjectActivetedDeviceDefault_2);
  }

  @Test
  public void activatedDeviceSubject_IT() {
    checkValidationSubject("IT", MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, subjectActivetedDeviceDefault_1,
                           subjectActivetedDeviceDefault_2);
  }

  @Test
  public void activatedDeviceSubject_DE() {
    checkValidationSubject("DE", MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, subjectActivetedDeviceDE_1,
                           subjectActivetedDeviceDE_2);
  }

  @Test
  public void activatedDeviceSubject_EN() {
    checkValidationSubject("EN", MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, subjectActivetedDeviceEN_1,
                           subjectActivetedDeviceEN_2);
  }

  @Test
  public void deviceAssignedSubject_notFoundLanguage() {
    checkValidationSubject("not-found-language", MailActivatedDeviceService.SUBJECT_KEY_DEVICES_ASSIGNED, subjectAssignedDeviceDefault_1,
                           subjectAssignedDeviceDefault_2);
  }

  @Test
  public void deviceAssignedSubject_IT() {
    checkValidationSubject("IT", MailActivatedDeviceService.SUBJECT_KEY_DEVICES_ASSIGNED, subjectAssignedDeviceDefault_1,
                           subjectAssignedDeviceDefault_2);
  }

  @Test
  public void deviceAssignedSubject_DE() {
    checkValidationSubject("DE", MailActivatedDeviceService.SUBJECT_KEY_DEVICES_ASSIGNED, subjectAssignedDeviceDE_1,
                           subjectAssignedDeviceDE_2);
  }

  @Test
  public void deviceAssignedSubject_EN() {
    checkValidationSubject("EN", MailActivatedDeviceService.SUBJECT_KEY_DEVICES_ASSIGNED, subjectAssignedDeviceEN_1,
                           subjectAssignedDeviceEN_2);
  }

  private void checkValidationSubject(String langCode, String keyMessage, String firstStringExpected, String lastStringExpected) {
    if (!langCode.equalsIgnoreCase("EN") && !langCode.equalsIgnoreCase("DE"))
      langCode = "it";
    Locale local = Locale.forLanguageTag(langCode);
    String subject = MessageSourceServiceExt.getMessage(keyMessage, local);
    boolean conditionStart = subject.startsWith(firstStringExpected);
    boolean conditionContains1 = subject.contains(firstStringExpected);
    assertThat(conditionStart || conditionContains1).isTrue();
    boolean conditionEnd = subject.endsWith(firstStringExpected);
    boolean conditionContains2 = subject.contains(firstStringExpected);
    assertThat(conditionEnd || conditionContains2).isTrue();
  }

}
