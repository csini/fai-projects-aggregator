package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.context.ApplicationEventPublisher;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.asyncjob.AsyncJobServiceImpl;
import it.fai.ms.efservice.client.DocumentClient;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.service.dto.CaricamentoOrdineFornitoreBySerialeDTO.OrdineFornitoreDispositivo;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import it.fai.ms.efservice.service.dto.ResponseOrdineFornitoreDTO;
import it.fai.ms.efservice.service.impl.DispositivoOrdineFornitoreServiceImpl;

public class OrdineFornitoreAsyncServiceTest {

  private static final long                 ID_ORDINE = 123l;
  private AsyncJobServiceImpl               asyncJobService;
  private DocumentClient                    documentClient;
  private OrdineFornitoreServiceExt         ordineFornitoreServiceExt;
  private DispositivoOrdineFornitoreService deviceOrdineFornitoreService;
  OrdineFornitoreAsyncService               subject;

  DispositivoServiceExt              dispositivoServiceExt;
  StatoDispositivoServizioRepository sdsRepository;
  TipoDispositivoServiceExt          tipoDispositivoServiceExt;
  ApplicationEventPublisher          applicationEventPublisher;

  @Before
  public void setup() {
    asyncJobService           = mock(AsyncJobServiceImpl.class);
    documentClient            = mock(DocumentClient.class);
    ordineFornitoreServiceExt = mock(OrdineFornitoreServiceExt.class);

    dispositivoServiceExt     = mock(DispositivoServiceExt.class);
    sdsRepository             = mock(StatoDispositivoServizioRepository.class);
    tipoDispositivoServiceExt = mock(TipoDispositivoServiceExt.class);
    applicationEventPublisher = mock(ApplicationEventPublisher.class);

    deviceOrdineFornitoreService = new DispositivoOrdineFornitoreServiceImpl(dispositivoServiceExt, sdsRepository,
                                                                             tipoDispositivoServiceExt, applicationEventPublisher);

    subject = new OrdineFornitoreAsyncService(asyncJobService, documentClient, ordineFornitoreServiceExt, deviceOrdineFornitoreService);
  }

  @Test
  public void testGenerateDeviceForOrdineFornitoreOrdineFornitoreDTOListOfStringString() throws Exception {
    OrdineFornitoreDTO ordineFornitoreDTO = init(new OrdineFornitoreDTO(), d -> {
      d.setId(ID_ORDINE);
    });
    when(ordineFornitoreServiceExt.getTipoDispositivoByOrdineDispositivoId(ID_ORDINE))
      .thenReturn(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT.name());

    TipoDispositivo deviceType = init(new TipoDispositivo(), t -> {
      t.addTipoServizio(init(new TipoServizio(), ts -> {
      }));
    });

    when(tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT)).thenReturn(deviceType);
    when(dispositivoServiceExt.createDispositivoDefault(deviceType)).thenAnswer(i -> {
      return new Dispositivo();
    });
    when(dispositivoServiceExt.save(Mockito.any())).then(invocation -> (Dispositivo) invocation.getArguments()[0]);

    // List<String[]> rowList =Arrays.asList((new String[]{"1234567811","","","30/12/2098"}),new
    // String[]{"1234567812","","","31/12/2099"});

    Collection<OrdineFornitoreDispositivo> rowList = Arrays.asList(init(new OrdineFornitoreDispositivo(), o -> {
      o.setSeriale("1234567811");
      o.setPan("");
      o.setTipoHardware("");
      o.setDataScadenza(ZonedDateTime.parse("2098-12-30T00:00:00+01:00[Europe/Rome]"));
    }), init(new OrdineFornitoreDispositivo(), o -> {
      o.setSeriale("1234567812");
      o.setPan("");
      o.setTipoHardware("");
      o.setDataScadenza(ZonedDateTime.parse("2099-12-31T00:00:00+01:00[Europe/Rome]"));
    }));

    ResponseOrdineFornitoreDTO generateDeviceForOrdineFornitore = subject.generateDeviceForOrdineFornitore(ordineFornitoreDTO, rowList, "");

    ArgumentCaptor<Dispositivo> disp = ArgumentCaptor.forClass(Dispositivo.class);
    verify(dispositivoServiceExt, times(2)).save(disp.capture());

    List<Dispositivo> allValues = disp.getAllValues();
    assertThat(allValues).hasSize(2);
    assertThat(allValues.get(0)
      .getSeriale()).isEqualTo("1234567811");
    assertThat(allValues.get(0)
      .getDataScadenza()).isEqualTo(ZonedDateTime.parse("2098-12-30T00:00:00+01:00[Europe/Paris]"));

    assertThat(allValues.get(1)
      .getSeriale()).isEqualTo("1234567812");
    assertThat(allValues.get(1)
      .getDataScadenza()).isEqualTo(ZonedDateTime.parse("2099-12-31T00:00:00+01:00[Europe/Paris]"));

    System.out.println(generateDeviceForOrdineFornitore);
  }

  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
