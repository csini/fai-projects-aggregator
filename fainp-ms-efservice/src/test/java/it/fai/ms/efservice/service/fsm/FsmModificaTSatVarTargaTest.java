/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.config.JmsSenderConfigurationTest;

/**
 * @author Luca Vassallo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FaiefserviceApp.class, JmsSenderConfigurationTest.class})
@Ignore
public class FsmModificaTSatVarTargaTest {

  private final static String CODICE_CLIENTE_FAI = "0073935";

  @Autowired
  private FsmRichiestaMappingService fsmMap;

  private AbstractFsmRichiesta fsm;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  private Richiesta richiesta;
  
  private String notaUtente = "::NOTA UTENTE::";
  
  @Before // Set-up
  public void setUp() throws Exception {

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    OrdineCliente ordineCliente = new OrdineCliente();
    richiesta = new Richiesta();
    richiesta.setOrdineCliente(ordineCliente);
    ordineCliente.setRichiestas(new HashSet<>(Arrays.asList(richiesta)));
    richiesta.setId(RandomUtils.nextLong());
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT);
    richiesta.getOrdineCliente().setClienteAssegnatario(clienteFaiRepo.findOneByCodiceCliente(CODICE_CLIENTE_FAI));
    richiesta.setUltimaNota(null);

    fsm = fsmMap.getFSM(FsmType.MOD_TSAT_VARIAZIONE_TARGA.fsmName());
  }

  @Test
  public void FsmModificaTeVarTarga_001_01() throws Exception {

    richiesta.setTipo(TipoRichiesta.VARIAZIONE_TARGA);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_001_02() throws Exception {

    richiesta.setTipo(TipoRichiesta.MEZZO_RITARGATO);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_001_03() throws Exception {

    richiesta.setTipo(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
  }

  @Test
  public void FsmModificaTeVarTarga_002_01() throws Exception {

    richiesta.setTipo(TipoRichiesta.VARIAZIONE_TARGA);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  @Test
  public void FsmModificaTeVarTarga_002_02() throws Exception {

    richiesta.setTipo(TipoRichiesta.MEZZO_RITARGATO);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);

  }

  @Test
  public void FsmModificaTeVarTarga_002_03() throws Exception {

    richiesta.setTipo(TipoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, StatoRichiesta.DISATTIVAZIONE_DISPOSITIVO);

    assertChangeStateFsmTe(RichiestaEvent.MU_LETTERA_VETTURA, StatoRichiesta.EVASO_CON_RICONSEGNA);
  }

  private void assertChangeStateFsmTe(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsm.isAvailableCommand(event, richiesta);
    assertTrue(availableCommand);

    fsm.executeCommandToChangeState(event, richiesta, notaUtente);
    StatoRichiesta fsmStatus = richiesta.getStato();
    assertEquals(state.name(), fsmStatus.name());
  }

}
