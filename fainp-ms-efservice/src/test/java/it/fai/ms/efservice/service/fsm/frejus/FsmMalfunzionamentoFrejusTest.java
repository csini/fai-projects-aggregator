package it.fai.ms.efservice.service.fsm.frejus;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.frejus.FsmMalfunzionamentoFrejusSost;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmMalfunzionamentoFrejusTest {

  private static final String TARGA_TEST = "AA111AA";
  private static final String TARGA_NAZIONE_TEST = "IT";

  private static final String CODICE_AZIENDA = "0073935";

  @Autowired//Spy bean for test
  private FsmSenderToQueue senderFsmService;

  @Autowired
  private FsmMalfunzionamentoFrejusSost fsm;


  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private FsmRichiestaCacheService cacheServiceMock;


  @Before
  public void init() {
    cacheServiceMock.clearCache();
  }



  @Test
  public void changeStatoRichiesta() throws Exception {
    Richiesta richiesta = buildRichiesta()
      .tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
      .addDispositivo(buildDispositivo())
      .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS);

    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);

    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.DA_INOLTRARE);
    verify(senderFsmService).sendMessageForChangeStatoDispositivo(Mockito.any(), Mockito.eq(DispositivoEvent.BLOCCO_NO_DICHIARATO));
  }




  private Dispositivo buildDispositivo() {
    Dispositivo d = new Dispositivo();
    return d;
  }



  private Richiesta buildRichiesta() {
    Richiesta richiesta = new Richiesta();
    richiesta.setId(5678L);
    richiesta.setTipo(TipoRichiesta.MALFUNZIONAMENTO);
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_FREJUS);
    richiesta.setOrdineCliente(newOrdineCliente());
    richiesta.setContratto(new Contratto().clienteFai(richiesta.getOrdineCliente().getClienteAssegnatario()));
    richiesta.setAssociazione(TARGA_TEST);
    richiesta.setNewTargaNazione(TARGA_NAZIONE_TEST);
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().clienteAssegnatario(newClienteFai());
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }




}
