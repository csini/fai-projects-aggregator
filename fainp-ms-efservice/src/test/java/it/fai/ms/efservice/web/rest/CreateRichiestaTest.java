package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.AnagaziendeClient;
import it.fai.ms.efservice.client.dto.IndirizziDiSpedizioneDTO;
import it.fai.ms.efservice.client.dto.IndirizzoSpedizioneDTO;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.dto.CarrelloDTO;
import it.fai.ms.efservice.service.dto.DispositivoCarrelloDTO;
import it.fai.ms.efservice.service.dto.VeicoloCarrelloDTO;
import it.fai.ms.efservice.service.impl.IndirizzoSpedizioneOrdiniServiceImplExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, CreateRichiestaTest.RichiestaResourceConfiguration.class })
@Ignore("Use only create richiesta test")
public class CreateRichiestaTest {

  @Configuration
  public static class RichiestaResourceConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    @Autowired
    private IndirizzoSpedizioneOrdiniRepository repositoryIndirizzi;

    private AnagaziendeClient anagAziendeMock = mock(AnagaziendeClient.class);

    @PostConstruct
    public void init() {
      IndirizziDiSpedizioneDTO indirizziDTO = new IndirizziDiSpedizioneDTO();
      newIndirizzoSpedizioneDTO();
      indirizziDTO.setFinale(newIndirizzoSpedizioneDTO());
      indirizziDTO.setTransito(newIndirizzoSpedizioneDTO());
      when(anagAziendeMock.getIndirizzoSpedizioneClienteFai(anyString(), anyString())).thenReturn(indirizziDTO);
      _log.debug("Test configuration for AnagaziendeClient");
    }

    private IndirizzoSpedizioneDTO newIndirizzoSpedizioneDTO() {
      IndirizzoSpedizioneDTO indirizzoSpedizioneDTO = new IndirizzoSpedizioneDTO();

      String randString = UUID.randomUUID()
                              .toString();
      indirizzoSpedizioneDTO.setVia(randString);
      indirizzoSpedizioneDTO.setCitta(randString);
      indirizzoSpedizioneDTO.setCap(randString);
      indirizzoSpedizioneDTO.setProvincia(randString);
      indirizzoSpedizioneDTO.setPaese(randString);
      indirizzoSpedizioneDTO.setDestinatario(randString);
      indirizzoSpedizioneDTO.setFulladdress(randString);
      indirizzoSpedizioneDTO.indirizzoFai(false);

      return indirizzoSpedizioneDTO;
    }

    @Bean
    @Primary
    public IndirizzoSpedizioneOrdiniServiceImplExt newIndirizzoSpedizioneOrdiniServiceExt() {
      IndirizzoSpedizioneOrdiniServiceImplExt indirizzoSpedizioneServiceExt = new IndirizzoSpedizioneOrdiniServiceImplExt(repositoryIndirizzi,
                                                                                                                          anagAziendeMock,
                                                                                                                          "token");
      _log.info("Created IndirizzoSpedizioneOrdiniServiceImplExt for test {}", indirizzoSpedizioneServiceExt);
      return indirizzoSpedizioneServiceExt;
    }
  }

  private static final String CODICE_CLIENTE_FAI = "0046276";

  private static final String tipoDispositivoDefault = "TELEPASS_EUROPEO";

  private static final String PEDAGGIO_ITALIA     = "PEDAGGI_ITALIA";
  private static final String PEDAGGIO_POLONIA    = "PEDAGGI_POLONIA";
  private static final String PEDAGGIO_FRANCIA    = "PEDAGGI_FRANCIA";
  private static final String PEDAGGIO_PORTOGALLO = "PEDAGGI_PORTOGALLO";

  private static final String tipoServizioDefault = PEDAGGIO_ITALIA;

  private static final String veicolo1 = "AA000AA";
  // private static final String veicolo2 = "AB001AA";
  private static final String veicolo3 = "AC002AA";
  // private static final String veicolo4 = "AD003AA";
  private static final String veicolo5 = "AE004AA";
  private static final String veicolo6 = "AF005AA";

  @Autowired
  private RichiestaRepositoryExt richiestaRepositoryExt;

  @Autowired
  private RichiestaServiceExt richiestaServiceExt;

  CarrelloDTO carrello;

  List<Long> idOrderCreated = new ArrayList<>();

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    carrello = new CarrelloDTO();
    carrello.setCodiceAzienda(CODICE_CLIENTE_FAI);
    HashMap<String, String> datiAggiuntivi = new HashMap<>();
    datiAggiuntivi.put("costo", "1500");
    carrello.setDatiAggiuntivi(datiAggiuntivi);
    carrello.setTipoServizio(tipoServizioDefault);
    carrello.setUuidVeicoli(createUuidVeicoli(new ArrayList<String>(Arrays.asList(veicolo1, veicolo3, veicolo5))));
    HashMap<Integer, HashMap<String, List<String>>> mappaDispositiviVeicoli = new HashMap<>();
    HashMap<String, List<String>> mappaVeicoli = new HashMap<>();
    mappaVeicoli.put(veicolo1, new ArrayList<String>(Arrays.asList(PEDAGGIO_FRANCIA, PEDAGGIO_POLONIA)));
    mappaVeicoli.put(veicolo3, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA, PEDAGGIO_PORTOGALLO)));
    mappaVeicoli.put(veicolo6, new ArrayList<String>(Arrays.asList(PEDAGGIO_ITALIA)));
    mappaDispositiviVeicoli.put(1, mappaVeicoli);
    carrello.setDispositivi(createDispositivi(tipoDispositivoDefault, mappaDispositiviVeicoli));
  }

  @After
  public void delete() {
    for (Long id : idOrderCreated) {
      Richiesta findOne = richiestaRepositoryExt.findOne(id);
      if (findOne != null) {
        richiestaRepositoryExt.delete(id);
      }
    }
  }

  @Test
  @Transactional
  public void createRichiestaTest() throws Exception {
    List<Richiesta> richieste = richiestaServiceExt.generateAndSaveOrdineAndRichiesteByCarrelloDTO(carrello);
    assertThat(richieste).isNotNull()
                         .isNotEmpty();
  }

  private List<DispositivoCarrelloDTO> createDispositivi(String tipoDispositivo,
                                                         HashMap<Integer, HashMap<String, List<String>>> mapDispositivi) {
    List<DispositivoCarrelloDTO> dispositivi = new ArrayList<>();

    Set<Integer> keySet = mapDispositivi.keySet();
    Iterator<Integer> iterator = keySet.iterator();

    while ((iterator != null) && iterator.hasNext()) {
      Integer keyDispositivo = iterator.next();
      HashMap<String, List<String>> veicoliCarrello = mapDispositivi.get(keyDispositivo);
      DispositivoCarrelloDTO dispositivoCarrello = new DispositivoCarrelloDTO();
      dispositivoCarrello.setTipoDispositivo(tipoDispositivo);
      dispositivoCarrello.setVeicoli(createVeicoliCarrello(veicoliCarrello));
      dispositivi.add(dispositivoCarrello);
    }

    return dispositivi;
  }

  private List<VeicoloCarrelloDTO> createVeicoliCarrello(HashMap<String, List<String>> map) {
    List<VeicoloCarrelloDTO> listVeicolo = new ArrayList<>();
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while ((iterator != null) && iterator.hasNext()) {
      String keyVeicolo = iterator.next();
      List<String> listServizi = map.get(keyVeicolo);
      VeicoloCarrelloDTO veicolo = new VeicoloCarrelloDTO();
      veicolo.setUuid(keyVeicolo);
      veicolo.setNomeTipiServizio(createNomeServizio(listServizi));
      listVeicolo.add(veicolo);
    }
    return listVeicolo;
  }

  private List<String> createNomeServizio(List<String> servizi) {
    List<String> nomeServizio = new ArrayList<>();

    for (String servizio : servizi) {
      nomeServizio.add(servizio);
    }
    return nomeServizio;
  }

  private List<String> createUuidVeicoli(List<String> listVeicoli) {
    List<String> uuidVeicoli = new ArrayList<>();
    for (String veicolo : listVeicoli) {
      uuidVeicoli.add(veicolo);
    }
    return uuidVeicoli;
  }
}
