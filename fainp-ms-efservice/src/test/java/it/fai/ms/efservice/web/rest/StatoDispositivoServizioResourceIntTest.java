package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepository;
import it.fai.ms.efservice.service.StatoDispositivoServizioService;
import it.fai.ms.efservice.service.dto.StatoDispositivoServizioDTO;
import it.fai.ms.efservice.service.mapper.StatoDispositivoServizioMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.StatoDS;
/**
 * Test class for the StatoDispositivoServizioResource REST controller.
 *
 * @see StatoDispositivoServizioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class StatoDispositivoServizioResourceIntTest {

    private static final StatoDS DEFAULT_STATO = StatoDS.NON_ATTIVO;
    private static final StatoDS UPDATED_STATO = StatoDS.ATTIVO;

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATA_ATTIVAZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_ATTIVAZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATA_DISATTIVAZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_DISATTIVAZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATA_SCADENZA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_SCADENZA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_STATO_ESTERNO = "AAAAAAAAAA";
    private static final String UPDATED_STATO_ESTERNO = "BBBBBBBBBB";

    @Autowired
    private StatoDispositivoServizioRepository statoDispositivoServizioRepository;

    @Autowired
    private StatoDispositivoServizioMapper statoDispositivoServizioMapper;

    @Autowired
    private StatoDispositivoServizioService statoDispositivoServizioService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStatoDispositivoServizioMockMvc;

    private StatoDispositivoServizio statoDispositivoServizio;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StatoDispositivoServizioResource statoDispositivoServizioResource = new StatoDispositivoServizioResource(statoDispositivoServizioService);
        this.restStatoDispositivoServizioMockMvc = MockMvcBuilders.standaloneSetup(statoDispositivoServizioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StatoDispositivoServizio createEntity(EntityManager em) {
        StatoDispositivoServizio statoDispositivoServizio = new StatoDispositivoServizio()
            .stato(DEFAULT_STATO)
            .pan(DEFAULT_PAN)
            .dataAttivazione(DEFAULT_DATA_ATTIVAZIONE)
            .dataDisattivazione(DEFAULT_DATA_DISATTIVAZIONE)
            .dataScadenza(DEFAULT_DATA_SCADENZA);
        return statoDispositivoServizio;
    }

    @Before
    public void initTest() {
        statoDispositivoServizio = createEntity(em);
    }

    @Test
    @Transactional
    public void createStatoDispositivoServizio() throws Exception {
        int databaseSizeBeforeCreate = statoDispositivoServizioRepository.findAll().size();

        // Create the StatoDispositivoServizio
        StatoDispositivoServizioDTO statoDispositivoServizioDTO = statoDispositivoServizioMapper.toDto(statoDispositivoServizio);
        restStatoDispositivoServizioMockMvc.perform(post("/api/stato-dispositivo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(statoDispositivoServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the StatoDispositivoServizio in the database
        List<StatoDispositivoServizio> statoDispositivoServizioList = statoDispositivoServizioRepository.findAll();
        assertThat(statoDispositivoServizioList).hasSize(databaseSizeBeforeCreate + 1);
        StatoDispositivoServizio testStatoDispositivoServizio = statoDispositivoServizioList.get(statoDispositivoServizioList.size() - 1);
        assertThat(testStatoDispositivoServizio.getStato()).isEqualTo(DEFAULT_STATO);
        assertThat(testStatoDispositivoServizio.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testStatoDispositivoServizio.getDataAttivazione()).isEqualTo(DEFAULT_DATA_ATTIVAZIONE);
        assertThat(testStatoDispositivoServizio.getDataDisattivazione()).isEqualTo(DEFAULT_DATA_DISATTIVAZIONE);
        assertThat(testStatoDispositivoServizio.getDataScadenza()).isEqualTo(DEFAULT_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void createStatoDispositivoServizioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = statoDispositivoServizioRepository.findAll().size();

        // Create the StatoDispositivoServizio with an existing ID
        statoDispositivoServizio.setId(1L);
        StatoDispositivoServizioDTO statoDispositivoServizioDTO = statoDispositivoServizioMapper.toDto(statoDispositivoServizio);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStatoDispositivoServizioMockMvc.perform(post("/api/stato-dispositivo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(statoDispositivoServizioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StatoDispositivoServizio in the database
        List<StatoDispositivoServizio> statoDispositivoServizioList = statoDispositivoServizioRepository.findAll();
        assertThat(statoDispositivoServizioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStatoDispositivoServizios() throws Exception {
        // Initialize the database
        statoDispositivoServizioRepository.saveAndFlush(statoDispositivoServizio);

        // Get all the statoDispositivoServizioList
        restStatoDispositivoServizioMockMvc.perform(get("/api/stato-dispositivo-servizios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(statoDispositivoServizio.getId().intValue())))
            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN.toString())))
            .andExpect(jsonPath("$.[*].dataAttivazione").value(hasItem(DEFAULT_DATA_ATTIVAZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataDisattivazione").value(hasItem(DEFAULT_DATA_DISATTIVAZIONE.toString())));
    }

    @Test
    @Transactional
    public void getStatoDispositivoServizio() throws Exception {
        // Initialize the database
        statoDispositivoServizioRepository.saveAndFlush(statoDispositivoServizio);

        // Get the statoDispositivoServizio
        restStatoDispositivoServizioMockMvc.perform(get("/api/stato-dispositivo-servizios/{id}", statoDispositivoServizio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(statoDispositivoServizio.getId().intValue()))
            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN.toString()))
            .andExpect(jsonPath("$.dataAttivazione").value(DEFAULT_DATA_ATTIVAZIONE.toString()))
            .andExpect(jsonPath("$.dataDisattivazione").value(DEFAULT_DATA_DISATTIVAZIONE.toString()))
            .andExpect(jsonPath("$.dataScadenza").value(DEFAULT_DATA_SCADENZA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStatoDispositivoServizio() throws Exception {
        // Get the statoDispositivoServizio
        restStatoDispositivoServizioMockMvc.perform(get("/api/stato-dispositivo-servizios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStatoDispositivoServizio() throws Exception {
        // Initialize the database
        statoDispositivoServizioRepository.saveAndFlush(statoDispositivoServizio);
        int databaseSizeBeforeUpdate = statoDispositivoServizioRepository.findAll().size();

        // Update the statoDispositivoServizio
        StatoDispositivoServizio updatedStatoDispositivoServizio = statoDispositivoServizioRepository.findOne(statoDispositivoServizio.getId());
        updatedStatoDispositivoServizio
            .stato(UPDATED_STATO)
            .pan(UPDATED_PAN)
            .dataAttivazione(UPDATED_DATA_ATTIVAZIONE)
            .dataDisattivazione(UPDATED_DATA_DISATTIVAZIONE)
            .dataScadenza(UPDATED_DATA_SCADENZA);
        StatoDispositivoServizioDTO statoDispositivoServizioDTO = statoDispositivoServizioMapper.toDto(updatedStatoDispositivoServizio);

        restStatoDispositivoServizioMockMvc.perform(put("/api/stato-dispositivo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(statoDispositivoServizioDTO)))
            .andExpect(status().isOk());

        // Validate the StatoDispositivoServizio in the database
        List<StatoDispositivoServizio> statoDispositivoServizioList = statoDispositivoServizioRepository.findAll();
        assertThat(statoDispositivoServizioList).hasSize(databaseSizeBeforeUpdate);
        StatoDispositivoServizio testStatoDispositivoServizio = statoDispositivoServizioList.get(statoDispositivoServizioList.size() - 1);
        assertThat(testStatoDispositivoServizio.getStato()).isEqualTo(UPDATED_STATO);
        assertThat(testStatoDispositivoServizio.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testStatoDispositivoServizio.getDataAttivazione()).isEqualTo(UPDATED_DATA_ATTIVAZIONE);
        assertThat(testStatoDispositivoServizio.getDataDisattivazione()).isEqualTo(UPDATED_DATA_DISATTIVAZIONE);
        assertThat(testStatoDispositivoServizio.getDataScadenza()).isEqualTo(UPDATED_DATA_SCADENZA);
    }

    @Test
    @Transactional
    public void updateNonExistingStatoDispositivoServizio() throws Exception {
        int databaseSizeBeforeUpdate = statoDispositivoServizioRepository.findAll().size();

        // Create the StatoDispositivoServizio
        StatoDispositivoServizioDTO statoDispositivoServizioDTO = statoDispositivoServizioMapper.toDto(statoDispositivoServizio);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStatoDispositivoServizioMockMvc.perform(put("/api/stato-dispositivo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(statoDispositivoServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the StatoDispositivoServizio in the database
        List<StatoDispositivoServizio> statoDispositivoServizioList = statoDispositivoServizioRepository.findAll();
        assertThat(statoDispositivoServizioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStatoDispositivoServizio() throws Exception {
        // Initialize the database
        statoDispositivoServizioRepository.saveAndFlush(statoDispositivoServizio);
        int databaseSizeBeforeDelete = statoDispositivoServizioRepository.findAll().size();

        // Get the statoDispositivoServizio
        restStatoDispositivoServizioMockMvc.perform(delete("/api/stato-dispositivo-servizios/{id}", statoDispositivoServizio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<StatoDispositivoServizio> statoDispositivoServizioList = statoDispositivoServizioRepository.findAll();
        assertThat(statoDispositivoServizioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StatoDispositivoServizio.class);
        StatoDispositivoServizio statoDispositivoServizio1 = new StatoDispositivoServizio();
        statoDispositivoServizio1.setId(1L);
        StatoDispositivoServizio statoDispositivoServizio2 = new StatoDispositivoServizio();
        statoDispositivoServizio2.setId(statoDispositivoServizio1.getId());
        assertThat(statoDispositivoServizio1).isEqualTo(statoDispositivoServizio2);
        statoDispositivoServizio2.setId(2L);
        assertThat(statoDispositivoServizio1).isNotEqualTo(statoDispositivoServizio2);
        statoDispositivoServizio1.setId(null);
        assertThat(statoDispositivoServizio1).isNotEqualTo(statoDispositivoServizio2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StatoDispositivoServizioDTO.class);
        StatoDispositivoServizioDTO statoDispositivoServizioDTO1 = new StatoDispositivoServizioDTO();
        statoDispositivoServizioDTO1.setId(1L);
        StatoDispositivoServizioDTO statoDispositivoServizioDTO2 = new StatoDispositivoServizioDTO();
        assertThat(statoDispositivoServizioDTO1).isNotEqualTo(statoDispositivoServizioDTO2);
        statoDispositivoServizioDTO2.setId(statoDispositivoServizioDTO1.getId());
        assertThat(statoDispositivoServizioDTO1).isEqualTo(statoDispositivoServizioDTO2);
        statoDispositivoServizioDTO2.setId(2L);
        assertThat(statoDispositivoServizioDTO1).isNotEqualTo(statoDispositivoServizioDTO2);
        statoDispositivoServizioDTO1.setId(null);
        assertThat(statoDispositivoServizioDTO1).isNotEqualTo(statoDispositivoServizioDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(statoDispositivoServizioMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(statoDispositivoServizioMapper.fromId(null)).isNull();
    }
}
