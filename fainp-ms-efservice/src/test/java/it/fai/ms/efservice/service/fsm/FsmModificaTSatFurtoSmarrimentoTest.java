package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.jms.dto.telepass.TelepassEuGiustificativoAggiuntoDTO;
import it.fai.ms.common.jms.dto.telepass.TipoEventoOutbound;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.fsm.bean.telepass.TelepassEuEventiOutboundDTO;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.config.JmsSenderConfigurationTest;

/**
 * @author Luca Vassallo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, JmsSenderConfigurationTest.class })
@Ignore
public class FsmModificaTSatFurtoSmarrimentoTest {

  private final static String CODICE_CLIENTE_FAI = "0073935";

  @Autowired
  private FsmRichiestaMappingService fsmMap;

  private AbstractFsmRichiesta fsm;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  private Richiesta richiesta;

  private TelepassEuEventiOutboundDTO telepassEuEventiOutboundDTO = new TelepassEuEventiOutboundDTO();

  @Before // Set-up
  public void setUp() throws Exception {

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    OrdineCliente ordineCliente = new OrdineCliente();
    ordineCliente.setIdentificativo("ABCD1234567890");
    richiesta = new Richiesta();
    richiesta.setOrdineCliente(ordineCliente);
    ordineCliente.setRichiestas(new HashSet<>(Arrays.asList(richiesta)));
    richiesta.setId(RandomUtils.nextLong());
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TSAT);
    richiesta.getOrdineCliente()
             .setClienteAssegnatario(clienteFaiRepo.findOneByCodiceCliente(CODICE_CLIENTE_FAI));
    richiesta.setUltimaNota(null);

    telepassEuEventiOutboundDTO = new TelepassEuEventiOutboundDTO();
    telepassEuEventiOutboundDTO.setIdentificativoOrdineCliente(richiesta.getOrdineCliente()
                                                                        .getIdentificativo());
    fsm = fsmMap.getFSM(FsmType.MOD_TSAT_FURTO_SMARRIMENTO.fsmName());
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_01() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);

    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender()).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_01_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(12)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_02() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.FURTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_FURTO);
    verify(JmsSenderConfigurationTest.getJmsSender()).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_02_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.FURTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_FURTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(2)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_03() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(3)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_03_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(10)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_04() throws Exception {
    assertionTrue(TipoRichiesta.FURTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_FURTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(2)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento001_04_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.FURTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertThat(JmsSenderConfigurationTest.getJmsSender()).isNotNull();
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_FURTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender()).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_01() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE, StatoRichiesta.ESAME_DENUNCIA);
    TelepassEuGiustificativoAggiuntoDTO telepassEuGiustificativoAggiuntoDTO = new TelepassEuGiustificativoAggiuntoDTO();
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(13)).publishGiustificativoMessage(telepassEuGiustificativoAggiuntoDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_01_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    ;
    assertChangeStateFsmTe(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE, StatoRichiesta.ESAME_DENUNCIA);
    TelepassEuGiustificativoAggiuntoDTO telepassEuGiustificativoAggiuntoDTO = new TelepassEuGiustificativoAggiuntoDTO();
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(6)).publishGiustificativoMessage(telepassEuGiustificativoAggiuntoDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_02() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO, StatoRichiesta.EVASO_POST_ALLINEAMENTO);
    TelepassEuGiustificativoAggiuntoDTO telepassEuGiustificativoAggiuntoDTO = new TelepassEuGiustificativoAggiuntoDTO();
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(14)).publishGiustificativoMessage(telepassEuGiustificativoAggiuntoDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_02_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO, StatoRichiesta.EVASO_POST_ALLINEAMENTO);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atLeast(2)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_03() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_RICEVUTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(5)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_03_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_RICEVUTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(11)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_04() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_RICEVUTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(6)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento002_04_sostituzione() throws Exception {
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, null, RichiestaEvent.INITIAL, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE, StatoRichiesta.ESAME_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(11)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_01() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(7)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_01_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(9)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_02() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_CORRETTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(8)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_02_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_CORRETTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(8)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_03() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(9)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_03_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(7)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_04() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_CORRETTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(10)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_04_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_GIUSTIFICATIVO_CARICATO_UTENTE, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_CORRETTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(5)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_05() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO, StatoRichiesta.EVASO_POST_ALLINEAMENTO);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(11)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_05_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MS_DENUNCIA_SU_ALLINEAMENTO, StatoRichiesta.EVASO_POST_ALLINEAMENTO);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(12)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_06() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_RICEVUTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(12)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  @Test
  public void FsmModificaTSatFurtoSmarrimento003_06_sostituzione() throws Exception {
    String documento = UUID.randomUUID()
                           .toString();
    assertionTrue(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE, documento, RichiestaEvent.INITIAL, StatoRichiesta.ESAME_DENUNCIA);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_ERRATO, StatoRichiesta.ASSENZA_ALLEGATO);
    assertChangeStateFsmTe(RichiestaEvent.MU_ALLEGATO_RICEVUTO, StatoRichiesta.EVASO_CON_DENUNCIA);
    // telepassEuEventiOutboundDTO.setTipoEventoOutbound(TipoEventoOutbound.MODIFICA_EVENTO_SMARRIMENTO_CON_SOSTITUZIONE);
    verify(JmsSenderConfigurationTest.getJmsSender(), atMost(10)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);
  }

  private void assertionTrue(TipoRichiesta tipoRichiesta, String uuidDocumento, RichiestaEvent event,
                             StatoRichiesta statoFinale) throws Exception {
    richiesta.setId(RandomUtils.nextLong());
    richiesta.setTipo(tipoRichiesta);
    richiesta.setUuidDocumento(uuidDocumento);
    assertChangeStateFsmTe(event, statoFinale);
  }

  private void assertChangeStateFsmTe(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsm.isAvailableCommand(event, richiesta);
    assertTrue(availableCommand);

    fsm.executeCommandToChangeState(event, richiesta);
    StatoRichiesta fsmStatus = richiesta.getStato();
    assertEquals(state.name(), fsmStatus.name());
  }

}
