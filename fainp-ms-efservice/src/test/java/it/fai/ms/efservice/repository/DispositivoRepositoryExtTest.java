package it.fai.ms.efservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionTemplate;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;

@Ignore("Fail Setup")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DispositivoRepositoryExtTest {
  private static final TransactionDefinition REQUIRES_NEW_TRANSACTION = new DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
  private static String                      hwDeviceType             = "AS";

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepo;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepo;

  @Autowired
  private PlatformTransactionManager transactionManager;
  private Dispositivo                dispositivo1;
  private Dispositivo                dispositivo2;

  @Before
  public void setUp() {
    TipoDispositivo telepassIta = tipoDispositivoRepo.findOne(1L);

    dispositivo1 = new Dispositivo().seriale("TEST-QUERY1")
      .dataModificaStato(Instant.now())
      .stato(StatoDispositivo.IN_DEPOSITO)
      .modalitaSpedizione(ModalitaSpedizione.A_FAI)
      .tipoDispositivo(telepassIta)
      .tipoHardware(null);

    dispositivoRepo.save(dispositivo1);

    dispositivo2 = new Dispositivo().seriale("TEST-QUERY2")
      .dataModificaStato(Instant.now())
      .stato(StatoDispositivo.IN_DEPOSITO)
      .modalitaSpedizione(ModalitaSpedizione.A_FAI)
      .tipoDispositivo(telepassIta)
      .tipoHardware("AS");

    new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(status -> {
      dispositivoRepo.save(dispositivo2);
      return null;
    });
  }

  @After
  public void afterTest() {
    dispositivoRepo.delete(dispositivo1);
    dispositivoRepo.delete(dispositivo2);
  }


  @Test
  @Transactional
  public void testFindOneByStatoAndTipoDispositivo_nome() {
    Optional<Dispositivo> optDispositivo = dispositivoRepo.findTop1ByStatoAndTipoDispositivo_nome(StatoDispositivo.IN_DEPOSITO,
                                                                                                  TipoDispositivoEnum.TELEPASS_ITALIANO);
    boolean               present        = optDispositivo.isPresent();
    assertThat(present).isTrue();

    Dispositivo dispositivo = optDispositivo.get();
    dispositivoRepo.save(dispositivo.stato(StatoDispositivo.INIZIALE));
  }

  @Test
  @Transactional
  public void testFindOneByStatoAndTipoDispositivo_nomeAndTipoHardware() {
    List<Dispositivo> optDispositivo = dispositivoRepo
      .findByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(StatoDispositivo.IN_DEPOSITO, TipoDispositivoEnum.TELEPASS_ITALIANO,
                                                            hwDeviceType);
    boolean           present        = optDispositivo.isEmpty();
    assertThat(present).isFalse();
  }

  @Test
  @Transactional
  public void testFindOneByStatoAndTipoDispositivo_nomeAndTipoHardwareNotASIsPresent() {
    List<Dispositivo> optDispositivo = dispositivoRepo
      .findByStatoAndTipoDispositivo_nomeAndTipoHardwareNot(StatoDispositivo.IN_DEPOSITO, TipoDispositivoEnum.TELEPASS_ITALIANO,
                                                            hwDeviceType);
    Dispositivo       dispositivo    = optDispositivo.get(0);
    String            tipoHardware   = dispositivo.getTipoHardware();
    assertThat(tipoHardware).isNotEqualTo(hwDeviceType);
  }

}
