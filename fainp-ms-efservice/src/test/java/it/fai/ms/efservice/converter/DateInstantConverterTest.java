package it.fai.ms.efservice.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

public class DateInstantConverterTest {

  private DateInstantConverter dateInstantConverter;

  @Before
  public void setUp() throws Exception {
    dateInstantConverter = new DateInstantConverter();
  }

  @Test
  public void testConvertToDatabaseColumn() {
    Date date = dateInstantConverter.convertToDatabaseColumn(newInstant());

    assertThat(date).withDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
                    .isEqualTo("1978-04-15T12:12:12+00:00");
  }

  @Test
  public void testConvertToDatabaseColumnWhenInstantIsNull() {
    Date date = dateInstantConverter.convertToDatabaseColumn(null);

    assertThat(date).isNull();
  }

  @Test
  public void testConvertToEntityAttribute() {
    Instant instant = dateInstantConverter.convertToEntityAttribute(newDate());

    assertThat(instant.toString()).isEqualTo("1978-04-15T12:12:12Z");
  }

  @Test
  public void testConvertToEntityAttributeWhenDateIsNull() {
    Instant instant = dateInstantConverter.convertToEntityAttribute(null);

    assertThat(instant).isNull();
  }

  private Date newDate() {
    Calendar cal = GregorianCalendar.getInstance(TimeZone.getTimeZone(ZoneId.of("UTC")));
    cal.set(1978, 3, 15, 12, 12, 12);
    cal.set(Calendar.MILLISECOND, 0);
    Date date = cal.getTime();
    return date;
  }

  private Instant newInstant() {
    return Instant.parse("1978-04-15T12:12:12Z");
  }

}
