package it.fai.ms.efservice.consumer.web.rest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

public class VehicleMessageListenerRestTest {

  private VehicleMessageListenerRest messageListener;
  private List<VehicleConsumer>      consumers = new LinkedList<>();

  @Before
  public void setUp() throws Exception {
    VehicleConsumer mockVehicleCosumer1 = mock(VehicleConsumer.class);
    VehicleConsumer mockVehicleCosumer2 = mock(VehicleConsumer.class);
    consumers.add(mockVehicleCosumer1);
    consumers.add(mockVehicleCosumer2);
    messageListener = new VehicleMessageListenerRest(consumers);
  }

  @Test
  @Ignore
  public void testReceive() {
    final VehicleMessage vehicleMessage = new VehicleMessage();
    messageListener.receive(vehicleMessage);

    verify(consumers.get(0)).consume(vehicleMessage);
    verify(consumers.get(1)).consume(vehicleMessage);
  }

}
