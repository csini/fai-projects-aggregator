package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class OrdineClienteServiceExtTest {

  @Autowired
  private OrdineClienteServiceExt ordineClienteServiceExt;
  
  @Autowired
  private OrdineClienteRepositoryExt ordineClienteRepoExt;
  
  @Autowired
  private ClienteFaiRepository clienteFaiRepo;
  
  @Autowired IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneRepo;
  
  private static final String CODICE_CLIENTE = "0046348";
  
  private OrdineCliente ordineCliente;
  
  private IndirizzoSpedizioneOrdini indirizzo;

  
  @Before
  public void setUp() throws Exception {
    
    ordineCliente = new OrdineCliente();
    ClienteFai clienteFaiByCodeClient = clienteFaiRepo.findOneByCodiceCliente(CODICE_CLIENTE);
    ordineCliente.clienteAssegnatario(clienteFaiByCodeClient);
    ordineCliente.setNumeroOrdine("OC-0001");
    
    indirizzo = new IndirizzoSpedizioneOrdini().via("::test::").paese("::paese::").provincia("::provincia::").fai(true);
    indirizzo = indirizzoSpedizioneRepo.saveAndFlush(indirizzo);
    ordineCliente.setIndirizzoDiTransito(indirizzo);
    ordineCliente.setIndirizzoDestinazioneFinale(indirizzo);
    ordineCliente = ordineClienteServiceExt.save(ordineCliente);
  }
  
  @After
  public void delete() throws Exception {
    ordineClienteRepoExt.delete(ordineCliente);
    indirizzoSpedizioneRepo.delete(indirizzo);
  }
  
  @Test
  public void queryLoadEagerOrdineClienteByIdentificativoTest() {
    OrdineCliente newOrdineCliente = ordineClienteServiceExt.findByIdentificativoOrdineCliente(ordineCliente.getIdentificativo());
    assertThat(newOrdineCliente).isNotNull();
    assertThat(newOrdineCliente.getClienteAssegnatario()).isNotNull();
    assertThat(newOrdineCliente.getClienteAssegnatario().getCodiceCliente()).isEqualTo(CODICE_CLIENTE);
  }
  
}
