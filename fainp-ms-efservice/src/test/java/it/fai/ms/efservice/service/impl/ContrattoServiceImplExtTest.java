package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.service.dto.ContrattoDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ContrattoServiceImplExtTest {
  
  @Autowired
  ContrattoServiceImplExt contrattoService;

  @Autowired
  ContrattoRepositoryExt contrattoRepository;

  @Autowired
  ClienteFaiRepository clienteFaiRepository;
  
  @Autowired
  ProduttoreRepository produttoreRepository;
  
  String SIMEONI = "0046348";
  
  @Test
  public void updateCodiceContratto_success() throws Exception {
    long sizeBefore = contrattoRepository.count();

    String oldCode = RandomString(7);
    String newCode = RandomString(7);
    
    ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(SIMEONI);
    Produttore produttore = produttoreRepository.findOne(1L);

    Contratto c = new Contratto();
    c.setCodContrattoCliente(oldCode);
    c.setClienteFai(clienteFai);
    c.setProduttore(produttore);    
    c.setDataModificaStato(Instant.now());
    c.setStato(StatoContratto.ATTIVO);

    c = contrattoRepository.saveAndFlush(c);
    assertThat(sizeBefore + 1).isEqualTo(contrattoRepository.count());
    
    ContrattoDTO dto = contrattoService.updateCodiceContratto(c.getIdentificativo(), newCode);

    assertThat(sizeBefore + 1).isEqualTo(contrattoRepository.count());

    assertThat(dto.getId()).isEqualTo(c.getId());
    assertThat(dto.getCodContrattoCliente()).isEqualTo(newCode);
    assertThat(dto.getClienteFaiId()).isEqualTo(clienteFai.getId());
    assertThat(dto.getProduttoreId()).isEqualTo(produttore.getId());
    assertThat(dto.getStato()).isEqualTo(StatoContratto.ATTIVO);
    //assertThat(dto.getDataModificaStato()).isEqualTo(c.getDataModificaStato());
    
    contrattoRepository.delete(dto.getId());
    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
  }

  @Test
  public void updateCodiceContratto_failed() throws Exception {
    ContrattoDTO dto = contrattoService.updateCodiceContratto("FAI-SERVICE-SPA", RandomString(10));
    assertThat(dto).isNull();
  }
  
  @Test
  public void existsByCodContrattoCliente() throws Exception {
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(SIMEONI);
    Produttore produttore = produttoreRepository.findOne(1L);

    Contratto c = new Contratto();
    c.setCodContrattoCliente(RandomString(7));
    c.setClienteFai(clienteFai);
    c.setProduttore(produttore);    
    c.setDataModificaStato(Instant.now());
    c.setStato(StatoContratto.ATTIVO);

    c = contrattoRepository.saveAndFlush(c);
    assertThat(sizeBefore + 1).isEqualTo(contrattoRepository.count());

    //Simulate check for contract on same cliente (when try to modify an owned contract with same contract number)
    Boolean flag = contrattoService.existsCodContrattoClienteOnDifferentClienteFai(clienteFai.getId(), c.getCodContrattoCliente(), produttore.getId());
    assertThat(flag).isFalse();

    //Simulate check for contract on different cliente (when modify an owned contract with different contract number)
    flag = contrattoService.existsCodContrattoClienteOnDifferentClienteFai(-1l, c.getCodContrattoCliente(), produttore.getId());
    assertThat(flag).isTrue();
   
    contrattoRepository.delete(c.getId());
    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());      

    flag = contrattoService.existsCodContrattoClienteOnDifferentClienteFai(clienteFai.getId(), c.getCodContrattoCliente(), produttore.getId());
    assertThat(flag).isFalse();
  }
  
  @Before
  public void setUp() {
    
  }
  
  @After
  public void after() {
    
  }
  
  static String RandomString(int count) {
    return RandomStringUtils.random(count, true, true).toUpperCase();
  }  
    
}
