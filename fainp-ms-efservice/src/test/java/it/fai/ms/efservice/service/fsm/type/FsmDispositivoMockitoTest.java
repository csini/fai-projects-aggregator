package it.fai.ms.efservice.service.fsm.type;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmDispositivoCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

@RunWith(MockitoJUnitRunner.class)
public class FsmDispositivoMockitoTest {

  private FsmDispositivo fsmDispositivo;

  @Mock
  private FsmDispositivoCacheService mockFsmDispositivoCacheService;

  @Mock
  private StateMachineDispositivoBuilder mockStateMachineBuilder;

  @Mock
  private StateMachine<StatoDispositivo, DispositivoEvent> mockStateMachine;

  @Before
  public void setUp() throws Exception {
    when(mockStateMachineBuilder.build()).thenReturn(mockStateMachine);
    fsmDispositivo = new FsmDispositivo(mockStateMachineBuilder, mockFsmDispositivoCacheService);

    List<Transition<StatoDispositivo, DispositivoEvent>> newMockTransitions = newMockTransitions();
    when(mockStateMachine.getTransitions()).thenReturn(newMockTransitions);
    when(mockStateMachine.sendEvent(any(Message.class))).thenReturn(true);
  }

  @Test
  public void testExecuteCommandToChangeStateDispositivoEventDispositivoString() throws Exception {
    String deviceId = String.valueOf(newDevice().getId());
    fsmDispositivo.executeCommandToChangeState(newDeviceEvent(), newDevice(), newNote());

    then(mockStateMachine).should()
                          .sendEvent(any(Message.class));
    then(mockFsmDispositivoCacheService).should()
                                        .persist(eq(mockStateMachine), anyString());
  }

  private Dispositivo newDevice() {
    Dispositivo device = new Dispositivo();
    device.setId(666L);
    device.setStato(StatoDispositivo.INIZIALE);
    return device;
  }

  private DispositivoEvent newDeviceEvent() {
    return DispositivoEvent.ACCETTATO;
  }

  private Transition<StatoDispositivo, DispositivoEvent> newMockTransition() {
    Transition<StatoDispositivo, DispositivoEvent> mockTransition = mock(Transition.class);
    State<StatoDispositivo, DispositivoEvent> mockState = mock(State.class);
    Trigger<StatoDispositivo, DispositivoEvent> mockTrigger = mock(Trigger.class);

    when(mockState.getId()).thenReturn(StatoDispositivo.INIZIALE);
    when(mockTransition.getSource()).thenReturn(mockState);
    when(mockTransition.getTrigger()).thenReturn(mockTrigger);
    when(mockTrigger.getEvent()).thenReturn(newDeviceEvent());
    return mockTransition;
  }

  private List<Transition<StatoDispositivo, DispositivoEvent>> newMockTransitions() {
    return Arrays.asList(newMockTransition());
  }

  private String newNote() {
    return "::note::";
  }

}
