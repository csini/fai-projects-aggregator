package it.fai.ms.efservice.service.fsm.type.libert;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.libert.FsmInoltroLiberTConfig;
import it.fai.ms.efservice.service.fsm.config.libert.FsmModificaLiberTFurtoSmarrimentoConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaLiberTFurtoSmarrimentoTest {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Configuration
  public static class LiberTModificaFurtoSmarrimentoConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    private static FsmSenderToQueue senderFsmService = mock(FsmSenderToQueue.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroLiberTConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmModificaLiberTFurtoSmarrimentoConfig newLiberTModificaFurtoSmarrimentoConfiguration() {
      FsmModificaLiberTFurtoSmarrimentoConfig fsmModificaLiberTFurtoSmarrimento = new FsmModificaLiberTFurtoSmarrimentoConfig(senderFsmService);
      _log.info("Created FsmModificaLiberTFurtoSmarrimentoConfig for test {}", fsmModificaLiberTFurtoSmarrimento);
      return fsmModificaLiberTFurtoSmarrimento;
    }
  }

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiestaFurtoConSostituzione       = null;
  private Richiesta   richiestaFurto                      = null;
  private Richiesta   richiestaSmarrimentoConSostituzione = null;
  private Richiesta   richiestaSmarrimento                = null;
  private Dispositivo dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    richiestaFurtoConSostituzione = newMockRichiesta(TipoRichiesta.FURTO_CON_SOSTITUZIONE);
    richiestaFurto = newMockRichiesta(TipoRichiesta.FURTO);
    richiestaSmarrimentoConSostituzione = newMockRichiesta(TipoRichiesta.SMARRIMENTO_CON_SOSTITUZIONE);
    richiestaSmarrimento = newMockRichiesta(TipoRichiesta.SMARRIMENTO);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private Dispositivo newDispositivo() {
    Dispositivo d = new Dispositivo();

    d.setStato(StatoDispositivo.IN_DEPOSITO);
    d.setTipoDispositivo(newTipoDispositivo());
    d.setId(1L);

    return d;
  }

  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo d = new TipoDispositivo();

    d.setId(10000001L);
    d.setModalitaSpedizione(ModalitaSpedizione.DA_DEPOSITO_FAI);
    d.setNome(TipoDispositivoEnum.LIBER_T);

    return d;
  }

  private Richiesta newMockRichiesta(TipoRichiesta tipoRichiesta) {
    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = newDispositivo();

    Richiesta richiesta = new Richiesta().tipo(tipoRichiesta)
                                         .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T)
                                         .tipoDispositivo(newTipoDispositivo())
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmInoltroLiberTFurtoConSostituzione_attivoPerModifica_evaso() throws Exception {
    if (richiestaFurtoConSostituzione.getStato()
                                     .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T)) {
      switchStateAccettatoProntoPerInoltroToDaInoltrare(richiestaFurtoConSostituzione);
      assertThat(richiestaFurtoConSostituzione.getStato()).isEqualTo(StatoRichiesta.EVASO);
    }
  }

  @Test
  public void FsmInoltroLiberTFurto_attivoPerModifica_evaso() throws Exception {
    if (richiestaFurto.getStato()
                      .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T)) {
      switchStateAccettatoProntoPerInoltroToDaInoltrare(richiestaFurto);
      assertThat(richiestaFurto.getStato()).isEqualTo(StatoRichiesta.EVASO);
    }
  }

  @Test
  public void FsmInoltroLiberTSmarrimentoConSostituzione_attivoPerModifica_evaso() throws Exception {
    if (richiestaSmarrimentoConSostituzione.getStato()
                                           .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T)) {
      switchStateAccettatoProntoPerInoltroToDaInoltrare(richiestaSmarrimentoConSostituzione);
      assertThat(richiestaSmarrimentoConSostituzione.getStato()).isEqualTo(StatoRichiesta.EVASO);
    }
  }

  @Test
  public void FsmInoltroLiberTSmarrimento_attivoPerModifica_evaso() throws Exception {
    if (richiestaSmarrimento.getStato()
                            .equals(StatoRichiesta.ATTIVO_PER_MODIFICA_LIBER_T)) {
      switchStateAccettatoProntoPerInoltroToDaInoltrare(richiestaSmarrimento);
      assertThat(richiestaSmarrimento.getStato()).isEqualTo(StatoRichiesta.EVASO);
    }
  }

  private void switchStateAccettatoProntoPerInoltroToDaInoltrare(Richiesta richiesta) throws Exception {
    Mockito.doNothing()
           .when(LiberTModificaFurtoSmarrimentoConfiguration.senderFsmService)
           .sendMessageForChangeStatoDispositivo(any(Dispositivo.class), any(DispositivoEvent.class));

    FsmCommand fsmCommand = null;
    switch (richiesta.getTipo()) {
    case FURTO:
      fsmCommand = FsmCommand.CMD_FURTO_NO_SOST_LIBER_T;
      break;
    case FURTO_CON_SOSTITUZIONE:
      fsmCommand = FsmCommand.CMD_FURTO_SOST_LIBER_T;
      break;
    case SMARRIMENTO:
      fsmCommand = FsmCommand.CMD_SMARRIMENTO_NO_SOST_LIBER_T;
      break;
    case SMARRIMENTO_CON_SOSTITUZIONE:
      fsmCommand = FsmCommand.CMD_SMARRIMENTO_SOST_LIBER_T;
      break;
    default:
      log.error("Tipologia Richiesta [" + richiesta.getTipo()
                                                   .name()
                + "] is not managed");
      break;
    }

    FsmModificaLiberTFurtoSmarrimento fsmModificaLiberTFurtoSmarrimento = (FsmModificaLiberTFurtoSmarrimento) fsmFactory.getFsm(fsmCommand);

    richiesta = fsmModificaLiberTFurtoSmarrimento.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  }

}
