package it.fai.ms.efservice.wizard.consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.service.WizardVehicleService;

@RunWith(MockitoJUnitRunner.class)
public class WizardVehicleConsumerImplTest {

  private WizardVehicleConsumerImpl     consumer;
  @Captor
  private ArgumentCaptor<WizardVehicle> captorWizardVehicle;
  @Mock
  private WizardVehicleService          mockWizardVehicleService;

  @Before
  public void setUp() throws Exception {
    consumer = new WizardVehicleConsumerImpl(mockWizardVehicleService);
  }

  @Test
  public void testConsume() {
    consumer.consume(newMessageObject());

    verify(mockWizardVehicleService).addVehicle(captorWizardVehicle.capture());
    verifyNoMoreInteractions(mockWizardVehicleService);

    final WizardVehicle wizardVehicle = captorWizardVehicle.getValue();
    assertThat(wizardVehicle.getId()).isEqualTo(new WizardVehicleId("::id::"));
    assertThat(wizardVehicle.getEuroClass()).isEqualTo("::euroClass::");
    assertThat(wizardVehicle.getLicensePlate()).isEqualTo("::licensePlate::");
    assertThat(wizardVehicle.getState()).isEqualTo("::state::");
    assertThat(wizardVehicle.getType()).isEqualTo("::type::");
  }

  private VehicleMessage newMessageObject() {
    VehicleMessage vehicleMessage = new VehicleMessage();
    vehicleMessage.setUuid("::id::");
    vehicleMessage.setEuroClass("::euroClass::");
    vehicleMessage.setLicensePlate("::licensePlate::");
    vehicleMessage.setState("::state::");
    vehicleMessage.setType("::type::");
    return vehicleMessage;
  }

}
