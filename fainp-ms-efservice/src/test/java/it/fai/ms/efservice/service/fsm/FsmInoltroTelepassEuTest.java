/**
 *
 */
package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.time.Instant;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.dto.FsmCommandDTO;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.OrdineClienteRepository;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.consumer.FsmChangeStatusConsumer;
import it.fai.ms.efservice.service.jms.util.JmsSenderQueueMessageService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmInoltroTelepassEuTest {

  private final String ERROR_PREFIX = "Exception DEVICE_LOSS_";

  private final static String CODICE_AZIENDA = "0073935";

  private boolean isNewContract = false;

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtils;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  @Autowired
  private RichiestaRepository richiestaRepo;

  @Autowired
  private DispositivoRepositoryExt deviceRepoExt;

  @Autowired
  private FsmChangeStatusConsumer fsmChangeStatusConsumer;

  @Autowired
  private ContrattoServiceExt contrattoService;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private OrdineClienteRepository ordineClienteRepo;
  
  @MockBean
  JmsSenderQueueMessageService jmsSenderQueueService;

  private Richiesta richiesta;

  private Dispositivo device;

  private Contratto contract;

  private FsmCommandDTO dto;

  private long timestamp = 0L;

  @Before
  public void setUp() throws Exception {
    
    TipoDispositivo tipoDispositivo = tipoDispositivoRepository.findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    newDevice(tipoDispositivo);
    richiesta = newRichiesta(tipoDispositivo);

    device = deviceRepoExt.save(device);
    richiesta.addDispositivo(device);
    richiesta = fsmRichiestaUtils.saveAndFlushRichiesta(richiesta);
    device = deviceRepoExt.save(device.addRichiesta(richiesta));

    timestamp = System.currentTimeMillis();
    String errorMessage = ERROR_PREFIX + timestamp;
    dto = new FsmCommandDTO().identificativo(richiesta.getIdentificativo())
                             .operazione(RichiestaEvent.MS_ORDINE_SOSPESO_EXCEPTION.name())
                             .errorMessage(errorMessage);
  }

  @After
  public void deleteRichiesta() {

    ordineClienteRepo.findAll()
                     .stream()
                     .filter(o -> o.getNumeroOrdine()
                                   .equals("ORD-" + timestamp))
                     .findFirst()
                     .ifPresent(o -> ordineClienteRepo.delete(o));
    
    richiesta.setDispositivos(null);
    richiesta.setContratto(null);
    richiesta = fsmRichiestaUtils.saveAndFlushRichiesta(richiesta);
    
    device.setRichiestas(null);
    device.setContratto(null);
    device = deviceRepoExt.save(device);

    if (isNewContract) {
      contrattoService.delete(contract);
    }

    richiestaRepo.delete(richiesta.getId());
    deviceRepoExt.delete(device);
  }

  @Test
  public void FsmInoltroTelepassEuropeo_change_status_managed_exception() {
    try {
      richiesta = fsmRichiestaUtils.changeStatusToRichiesta(richiesta, RichiestaEvent.INITIAL.name(), "TEST di integrazione", "");
    } catch (FsmExecuteCommandException e) {
      throw new RuntimeException(e);
    }
    assertThat(richiesta).isNotNull()
                         .extracting(r -> tuple(r.getStato()))
                         .containsExactly(tuple(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA));

    fsmRichiestaUtils.saveAndFlushRichiesta(richiesta);
    
    verify(jmsSenderQueueService).sendCommandToFsm(any(), any(), any());

    try {
      fsmChangeStatusConsumer.consume(dto);
    } catch (FsmExecuteCommandException e) {
      throw new RuntimeException(e);
    }

    Richiesta request = richiestaRepo.findOne(richiesta.getId());
    assertThat(request).isNotNull()
                       .extracting(r -> tuple(r.getStato()))
                       .containsExactly(tuple(StatoRichiesta.ORDINE_SOSPESO));
  }

  private Richiesta newRichiesta(TipoDispositivo tipoDispositivo) {
    Richiesta richiesta = new Richiesta().contratto(newContratto(tipoDispositivo))
                                         .ordineCliente(newOrdineCliente());
    richiesta.setTipo(TipoRichiesta.FURTO);
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    richiesta.setTipoDispositivo(tipoDispositivo);
    richiesta.setData(Instant.now());
    return richiesta;
  }

  private void newDevice(TipoDispositivo tipoDispositivo) {
    long currentTimeMillis = System.currentTimeMillis();
    device = new Dispositivo().seriale(currentTimeMillis + "0")
                              .dataPrimaAttivazione(Instant.now())
                              .stato(StatoDispositivo.ATTIVO);
  }

  private OrdineCliente newOrdineCliente() {
    OrdineCliente ordineCliente = new OrdineCliente().clienteAssegnatario(newClienteFai())
                                                     .numeroOrdine("ORD-" + timestamp);
    return ordineClienteRepo.save(ordineCliente);
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }

  private Contratto newContratto(TipoDispositivo tipoDispositivo) {
    Produttore produttore = tipoDispositivo.getProduttore();
    Optional<Contratto> optContratto = contrattoService.findContrattoByProduttoreAndCodiceAzienda(produttore, CODICE_AZIENDA);

    if (!optContratto.isPresent()) {
      contract = contrattoService.createContrattoByProduttoreAndClienteFai(produttore, newClienteFai());
      isNewContract = true;
    } else {
      contract = optContratto.get();
    }

    return contract;
  }

}
