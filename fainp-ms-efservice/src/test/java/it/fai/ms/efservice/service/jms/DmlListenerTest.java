package it.fai.ms.efservice.service.jms;

import static it.fai.ms.common.jms.AndesJmsListenerEndpointRegistrar.isListenerRegistered;
import static org.mockito.BDDMockito.given;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.AssertionErrors;

import it.fai.ms.common.dml.AbstractDml;
import it.fai.ms.common.dml.AbstractDmlDto;
import it.fai.ms.common.dml.AbstractJmsDmlListener;
import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.JmsTopicNames;

public abstract class DmlListenerTest<E extends AbstractDmlDto,T extends AbstractDml> {

  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  E dmldto;

  @Mock
  protected ObjectMessage message;

  protected final JmsTopicNames TOPIC_NAME;
  protected final JmsQueueNames QUEUE_NAME;


  protected DmlListenerTest(JmsTopicNames jmsTopicNames) {
    TOPIC_NAME = jmsTopicNames;
    QUEUE_NAME = null;
  }

  protected DmlListenerTest(JmsQueueNames jmsQueueNames) {
    TOPIC_NAME = null;
    QUEUE_NAME = jmsQueueNames;
  }

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testIfListenerIsRegistred() throws JMSException {
    boolean condition = TOPIC_NAME != null ? isListenerRegistered(TOPIC_NAME) : isListenerRegistered(QUEUE_NAME);
    String msg = TOPIC_NAME != null ? "Topic " + TOPIC_NAME : "Queue " + QUEUE_NAME;
    AssertionErrors.assertTrue(msg + " is not registred: check JmsConfiguration", condition);
  }

  protected E getDMLDTO() {
    return dmldto;
  }

  @Test
  public void testIfDmlSaveReceivedMessages() throws JMSException {

    dmldto = createDMLDTO();

    given(message.getObject()).willReturn(dmldto);
    getListener().onMessage(message);

    T persistedEntity = getRepository().findByDmlUniqueIdentifier(dmldto.getDmlUniqueIdentifier());

    doAssertionOnPersistedEntity(persistedEntity);
  }

  protected abstract void doAssertionOnPersistedEntity(T persistedEntity);

  protected abstract InterfaceDmlRepository<T> getRepository() ;

  protected abstract AbstractJmsDmlListener<E, T> getListener() ;

  protected abstract E createDMLDTO();

}
