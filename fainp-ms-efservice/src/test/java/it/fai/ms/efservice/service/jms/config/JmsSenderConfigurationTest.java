package it.fai.ms.efservice.service.jms.config;

import static org.mockito.Mockito.mock;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@Configuration
public class JmsSenderConfigurationTest {

  private Logger _log = LoggerFactory.getLogger(getClass());

  public static JmsTopicSenderService jmsTopicSenderService = mock(JmsTopicSenderService.class);

  @PostConstruct
  public void init() {
    _log.debug("Test configuration for FsmSenderToQueue");
  }

  @Bean
  @Primary
  public FsmSenderToQueue newFsmSenderToQueue() {
    FsmSenderToQueue senderFsmUtil = new FsmSenderToQueue(jmsTopicSenderService);
    _log.info("Created FsmSenderToQueue for test {}", senderFsmUtil);
    return senderFsmUtil;
  }

  public static JmsTopicSenderService getJmsSender() {
    return jmsTopicSenderService;
  }

}
