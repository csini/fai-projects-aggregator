package it.fai.ms.efservice.service.jms.consumer.device;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.ObuNumber;
import it.fai.ms.common.jms.efservice.ObuType;
import it.fai.ms.common.jms.efservice.ObuTypeId;
import it.fai.ms.common.jms.efservice.message.device.DeviceReturnedMessage;
import it.fai.ms.common.jms.efservice.message.device.ObuDeviceInfo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.OrdineClienteRepositoryExt;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class DeviceReturnedConsumerTest {

  private static final String SIMEONI_CODE_CLIENT = "0046348";

  @Autowired
  private OrdineClienteRepositoryExt ordineClienteRepo;

  @Autowired
  private RichiestaServiceExt richiestaServiceExt;

  @Autowired
  private DispositivoServiceExt deviceServiceExt;

  @Autowired
  private TipoDispositivoServiceExt deviceTypeService;

  @Autowired
  private ContrattoServiceExt contrattoService;

  @Autowired
  private ClienteFaiServiceExt clienteFaiService;

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtil;

  @Autowired
  private DeviceReturnedConsumer consumer;

  private Richiesta richiesta;

  private Dispositivo dispositivo;

  private TipoDispositivo tipoDispositivo;

  private AbstractFsmRichiesta fsm;

  private ClienteFai clienteFai;

  private Contratto contratto;

  private OrdineCliente orderClient;

  @Before
  public void setUp() throws Exception {
    String seriale = RandomStringUtils.randomNumeric(6) + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

    TipoDispositivoEnum deviceType = TipoDispositivoEnum.TELEPASS_EUROPEO;
    clienteFai = clienteFaiService.findByCodiceCliente(SIMEONI_CODE_CLIENT);
    tipoDispositivo = deviceTypeService.findOneByNome(deviceType);

    Optional<Contratto> optContratto = contrattoService.findContrattoByProduttoreAndCodiceAzienda(tipoDispositivo.getProduttore(),
                                                                                                  SIMEONI_CODE_CLIENT);
    contratto = optContratto.get();

    orderClient = new OrdineCliente().clienteAssegnatario(clienteFai)
                                     .numeroOrdine(seriale);

    orderClient = ordineClienteRepo.save(orderClient);

    richiesta = new Richiesta().tipo(TipoRichiesta.MALFUNZIONAMENTO)
                               .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_TE)
                               .data(Instant.now())
                               .dataModificaStato(Instant.now())
                               .tipoDispositivo(tipoDispositivo)
                               .contratto(contratto)
                               .ordineCliente(orderClient);

    dispositivo = new Dispositivo().seriale(seriale)
                                   .dataModificaStato(Instant.now())
                                   .contratto(contratto)
                                   .stato(StatoDispositivo.ATTIVO_SPEDITO_DA_FAI)
                                   .tipoDispositivo(tipoDispositivo)
                                   .tipoHardware(ObuTypeId.AD.name());

    dispositivo = deviceServiceExt.save(dispositivo);
    Set<Dispositivo> dispositivos = new HashSet<>();
    dispositivos.add(dispositivo);
    richiesta.setDispositivos(dispositivos);

    richiesta = richiestaServiceExt.save(richiesta);

    fsm = fsmRichiestaUtil.retrieveFsmByRichiesta(richiesta);
  }

  @After
  public void destory() throws Exception {
    richiesta.setContratto(null);
    richiesta.setDispositivos(null);
    richiesta.setOrdineCliente(null);

    richiestaServiceExt.save(richiesta);

    orderClient = ordineClienteRepo.save(orderClient.richiestas(null));

    dispositivo.setContratto(null);
    dispositivo = deviceServiceExt.save(dispositivo);

    ordineClienteRepo.delete(orderClient);
    deviceServiceExt.delete(dispositivo);
    richiestaServiceExt.delete(richiesta);
  }

  @Test
  @Transactional
  public void testConsumeMessage_001() throws Exception {
    assertThat(fsm).isNotNull();

    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);
    richiesta = fsm.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_DISATTIVAZIONE_DISPOSITIVO);

    ContractOrderRequest contractUuid = new ContractOrderRequest(contratto.getCodContrattoCliente(),contratto.getClienteFai()!=null ? contratto.getClienteFai().getCodiceCliente() : null);
    ObuDeviceInfo oDeviceInfo = new ObuDeviceInfo(new ObuNumber(dispositivo.getSeriale()), new ObuType(ObuTypeId.AD));

    DeviceReturnedMessage message = new DeviceReturnedMessage(contractUuid, oDeviceInfo);

    consumer.consume(message);

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
  }

}
