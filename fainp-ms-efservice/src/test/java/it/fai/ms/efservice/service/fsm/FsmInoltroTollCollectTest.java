package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.tollcollect.FsmInoltroTollCollect;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Transactional
public class FsmInoltroTollCollectTest {

  private static final String TARGA_TEST         = "testtarga";
  private static final String TARGA_NAZIONE_TEST = "IT";

  private final static String CODICE_AZIENDA = "0073935";

  @Autowired
  private FsmInoltroTollCollect fsm;

  @Autowired
  private FsmRichiestaCacheService cacheServiceMock;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private DispositivoServiceExt dispositivoService;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoServiceExt;

  @SpyBean
  private FsmSenderToQueue fsmSenderToQueue;

  @MockBean
  private DocumentService documentService;

  private TipoDispositivo tollCollect;

  @Before
  public void init() {
    cacheServiceMock.clearCache();
    tollCollect = tipoDispositivoServiceExt.findOneByNome(TipoDispositivoEnum.TOLL_COLLECT);
  }

  @Test
  public void skipFsmWrongDevice() throws Exception {
    Richiesta richiesta = buildRichiesta();
    richiesta.setTipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TELEPASS_ITALIANO));
    richiesta.setStato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
  }

  @Test
  public void skipFmNoCommandAvailable() throws Exception {
    Richiesta richiesta = buildRichiesta().tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TOLL_COLLECT))
                                          .stato(StatoRichiesta.ESAME_DENUNCIA);
    try {
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    } catch (Exception e) {
      assertThat(e.getMessage()).startsWith("Command INITIAL");
    }
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ESAME_DENUNCIA);
  }

  @Test
  public void accettatoToIncompletoTecnicoNoDocument() throws Exception {
    Richiesta richiesta = goToAccettatoProntoPerInoltro();
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.INCOMPLETO_TECNICO);
  }

  @Test
  public void accettatoToDaValidare() throws Exception {
    Richiesta richiesta = goToDaValidare(true, true);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.DA_VALIDARE);
    assertThat(richiesta.getUuidDocuments()).hasSize(1)
                                            .containsExactly("iddocumento1");
  }

  @Test
  public void daValidareToDaInoltrareAlFornitore() throws Exception {
    Richiesta richiesta = goToDaInoltrare();
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.DA_INOLTRARE);
  }

  @Test
  public void daValidareToAnnullatoOperatoreFAI() throws Exception {
    Richiesta richiesta = goToDaValidare(false, false);
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    richiesta.getDispositivos()
             .stream()
             .forEach(d -> {
               verify(fsmSenderToQueue).sendMessageForChangeStatoDispositivo(d, DispositivoEvent.ANNULLAMENTO);
             });
  }

  @Test
  public void incompletoTecnicoToAnnullatoOperatoreFAI() throws Exception {
    Richiesta richiesta = goToIncompletoTecnico();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    richiesta.getDispositivos()
             .stream()
             .forEach(d -> {
               verify(fsmSenderToQueue).sendMessageForChangeStatoDispositivo(d, DispositivoEvent.ANNULLAMENTO);
             });
  }

  @Test
  public void incompletoTecnicoToDaInoltrareAlFornitore() throws Exception {
    Richiesta richiesta = goToIncompletoTecnico();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_DOCUMENTI_VALIDATI, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.DA_INOLTRARE);
  }

  @Test
  public void daInoltrareToInoltroParziale() throws Exception {
    Richiesta richiesta = goToDaInoltrare();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_INOLTRO_PARZIALE, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.INOLTRO_PARZIALE);
  }

  @Test
  public void daInoltrareToInAttesaConsumo() throws Exception {
    Richiesta richiesta = goToInAttesaConsumo();
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
  }

  @Test
  public void daInoltrareToOrdineSospesoCodiceContratto() throws Exception {
    Richiesta richiesta = goToOrdineSospesoCodContratto();
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
  }

  @Test
  public void ordineSospesoToOrdineSospeso() throws Exception {
    Richiesta richiesta = goToOrdineSospesoCodContratto();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_CODICE_CONTRATTO_INSERITO, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO);
  }

  @Test
  public void ordineSospesoToInAttesaConsumi() throws Exception {
    Richiesta richiesta = goToOrdineSospesoCodContratto();
    richiesta.getContratto()
             .setCodContrattoCliente("codcontratto123");
    fsm.executeCommandToChangeState(RichiestaEvent.MU_CODICE_CONTRATTO_INSERITO, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
  }

  @Test
  public void ordineSospesoInAttesaConsumiToAttivoEvaso() throws Exception {
    Richiesta richiesta = goToInAttesaConsumo();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ATTIVO_EVASO);
    richiesta.getDispositivos()
             .stream()
             .forEach(d -> {
               verify(fsmSenderToQueue).sendMessageForChangeStatoDispositivo(d, DispositivoEvent.SPEDITO_DAL_FORNITORE);
             });

  }

  @Test
  public void ordineSospesoInAttesaConsumiToAnnullamento() throws Exception {
    Richiesta richiesta = goToInAttesaConsumo();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ANNULLAMENTO_OPERATORE, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ANNULLATO_OPERATORE_FAI);
    richiesta.getDispositivos()
             .stream()
             .forEach(d -> {
               verify(fsmSenderToQueue).sendMessageForChangeStatoDispositivo(d, DispositivoEvent.ANNULLAMENTO);
             });
  }

  private Richiesta buildRichiesta() throws Exception {
    Richiesta richiesta = new Richiesta();
    richiesta.setId(5678L);
    richiesta.setTipo(TipoRichiesta.NUOVO_ORDINE);
    richiesta.setStato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    richiesta.setOrdineCliente(newOrdineCliente());
    richiesta.setContratto(new Contratto().clienteFai(richiesta.getOrdineCliente()
                                                               .getClienteAssegnatario()));
    richiesta.setAssociazione(TARGA_TEST);
    richiesta.setNewTargaNazione(TARGA_NAZIONE_TEST);
    richiesta.addDispositivo(buildDispositivo(tollCollect));
    return richiesta;
  }

  private Dispositivo buildDispositivo(TipoDispositivo tipoDispositivo) throws Exception {

    Dispositivo dispositivo = new Dispositivo().stato(StatoDispositivo.INIZIALE)
                                               .dataModificaStato(Instant.now())
                                               .tipoDispositivo(tipoDispositivo)
                                               .seriale(UUID.randomUUID()
                                                            .toString());
    // tollCollect.getTipoServizios()
    tollCollect.getTipoServizios()
               .stream()
               .map(ts -> new StatoDispositivoServizio().tipoServizio(ts)
                                                        .stato(StatoDS.IN_ATTIVAZIONE))
               .forEach(sds -> dispositivo.addStatoDispositivoServizio(sds));
    return dispositivoService.save(dispositivo);
  }

  private Richiesta goToDaValidare(boolean expectFindDocRichiestaContratto, boolean expectedFindRichiestaDispositivo) throws Exception {
    Richiesta richiesta = goToAccettatoProntoPerInoltro();
    richiesta.setId(1l);
    if (expectFindDocRichiestaContratto) {
      when(documentService.findLastDocument(richiesta.getContratto()
                                                     .getClienteFai()
                                                     .getCodiceCliente(),
                                            DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO, TipoDispositivoEnum.TOLL_COLLECT, null,
                                            null)).thenReturn(Optional.of(new DocumentoDTO().id(1l)
                                                                                            .identificativo("iddocumento1")));
    }
    if (expectedFindRichiestaDispositivo) {
      when(documentService.findLastDocument(richiesta.getContratto()
                                                     .getClienteFai()
                                                     .getCodiceCliente(),
                                            DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO, TipoDispositivoEnum.TOLL_COLLECT,
                                            richiesta.getAssociazione(), richiesta.getCountry()))
                                                                                                 .thenReturn(Optional.of(new DocumentoDTO().id(1l)
                                                                                                                                           .identificativo("iddocumento1")));
    }

    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    return richiesta;
  }

  private Richiesta goToIncompletoTecnico() throws Exception {
    Richiesta richiesta = goToAccettatoProntoPerInoltro();
    richiesta.setId(1l);
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    return richiesta;
  }

  private Richiesta goToDaInoltrare() throws Exception {
    Richiesta richiesta = goToDaValidare(true, true);
    fsm.executeCommandToChangeState(RichiestaEvent.MU_DOCUMENTI_VALIDATI, richiesta);
    return richiesta;
  }

  private Richiesta goToOrdineSospesoCodContratto() throws Exception {
    Richiesta richiesta = goToDaInoltrare();
    Contratto contratto = buildContratto(richiesta.getOrdineCliente()
                                                  .getClienteAssegnatario());
    richiesta.setContratto(contratto);
    fsm.executeCommandToChangeState(RichiestaEvent.MU_INOLTRO_COMPLETO, richiesta);
    return richiesta;
  }

  private Richiesta goToAccettatoProntoPerInoltro() throws Exception {
    return buildRichiesta().tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TOLL_COLLECT))
                           .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
  }

  private Richiesta goToInAttesaConsumo() throws Exception {
    Richiesta richiesta = goToDaInoltrare();
    Contratto contratto = buildContratto(richiesta.getOrdineCliente()
                                                  .getClienteAssegnatario()).codContrattoCliente("codcontratto1234");
    richiesta.setContratto(contratto);
    fsm.executeCommandToChangeState(RichiestaEvent.MU_INOLTRO_COMPLETO, richiesta);
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().clienteAssegnatario(newClienteFai());
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }

  private Contratto buildContratto(ClienteFai cliente) {
    return new Contratto().clienteFai(cliente);
  }

}
