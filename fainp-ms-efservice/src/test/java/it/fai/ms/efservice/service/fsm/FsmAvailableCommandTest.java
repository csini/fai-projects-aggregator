/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FsmAvailableCommandTest {

  @Autowired
  private FsmFactory fsmFactory;

  // @Autowired
  // private StateMachineFactory<StatoDispositivo, DispositivoEvent> smFactoryDispositivo;

  private AbstractFsmRichiesta fsm;

  private Richiesta entityTest;

  @Before // Set-up
  public void setUp() throws Exception {
    entityTest = new Richiesta();
    int randId = (int) (Math.random() * Integer.MAX_VALUE);
    entityTest.setId((long) randId);
    entityTest.setTipo(TipoRichiesta.NUOVO_ORDINE);
  }

  @Test
  public void availableCommandUser_001_FsmAccettazione() throws Exception {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_IN_ACCETTAZIONE);
    assertCount(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, 5);
    assertCount(StatoRichiesta.SOSPESA, 3);
  }

  @Test
  public void availableCommand_001_FsmAccettazione() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_IN_ACCETTAZIONE);
    assertTrueCommand(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, RichiestaEvent.MU_ACCETTA_RICHIESTA);
    assertTrueCommand(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, RichiestaEvent.MU_ANNULLAMENTO_OPERATORE);
    assertTrueCommand(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, RichiestaEvent.MU_VALUTAZIONE_PRECONDIZIONI);
    assertTrueCommand(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, RichiestaEvent.MU_ANNULLAMENTO_UTENTE);
    assertTrueCommand(StatoRichiesta.SOSPESA, RichiestaEvent.MU_ACCETTA_RICHIESTA);
    assertTrueCommand(StatoRichiesta.SOSPESA, RichiestaEvent.MU_ANNULLA_RICHIESTA);
    assertTrueCommand(StatoRichiesta.SOSPESA, RichiestaEvent.MU_RIFIUTATA_RICHIESTA);
  }

  @Test
  public void availableCommandUser_002_FsmInoltroTe() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE);
    assertCount(StatoRichiesta.ACCETTATO, 0);
    assertCount(StatoRichiesta.INCOMPLETO_TECNICO, 0);
    assertCount(StatoRichiesta.ATTESA_RISPOSTA, 0);
    assertCount(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO, 0);
    assertCount(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO, 0);
    assertCount(StatoRichiesta.COMPLETO, 0);
    assertCount(StatoRichiesta.DA_INOLTRARE, 0);
    assertCount(StatoRichiesta.INOLTRATO, 0);
    assertCount(StatoRichiesta.ANALISI_RISPOSTA_ORDINE, 0);
    assertCount(StatoRichiesta.ACCETTATO_FORNITORE, 0);
    assertCount(StatoRichiesta.ORDINE_SOSPESO, 2);
    assertCount(StatoRichiesta.PREPARAZIONE_SPEDIZIONE, 0);
    assertCount(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI, 2);
  }

  @Test
  public void availableCommand_002_FsmInoltroTe() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TE);
    assertTrueCommand(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.ATTESA_RISPOSTA, RichiestaEvent.MS_CTRRI);
    assertTrueCommand(StatoRichiesta.INOLTRATO, RichiestaEvent.MS_ORDRI_RISIM);
    assertTrueCommand(StatoRichiesta.ORDINE_SOSPESO, RichiestaEvent.MS_RIPETI_INVIO);
    assertTrueCommand(StatoRichiesta.ORDINE_SOSPESO, RichiestaEvent.MU_CHIUDI_ORDINE_SU_PORTALE_TELEPASS);
    assertTrueCommand(StatoRichiesta.ORDINE_SOSPESO, RichiestaEvent.MU_ANNULLA_ORDINE);
    assertTrueCommand(StatoRichiesta.ACCETTATO_FORNITORE, RichiestaEvent.MS_OBUNO);
    assertTrueCommand(StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI, RichiestaEvent.MU_SPEDIZIONE_COMPLETA);
  }

  @Test
  public void availableCommandUser_002_FsmInoltroTsat() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TSAT);
    assertCount(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, 0);
    assertCount(StatoRichiesta.ACCETTATO, 1);
    assertCount(StatoRichiesta.VERIFICA_ACCETTAZIONE, 0);
    assertCount(StatoRichiesta.ABBINATO, 0);
    assertCount(StatoRichiesta.INOLTRATO, 0);
    assertCount(StatoRichiesta.ACCETTATO_FORNITORE, 0);
    assertCount(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA, 2);
    assertCount(StatoRichiesta.ANNULLATO_OPERATORE_FAI, 0);
  }

  @Test
  public void availableCommand_002_FsmInoltroTsat() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_TSAT);
    assertTrueCommand(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA, RichiestaEvent.MU_ANNULLAMENTO_OPERATORE);
    assertTrueCommand(StatoRichiesta.ORDINE_SOSPESO_TARGA_OCCUPATA, RichiestaEvent.MU_PLATE_FREE);
    assertTrueCommand(StatoRichiesta.ACCETTATO, RichiestaEvent.MU_VERIFICA_VC);
  }

  @Test
  public void testAvailableCommandUser_003_FsmTeFurtoSmarrimento() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_FURTO_NO_SOST_TE);
    assertCount(StatoRichiesta.ATTIVO, 0);
    assertCount(StatoRichiesta.SMARRIMENTO, 0);
    assertCount(StatoRichiesta.FURTO, 0);
    assertCount(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA, 0);
    assertCount(StatoRichiesta.EVASO, 0);
  }

  @Test
  public void availableCommand_003_FsmTeFurtoSmarrimento() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_FURTO_NO_SOST_TE);
    assertTrueCommand(StatoRichiesta.ATTIVO_PER_MODIFICA_TE, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA, RichiestaEvent.RESPONSE_KO);
    assertTrueCommand(StatoRichiesta.INOLTRATO_IN_ATTESA_RISPOSTA, RichiestaEvent.RESPONSE_OK);
  }

  @Test
  public void testAvailableCommandUser_004_FsmTeVariazioneTarga() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TE);
    assertCount(StatoRichiesta.ATTIVO, 0);
    assertCount(StatoRichiesta.MALFUNZIONAMENTO_CON_SOSTITUZIONE, 0);
    assertCount(StatoRichiesta.VARIAZIONE_TARGA, 0);
    assertCount(StatoRichiesta.MEZZO_RITARGATO, 0);
    assertCount(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, 1);
    assertCount(StatoRichiesta.DISPOSITIVO_RIENTRATO, 0);
    assertCount(StatoRichiesta.DISPOSITIVO_DISATTIVATO, 1);
    assertCount(StatoRichiesta.EVASO_CON_RICONSEGNA, 0);
  }

  @Test
  public void availableCommandFsm_004_TeVariazioneTarga() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_TE);
    fsm = fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_SOST_TE);
    fsm = fsmFactory.getFsm(FsmCommand.CMD_MEZZO_RITARGATO_TE);
    assertTrueCommand(StatoRichiesta.ATTIVO_PER_MODIFICA_TE, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, RichiestaEvent.MU_DISPOSITIVO_RIENTRATO);
    assertTrueCommand(StatoRichiesta.DISPOSITIVO_DISATTIVATO, RichiestaEvent.MU_LETTERA_VETTURA);
  }

  @Test
  public void testAvailableCommandUser_005_FsmTeMalfunzionamento() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE);
    assertCount(StatoRichiesta.ATTIVO, 0);
    assertCount(StatoRichiesta.MALFUNZIONAMENTO, 0);
    assertCount(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, 3);
    assertCount(StatoRichiesta.DISPOSITIVO_RIENTRATO, 0);
    assertCount(StatoRichiesta.DISPOSITIVO_DISATTIVATO, 1);
    assertCount(StatoRichiesta.EVASO_CON_RICONSEGNA, 0);
  }

  @Test
  public void availableCommand_005_FsmTeMalfunzionamento() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_MALFUNZIONAMENTO_NO_SOST_TE);
    assertTrueCommand(StatoRichiesta.ATTIVO_PER_MODIFICA_TE, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, RichiestaEvent.MU_DISPOSITIVO_RIENTRATO);
    assertTrueCommand(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, RichiestaEvent.MU_ANNULLAMENTO_OPERATORE);
    assertTrueCommand(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO, RichiestaEvent.MU_ANNULLAMENTO_UTENTE);
    assertTrueCommand(StatoRichiesta.DISPOSITIVO_DISATTIVATO, RichiestaEvent.MU_LETTERA_VETTURA);
  }

  @Test
  public void testAvailableCommandUser_006_FsmTeRescRiattSospensione() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_RESC_RIATT_SOSPENSIONE_TE);
    assertCount(StatoRichiesta.ATTIVO, 0);
    assertCount(StatoRichiesta.IN_SOSPENSIONE, 0);
    assertCount(StatoRichiesta.IN_RESCISSIONE, 0);
    assertCount(StatoRichiesta.SOSPESO, 1);
    assertCount(StatoRichiesta.CESSATO, 0);
    assertCount(StatoRichiesta.IN_RIATTIVAZIONE, 0);
    assertCount(StatoRichiesta.RIATTIVATO, 0);
  }

  @Test
  public void availableCommand_006_FsmTeRescRiattSospensione() {
    fsm = fsmFactory.getFsm(FsmCommand.CMD_RESC_RIATT_SOSPENSIONE_TE);
    assertTrueCommand(StatoRichiesta.ATTIVO_PER_MODIFICA_TE, RichiestaEvent.INITIAL);
    assertTrueCommand(StatoRichiesta.SOSPESO, RichiestaEvent.MU_RICHIESTA_RIATTIVAZIONE);
  }

  private void assertCount(StatoRichiesta stato, int size) {
    entityTest.setStato(stato);
    List<String> userAvailableCommand = fsm.getUserAvailableCommand(entityTest);
    assertThat(userAvailableCommand).hasSize(size);
  }

  private void assertTrueCommand(StatoRichiesta stato, RichiestaEvent event) {
    entityTest.setStato(stato);
    boolean availableCommand = fsm.isAvailableCommand(event, entityTest);
    assertThat(availableCommand).isTrue();
  }

}
