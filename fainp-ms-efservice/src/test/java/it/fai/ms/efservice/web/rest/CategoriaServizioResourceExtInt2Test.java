package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import it.fai.ms.efservice.service.CategoriaServizioServiceExt;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;

public class CategoriaServizioResourceExtInt2Test {

	private CategoriaServizioResource categoriaServizioResource = mock(CategoriaServizioResource.class);
	private CategoriaServizioServiceExt categoriaServizioServiceExt = mock(CategoriaServizioServiceExt.class);


	@Before
	public void setUp() throws Exception {
		when(categoriaServizioResource.getAllCategoriaServizios()).thenReturn(createCategoriaServizioDtoList());
	}

	@Test
	public void test() {
		CategoriaServizioResourceExt csre = new CategoriaServizioResourceExt(categoriaServizioResource,categoriaServizioServiceExt);
		ResponseEntity<List<CategoriaServizioDTO>> responseEntity = csre.getListaCategoriaServizio();
	    verify(categoriaServizioResource).getAllCategoriaServizios();
	    assertThat(responseEntity.getBody()).hasSize(3);
	    assertThat(responseEntity.getBody()).isSortedAccordingTo(new FieldOrdinamentoComparator());
	    
	}
	
	private List<CategoriaServizioDTO> createCategoriaServizioDtoList() {
		List<CategoriaServizioDTO> lista = new LinkedList<>();
		CategoriaServizioDTO categoriaServizio1 = new CategoriaServizioDTO();
		CategoriaServizioDTO categoriaServizio2 = new CategoriaServizioDTO();
		CategoriaServizioDTO categoriaServizio3 = new CategoriaServizioDTO();
		categoriaServizio1.setOrdinamento(5L);
		categoriaServizio2.setOrdinamento(1L);
		categoriaServizio3.setOrdinamento(3L);
		lista.add(categoriaServizio1);
		lista.add(categoriaServizio2);
		lista.add(categoriaServizio3);
		return lista;
	}

}
