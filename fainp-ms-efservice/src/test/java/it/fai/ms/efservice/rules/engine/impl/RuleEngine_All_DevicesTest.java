package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_All_DevicesTest {

  @Test
  public void testWithCountryEuroClassMakeType() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLicensePlate(null);

    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertCodeFailure(ruleOutcome, "licensePlate");
  }

  @Test
  public void testWithEuroClassLicensePlateMakeType() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setCountry(null);

    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertCodeFailure(ruleOutcome, "country");
  }

  @Test
  public void testWithCountryEuroClassLicensePlateMake() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType(null);

    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertCodeFailure(ruleOutcome, "type");
  }

  @Test
  public void testWithCountryLicensePlateMakeType() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setEuroClass("euroClass");

    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testWithCountryEuroClassLicensePlateType() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setMake(null);

    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertCodeFailure(ruleOutcome, "make");
  }

  @Test
  public void testComplete() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_All_Devices(ruleContextVehicle).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    RuleEngineVehicle ruleContextVehicle = new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
    ruleContextVehicle.setLicensePlate("::licensePlate::");
    ruleContextVehicle.setCountry("::country::");
    ruleContextVehicle.setType("::type::");
    ruleContextVehicle.setEuroClass("EURO0");
    ruleContextVehicle.setMake("::make::");
    return ruleContextVehicle;
  }

  private void assertCodeFailure(RuleOutcome ruleOutcome, String expectedValue) {
    String codeFailure = ruleOutcome.getFailure()
                                    .get()
                                    .getCode();
    assertThat(codeFailure).isEqualTo(expectedValue);
  }

}
