package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Optional;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ContrattoServiceExtTest {

  @Autowired
  private ContrattoRepositoryExt contrattoRepository;

  @Autowired
  private ClienteFaiRepository clienteFaiRepository;

  @Autowired
  private ProduttoreRepository produttoreRepository;

  @Autowired
  private TipoDispositivoRepository deviceTypeRepo;

  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void testEntityListener() throws Exception {
    String codContrattoCliente = RandomString();

    int cliente_fai_size_before = clienteFaiRepository.findAll()
                                                      .size();
    int contratto_size_before = contrattoRepository.findAll()
                                                   .size();
    int produttore_size_before = produttoreRepository.findAll()
                                                     .size();

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCitta(RandomString());
    clienteFai.setVia(RandomString());
    clienteFai.setCodiceCliente(RandomString());
    clienteFai.setDmlRevisionTimestamp(Instant.now());

    clienteFai = clienteFaiRepository.save(clienteFai);

    int cliente_fai_size_after = clienteFaiRepository.findAll()
                                                     .size();
    assertThat(cliente_fai_size_before + 1).isEqualTo(cliente_fai_size_after);

    Produttore produttore = new Produttore();
    produttore.setCodiceFornitoreNav(RandomString());
    produttore.setNome(RandomString());

    produttore = produttoreRepository.save(produttore);

    int produttore_size_after = produttoreRepository.findAll()
                                                    .size();
    assertThat(produttore_size_before + 1).isEqualTo(produttore_size_after);

    Contratto contratto = new Contratto().stato(StatoContratto.ATTIVO)
                                         .dataModificaStato(Instant.now());
    contratto.setClienteFai(clienteFai);
    contratto.setProduttore(produttore);
    contratto.setCodContrattoCliente(codContrattoCliente);

    contratto = contrattoRepository.save(contratto);

    int contratto_size_after = contrattoRepository.findAll()
                                                  .size();
    assertThat(contratto_size_before + 1).isEqualTo(contratto_size_after);

    assertThat(contratto.getCodContrattoCliente()).isEqualTo(codContrattoCliente);

    contrattoRepository.delete(contratto.getId());

    contratto_size_after = contrattoRepository.findAll()
                                              .size();
    assertThat(contratto_size_before).isEqualTo(contratto_size_after);

    clienteFaiRepository.delete(clienteFai.getId());

    cliente_fai_size_after = clienteFaiRepository.findAll()
                                                 .size();
    assertThat(cliente_fai_size_before).isEqualTo(cliente_fai_size_after);

    produttoreRepository.delete(produttore.getId());

    produttore_size_after = produttoreRepository.findAll()
                                                .size();
    assertThat(produttore_size_before).isEqualTo(produttore_size_after);
  }

  @Test
  @Transactional
  public void checkContratto() {

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCitta(RandomString());
    clienteFai.setVia(RandomString());
    String codiceCliente = RandomString();
    clienteFai.setCodiceCliente(codiceCliente);
    clienteFai.setDmlRevisionTimestamp(Instant.now());
    clienteFai = clienteFaiRepository.save(clienteFai);

    Produttore produttore = new Produttore();
    produttore.setCodiceFornitoreNav(RandomString());
    String nomeProduttore = RandomString();
    produttore.setNome(nomeProduttore);
    produttore = produttoreRepository.save(produttore);

    TipoDispositivo tipoDispositivo = deviceTypeRepo.save(new TipoDispositivo().produttore(produttore)
                                                                               .nome(TipoDispositivoEnum.TRACKYCARD).multiservizio(true).virtuale(false));
    produttore.addTipoDispositivo(tipoDispositivo);
    produttore = produttoreRepository.save(produttore);

    Optional<Contratto> optContratto = contrattoRepository.findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue(codiceCliente,
                                                                                                                             TipoDispositivoEnum.TRACKYCARD);

    assertThat(optContratto.isPresent()).isFalse();

    String codiceContratto = RandomString();
    Contratto contract = new Contratto().clienteFai(clienteFai)
                                        .codContrattoCliente(codiceContratto)
                                        .produttore(produttore).stato(StatoContratto.ATTIVO).dataModificaStato(Instant.now());
    contract = contrattoRepository.save(contract);

    optContratto = contrattoRepository.findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue("::codiceCliente::",
                                                                                                         TipoDispositivoEnum.TRACKYCARD);

    assertThat(optContratto.isPresent()).isFalse();

    optContratto = contrattoRepository.findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue(codiceCliente,
                                                                                                         TipoDispositivoEnum.GO_BOX);

    assertThat(optContratto.isPresent()).isFalse();

    optContratto = contrattoRepository.findByClienteFai_codiceClienteAndProduttore_tipoDispositivos_nomeAndPrimarioTrue(codiceCliente,
                                                                                                         TipoDispositivoEnum.TRACKYCARD);

    assertThat(optContratto.isPresent()).isTrue();

  }

  static String RandomEmail() {
    return RandomStringUtils.random(20, true, true) + "@" + RandomStringUtils.random(10, true, false) + "."
           + RandomStringUtils.random(2, true, false);
  }

  static String RandomString() {
    return RandomStringUtils.random(20, true, true);
  }

}
