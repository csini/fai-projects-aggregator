package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_DartFordCrossingTest {

  @Test
  public void testVehicleLicensePlateNull() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setLicensePlate(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "licensePlate must be present");
  }

  @Test
  public void testVehicleLicensePlateEmpty() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setLicensePlate("");
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "licensePlate must be present");
  }

  @Test
  public void testVehicleMakeNull() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setMake(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "make must be present");
  }

  @Test
  public void testVehicleMakeEmpty() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setMake("");
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "make must be present");
  }

  @Test
  public void testVehicleModelloNull() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setModel(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "model must be present");
  }

  @Test
  public void testVehicleModelloEmpty() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setModel("");
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "model must be present");
  }

  @Test
  public void testVehicleColorNull() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setColor(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "color must be present");

  }

  @Test
  public void testVehicleColorEmpty() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    ruleContextVehicle.setColor("");
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "color must be present");
  }

  @Test
  public void testComplete() {
    RuleEngineVehicle ruleContextVehicle = dartfordCrossingRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_Device_DartfordCrossing(newRuleContextDeviceType(), ruleContextVehicle,
                                                                     newRuleContextServiceType()).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  private void assertFailureMessage(RuleOutcome ruleOutcome, String msgExpected) {
    Optional<RuleFailure> failure = ruleOutcome.getFailure();
    assertThat(failure.isPresent()).isTrue();
    RuleFailure ruleFailure = failure.get();
    String msg = ruleFailure.getMess();
    assertThat(msg).isEqualTo(msgExpected);
  }

  private RuleEngineVehicle dartfordCrossingRuleContextVehicle() {
    RuleEngineVehicle ruleContextVehicle = new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
    ruleContextVehicle.setLicensePlate("::licensePlate::");
    ruleContextVehicle.setMake("::make::");
    ruleContextVehicle.setModel("::model::");
    ruleContextVehicle.setColor("::color::");
    return ruleContextVehicle;
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType() {
    return newRuleContextServiceType("::serviceType::");
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

}
