package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.ContrattoService;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.mapper.ContrattoMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the ContrattoResource REST controller.
 *
 * @see ContrattoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ContrattoResourceIntTest {

  private static final String DEFAULT_COD_CONTRATTO_CLIENTE = "AAAAAAAAAA";
  private static final String UPDATED_COD_CONTRATTO_CLIENTE = "BBBBBBBBBB";

  @Autowired
  private ContrattoRepositoryExt contrattoRepository;

  @Autowired
  private ContrattoMapper contrattoMapper;

  @Autowired
  private ContrattoService contrattoService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restContrattoMockMvc;

  private Contratto contratto;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final ContrattoResource contrattoResource = new ContrattoResource(contrattoService);
    this.restContrattoMockMvc = MockMvcBuilders.standaloneSetup(contrattoResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static Contratto createEntity(EntityManager em) {
    Contratto contratto = new Contratto()
        .codContrattoCliente(DEFAULT_COD_CONTRATTO_CLIENTE)
        .stato(StatoContratto.ATTIVO)
        .paeseRiferimentoIva("IT")
        .dataModificaStato(Instant.now());
    return contratto;
  }

  @Before
  public void initTest() {
    contratto = createEntity(em);
  }

  @Test
  @Transactional
  public void createContratto() throws Exception {
    int databaseSizeBeforeCreate = contrattoRepository.findAll()
        .size();

    // Create the Contratto
    ContrattoDTO contrattoDTO = contrattoMapper.toDto(contratto);
    restContrattoMockMvc.perform(post("/api/contrattos").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
    .andExpect(status().isCreated());

    // Validate the Contratto in the database
    List<Contratto> contrattoList = contrattoRepository.findAll();
    assertThat(contrattoList).hasSize(databaseSizeBeforeCreate + 1);
    Contratto testContratto = contrattoList.get(contrattoList.size() - 1);
    assertThat(testContratto.getCodContrattoCliente()).isEqualTo(DEFAULT_COD_CONTRATTO_CLIENTE);
  }

  @Test
  @Transactional
  public void createContrattoWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = contrattoRepository.findAll()
        .size();

    // Create the Contratto with an existing ID
    contratto.setId(1L);
    ContrattoDTO contrattoDTO = contrattoMapper.toDto(contratto);

    // An entity with an existing ID cannot be created, so this API call must fail
    restContrattoMockMvc.perform(post("/api/contrattos").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
    .andExpect(status().isBadRequest());

    // Validate the Contratto in the database
    List<Contratto> contrattoList = contrattoRepository.findAll();
    assertThat(contrattoList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void getAllContrattos() throws Exception {
    // Initialize the database
    contrattoRepository.saveAndFlush(contratto);

    // Get all the contrattoList
    restContrattoMockMvc.perform(get("/api/contrattos?sort=id,desc"))
    .andExpect(status().isOk())
    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    .andExpect(jsonPath("$.[*].id").value(hasItem(contratto.getId()
                                                  .intValue())))
    .andExpect(jsonPath("$.[*].codContrattoCliente").value(hasItem(DEFAULT_COD_CONTRATTO_CLIENTE.toString())));
  }

  @Test
  @Transactional
  public void getContratto() throws Exception {
    // Initialize the database
    contrattoRepository.saveAndFlush(contratto);

    // Get the contratto
    restContrattoMockMvc.perform(get("/api/contrattos/{id}", contratto.getId()))
    .andExpect(status().isOk())
    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    .andExpect(jsonPath("$.id").value(contratto.getId()
                                      .intValue()))
    .andExpect(jsonPath("$.paeseRiferimentoIva").value(contratto.getPaeseRiferimentoIva().toString()))
    .andExpect(jsonPath("$.codContrattoCliente").value(DEFAULT_COD_CONTRATTO_CLIENTE.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingContratto() throws Exception {
    // Get the contratto
    restContrattoMockMvc.perform(get("/api/contrattos/{id}", Long.MAX_VALUE))
    .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateContratto() throws Exception {
    // Initialize the database
    contrattoRepository.saveAndFlush(contratto);
    int databaseSizeBeforeUpdate = contrattoRepository.findAll()
        .size();

    // Update the contratto
    Contratto updatedContratto = contrattoRepository.findOne(contratto.getId());
    updatedContratto.codContrattoCliente(UPDATED_COD_CONTRATTO_CLIENTE);
    ContrattoDTO contrattoDTO = contrattoMapper.toDto(updatedContratto);

    restContrattoMockMvc.perform(put("/api/contrattos").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
    .andExpect(status().isOk());

    // Validate the Contratto in the database
    List<Contratto> contrattoList = contrattoRepository.findAll();
    assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate);
    Contratto testContratto = contrattoList.get(contrattoList.size() - 1);
    assertThat(testContratto.getCodContrattoCliente()).isEqualTo(UPDATED_COD_CONTRATTO_CLIENTE);
  }

  @Test
  @Transactional
  public void updateNonExistingContratto() throws Exception {
    int databaseSizeBeforeUpdate = contrattoRepository.findAll()
        .size();

    // Create the Contratto
    ContrattoDTO contrattoDTO = contrattoMapper.toDto(contratto);

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restContrattoMockMvc.perform(put("/api/contrattos").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(contrattoDTO)))
    .andExpect(status().isCreated());

    // Validate the Contratto in the database
    List<Contratto> contrattoList = contrattoRepository.findAll();
    assertThat(contrattoList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteContratto() throws Exception {
    // Initialize the database
    contrattoRepository.saveAndFlush(contratto);
    int databaseSizeBeforeDelete = contrattoRepository.findAll()
        .size();

    // Get the contratto
    restContrattoMockMvc.perform(delete("/api/contrattos/{id}", contratto.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().isOk());

    // Validate the database is empty
    List<Contratto> contrattoList = contrattoRepository.findAll();
    assertThat(contrattoList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(Contratto.class);
    Contratto contratto1 = new Contratto();
    contratto1.setId(1L);
    Contratto contratto2 = new Contratto();
    contratto2.setId(contratto1.getId());
    assertThat(contratto1).isEqualTo(contratto2);
    contratto2.setId(2L);
    assertThat(contratto1).isNotEqualTo(contratto2);
    contratto1.setId(null);
    assertThat(contratto1).isNotEqualTo(contratto2);
  }

  @Test
  @Transactional
  public void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(ContrattoDTO.class);
    ContrattoDTO contrattoDTO1 = new ContrattoDTO();
    contrattoDTO1.setId(1L);
    ContrattoDTO contrattoDTO2 = new ContrattoDTO();
    assertThat(contrattoDTO1).isNotEqualTo(contrattoDTO2);
    contrattoDTO2.setId(contrattoDTO1.getId());
    assertThat(contrattoDTO1).isEqualTo(contrattoDTO2);
    contrattoDTO2.setId(2L);
    assertThat(contrattoDTO1).isNotEqualTo(contrattoDTO2);
    contrattoDTO1.setId(null);
    assertThat(contrattoDTO1).isNotEqualTo(contrattoDTO2);
  }

  @Test
  @Transactional
  public void testEntityFromId() {
    assertThat(contrattoMapper.fromId(42L)
               .getId()).isEqualTo(42);
    assertThat(contrattoMapper.fromId(null)).isNull();
  }
}
