package it.fai.ms.efservice.service.jms.listener.device;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.jms.ObjectMessage;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.dto.DispositivoStatoDTO;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.AbstractCommonTest;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.DispositivoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore
public class JmsListenerDeviceStateUpdateExtTest extends AbstractCommonTest {
  
  @Autowired JmsListenerDeviceStateUpdate listener;
  
  @Autowired DispositivoRepository dispositivoRepository;

  @Test
  public void onMessage() throws Exception {
    long size = dispositivoRepository.count();
    
    TipoServizio ts = new TipoServizio();
    ts.setId(4L);

    Set<TipoServizio> tipoServizios = new HashSet<>();
    tipoServizios.add(ts);
    
    TipoDispositivo td = new TipoDispositivo();
    td.setId(1L);
    td.setTipoServizios(tipoServizios);
    
    Dispositivo device = new Dispositivo();
    device.setTipoDispositivo(td);
    device.setSeriale(RandomString());
    device.setStato(StatoDispositivo.ATTIVO);
    device.setIdentificativo(RandomUUID());
    
    device = dispositivoRepository.save(device);
    assertThat(size + 1).isEqualTo(dispositivoRepository.count());
    
    DispositivoStatoDTO dto = new DispositivoStatoDTO();
    dto.setDataModificaStato(Instant.now());
    dto.setDataSpedizione(device.getDataSpedizione());
    dto.setUuidDispositivo(device.getIdentificativo());
    dto.setStato("RIENTRATO_RESTITUITO_AL_FORNITORE");

    ObjectMessage objectMessage = mock(ObjectMessage.class);
    when(objectMessage.getObject()).thenReturn(dto);
    listener.onMessage(objectMessage);

    device = dispositivoRepository.findOne(device.getId());

    assertThat(device.getStato()).isEqualTo(StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE);
    
    dispositivoRepository.delete(device);
    assertThat(size).isEqualTo(dispositivoRepository.count());
  }

}
