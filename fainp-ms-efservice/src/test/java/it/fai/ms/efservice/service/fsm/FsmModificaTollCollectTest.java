package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.jgroups.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmDispositivoCacheService;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.fsm.type.tollcollect.FsmModificaTollCollect;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmModificaTollCollectTest {

  private static final String TARGA_TEST         = "AA111AA";
  private static final String TARGA_NAZIONE_TEST = "IT";

  private static final String CODICE_AZIENDA = "0073935";

  @Autowired
  private FsmModificaTollCollect fsm;

  private FsmResetService fsmResetService;

  private RichiestaRepositoryExt richiestaRepoMock = mock(RichiestaRepositoryExt.class);

  private DispositivoRepositoryExt deviceRepoMock = mock(DispositivoRepositoryExt.class);

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private FsmRichiestaCacheService cacheServiceMock;

  @Autowired
  private FsmDispositivoCacheService cacheServiceDispositivoMock;

  @Before
  public void init() {
    cacheServiceMock.clearCache();
    fsmResetService = new FsmResetService(cacheServiceMock, cacheServiceDispositivoMock, richiestaRepoMock, deviceRepoMock);
  }

  @Test
  public void skipFsmWrongDevice() throws Exception {
    Richiesta richiesta = buildRichiesta();
    richiesta.setTipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TRACKYCARD));
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT);
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT);
  }

  @Test
  public void skipFmNoCommandAvailable() throws Exception {
    Richiesta richiesta = buildRichiesta().tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TOLL_COLLECT))
                                          .stato(StatoRichiesta.ESAME_DENUNCIA);
    try {
      fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    } catch (Exception e) {
      assertThat(e.getMessage()).startsWith("Command INITIAL");
    }
    assertThat(richiesta.getStato()).isEqualByComparingTo(StatoRichiesta.ESAME_DENUNCIA);
  }

  @Test
  public void startFsmModificaTollCollect() throws Exception {
    Richiesta richiesta = goToIniziale();
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INIZIALE);
  }

  private Richiesta goToIniziale() throws Exception {
    Richiesta richiesta = buildRichiesta().tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.TOLL_COLLECT));
    fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    return richiesta;
  }

  @Test
  public void fromInizialeToAttivoEvaso() throws Exception {
    Richiesta richiesta = goToIniziale();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void fromInizialeToOrdineSospeso() throws Exception {
    Richiesta richiesta = goToOrdineSospeso();
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ORDINE_SOSPESO);
  }

  private Richiesta goToOrdineSospeso() throws Exception {
    Richiesta richiesta = goToIniziale();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_SOSPENDI, richiesta);
    return richiesta;
  }

  @Test
  public void fromInizialeToRichiestaRifiutata() throws Exception {
    Richiesta richiesta = goToIniziale();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_RIFIUTATA_RICHIESTA, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.RICHIESTA_RIFIUTATA);
  }

  @Test
  public void fromOrdineSospesoToRichiestaRifiutata() throws Exception {
    Richiesta richiesta = goToOrdineSospeso();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_RIFIUTATA_RICHIESTA, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.RICHIESTA_RIFIUTATA);
  }

  @Test
  public void fromOrdineSospesoToAttivoEvaso() throws Exception {
    Richiesta richiesta = goToOrdineSospeso();
    fsm.executeCommandToChangeState(RichiestaEvent.MU_ATTIVO, richiesta);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void executeStateMachineFromVerificaRientroDispositivo() throws FsmExecuteCommandException {
    Richiesta request = buildRichiesta();
    request.stato(StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO)
           .tipo(TipoRichiesta.MALFUNZIONAMENTO);
    request.setTipoDispositivo(newTipoDispositivo(TipoDispositivoEnum.TOLL_COLLECT));
    when(richiestaRepoMock.findOneByIdentificativo(request.getIdentificativo())).thenReturn(request);
    fsmResetService.putInCacheFsm(request, fsm.getStateMachine());
    fsmResetService.resetStateFsm(request.getIdentificativo(), StatoRichiesta.VERIFICA_RIENTRO_DISPOSITIVO);

    Richiesta requestChanged = fsm.executeCommandToChangeState(RichiestaEvent.MU_DISPOSITIVO_RIENTRATO, request);
    assertThat(requestChanged).isNotNull();
    assertThat(requestChanged.getStato()).isEqualTo(StatoRichiesta.DISPOSITIVO_DISATTIVATO);
  }

  private TipoDispositivo newTipoDispositivo(TipoDispositivoEnum deviceType) {
    return new TipoDispositivo().nome(deviceType);
  }

  private Richiesta buildRichiesta() {
    Richiesta richiesta = new Richiesta();
    richiesta.setId(5678L);
    richiesta.setIdentificativo(UUID.randomUUID()
                                    .toString());
    richiesta.setTipo(TipoRichiesta.FURTO);
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TOLLCOLLECT);
    richiesta.setOrdineCliente(newOrdineCliente());
    richiesta.setContratto(new Contratto().clienteFai(richiesta.getOrdineCliente()
                                                               .getClienteAssegnatario()));
    richiesta.setAssociazione(TARGA_TEST);
    richiesta.setNewTargaNazione(TARGA_NAZIONE_TEST);
    return richiesta;
  }

  private OrdineCliente newOrdineCliente() {
    return new OrdineCliente().clienteAssegnatario(newClienteFai());
  }

  private ClienteFai newClienteFai() {
    return clienteFaiRepo.findOneByCodiceCliente(CODICE_AZIENDA);
  }

}
