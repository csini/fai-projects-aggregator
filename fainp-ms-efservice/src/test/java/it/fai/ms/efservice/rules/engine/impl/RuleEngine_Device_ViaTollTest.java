package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleWeight;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;

public class RuleEngine_Device_ViaTollTest {

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

  @Test
  public void testTrainsetNotNullAndGrossWeightGreaterThan3500() {

    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO 6";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setTrainsetGrossWeight(3501);
    weight.setLoneVehicleGrossWeight(100);

    RuleOutcome ruleOutcome = new RuleEngine_Device_ViaToll(newRuleContextDeviceType(), ruleContextVehicle,
                                                            newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testTrainsetNotNullAndGrossWeightLessOrEqualThan3500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO 6";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setTrainsetGrossWeight(3500);
    weight.setLoneVehicleGrossWeight(100);

    RuleOutcome ruleOutcome = new RuleEngine_Device_ViaToll(newRuleContextDeviceType(), ruleContextVehicle,
                                                            newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
  }

  @Test
  public void testTrainsetNullAndGrossWeightGreaterThan3500() {

    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO 6";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setTrainsetGrossWeight(null);
    weight.setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_ViaToll(newRuleContextDeviceType(), ruleContextVehicle,
                                                            newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testTrainsetNullAndGrossWeightLessOrEqualThan3500() {

    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("");
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO 6";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setTrainsetGrossWeight(null);
    weight.setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_ViaToll(newRuleContextDeviceType(), ruleContextVehicle,
                                                            newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
  }

}
