package it.fai.ms.efservice.service.fsm.domain.enumaration;

/**
 * @author Luca Vassallo
 */
public enum EventTest {
  T1, T2, T3, T4, T5;
}
