package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_TollCollectTest {

  @Test
  public void testServiceIsGermaniaAndGrossIsNull() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setAxes(1);
    newRuleContextVehicle.setNumberChassis("::numberChassis::");
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Gross weight must be set");
  }

  @Test
  public void testServiceIsGermaniaFailedAxesNotSet() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setAxes(null);
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(7500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("axes must be present");
  }

  @Test
  public void testServiceIsGermaniaFailedNumberChassis() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setAxes(1);
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(7500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle must be set Number Chassis");
  }

  @Test
  public void testServiceIsGermaniaAndGrossLessThan7500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setNumberChassis("::numberChassis::");
    newRuleContextVehicle.setCountry("IT");
    newRuleContextVehicle.setEuroClass("EURO2");
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(7499);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Vehicle gross weight is less than 7500");
  }

  @Test
  public void testServiceIsGermaniaAndGrossEqualThan7500() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(7500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("axes must be present");
  }

  @Test
  public void testServiceIsGermaniaAndGrossGreaterThan7500AndAxesAndNumberChassisAreSet() {
    RuleEngineServiceType newRuleContextServiceType = newRuleContextServiceType("PEDAGGI_GERMANIA");
    RuleEngineVehicle newRuleContextVehicle = newRuleContextVehicle();
    newRuleContextVehicle.setNumberChassis("::numberChassis::");
    newRuleContextVehicle.setAxes(1);
    newRuleContextVehicle.getWeight()
                         .setLoneVehicleGrossWeight(7501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TollCollect(newRuleContextDeviceType(), newRuleContextVehicle,
                                                                newRuleContextServiceType).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceName) {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId(serviceName));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

}
