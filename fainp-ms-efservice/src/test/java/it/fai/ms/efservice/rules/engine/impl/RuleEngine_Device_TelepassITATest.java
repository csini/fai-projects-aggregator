package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleDieselParticulateFilter;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleWeight;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_TelepassITATest {

  private enum vehicleType {
    MOTOCARRO, FURGONE, AUTOVETTURA, AUTOGRU, MOTRICE, TRATTORE;
  }

  private enum serviceType {
    AREA_C, TESSERA_PREMIUM, TESSERA_PREMIUM_TRUCK;
  }

  @Test
  public void testServiceTypeAreaC() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle type not AUTOBUS length must be <= 7,5mt");
  }

  @Test
  public void testServiceTypePremium() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
  }

  @Test
  public void testServiceTypePremiumTruck() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
  }

  @Test
  public void testServiceType() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle type not AUTOBUS length must be <= 7,5mt");
  }

  @Test
  public void testServiceTypeNotAreaC() {
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), newRuleContextVehicle(),
                                                                newRuleContextServiceType(null)).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testAreaC_VehicleAutobus() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle("AUTOBUS");
    ruleContextVehicle.setLength(11f);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testAreaC_VehicleNotAutobus_lengthVehicleGreaterThat7() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(7.5001f);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle type not AUTOBUS length must be <= 7,5mt");

  }

  @Test
  public void testAreaC_VehicleNotAutobus_lengthVehicleMinusThat7() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(7.50000f);
    ruleContextVehicle.setFuelType("METANO");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testAreaC_VehicleBenzinaEuro0() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO0");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle benzina euro must be >= 1");
  }

  @Test
  public void testAreaC_VehicleBenzinaEuro1() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testAreaC_VehicleDieselEuro2() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    ruleContextVehicle.setEuroClass("EURO2");
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle diesel euro must be > 3");
  }

  @Test
  public void testAreaC_VehicleDieselEuro3NoFap() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO3";
    ruleContextVehicle.setEuroClass(euroClass);
    ruleContextVehicle.setDieselParticulateFilter(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle diesel euro must be > 3");
  }

  @Test
  public void testAreaC_VehicleDieselEuro3Fap() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO3";
    ruleContextVehicle.setEuroClass(euroClass);
    ruleContextVehicle.setDieselParticulateFilter(new RuleEngineVehicleDieselParticulateFilter(3));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle diesel euro must be > 3");
  }

  @Test
  public void testAreaC_VehicleDieselEuro4NoFap() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("TRATTORE");
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO4";
    ruleContextVehicle.setEuroClass(euroClass);
    ruleContextVehicle.setDieselParticulateFilter(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle diesel euro 4 must have FAP");
  }

  @Test
  public void testAreaC_VehicleDieselEuro4Fap() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("MOTOCARRO");
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO4";
    ruleContextVehicle.setEuroClass(euroClass);
    ruleContextVehicle.setDieselParticulateFilter(new RuleEngineVehicleDieselParticulateFilter(3));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testAreaC_VehicleDieselEuro5() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(6.99f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.AREA_C.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremiumTruck_allWeightNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "At least one value between Vehicle tare, gross and trainset weight must be set");
  }

  @Test
  public void testPremium_allWeightNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(null);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "At least one value between Vehicle tare, gross and trainset weight must be set");
  }

  @Test
  public void testPremiumTruck_TareNotNullAndLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(3500);
    weight.setLoneVehicleGrossWeight(100);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle tare weight must be > 3500 Kg");
  }

  @Test
  public void testPremiumTruck_TareNotNullAndGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(3501);
    weight.setLoneVehicleGrossWeight(100);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNotNullAndLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(3500);
    weight.setLoneVehicleGrossWeight(100);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNotNullAndGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(3501);
    weight.setLoneVehicleGrossWeight(100);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle tare weight must be <= 3500 Kg");
  }

  @Test
  public void testPremiumTruck_TareNullAndGrossWeightLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle gross weight must be > 3500 Kg");
  }

  @Test
  public void testPremiumTruck_TareNullAndGrossWeightGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNullAndGrossWeightLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNullAndGrossWeightGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle gross weight must be <= 3500 Kg");
  }

  @Test
  public void testPremiumTruck_TareNullAndGrossNullAndTrainsetWeightLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle trainset weight must be > 3500 Kg");
  }

  @Test
  public void testPremiumTruck_TareNullAndGrossNullAndTrainsetWeightGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM_TRUCK.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNullAndGrossNullAndTrainsetWeightLessOrEqualThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(3500);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testPremium_TareNullAndGrossNullAndTrainsetWeightGreaterThan3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setType("AUTOVETTURA");
    ruleContextVehicle.setLength(11f);
    ruleContextVehicle.setFuelType("DIESEL");
    String euroClass = "EURO5";
    ruleContextVehicle.setEuroClass(euroClass);
    RuleEngineVehicleWeight weight = ruleContextVehicle.getWeight();
    weight.setLoneVehicleTareWeight(null);
    weight.setLoneVehicleGrossWeight(null);
    weight.setTrainsetGrossWeight(3501);
    RuleOutcome ruleOutcome = new RuleEngine_Device_TelepassITA(newRuleContextDeviceType(), ruleContextVehicle,
                                                                newRuleContextServiceType(serviceType.TESSERA_PREMIUM.name())).executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertFailureMessage(ruleOutcome, "Vehicle trainset weight must be <= 3500 Kg");
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType(String serviceTypeName) {
    RuleEngineServiceTypeId ruleEngineServiceTypeId = new RuleEngineServiceTypeId("::serviceTypeId::");
    if (StringUtils.isNotBlank(serviceTypeName)) {
      ruleEngineServiceTypeId = new RuleEngineServiceTypeId(serviceTypeName);
    }
    return new RuleEngineServiceType(ruleEngineServiceTypeId);
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    RuleEngineVehicle ruleEngineVehicle = new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
    vehicleType[] values = vehicleType.values();
    int size = values.length;
    int randomAccessNumber = (int) (Math.random() * (size - 1));
    vehicleType vehicleType = values[randomAccessNumber];
    ruleEngineVehicle.setType(vehicleType.name());
    return ruleEngineVehicle;
  }

  private RuleEngineVehicle newRuleContextVehicle(String typeVehicle) {
    RuleEngineVehicle ruleEngineVehicle = new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
    ruleEngineVehicle.setType(typeVehicle);
    return ruleEngineVehicle;
  }

  private void assertFailureMessage(RuleOutcome ruleOutcome, String msgExpected) {
    Optional<RuleFailure> failure = ruleOutcome.getFailure();
    assertThat(failure.isPresent()).isTrue();
    RuleFailure ruleFailure = failure.get();
    String msg = ruleFailure.getMess();
    assertThat(msg).isEqualTo(msgExpected);
  }

}
