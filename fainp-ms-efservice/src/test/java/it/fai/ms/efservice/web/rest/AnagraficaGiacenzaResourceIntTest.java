package it.fai.ms.efservice.web.rest;

import static it.fai.ms.efservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.AnagraficaGiacenza;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.AnagraficaGiacenzaRepository;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.OrdineFornitoreRepository;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.AnagraficaGiacenzaService;
import it.fai.ms.efservice.service.dto.AnagraficaGiacenzaDTO;
import it.fai.ms.efservice.service.mapper.AnagraficaGiacenzaMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the AnagraficaGiacenzaResource REST controller.
 *
 * @see AnagraficaGiacenzaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class AnagraficaGiacenzaResourceIntTest {

  private static final String DEFAULT_TIPO_DISPOSITIVO = TipoDispositivoEnum.TELEPASS_ITALIANO.name();
  private static final String UPDATED_TIPO_DISPOSITIVO = "BBBBBBBBBB";

  private static final String DEFAULT_PRODUTTORE_NOME = "AAAAAAAAAA";
  private static final String UPDATED_PRODUTTORE_NOME = "BBBBBBBBBB";

  private static final Integer DEFAULT_GIACENZA_MINIMA = 1;
  private static final Integer UPDATED_GIACENZA_MINIMA = 2;

  private static final Integer DEFAULT_ALERT_RIORDINO = 1;
  private static final Integer UPDATED_ALERT_RIORDINO = 2;

  private static final Integer DEFAULT_QUANTITA_ULTIMO_RIORDINO = 1;
  private static final Integer UPDATED_QUANTITA_ULTIMO_RIORDINO = 2;

  private static final Boolean DEFAULT_NOTIFICATION_SEND = false;
  private static final Boolean UPDATED_NOTIFICATION_SEND = true;

  @Autowired
  private AnagraficaGiacenzaRepository anagraficaGiacenzaRepository;

  @Autowired
  private AnagraficaGiacenzaMapper anagraficaGiacenzaMapper;

  @Autowired
  private AnagraficaGiacenzaService anagraficaGiacenzaService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restAnagraficaGiacenzaMockMvc;

  private AnagraficaGiacenza anagraficaGiacenza;

  @Autowired
  private DispositivoRepository dispositivoRepository;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepository;

  @Autowired
  private OrdineFornitoreRepository ordineFornitoreRepository;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final AnagraficaGiacenzaResource anagraficaGiacenzaResource = new AnagraficaGiacenzaResource(anagraficaGiacenzaService);
    this.restAnagraficaGiacenzaMockMvc = MockMvcBuilders.standaloneSetup(anagraficaGiacenzaResource)
                                                        .setCustomArgumentResolvers(pageableArgumentResolver)
                                                        .setControllerAdvice(exceptionTranslator)
                                                        .setConversionService(createFormattingConversionService())
                                                        .setMessageConverters(jacksonMessageConverter)
                                                        .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static AnagraficaGiacenza createEntity(EntityManager em) {
    AnagraficaGiacenza anagraficaGiacenza = new AnagraficaGiacenza().tipoDispositivo(DEFAULT_TIPO_DISPOSITIVO)
                                                                    .produttoreNome(DEFAULT_PRODUTTORE_NOME)
                                                                    .giacenzaMinima(DEFAULT_GIACENZA_MINIMA)
                                                                    .alertRiordino(DEFAULT_ALERT_RIORDINO)
                                                                    .quantitaUltimoRiordino(DEFAULT_QUANTITA_ULTIMO_RIORDINO)
                                                                    .notificationSend(DEFAULT_NOTIFICATION_SEND);
    return anagraficaGiacenza;
  }

  @Before
  public void initTest() {
    anagraficaGiacenza = createEntity(em);
  }

  @Test
  @Transactional
  public void createAnagraficaGiacenza() throws Exception {
    int databaseSizeBeforeCreate = anagraficaGiacenzaRepository.findAll()
                                                               .size();

    // Create the AnagraficaGiacenza
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaMapper.toDto(anagraficaGiacenza);
    restAnagraficaGiacenzaMockMvc.perform(post("/api/anagrafica-giacenzas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                           .content(TestUtil.convertObjectToJsonBytes(anagraficaGiacenzaDTO)))
                                 .andExpect(status().isCreated());

    // Validate the AnagraficaGiacenza in the database
    List<AnagraficaGiacenza> anagraficaGiacenzaList = anagraficaGiacenzaRepository.findAll();
    assertThat(anagraficaGiacenzaList).hasSize(databaseSizeBeforeCreate + 1);
    AnagraficaGiacenza testAnagraficaGiacenza = anagraficaGiacenzaList.get(anagraficaGiacenzaList.size() - 1);
    assertThat(testAnagraficaGiacenza.getTipoDispositivo()).isEqualTo(DEFAULT_TIPO_DISPOSITIVO);
    assertThat(testAnagraficaGiacenza.getProduttoreNome()).isEqualTo(DEFAULT_PRODUTTORE_NOME);
    assertThat(testAnagraficaGiacenza.getGiacenzaMinima()).isEqualTo(DEFAULT_GIACENZA_MINIMA);
    assertThat(testAnagraficaGiacenza.getAlertRiordino()).isEqualTo(DEFAULT_ALERT_RIORDINO);
    assertThat(testAnagraficaGiacenza.getQuantitaUltimoRiordino()).isEqualTo(DEFAULT_QUANTITA_ULTIMO_RIORDINO);
    assertThat(testAnagraficaGiacenza.isNotificationSend()).isEqualTo(DEFAULT_NOTIFICATION_SEND);
  }

  @Test
  @Transactional
  public void createAnagraficaGiacenzaWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = anagraficaGiacenzaRepository.findAll()
                                                               .size();

    // Create the AnagraficaGiacenza with an existing ID
    anagraficaGiacenza.setId(1L);
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaMapper.toDto(anagraficaGiacenza);

    // An entity with an existing ID cannot be created, so this API call must fail
    restAnagraficaGiacenzaMockMvc.perform(post("/api/anagrafica-giacenzas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                           .content(TestUtil.convertObjectToJsonBytes(anagraficaGiacenzaDTO)))
                                 .andExpect(status().isBadRequest());

    // Validate the AnagraficaGiacenza in the database
    List<AnagraficaGiacenza> anagraficaGiacenzaList = anagraficaGiacenzaRepository.findAll();
    assertThat(anagraficaGiacenzaList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void getAllAnagraficaGiacenzas() throws Exception {

    TipoDispositivo tipoDispositivo = TipoDispositivoResourceIntTest.createEntity(null);
    tipoDispositivo.setNome(TipoDispositivoEnum.TELEPASS_ITALIANO);

    tipoDispositivoRepository.saveAndFlush(tipoDispositivo);

    Dispositivo dispositivo = DispositivoResourceIntTest.createEntity(null);
    dispositivo.setStato(StatoDispositivo.IN_DEPOSITO);
    dispositivo.setTipoDispositivo(tipoDispositivo);

    dispositivoRepository.saveAndFlush(dispositivo);

    // Initialize the database
    anagraficaGiacenzaRepository.saveAndFlush(anagraficaGiacenza);

    OrdineFornitore ordineFornitore = OrdineFornitoreResourceIntTest.createEntity(null);
    ordineFornitore.setAnagraficaGiacenza(anagraficaGiacenza);

    ordineFornitoreRepository.saveAndFlush(ordineFornitore);

    // Get all the anagraficaGiacenzaList
    restAnagraficaGiacenzaMockMvc.perform(get("/api/anagrafica-giacenzas?sort=id,desc"))
                                 .andExpect(status().isOk())
                                 .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                 .andExpect(jsonPath("$.[*].id").value(hasItem(anagraficaGiacenza.getId()
                                                                                                 .intValue())))
                                 .andExpect(jsonPath("$.[*].tipoDispositivo").value(hasItem(DEFAULT_TIPO_DISPOSITIVO.toString())))
                                 .andExpect(jsonPath("$.[*].produttoreNome").value(hasItem(DEFAULT_PRODUTTORE_NOME.toString())))
                                 .andExpect(jsonPath("$.[*].giacenzaMinima").value(hasItem(DEFAULT_GIACENZA_MINIMA)))
                                 .andExpect(jsonPath("$.[*].alertRiordino").value(hasItem(DEFAULT_ALERT_RIORDINO)))
                                 .andExpect(jsonPath("$.[*].quantitaUltimoRiordino").value(hasItem(DEFAULT_QUANTITA_ULTIMO_RIORDINO)))
                                 .andExpect(jsonPath("$.[*].notificationSend").value(hasItem(DEFAULT_NOTIFICATION_SEND.booleanValue())))
                                 .andExpect(jsonPath("$.[*].totaleDispositiviInGiacenza").value(hasItem(1)));
  }

  @Test
  @Transactional
  public void getAnagraficaGiacenza() throws Exception {
    // Initialize the database
    anagraficaGiacenzaRepository.saveAndFlush(anagraficaGiacenza);

    // Get the anagraficaGiacenza
    restAnagraficaGiacenzaMockMvc.perform(get("/api/anagrafica-giacenzas/{id}", anagraficaGiacenza.getId()))
                                 .andExpect(status().isOk())
                                 .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                 .andExpect(jsonPath("$.id").value(anagraficaGiacenza.getId()
                                                                                     .intValue()))
                                 .andExpect(jsonPath("$.tipoDispositivo").value(DEFAULT_TIPO_DISPOSITIVO.toString()))
                                 .andExpect(jsonPath("$.produttoreNome").value(DEFAULT_PRODUTTORE_NOME.toString()))
                                 .andExpect(jsonPath("$.giacenzaMinima").value(DEFAULT_GIACENZA_MINIMA))
                                 .andExpect(jsonPath("$.alertRiordino").value(DEFAULT_ALERT_RIORDINO))
                                 .andExpect(jsonPath("$.quantitaUltimoRiordino").value(DEFAULT_QUANTITA_ULTIMO_RIORDINO))
                                 .andExpect(jsonPath("$.notificationSend").value(DEFAULT_NOTIFICATION_SEND.booleanValue()));
  }

  @Test
  @Transactional
  public void getNonExistingAnagraficaGiacenza() throws Exception {
    // Get the anagraficaGiacenza
    restAnagraficaGiacenzaMockMvc.perform(get("/api/anagrafica-giacenzas/{id}", Long.MAX_VALUE))
                                 .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateAnagraficaGiacenza() throws Exception {
    // Initialize the database
    anagraficaGiacenzaRepository.saveAndFlush(anagraficaGiacenza);
    int databaseSizeBeforeUpdate = anagraficaGiacenzaRepository.findAll()
                                                               .size();

    // Update the anagraficaGiacenza
    AnagraficaGiacenza updatedAnagraficaGiacenza = anagraficaGiacenzaRepository.findOne(anagraficaGiacenza.getId());
    // Disconnect from session so that the updates on updatedAnagraficaGiacenza are not directly saved in db
    em.detach(updatedAnagraficaGiacenza);
    updatedAnagraficaGiacenza.tipoDispositivo(UPDATED_TIPO_DISPOSITIVO)
                             .produttoreNome(UPDATED_PRODUTTORE_NOME)
                             .giacenzaMinima(UPDATED_GIACENZA_MINIMA)
                             .alertRiordino(UPDATED_ALERT_RIORDINO)
                             .quantitaUltimoRiordino(UPDATED_QUANTITA_ULTIMO_RIORDINO)
                             .notificationSend(UPDATED_NOTIFICATION_SEND);
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaMapper.toDto(updatedAnagraficaGiacenza);

    restAnagraficaGiacenzaMockMvc.perform(put("/api/anagrafica-giacenzas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                          .content(TestUtil.convertObjectToJsonBytes(anagraficaGiacenzaDTO)))
                                 .andExpect(status().isOk());

    // Validate the AnagraficaGiacenza in the database
    List<AnagraficaGiacenza> anagraficaGiacenzaList = anagraficaGiacenzaRepository.findAll();
    assertThat(anagraficaGiacenzaList).hasSize(databaseSizeBeforeUpdate);
    AnagraficaGiacenza testAnagraficaGiacenza = anagraficaGiacenzaList.get(anagraficaGiacenzaList.size() - 1);
    assertThat(testAnagraficaGiacenza.getTipoDispositivo()).isEqualTo(UPDATED_TIPO_DISPOSITIVO);
    assertThat(testAnagraficaGiacenza.getProduttoreNome()).isEqualTo(UPDATED_PRODUTTORE_NOME);
    assertThat(testAnagraficaGiacenza.getGiacenzaMinima()).isEqualTo(UPDATED_GIACENZA_MINIMA);
    assertThat(testAnagraficaGiacenza.getAlertRiordino()).isEqualTo(UPDATED_ALERT_RIORDINO);
    assertThat(testAnagraficaGiacenza.getQuantitaUltimoRiordino()).isEqualTo(UPDATED_QUANTITA_ULTIMO_RIORDINO);
    assertThat(testAnagraficaGiacenza.isNotificationSend()).isEqualTo(UPDATED_NOTIFICATION_SEND);
  }

  @Test
  @Transactional
  public void updateNonExistingAnagraficaGiacenza() throws Exception {
    int databaseSizeBeforeUpdate = anagraficaGiacenzaRepository.findAll()
                                                               .size();

    // Create the AnagraficaGiacenza
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO = anagraficaGiacenzaMapper.toDto(anagraficaGiacenza);

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restAnagraficaGiacenzaMockMvc.perform(put("/api/anagrafica-giacenzas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                          .content(TestUtil.convertObjectToJsonBytes(anagraficaGiacenzaDTO)))
                                 .andExpect(status().isCreated());

    // Validate the AnagraficaGiacenza in the database
    List<AnagraficaGiacenza> anagraficaGiacenzaList = anagraficaGiacenzaRepository.findAll();
    assertThat(anagraficaGiacenzaList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteAnagraficaGiacenza() throws Exception {
    // Initialize the database
    anagraficaGiacenzaRepository.saveAndFlush(anagraficaGiacenza);
    int databaseSizeBeforeDelete = anagraficaGiacenzaRepository.findAll()
                                                               .size();

    // Get the anagraficaGiacenza
    restAnagraficaGiacenzaMockMvc.perform(delete("/api/anagrafica-giacenzas/{id}",
                                                 anagraficaGiacenza.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                                 .andExpect(status().isOk());

    // Validate the database is empty
    List<AnagraficaGiacenza> anagraficaGiacenzaList = anagraficaGiacenzaRepository.findAll();
    assertThat(anagraficaGiacenzaList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(AnagraficaGiacenza.class);
    AnagraficaGiacenza anagraficaGiacenza1 = new AnagraficaGiacenza();
    anagraficaGiacenza1.setId(1L);
    AnagraficaGiacenza anagraficaGiacenza2 = new AnagraficaGiacenza();
    anagraficaGiacenza2.setId(anagraficaGiacenza1.getId());
    assertThat(anagraficaGiacenza1).isEqualTo(anagraficaGiacenza2);
    anagraficaGiacenza2.setId(2L);
    assertThat(anagraficaGiacenza1).isNotEqualTo(anagraficaGiacenza2);
    anagraficaGiacenza1.setId(null);
    assertThat(anagraficaGiacenza1).isNotEqualTo(anagraficaGiacenza2);
  }

  @Test
  @Transactional
  public void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(AnagraficaGiacenzaDTO.class);
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO1 = new AnagraficaGiacenzaDTO();
    anagraficaGiacenzaDTO1.setId(1L);
    AnagraficaGiacenzaDTO anagraficaGiacenzaDTO2 = new AnagraficaGiacenzaDTO();
    assertThat(anagraficaGiacenzaDTO1).isNotEqualTo(anagraficaGiacenzaDTO2);
    anagraficaGiacenzaDTO2.setId(anagraficaGiacenzaDTO1.getId());
    assertThat(anagraficaGiacenzaDTO1).isEqualTo(anagraficaGiacenzaDTO2);
    anagraficaGiacenzaDTO2.setId(2L);
    assertThat(anagraficaGiacenzaDTO1).isNotEqualTo(anagraficaGiacenzaDTO2);
    anagraficaGiacenzaDTO1.setId(null);
    assertThat(anagraficaGiacenzaDTO1).isNotEqualTo(anagraficaGiacenzaDTO2);
  }

  @Test
  @Transactional
  public void testEntityFromId() {
    assertThat(anagraficaGiacenzaMapper.fromId(42L)
                                       .getId()).isEqualTo(42);
    assertThat(anagraficaGiacenzaMapper.fromId(null)).isNull();
  }
}
