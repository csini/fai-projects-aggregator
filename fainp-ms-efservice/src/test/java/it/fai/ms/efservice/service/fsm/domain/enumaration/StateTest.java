package it.fai.ms.efservice.service.fsm.domain.enumaration;

/**
 * @author Luca Vassallo
 */
public enum StateTest {
  A, B, C, D;
}
