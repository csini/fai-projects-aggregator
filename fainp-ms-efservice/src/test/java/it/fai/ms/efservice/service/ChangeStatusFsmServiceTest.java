package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.asyncjob.StateChangeStatusFsm;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Indirizzo;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.dto.FsmDispositivoDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ChangeStatusFsmServiceTest {

  private final static String COD_CONTRATTO_TE_SIMEONI = "118946365";

  private final static String SERIALE_DISPOSITIVO = "SERIALE-TEST-DISPOSITIVO-";

  @Autowired
  private ChangeStatusFsmService changeStatusFsmService;

  @Autowired
  private TipoDispositivoRepository tipoDispositivoRepo;

  @Autowired
  private ContrattoRepositoryExt contrattoRepo;

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private DispositivoRepositoryExt dispositivoRepo;

  @Autowired
  private FsmAsyncService fsmAsyncService;

  private Dispositivo device;

  private FsmDispositivoDTO fsmDTO;

  @Before
  public void setUp() {
  }

  @After
  public void destroyDataTest() {
    dispositivoRepo.save(device.contratto(null));
    dispositivoRepo.delete(device);
  }

  @Test
  @Transactional
  public void testTelepassIta() {
    setupByDeviceType(TipoDispositivoEnum.TELEPASS_ITALIANO);
    verifyChangeStatus();
  }

  @Test
  @Transactional
  public void testTollCollect() {
    TipoDispositivoEnum deviceType = TipoDispositivoEnum.TOLL_COLLECT;
    setupByDeviceType(deviceType);

    TipoDispositivo tipoDispositivo = tipoDispositivoRepo.findOneByNome(deviceType);
    Indirizzo indirizzoRientro = tipoDispositivo.getIndirizzoRientro();
    tipoDispositivo.setIndirizzoRientro(null);
    tipoDispositivoRepo.save(tipoDispositivo);

    verifyChangeStatus();

    tipoDispositivo.setIndirizzoRientro(indirizzoRientro);
    tipoDispositivoRepo.save(tipoDispositivo);
  }

  @Test
  @Transactional
  public void testFrejus() {
    setupByDeviceType(TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO);
    verifyChangeStatus();
  }

  @Test
  @Transactional
  public void testGranSanBernardo() {
    setupByDeviceType(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);
    verifyChangeStatus();
  }

  public void verifyChangeStatus() {
    FsmDispositivoDTO fsmDTO = new FsmDispositivoDTO();
    Set<String> identificativi = new HashSet<>();
    identificativi.add(device.getIdentificativo());
    fsmDTO.setIdentificativi(identificativi);
    fsmDTO.setOperazione(DispositivoEvent.MU_DA_RESTITUIRE.name());
    StateChangeStatusFsm status = changeStatusFsmService.changeFsmDispositivoStatus(fsmDTO);
    assertThat(status).isEqualTo(StateChangeStatusFsm.SUCCESS);
  }

  private void setupByDeviceType(TipoDispositivoEnum deviceType) {
    TipoDispositivo tipoDispositivo = tipoDispositivoRepo.findOneTipoDispositivoAndServiziByNome(deviceType);
    String nomeProduttore = tipoDispositivo.getProduttore()
                                           .getNome();
    Optional<Contratto> contrattoOpt = contrattoRepo.findFirstByCodContrattoClienteAndProduttore_nome(COD_CONTRATTO_TE_SIMEONI,
                                                                                                      nomeProduttore);
    Contratto contratto = null;
    if (contrattoOpt.isPresent()) {
      contratto = contrattoOpt.get();
    } else {
      String randomUuid = UUID.randomUUID()
                              .toString();
      contratto = new Contratto().clienteFai(newClienteFai(randomUuid.substring(0, 7)))
                                 .codContrattoCliente(COD_CONTRATTO_TE_SIMEONI)
                                 .primario(true)
                                 .dataModificaStato(Instant.now())
                                 .produttore(tipoDispositivo.getProduttore())
                                 .stato(StatoContratto.ATTIVO);
      contratto = contrattoRepo.save(contratto);
    }
    String seriale = SERIALE_DISPOSITIVO + new SimpleDateFormat("yyyyMMdd").format(new Date());
    device = new Dispositivo().stato(StatoDispositivo.ATTIVO)
                              .tipoDispositivo(tipoDispositivo)
                              .contratto(contratto)
                              .seriale(seriale)
                              .dataModificaStato(Instant.now());

    device = dispositivoRepo.save(device);
    HashSet<Dispositivo> devices = new HashSet<>();
    devices.add(device);
    contratto.setDispositivo(devices);
    contrattoRepo.save(contratto);

    fsmDTO = new FsmDispositivoDTO();
    Set<String> identificativi = new HashSet<>();
    identificativi.add(device.getIdentificativo());
    fsmDTO.setIdentificativi(identificativi);
    fsmDTO.setOperazione(DispositivoEvent.MU_DA_RESTITUIRE.name());
  }

  private ClienteFai newClienteFai(String codiceCliente) {
    ClienteFai clienteFai = new ClienteFai().codiceCliente(codiceCliente).dmlRevisionTimestamp(Instant.now()).via("Via tal dei tali").citta("Brescia");
    return clienteFaiRepo.save(clienteFai);
  }

  public void test001() {
    String identificativoJob = fsmAsyncService.getKeyCacheAndPushDefaultValue("TEST-ASYNC-SERVICE-"
                                                                              + new SimpleDateFormat("yyyyMMdd").format(new Date()));
    fsmAsyncService.changeAsyncFsmDispositivo(identificativoJob, fsmDTO);
  }

}
