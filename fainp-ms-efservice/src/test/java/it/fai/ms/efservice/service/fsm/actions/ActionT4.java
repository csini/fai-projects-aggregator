/**
 * 
 */
package it.fai.ms.efservice.service.fsm.actions;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.testdomain.TestFSM;

/**
 * @author Luca Vassallo
 */
public class ActionT4 implements Action<StateTest, EventTest> {

  @Override
  public void execute(StateContext<StateTest, EventTest> context) {
    Object obj = context.getMessageHeader("itemTest");
    if (obj instanceof TestFSM) {
      TestFSM item = (TestFSM) obj;

      if (item.getCampo2() == null) {
        item.setCampo2("");
      }

      item.setCampo2(item.getCampo2() + "T4");
    }

  }

}
