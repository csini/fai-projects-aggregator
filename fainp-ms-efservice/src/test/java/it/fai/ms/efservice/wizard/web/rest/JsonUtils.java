package it.fai.ms.efservice.wizard.web.rest;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public final class JsonUtils {

  public static String readFile(final String _fileName) throws IOException {
    final Path path = FileSystems.getDefault()
                                 .getPath("src/test/resources/json/wizard", _fileName);
    byte[] bytes = Files.readAllBytes(path);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  private JsonUtils() {
  }

}
