package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.StoricoAssociazioneDV;
import it.fai.ms.efservice.repository.StoricoAssociazioneDVRepository;
import it.fai.ms.efservice.service.StoricoAssociazioneDVService;
import it.fai.ms.efservice.service.dto.StoricoAssociazioneDVDTO;
import it.fai.ms.efservice.service.mapper.StoricoAssociazioneDVMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StoricoAssociazioneDVResource REST controller.
 *
 * @see StoricoAssociazioneDVResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class StoricoAssociazioneDVResourceIntTest {

    private static final Instant DEFAULT_DATA_INIZIO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_INIZIO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATA_FINE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_FINE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UUID_VEICOLO = "AAAAAAAAAA";
    private static final String UPDATED_UUID_VEICOLO = "BBBBBBBBBB";

    @Autowired
    private StoricoAssociazioneDVRepository storicoAssociazioneDVRepository;

    @Autowired
    private StoricoAssociazioneDVMapper storicoAssociazioneDVMapper;

    @Autowired
    private StoricoAssociazioneDVService storicoAssociazioneDVService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStoricoAssociazioneDVMockMvc;

    private StoricoAssociazioneDV storicoAssociazioneDV;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StoricoAssociazioneDVResource storicoAssociazioneDVResource = new StoricoAssociazioneDVResource(storicoAssociazioneDVService);
        this.restStoricoAssociazioneDVMockMvc = MockMvcBuilders.standaloneSetup(storicoAssociazioneDVResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StoricoAssociazioneDV createEntity(EntityManager em) {
        StoricoAssociazioneDV storicoAssociazioneDV = new StoricoAssociazioneDV()
            .dataInizio(DEFAULT_DATA_INIZIO)
            .dataFine(DEFAULT_DATA_FINE)
            .uuidVeicolo(DEFAULT_UUID_VEICOLO);
        return storicoAssociazioneDV;
    }

    @Before
    public void initTest() {
        storicoAssociazioneDV = createEntity(em);
    }

    @Test
    @Transactional
    public void createStoricoAssociazioneDV() throws Exception {
        int databaseSizeBeforeCreate = storicoAssociazioneDVRepository.findAll().size();

        // Create the StoricoAssociazioneDV
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = storicoAssociazioneDVMapper.toDto(storicoAssociazioneDV);
        restStoricoAssociazioneDVMockMvc.perform(post("/api/storico-associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storicoAssociazioneDVDTO)))
            .andExpect(status().isCreated());

        // Validate the StoricoAssociazioneDV in the database
        List<StoricoAssociazioneDV> storicoAssociazioneDVList = storicoAssociazioneDVRepository.findAll();
        assertThat(storicoAssociazioneDVList).hasSize(databaseSizeBeforeCreate + 1);
        StoricoAssociazioneDV testStoricoAssociazioneDV = storicoAssociazioneDVList.get(storicoAssociazioneDVList.size() - 1);
        assertThat(testStoricoAssociazioneDV.getDataInizio()).isEqualTo(DEFAULT_DATA_INIZIO);
        assertThat(testStoricoAssociazioneDV.getDataFine()).isEqualTo(DEFAULT_DATA_FINE);
        assertThat(testStoricoAssociazioneDV.getUuidVeicolo()).isEqualTo(DEFAULT_UUID_VEICOLO);
    }

    @Test
    @Transactional
    public void createStoricoAssociazioneDVWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = storicoAssociazioneDVRepository.findAll().size();

        // Create the StoricoAssociazioneDV with an existing ID
        storicoAssociazioneDV.setId(1L);
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = storicoAssociazioneDVMapper.toDto(storicoAssociazioneDV);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStoricoAssociazioneDVMockMvc.perform(post("/api/storico-associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storicoAssociazioneDVDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StoricoAssociazioneDV in the database
        List<StoricoAssociazioneDV> storicoAssociazioneDVList = storicoAssociazioneDVRepository.findAll();
        assertThat(storicoAssociazioneDVList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllStoricoAssociazioneDVS() throws Exception {
        // Initialize the database
        storicoAssociazioneDVRepository.saveAndFlush(storicoAssociazioneDV);

        // Get all the storicoAssociazioneDVList
        restStoricoAssociazioneDVMockMvc.perform(get("/api/storico-associazione-dvs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(storicoAssociazioneDV.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataInizio").value(hasItem(DEFAULT_DATA_INIZIO.toString())))
            .andExpect(jsonPath("$.[*].dataFine").value(hasItem(DEFAULT_DATA_FINE.toString())))
            .andExpect(jsonPath("$.[*].uuidVeicolo").value(hasItem(DEFAULT_UUID_VEICOLO.toString())));
    }

    @Test
    @Transactional
    public void getStoricoAssociazioneDV() throws Exception {
        // Initialize the database
        storicoAssociazioneDVRepository.saveAndFlush(storicoAssociazioneDV);

        // Get the storicoAssociazioneDV
        restStoricoAssociazioneDVMockMvc.perform(get("/api/storico-associazione-dvs/{id}", storicoAssociazioneDV.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(storicoAssociazioneDV.getId().intValue()))
            .andExpect(jsonPath("$.dataInizio").value(DEFAULT_DATA_INIZIO.toString()))
            .andExpect(jsonPath("$.dataFine").value(DEFAULT_DATA_FINE.toString()))
            .andExpect(jsonPath("$.uuidVeicolo").value(DEFAULT_UUID_VEICOLO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingStoricoAssociazioneDV() throws Exception {
        // Get the storicoAssociazioneDV
        restStoricoAssociazioneDVMockMvc.perform(get("/api/storico-associazione-dvs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStoricoAssociazioneDV() throws Exception {
        // Initialize the database
        storicoAssociazioneDVRepository.saveAndFlush(storicoAssociazioneDV);
        int databaseSizeBeforeUpdate = storicoAssociazioneDVRepository.findAll().size();

        // Update the storicoAssociazioneDV
        StoricoAssociazioneDV updatedStoricoAssociazioneDV = storicoAssociazioneDVRepository.findOne(storicoAssociazioneDV.getId());
        updatedStoricoAssociazioneDV
            .dataInizio(UPDATED_DATA_INIZIO)
            .dataFine(UPDATED_DATA_FINE)
            .uuidVeicolo(UPDATED_UUID_VEICOLO);
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = storicoAssociazioneDVMapper.toDto(updatedStoricoAssociazioneDV);

        restStoricoAssociazioneDVMockMvc.perform(put("/api/storico-associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storicoAssociazioneDVDTO)))
            .andExpect(status().isOk());

        // Validate the StoricoAssociazioneDV in the database
        List<StoricoAssociazioneDV> storicoAssociazioneDVList = storicoAssociazioneDVRepository.findAll();
        assertThat(storicoAssociazioneDVList).hasSize(databaseSizeBeforeUpdate);
        StoricoAssociazioneDV testStoricoAssociazioneDV = storicoAssociazioneDVList.get(storicoAssociazioneDVList.size() - 1);
        assertThat(testStoricoAssociazioneDV.getDataInizio()).isEqualTo(UPDATED_DATA_INIZIO);
        assertThat(testStoricoAssociazioneDV.getDataFine()).isEqualTo(UPDATED_DATA_FINE);
        assertThat(testStoricoAssociazioneDV.getUuidVeicolo()).isEqualTo(UPDATED_UUID_VEICOLO);
    }

    @Test
    @Transactional
    public void updateNonExistingStoricoAssociazioneDV() throws Exception {
        int databaseSizeBeforeUpdate = storicoAssociazioneDVRepository.findAll().size();

        // Create the StoricoAssociazioneDV
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO = storicoAssociazioneDVMapper.toDto(storicoAssociazioneDV);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStoricoAssociazioneDVMockMvc.perform(put("/api/storico-associazione-dvs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storicoAssociazioneDVDTO)))
            .andExpect(status().isCreated());

        // Validate the StoricoAssociazioneDV in the database
        List<StoricoAssociazioneDV> storicoAssociazioneDVList = storicoAssociazioneDVRepository.findAll();
        assertThat(storicoAssociazioneDVList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStoricoAssociazioneDV() throws Exception {
        // Initialize the database
        storicoAssociazioneDVRepository.saveAndFlush(storicoAssociazioneDV);
        int databaseSizeBeforeDelete = storicoAssociazioneDVRepository.findAll().size();

        // Get the storicoAssociazioneDV
        restStoricoAssociazioneDVMockMvc.perform(delete("/api/storico-associazione-dvs/{id}", storicoAssociazioneDV.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<StoricoAssociazioneDV> storicoAssociazioneDVList = storicoAssociazioneDVRepository.findAll();
        assertThat(storicoAssociazioneDVList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StoricoAssociazioneDV.class);
        StoricoAssociazioneDV storicoAssociazioneDV1 = new StoricoAssociazioneDV();
        storicoAssociazioneDV1.setId(1L);
        StoricoAssociazioneDV storicoAssociazioneDV2 = new StoricoAssociazioneDV();
        storicoAssociazioneDV2.setId(storicoAssociazioneDV1.getId());
        assertThat(storicoAssociazioneDV1).isEqualTo(storicoAssociazioneDV2);
        storicoAssociazioneDV2.setId(2L);
        assertThat(storicoAssociazioneDV1).isNotEqualTo(storicoAssociazioneDV2);
        storicoAssociazioneDV1.setId(null);
        assertThat(storicoAssociazioneDV1).isNotEqualTo(storicoAssociazioneDV2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StoricoAssociazioneDVDTO.class);
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO1 = new StoricoAssociazioneDVDTO();
        storicoAssociazioneDVDTO1.setId(1L);
        StoricoAssociazioneDVDTO storicoAssociazioneDVDTO2 = new StoricoAssociazioneDVDTO();
        assertThat(storicoAssociazioneDVDTO1).isNotEqualTo(storicoAssociazioneDVDTO2);
        storicoAssociazioneDVDTO2.setId(storicoAssociazioneDVDTO1.getId());
        assertThat(storicoAssociazioneDVDTO1).isEqualTo(storicoAssociazioneDVDTO2);
        storicoAssociazioneDVDTO2.setId(2L);
        assertThat(storicoAssociazioneDVDTO1).isNotEqualTo(storicoAssociazioneDVDTO2);
        storicoAssociazioneDVDTO1.setId(null);
        assertThat(storicoAssociazioneDVDTO1).isNotEqualTo(storicoAssociazioneDVDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(storicoAssociazioneDVMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(storicoAssociazioneDVMapper.fromId(null)).isNull();
    }
}
