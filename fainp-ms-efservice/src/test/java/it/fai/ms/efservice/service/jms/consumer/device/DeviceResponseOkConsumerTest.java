package it.fai.ms.efservice.service.jms.consumer.device;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.efservice.FileElaborationDetail;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.service.RaggruppamentoRichiesteOrdineFornitoreServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class DeviceResponseOkConsumerTest {

  @Autowired
  private RaggruppamentoRichiesteOrdineFornitoreServiceExt raggruppamentoServiceExt;

  @Autowired
  private DeviceResponseOkConsumer consumer;

  @Autowired
  private TipoDispositivoServiceExt deviceTypeService;

  @Autowired
  private EntityManager entityManager;

  private Richiesta richiesta;

  private TipoDispositivo deviceType;

  @Before
  public void setUp() {
    deviceType = deviceTypeService.findOneByNome(TipoDispositivoEnum.TRACKYCARD);
  }

  @Test
  @Transactional
  public void test() {

    Instant now = Instant.ofEpochMilli(1001);
    String dateFormatted = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm"));
    FileElaborationDetail message = new FileElaborationDetail("FILE_" + dateFormatted, now);
    consumer.saveRaggruppamentoRichiesteFornitore(message, deviceType, new Richiesta().identificativo(UUID.randomUUID().toString()));
    consumer.saveRaggruppamentoRichiesteFornitore(message, deviceType, new Richiesta().identificativo(UUID.randomUUID().toString()));
    consumer.saveRaggruppamentoRichiesteFornitore(message, deviceType, new Richiesta().identificativo(UUID.randomUUID().toString()));

    Optional<RaggruppamentoRichiesteOrdineFornitore> optRaggruppamento = raggruppamentoServiceExt.findByNomeFileDataAcquisizioneTipoDispositivo(message.getFileName(),
                                                                                                                                                now,
                                                                                                                                                deviceType);
    assertThat(optRaggruppamento.isPresent())
      .isTrue();

    assertThat(optRaggruppamento.get().getRichiestas())
      .hasSize(3);

    RaggruppamentoRichiesteOrdineFornitore raggruppamento = optRaggruppamento.get();
    assertThat(raggruppamento.getNomeFileRicevuto()).isEqualTo(message.getFileName());
  }

}
