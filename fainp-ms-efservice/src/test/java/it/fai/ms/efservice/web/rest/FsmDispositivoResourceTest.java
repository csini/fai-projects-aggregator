package it.fai.ms.efservice.web.rest;

import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.asyncjob.AsyncPollingJobServiceImpl;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.async.FsmAsyncService;
import it.fai.ms.efservice.service.fsm.FsmResetService;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmDispositivoResourceTest {

  private FsmAsyncService            fsmAsyncService = mock(FsmAsyncService.class);
  private AsyncPollingJobServiceImpl asyncPollingJob = mock(AsyncPollingJobServiceImpl.class);
  private FsmDispositivo             fsmDispositivo  = mock(FsmDispositivo.class);
  private DispositivoServiceExt      deviceService   = mock(DispositivoServiceExt.class);
  private FsmResetService            fsmResetService = mock(FsmResetService.class);

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  private MockMvc restFsmDispositivoMockMvc;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final FsmDispositivoResourceExt fsmDispositivoResource = new FsmDispositivoResourceExt(fsmAsyncService, asyncPollingJob, fsmDispositivo,
                                                                                           deviceService, fsmResetService);
    this.restFsmDispositivoMockMvc = MockMvcBuilders.standaloneSetup(fsmDispositivoResource)
                                                    .setCustomArgumentResolvers(pageableArgumentResolver)
                                                    .setControllerAdvice(exceptionTranslator)
                                                    .setMessageConverters(jacksonMessageConverter)
                                                    .build();
  }

  @Test
  public void getAllStatusDeviceApiTest() throws Exception {

    List<StatoDispositivo> status = new ArrayList<>(Arrays.asList(StatoDispositivo.values()));
    List<String> statusDevice = status.stream()
                                      .map(s -> s.name())
                                      .collect(toList());
    restFsmDispositivoMockMvc.perform(get(FsmDispositivoResourceExt.BASE_PATH
                                          + FsmDispositivoResourceExt.ALL_STATUS_DISPOSITIVO).contentType(TestUtil.APPLICATION_JSON_UTF8))
                             .andExpect(status().is2xxSuccessful())
                             .andExpect(jsonPath("$.[*]").value(statusDevice));
  }
}
