package it.fai.ms.efservice.service.fsm.type;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.FsmInoltroToll2GoConfig;
import it.fai.ms.efservice.service.fsm.config.fai.FsmInoltroTrackyCardConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroToll2GoTest {

  @Configuration
  public static class Toll2GoInoltroConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    static FsmSenderToQueue fsmSender = mock(FsmSenderToQueue.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroTrackyCardConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmInoltroToll2GoConfig newToll2GoInoltroConfiguration() {
      FsmInoltroToll2GoConfig fsmInoltroToll2Go = new FsmInoltroToll2GoConfig(fsmSender);
      _log.info("Created FsmInoltroToll2GoConfig for test {}", fsmInoltroToll2Go);
      return fsmInoltroToll2Go;
    }
  }

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtils;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta richiesta;

  @Before
  public void setUp() throws Exception {
    cache.clearCache();

    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
  }

  @Test
  public void testStartFsmInoltroToll2Go() {
    changeStatus(richiesta, RichiestaEvent.INOLTRO_SERVIZI);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.DA_INOLTRARE);
  }

  @Test
  public void testInoltratoAlFornitoreFsmInoltroToll2Go() {
    testStartFsmInoltroToll2Go();

    changeStatus(richiesta, RichiestaEvent.MU_INOLTRATO);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
  }

  @Test
  public void testAttivoEvasoFsmInoltroToll2Go() {
    testInoltratoAlFornitoreFsmInoltroToll2Go();

    changeStatus(richiesta, RichiestaEvent.MU_ATTIVO);
    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);

    verify(Toll2GoInoltroConfiguration.fsmSender).sendMessageForChangeStatoDispositivo(Mockito.any(),
                                                                                       Mockito.eq(DispositivoEvent.SPEDITO_DAL_FORNITORE));
  }
  
  private void changeStatus(Richiesta richiesta, RichiestaEvent command) {
    try {
      richiesta = fsmRichiestaUtils.changeStatusToRichiesta(richiesta, command.name(), "", "");
    } catch (FsmExecuteCommandException e) {
      throw new RuntimeException(e);
    }
  }

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = newClienteFai();
    TipoDispositivo deviceType = new TipoDispositivo().nome(TipoDispositivoEnum.TOLL_COLLECT);
    Dispositivo device = new Dispositivo().tipoDispositivo(deviceType)
                                          .seriale("no-seriale")
                                          .stato(StatoDispositivo.INIZIALE);
    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.ATTIVAZIONE_SERVIZIO)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(deviceType)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");
    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    richiesta.addDispositivo(device);
    return richiesta;
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private ClienteFai newClienteFai() {
    return new ClienteFai().codiceCliente("codcli");
  }
}
