package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.ViaCardServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class ViaCardResourceExtTest {

  private final static String CLIENT_CODE = "0046348";

  @Autowired
  private ViaCardServiceExt viaCardService;

  @Autowired
  private ContrattoServiceExt       contrattoService;
  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  private long absDiff        = 0;
  private long result         = 0;
  private long numViaCard     = 0;
  private long numTelepassIta = 0;

  @Test
  @Transactional
  public void getNumberOfViaCardNecessaryTest() {


    Produttore          produttore  = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.VIACARD).getProduttore();
    Optional<Contratto> contratto   = contrattoService.findContrattoByProduttoreAndCodiceAzienda(produttore, CLIENT_CODE);
    long                viaCard     = viaCardService.countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum.VIACARD,
                                                                                                    contratto.get());
    long                telepassIta = viaCardService
      .countDeviceFilteredByStatoNotIsBluAndContratto(TipoDispositivoEnum.TELEPASS_ITALIANO, contratto.get());

    long diff = viaCard - telepassIta;
    if (diff < 0) {
      absDiff         = Math.abs(diff);
      numViaCard      = 0;
      numTelepassIta  = 2;
      absDiff        += 2;
    } else {
      absDiff        = Math.abs(diff);
      numViaCard     = 0;
      numTelepassIta = absDiff + 1;

      absDiff = 1;
    }

    result = viaCardService.getNumberOfViaCardNecessaryForOrder(CLIENT_CODE, numViaCard, numTelepassIta);
    assertThat(result).isEqualTo(absDiff);
  }

}
