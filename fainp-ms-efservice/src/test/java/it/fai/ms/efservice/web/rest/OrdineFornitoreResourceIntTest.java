package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.OrdineFornitore;
import it.fai.ms.efservice.repository.OrdineFornitoreRepository;
import it.fai.ms.efservice.service.OrdineFornitoreService;
import it.fai.ms.efservice.service.dto.OrdineFornitoreDTO;
import it.fai.ms.efservice.service.mapper.OrdineFornitoreMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static it.fai.ms.efservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.TipoDisposizione;
/**
 * Test class for the OrdineFornitoreResource REST controller.
 *
 * @see OrdineFornitoreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class OrdineFornitoreResourceIntTest {

    private static final Integer DEFAULT_QUANTITA = 1;
    private static final Integer UPDATED_QUANTITA = 2;

    private static final TipoDisposizione DEFAULT_TIPOLOGIA_DISPOSIZIONE = TipoDisposizione.ACQUISITO;
    private static final TipoDisposizione UPDATED_TIPOLOGIA_DISPOSIZIONE = TipoDisposizione.DA_ACQUISIRE;

    private static final String DEFAULT_RICHIEDENTE = "AAAAAAAAAA";
    private static final String UPDATED_RICHIEDENTE = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATA_RIORDINO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_RIORDINO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_NUMERO_DISPOSITIVI_ACQUISITI = 0;
    private static final Integer UPDATED_NUMERO_DISPOSITIVI_ACQUISITI = 1;

    @Autowired
    private OrdineFornitoreRepository ordineFornitoreRepository;

    @Autowired
    private OrdineFornitoreMapper ordineFornitoreMapper;

    @Autowired
    private OrdineFornitoreService ordineFornitoreService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restOrdineFornitoreMockMvc;

    private OrdineFornitore ordineFornitore;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrdineFornitoreResource ordineFornitoreResource = new OrdineFornitoreResource(ordineFornitoreService);
        this.restOrdineFornitoreMockMvc = MockMvcBuilders.standaloneSetup(ordineFornitoreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrdineFornitore createEntity(EntityManager em) {
        OrdineFornitore ordineFornitore = new OrdineFornitore()
            .quantita(DEFAULT_QUANTITA)
            .tipologiaDisposizione(DEFAULT_TIPOLOGIA_DISPOSIZIONE)
            .richiedente(DEFAULT_RICHIEDENTE)
            .dataRiordino(DEFAULT_DATA_RIORDINO)
            .numeroDispositiviAcquisiti(DEFAULT_NUMERO_DISPOSITIVI_ACQUISITI);
        return ordineFornitore;
    }

    @Before
    public void initTest() {
        ordineFornitore = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrdineFornitore() throws Exception {
        int databaseSizeBeforeCreate = ordineFornitoreRepository.findAll().size();

        // Create the OrdineFornitore
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreMapper.toDto(ordineFornitore);
        restOrdineFornitoreMockMvc.perform(post("/api/ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ordineFornitoreDTO)))
            .andExpect(status().isCreated());

        // Validate the OrdineFornitore in the database
        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeCreate + 1);
        OrdineFornitore testOrdineFornitore = ordineFornitoreList.get(ordineFornitoreList.size() - 1);
        assertThat(testOrdineFornitore.getQuantita()).isEqualTo(DEFAULT_QUANTITA);
        assertThat(testOrdineFornitore.getTipologiaDisposizione()).isEqualTo(DEFAULT_TIPOLOGIA_DISPOSIZIONE);
        assertThat(testOrdineFornitore.getRichiedente()).isEqualTo(DEFAULT_RICHIEDENTE);
        assertThat(testOrdineFornitore.getDataRiordino()).isEqualTo(DEFAULT_DATA_RIORDINO);
        assertThat(testOrdineFornitore.getNumeroDispositiviAcquisiti()).isEqualTo(DEFAULT_NUMERO_DISPOSITIVI_ACQUISITI);
    }

    @Test
    @Transactional
    public void createOrdineFornitoreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ordineFornitoreRepository.findAll().size();

        // Create the OrdineFornitore with an existing ID
        ordineFornitore.setId(1L);
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreMapper.toDto(ordineFornitore);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdineFornitoreMockMvc.perform(post("/api/ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ordineFornitoreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrdineFornitore in the database
        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNumeroDispositiviAcquisitiIsRequired() throws Exception {
        int databaseSizeBeforeTest = ordineFornitoreRepository.findAll().size();
        // set the field null
        ordineFornitore.setNumeroDispositiviAcquisiti(null);

        // Create the OrdineFornitore, which fails.
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreMapper.toDto(ordineFornitore);

        restOrdineFornitoreMockMvc.perform(post("/api/ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ordineFornitoreDTO)))
            .andExpect(status().isBadRequest());

        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOrdineFornitores() throws Exception {
        // Initialize the database
        ordineFornitoreRepository.saveAndFlush(ordineFornitore);

        // Get all the ordineFornitoreList
        restOrdineFornitoreMockMvc.perform(get("/api/ordine-fornitores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ordineFornitore.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantita").value(hasItem(DEFAULT_QUANTITA)))
            .andExpect(jsonPath("$.[*].tipologiaDisposizione").value(hasItem(DEFAULT_TIPOLOGIA_DISPOSIZIONE.toString())))
            .andExpect(jsonPath("$.[*].richiedente").value(hasItem(DEFAULT_RICHIEDENTE.toString())))
            .andExpect(jsonPath("$.[*].dataRiordino").value(hasItem(DEFAULT_DATA_RIORDINO.toString())))
            .andExpect(jsonPath("$.[*].numeroDispositiviAcquisiti").value(hasItem(DEFAULT_NUMERO_DISPOSITIVI_ACQUISITI)));
    }

    @Test
    @Transactional
    public void getOrdineFornitore() throws Exception {
        // Initialize the database
        ordineFornitoreRepository.saveAndFlush(ordineFornitore);

        // Get the ordineFornitore
        restOrdineFornitoreMockMvc.perform(get("/api/ordine-fornitores/{id}", ordineFornitore.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ordineFornitore.getId().intValue()))
            .andExpect(jsonPath("$.quantita").value(DEFAULT_QUANTITA))
            .andExpect(jsonPath("$.tipologiaDisposizione").value(DEFAULT_TIPOLOGIA_DISPOSIZIONE.toString()))
            .andExpect(jsonPath("$.richiedente").value(DEFAULT_RICHIEDENTE.toString()))
            .andExpect(jsonPath("$.dataRiordino").value(DEFAULT_DATA_RIORDINO.toString()))
            .andExpect(jsonPath("$.numeroDispositiviAcquisiti").value(DEFAULT_NUMERO_DISPOSITIVI_ACQUISITI));
    }

    @Test
    @Transactional
    public void getNonExistingOrdineFornitore() throws Exception {
        // Get the ordineFornitore
        restOrdineFornitoreMockMvc.perform(get("/api/ordine-fornitores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrdineFornitore() throws Exception {
        // Initialize the database
        ordineFornitoreRepository.saveAndFlush(ordineFornitore);
        int databaseSizeBeforeUpdate = ordineFornitoreRepository.findAll().size();

        // Update the ordineFornitore
        OrdineFornitore updatedOrdineFornitore = ordineFornitoreRepository.findOne(ordineFornitore.getId());
        // Disconnect from session so that the updates on updatedOrdineFornitore are not directly saved in db
        em.detach(updatedOrdineFornitore);
        updatedOrdineFornitore
            .quantita(UPDATED_QUANTITA)
            .tipologiaDisposizione(UPDATED_TIPOLOGIA_DISPOSIZIONE)
            .richiedente(UPDATED_RICHIEDENTE)
            .dataRiordino(UPDATED_DATA_RIORDINO)
            .numeroDispositiviAcquisiti(UPDATED_NUMERO_DISPOSITIVI_ACQUISITI);
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreMapper.toDto(updatedOrdineFornitore);

        restOrdineFornitoreMockMvc.perform(put("/api/ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ordineFornitoreDTO)))
            .andExpect(status().isOk());

        // Validate the OrdineFornitore in the database
        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeUpdate);
        OrdineFornitore testOrdineFornitore = ordineFornitoreList.get(ordineFornitoreList.size() - 1);
        assertThat(testOrdineFornitore.getQuantita()).isEqualTo(UPDATED_QUANTITA);
        assertThat(testOrdineFornitore.getTipologiaDisposizione()).isEqualTo(UPDATED_TIPOLOGIA_DISPOSIZIONE);
        assertThat(testOrdineFornitore.getRichiedente()).isEqualTo(UPDATED_RICHIEDENTE);
        assertThat(testOrdineFornitore.getDataRiordino()).isEqualTo(UPDATED_DATA_RIORDINO);
        assertThat(testOrdineFornitore.getNumeroDispositiviAcquisiti()).isEqualTo(UPDATED_NUMERO_DISPOSITIVI_ACQUISITI);
    }

    @Test
    @Transactional
    public void updateNonExistingOrdineFornitore() throws Exception {
        int databaseSizeBeforeUpdate = ordineFornitoreRepository.findAll().size();

        // Create the OrdineFornitore
        OrdineFornitoreDTO ordineFornitoreDTO = ordineFornitoreMapper.toDto(ordineFornitore);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restOrdineFornitoreMockMvc.perform(put("/api/ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ordineFornitoreDTO)))
            .andExpect(status().isCreated());

        // Validate the OrdineFornitore in the database
        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteOrdineFornitore() throws Exception {
        // Initialize the database
        ordineFornitoreRepository.saveAndFlush(ordineFornitore);
        int databaseSizeBeforeDelete = ordineFornitoreRepository.findAll().size();

        // Get the ordineFornitore
        restOrdineFornitoreMockMvc.perform(delete("/api/ordine-fornitores/{id}", ordineFornitore.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OrdineFornitore> ordineFornitoreList = ordineFornitoreRepository.findAll();
        assertThat(ordineFornitoreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrdineFornitore.class);
        OrdineFornitore ordineFornitore1 = new OrdineFornitore();
        ordineFornitore1.setId(1L);
        OrdineFornitore ordineFornitore2 = new OrdineFornitore();
        ordineFornitore2.setId(ordineFornitore1.getId());
        assertThat(ordineFornitore1).isEqualTo(ordineFornitore2);
        ordineFornitore2.setId(2L);
        assertThat(ordineFornitore1).isNotEqualTo(ordineFornitore2);
        ordineFornitore1.setId(null);
        assertThat(ordineFornitore1).isNotEqualTo(ordineFornitore2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrdineFornitoreDTO.class);
        OrdineFornitoreDTO ordineFornitoreDTO1 = new OrdineFornitoreDTO();
        ordineFornitoreDTO1.setId(1L);
        OrdineFornitoreDTO ordineFornitoreDTO2 = new OrdineFornitoreDTO();
        assertThat(ordineFornitoreDTO1).isNotEqualTo(ordineFornitoreDTO2);
        ordineFornitoreDTO2.setId(ordineFornitoreDTO1.getId());
        assertThat(ordineFornitoreDTO1).isEqualTo(ordineFornitoreDTO2);
        ordineFornitoreDTO2.setId(2L);
        assertThat(ordineFornitoreDTO1).isNotEqualTo(ordineFornitoreDTO2);
        ordineFornitoreDTO1.setId(null);
        assertThat(ordineFornitoreDTO1).isNotEqualTo(ordineFornitoreDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ordineFornitoreMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ordineFornitoreMapper.fromId(null)).isNull();
    }
}
