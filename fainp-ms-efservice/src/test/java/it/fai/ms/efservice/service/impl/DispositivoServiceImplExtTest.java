package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.TipoServizioServiceExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class DispositivoServiceImplExtTest {

  @Autowired
  private DispositivoServiceExt deviceServiceExt;

  @Autowired
  private TipoDispositivoServiceExt deviceTypeServiceExt;

  @Autowired
  private TipoServizioServiceExt serviceTypeExt;

  private TipoDispositivo deviceType;

  private String serialeDispositivoGoBox = "::seriale-go-box::";

  private String seriale = "::serial-old-trackycard::";

  @Before
  public void setUp() throws Exception {
    TipoServizio pedaggiAustria = serviceTypeExt.findByNome("PEDAGGI_AUSTRIA");
    deviceType = deviceTypeServiceExt.findOneByNome(TipoDispositivoEnum.GO_BOX);

    StatoDispositivoServizio sds = new StatoDispositivoServizio().pan(seriale).stato(StatoDS.ATTIVO)
                                                                 .tipoServizio(pedaggiAustria);
    sds = deviceServiceExt.updateStatoDispositivoServizio(sds);
    Dispositivo d = new Dispositivo().tipoDispositivo(deviceType)
                                     .seriale(serialeDispositivoGoBox)
                                     .stato(StatoDispositivo.INIZIALE)
                                     .addStatoDispositivoServizio(sds);
    deviceServiceExt.save(d);
    deviceServiceExt.updateStatoDispositivoServizio(sds.dispositivo(d));
  }

  @Test
  @Transactional
  public void testFindGoBoxDeviceRelated() {
    Optional<Dispositivo> deviceOpt = deviceServiceExt.findGoBoxDeviceRelated(seriale);
    assertThat(deviceOpt).isNotNull();
    assertThat(deviceOpt.isPresent()).isTrue();
    assertThat(deviceOpt.get()
                        .getSeriale()).isEqualTo(serialeDispositivoGoBox);
  }

}
