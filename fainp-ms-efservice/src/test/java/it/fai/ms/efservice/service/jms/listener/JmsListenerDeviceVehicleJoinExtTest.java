package it.fai.ms.efservice.service.jms.listener;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import javax.jms.ObjectMessage;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.jms.efservice.ContractOrderRequest;
import it.fai.ms.common.jms.efservice.DeviceUuid;
import it.fai.ms.common.jms.efservice.VehicleUuid;
import it.fai.ms.common.jms.efservice.message.device.DeviceVehicleJoinMessage;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceVehicleJoin;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore("Test  di integrazione molto lento")
public class JmsListenerDeviceVehicleJoinExtTest {
  
  @Autowired
  JmsListenerDeviceVehicleJoin jmsListenerDeviceVehicleJoin;

  @Before
  public void setup() {

  }

  @After
  public void after() {

  }
  
  @Test
  public void test_listener_001() throws Exception {
    ContractOrderRequest contractUuid = new ContractOrderRequest("118946365","11111");
    DeviceUuid deviceUuid = new DeviceUuid("71fed1dc-c5a7-4dd8-afed-64a6e85ad1c8");
    VehicleUuid vehicleUuid = new VehicleUuid(RandomString());    
    DeviceVehicleJoinMessage dto = new DeviceVehicleJoinMessage(contractUuid, deviceUuid, vehicleUuid);
    
    ObjectMessage message = mock(ObjectMessage.class);
    when(message.getObject()).thenReturn(dto);
    
    jmsListenerDeviceVehicleJoin.onMessage(message);
  }
  
  static String RandomString() {
    return RandomStringUtils.random(20, true, true);
  }

  static String RandomUUID() {
    return UUID.randomUUID()
               .toString();
  }
  
}
