package it.fai.ms.efservice.vehicle.ownership.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.RuleOutcomeRepository;
import it.fai.ms.efservice.service.AvailableVehicleService;
import it.fai.ms.efservice.service.DispositivoServiceExt;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnership;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipCompanyCode;
import it.fai.ms.efservice.vehicle.ownership.model.VehicleOwnershipId;
import it.fai.ms.efservice.vehicle.ownership.service.VehicleOwnershipService;

public class AvailableVehiclesForDeviceInstallationControllerTest {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private MockMvc                                          mockMvc;
  private AvailableVehiclesForDeviceInstallationController availableVehiclesForDeviceInstallationController;
  private VehicleOwnershipService                          vehicleOwnershipService = mock(VehicleOwnershipService.class);
  private RuleOutcomeRepository                            ruleOutcomeRepository   = mock(RuleOutcomeRepository.class);
  private DispositivoServiceExt                            deviceService           = mock(DispositivoServiceExt.class);

  @Before
  public void setUp() throws Exception {
    availableVehiclesForDeviceInstallationController = spy(new AvailableVehiclesForDeviceInstallationController(new AvailableVehicleService(vehicleOwnershipService,
                                                                                                                                            ruleOutcomeRepository,
                                                                                                                                            deviceService)));
    mockMvc = MockMvcBuilders.standaloneSetup(availableVehiclesForDeviceInstallationController)
                             .build();
  }

  @Test
  public void testGetAllVehiclesAvailableForDeviceTypeId() throws Exception {
    given(vehicleOwnershipService.getEnabledVehicles(newVehicleOwnershipCompanyCode(),
                                                     newVehicleOwnershipId())).willReturn(newVehicleOwnerships());

    given(ruleOutcomeRepository.find(Mockito.any())).willReturn(Optional.of(new RuleOutcome()));

    MvcResult mvcResult = mockMvc.perform(newRequestBuilder())
                                 .andReturn();

    String cacheHeader = mvcResult.getResponse()
                                  .getHeader("Cache-control");
    assertThat(cacheHeader).contains("no-store");

    String json = mvcResult.getResponse()
                           .getContentAsString();

    _log.info("Returned json: {}", json);
    JSONAssert.assertEquals(JsonUtils.readFile("availablevehicles.json"), json, false);
  }

  private VehicleOwnershipId newVehicleOwnershipId() {
    return new VehicleOwnershipId("::vehicleUuid::");
  }

  private Set<VehicleOwnership> newVehicleOwnerships() {
    Set<VehicleOwnership> vehicleOwnerships = new HashSet<>();
    VehicleOwnership vo1 = new VehicleOwnership("::uuid1::", "::licensePlate1::", newVehicleOwnershipCompanyCode());
    vehicleOwnerships.add(vo1);
    VehicleOwnership vo2 = new VehicleOwnership("::uuid2::", "::licensePlate2::", newVehicleOwnershipCompanyCode());
    vehicleOwnerships.add(vo2);
    return vehicleOwnerships;
  }

  private VehicleOwnershipCompanyCode newVehicleOwnershipCompanyCode() {
    return new VehicleOwnershipCompanyCode("::companyCodeId::");
  }

  private RequestBuilder newRequestBuilder() {
    RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/vehicle/ownership/::companyCodeId::/::vehicleUuid::")
                                                          .accept(MediaType.APPLICATION_JSON_UTF8);
    return requestBuilder;
  }

}
