package it.fai.ms.efservice.wizard.service;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.client.VehicleClient;
import it.fai.ms.efservice.dto.WizardVehicleDTO;
import it.fai.ms.efservice.rules.engine.PrecalculatedRuleService;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.service.ConfigurazioneDispositiviServiziService;
import it.fai.ms.efservice.wizard.model.ActivabilityState;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicle;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;
import it.fai.ms.efservice.wizard.model.matrix.WizardMatrixActivability;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4Matrix;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixDeviceType;
import it.fai.ms.efservice.wizard.model.matrix.step4.WizardStep4MatrixVehicle;
import it.fai.ms.efservice.wizard.repository.WizardDeviceRepository;
import it.fai.ms.efservice.wizard.repository.WizardDeviceTypeRepository;
import it.fai.ms.efservice.wizard.repository.WizardVehicleRepository;

@RunWith(MockitoJUnitRunner.class)
public class Step4ServiceImplTest {

  private Step4ServiceImpl           step4Service;
  @Mock
  private PrecalculatedRuleService   precalculatedRuleService;
  @Mock
  private WizardDeviceRepository     deviceRepository;
  @Mock
  private WizardDeviceTypeRepository deviceTypeRepository;
  @Mock
  private WizardVehicleRepository    vehicleRepository;
  @Mock
  private WizardVehicleService       wizardVehicleService;
  
  @Mock
  private VehicleClient       vehicleClient;
  
  @Mock
  private ConfigurazioneDispositiviServiziService configDevicesService;

  @Before
  public void setUp() throws Exception {
    step4Service = new Step4ServiceImpl(precalculatedRuleService, deviceRepository, deviceTypeRepository, vehicleRepository,
                                        wizardVehicleService, configDevicesService,vehicleClient);
  }

  @Test
  public void testBuildMatrix() {

    when(deviceTypeRepository.findByServiceTypeId(newWizardServiceTypeId())).thenReturn(newWizardDeviceTypes());
    when(vehicleRepository.find("::vehicleId1::")).thenReturn(Optional.ofNullable(newWizardVehicle("1")));
    when(vehicleRepository.find("::vehicleId2::")).thenReturn(Optional.ofNullable(newWizardVehicle("2")));

    // Vehicle 1
    when(precalculatedRuleService.execute(new RuleEngineVehicleId("::vehicleId1::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineDeviceTypeId("::deviceTypeId1::"),
                                          new RuleEngineVehicleId("::vehicleId1::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineDeviceTypeId("::deviceTypeId2::"),
                                          new RuleEngineVehicleId("::vehicleId1::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineServiceTypeId("::serviceTypeId::"), new RuleEngineDeviceTypeId("::deviceTypeId1::"),
                                          new RuleEngineVehicleId("::vehicleId1::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineServiceTypeId("::serviceTypeId::"), new RuleEngineDeviceTypeId("::deviceTypeId2::"),
                                          new RuleEngineVehicleId("::vehicleId1::"))).thenReturn(Optional.of(new RuleOutcome()));

    // Vehicle 2
    when(precalculatedRuleService.execute(new RuleEngineVehicleId("::vehicleId2::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineDeviceTypeId("::deviceTypeId1::"),
                                          new RuleEngineVehicleId("::vehicleId2::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineDeviceTypeId("::deviceTypeId2::"),
                                          new RuleEngineVehicleId("::vehicleId2::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineServiceTypeId("::serviceTypeId::"), new RuleEngineDeviceTypeId("::deviceTypeId1::"),
                                          new RuleEngineVehicleId("::vehicleId2::"))).thenReturn(Optional.of(new RuleOutcome()));
    when(precalculatedRuleService.execute(new RuleEngineServiceTypeId("::serviceTypeId::"), new RuleEngineDeviceTypeId("::deviceTypeId2::"),
                                          new RuleEngineVehicleId("::vehicleId2::"))).thenReturn(Optional.of(new RuleOutcome()));

    WizardStep4Matrix wizardMatrix = step4Service.buildMatrix(newWizardServiceTypeId(), newWizardVehicleIds(), "0046348");

    verify(deviceTypeRepository).findByServiceTypeId(new WizardServiceTypeId("::serviceTypeId::"));
    verifyNoMoreInteractions(deviceTypeRepository);
    verify(vehicleRepository).find("::vehicleId1::");
    verify(vehicleRepository).find("::vehicleId2::");
    verifyNoMoreInteractions(vehicleRepository);
    verify(precalculatedRuleService, times(2)).execute(new RuleEngineVehicleId("::vehicleId1::"));
    verify(precalculatedRuleService, times(2)).execute(new RuleEngineVehicleId("::vehicleId2::"));

    verify(precalculatedRuleService).execute(new RuleEngineDeviceTypeId("::deviceTypeId1::"), new RuleEngineVehicleId("::vehicleId1::"));
    verify(precalculatedRuleService).execute(new RuleEngineDeviceTypeId("::deviceTypeId2::"), new RuleEngineVehicleId("::vehicleId1::"));
    verify(precalculatedRuleService).execute(new RuleEngineDeviceTypeId("::deviceTypeId1::"), new RuleEngineVehicleId("::vehicleId2::"));
    verify(precalculatedRuleService).execute(new RuleEngineDeviceTypeId("::deviceTypeId2::"), new RuleEngineVehicleId("::vehicleId2::"));

    verify(precalculatedRuleService).execute(new RuleEngineServiceTypeId("::serviceTypeId::"),
                                             new RuleEngineDeviceTypeId("::deviceTypeId1::"), new RuleEngineVehicleId("::vehicleId1::"));
    verify(precalculatedRuleService).execute(new RuleEngineServiceTypeId("::serviceTypeId::"),
                                             new RuleEngineDeviceTypeId("::deviceTypeId2::"), new RuleEngineVehicleId("::vehicleId1::"));
    verify(precalculatedRuleService).execute(new RuleEngineServiceTypeId("::serviceTypeId::"),
                                             new RuleEngineDeviceTypeId("::deviceTypeId1::"), new RuleEngineVehicleId("::vehicleId2::"));
    verify(precalculatedRuleService).execute(new RuleEngineServiceTypeId("::serviceTypeId::"),
                                             new RuleEngineDeviceTypeId("::deviceTypeId2::"), new RuleEngineVehicleId("::vehicleId2::"));

    verifyNoMoreInteractions(precalculatedRuleService);

    assertThat(wizardMatrix).isNotNull();
    assertThat(wizardMatrix.getServiceTypeId()).isEqualTo("::serviceTypeId::");

    Set<WizardStep4MatrixVehicle> vehicles = wizardMatrix.getVehicles();
    assertThat(vehicles).hasSize(2);
    assertThat(vehicles.stream()).extracting("uuid")
                                 .containsExactlyInAnyOrder("::vehicleId1::", "::vehicleId2::");

    WizardStep4MatrixVehicle vehicle1 = vehicles.stream()
                                                .filter((WizardStep4MatrixVehicle vehicle) -> vehicle.getUuid()
                                                                                                     .equals("::vehicleId1::"))
                                                .findFirst()
                                                .get();

    WizardStep4MatrixVehicle vehicle2 = vehicles.stream()
                                                .filter(vehicle -> vehicle.getUuid()
                                                                          .equals("::vehicleId2::"))
                                                .findFirst()
                                                .get();

    assertThat(vehicle1.getEuroClass()).isEqualTo("::euro1::");
    assertThat(vehicle2.getEuroClass()).isEqualTo("::euro2::");
    assertThat(vehicle1.getLicensePlate()).isEqualTo("::licensePlate1::");
    assertThat(vehicle2.getLicensePlate()).isEqualTo("::licensePlate2::");
    assertThat(vehicle1.getDeviceTypes()).containsOnlyKeys("::deviceTypeId1::", "::deviceTypeId2::");
    assertThat(vehicle2.getDeviceTypes()).containsOnlyKeys("::deviceTypeId1::", "::deviceTypeId2::");
    Entry<? extends String, ? extends WizardStep4MatrixDeviceType> entry1 = new java.util.AbstractMap.SimpleEntry<>("::deviceTypeId1::",
                                                                                                                    newStep4WizardMatrixDeviceType("1"));
    Entry<? extends String, ? extends WizardStep4MatrixDeviceType> entry2 = new java.util.AbstractMap.SimpleEntry<>("::deviceTypeId2::",
                                                                                                                    newStep4WizardMatrixDeviceType("2"));

    assertThat(vehicle1.getDeviceTypes()).containsExactly(entry1, entry2);
    assertThat(vehicle2.getDeviceTypes()).containsExactly(entry1, entry2);
  }

  @Test
  @Ignore
  public void testBuildCompleteMatrix() {

    when(deviceTypeRepository.findAll()).thenReturn(newWizardDeviceTypes());

    // when(vehicleRepository.find("::vehicleId1::")).thenReturn(newOptionalWizardVehicle(1));
    // when(vehicleRepository.find("::vehicleId2::")).thenReturn(newOptionalWizardVehicle(2));

    List<WizardVehicleDTO> wizardVehiclesDTO = new ArrayList<>();
    WizardVehicleDTO dto = new WizardVehicleDTO();
    dto.setUuidVehicle("::custom::");
    dto.setHasDevicesActive(false);
    dto.setExpired(false);
    dto.setExpiring(true);
    wizardVehiclesDTO.add(dto);
    
    Set<String> uuidsVehicle = new HashSet<>();
    uuidsVehicle.add("::custom::");
    WizardStep4Matrix wizardMatrix = step4Service.buildCompleteMatrix(wizardVehiclesDTO, "0046348");
    verify(deviceTypeRepository).findAllIds();

    assertThat(wizardMatrix).isNotNull();

    Set<WizardStep4MatrixVehicle> setp4MatrixVehicles = wizardMatrix.getVehicles();
    assertThat(setp4MatrixVehicles).hasSize(2);

    WizardStep4MatrixVehicle vehicle1 = setp4MatrixVehicles.stream()
                                                           .filter((WizardStep4MatrixVehicle vehicle) -> vehicle.getUuid()
                                                                                                                .equals("::vehicleId1::"))
                                                           .findFirst()
                                                           .get();

    WizardStep4MatrixVehicle vehicle2 = setp4MatrixVehicles.stream()
                                                           .filter(vehicle -> vehicle.getUuid()
                                                                                     .equals("::vehicleId2::"))
                                                           .findFirst()
                                                           .get();

    assertThat(vehicle1.getDeviceTypes()).containsOnlyKeys("::deviceTypeId1::", "::deviceTypeId2::");
    assertThat(vehicle2.getDeviceTypes()).containsOnlyKeys("::deviceTypeId1::", "::deviceTypeId2::");
  }

  private WizardStep4MatrixDeviceType newStep4WizardMatrixDeviceType(String _item) {
    WizardStep4MatrixDeviceType step4WizardMatrixDeviceType = new WizardStep4MatrixDeviceType("::deviceTypeId" + _item + "::");
    step4WizardMatrixDeviceType.setActivability(new WizardMatrixActivability(ActivabilityState.ACTIVABLE));
    return step4WizardMatrixDeviceType;
  }

  private WizardVehicle newWizardVehicle(String _item) {
    WizardVehicle wizardVehicle = new WizardVehicle(new WizardVehicleId("::vehicleId" + _item + "::"));
    wizardVehicle.setEuroClass("::euro" + _item + "::");
    wizardVehicle.setLicensePlate("::licensePlate" + _item + "::");
    wizardVehicle.setState("::state" + _item + "::");
    return wizardVehicle;
  }

  private Set<WizardDeviceType> newWizardDeviceTypes() {
    return Arrays.asList("::deviceTypeId1::", "::deviceTypeId2::")
                 .stream()
                 .map(id -> new WizardDeviceType(new WizardDeviceTypeId(id)))
                 .collect(toSet());
  }

  private WizardServiceTypeId newWizardServiceTypeId() {
    return new WizardServiceTypeId("::serviceTypeId::");
  }

  private Set<WizardVehicleId> newWizardVehicleIds() {
    Set<WizardVehicleId> wizardVehicleIds = Arrays.asList("::vehicleId1::", "::vehicleId2::")
                                                  .stream()
                                                  .map(itm -> new WizardVehicleId(itm))
                                                  .collect(toSet());
    return wizardVehicleIds;
  }

}
