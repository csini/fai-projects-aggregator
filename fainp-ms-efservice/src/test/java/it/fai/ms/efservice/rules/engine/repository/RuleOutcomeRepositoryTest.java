package it.fai.ms.efservice.rules.engine.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.rules.consumer.RulePrecalculatorConsumer;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.service.NotificationPreconditionVehicleAnomalyService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, RuleOutcomeRepositoryTest.ConfigRuleOutComeRepository.class })
public class RuleOutcomeRepositoryTest {

  @Configuration
  public static class ConfigRuleOutComeRepository {

    private Logger log = LoggerFactory.getLogger(getClass());

    private static NotificationPreconditionVehicleAnomalyService notificationPreconditionService = mock(NotificationPreconditionVehicleAnomalyService.class);

    private static Map<RuleEntityKey, RuleOutcomeEntity> cache = new HashMap<>();

    @PostConstruct
    public void init() {
      log.info("Init custom configuration of RuleOutcomeRepositoryImpl for test");
    }

    public static NotificationPreconditionVehicleAnomalyService getService() {
      return notificationPreconditionService;
    }

    public static Map<RuleEntityKey, RuleOutcomeEntity> getCache() {
      return cache;
    }

    @Bean
    @Primary
    public RuleOutcomeRepositoryImpl newRuleOutcomeRepositoryImpl() {
      RuleOutcomeRepositoryImpl ruleOutcomeRepository = new RuleOutcomeRepositoryImpl(cache, notificationPreconditionService);
      log.info("Created RuleOutcomeRepositoryImpl for test {}", ruleOutcomeRepository);
      return ruleOutcomeRepository;
    }
  }

  public void destory() {
    ConfigRuleOutComeRepository.getCache()
                               .clear();
  }

  @Autowired
  private RulePrecalculatorConsumer consumer;

  @Test
  public void peristSendAnomalyTest() {

    VehicleMessage message = new VehicleMessage();
    message.setUuid("::vehicleUUID::");
    message.setLicensePlate("::targa::");
    message.setCountry("::country::");
    message.setMake("::make::");
    message.setType("::typeVehicle::");
    message.setEuroClass("EURO1");
    consumer.consume(message);

    verify(ConfigRuleOutComeRepository.getService(), never()).sendNotification(Mockito.any(), Mockito.any());

    message.setAxes(2);
    message.setCategory("M1");
    consumer.consume(message);
    verify(ConfigRuleOutComeRepository.getService(), times(18)).sendNotification(Mockito.any(), Mockito.any());

  }

}
