/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.util.TestFsmUtilService;

/**
 * @author Luca Vassallo
 */
@WithStateMachine(id = "fsm2")
public class TestFsm2Transition {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private TestFsmUtilService testFsmServiceUtil;

  @OnTransition(source = "A", target = "B")
  public void switchFromAtoB(StateContext<StateTest, EventTest> stateContext) {

    log.info("From A TO B");
    Transition<StateTest, EventTest> transition = stateContext.getTransition();
    State<StateTest, EventTest> target = stateContext.getTarget();
    StateTest toState = target.getId();
    Message<EventTest> message = stateContext.getMessage();
    testFsmServiceUtil.switchToState(message, toState, transition);

  }

  @OnTransition(source = "A", target = "C")
  public void switchFromAtoC(StateContext<StateTest, EventTest> stateContext) {

    log.info("From A TO C");
    Transition<StateTest, EventTest> transition = stateContext.getTransition();
    State<StateTest, EventTest> target = stateContext.getTarget();
    StateTest toState = target.getId();
    Message<EventTest> message = stateContext.getMessage();
    testFsmServiceUtil.switchToState(message, toState, transition);
  }

  @OnTransition(source = "B", target = "C")
  public void switchFromBtoC(StateContext<StateTest, EventTest> stateContext) {

    log.info("From B TO C");
    Transition<StateTest, EventTest> transition = stateContext.getTransition();
    State<StateTest, EventTest> target = stateContext.getTarget();
    StateTest toState = target.getId();
    Message<EventTest> message = stateContext.getMessage();
    testFsmServiceUtil.switchToState(message, toState, transition);
  }

  @OnTransition(source = "B", target = "D")
  public void switchFromBtoD(StateContext<StateTest, EventTest> stateContext) {

    log.info("From B TO D");
    Transition<StateTest, EventTest> transition = stateContext.getTransition();
    State<StateTest, EventTest> target = stateContext.getTarget();
    StateTest toState = target.getId();
    Message<EventTest> message = stateContext.getMessage();
    testFsmServiceUtil.switchToState(message, toState, transition);
  }

  @OnTransition(source = "C", target = "D")
  public void switchFromCtoD(StateContext<StateTest, EventTest> stateContext) {

    log.info("From C TO D");
    Transition<StateTest, EventTest> transition = stateContext.getTransition();
    State<StateTest, EventTest> target = stateContext.getTarget();
    StateTest toState = target.getId();
    Message<EventTest> message = stateContext.getMessage();
    testFsmServiceUtil.switchToState(message, toState, transition);
  }

  @OnTransition
  public void anyTransition() {
    log.debug("..... Transition ......");
  }

}
