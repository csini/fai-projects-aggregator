package it.fai.ms.efservice.service.jms.listener;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.time.Instant;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.dml.AbstractJmsDmlListener;
import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;
import it.fai.ms.efservice.service.jms.DmlListenerTest;
import it.fai.ms.efservice.service.jms.listener.repository.VehicleDMLDTORepository;

@Configuration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, JmsDmlVeicoliSaveListenerIntTest.class})
@Ignore("Test  di integrazione molto lento")
public class JmsDmlVeicoliSaveListenerIntTest
  extends DmlListenerTest<VehicleDMLDTO, VehicleDMLDTO> {

  @Autowired
  VehicleDMLDTORepository vehicleDMLDTORepository;

  @Autowired
  JmsDmlVeicoliSaveListener jmsDmlVeicoliSaveListener;

  @Bean
  public VehicleConsumer mockConsumer(){
    return Mockito.mock(VehicleConsumer.class);
  }
  @Autowired
  @Qualifier("mockConsumer")
  VehicleConsumer mockConsumer;

  public JmsDmlVeicoliSaveListenerIntTest() {
    super(JmsTopicNames.DML_VEHICLE_SAVE);
  }

  @Override
  protected InterfaceDmlRepository<VehicleDMLDTO> getRepository() {
    return vehicleDMLDTORepository;
  }

  @Override
  protected AbstractJmsDmlListener<VehicleDMLDTO, VehicleDMLDTO> getListener() {
    return jmsDmlVeicoliSaveListener;
  }

  @Override
  protected VehicleDMLDTO createDMLDTO() {
    VehicleDMLDTO vehicleDMLDTOCurrent = new VehicleDMLDTO();
    vehicleDMLDTOCurrent.setDmlUniqueIdentifier("1");
    vehicleDMLDTOCurrent.setCategoria("B");
    vehicleDMLDTOCurrent.setDmlRevisionTimestamp(Instant.now());
    return vehicleDMLDTOCurrent;
  }

  @Override
  protected void doAssertionOnPersistedEntity(VehicleDMLDTO sce) {
    Assert.assertEquals( super.getDMLDTO().getIdentificativo(), sce.getIdentificativo());
    Assert.assertEquals(super.getDMLDTO().getDmlUniqueIdentifier(), sce.getDmlUniqueIdentifier());
  }

  @Test
  public void testConsumeVehicleMessage() throws JMSException {
    //needed cause spring context use same mock for all tests
    Mockito.clearInvocations(mockConsumer);

    VehicleDMLDTO dmldto = createDMLDTO();
    ObjectMessage jmsMessage = Mockito.mock(ObjectMessage.class);
    ArgumentCaptor<VehicleMessage> argumentCaptor = ArgumentCaptor.forClass(VehicleMessage.class);

    given(jmsMessage.getObject()).willReturn(dmldto);

    getListener().onMessage(jmsMessage);

    then(mockConsumer).should().consume(argumentCaptor.capture());

    Assert.assertEquals("123", argumentCaptor.getValue().getUuid());
  }

  @Test
  public void testNotConsumeOldMessageVehicleMessage() throws JMSException {
    //needed cause spring context use same mock for all tests
    Mockito.clearInvocations(mockConsumer);

    VehicleDMLDTO currentDMLDTO = createDMLDTO();
    VehicleDMLDTO oldDMLDTO = createDMLDTO();
    oldDMLDTO.setDmlRevisionTimestamp(currentDMLDTO.getDmlRevisionTimestamp().minusSeconds(60));

    ObjectMessage jmsCurrentMessage = Mockito.mock(ObjectMessage.class);
    ObjectMessage jmsOldMessage = Mockito.mock(ObjectMessage.class);

    ArgumentCaptor<VehicleMessage> argumentCaptor = ArgumentCaptor.forClass(VehicleMessage.class);

    given(jmsCurrentMessage.getObject()).willReturn(currentDMLDTO);
    given(jmsOldMessage.getObject()).willReturn(oldDMLDTO);


    getListener().onMessage(jmsCurrentMessage);
    then(mockConsumer).should().consume(argumentCaptor.capture());

    getListener().onMessage(jmsOldMessage);
    then(mockConsumer).shouldHaveNoMoreInteractions();

    Assert.assertEquals("123", argumentCaptor.getValue().getUuid());
  }

}
