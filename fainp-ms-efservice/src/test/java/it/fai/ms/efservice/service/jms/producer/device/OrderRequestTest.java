package it.fai.ms.efservice.service.jms.producer.device;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;

public class OrderRequestTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void whenRequestHasMoreThanOneDeviceWithHardwareType() {
    Richiesta request = new Richiesta();
    Dispositivo device1 = new Dispositivo();
    device1.setTipoHardware("::hardwareType1::");
    Dispositivo device2 = new Dispositivo();
    device2.setTipoHardware("::hardwareType1::");
    request.addDispositivo(device1);
    request.addDispositivo(device2);

    OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);

    assertThat(orderRequest.getHardwareDeviceType()).isEqualTo("::hardwareType1::");
  }

  @Test
  public void whenRequestHasOneDeviceWithHardwareType() {
    Richiesta request = new Richiesta();
    Dispositivo device = new Dispositivo();
    device.setTipoHardware("::hardwareType::");
    request.addDispositivo(device);

    OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);

    assertThat(orderRequest.getHardwareDeviceType()).isEqualTo("::hardwareType::");
  }

  @Test
  public void whenRequestHasOneDeviceWithoutHardwareType() {
    Richiesta request = new Richiesta();
    Dispositivo device = new Dispositivo();
    request.addDispositivo(device);

    OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);

    assertThat(orderRequest.getHardwareDeviceType()).isEmpty();
  }

  @Test
  public void whenRequestHasZeroDevices() {
    Richiesta request = new Richiesta();

    OrderRequestDecorator orderRequest = new OrderRequestDecorator(request);

    assertThat(orderRequest.getHardwareDeviceType()).isEmpty();
  }
}
