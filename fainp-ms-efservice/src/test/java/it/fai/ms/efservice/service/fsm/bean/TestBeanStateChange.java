/**
 * 
 */
package it.fai.ms.efservice.service.fsm.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.annotation.OnStateChanged;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * @author Luca Vassallo
 */
@WithStateMachine
public class TestBeanStateChange {

  // This class is not used;

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @OnStateChanged(source = "A", target = "B")
  public void stateChangeFromAtoB() {
  }

  @OnStateChanged(source = "A", target = "C")
  public void stateChangeFromAtoC() {
  }

  @OnStateChanged(source = "B", target = "C")
  public void stateChangeFromBtoC() {
  }

  @OnStateChanged(source = "B", target = "D")
  public void stateChangeFromBtoD() {
  }

  @OnStateChanged(source = "C", target = "D")
  public void stateChangeFromCtoD() {
  }
}
