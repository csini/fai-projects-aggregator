package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.RaggruppamentoRichiesteOrdineFornitore;
import it.fai.ms.efservice.repository.RaggruppamentoRichiesteOrdineFornitoreRepository;
import it.fai.ms.efservice.service.RaggruppamentoRichiesteOrdineFornitoreService;
import it.fai.ms.efservice.service.dto.RaggruppamentoRichiesteOrdineFornitoreDTO;
import it.fai.ms.efservice.service.mapper.RaggruppamentoRichiesteOrdineFornitoreMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RaggruppamentoRichiesteOrdineFornitoreResource REST controller.
 *
 * @see RaggruppamentoRichiesteOrdineFornitoreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class RaggruppamentoRichiesteOrdineFornitoreResourceIntTest {

    private static final String DEFAULT_NOME_FILE_RICEVUTO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_FILE_RICEVUTO = "BBBBBBBBBB";

    private static final Instant DEFAULT_DATA_GENERAZIONE_FILE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_GENERAZIONE_FILE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATA_ULTIMA_ACQUISIZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_ULTIMA_ACQUISIZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private RaggruppamentoRichiesteOrdineFornitoreRepository raggruppamentoRichiesteOrdineFornitoreRepository;

    @Autowired
    private RaggruppamentoRichiesteOrdineFornitoreMapper raggruppamentoRichiesteOrdineFornitoreMapper;

    @Autowired
    private RaggruppamentoRichiesteOrdineFornitoreService raggruppamentoRichiesteOrdineFornitoreService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRaggruppamentoRichiesteOrdineFornitoreMockMvc;

    private RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RaggruppamentoRichiesteOrdineFornitoreResource raggruppamentoRichiesteOrdineFornitoreResource = new RaggruppamentoRichiesteOrdineFornitoreResource(raggruppamentoRichiesteOrdineFornitoreService);
        this.restRaggruppamentoRichiesteOrdineFornitoreMockMvc = MockMvcBuilders.standaloneSetup(raggruppamentoRichiesteOrdineFornitoreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RaggruppamentoRichiesteOrdineFornitore createEntity(EntityManager em) {
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore = new RaggruppamentoRichiesteOrdineFornitore()
            .nomeFileRicevuto(DEFAULT_NOME_FILE_RICEVUTO)
            .dataGenerazioneFile(DEFAULT_DATA_GENERAZIONE_FILE)
            .dataUltimaAcquisizione(DEFAULT_DATA_ULTIMA_ACQUISIZIONE);
        return raggruppamentoRichiesteOrdineFornitore;
    }

    @Before
    public void initTest() {
        raggruppamentoRichiesteOrdineFornitore = createEntity(em);
    }

    @Test
    @Transactional
    public void createRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        int databaseSizeBeforeCreate = raggruppamentoRichiesteOrdineFornitoreRepository.findAll().size();

        // Create the RaggruppamentoRichiesteOrdineFornitore
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO = raggruppamentoRichiesteOrdineFornitoreMapper.toDto(raggruppamentoRichiesteOrdineFornitore);
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(post("/api/raggruppamento-richieste-ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raggruppamentoRichiesteOrdineFornitoreDTO)))
            .andExpect(status().isCreated());

        // Validate the RaggruppamentoRichiesteOrdineFornitore in the database
        List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentoRichiesteOrdineFornitoreList = raggruppamentoRichiesteOrdineFornitoreRepository.findAll();
        assertThat(raggruppamentoRichiesteOrdineFornitoreList).hasSize(databaseSizeBeforeCreate + 1);
        RaggruppamentoRichiesteOrdineFornitore testRaggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreList.get(raggruppamentoRichiesteOrdineFornitoreList.size() - 1);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getNomeFileRicevuto()).isEqualTo(DEFAULT_NOME_FILE_RICEVUTO);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getDataGenerazioneFile()).isEqualTo(DEFAULT_DATA_GENERAZIONE_FILE);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getDataUltimaAcquisizione()).isEqualTo(DEFAULT_DATA_ULTIMA_ACQUISIZIONE);
    }

    @Test
    @Transactional
    public void createRaggruppamentoRichiesteOrdineFornitoreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = raggruppamentoRichiesteOrdineFornitoreRepository.findAll().size();

        // Create the RaggruppamentoRichiesteOrdineFornitore with an existing ID
        raggruppamentoRichiesteOrdineFornitore.setId(1L);
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO = raggruppamentoRichiesteOrdineFornitoreMapper.toDto(raggruppamentoRichiesteOrdineFornitore);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(post("/api/raggruppamento-richieste-ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raggruppamentoRichiesteOrdineFornitoreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaggruppamentoRichiesteOrdineFornitore in the database
        List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentoRichiesteOrdineFornitoreList = raggruppamentoRichiesteOrdineFornitoreRepository.findAll();
        assertThat(raggruppamentoRichiesteOrdineFornitoreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRaggruppamentoRichiesteOrdineFornitores() throws Exception {
        // Initialize the database
        raggruppamentoRichiesteOrdineFornitoreRepository.saveAndFlush(raggruppamentoRichiesteOrdineFornitore);

        // Get all the raggruppamentoRichiesteOrdineFornitoreList
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(get("/api/raggruppamento-richieste-ordine-fornitores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(raggruppamentoRichiesteOrdineFornitore.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeFileRicevuto").value(hasItem(DEFAULT_NOME_FILE_RICEVUTO.toString())))
            .andExpect(jsonPath("$.[*].dataGenerazioneFile").value(hasItem(DEFAULT_DATA_GENERAZIONE_FILE.toString())))
            .andExpect(jsonPath("$.[*].dataUltimaAcquisizione").value(hasItem(DEFAULT_DATA_ULTIMA_ACQUISIZIONE.toString())));
    }

    @Test
    @Transactional
    public void getRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        // Initialize the database
        raggruppamentoRichiesteOrdineFornitoreRepository.saveAndFlush(raggruppamentoRichiesteOrdineFornitore);

        // Get the raggruppamentoRichiesteOrdineFornitore
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(get("/api/raggruppamento-richieste-ordine-fornitores/{id}", raggruppamentoRichiesteOrdineFornitore.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(raggruppamentoRichiesteOrdineFornitore.getId().intValue()))
            .andExpect(jsonPath("$.nomeFileRicevuto").value(DEFAULT_NOME_FILE_RICEVUTO.toString()))
            .andExpect(jsonPath("$.dataGenerazioneFile").value(DEFAULT_DATA_GENERAZIONE_FILE.toString()))
            .andExpect(jsonPath("$.dataUltimaAcquisizione").value(DEFAULT_DATA_ULTIMA_ACQUISIZIONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        // Get the raggruppamentoRichiesteOrdineFornitore
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(get("/api/raggruppamento-richieste-ordine-fornitores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        // Initialize the database
        raggruppamentoRichiesteOrdineFornitoreRepository.saveAndFlush(raggruppamentoRichiesteOrdineFornitore);
        int databaseSizeBeforeUpdate = raggruppamentoRichiesteOrdineFornitoreRepository.findAll().size();

        // Update the raggruppamentoRichiesteOrdineFornitore
        RaggruppamentoRichiesteOrdineFornitore updatedRaggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreRepository.findOne(raggruppamentoRichiesteOrdineFornitore.getId());
        updatedRaggruppamentoRichiesteOrdineFornitore
            .nomeFileRicevuto(UPDATED_NOME_FILE_RICEVUTO)
            .dataGenerazioneFile(UPDATED_DATA_GENERAZIONE_FILE)
            .dataUltimaAcquisizione(UPDATED_DATA_ULTIMA_ACQUISIZIONE);
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO = raggruppamentoRichiesteOrdineFornitoreMapper.toDto(updatedRaggruppamentoRichiesteOrdineFornitore);

        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(put("/api/raggruppamento-richieste-ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raggruppamentoRichiesteOrdineFornitoreDTO)))
            .andExpect(status().isOk());

        // Validate the RaggruppamentoRichiesteOrdineFornitore in the database
        List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentoRichiesteOrdineFornitoreList = raggruppamentoRichiesteOrdineFornitoreRepository.findAll();
        assertThat(raggruppamentoRichiesteOrdineFornitoreList).hasSize(databaseSizeBeforeUpdate);
        RaggruppamentoRichiesteOrdineFornitore testRaggruppamentoRichiesteOrdineFornitore = raggruppamentoRichiesteOrdineFornitoreList.get(raggruppamentoRichiesteOrdineFornitoreList.size() - 1);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getNomeFileRicevuto()).isEqualTo(UPDATED_NOME_FILE_RICEVUTO);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getDataGenerazioneFile()).isEqualTo(UPDATED_DATA_GENERAZIONE_FILE);
        assertThat(testRaggruppamentoRichiesteOrdineFornitore.getDataUltimaAcquisizione()).isEqualTo(UPDATED_DATA_ULTIMA_ACQUISIZIONE);
    }

    @Test
    @Transactional
    public void updateNonExistingRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        int databaseSizeBeforeUpdate = raggruppamentoRichiesteOrdineFornitoreRepository.findAll().size();

        // Create the RaggruppamentoRichiesteOrdineFornitore
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO = raggruppamentoRichiesteOrdineFornitoreMapper.toDto(raggruppamentoRichiesteOrdineFornitore);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(put("/api/raggruppamento-richieste-ordine-fornitores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raggruppamentoRichiesteOrdineFornitoreDTO)))
            .andExpect(status().isCreated());

        // Validate the RaggruppamentoRichiesteOrdineFornitore in the database
        List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentoRichiesteOrdineFornitoreList = raggruppamentoRichiesteOrdineFornitoreRepository.findAll();
        assertThat(raggruppamentoRichiesteOrdineFornitoreList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRaggruppamentoRichiesteOrdineFornitore() throws Exception {
        // Initialize the database
        raggruppamentoRichiesteOrdineFornitoreRepository.saveAndFlush(raggruppamentoRichiesteOrdineFornitore);
        int databaseSizeBeforeDelete = raggruppamentoRichiesteOrdineFornitoreRepository.findAll().size();

        // Get the raggruppamentoRichiesteOrdineFornitore
        restRaggruppamentoRichiesteOrdineFornitoreMockMvc.perform(delete("/api/raggruppamento-richieste-ordine-fornitores/{id}", raggruppamentoRichiesteOrdineFornitore.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RaggruppamentoRichiesteOrdineFornitore> raggruppamentoRichiesteOrdineFornitoreList = raggruppamentoRichiesteOrdineFornitoreRepository.findAll();
        assertThat(raggruppamentoRichiesteOrdineFornitoreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaggruppamentoRichiesteOrdineFornitore.class);
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore1 = new RaggruppamentoRichiesteOrdineFornitore();
        raggruppamentoRichiesteOrdineFornitore1.setId(1L);
        RaggruppamentoRichiesteOrdineFornitore raggruppamentoRichiesteOrdineFornitore2 = new RaggruppamentoRichiesteOrdineFornitore();
        raggruppamentoRichiesteOrdineFornitore2.setId(raggruppamentoRichiesteOrdineFornitore1.getId());
        assertThat(raggruppamentoRichiesteOrdineFornitore1).isEqualTo(raggruppamentoRichiesteOrdineFornitore2);
        raggruppamentoRichiesteOrdineFornitore2.setId(2L);
        assertThat(raggruppamentoRichiesteOrdineFornitore1).isNotEqualTo(raggruppamentoRichiesteOrdineFornitore2);
        raggruppamentoRichiesteOrdineFornitore1.setId(null);
        assertThat(raggruppamentoRichiesteOrdineFornitore1).isNotEqualTo(raggruppamentoRichiesteOrdineFornitore2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaggruppamentoRichiesteOrdineFornitoreDTO.class);
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO1 = new RaggruppamentoRichiesteOrdineFornitoreDTO();
        raggruppamentoRichiesteOrdineFornitoreDTO1.setId(1L);
        RaggruppamentoRichiesteOrdineFornitoreDTO raggruppamentoRichiesteOrdineFornitoreDTO2 = new RaggruppamentoRichiesteOrdineFornitoreDTO();
        assertThat(raggruppamentoRichiesteOrdineFornitoreDTO1).isNotEqualTo(raggruppamentoRichiesteOrdineFornitoreDTO2);
        raggruppamentoRichiesteOrdineFornitoreDTO2.setId(raggruppamentoRichiesteOrdineFornitoreDTO1.getId());
        assertThat(raggruppamentoRichiesteOrdineFornitoreDTO1).isEqualTo(raggruppamentoRichiesteOrdineFornitoreDTO2);
        raggruppamentoRichiesteOrdineFornitoreDTO2.setId(2L);
        assertThat(raggruppamentoRichiesteOrdineFornitoreDTO1).isNotEqualTo(raggruppamentoRichiesteOrdineFornitoreDTO2);
        raggruppamentoRichiesteOrdineFornitoreDTO1.setId(null);
        assertThat(raggruppamentoRichiesteOrdineFornitoreDTO1).isNotEqualTo(raggruppamentoRichiesteOrdineFornitoreDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(raggruppamentoRichiesteOrdineFornitoreMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(raggruppamentoRichiesteOrdineFornitoreMapper.fromId(null)).isNull();
    }
}
