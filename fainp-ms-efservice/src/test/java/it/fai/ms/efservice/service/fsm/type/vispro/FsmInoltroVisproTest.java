package it.fai.ms.efservice.service.fsm.type.vispro;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.dto.DocumentoDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoOrdineCliente;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.DocumentService;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.config.vispro.FsmInoltroVisproConfig;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.producer.device.DeviceProducerService;
import it.fai.ms.efservice.service.jms.util.FsmSenderToQueue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmInoltroVisproTest {

  @Configuration
  public static class VisproInoltroConfiguration {

    private Logger _log = LoggerFactory.getLogger(getClass());

    private static final FsmSenderToQueue senderFsmService = mock(FsmSenderToQueue.class);

    private static final DocumentService documentService = mock(DocumentService.class);

    private static final DeviceProducerService deviceProducerService = mock(DeviceProducerService.class);

    @PostConstruct
    public void init() {
      _log.debug("Test configuration for {}", FsmInoltroVisproConfig.class.getSimpleName());
    }

    @Bean
    @Primary
    public FsmInoltroVisproConfig newVisproInoltroConfiguration() {
      FsmInoltroVisproConfig fsmInoltroVispro = new FsmInoltroVisproConfig(senderFsmService, documentService, deviceProducerService);
      _log.info("Created FsmInoltroVisproConfig for test {}", fsmInoltroVispro);
      return fsmInoltroVispro;
    }
  }

  private FsmInoltroVispro fsmInoltroVispro;

  @Autowired
  private FsmFactory fsmFactory;

  @Autowired
  private FsmRichiestaCacheService cache;

  private Richiesta   richiesta = null;
  private Dispositivo dispositivo;

  @Before // Set-up
  public void setUp() throws Exception {
    cache.clearCache();

    String uuidData = LocalDateTime.now()
                                   .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    richiesta = newMockRichiesta(uuidData);
    fsmInoltroVispro = (FsmInoltroVispro) fsmFactory.getFsm(FsmCommand.CMD_ACCETTATO_PER_INOLTRO_VISPRO);
  }

  private Contratto newContratto(ClienteFai clienteFai) {
    Contratto c = new Contratto().produttore(new Produttore())
                                 .clienteFai(clienteFai)
                                 .stato(StatoContratto.ATTIVO)
                                 .dataModificaStato(Instant.now());
    c.setId(1L);
    return c;
  }

  private Dispositivo newDispositivo() {
    Dispositivo d = new Dispositivo();

    d.setStato(StatoDispositivo.IN_DEPOSITO);
    d.setTipoDispositivo(newTipoDispositivo());
    d.setId(1L);

    return d;
  }

  private TipoDispositivo newTipoDispositivo() {
    TipoDispositivo d = new TipoDispositivo();

    d.setId(10000001L);
    d.setModalitaSpedizione(ModalitaSpedizione.DA_DEPOSITO_FAI);
    d.setNome(TipoDispositivoEnum.VISPRO);

    return d;
  }

  private Richiesta newMockRichiesta(String uuidData) {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    OrdineCliente oc = new OrdineCliente().clienteAssegnatario(newClienteFai)
                                          .numeroOrdine("NUOVOORDINE-" + uuidData)
                                          .tipo(TipoOrdineCliente.NUOVO_ORDINE)
                                          .stato(StatoOrdineCliente.DA_EVADERE);

    dispositivo = newDispositivo();
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .stato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO)
                                         .tipoDispositivo(newTipoDispositivo())
                                         .addDispositivo(dispositivo)
                                         .contratto(newContratto(newClienteFai))
                                         .data(Instant.now())
                                         .dataModificaStato(Instant.now())
                                         .ordineCliente(oc)
                                         .associazione("::targa::")
                                         .country("::country::");

    richiesta.setId(Long.valueOf(uuidData));
    richiesta.setIdentificativo("identifier-" + uuidData + "-request");
    return richiesta;
  }

  @Test
  public void FsmInoltroVispro_accettato_incompletoTecnico() throws Exception {
    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta, false);

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.INCOMPLETO_TECNICO);
  }

  @Test
  public void FsmInoltroVispro_accettato_attivoEvaso() throws Exception {
    switchStateAccettatoProntoPerInoltroToDaInoltrare(richiesta, true);

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ATTIVO_EVASO);
  }

  @Test
  public void FsmInoltroVispro_not_available_command() throws Exception {
    try {
      richiesta = fsmInoltroVispro.executeCommandToChangeState(RichiestaEvent.MU_CHIUSURA_ORDINE, richiesta);
    } catch (Exception e) {
      assertThat(e.getMessage()).startsWith("Command MU_CHIUSURA_ORDINE");
    }

    assertThat(richiesta.getStato()).isEqualTo(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
  }

  private void switchStateAccettatoProntoPerInoltroToDaInoltrare(Richiesta richiesta, boolean docsOk) throws Exception {
    ClienteFai newClienteFai = new ClienteFai().codiceCliente("codcli");

    Mockito.doNothing()
           .when(VisproInoltroConfiguration.senderFsmService)
           .sendMessageForChangeStatoDispositivo(any(Dispositivo.class), any(DispositivoEvent.class));
    if (docsOk) {
      Mockito.when(VisproInoltroConfiguration.documentService.findLastDocument(richiesta.getContratto()
                                                                                        .getClienteFai()
                                                                                        .getCodiceCliente(),
                                                                               DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
                                                                               TipoDispositivoEnum.VISPRO, null, null))
             .thenReturn(Optional.of(new DocumentoDTO().id(1l)
                                                       .identificativo("iddocumento1")));
      Mockito.when(VisproInoltroConfiguration.documentService.findLastDocument(richiesta.getContratto()
                                                                                        .getClienteFai()
                                                                                        .getCodiceCliente(),
                                                                               DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                                               TipoDispositivoEnum.VISPRO, richiesta.getAssociazione(),
                                                                               richiesta.getCountry()))
             .thenReturn(Optional.of(new DocumentoDTO().id(1l)
                                                       .identificativo("iddocumento1")));
    } else {
      Mockito.when(VisproInoltroConfiguration.documentService.findLastDocument(richiesta.getContratto()
                                                                                        .getClienteFai()
                                                                                        .getCodiceCliente(),
                                                                               DocumentService.TipologiaDocumento.RICHIESTA_CONTRATTO,
                                                                               TipoDispositivoEnum.VISPRO, null, null))
             .thenReturn(null);
      Mockito.when(VisproInoltroConfiguration.documentService.findLastDocument(richiesta.getContratto()
                                                                                        .getClienteFai()
                                                                                        .getCodiceCliente(),
                                                                               DocumentService.TipologiaDocumento.RICHIESTA_DISPOSITIVO,
                                                                               TipoDispositivoEnum.VISPRO, richiesta.getAssociazione(),
                                                                               richiesta.getCountry()))
             .thenReturn(null);
    }

    richiesta = fsmInoltroVispro.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
  }

}
