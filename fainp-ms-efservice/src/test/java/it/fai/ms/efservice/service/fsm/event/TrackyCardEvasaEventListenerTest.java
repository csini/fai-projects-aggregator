package it.fai.ms.efservice.service.fsm.event;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.StatoDispositivoServizioServiceExt;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

public class TrackyCardEvasaEventListenerTest {

  private FsmRichiestaUtils                  fsmRichiestaUtils = mock(FsmRichiestaUtils.class);
  private StatoDispositivoServizioServiceExt sdsServiceExt     = mock(StatoDispositivoServizioServiceExt.class);

  private TrackyCardEvasaEventListener trackyEventListener;
  private Richiesta                    richiestaTrackyCard;
  private Richiesta                    richiestaGoBox;
  private Dispositivo                  dispositivoGoBox;

  private StatoDispositivoServizio sds;

  private TipoDispositivo deviceTypeTracky = new TipoDispositivo().nome(TipoDispositivoEnum.TRACKYCARD);
  private TipoDispositivo deviceTypeGoBox  = new TipoDispositivo().nome(TipoDispositivoEnum.GO_BOX);

  private String identifierRequestTracky = RandomStringUtils.random(16);
  private String identifierRequestGoBox  = RandomStringUtils.random(16);

  private final String serialNumber = "::serial_number::";
  private final String licensePlate = "::license_plate::";
  private final String country      = "::country::";

  private String nota = null;

  @Before
  public void setUp() {
    richiestaTrackyCard = createRichiestaTrackyCard();

    dispositivoGoBox = setUpDeviceGoBox();
    sds = createStatoDispositivoServizioGoBox();
    dispositivoGoBox.addStatoDispositivoServizio(sds);

    richiestaGoBox = createRichiestaGoBox();

    OrdineCliente oc = createOrdineCliente(richiestaTrackyCard, richiestaGoBox);
    richiestaGoBox.ordineCliente(oc);
    richiestaTrackyCard.ordineCliente(oc);
    nota = "TrackyCard rilasciata dalla Richiesta [" + identifierRequestTracky + "]";

    trackyEventListener = new TrackyCardEvasaEventListener(fsmRichiestaUtils, sdsServiceExt);
  }

  @Test
  public void testConsumeEvent() throws FsmExecuteCommandException {
    trackyEventListener.consumeEvent(new TrackyCardEvasaEvent(richiestaTrackyCard, dispositivoGoBox));
    verify(sdsServiceExt).save(any(StatoDispositivoServizio.class));
    verify(fsmRichiestaUtils).changeStatusToRichiesta(richiestaGoBox, RichiestaEvent.MU_RIPETI_INVIO.name(), "", nota);
    assertThat(sds.getPan()).isEqualTo(serialNumber);
  }

  private Richiesta createRichiestaTrackyCard() {
    Dispositivo d = new Dispositivo().tipoDispositivo(deviceTypeTracky)
                                     .seriale(serialNumber);
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .identificativo(identifierRequestTracky)
                                         .associazione(licensePlate)
                                         .country(country)
                                         .stato(StatoRichiesta.ATTIVO_EVASO)
                                         .tipoDispositivo(deviceTypeTracky)
                                         .addDispositivo(d);
    return richiesta;
  }

  private Richiesta createRichiestaGoBox() {
    Richiesta richiesta = new Richiesta().tipo(TipoRichiesta.NUOVO_ORDINE)
                                         .identificativo(identifierRequestGoBox)
                                         .associazione(licensePlate)
                                         .country(country)
                                         .stato(StatoRichiesta.ORDINE_SOSPESO)
                                         .tipoDispositivo(deviceTypeGoBox)
                                         .addDispositivo(dispositivoGoBox);

    return richiesta;
  }

  private Dispositivo setUpDeviceGoBox() {
    return new Dispositivo().tipoDispositivo(deviceTypeGoBox);
  }

  private StatoDispositivoServizio createStatoDispositivoServizioGoBox() {
    return new StatoDispositivoServizio().stato(StatoDS.IN_ATTIVAZIONE)
                                         .tipoServizio(new TipoServizio().nome("PEDAGGI_AUSTRIA"))
                                         .dispositivo(dispositivoGoBox);
  }

  private OrdineCliente createOrdineCliente(Richiesta richiesta1, Richiesta richiesta2) {
    return new OrdineCliente().numeroOrdine("::prova_0001::")
                              .addRichiesta(richiesta1)
                              .addRichiesta(richiesta2);
  }
}
