package it.fai.ms.efservice.rules.engine.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;

@RunWith(MockitoJUnitRunner.class)
public class RuleContextServiceTypeRepositoryImplTest {

  private RuleContextServiceTypeRepositoryImpl ruleContextServiceTypeRepository;
  @Mock
  private TipoServizioRepository               serviceTypeRepository;
  
  @Before
  public void setUp() throws Exception {
    ruleContextServiceTypeRepository = new RuleContextServiceTypeRepositoryImpl(serviceTypeRepository);
  }

  @Test
  public void testStar() {
    RuleEngineServiceType contextServiceType = ruleContextServiceTypeRepository.newStarContextServiceType();

    verifyNoMoreInteractions(serviceTypeRepository);
    assertThat(contextServiceType.getId()).isEqualTo(RuleEngineServiceTypeId.ofWildcard());
  }

}
