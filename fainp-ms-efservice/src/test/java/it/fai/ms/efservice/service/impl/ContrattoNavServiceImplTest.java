package it.fai.ms.efservice.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;

public class ContrattoNavServiceImplTest {

  private ContrattoNavServiceImpl service;

  private final String codiceCliente = "::codice-cliente::";

  private ContrattoServiceExt contrattoService = mock(ContrattoServiceExt.class);

  private FsmDispositivo fsmDispositivo = mock(FsmDispositivo.class);

  private NotificationService notificationService = mock(NotificationService.class);

  private JmsProperties jmsPropertiesMock = mock(JmsProperties.class);

  private final static String CODE_ANOMALY  = "FSM_CTR001";
  private final static String PROD_TELEPASS = "TELEPASS";
  private final static String PROD_FREJUS   = "TES_TRA_FREJUS_MONTE_BIANCO";

  @Before
  public void setUp() throws Exception {
    service = new ContrattoNavServiceImpl(contrattoService, fsmDispositivo, notificationService, jmsPropertiesMock);
  }

  @Test
  public void testFindContractsToNavWithoutCodiceCliente() {
    List<Contratto> list = new ArrayList<Contratto>(Arrays.asList(newContratto(codiceCliente, PROD_TELEPASS)));
    when(contrattoService.findAll()).thenReturn(list);
    List<ContrattoNavDTO> contratti = service.findContractsToNav(null);
    verify(contrattoService).findAll();
    assertThat(contratti).hasSize(1);
  }

  @Test
  public void testFindContractsToNavWithCodiceCliente() {

    List<Contratto> list = new ArrayList<Contratto>(Arrays.asList(newContratto(codiceCliente, PROD_TELEPASS)));
    when(contrattoService.findByCodiceCliente(codiceCliente)).thenReturn(list);
    List<ContrattoNavDTO> contratti = service.findContractsToNav(codiceCliente);
    verify(contrattoService).findByCodiceCliente(codiceCliente);
    assertThat(contratti).hasSize(1);
  }

  @Test
  public void testSuspendTelepassContract() throws Exception {
    String identificativo = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS").format(new Date());
    ContrattoEvent command = ContrattoEvent.MS_SOSPENDI;
    Contratto newContratto = newContratto(codiceCliente, PROD_TELEPASS);
    when(contrattoService.findByIdentificativo(identificativo)).thenReturn(Optional.of(newContratto));
    when(contrattoService.isAvailableCommandFsm(newContratto, command)).thenReturn(true);
    when(contrattoService.fsmChangeStatus(newContratto, command)).thenReturn(newContratto.stato(StatoContratto.SOSPESO));
    when(jmsPropertiesMock.getUrl()).thenReturn("no-host");
    Contratto contratto = service.changeStatusContractByNav(identificativo, command);
    verify(contrattoService).findByIdentificativo(identificativo);
    verify(contrattoService).fsmChangeStatus(newContratto, command);
    verify(jmsPropertiesMock).getUrl();
    verifyZeroInteractions(notificationService);
    assertThat(contratto.getStato()).isEqualTo(StatoContratto.SOSPESO);
  }

  @Test
  public void testSuspendFrejusContract() throws Exception {
    String identificativo = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS").format(new Date());
    ContrattoEvent command = ContrattoEvent.MS_SOSPENDI;
    Contratto newContratto = newContratto(codiceCliente, PROD_FREJUS);
    when(contrattoService.findByIdentificativo(identificativo)).thenReturn(Optional.of(newContratto));
    when(contrattoService.isAvailableCommandFsm(newContratto, command)).thenReturn(true);
    when(contrattoService.fsmChangeStatus(newContratto, command)).thenReturn(newContratto.stato(StatoContratto.SOSPESO));
    when(jmsPropertiesMock.getUrl()).thenReturn("no-host");
    Contratto contratto = service.changeStatusContractByNav(identificativo, command);
    verify(contrattoService).findByIdentificativo(identificativo);
    verify(contrattoService).isAvailableCommandFsm(newContratto, command);
    verify(contrattoService).fsmChangeStatus(newContratto, command);
    verifyZeroInteractions(jmsPropertiesMock);
    Map<String, Object> params = createMapParams(newContratto, command);
    verify(notificationService).notify(CODE_ANOMALY, params);
    assertThat(contratto.getStato()).isEqualTo(StatoContratto.SOSPESO);
  }

  @Test
  public void testRevokeTelepassContract() throws Exception {
    String identificativo = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS").format(new Date());
    ContrattoEvent command = ContrattoEvent.MU_REVOCA;
    Contratto newContratto = newContratto(codiceCliente, PROD_TELEPASS);
    when(contrattoService.findByIdentificativo(identificativo)).thenReturn(Optional.of(newContratto));
    when(contrattoService.isAvailableCommandFsm(newContratto, command)).thenReturn(true);
    when(contrattoService.fsmChangeStatus(newContratto, command)).thenReturn(newContratto.stato(StatoContratto.REVOCATO));
    Dispositivo dispositivo = newContratto.getDispositivo()
                                          .stream()
                                          .findFirst()
                                          .get();
    Dispositivo newDispositivo = newContratto.getDispositivo()
                                             .stream()
                                             .findFirst()
                                             .get();
    when(fsmDispositivo.executeCommandToChangeState(DispositivoEvent.REVOCA_DA_CONTRATTO,
                                                    dispositivo)).thenReturn(newDispositivo.stato(StatoDispositivo.REVOCATO));
    when(jmsPropertiesMock.getUrl()).thenReturn("no-host");
    Contratto contratto = service.changeStatusContractByNav(identificativo, command);
    verify(contrattoService).findByIdentificativo(identificativo);
    verify(contrattoService).isAvailableCommandFsm(newContratto, command);
    verify(contrattoService).fsmChangeStatus(newContratto, command);
    verify(fsmDispositivo, times(1)).executeCommandToChangeState(DispositivoEvent.REVOCA_DA_CONTRATTO, dispositivo);
    verify(jmsPropertiesMock).getUrl();
    verifyZeroInteractions(notificationService);
    assertThat(contratto.getStato()).isEqualTo(StatoContratto.REVOCATO);
  }

  @Test
  public void testRevokeFrejusContract() throws Exception {
    String identificativo = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS").format(new Date());
    ContrattoEvent command = ContrattoEvent.MU_REVOCA;
    Contratto newContratto = newContratto(codiceCliente, PROD_FREJUS);
    when(contrattoService.findByIdentificativo(identificativo)).thenReturn(Optional.of(newContratto));
    when(contrattoService.isAvailableCommandFsm(newContratto, command)).thenReturn(true);
    when(contrattoService.fsmChangeStatus(newContratto, command)).thenReturn(newContratto.stato(StatoContratto.REVOCATO));
    Dispositivo dispositivo = newContratto.getDispositivo()
                                          .stream()
                                          .findFirst()
                                          .get();
    Dispositivo newDispositivo = newContratto.getDispositivo()
                                             .stream()
                                             .findFirst()
                                             .get();
    when(fsmDispositivo.executeCommandToChangeState(DispositivoEvent.REVOCA_DA_CONTRATTO,
                                                    dispositivo)).thenReturn(newDispositivo.stato(StatoDispositivo.REVOCATO));
    when(jmsPropertiesMock.getUrl()).thenReturn("no-host");
    Contratto contratto = service.changeStatusContractByNav(identificativo, command);
    verify(contrattoService).findByIdentificativo(identificativo);
    verify(contrattoService).isAvailableCommandFsm(newContratto, command);
    verify(contrattoService).fsmChangeStatus(newContratto, command);
    verify(fsmDispositivo, times(1)).executeCommandToChangeState(DispositivoEvent.REVOCA_DA_CONTRATTO, dispositivo);
    verifyZeroInteractions(jmsPropertiesMock);
    Map<String, Object> params = createMapParams(newContratto, command);
    verify(notificationService).notify(CODE_ANOMALY, params);
    assertThat(contratto.getStato()).isEqualTo(StatoContratto.REVOCATO);
  }

  @Test
  public void testIsNotAvailableCommand() throws Exception {
    String identificativo = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss_SSS").format(new Date());
    ContrattoEvent command = ContrattoEvent.MS_RIATTIVA;
    Contratto newContratto = newContratto(codiceCliente, PROD_FREJUS);
    when(contrattoService.findByIdentificativo(identificativo)).thenReturn(Optional.of(newContratto));
    when(contrattoService.isAvailableCommandFsm(newContratto, command)).thenReturn(false);
    service.changeStatusContractByNav(identificativo, command);
    verify(contrattoService).findByIdentificativo(identificativo);
    verify(contrattoService).isAvailableCommandFsm(newContratto, command);
    verifyZeroInteractions(notificationService);
    verifyZeroInteractions(jmsPropertiesMock);
  }

  private Contratto newContratto(String codiceCliente, String nomeProduttore) {
    Contratto contract = new Contratto().clienteFai(newClienteFai(codiceCliente))
                                        .produttore(newProduttore(nomeProduttore))
                                        .stato(StatoContratto.ATTIVO)
                                        .codContrattoCliente("::codice-contratto::");
    contract.setDispositivo(newDispositivos());
    return contract;
  }

  private Set<Dispositivo> newDispositivos() {
    Dispositivo device = new Dispositivo().seriale("::seriale::");
    Set<Dispositivo> devices = new HashSet<>();
    devices.add(device);
    return devices;
  }

  private Produttore newProduttore(String nomeProduttore) {
    Produttore produttore = new Produttore().nome(nomeProduttore);
    return produttore;
  }

  private ClienteFai newClienteFai(String codiceCliente) {
    return new ClienteFai().codiceCliente(codiceCliente);
  }

  private Map<String, Object> createMapParams(Contratto contract, ContrattoEvent event) {
    Map<String, Object> maps = new HashMap<>();
    maps.put("contract_code", contract.getCodContrattoCliente());
    String codiceCliente = contract.getClienteFai() != null ? contract.getClienteFai()
                                                                      .getCodiceCliente()
                                                            : "";
    maps.put("client_code", codiceCliente);
    String actionEvent = (event.equals(ContrattoEvent.MU_REVOCA)) ? "REVOCATO"
                                                                  : (event.equals(ContrattoEvent.MS_RIATTIVA) ? "RIPRISTINATO" : "SOSPESO");
    maps.put("action", actionEvent);
    String produttoreNome = contract.getProduttore() != null ? contract.getProduttore()
                                                                       .getNome()
                                                             : "";
    maps.put("producerName", produttoreNome);
    return maps;
  }

}
