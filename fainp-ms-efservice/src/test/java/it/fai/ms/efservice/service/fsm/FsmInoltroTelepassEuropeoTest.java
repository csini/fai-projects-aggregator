/**
 *
 */
package it.fai.ms.efservice.service.fsm;

import static it.fai.ms.efservice.service.fsm.FsmTestUtils.newMockTransition;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.transition.Transition;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmInoltroTelepassEuropeo;

@RunWith(MockitoJUnitRunner.Silent.class)
@Ignore("This test skipped because should to be used real entity")
public class FsmInoltroTelepassEuropeoTest {

  private final String fsmInoltroTeName = FsmType.INOLTRO_TE.fsmName();

  private FsmInoltroTelepassEuropeo fsmInoltroTe;

  @Mock
  private StateMachineFactory<StatoRichiesta, RichiestaEvent> stateMachineFactoryMock;

  @Mock
  private FsmFactory fsmFactoryMock;

  @Mock
  private FsmRichiestaCacheService cacheServiceMock;

  @Mock
  private StateMachine<StatoRichiesta, RichiestaEvent> stateMachineInoltroMock;

  private Richiesta richiesta;

  @Before // Set-up
  public void setUp() throws Exception {

    richiesta = newRichiesta();
    when(stateMachineInoltroMock.getId()).thenReturn(fsmInoltroTeName);
    when(stateMachineFactoryMock.getStateMachine(fsmInoltroTeName)).thenReturn(stateMachineInoltroMock);

    List<Transition<StatoRichiesta, RichiestaEvent>> newMockTransitionsInoltro = newMockTransitions();
    when(stateMachineInoltroMock.getTransitions()).thenReturn(newMockTransitionsInoltro);
    when(stateMachineInoltroMock.sendEvent(any(Message.class))).thenReturn(true);

    fsmInoltroTe = new FsmInoltroTelepassEuropeo(stateMachineFactoryMock, cacheServiceMock);
  }

  @Test
  public void FsmInoltroTelepassEuropeo_change_status() throws Exception {

    String RichiestaId = String.valueOf(richiesta.getId());

    TipoDispositivo tipoDispositivo = new TipoDispositivo().nome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    richiesta.setTipoDispositivo(tipoDispositivo);

    richiesta.setStato(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO);
    fsmInoltroTe.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);

    richiesta.setStato(StatoRichiesta.ATTESA_RISPOSTA);
    fsmInoltroTe.executeCommandToChangeState(RichiestaEvent.MS_CTRRI, richiesta);

    verify(stateMachineInoltroMock, times(2)).sendEvent(any(Message.class));
    verify(cacheServiceMock, times(2)).getStateMachineContext(eq(RichiestaId));
    verify(cacheServiceMock, times(2)).persist(eq(stateMachineInoltroMock), anyString());
    then(cacheServiceMock).shouldHaveNoMoreInteractions();
  }

  @Test
  public void FsmInoltroTelepassEuropeo_not_available_command() throws Exception {
    richiesta.setStato(StatoRichiesta.ATTESA_RISPOSTA);
    fsmInoltroTe.executeCommandToChangeState(RichiestaEvent.MS_ORDRI_RISIM, richiesta);

    then(cacheServiceMock).shouldHaveZeroInteractions();
  }

  // ****** Utils method ************

  private Richiesta newRichiesta() {
    Richiesta richiesta = new Richiesta();
    richiesta.setId(5678L);
    richiesta.setTipo(TipoRichiesta.NUOVO_ORDINE);
    richiesta.setStato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    return richiesta;
  }



  private List<Transition<StatoRichiesta, RichiestaEvent>> newMockTransitions() {
    List<Transition<StatoRichiesta, RichiestaEvent>> transitions = new ArrayList<>();
    transitions.add(newMockTransition(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, RichiestaEvent.INITIAL, StatoRichiesta.ACCETTATO));
    transitions.add(newMockTransition(StatoRichiesta.ACCETTATO, null, StatoRichiesta.INCOMPLETO_TECNICO));
    transitions.add(newMockTransition(StatoRichiesta.INCOMPLETO_TECNICO, null, StatoRichiesta.ATTESA_RISPOSTA));
    transitions.add(newMockTransition(StatoRichiesta.ATTESA_RISPOSTA, RichiestaEvent.MS_CTRRI,
                                      StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO));
    transitions.add(newMockTransition(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO, null,
                                      StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO));
    transitions.add(newMockTransition(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO, null, StatoRichiesta.INCOMPLETO_TECNICO));
    return transitions;
  }

}
