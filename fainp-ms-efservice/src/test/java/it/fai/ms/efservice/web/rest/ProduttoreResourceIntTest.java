package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.service.ProduttoreService;
import it.fai.ms.efservice.service.dto.ProduttoreDTO;
import it.fai.ms.efservice.service.mapper.ProduttoreMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProduttoreResource REST controller.
 *
 * @see ProduttoreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ProduttoreResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_CODICE_FORNITORE_NAV = "AAAAAAAAAA";
    private static final String UPDATED_CODICE_FORNITORE_NAV = "BBBBBBBBBB";

    @Autowired
    private ProduttoreRepository produttoreRepository;

    @Autowired
    private ProduttoreMapper produttoreMapper;

    @Autowired
    private ProduttoreService produttoreService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restProduttoreMockMvc;

    private Produttore produttore;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProduttoreResource produttoreResource = new ProduttoreResource(produttoreService);
        this.restProduttoreMockMvc = MockMvcBuilders.standaloneSetup(produttoreResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Produttore createEntity(EntityManager em) {
        Produttore produttore = new Produttore()
            .nome(DEFAULT_NOME)
            .codiceFornitoreNav(DEFAULT_CODICE_FORNITORE_NAV);
        return produttore;
    }

    @Before
    public void initTest() {
        produttore = createEntity(em);
    }

    @Test
    @Transactional
    public void createProduttore() throws Exception {
        int databaseSizeBeforeCreate = produttoreRepository.findAll().size();

        // Create the Produttore
        ProduttoreDTO produttoreDTO = produttoreMapper.toDto(produttore);
        restProduttoreMockMvc.perform(post("/api/produttores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produttoreDTO)))
            .andExpect(status().isCreated());

        // Validate the Produttore in the database
        List<Produttore> produttoreList = produttoreRepository.findAll();
        assertThat(produttoreList).hasSize(databaseSizeBeforeCreate + 1);
        Produttore testProduttore = produttoreList.get(produttoreList.size() - 1);
        assertThat(testProduttore.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testProduttore.getCodiceFornitoreNav()).isEqualTo(DEFAULT_CODICE_FORNITORE_NAV);
    }

    @Test
    @Transactional
    public void createProduttoreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = produttoreRepository.findAll().size();

        // Create the Produttore with an existing ID
        produttore.setId(1L);
        ProduttoreDTO produttoreDTO = produttoreMapper.toDto(produttore);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProduttoreMockMvc.perform(post("/api/produttores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produttoreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Produttore in the database
        List<Produttore> produttoreList = produttoreRepository.findAll();
        assertThat(produttoreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProduttores() throws Exception {
        // Initialize the database
        produttoreRepository.saveAndFlush(produttore);

        // Get all the produttoreList
        restProduttoreMockMvc.perform(get("/api/produttores?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(produttore.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].codiceFornitoreNav").value(hasItem(DEFAULT_CODICE_FORNITORE_NAV.toString())));
    }

    @Test
    @Transactional
    public void getProduttore() throws Exception {
        // Initialize the database
        produttoreRepository.saveAndFlush(produttore);

        // Get the produttore
        restProduttoreMockMvc.perform(get("/api/produttores/{id}", produttore.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(produttore.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.codiceFornitoreNav").value(DEFAULT_CODICE_FORNITORE_NAV.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProduttore() throws Exception {
        // Get the produttore
        restProduttoreMockMvc.perform(get("/api/produttores/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProduttore() throws Exception {
        // Initialize the database
        produttoreRepository.saveAndFlush(produttore);
        int databaseSizeBeforeUpdate = produttoreRepository.findAll().size();

        // Update the produttore
        Produttore updatedProduttore = produttoreRepository.findOne(produttore.getId());
        updatedProduttore
            .nome(UPDATED_NOME)
            .codiceFornitoreNav(UPDATED_CODICE_FORNITORE_NAV);
        ProduttoreDTO produttoreDTO = produttoreMapper.toDto(updatedProduttore);

        restProduttoreMockMvc.perform(put("/api/produttores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produttoreDTO)))
            .andExpect(status().isOk());

        // Validate the Produttore in the database
        List<Produttore> produttoreList = produttoreRepository.findAll();
        assertThat(produttoreList).hasSize(databaseSizeBeforeUpdate);
        Produttore testProduttore = produttoreList.get(produttoreList.size() - 1);
        assertThat(testProduttore.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testProduttore.getCodiceFornitoreNav()).isEqualTo(UPDATED_CODICE_FORNITORE_NAV);
    }

    @Test
    @Transactional
    public void updateNonExistingProduttore() throws Exception {
        int databaseSizeBeforeUpdate = produttoreRepository.findAll().size();

        // Create the Produttore
        ProduttoreDTO produttoreDTO = produttoreMapper.toDto(produttore);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProduttoreMockMvc.perform(put("/api/produttores")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(produttoreDTO)))
            .andExpect(status().isCreated());

        // Validate the Produttore in the database
        List<Produttore> produttoreList = produttoreRepository.findAll();
        assertThat(produttoreList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteProduttore() throws Exception {
        // Initialize the database
        produttoreRepository.saveAndFlush(produttore);
        int databaseSizeBeforeDelete = produttoreRepository.findAll().size();

        // Get the produttore
        restProduttoreMockMvc.perform(delete("/api/produttores/{id}", produttore.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Produttore> produttoreList = produttoreRepository.findAll();
        assertThat(produttoreList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Produttore.class);
        Produttore produttore1 = new Produttore();
        produttore1.setId(1L);
        Produttore produttore2 = new Produttore();
        produttore2.setId(produttore1.getId());
        assertThat(produttore1).isEqualTo(produttore2);
        produttore2.setId(2L);
        assertThat(produttore1).isNotEqualTo(produttore2);
        produttore1.setId(null);
        assertThat(produttore1).isNotEqualTo(produttore2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProduttoreDTO.class);
        ProduttoreDTO produttoreDTO1 = new ProduttoreDTO();
        produttoreDTO1.setId(1L);
        ProduttoreDTO produttoreDTO2 = new ProduttoreDTO();
        assertThat(produttoreDTO1).isNotEqualTo(produttoreDTO2);
        produttoreDTO2.setId(produttoreDTO1.getId());
        assertThat(produttoreDTO1).isEqualTo(produttoreDTO2);
        produttoreDTO2.setId(2L);
        assertThat(produttoreDTO1).isNotEqualTo(produttoreDTO2);
        produttoreDTO1.setId(null);
        assertThat(produttoreDTO1).isNotEqualTo(produttoreDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(produttoreMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(produttoreMapper.fromId(null)).isNull();
    }
}
