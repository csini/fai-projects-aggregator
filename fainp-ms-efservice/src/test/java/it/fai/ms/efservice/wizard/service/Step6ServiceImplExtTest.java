package it.fai.ms.efservice.wizard.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.TipoServizioServiceExt;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.ContractStep6;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceDocumentsStep6BodyRequest;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DeviceStep6;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.DocumentStep6Response;
import it.fai.ms.efservice.wizard.web.rest.vm.step6.VehicleStep6;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class Step6ServiceImplExtTest {

  @Autowired TipoServizioServiceExt tpService;

  Step6ServiceImpl step6Service;

  String codiceAzienda = "0046348";

  @Test
  public void getDocuments_assistenza_veicoli() throws Exception {
    DeviceDocumentsStep6BodyRequest request = makeAssistenzaVeicoliContract();
    List<DocumentStep6Response> arr = step6Service.getDocuments(request, codiceAzienda);

    assertThat(arr.size()).isEqualTo(1);

    DocumentStep6Response e = arr.get(0);
    assertThat(e.getNome()).isEqualTo(TipoDispositivoEnum.ASSISTENZA_VEICOLI.name());
    assertThat(e.isUpload()).isTrue();
    assertThat(e.isReservationFields()).isFalse();
    assertThat(e.getDownloadUri()).isEqualTo("/servicedocuments/download/new-contract/precompiled/ASSISTENZA_VEICOLI/" + codiceAzienda + "?property1=identificativo");
    assertThat(e.getVehicle()).isNotNull();
  }

  @Test
  public void getDocuments_visrpo() throws Exception {
    ContractDocumentsStep6BodyRequest request = makeVisproContract();
    List<DocumentStep6Response> arr = step6Service.getDocuments(request, codiceAzienda);

    assertThat(arr.size()).isEqualTo(1);

    DocumentStep6Response e = arr.get(0);
    assertThat(e.getNome()).isEqualTo(TipoDispositivoEnum.VISPRO.name());
    assertThat(e.isUpload()).isTrue();
    assertThat(e.isReservationFields()).isFalse();
    assertThat(e.getDownloadUri()).isEqualTo("/servicedocuments/download/precompiled-contract/VISPRO/" + codiceAzienda);
    assertThat(e.getVehicle()).isNull();
  }

  @Before
  public void setUp() throws Exception {
    step6Service = new Step6ServiceImpl(tpService);
  }

  @After
  public void after() throws Exception { }

  private DeviceDocumentsStep6BodyRequest makeAssistenzaVeicoliContract() {
    DeviceDocumentsStep6BodyRequest request = new DeviceDocumentsStep6BodyRequest();
    VehicleStep6 vehicle = new VehicleStep6();
    vehicle.setIdentificativo("identificativo");
    List<VehicleStep6> vehicles = new ArrayList<>();
    vehicles.add(vehicle);
    DeviceStep6 device = new DeviceStep6();
    device.setTipoDispositivo(TipoDispositivoEnum.ASSISTENZA_VEICOLI.name());
    device.setVehicles(vehicles);
    List<DeviceStep6> devices = new ArrayList<>();
    devices.add(device);
    request.setDevices(devices);
    return request;
  }

  private ContractDocumentsStep6BodyRequest makeVisproContract() {
    ContractDocumentsStep6BodyRequest request = new ContractDocumentsStep6BodyRequest();
    request.setContracts(arrayContractsStep6(TipoDispositivoEnum.VISPRO.name()));
    return request;
  }

  private List<ContractStep6> arrayContractsStep6(String string) {
    List<ContractStep6> arr = new ArrayList<>();
    arr.add(addContractStep6(string));
    return arr;
  }

  private ContractStep6 addContractStep6(String tipoDispositivo) {
    ContractStep6 c = new ContractStep6();
    c.setContrattoAltroFornitore(true);
    c.setTipoDispositivo(tipoDispositivo);
    return c;
  }

}
