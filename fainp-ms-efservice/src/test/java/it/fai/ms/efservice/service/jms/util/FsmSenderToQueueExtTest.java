package it.fai.ms.efservice.service.jms.util;

import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.efservice.domain.*;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.OrdineClienteServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;
import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;


import javax.inject.Inject;
@RunWith(SpringJUnit4ClassRunner.class)

@SpringBootTest(
  classes = {JmsConfiguration.class, JmsTopicSenderService.class})
@Import({FsmSenderToQueue.class})
public class FsmSenderToQueueExtTest {
  @Mock
  Logger log;
  //@Mock
  //@Inject
  JmsTopicSenderService senderJmsService = new JmsTopicSenderService();

  //@Mock
  //sOrdineCliente ordineCliente;

  @Mock
  Contratto contratto;

  @Mock
  ClienteFai clienteFai;

  @Mock
  Richiesta richiesta;

  @Mock
  OrdineClienteServiceExt ordineClienteServiceExt;
  //@InjectMocks
  @Inject
  FsmSenderToQueue fsmSenderToQueue = new FsmSenderToQueue(senderJmsService);

  //Inject
  FaiMessageSystem faiMessageSystem;
  public FsmSenderToQueueExtTest() throws Exception {
  }

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  //@Test
  public void testSendMessageTo() throws Exception {
    Long codice = 1L;
    Richiesta r = new Richiesta();
    r.setId(codice);
    r.setIdentificativo("ABCD");
    r.setTipo(TipoRichiesta.NUOVO_DISPOSITIVO);
    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente("0073935");
    OrdineCliente ordineCliente = new OrdineCliente();
    ordineCliente.setIdentificativo("123");
    ordineCliente.setClienteAssegnatario(clienteFai);
    Contratto contratto = new Contratto();
    contratto.setId(codice);
    contratto.setCodContrattoCliente("00064816");
    r.setContratto(contratto);
    r.setOrdineCliente(ordineCliente);

    senderJmsService.setOrdineClienteServiceExt(ordineClienteServiceExt);
    /*
     new Richiesta(identificativo: "ABCD", tipo: TipoRichiesta.NUOVO_DISPOSITIVO, contratto: new Contratto(codContrattoCliente: "00064816"), ordineCliente: new OrdineCliente(identificativo: "123", clienteAssegnatario: new ClienteFai(codiceCliente: "0073935"))) || true

    clienteFai.setCodiceCliente("0073935");
    new Richiesta("ABCD", TipoRichiesta.NUOVO_DISPOSITIVO,
      new Contratto("00064816"), new OrdineCliente("123", new ClienteFai("0073935"))) ;
*/
    boolean result = fsmSenderToQueue.sendMessageTo(r);
    Assert.assertEquals(true, result);
  }

  @Test
  public void testSendMessageForChangeStatoDispositivo() throws Exception {
    Dispositivo dispositivo = new Dispositivo();
    dispositivo.setId(1L);
    dispositivo.setIdentificativo("45444545454");
    dispositivo.setContratto(getContratto(1L));
    senderJmsService.setFaiMessageSystem(faiMessageSystem);
    fsmSenderToQueue.sendMessageForChangeStatoDispositivo(dispositivo, DispositivoEvent.ACCETTATO);
  }
  Contratto getContratto(Long codice){
    Contratto contratto = new Contratto();
    contratto.setId(codice);
    contratto.setCodContrattoCliente("00064816");

    return contratto;
  }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
