/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestContextManager;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmAccettazione;

/**
 * @author Luca Vassallo
 */
@RunWith(Parameterized.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmAccettazioneSimpleTest {

  @Autowired
  private FsmAccettazione fsmAccettazione;

  private TestContextManager manager;

  private Richiesta richiesta;

  private List<RichiestaEvent> expectedCommands;

  @Before // Set-up
  public void setUp() throws Exception {

    this.manager = new TestContextManager(getClass());
    this.manager.prepareTestInstance(this);

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    expectedCommands = new ArrayList<RichiestaEvent>(Arrays.asList(RichiestaEvent.MU_ANNULLAMENTO_UTENTE,
                                                                   RichiestaEvent.MU_VALUTAZIONE_PRECONDIZIONI,
                                                                   RichiestaEvent.MU_ACCETTA_RICHIESTA,
                                                                   RichiestaEvent.MU_RIFIUTATA_RICHIESTA,
                                                                   RichiestaEvent.MU_ANNULLAMENTO_OPERATORE));

    richiesta = new Richiesta();
    richiesta.setId(RandomUtils.nextLong());
    richiesta.setIdentificativo(UUID.randomUUID()
                                    .toString());
    richiesta.setTipo(TipoRichiesta.NUOVO_ORDINE);
    richiesta.setStato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    richiesta.setData(Instant.now());
    
    

    TipoDispositivo tipoDispositivo = new TipoDispositivo();
    tipoDispositivo.setNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    richiesta.setTipoDispositivo(tipoDispositivo);

    Set<TipoDispositivo> tipoDispositivos = new HashSet<>();
    tipoDispositivos.add(tipoDispositivo);
    Produttore p = new Produttore().tipoDispositivos(tipoDispositivos);
    p.setId(1L);
    tipoDispositivo.setProduttore(p);

    OrdineCliente oc = new OrdineCliente();
    oc.setIdentificativo("0987123456TEST");
    richiesta.setOrdineCliente(oc);
    ClienteFai clienteFai = new ClienteFai().codiceCliente("::codeClient-test::");
    clienteFai.setId(2727L);
    oc.clienteAssegnatario(clienteFai);
    
    Contratto contratto = newContratto(p, clienteFai);
    contratto.setId(91929394L);
    richiesta.setContratto(contratto);

    Set<Dispositivo> dispositivos = new HashSet<>();
    Dispositivo d = new Dispositivo();
    d.setTipoDispositivo(tipoDispositivo);
    dispositivos.add(d);
    richiesta.setDispositivos(dispositivos);
  }

  @Parameter(value = 0)
  public String notaUtente;

  @Parameter(value = 1)
  public RichiestaEvent event;

  @Parameter(value = 2)
  public StatoRichiesta state;

  @Parameters
  public static Collection<Object[]> data() {
    Collection<Object[]> transition = new ArrayList<>();
    transition.add(new Object[] { "NOTA ANNULLAMENTO UTENTE", RichiestaEvent.MU_ANNULLAMENTO_UTENTE,
                                  StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE });
    transition.add(new Object[] { "NOTA ANNULLAMENTO OPERATORE", RichiestaEvent.MU_ANNULLAMENTO_OPERATORE,
                                  StatoRichiesta.RICHIESTA_ANNULLATA });
    transition.add(new Object[] { "NOTA ACCETTATA", RichiestaEvent.MU_ACCETTA_RICHIESTA, StatoRichiesta.ATTESA_RISPOSTA });
    transition.add(new Object[] { "NOTA RIFIUTATA", RichiestaEvent.MU_RIFIUTATA_RICHIESTA, StatoRichiesta.RICHIESTA_RIFIUTATA });
    transition.add(new Object[] { "NOTA SOSPESA", RichiestaEvent.MU_VALUTAZIONE_PRECONDIZIONI, StatoRichiesta.SOSPESA });

    return transition;
  }

  @Test
  public void FsmAccettazione() throws Exception {

    assertList(expectedCommands);

    assertChangeState(event, state);

  }

  private void assertChangeState(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsmAccettazione.isAvailableCommand(event, richiesta);
    assertTrue(availableCommand);

    fsmAccettazione.executeCommandToChangeState(event, richiesta, notaUtente);
    StatoRichiesta fsmStatus = richiesta.getStato();
    assertEquals(state.name(), fsmStatus.name());

    String ultimaNota = richiesta.getUltimaNota();
    assertEquals(notaUtente, ultimaNota);
  }

  private void assertList(List<RichiestaEvent> expectedCommands) {
    Collection<RichiestaEvent> allAvailableCommand = fsmAccettazione.getAllAvailableCommand(richiesta);
    for (RichiestaEvent fsmEvent : allAvailableCommand) {
      assertTrue(expectedCommands.contains(fsmEvent));
    }
  }
  
  private Contratto newContratto(Produttore p, ClienteFai clienteFai) {
    return new Contratto().produttore(p).clienteFai(clienteFai).stato(StatoContratto.ATTIVO).dataModificaStato(Instant.now());
  }

}
