package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.TipoDispositivoServiceExt;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmContrattoCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmContratto;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class FsmContrattoTest {

  private final static String CODICE_CLIENTE_FAI = "0046348";

  @Autowired
  private ClienteFaiRepository clienteFaiRepo;

  @Autowired
  private TipoDispositivoServiceExt tipoDispositivoService;

  @Autowired
  private ContrattoServiceExt contrattoServiceExt;

  @Autowired
  private FsmContrattoCacheService cacheContrattoFsm;

  @Autowired
  private FsmContratto fsm;

  private Contratto contratto;

  private ClienteFai clienteFai;

  private Produttore produttore;

  private Long idContratto = 987111L;

  @Before
  public void setUp() {
    ClienteFai clienteFai = clienteFaiRepo.findOneByCodiceCliente(CODICE_CLIENTE_FAI);
    Produttore produttore = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TELEPASS_EUROPEO)
                                                  .getProduttore();
    contratto = new Contratto().clienteFai(clienteFai)
                               .produttore(produttore)
                               .stato(StatoContratto.ATTIVO)
                               .dataModificaStato(Instant.now());
    contratto.setId(idContratto);
  }

  @After
  public void afterTest() {
    cacheContrattoFsm.clearCache(String.valueOf(idContratto));
  }

  @Test
  public void FsmTestChangeStatusFromAttivoToSospendi() throws Exception {
    changeStatus(contratto, ContrattoEvent.MS_SOSPENDI, StatoContratto.SOSPESO);
  }

  @Test
  public void FsmTestChangeStatusFromAttivoToRevocato() throws Exception {
    changeStatus(contratto, ContrattoEvent.MU_REVOCA, StatoContratto.REVOCATO, "REVOCA");
  }

  @Test
  public void FsmTestChangeStatusFromAttivoToRiattivoBySospeso() throws Exception {
    changeStatus(contratto, ContrattoEvent.MS_SOSPENDI, StatoContratto.SOSPESO);
    changeStatus(contratto, ContrattoEvent.MS_RIATTIVA, StatoContratto.ATTIVO);
  }

  @Test
  public void FsmTestChangeStatusFromAttivoToRiattivoByRevocatoNotPossible() throws Exception {
    changeStatus(contratto, ContrattoEvent.MU_REVOCA, StatoContratto.REVOCATO);
    fsm.isAvailableCommand(ContrattoEvent.MS_RIATTIVA, contratto);
  }

  @Test
  public void FsmTestIsAvailableCommandSospendi() {
    Collection<ContrattoEvent> allAvailableCommand = fsm.getAllAvailableCommand(contratto);
    Optional<ContrattoEvent> contractEventOpt = allAvailableCommand.stream()
                                                                   .filter(event -> event.equals(ContrattoEvent.MS_SOSPENDI))
                                                                   .findFirst();
    assertThat(contractEventOpt.isPresent()).isTrue();
  }

  @Test
  public void FsmTestIsAvailableCommandRevoca() throws Exception {
    Collection<ContrattoEvent> allAvailableCommand = fsm.getAllAvailableCommand(contratto);
    Optional<ContrattoEvent> contractEventOpt = allAvailableCommand.stream()
                                                                   .filter(event -> event.equals(ContrattoEvent.MU_REVOCA))
                                                                   .findFirst();
    assertThat(contractEventOpt.isPresent()).isTrue();
  }

  @Test
  public void FsmTestIsAvailableCommandRiattiva() throws Exception {
    changeStatus(contratto, ContrattoEvent.MS_SOSPENDI, StatoContratto.SOSPESO);
    Collection<ContrattoEvent> allAvailableCommand = fsm.getAllAvailableCommand(contratto);
    Optional<ContrattoEvent> contractEventOpt = allAvailableCommand.stream()
                                                                   .filter(event -> event.equals(ContrattoEvent.MS_RIATTIVA))
                                                                   .findFirst();
    assertThat(contractEventOpt.isPresent()).isTrue();
  }

  @Test
  @Transactional
  public void FsmContrattoPersistNewTest() {
    produttore = tipoDispositivoService.findOneByNome(TipoDispositivoEnum.TES_CONV_CARBURANTI)
                                       .getProduttore();
    Contratto contract = contrattoServiceExt.createContrattoByProduttoreAndClienteFai(produttore, clienteFai);
    String operazioniPossibili = contract.getOperazioniPossibili();
    assertThat(operazioniPossibili).contains(ContrattoEvent.MU_REVOCA.name());

    contrattoServiceExt.delete(contract);
  }

  private void changeStatus(Contratto contract, ContrattoEvent event, StatoContratto stato) throws Exception {
    String nota = "";
    changeStatus(contract, event, stato, nota);
  }

  private void changeStatus(Contratto contract, ContrattoEvent event, StatoContratto stato, String nota) throws Exception {
    contract = fsm.executeCommandToChangeState(event, contract, nota);
    assertThat(contract.getStato()).isEqualTo(stato);
  }

}
