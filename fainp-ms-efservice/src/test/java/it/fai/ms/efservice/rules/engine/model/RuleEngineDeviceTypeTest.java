package it.fai.ms.efservice.rules.engine.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class RuleEngineDeviceTypeTest {

  @Test
  public void testEqualsObject() {
    RuleEngineDeviceType ruleEngineDeviceType1 = new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::id::"));
    RuleEngineDeviceType ruleEngineDeviceType2 = new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::id::"));
    assertThat(ruleEngineDeviceType1).isEqualTo(ruleEngineDeviceType2);
  }

  @Test
  public void testHashCode() {
    RuleEngineDeviceType ruleEngineDeviceType1 = new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::id::"));
    RuleEngineDeviceType ruleEngineDeviceType2 = new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::id::"));
    assertThat(ruleEngineDeviceType1.hashCode()).isEqualTo(ruleEngineDeviceType2.hashCode());
  }

  @SuppressWarnings("unused")
  @Test(expected = IllegalArgumentException.class)
  public void testRuleEngineDeviceTypeWhenIdIsNull() {
    new RuleEngineDeviceType(null);
  }

}
