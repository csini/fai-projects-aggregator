package it.fai.ms.efservice.service.jms.consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.dto.efservice.ContrattoNavDTO;
import it.fai.ms.efservice.service.ContrattoNavService;
import it.fai.ms.efservice.service.fsm.enumeration.ContrattoEvent;

public class ClienteFaiToChangeStatusContrattoConsumerTest {

  private ClienteFaiToChangeStatusContrattoConsumer consumer;

  private ContrattoNavService contrattoNavService = mock(ContrattoNavService.class);

  private String codiceCliente = "::codice-cliente::";
  
  private String identificativoContratto = "UUID_contratto";

  @Before
  public void setUp() throws Exception {
    consumer = new ClienteFaiToChangeStatusContrattoConsumer(contrattoNavService);
  }

  @Test(expected = RuntimeException.class)
  public void messageDmlNotValidForNull() {
    consumer.consume(null);
    verifyZeroInteractions(contrattoNavService);
  }

  @Test(expected = RuntimeException.class)
  public void messageDmlNotValidForEmptyCodiceCliente() {
    AziendaDMLDTO dml = new AziendaDMLDTO();
    dml.setCodiceAzienda(null);
    consumer.consume(dml);
    verifyZeroInteractions(contrattoNavService);
  }

  @Test(expected = RuntimeException.class)
  public void messageDmlNotValidForNotEmptyCodiceClienteAndEmptyStato() {
    AziendaDMLDTO dml = new AziendaDMLDTO();
    dml.setCodiceAzienda(codiceCliente);
    dml.setStato("");
    consumer.consume(dml);
    verifyZeroInteractions(contrattoNavService);
  }

  @Test
  public void messageDmlWithStatoIsNotBloccato() {
    AziendaDMLDTO dml = new AziendaDMLDTO();
    dml.setCodiceAzienda(codiceCliente);
    dml.setStato("ATTIVO");
    consumer.consume(dml);
    verifyZeroInteractions(contrattoNavService);
  }
  
  @Test
  public void messageDmlWithStatoBloccato() throws Exception {
    AziendaDMLDTO dml = new AziendaDMLDTO();
    dml.setCodiceAzienda(codiceCliente);
    dml.setStato("BLOCCATO");
    List<ContrattoNavDTO> newContratti = newContratti();
    when(contrattoNavService.findContractsToNav(codiceCliente)).thenReturn(newContratti);
    consumer.consume(dml);
    verify(contrattoNavService).findContractsToNav(codiceCliente);
    verify(contrattoNavService).changeStatusContractByNav(newContratti.get(0).getIdentificativo(), ContrattoEvent.MU_REVOCA);
  }

  private List<ContrattoNavDTO> newContratti() {
    ContrattoNavDTO contrattoNavDTO = new ContrattoNavDTO();
    contrattoNavDTO.setIdentificativo(identificativoContratto);
    return new ArrayList<>(Arrays.asList(contrattoNavDTO));
  }

}
