package it.fai.ms.efservice.service.jms.consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.time.OffsetDateTime;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import it.fai.ms.common.jms.dto.anagazienda.TrasformazioneDTO;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;

public class NotificaTrasformazioneConsumerImplTest {

  private ClienteFaiServiceExt      clienteFaiServiceExt = mock(ClienteFaiServiceExt.class);
  private final ContrattoServiceExt contrattoService     = mock(ContrattoServiceExt.class);

  NotificaTrasformazioneConsumerImpl notificaTrasformazioneConsumer;
  private String                     CODICE_AZIENDA_A  = "::codiceAziendaA::";
  private String                     CODICE_AZIENDA_DA = "::codiceAziendaDa::";

  private Produttore telepass;
  // private Contratto contrattoTelepass1;
  //
  private Produttore tollCollect;
  // private Contratto contrattoTollCollect1;

  private ClienteFai clienteA;
  private ClienteFai clienteDA;

  @Before
  public void setUp() throws Exception {

    notificaTrasformazioneConsumer = new NotificaTrasformazioneConsumerImpl(clienteFaiServiceExt, contrattoService);

    telepass = new Produttore().nome("TELEPASS");
    telepass.setId(1l);
    tollCollect = new Produttore().nome("TOLL_COLLECT");
    tollCollect.setId(2l);

    clienteA = new ClienteFai();
    clienteDA = new ClienteFai();

    given(clienteFaiServiceExt.findByCodiceCliente(CODICE_AZIENDA_A)).willReturn(clienteA);
    given(clienteFaiServiceExt.findByCodiceCliente(CODICE_AZIENDA_DA)).willReturn(clienteDA);

    given(clienteFaiServiceExt.save(any())).willAnswer(returnsFirstArg());
  }

  @Test(expected = IllegalArgumentException.class)
  public void consume_null_should_throw_IllegalArgumentException() {
    TrasformazioneDTO notificaTrasformazione = null;
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);
  }

  @Test(expected = IllegalArgumentException.class)
  public void consume_without_codiceAziendaA_should_throw_IllegalArgumentException() {
    final TrasformazioneDTO notificaTrasformazione = new TrasformazioneDTO().codiceAziendaDa(CODICE_AZIENDA_DA);
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);
  }

  @Test(expected = IllegalArgumentException.class)
  public void consume_without_codiceAziendaDa_should_throw_IllegalArgumentException() {
    final TrasformazioneDTO notificaTrasformazione = new TrasformazioneDTO().codiceAziendaA(CODICE_AZIENDA_A);
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);
  }

  @Test
  public void given_clienteA_with_a_contrattoTelepass_and_clienteDA_with_a_contrattoTollCollect_consume_should_assign_at_clienteA_contrattoTollCollect() {

    // GIVEN
    final TrasformazioneDTO notificaTrasformazione = new TrasformazioneDTO().codiceAziendaA(CODICE_AZIENDA_A)
                                                                            .codiceAziendaDa(CODICE_AZIENDA_DA)
                                                                            .data(OffsetDateTime.now());

    Contratto contrattoTelepass = new Contratto().produttore(telepass)
                                                 .codContrattoCliente("TP001")
                                                 .stato(StatoContratto.ATTIVO);
    Contratto contrattoTollCollect = new Contratto().produttore(tollCollect)
                                                    .codContrattoCliente("TC999")
                                                    .stato(StatoContratto.ATTIVO);
    addContrattoToClienteFai(contrattoTelepass, clienteA);
    addContrattoToClienteFai(contrattoTollCollect, clienteDA);

    given(contrattoService.save(any())).willReturn(contrattoTollCollect.clienteFai(clienteA));

    // WHEN
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);

    // THEN
    ArgumentCaptor<ClienteFai> clienteFaiArgumentCaptor = ArgumentCaptor.forClass(ClienteFai.class);
    then(clienteFaiServiceExt).should(times(2))
                              .save(clienteFaiArgumentCaptor.capture());

    ClienteFai clienteDA = clienteFaiArgumentCaptor.getAllValues()
                                                   .get(0);
    ClienteFai clienteA = clienteFaiArgumentCaptor.getAllValues()
                                                  .get(1);

    assertThat(clienteDA.getContrattos()).isEmpty();

    assertThat(clienteA.getContrattos()).containsExactly(contrattoTelepass, contrattoTollCollect);

    assertThat(contrattoTelepass.isPrimario()).isTrue();

    assertThat(contrattoTollCollect.isPrimario()).isTrue();

  }

  @Test
  public void given_clienteA_with_a_contrattoTelepass_and_clienteDA_with_a_contrattoTollCollect_REVOCATO_consume_should_NOT_assign_at_clienteA_contrattoTollCollect() {

    // GIVEN
    final TrasformazioneDTO notificaTrasformazione = new TrasformazioneDTO();
    notificaTrasformazione.codiceAziendaA(CODICE_AZIENDA_A)
                          .codiceAziendaDa(CODICE_AZIENDA_DA);

    Contratto contrattoTelepass = new Contratto().produttore(telepass)
                                                 .codContrattoCliente("TP001")
                                                 .stato(StatoContratto.ATTIVO);
    Contratto contrattoTollCollect = new Contratto().produttore(tollCollect)
                                                    .codContrattoCliente("TC999")
                                                    .stato(StatoContratto.REVOCATO);
    addContrattoToClienteFai(contrattoTelepass, clienteA);
    addContrattoToClienteFai(contrattoTollCollect, clienteDA);

    // WHEN
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);

    // THEN
    ArgumentCaptor<ClienteFai> clienteFaiArgumentCaptor = ArgumentCaptor.forClass(ClienteFai.class);
    then(clienteFaiServiceExt).should(times(2))
                              .save(clienteFaiArgumentCaptor.capture());

    ClienteFai clienteDA = clienteFaiArgumentCaptor.getAllValues()
                                                   .get(0);
    ClienteFai clienteA = clienteFaiArgumentCaptor.getAllValues()
                                                  .get(1);

    assertThat(clienteDA.getContrattos()).containsExactly(contrattoTollCollect);

    assertThat(clienteA.getContrattos()).containsExactly(contrattoTelepass);

  }

  @Test
  public void given_clienteA_with_a_contrattoTelepass1_and_clienteDA_with_a_contrattoTelepass2_consume_should_assign_at_clienteA_contrattoTelepass1_with_MASTER_false_contrattoTelepass2_with_MASTER_true() {

    // GIVEN
    final TrasformazioneDTO notificaTrasformazione = new TrasformazioneDTO();
    notificaTrasformazione.codiceAziendaA(CODICE_AZIENDA_A)
                          .codiceAziendaDa(CODICE_AZIENDA_DA);

    Contratto contrattoTelepass1 = new Contratto().produttore(telepass)
                                                  .codContrattoCliente("TP001")
                                                  .stato(StatoContratto.ATTIVO);
    Contratto contrattoTelepass2 = new Contratto().produttore(telepass)
                                                  .codContrattoCliente("TP002")
                                                  .stato(StatoContratto.ATTIVO);

    addContrattoToClienteFai(contrattoTelepass1, clienteA);
    addContrattoToClienteFai(contrattoTelepass2, clienteDA);

    given(contrattoService.save(any())).willReturn(contrattoTelepass2.primario(false)
                                                                     .clienteFai(clienteA));

    // WHEN
    notificaTrasformazioneConsumer.consume(notificaTrasformazione);

    then(contrattoService).should(times(1))
                          .save(contrattoTelepass2);

    // THEN
    ArgumentCaptor<ClienteFai> clienteFaiArgumentCaptor = ArgumentCaptor.forClass(ClienteFai.class);
    then(clienteFaiServiceExt).should(times(2))
                              .save(clienteFaiArgumentCaptor.capture());

    ClienteFai clienteDA = clienteFaiArgumentCaptor.getAllValues()
                                                   .get(0);
    ClienteFai clienteA = clienteFaiArgumentCaptor.getAllValues()
                                                  .get(1);

    assertThat(clienteA.getContrattos()).containsExactly(contrattoTelepass1, contrattoTelepass2);

    assertThat(contrattoTelepass1.isPrimario()).isTrue();

    assertThat(contrattoTelepass2.isPrimario()).isFalse();

    assertThat(clienteDA.getContrattos()).isEmpty();

  }

  private void addContrattoToClienteFai(Contratto contratto, ClienteFai clienteFai) {
    contratto.setClienteFai(clienteFai);
    clienteFai.getContrattos()
              .add(contratto);
  }
}
