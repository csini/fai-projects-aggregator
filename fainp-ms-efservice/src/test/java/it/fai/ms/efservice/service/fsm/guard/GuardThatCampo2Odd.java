/**
 * 
 */
package it.fai.ms.efservice.service.fsm.guard;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.testdomain.TestFSM;

/**
 * @author Luca Vassallo
 */
public class GuardThatCampo2Odd implements Guard<StateTest, EventTest> {

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.guard.Guard#evaluate(org.springframework.statemachine.StateContext)
   */
  @Override
  public boolean evaluate(StateContext<StateTest, EventTest> ctx) {
    Object obj = ctx.getMessageHeader("itemTest");
    if (obj instanceof TestFSM) {
      TestFSM testFsm = (TestFSM) obj;
      String campo1 = testFsm.getCampo1();
      if (campo1 == null) {
        campo1 = "";
      }

      return !isEven(campo1.length());
    }

    return false;
  }

  private boolean isEven(int num) {
    if (num % 2 == 0) {
      return true;
    }
    return false;
  }

}
