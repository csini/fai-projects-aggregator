package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_TesseraCaronteTest {

  private RuleEngineDeviceType ruleEngineDevice;

  private RuleEngineVehicle ruleEngineVehicle;

  @Before
  public void setUp() throws Exception {
    ruleEngineDevice = newRuleContextDeviceType();
    ruleEngineVehicle = newRuleContextVehicle();
  }

  @Test
  public void testDeviceIsNotTesseraCaronte() {
    RuleOutcome ruleOutcome = new RuleEngine_Device_TesseraCaronte(ruleEngineDevice, ruleEngineVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }
  
  @Test
  public void testDeviceIsTelepass() {
    ruleEngineDevice = new RuleEngineDeviceType(new RuleEngineDeviceTypeId(TipoDispositivoEnum.TELEPASS_EUROPEO.name()));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TesseraCaronte(ruleEngineDevice, ruleEngineVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }
  
  @Test
  public void testDeviceIsTrackyCard() {
    ruleEngineDevice = new RuleEngineDeviceType(new RuleEngineDeviceTypeId(TipoDispositivoEnum.TRACKYCARD.name()));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TesseraCaronte(ruleEngineDevice, ruleEngineVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }
  
  @Test
  public void testDeviceIsTollCollect() {
    ruleEngineDevice = new RuleEngineDeviceType(new RuleEngineDeviceTypeId(TipoDispositivoEnum.TOLL_COLLECT.name()));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TesseraCaronte(ruleEngineDevice, ruleEngineVehicle).executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testDeviceIsTesseraCaronte() {
    ruleEngineDevice = new RuleEngineDeviceType(new RuleEngineDeviceTypeId(TipoDispositivoEnum.TESSERA_CARONTE.name()));
    RuleOutcome ruleOutcome = new RuleEngine_Device_TesseraCaronte(ruleEngineDevice, ruleEngineVehicle).executeRule();
    
    assertThat(ruleOutcome.getOutcome()).isFalse();
    Optional<RuleFailure> failure = ruleOutcome.getFailure();
    assertThat(failure.isPresent()).isTrue();
    assertThat(failure.get().isTesseraCaronteNotActivable()).isTrue();
    
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

}
