package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.RichiestaRepositoryExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.FsmAccettazione;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmInoltroTelepassEuropeo;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeFurtoSmarrimento;
import it.fai.ms.efservice.web.rest.util.CreateRichiestaTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore("Skipped")
public class FsmModificaIntegrationTest {

  @Autowired
  private FsmAccettazione fsmAccettazioneTest;

  @Autowired
  private FsmInoltroTelepassEuropeo fsmInoltroTeTest;

  @Autowired
  private FsmModificaTeFurtoSmarrimento fsmFurtoSmarrimento;

  @Autowired
  private RichiestaRepositoryExt RichiestaRepo;

  @Autowired
  private DispositivoRepository dispositivoRepo;

  @Autowired
  private ContrattoRepositoryExt contrattoRepo;

  @Autowired
  private CreateRichiestaTestUtil createOrderTest;

  // private TestContextManager manager;

  private Richiesta entityTest;

  @Before // Set-up
  public void setUp() throws Exception {

    // this.manager = new TestContextManager(getClass());
    // this.manager.prepareTestInstance(this);

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);
  }

  @Test
  @Ignore("This is an Integration TEST....")
  public void fsmModifica_001() throws Exception {
    entityTest = createOrderTest.createRichiestaByCarrelloDTO();
    RichiestaRepo.save(entityTest);

    // First Flow: Accettato → Completo → Da inoltrare → Inoltrato → Analisi risposta → Accettato fornitore →
    // Preparazione spedizione → Da spedire a cura di Fai → Spedito Parziale → Attivo/Evaso
    Set<Dispositivo> dispositivos = entityTest.getDispositivos();
    Iterator<Dispositivo> iterator = dispositivos.iterator();
    while (iterator.hasNext()) {
      Dispositivo next = iterator.next();
      next.setStato(StatoDispositivo.ACCETTATO_DA_FORNITORE);
      next.setModalitaSpedizione(ModalitaSpedizione.A_FAI);
      dispositivoRepo.save(next);
    }
    entityTest.setDispositivos(dispositivos);
    Contratto c = createContratto();
    entityTest.setContratto(c);

    assertChangeStateFsmAccettazione(RichiestaEvent.MU_ACCETTA_RICHIESTA, StatoRichiesta.INOLTRATO);
    assertChangeStateFsmInoltroTe(RichiestaEvent.MS_ORDRI_RISIM, StatoRichiesta.ACCETTATO_FORNITORE);
    assertChangeStateFsmInoltroTe(RichiestaEvent.MS_OBUNO, StatoRichiesta.DA_SPEDIRE_A_CURA_DI_FAI);
    assertChangeStateFsmInoltroTe(RichiestaEvent.MU_CHIUSURA_ORDINE, StatoRichiesta.ATTIVO_EVASO);

    Richiesta RichiestaReload = RichiestaRepo.findOne(entityTest.getId());
    Richiesta richiesta = new Richiesta();
    richiesta.setTipo(TipoRichiesta.FURTO_CON_SOSTITUZIONE);
    Contratto c1 = createContratto();
    richiesta.setContratto(c1);
    Set<Dispositivo> dispRubati = new HashSet<>();
    Set<Dispositivo> dispositivi = RichiestaReload.getDispositivos();
    for (Dispositivo dispositivo : dispositivi) {
      dispRubati.add(dispositivo);
      break;
    }
    richiesta.setDispositivos(dispRubati);
    richiesta.getOrdineCliente()
             .setClienteAssegnatario(RichiestaReload.getOrdineCliente()
                                                    .getClienteAssegnatario());
    richiesta.setOrdineCliente(RichiestaReload.getOrdineCliente());

    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    richiesta.setData(Instant.now());
    richiesta = RichiestaRepo.save(richiesta);

    List<Richiesta> listOrdini = RichiestaRepo.findAll();
    int countOrdini = (listOrdini != null) ? listOrdini.size() : 0;

    richiesta = fsmFurtoSmarrimento.executeCommandToChangeState(RichiestaEvent.INITIAL, richiesta);
    Thread.sleep(10000);
    richiesta = RichiestaRepo.save(richiesta);
    StatoRichiesta stato = richiesta.getStato();
    assertThat(stato).isEqualTo(StatoRichiesta.ASSENZA_ALLEGATO);

    List<Richiesta> ordini = RichiestaRepo.findAll();
    int count = (ordini != null) ? ordini.size() : 0;
    Thread.sleep(10000);
    assertThat(count).isEqualTo(countOrdini + 1);
  }

  private void assertChangeStateFsmAccettazione(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsmAccettazioneTest.isAvailableCommand(event, entityTest);
    assertTrue(availableCommand);

    fsmAccettazioneTest.executeCommandToChangeState(event, entityTest);
    StatoRichiesta fsmStatus = entityTest.getStato();
    assertEquals(state.name(), fsmStatus.name());

  }

  private void assertChangeStateFsmInoltroTe(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsmInoltroTeTest.isAvailableCommand(event, entityTest);
    assertTrue(availableCommand);

    fsmInoltroTeTest.executeCommandToChangeState(event, entityTest);
    StatoRichiesta fsmStatus = entityTest.getStato();
    assertEquals(state.name(), fsmStatus.name());
  }

  private Contratto createContratto() {
    Contratto contratto = new Contratto().stato(StatoContratto.ATTIVO)
                                         .dataModificaStato(Instant.now());
    contratto.setCodContrattoCliente(UUID.randomUUID()
                                         .toString());

    contrattoRepo.save(contratto);
    return contratto;
  }

}
