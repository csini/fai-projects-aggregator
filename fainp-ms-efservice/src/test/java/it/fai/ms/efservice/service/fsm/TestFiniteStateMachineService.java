/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;
import org.springframework.stereotype.Service;

import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.hazelcastcache.FsmCache1Service;
import it.fai.ms.efservice.service.fsm.persist.InMemoryStateMachinePersistTest;
import it.fai.ms.efservice.service.fsm.testdomain.FSMEntityTest;

/**
 * @author Luca Vassallo
 */
@Service
public class TestFiniteStateMachineService<T extends FSMEntityTest> {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  protected String idMachine = "fsm1";

  @Qualifier("config1")
  @Autowired
  private StateMachineFactory<StateTest, EventTest> stateMachineFactory;
  
  @Autowired
  private FsmCache1Service cacheService;

  private InMemoryStateMachinePersistTest stateMachinePersist;

  @PostConstruct
  public void init() {
    stateMachinePersist = new InMemoryStateMachinePersistTest();
  }

  public void executeCommandToChangeState(Enum<?> command, T obj) throws Exception {
    executeCommandToChangeState(command, obj, null);
  }

  public void executeCommandToChangeState(Enum<?> command, T obj, String nota) throws Exception {

    StateMachine<StateTest, EventTest> stateMachine = getStateMachine();
    stateMachine.start();

    // Use to persist in memory;
    // StateMachinePersister<StateTest, EventTest, String> persister = new
    // DefaultStateMachinePersister<>(stateMachinePersist);

    String contextObj = "" + obj.getIdentificativo();
    if (StringUtils.isNotBlank(contextObj)) {
      stateMachine = cacheService.restore(stateMachine, contextObj);// persister.restore(stateMachine, contextObj);
    }

    EventTest cmd = null;
    if (command instanceof EventTest) {
      cmd = (EventTest) command;
    }

    if (cmd != null) {
      final Message<EventTest> message = MessageBuilder.withPayload(cmd)
                                                       .setHeader("itemTest", obj)
                                                       .setHeader("nota", nota)
                                                       .build();
      stateMachine.sendEvent(message);
      log.info("Send command: " + command + " to obj: " + obj.toString());

      cacheService.persist(stateMachine, contextObj);
      // persister.persist(stateMachine, contextObj);
    } else {
      log.warn("Enum command (RichiestaEvent) is: " + command + " - class: " + command.getClass());
    }

    log.info("FINISH cmd: " + command + " to obj: " + obj.toString());
  }

  private StateMachine<StateTest, EventTest> getStateMachine() {
    StateMachine<StateTest, EventTest> stateMachine = stateMachineFactory.getStateMachine(idMachine);
    return stateMachine;
  }

  public Collection<EventTest> getAllAvailableCommand(T obj) {
    Enum<?> fsmStatus = obj.findFsmStatus();
    Collection<EventTest> events = new ArrayList<>();
    Collection<Transition<StateTest, EventTest>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StateTest, EventTest>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StateTest, EventTest> tx = iterator.next();
      StateTest source = tx.getSource()
                           .getId();
      if (source.name()
                .equals(fsmStatus.name())) {
        Trigger<StateTest, EventTest> trigger = tx.getTrigger();
        if (trigger != null) {
          EventTest event = trigger.getEvent();
          events.add(event);
        }
      }
    }

    return events;
  }

  public Collection<EventTest> getUserAvailableCommand(T obj) {
    Collection<EventTest> allAvailableCommand = getAllAvailableCommand(obj);
    Collection<EventTest> userAvailableCommand = new ArrayList<>();

    for (EventTest eventTest : allAvailableCommand) {
      String eventName = eventTest.name();

      if (eventName.startsWith("MU_")) {
        userAvailableCommand.add(eventTest);
      }
    }

    return userAvailableCommand;
  }

  public boolean isAvailableCommand(Enum<?> command, T obj) {
    Collection<EventTest> allAvailableCommand = getAllAvailableCommand(obj);
    for (EventTest eventTest : allAvailableCommand) {
      if (eventTest.name()
                   .equalsIgnoreCase(command.name())) {
        return true;
      }
    }
    return false;
  }

  public boolean isAutoTransition(Enum<?> command) {
    boolean foundCommand = true;
    log.info("Command to check: " + command);
    Collection<Transition<StateTest, EventTest>> transitions = getStateMachine().getTransitions();
    Iterator<Transition<StateTest, EventTest>> iterator = transitions.iterator();
    while (iterator.hasNext()) {
      Transition<StateTest, EventTest> tx = iterator.next();
      log.info("Transition: " + tx.toString());
      Trigger<StateTest, EventTest> trigger = tx.getTrigger();
      if (trigger != null) {
        log.info("Trigger: " + trigger.toString());
        EventTest event = trigger.getEvent();
        log.info("EVENT: " + event.name());
        if (event.name()
                 .equalsIgnoreCase(command.name())) {
          foundCommand = false;
          break;
        }
      }
    }

    return foundCommand;
  }

}
