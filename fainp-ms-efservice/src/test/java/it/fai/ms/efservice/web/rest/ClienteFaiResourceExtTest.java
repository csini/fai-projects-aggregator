package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.params.ClientPNames;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.dto.ClienteFaiDTO;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class ClienteFaiResourceExtTest {

  private static final String CODICE_CLIENTE = "0046348";

  private static final String BASE_PATH = "/api/public";

  private static final String CLIENTE_FAI_BY_CODICE = "/clientefai";

  @Autowired
  private ClienteFaiServiceExt clienteFaiServiceExt;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  private MockMvc restMockMvc;

  @Before
  public void setUp() throws Exception {

    final ClienteFaiResourceExt clienteFaiResourceExt = new ClienteFaiResourceExt(clienteFaiServiceExt);
    this.restMockMvc = MockMvcBuilders.standaloneSetup(clienteFaiResourceExt)
                                               .alwaysDo(MockMvcResultHandlers.print())
                                               .setCustomArgumentResolvers(pageableArgumentResolver)
                                               .setControllerAdvice(exceptionTranslator)
                                               .setMessageConverters(jacksonMessageConverter)
                                               .build();
  }

  @Test
  public void getClienteFaiByCodiceTest() throws Exception {

    MvcResult result = restMockMvc.perform(get(BASE_PATH + CLIENTE_FAI_BY_CODICE + "/" + CODICE_CLIENTE).contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();


    ObjectMapper objMapper = new ObjectMapper();
    objMapper.registerModule(new JavaTimeModule());
    JsonNode json = objMapper.readTree(result.getResponse().getContentAsString());

    ClienteFaiDTO clienteFaiDTO = objMapper.convertValue(json, ClienteFaiDTO.class);
    assertThat(clienteFaiDTO).isNotNull();
    assertThat(clienteFaiDTO.getCodiceCliente()).isEqualTo(CODICE_CLIENTE);
  }

  @Test
  public void getClienteFaiByCodiceExceptionTest() throws Exception {

    restMockMvc.perform(get(BASE_PATH + CLIENTE_FAI_BY_CODICE + "/").contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError())
                .andReturn();
  }

}
