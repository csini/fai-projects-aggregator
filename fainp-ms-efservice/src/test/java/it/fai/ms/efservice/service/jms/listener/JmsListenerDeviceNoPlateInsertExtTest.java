package it.fai.ms.efservice.service.jms.listener;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import javax.jms.ObjectMessage;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.jms.dto.DispositivoNotargaDTO;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepositoryExt;
import it.fai.ms.efservice.service.jms.listener.device.JmsListenerDeviceNoPlateInsert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@Ignore("Test  di integrazione molto lento")
public class JmsListenerDeviceNoPlateInsertExtTest {

  @Autowired
  JmsListenerDeviceNoPlateInsert jmsListenerDeviceNoPlateInsert;

  @Autowired
  DispositivoRepositoryExt dispositivoRepository;

  @Autowired
  ClienteFaiRepository clienteFaiRepository;

  @Autowired
  ContrattoRepositoryExt contrattoRepository;

  @Before
  public void setup() {

  }

  @After
  public void after() {

  }

  @Test
  public void test_jms_listener() throws Exception {
    long size_before = dispositivoRepository.count(); 
    long cliente_size_before = clienteFaiRepository.count(); 
    long contratto_size_before = contrattoRepository.count(); 

    DispositivoNotargaDTO dto = new DispositivoNotargaDTO();
    dto.setCodiceCliente(RandomString());
    dto.setContractNumber(RandomString());
    dto.setSeriale(RandomString());
    dto.setData(LocalDate.now());

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setCitta(RandomString());
    clienteFai.setCodiceCliente(dto.getCodiceCliente());
    clienteFai.setVia(RandomString());
    clienteFai.setDmlRevisionTimestamp(Instant.now());

    clienteFai = clienteFaiRepository.saveAndFlush(clienteFai);

    ObjectMessage message = mock(ObjectMessage.class);
    when(message.getObject()).thenReturn(dto);

    jmsListenerDeviceNoPlateInsert.onMessage(message);

    assertThat(size_before + 1).isEqualTo(dispositivoRepository.count()); 
    assertThat(contratto_size_before + 1).isEqualTo(contrattoRepository.count()); 
    assertThat(cliente_size_before + 1).isEqualTo(clienteFaiRepository.count()); 

    Dispositivo dispositivo = dispositivoRepository.findFirstByOrderByIdDesc();
    Contratto contratto = contrattoRepository.findFirstByOrderByIdDesc();

    dispositivoRepository.delete(dispositivo);
    contrattoRepository.delete(contratto);
    clienteFaiRepository.delete(clienteFai);
  }

  @Test
  public void test_integration_001() throws Exception {
    long size_before = dispositivoRepository.count(); 
    long cliente_size_before = clienteFaiRepository.count(); 
    long contratto_size_before = contrattoRepository.count(); 

    DispositivoNotargaDTO dto = new DispositivoNotargaDTO();
    dto.setCodiceCliente("0046348");
    dto.setContractNumber("118946365");
    dto.setSeriale("00997905784");
    dto.setData(LocalDate.now());

    ObjectMessage message = mock(ObjectMessage.class);
    when(message.getObject()).thenReturn(dto);

    jmsListenerDeviceNoPlateInsert.onMessage(message);

    assertThat(size_before + 1).isEqualTo(dispositivoRepository.count()); 
    assertThat(contratto_size_before).isEqualTo(contrattoRepository.count()); 
    assertThat(cliente_size_before).isEqualTo(clienteFaiRepository.count()); 

    Dispositivo dispositivo = dispositivoRepository.findFirstByOrderByIdDesc();
    dispositivoRepository.delete(dispositivo);
  }

  static String RandomString() {
    return RandomStringUtils.random(20, true, true);
  }

  static String RandomUUID() {
    return UUID.randomUUID()
        .toString();
  }
}
