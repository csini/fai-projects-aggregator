package it.fai.ms.efservice.web.rest;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.service.dto.CategoriaServizioDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class CategoriaServizioResourceIntExtTest {
	

	@Autowired
	CategoriaServizioResourceExt categoriaServizioResourceExt;
	
	@Before()
	public void setup() {}
	
	@After
	public void clean_all() {
	}
	
	@Test
	public void getListaCategoriaServizio() throws Exception {
		ResponseEntity<List<CategoriaServizioDTO>> listaRE = categoriaServizioResourceExt.getListaCategoriaServizio();
	    List<CategoriaServizioDTO> lista = listaRE.getBody();	    
	    FieldOrdinamentoComparator comparator = new FieldOrdinamentoComparator();
	    assertThat(lista).isSortedAccordingTo(comparator);
	}
}