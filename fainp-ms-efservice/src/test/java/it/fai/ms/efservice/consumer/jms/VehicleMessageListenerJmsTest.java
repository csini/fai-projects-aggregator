package it.fai.ms.efservice.consumer.jms;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import it.fai.ms.efservice.consumer.model.VehicleMessage;
import it.fai.ms.efservice.consumer.spi.VehicleConsumer;

public class VehicleMessageListenerJmsTest {

  private VehicleMessageListenerJms messageListener;
  private List<VehicleConsumer>     consumers = new LinkedList<>();

  @Before
  public void setUp() throws Exception {
    VehicleConsumer mockVehicleCosumer1 = mock(VehicleConsumer.class);
    VehicleConsumer mockVehicleCosumer2 = mock(VehicleConsumer.class);
    consumers.add(mockVehicleCosumer1);
    consumers.add(mockVehicleCosumer2);
    messageListener = new VehicleMessageListenerJms(consumers);
  }

  @Test
  public void testReceive() {
    final VehicleMessage vehicleMessage = new VehicleMessage();
    messageListener.receive(vehicleMessage);

    verify(consumers.get(0)).consume(vehicleMessage);
    verify(consumers.get(1)).consume(vehicleMessage);
  }

}
