package it.fai.ms.efservice.wizard.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceType;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardVehicleId;

@RunWith(MockitoJUnitRunner.class)
public class WizardDeviceTypesForVehicleRepositoryImplTest {

  private WizardDeviceTypesForVehicleRepositoryImpl wizardDeviceTypesForVehicleRepository;

  @Mock
  private AssociazioneDVRepository devicesForVehicleRepository;

  @Before
  public void setUp() throws Exception {
    wizardDeviceTypesForVehicleRepository = new WizardDeviceTypesForVehicleRepositoryImpl(devicesForVehicleRepository);
  }

  @Test
  public void testFindByWizardVehicleId() {
    given(devicesForVehicleRepository.findByUuidVeicolo("::vehicleId::")).willReturn(newDeviceTypesForVehicle());

    Set<WizardDeviceType> wizardDeviceTypes = wizardDeviceTypesForVehicleRepository.findByWizardVehicleId(new WizardVehicleId("::vehicleId::"));

    verify(devicesForVehicleRepository).findByUuidVeicolo("::vehicleId::");
    verifyNoMoreInteractions(devicesForVehicleRepository);

    assertThat(wizardDeviceTypes).containsExactlyInAnyOrder(new WizardDeviceType(new WizardDeviceTypeId(TipoDispositivoEnum.TELEPASS_EUROPEO.name())));
  }

  private List<AssociazioneDV> newDeviceTypesForVehicle() {
    List<AssociazioneDV> list = new LinkedList<>();
    AssociazioneDV devicesForVehicle = new AssociazioneDV();
    Dispositivo dispositivo = new Dispositivo();
    TipoDispositivo tipoDispositivo = new TipoDispositivo();
    tipoDispositivo.setNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    dispositivo.setTipoDispositivo(tipoDispositivo);
    devicesForVehicle.setDispositivo(dispositivo);
    list.add(devicesForVehicle);
    return list;
  }

}
