/**
 * 
 */
package it.fai.ms.efservice.service.fsm.persist;

import java.util.HashMap;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;

/**
 * @author Luca Vassallo
 */
public class InMemoryStateMachinePersistTest<S, E> implements StateMachinePersist<S, E, String> {

  private final HashMap<String, StateMachineContext<S, E>> contexts = new HashMap<>();

  @Override
  public void write(StateMachineContext<S, E> context, String contextObj) throws Exception {
    contexts.put(contextObj, context);
  }

  @Override
  public StateMachineContext<S, E> read(String contextObj) throws Exception {
    return contexts.get(contextObj);
  }

}
