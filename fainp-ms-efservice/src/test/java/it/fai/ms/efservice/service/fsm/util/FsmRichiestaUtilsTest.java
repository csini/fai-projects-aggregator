/**
 * 
 */
package it.fai.ms.efservice.service.fsm.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.statemachine.trigger.Trigger;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.service.OrdineClienteService;
import it.fai.ms.efservice.service.RichiestaServiceExt;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.jms.JmsTopicSenderService;

@RunWith(MockitoJUnitRunner.class)
@Ignore("Ignore test")
public class FsmRichiestaUtilsTest {

  @Mock
  private RichiestaServiceExt richiestaServiceMock;

  @Mock
  private ClienteFaiRepository clienteFaiRepoMock;

  @Mock
  private JmsTopicSenderService senderJmsMock;

  @Mock
  private OrdineClienteService ordineClienteServiceMock;

  private FsmRichiestaUtilService fsmOrderUtil;

  private Richiesta richiesta;

  private ClienteFai clienteFai;

  @Mock
  private Appender<ILoggingEvent>      mockAppender;
  @Captor
  private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

  private static final String IDENTIFICATIVO_RICHIESTA = "1234567890-A";

  private static final String MESSAGE_LOG = "INFO TRANSITION:\n\t- IdentificativoRichiesta: {}\n\t- Data: {}\n\t- Transizione => Da: [{}] A: [{}]\n\t- UltimaNota: {}\n\t- OperatoreFai: {}";

  private static final String MESSAGE_MANUAL_CHANGE_STATUS = "cambio stato MANUALE";

  private static final Long ORDINE_CLIENTE_ID = 0L;

  private static final Long ID_ASSEGNATARIO = 1L;
  
  private OrdineCliente ordineCliente;

  @Before
  public void setUp() {
    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    root.addAppender(mockAppender);

    ordineCliente = new OrdineCliente();
    ordineCliente.setId(ORDINE_CLIENTE_ID);
    richiesta = new Richiesta();
    richiesta.setOrdineCliente(ordineCliente);
    ordineCliente.setRichiestas(new HashSet<>(Arrays.asList(richiesta)));
    richiesta.setId(5678L);
    richiesta.setStato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE);
    richiesta.setIdentificativo(IDENTIFICATIVO_RICHIESTA);
    clienteFai = new ClienteFai();
    clienteFai.setCodiceCliente("ABCDEFG");
    clienteFai.setId(1L);
    richiesta.getOrdineCliente()
             .setClienteAssegnatario(clienteFai);

    fsmOrderUtil = new FsmRichiestaUtilService(richiestaServiceMock, clienteFaiRepoMock, ordineClienteServiceMock, senderJmsMock);
  }

  @Test
  public void switchState() {
    Transition<StatoRichiesta, RichiestaEvent> transition = newMockTransitions().get(0);
    StatoRichiesta toState = StatoRichiesta.ACCETTATO;

    fsmOrderUtil.switchToState(richiesta, toState, transition);

    /*
     * USE to test log, but now use completableFuture with log and create error in test; LV
     * 
     * verify(mockAppender, atLeast(4)).doAppend(captorLoggingEvent.capture()); List<LoggingEvent> allValues =
     * captorLoggingEvent.getAllValues(); int size = allValues.size(); LoggingEvent loggingEvent = allValues.get(size -
     * 1); assertLogging(loggingEvent, Level.INFO, MESSAGE_LOG);
     */
  }

  private void assertLogging(LoggingEvent loggingEvent, Level logLevelExpected, String msg) {
    Level logLevel = loggingEvent.getLevel();
    assertThat(logLevel, is(logLevelExpected));
    String message = loggingEvent.getMessage();
    assertThat(message).contains(msg);
  }

  private Transition<StatoRichiesta, RichiestaEvent> newMockTransition(StatoRichiesta from, RichiestaEvent event, StatoRichiesta to) {
    Transition<StatoRichiesta, RichiestaEvent> mockTransition = mock(Transition.class);
    State<StatoRichiesta, RichiestaEvent> mockStateFrom = mock(State.class);
    State<StatoRichiesta, RichiestaEvent> mockStateTo = mock(State.class);
    Trigger<StatoRichiesta, RichiestaEvent> mockTrigger = mock(Trigger.class);

    when(mockStateFrom.getId()).thenReturn(from);
    when(mockStateTo.getId()).thenReturn(to);
    when(mockTrigger.getEvent()).thenReturn(event);
    when(mockTransition.getSource()).thenReturn(mockStateFrom);
    when(mockTransition.getTarget()).thenReturn(mockStateTo);
    if (event != null) {
      when(mockTransition.getTrigger()).thenReturn(mockTrigger);
    }
    return mockTransition;
  }

  private List<Transition<StatoRichiesta, RichiestaEvent>> newMockTransitions() {
    List<Transition<StatoRichiesta, RichiestaEvent>> transitions = new ArrayList<>();
    transitions.add(newMockTransition(StatoRichiesta.ACCETTATO_PRONTO_PER_INOLTRO, RichiestaEvent.INITIAL, StatoRichiesta.ACCETTATO));
    transitions.add(newMockTransition(StatoRichiesta.ACCETTATO, null, StatoRichiesta.INCOMPLETO_TECNICO));
    transitions.add(newMockTransition(StatoRichiesta.INCOMPLETO_TECNICO, null, StatoRichiesta.ATTESA_RISPOSTA));
    transitions.add(newMockTransition(StatoRichiesta.ATTESA_RISPOSTA, RichiestaEvent.MS_CTRRI,
                                      StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO));
    transitions.add(newMockTransition(StatoRichiesta.ANALISI_RISPOSTA_CODICE_CONTRATTO, null,
                                      StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO));
    transitions.add(newMockTransition(StatoRichiesta.ORDINE_SOSPESO_CODICE_CONTRATTO, null, StatoRichiesta.INCOMPLETO_TECNICO));
    transitions.add(newMockTransition(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE, null, StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE));
    return transitions;
  }

}
