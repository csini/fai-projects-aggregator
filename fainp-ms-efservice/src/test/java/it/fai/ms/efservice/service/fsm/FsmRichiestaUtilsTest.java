package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmRichiestaUtilsTest {

  @Autowired
  private FsmRichiestaUtils fsmRichiestaUtils;

  private Richiesta richiesta;

  private AbstractFsmRichiesta fsm;

  @Before
  public void setUp() throws Exception {
    long timestamp = System.currentTimeMillis();
    richiesta = new Richiesta().identificativo(timestamp + "")
                               .tipo(TipoRichiesta.DISATTIVAZIONE)
                               .stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                               .tipoDispositivo(newTipoDispositivo(TipoDispositivoEnum.ASSISTENZA_VEICOLI));

  }

  @Test
  public void testAssistenzaVeicoli() {

    newRichiesta(TipoRichiesta.NUOVO_ORDINE, TipoDispositivoEnum.ASSISTENZA_VEICOLI);
    fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
    assertThat(fsm).isNotNull();

    newRichiesta(TipoRichiesta.VARIAZIONE_TARGA, TipoDispositivoEnum.ASSISTENZA_VEICOLI);
    fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
    assertThat(fsm).isNotNull();

    newRichiesta(TipoRichiesta.DISATTIVAZIONE, TipoDispositivoEnum.ASSISTENZA_VEICOLI);
    fsm = fsmRichiestaUtils.retrieveFsmByRichiesta(richiesta);
    assertThat(fsm).isNotNull();
  }

  private TipoDispositivo newTipoDispositivo(TipoDispositivoEnum type) {
    return new TipoDispositivo().nome(type);
  }

  private void newRichiesta(TipoRichiesta requestType, TipoDispositivoEnum deviceType) {
    long timestamp = System.currentTimeMillis();
    richiesta = new Richiesta().identificativo(timestamp + "")
                               .tipo(requestType)
                               .stato(StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE)
                               .tipoDispositivo(newTipoDispositivo(deviceType));
  }

}
