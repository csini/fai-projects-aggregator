package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.IndirizzoSpedizioneOrdini;
import it.fai.ms.efservice.repository.IndirizzoSpedizioneOrdiniRepository;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniQueryService;
import it.fai.ms.efservice.service.IndirizzoSpedizioneOrdiniService;
import it.fai.ms.efservice.service.dto.IndirizzoSpedizioneOrdiniDTO;
import it.fai.ms.efservice.service.mapper.IndirizzoSpedizioneOrdiniMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the IndirizzoSpedizioneOrdiniResource REST controller.
 *
 * @see IndirizzoSpedizioneOrdiniResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class IndirizzoSpedizioneOrdiniResourceIntTest {

  private static final String DEFAULT_VIA = "AAAAAAAAAA";
  private static final String UPDATED_VIA = "BBBBBBBBBB";

  private static final String DEFAULT_CITTA = "AAAAAAAAAA";
  private static final String UPDATED_CITTA = "BBBBBBBBBB";

  private static final String DEFAULT_CAP = "AAAAAAAAAA";
  private static final String UPDATED_CAP = "BBBBBBBBBB";

  private static final String DEFAULT_PROVINCIA = "AAAAAAAAAA";
  private static final String UPDATED_PROVINCIA = "BBBBBBBBBB";

  private static final String DEFAULT_PAESE = "AAAAAAAAAA";
  private static final String UPDATED_PAESE = "BBBBBBBBBB";

  private static final Boolean DEFAULT_FAI = false;
  private static final Boolean UPDATED_FAI = true;

  @Autowired
  private IndirizzoSpedizioneOrdiniRepository indirizzoSpedizioneOrdiniRepository;

  @Autowired
  private IndirizzoSpedizioneOrdiniMapper indirizzoSpedizioneOrdiniMapper;

  @Autowired
  private IndirizzoSpedizioneOrdiniService indirizzoSpedizioneOrdiniService;

  @Autowired
  private IndirizzoSpedizioneOrdiniQueryService indirizzoSpedizioneOrdiniQueryService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restIndirizzoSpedizioneOrdiniMockMvc;

  private IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final IndirizzoSpedizioneOrdiniResource indirizzoSpedizioneOrdiniResource = new IndirizzoSpedizioneOrdiniResource(indirizzoSpedizioneOrdiniService,
                                                                                                                      indirizzoSpedizioneOrdiniQueryService);
    this.restIndirizzoSpedizioneOrdiniMockMvc = MockMvcBuilders.standaloneSetup(indirizzoSpedizioneOrdiniResource)
                                                               .setCustomArgumentResolvers(pageableArgumentResolver)
                                                               .setControllerAdvice(exceptionTranslator)
                                                               .setMessageConverters(jacksonMessageConverter)
                                                               .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static IndirizzoSpedizioneOrdini createEntity(EntityManager em) {
    IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini = new IndirizzoSpedizioneOrdini().via(DEFAULT_VIA)
                                                                                         .citta(DEFAULT_CITTA)
                                                                                         .cap(DEFAULT_CAP)
                                                                                         .provincia(DEFAULT_PROVINCIA)
                                                                                         .paese(DEFAULT_PAESE)
                                                                                         .fai(DEFAULT_FAI);
    return indirizzoSpedizioneOrdini;
  }

  @Before
  public void initTest() {
    indirizzoSpedizioneOrdini = createEntity(em);
  }

  @Test
  @Transactional
  public void createIndirizzoSpedizioneOrdini() throws Exception {
    int databaseSizeBeforeCreate = indirizzoSpedizioneOrdiniRepository.findAll()
                                                                      .size();

    // Create the IndirizzoSpedizioneOrdini
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdini);
    restIndirizzoSpedizioneOrdiniMockMvc.perform(post("/api/indirizzo-spedizione-ordinis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                          .content(TestUtil.convertObjectToJsonBytes(indirizzoSpedizioneOrdiniDTO)))
                                        .andExpect(status().isCreated());

    // Validate the IndirizzoSpedizioneOrdini in the database
    List<IndirizzoSpedizioneOrdini> indirizzoSpedizioneOrdiniList = indirizzoSpedizioneOrdiniRepository.findAll();
    assertThat(indirizzoSpedizioneOrdiniList).hasSize(databaseSizeBeforeCreate + 1);
    IndirizzoSpedizioneOrdini testIndirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniList.get(indirizzoSpedizioneOrdiniList.size() - 1);
    assertThat(testIndirizzoSpedizioneOrdini.getVia()).isEqualTo(DEFAULT_VIA);
    assertThat(testIndirizzoSpedizioneOrdini.getCitta()).isEqualTo(DEFAULT_CITTA);
    assertThat(testIndirizzoSpedizioneOrdini.getCap()).isEqualTo(DEFAULT_CAP);
    assertThat(testIndirizzoSpedizioneOrdini.getProvincia()).isEqualTo(DEFAULT_PROVINCIA);
    assertThat(testIndirizzoSpedizioneOrdini.getPaese()).isEqualTo(DEFAULT_PAESE);
    assertThat(testIndirizzoSpedizioneOrdini.isFai()).isEqualTo(DEFAULT_FAI);
  }

  @Test
  @Transactional
  public void createIndirizzoSpedizioneOrdiniWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = indirizzoSpedizioneOrdiniRepository.findAll()
                                                                      .size();

    // Create the IndirizzoSpedizioneOrdini with an existing ID
    indirizzoSpedizioneOrdini.setId(1L);
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdini);

    // An entity with an existing ID cannot be created, so this API call must fail
    restIndirizzoSpedizioneOrdiniMockMvc.perform(post("/api/indirizzo-spedizione-ordinis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                          .content(TestUtil.convertObjectToJsonBytes(indirizzoSpedizioneOrdiniDTO)))
                                        .andExpect(status().isBadRequest());

    // Validate the IndirizzoSpedizioneOrdini in the database
    List<IndirizzoSpedizioneOrdini> indirizzoSpedizioneOrdiniList = indirizzoSpedizioneOrdiniRepository.findAll();
    assertThat(indirizzoSpedizioneOrdiniList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinis() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList
    restIndirizzoSpedizioneOrdiniMockMvc.perform(get("/api/indirizzo-spedizione-ordinis?sort=id,desc"))
                                        .andExpect(status().isOk())
                                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                        .andExpect(jsonPath("$.[*].id").value(hasItem(indirizzoSpedizioneOrdini.getId()
                                                                                                               .intValue())))
                                        .andExpect(jsonPath("$.[*].via").value(hasItem(DEFAULT_VIA.toString())))
                                        .andExpect(jsonPath("$.[*].citta").value(hasItem(DEFAULT_CITTA.toString())))
                                        .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
                                        .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
                                        .andExpect(jsonPath("$.[*].paese").value(hasItem(DEFAULT_PAESE.toString())))
                                        .andExpect(jsonPath("$.[*].fai").value(hasItem(DEFAULT_FAI.booleanValue())));
  }

  @Test
  @Transactional
  public void getIndirizzoSpedizioneOrdini() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get the indirizzoSpedizioneOrdini
    restIndirizzoSpedizioneOrdiniMockMvc.perform(get("/api/indirizzo-spedizione-ordinis/{id}", indirizzoSpedizioneOrdini.getId()))
                                        .andExpect(status().isOk())
                                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                        .andExpect(jsonPath("$.id").value(indirizzoSpedizioneOrdini.getId()
                                                                                                   .intValue()))
                                        .andExpect(jsonPath("$.via").value(DEFAULT_VIA.toString()))
                                        .andExpect(jsonPath("$.citta").value(DEFAULT_CITTA.toString()))
                                        .andExpect(jsonPath("$.cap").value(DEFAULT_CAP.toString()))
                                        .andExpect(jsonPath("$.provincia").value(DEFAULT_PROVINCIA.toString()))
                                        .andExpect(jsonPath("$.paese").value(DEFAULT_PAESE.toString()))
                                        .andExpect(jsonPath("$.fai").value(DEFAULT_FAI.booleanValue()));
  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByViaIsEqualToSomething() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where via equals to DEFAULT_VIA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("via.equals=" + DEFAULT_VIA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByViaIsInShouldWork() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where via in DEFAULT_VIA or UPDATED_VIA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("via.in=" + DEFAULT_VIA + "," + UPDATED_VIA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByViaIsNullOrNotNull() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where via is not null
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("via.specified=true");

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCittaIsEqualToSomething() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where citta equals to DEFAULT_CITTA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("citta.equals=" + DEFAULT_CITTA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCittaIsInShouldWork() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where citta in DEFAULT_CITTA or UPDATED_CITTA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("citta.in=" + DEFAULT_CITTA + "," + UPDATED_CITTA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCittaIsNullOrNotNull() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where citta is not null
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("citta.specified=true");
  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCapIsEqualToSomething() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where cap equals to DEFAULT_CAP
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("cap.equals=" + DEFAULT_CAP);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCapIsInShouldWork() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where cap in DEFAULT_CAP or UPDATED_CAP
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("cap.in=" + DEFAULT_CAP + "," + UPDATED_CAP);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByCapIsNullOrNotNull() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where cap is not null
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("cap.specified=true");
  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByProvinciaIsEqualToSomething() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where provincia equals to DEFAULT_PROVINCIA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("provincia.equals=" + DEFAULT_PROVINCIA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByProvinciaIsInShouldWork() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where provincia in DEFAULT_PROVINCIA or UPDATED_PROVINCIA
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("provincia.in=" + DEFAULT_PROVINCIA + "," + UPDATED_PROVINCIA);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByProvinciaIsNullOrNotNull() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where provincia is not null
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("provincia.specified=true");

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByPaeseIsEqualToSomething() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where paese equals to DEFAULT_PAESE
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("paese.equals=" + DEFAULT_PAESE);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByPaeseIsInShouldWork() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where paese in DEFAULT_PAESE or UPDATED_PAESE
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("paese.in=" + DEFAULT_PAESE + "," + UPDATED_PAESE);

  }

  @Test
  @Transactional
  public void getAllIndirizzoSpedizioneOrdinisByPaeseIsNullOrNotNull() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);

    // Get all the indirizzoSpedizioneOrdiniList where paese is not null
    defaultIndirizzoSpedizioneOrdiniShouldBeFound("paese.specified=true");
  }

  /**
   * Executes the search, and checks that the default entity is returned
   */
  private void defaultIndirizzoSpedizioneOrdiniShouldBeFound(String filter) throws Exception {

    restIndirizzoSpedizioneOrdiniMockMvc.perform(get("/api/indirizzo-spedizione-ordinis?sort=id,desc&" + filter))
                                        .andExpect(status().isOk())
                                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                                        .andExpect(jsonPath("$.[*].id").value(hasItem(indirizzoSpedizioneOrdini.getId()
                                                                                                               .intValue())))
                                        .andExpect(jsonPath("$.[*].via").value(hasItem(DEFAULT_VIA.toString())))
                                        .andExpect(jsonPath("$.[*].citta").value(hasItem(DEFAULT_CITTA.toString())))
                                        .andExpect(jsonPath("$.[*].cap").value(hasItem(DEFAULT_CAP.toString())))
                                        .andExpect(jsonPath("$.[*].provincia").value(hasItem(DEFAULT_PROVINCIA.toString())))
                                        .andExpect(jsonPath("$.[*].paese").value(hasItem(DEFAULT_PAESE.toString())))
                                        .andExpect(jsonPath("$.[*].fai").value(hasItem(DEFAULT_FAI.booleanValue())));
  }

  @Test
  @Transactional
  public void getNonExistingIndirizzoSpedizioneOrdini() throws Exception {
    // Get the indirizzoSpedizioneOrdini
    restIndirizzoSpedizioneOrdiniMockMvc.perform(get("/api/indirizzo-spedizione-ordinis/{id}", Long.MAX_VALUE))
                                        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateIndirizzoSpedizioneOrdini() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);
    int databaseSizeBeforeUpdate = indirizzoSpedizioneOrdiniRepository.findAll()
                                                                      .size();

    // Update the indirizzoSpedizioneOrdini
    IndirizzoSpedizioneOrdini updatedIndirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniRepository.findOne(indirizzoSpedizioneOrdini.getId());
    updatedIndirizzoSpedizioneOrdini.via(UPDATED_VIA)
                                    .citta(UPDATED_CITTA)
                                    .cap(UPDATED_CAP)
                                    .provincia(UPDATED_PROVINCIA)
                                    .paese(UPDATED_PAESE)
                                    .fai(UPDATED_FAI);
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = indirizzoSpedizioneOrdiniMapper.toDto(updatedIndirizzoSpedizioneOrdini);

    restIndirizzoSpedizioneOrdiniMockMvc.perform(put("/api/indirizzo-spedizione-ordinis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                         .content(TestUtil.convertObjectToJsonBytes(indirizzoSpedizioneOrdiniDTO)))
                                        .andExpect(status().isOk());

    // Validate the IndirizzoSpedizioneOrdini in the database
    List<IndirizzoSpedizioneOrdini> indirizzoSpedizioneOrdiniList = indirizzoSpedizioneOrdiniRepository.findAll();
    assertThat(indirizzoSpedizioneOrdiniList).hasSize(databaseSizeBeforeUpdate);
    IndirizzoSpedizioneOrdini testIndirizzoSpedizioneOrdini = indirizzoSpedizioneOrdiniList.get(indirizzoSpedizioneOrdiniList.size() - 1);
    assertThat(testIndirizzoSpedizioneOrdini.getVia()).isEqualTo(UPDATED_VIA);
    assertThat(testIndirizzoSpedizioneOrdini.getCitta()).isEqualTo(UPDATED_CITTA);
    assertThat(testIndirizzoSpedizioneOrdini.getCap()).isEqualTo(UPDATED_CAP);
    assertThat(testIndirizzoSpedizioneOrdini.getProvincia()).isEqualTo(UPDATED_PROVINCIA);
    assertThat(testIndirizzoSpedizioneOrdini.getPaese()).isEqualTo(UPDATED_PAESE);
    assertThat(testIndirizzoSpedizioneOrdini.isFai()).isEqualTo(UPDATED_FAI);
  }

  @Test
  @Transactional
  public void updateNonExistingIndirizzoSpedizioneOrdini() throws Exception {
    int databaseSizeBeforeUpdate = indirizzoSpedizioneOrdiniRepository.findAll()
                                                                      .size();

    // Create the IndirizzoSpedizioneOrdini
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO = indirizzoSpedizioneOrdiniMapper.toDto(indirizzoSpedizioneOrdini);

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restIndirizzoSpedizioneOrdiniMockMvc.perform(put("/api/indirizzo-spedizione-ordinis").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                         .content(TestUtil.convertObjectToJsonBytes(indirizzoSpedizioneOrdiniDTO)))
                                        .andExpect(status().isCreated());

    // Validate the IndirizzoSpedizioneOrdini in the database
    List<IndirizzoSpedizioneOrdini> indirizzoSpedizioneOrdiniList = indirizzoSpedizioneOrdiniRepository.findAll();
    assertThat(indirizzoSpedizioneOrdiniList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteIndirizzoSpedizioneOrdini() throws Exception {
    // Initialize the database
    indirizzoSpedizioneOrdiniRepository.saveAndFlush(indirizzoSpedizioneOrdini);
    int databaseSizeBeforeDelete = indirizzoSpedizioneOrdiniRepository.findAll()
                                                                      .size();

    // Get the indirizzoSpedizioneOrdini
    restIndirizzoSpedizioneOrdiniMockMvc.perform(delete("/api/indirizzo-spedizione-ordinis/{id}",
                                                        indirizzoSpedizioneOrdini.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                                        .andExpect(status().isOk());

    // Validate the database is empty
    List<IndirizzoSpedizioneOrdini> indirizzoSpedizioneOrdiniList = indirizzoSpedizioneOrdiniRepository.findAll();
    assertThat(indirizzoSpedizioneOrdiniList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(IndirizzoSpedizioneOrdini.class);
    IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini1 = new IndirizzoSpedizioneOrdini();
    indirizzoSpedizioneOrdini1.setId(1L);
    IndirizzoSpedizioneOrdini indirizzoSpedizioneOrdini2 = new IndirizzoSpedizioneOrdini();
    indirizzoSpedizioneOrdini2.setId(indirizzoSpedizioneOrdini1.getId());
    assertThat(indirizzoSpedizioneOrdini1).isEqualTo(indirizzoSpedizioneOrdini2);
    indirizzoSpedizioneOrdini2.setId(2L);
    assertThat(indirizzoSpedizioneOrdini1).isNotEqualTo(indirizzoSpedizioneOrdini2);
    indirizzoSpedizioneOrdini1.setId(null);
    assertThat(indirizzoSpedizioneOrdini1).isNotEqualTo(indirizzoSpedizioneOrdini2);
  }

  @Test
  @Transactional
  public void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(IndirizzoSpedizioneOrdiniDTO.class);
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO1 = new IndirizzoSpedizioneOrdiniDTO();
    indirizzoSpedizioneOrdiniDTO1.setId(1L);
    IndirizzoSpedizioneOrdiniDTO indirizzoSpedizioneOrdiniDTO2 = new IndirizzoSpedizioneOrdiniDTO();
    assertThat(indirizzoSpedizioneOrdiniDTO1).isNotEqualTo(indirizzoSpedizioneOrdiniDTO2);
    indirizzoSpedizioneOrdiniDTO2.setId(indirizzoSpedizioneOrdiniDTO1.getId());
    assertThat(indirizzoSpedizioneOrdiniDTO1).isEqualTo(indirizzoSpedizioneOrdiniDTO2);
    indirizzoSpedizioneOrdiniDTO2.setId(2L);
    assertThat(indirizzoSpedizioneOrdiniDTO1).isNotEqualTo(indirizzoSpedizioneOrdiniDTO2);
    indirizzoSpedizioneOrdiniDTO1.setId(null);
    assertThat(indirizzoSpedizioneOrdiniDTO1).isNotEqualTo(indirizzoSpedizioneOrdiniDTO2);
  }

  @Test
  @Transactional
  public void testEntityFromId() {
    assertThat(indirizzoSpedizioneOrdiniMapper.fromId(42L)
                                              .getId()).isEqualTo(42);
    assertThat(indirizzoSpedizioneOrdiniMapper.fromId(null)).isNull();
  }
}
