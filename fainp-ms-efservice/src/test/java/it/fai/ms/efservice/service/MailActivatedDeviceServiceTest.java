package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.email.EmailMessage.RECIPIENTS;
import it.fai.ms.common.jms.email.EmailMessageByTemplate;
import it.fai.ms.common.utils.MessageSourceServiceExt;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.client.dto.UserDTO;
import it.fai.ms.efservice.repository.dispositivo.TipoDispositivoAssegnabile;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class MailActivatedDeviceServiceTest {

  private GatewayClientService gatewayClientService = mock(GatewayClientService.class);

  private JmsProperties jmsProperties = mock(JmsProperties.class);

  @MockBean
  private EmailNotificationSender emailNotificationSender;

  private MailActivatedDeviceService mailService;
  private UserDTO                    user;

  @Before
  public void setUp() throws Exception {
    user = new UserDTO();
    user.setEmail("faiservice@faitest.it");
    when(jmsProperties.getUrl()).thenReturn("no-host");
    mailService = new MailActivatedDeviceService(gatewayClientService, emailNotificationSender);
  }

  @Test
  public void sendActivationViaCard() {
    String language = "it";
    user.setLangKey(language);

    when(gatewayClientService.getUserByCodiceCliente(anyString())).thenReturn(user);
    when(emailNotificationSender.sendNotification(any())).thenReturn(true);

    mailService.sendActivationViaCard(anyString());

    ArgumentCaptor<EmailMessageByTemplate> captor = ArgumentCaptor.forClass(EmailMessageByTemplate.class);
    verify(emailNotificationSender, times(1)).sendNotification(captor.capture());
    EmailMessageByTemplate dto = captor.getValue();
    assertThat(dto.getRecipients()
                  .get(RECIPIENTS.to.name())).contains("faiservice@faitest.it");

    assertThat(dto.getLangKey()).isEqualTo(language);

    String message = MessageSourceServiceExt.getMessage(MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, language);
    String subject = message.replace("#deviceType#", TipoDispositivoEnum.TELEPASS_ITALIANO.name());
    assertThat(dto.getSubject()).isEqualTo(subject);

    assertThat(dto.getTemplateName()).endsWith(MailActivatedDeviceService.TEMPLATE_ACTIVATION_VIACARD + "-" + language);

    Map<String, Object> templateParamsDto = dto.getTemplateParams();
    assertThat(templateParamsDto.keySet()).hasSize(2);
    assertThat(templateParamsDto.keySet()).contains("langKey","ragioneSociale");
  }

  @Test
  public void sendActivationTrackyCard() {
    String language = "EN";

    user.setLangKey(language);
    when(gatewayClientService.getUserByCodiceCliente(anyString())).thenReturn(user);

    mailService.sendActivationTrackyCard("::clientCode::");

    ArgumentCaptor<EmailMessageByTemplate> captor = ArgumentCaptor.forClass(EmailMessageByTemplate.class);
    verify(emailNotificationSender, times(1)).sendNotification(captor.capture());
    EmailMessageByTemplate dto = captor.getValue();
    assertThat(dto.getRecipients()
                  .get(RECIPIENTS.to.name())).contains("faiservice@faitest.it");

    assertThat(dto.getLangKey()).isEqualTo(language);

    String message = MessageSourceServiceExt.getMessage(MailActivatedDeviceService.SUBJECT_KEY_DEVICE_ACTIVE, language);
    String subject = message.replace("#deviceType#", TipoDispositivoEnum.GO_BOX.name());
    assertThat(dto.getSubject()).isEqualTo(subject);

    assertThat(dto.getTemplateName()).endsWith(MailActivatedDeviceService.TEMPLATE_ACTIVATION_TRACKYCARD + "-" + language.toLowerCase());

    Map<String, Object> templateParamsDto = dto.getTemplateParams();
    assertThat(templateParamsDto.keySet()).hasSize(2);
    assertThat(templateParamsDto.keySet()).contains("langKey","ragioneSociale");
  }

  @Test
  public void sendAssociationFromDeposito() {
    String language = "EN";

    user.setLangKey(language);
    when(gatewayClientService.getUserByCodiceCliente(anyString())).thenReturn(user);

    mailService.sendAssociationFromDeposito("::codiceCliente::", TipoDispositivoAssegnabile.BUONI_GRAN_SAN_BERNARDO, 10, language,
                                            new ArrayList<>());

    ArgumentCaptor<EmailMessageByTemplate> captor = ArgumentCaptor.forClass(EmailMessageByTemplate.class);
    verify(emailNotificationSender, times(1)).sendNotification(captor.capture());
    EmailMessageByTemplate dto = captor.getValue();
    assertThat(dto.getRecipients()
                  .get(RECIPIENTS.to.name())).contains("");
    assertThat(dto.getRecipients()
                  .get(RECIPIENTS.operatore.name())).contains("operatore");

    assertThat(dto.getLangKey()).isEqualTo(language);

    String message = MessageSourceServiceExt.getMessage(MailActivatedDeviceService.SUBJECT_KEY_DEVICES_ASSIGNED, language);
    String subject = message.replace("#deviceType#", TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO.name());
    subject = subject.replace("#numDevice#", "10");
    subject = subject.replace("#clientCode#", "::codiceCliente::");
    assertThat(dto.getSubject()).isEqualTo(subject);

    assertThat(dto.getTemplateName()).endsWith(MailActivatedDeviceService.TEMPLATE_OPERATOR_DEVICE + "-" + language.toLowerCase());

    Map<String, Object> templateParamsDto = dto.getTemplateParams();
    assertThat(templateParamsDto.keySet()).hasSize(5);
    assertThat(templateParamsDto.keySet()).contains("devices", "customerCode", "quantita", "deviceTypeAssigned","ragioneSociale");
  }

  @Test
  public void sendTrackyCardPin() {
    String language = "DE";

    user.setLangKey(language);
    when(gatewayClientService.getUserByCodiceCliente(anyString())).thenReturn(user);

    mailService.sendTrackyCardPin("::codiceCliente::","targa" ,"S/N TrackyCard", "::pinCode::");

    ArgumentCaptor<EmailMessageByTemplate> captor = ArgumentCaptor.forClass(EmailMessageByTemplate.class);
    verify(emailNotificationSender, times(1)).sendNotification(captor.capture());

    EmailMessageByTemplate dto = captor.getValue();

    assertThat(dto.getRecipients()
                  .get(RECIPIENTS.to.name())).contains("");

    assertThat(dto.getLangKey()).isEqualTo(language);
    assertThat(dto.getSubject()).isEqualTo("PIN request to TrackyCard S/N TrackyCard");
    assertThat(dto.getTemplateName()).endsWith(MailActivatedDeviceService.TEMPLATE_TRACKYCARD_PIN + "-" + language.toLowerCase());

    Map<String, Object> templateParamsDto = dto.getTemplateParams();
    assertThat(templateParamsDto.keySet()).hasSize(5);
    assertThat(templateParamsDto.keySet()).contains("pin","targa", "customerCode", "trackycardSeriale","ragioneSociale");
  }

}
