package it.fai.ms.efservice.wizard.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoDispositivoRepository;
import it.fai.ms.efservice.wizard.model.WizardDeviceTypeId;
import it.fai.ms.efservice.wizard.model.WizardServiceType;
import it.fai.ms.efservice.wizard.model.WizardServiceTypeId;

@RunWith(MockitoJUnitRunner.class)
public class WizardServiceTypeRepositoryImplTest {

  private WizardServiceTypeRepositoryImpl wizardServiceTypeRepository;

  @Mock
  private TipoDispositivoRepository deviceTypeRepository;

  @Before
  public void setUp() throws Exception {
    wizardServiceTypeRepository = new WizardServiceTypeRepositoryImpl(deviceTypeRepository);
  }

  @Test
  public void testFindAllByDeviceTypeId() {
    given(deviceTypeRepository.findAll()).willReturn(newTipoDispositivoList());

    Set<WizardServiceType> serviceTypeIds = wizardServiceTypeRepository.findAllByDeviceTypeId(new WizardDeviceTypeId(TipoDispositivoEnum.TELEPASS_EUROPEO.name()));

    verify(deviceTypeRepository).findAll();
    verifyNoMoreInteractions(deviceTypeRepository);

    assertThat(serviceTypeIds).containsExactlyInAnyOrder(new WizardServiceType(new WizardServiceTypeId("::serviceTypeName1::")),
                                                         new WizardServiceType(new WizardServiceTypeId("::serviceTypeName2::"))).extracting(serv -> serv.getOrdering()).contains(1L,2L);
    
  }

  private List<TipoDispositivo> newTipoDispositivoList() {
    List<TipoDispositivo> deviceTypes = new LinkedList<>();
    TipoDispositivo deviceType = new TipoDispositivo();
    deviceType.setNome(TipoDispositivoEnum.TELEPASS_EUROPEO);
    TipoServizio tipoServizio1 = new TipoServizio();
    tipoServizio1.setNome("::serviceTypeName1::");
    tipoServizio1.setOrdinamento(1L);
    TipoServizio tipoServizio2 = new TipoServizio();
    tipoServizio2.setNome("::serviceTypeName2::");
    tipoServizio2.setOrdinamento(2L);
    deviceType.addTipoServizio(tipoServizio1);
    deviceType.addTipoServizio(tipoServizio2);
    deviceTypes.add(deviceType);
    return deviceTypes;
  }

}
