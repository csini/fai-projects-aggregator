/**
 * 
 */
package it.fai.ms.efservice.service.fsm.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import it.fai.ms.efservice.service.fsm.actions.ActionT1;
import it.fai.ms.efservice.service.fsm.actions.ActionT2;
import it.fai.ms.efservice.service.fsm.actions.ActionT3;
import it.fai.ms.efservice.service.fsm.actions.ActionT4;
import it.fai.ms.efservice.service.fsm.actions.ActionT5;
import it.fai.ms.efservice.service.fsm.domain.enumaration.EventTest;
import it.fai.ms.efservice.service.fsm.domain.enumaration.StateTest;
import it.fai.ms.efservice.service.fsm.guard.GuardThatCampo2Even;
import it.fai.ms.efservice.service.fsm.guard.GuardThatCampo2Odd;
import it.fai.ms.efservice.service.fsm.listener.StateMachineEventListenerTest;
import it.fai.ms.efservice.service.fsm.listener.StateMachineMonitorTest;

/**
 * @author Luca Vassallo
 */
@Configuration
@EnableStateMachineFactory(name = "config2")
public class FiniteStateMachineConfiguration2 extends EnumStateMachineConfigurerAdapter<StateTest, EventTest> {

  private final static Logger log = LoggerFactory.getLogger(FiniteStateMachineConfiguration2.class);

  /*
   * (non-Javadoc)
   * @see org.springframework.statemachine.config.AbstractStateMachineConfigurerAdapter
   * #configure(org.springframework.statemachine.config.builders. StateMachineConfigurationConfigurer)
   */
  @Override
  public void configure(StateMachineConfigurationConfigurer<StateTest, EventTest> config) throws Exception {
    // TODO Auto-generated method stub
    super.configure(config);
    config.withConfiguration()
          .autoStartup(true);
    config.withMonitoring()
          .monitor(new StateMachineMonitorTest());

    config.withConfiguration()
          .machineId("fsm2");
    config.withConfiguration()
          .listener(new StateMachineEventListenerTest());
  }

  @Override
  public void configure(StateMachineStateConfigurer<StateTest, EventTest> states) throws Exception {
    states.withStates()
          .initial(StateTest.A, ctx -> log.info("Target: " + ctx.getTarget()
                                                                .getIds()))
          .state(StateTest.B, ctx -> log.info("Target: " + ctx.getTarget()
                                                              .getIds()))
          .state(StateTest.C, ctx -> log.info("Target: " + ctx.getTarget()
                                                              .getIds()))
          .state(StateTest.D, ctx -> log.info("Target: " + ctx.getTarget()
                                                              .getIds()));

  }

  @Override
  public void configure(StateMachineTransitionConfigurer<StateTest, EventTest> transitions) throws Exception {
    transitions.withExternal()
               .source(StateTest.A)
               .target(StateTest.B)
               .event(EventTest.T1)
               .action(new ActionT1())
               .and()
               .withExternal()
               .source(StateTest.A)
               .target(StateTest.C)
               .event(EventTest.T2)
               .action(new ActionT2())
               .and()
               .withExternal()
               .source(StateTest.B)
               .target(StateTest.C)
               .guard(new GuardThatCampo2Odd())
               .action(new ActionT3())
               .and()
               .withExternal()
               .source(StateTest.B)
               .target(StateTest.D)
               .guard(new GuardThatCampo2Even())
               .action(new ActionT4())
               .and()
               .withExternal()
               .source(StateTest.C)
               .target(StateTest.D)
               .event(EventTest.T5)
               .action(new ActionT5());

  }

}
