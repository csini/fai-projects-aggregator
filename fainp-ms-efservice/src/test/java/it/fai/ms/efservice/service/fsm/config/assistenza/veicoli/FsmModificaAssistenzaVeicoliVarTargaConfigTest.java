package it.fai.ms.efservice.service.fsm.config.assistenza.veicoli;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.AbstractFsmRichiesta;
import it.fai.ms.efservice.service.fsm.FsmFactory;
import it.fai.ms.efservice.service.fsm.cacheconfig.FsmRichiestaCacheService;
import it.fai.ms.efservice.service.fsm.enumeration.FsmCommand;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class })
public class FsmModificaAssistenzaVeicoliVarTargaConfigTest {

  @Autowired
  private FsmFactory fsmfactory;
  
  @Autowired
  private FsmRichiestaCacheService fsmRichiestaCache;

  private Richiesta r;

  @Before
  public void setUp() throws Exception {
    
    fsmRichiestaCache.clearCache();
    
    r = new Richiesta().identificativo(System.currentTimeMillis() + "R")
                       .tipo(TipoRichiesta.VARIAZIONE_TARGA)
                       .stato(StatoRichiesta.ATTIVO_PER_MODIFICA_ASSISTENZA_VEICOLI)
                       .tipoDispositivo(new TipoDispositivo().nome(TipoDispositivoEnum.ASSISTENZA_VEICOLI));
    
    long idRichiesta = (long) (Math.random() * 100000);
    r.setId(idRichiesta);
  }

  @Test
  public void testVariazioneTarga() {
    AbstractFsmRichiesta fsm = fsmfactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_ASSISTENZA_VEICOLI);
    r = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, r);
    assertThat(r).isNotNull()
                 .extracting(r -> tuple(r.getStato()))
                 .containsExactly(tuple(StatoRichiesta.EVASO));
  }
  
  @Test
  public void testDisattivazione() {
    r.tipo(TipoRichiesta.DISATTIVAZIONE);
    AbstractFsmRichiesta fsm = fsmfactory.getFsm(FsmCommand.CMD_VARIAZIONE_TARGA_ASSISTENZA_VEICOLI);
    r = fsm.executeCommandToChangeState(RichiestaEvent.INITIAL, r);
    assertThat(r).isNotNull()
                 .extracting(r -> tuple(r.getStato()))
                 .containsExactly(tuple(StatoRichiesta.EVASO));
  }

}
