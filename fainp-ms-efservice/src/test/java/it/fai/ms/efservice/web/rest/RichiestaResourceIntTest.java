package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.repository.RichiestaRepository;
import it.fai.ms.efservice.service.RichiestaService;
import it.fai.ms.efservice.service.dto.RichiestaDTO;
import it.fai.ms.efservice.service.mapper.RichiestaMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;

/**
 * Test class for the RichiestaResource REST controller.
 *
 * @see RichiestaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class RichiestaResourceIntTest {

  private static final Instant DEFAULT_DATA = Instant.ofEpochMilli(0L);
  private static final Instant UPDATED_DATA = Instant.now()
                                                     .truncatedTo(ChronoUnit.MILLIS);

  private static final TipoRichiesta DEFAULT_TIPO = TipoRichiesta.NUOVO_DISPOSITIVO;
  private static final TipoRichiesta UPDATED_TIPO = TipoRichiesta.NUOVO_ORDINE;

  private static final StatoRichiesta DEFAULT_STATO = StatoRichiesta.IN_ATTESA_DI_LAVORAZIONE;
  private static final StatoRichiesta UPDATED_STATO = StatoRichiesta.RICHIESTA_ANNULLATA_DA_UTENTE;

  private static final String DEFAULT_ULTIMA_NOTA = "AAAAAAAAAA";
  private static final String UPDATED_ULTIMA_NOTA = "BBBBBBBBBB";

  private static final String DEFAULT_UUID_DOCUMENTO = "AAAAAAAAAA";
  private static final String UPDATED_UUID_DOCUMENTO = "BBBBBBBBBB";

  private static final String DEFAULT_ANOMALIA = "AAAAAAAAAA";
  private static final String UPDATED_ANOMALIA = "BBBBBBBBBB";

  private static final String DEFAULT_MOTIVO_SOSPENSIONE = "AAAAAAAAAA";
  private static final String UPDATED_MOTIVO_SOSPENSIONE = "BBBBBBBBBB";

  private static final String DEFAULT_ASSOCIAZIONE = "AAAAAAAAAA";
  private static final String UPDATED_ASSOCIAZIONE = "BBBBBBBBBB";

  private static final String DEFAULT_OPERAZIONI_POSSIBILI = "AAAAAAAAAA";
  private static final String UPDATED_OPERAZIONI_POSSIBILI = "BBBBBBBBBB";

  private static final Instant DEFAULT_DATA_MODIFICA_STATO = Instant.ofEpochMilli(0L);
  private static final Instant UPDATED_DATA_MODIFICA_STATO = Instant.now()
                                                                    .truncatedTo(ChronoUnit.MILLIS);

  private static final String DEFAULT_NOME_SERVIZIO = "AAAAAAAAAA";
  private static final String UPDATED_NOME_SERVIZIO = "BBBBBBBBBB";

  @Autowired
  private RichiestaRepository richiestaRepository;

  @Autowired
  private RichiestaMapper richiestaMapper;

  @Autowired
  private RichiestaService richiestaService;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  private EntityManager em;

  private MockMvc restRichiestaMockMvc;

  private Richiesta richiesta;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    final RichiestaResource richiestaResource = new RichiestaResource(richiestaService);
    this.restRichiestaMockMvc = MockMvcBuilders.standaloneSetup(richiestaResource)
                                               .setCustomArgumentResolvers(pageableArgumentResolver)
                                               .setControllerAdvice(exceptionTranslator)
                                               .setMessageConverters(jacksonMessageConverter)
                                               .build();
  }

  /**
   * Create an entity for this test. This is a static method, as tests for other entities might also need it, if they
   * test an entity which requires the current entity.
   */
  public static Richiesta createEntity(EntityManager em) {
    Richiesta richiesta = new Richiesta().data(DEFAULT_DATA)
                                         .tipo(DEFAULT_TIPO)
                                         .stato(DEFAULT_STATO)
                                         .ultimaNota(DEFAULT_ULTIMA_NOTA)
                                         .uuidDocumento(DEFAULT_UUID_DOCUMENTO)
                                         .anomalia(DEFAULT_ANOMALIA)
                                         .motivoSospensione(DEFAULT_MOTIVO_SOSPENSIONE)
                                         .associazione(DEFAULT_ASSOCIAZIONE)
                                         .operazioniPossibili(DEFAULT_OPERAZIONI_POSSIBILI)
                                         .dataModificaStato(DEFAULT_DATA_MODIFICA_STATO)
                                         .nomeServizio(DEFAULT_NOME_SERVIZIO);
    return richiesta;
  }

  @Before
  public void initTest() {
    richiesta = createEntity(em);
  }

  @Test
  @Transactional
  public void createRichiesta() throws Exception {
    int databaseSizeBeforeCreate = richiestaRepository.findAll()
                                                      .size();

    // Create the Richiesta
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);
    restRichiestaMockMvc.perform(post("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                        .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isCreated());

    // Validate the Richiesta in the database
    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeCreate + 1);
    Richiesta testRichiesta = richiestaList.get(richiestaList.size() - 1);
    assertThat(testRichiesta.getData()).isEqualTo(DEFAULT_DATA);
    assertThat(testRichiesta.getTipo()).isEqualTo(DEFAULT_TIPO);
    assertThat(testRichiesta.getStato()).isEqualTo(DEFAULT_STATO);
    assertThat(testRichiesta.getUltimaNota()).isEqualTo(DEFAULT_ULTIMA_NOTA);
    assertThat(testRichiesta.getUuidDocumento()).isEqualTo(DEFAULT_UUID_DOCUMENTO);
    assertThat(testRichiesta.getAnomalia()).isEqualTo(DEFAULT_ANOMALIA);
    assertThat(testRichiesta.getMotivoSospensione()).isEqualTo(DEFAULT_MOTIVO_SOSPENSIONE);
    assertThat(testRichiesta.getAssociazione()).isEqualTo(DEFAULT_ASSOCIAZIONE);
    assertThat(testRichiesta.getOperazioniPossibili()).isEqualTo(DEFAULT_OPERAZIONI_POSSIBILI);
    assertThat(testRichiesta.getDataModificaStato()).isEqualTo(DEFAULT_DATA_MODIFICA_STATO);
    assertThat(testRichiesta.getNomeServizio()).isEqualTo(DEFAULT_NOME_SERVIZIO);
  }

  @Test
  @Transactional
  public void createRichiestaWithExistingId() throws Exception {
    int databaseSizeBeforeCreate = richiestaRepository.findAll()
                                                      .size();

    // Create the Richiesta with an existing ID
    richiesta.setId(1L);
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);

    // An entity with an existing ID cannot be created, so this API call must fail
    restRichiestaMockMvc.perform(post("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                        .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isBadRequest());

    // Validate the Richiesta in the database
    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeCreate);
  }

  @Test
  @Transactional
  public void checkDataIsRequired() throws Exception {
    int databaseSizeBeforeTest = richiestaRepository.findAll()
                                                    .size();
    // set the field null
    richiesta.setData(null);

    // Create the Richiesta, which fails.
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);

    restRichiestaMockMvc.perform(post("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                        .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isBadRequest());

    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkTipoIsRequired() throws Exception {
    int databaseSizeBeforeTest = richiestaRepository.findAll()
                                                    .size();
    // set the field null
    richiesta.setTipo(null);

    // Create the Richiesta, which fails.
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);

    restRichiestaMockMvc.perform(post("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                        .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isBadRequest());

    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkStatoIsRequired() throws Exception {
    int databaseSizeBeforeTest = richiestaRepository.findAll()
                                                    .size();
    // set the field null
    richiesta.setStato(null);

    // Create the Richiesta, which fails.
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);

    restRichiestaMockMvc.perform(post("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                        .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isBadRequest());

    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllRichiestas() throws Exception {
    // Initialize the database
    richiestaRepository.saveAndFlush(richiesta);

    // Get all the richiestaList
    restRichiestaMockMvc.perform(get("/api/richiestas?sort=id,desc"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(jsonPath("$.[*].id").value(hasItem(richiesta.getId()
                                                                               .intValue())))
                        .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
                        .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
                        .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
                        .andExpect(jsonPath("$.[*].ultimaNota").value(hasItem(DEFAULT_ULTIMA_NOTA.toString())))
                        .andExpect(jsonPath("$.[*].uuidDocumento").value(hasItem(DEFAULT_UUID_DOCUMENTO.toString())))
                        .andExpect(jsonPath("$.[*].anomalia").value(hasItem(DEFAULT_ANOMALIA.toString())))
                        .andExpect(jsonPath("$.[*].motivoSospensione").value(hasItem(DEFAULT_MOTIVO_SOSPENSIONE.toString())))
                        .andExpect(jsonPath("$.[*].associazione").value(hasItem(DEFAULT_ASSOCIAZIONE.toString())))
                        .andExpect(jsonPath("$.[*].operazioniPossibili").value(hasItem(DEFAULT_OPERAZIONI_POSSIBILI.toString())))
                        .andExpect(jsonPath("$.[*].dataModificaStato").value(hasItem(DEFAULT_DATA_MODIFICA_STATO.toString())))
                        .andExpect(jsonPath("$.[*].nomeServizio").value(hasItem(DEFAULT_NOME_SERVIZIO.toString())));
  }

  @Test
  @Transactional
  public void getRichiesta() throws Exception {
    // Initialize the database
    richiestaRepository.saveAndFlush(richiesta);

    // Get the richiesta
    restRichiestaMockMvc.perform(get("/api/richiestas/{id}", richiesta.getId()))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(jsonPath("$.id").value(richiesta.getId()
                                                                   .intValue()))
                        .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
                        .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
                        .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
                        .andExpect(jsonPath("$.ultimaNota").value(DEFAULT_ULTIMA_NOTA.toString()))
                        .andExpect(jsonPath("$.uuidDocumento").value(DEFAULT_UUID_DOCUMENTO.toString()))
                        .andExpect(jsonPath("$.anomalia").value(DEFAULT_ANOMALIA.toString()))
                        .andExpect(jsonPath("$.motivoSospensione").value(DEFAULT_MOTIVO_SOSPENSIONE.toString()))
                        .andExpect(jsonPath("$.associazione").value(DEFAULT_ASSOCIAZIONE.toString()))
                        .andExpect(jsonPath("$.operazioniPossibili").value(DEFAULT_OPERAZIONI_POSSIBILI.toString()))
                        .andExpect(jsonPath("$.dataModificaStato").value(DEFAULT_DATA_MODIFICA_STATO.toString()))
                        .andExpect(jsonPath("$.nomeServizio").value(DEFAULT_NOME_SERVIZIO.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingRichiesta() throws Exception {
    // Get the richiesta
    restRichiestaMockMvc.perform(get("/api/richiestas/{id}", Long.MAX_VALUE))
                        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateRichiesta() throws Exception {
    // Initialize the database
    richiestaRepository.saveAndFlush(richiesta);
    int databaseSizeBeforeUpdate = richiestaRepository.findAll()
                                                      .size();

    // Update the richiesta
    Richiesta updatedRichiesta = richiestaRepository.findOne(richiesta.getId());
    updatedRichiesta.data(UPDATED_DATA)
                    .tipo(UPDATED_TIPO)
                    .stato(UPDATED_STATO)
                    .ultimaNota(UPDATED_ULTIMA_NOTA)
                    .uuidDocumento(UPDATED_UUID_DOCUMENTO)
                    .anomalia(UPDATED_ANOMALIA)
                    .motivoSospensione(UPDATED_MOTIVO_SOSPENSIONE)
                    .associazione(UPDATED_ASSOCIAZIONE)
                    .operazioniPossibili(UPDATED_OPERAZIONI_POSSIBILI)
                    .dataModificaStato(UPDATED_DATA_MODIFICA_STATO)
                    .nomeServizio(UPDATED_NOME_SERVIZIO);
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(updatedRichiesta);

    restRichiestaMockMvc.perform(put("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                       .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isOk());

    // Validate the Richiesta in the database
    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeUpdate);
    Richiesta testRichiesta = richiestaList.get(richiestaList.size() - 1);
    assertThat(testRichiesta.getData()).isEqualTo(UPDATED_DATA);
    assertThat(testRichiesta.getTipo()).isEqualTo(UPDATED_TIPO);
    assertThat(testRichiesta.getStato()).isEqualTo(UPDATED_STATO);
    assertThat(testRichiesta.getUltimaNota()).isEqualTo(UPDATED_ULTIMA_NOTA);
    assertThat(testRichiesta.getUuidDocumento()).isEqualTo(UPDATED_UUID_DOCUMENTO);
    assertThat(testRichiesta.getAnomalia()).isEqualTo(UPDATED_ANOMALIA);
    assertThat(testRichiesta.getMotivoSospensione()).isEqualTo(UPDATED_MOTIVO_SOSPENSIONE);
    assertThat(testRichiesta.getAssociazione()).isEqualTo(UPDATED_ASSOCIAZIONE);
    assertThat(testRichiesta.getOperazioniPossibili()).isEqualTo(UPDATED_OPERAZIONI_POSSIBILI);
    assertThat(testRichiesta.getDataModificaStato()).isEqualTo(UPDATED_DATA_MODIFICA_STATO);
    assertThat(testRichiesta.getNomeServizio()).isEqualTo(UPDATED_NOME_SERVIZIO);
  }

  @Test
  @Transactional
  public void updateNonExistingRichiesta() throws Exception {
    int databaseSizeBeforeUpdate = richiestaRepository.findAll()
                                                      .size();

    // Create the Richiesta
    RichiestaDTO richiestaDTO = richiestaMapper.toDto(richiesta);

    // If the entity doesn't have an ID, it will be created instead of just being updated
    restRichiestaMockMvc.perform(put("/api/richiestas").contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                       .content(TestUtil.convertObjectToJsonBytes(richiestaDTO)))
                        .andExpect(status().isCreated());

    // Validate the Richiesta in the database
    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeUpdate + 1);
  }

  @Test
  @Transactional
  public void deleteRichiesta() throws Exception {
    // Initialize the database
    richiestaRepository.saveAndFlush(richiesta);
    int databaseSizeBeforeDelete = richiestaRepository.findAll()
                                                      .size();

    // Get the richiesta
    restRichiestaMockMvc.perform(delete("/api/richiestas/{id}", richiesta.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                        .andExpect(status().isOk());

    // Validate the database is empty
    List<Richiesta> richiestaList = richiestaRepository.findAll();
    assertThat(richiestaList).hasSize(databaseSizeBeforeDelete - 1);
  }

  @Test
  @Transactional
  public void equalsVerifier() throws Exception {
    TestUtil.equalsVerifier(Richiesta.class);
    Richiesta richiesta1 = new Richiesta();
    richiesta1.setId(1L);
    Richiesta richiesta2 = new Richiesta();
    richiesta2.setId(richiesta1.getId());
    assertThat(richiesta1).isEqualTo(richiesta2);
    richiesta2.setId(2L);
    assertThat(richiesta1).isNotEqualTo(richiesta2);
    richiesta1.setId(null);
    assertThat(richiesta1).isNotEqualTo(richiesta2);
  }

  @Test
  @Transactional
  public void dtoEqualsVerifier() throws Exception {
    TestUtil.equalsVerifier(RichiestaDTO.class);
    RichiestaDTO richiestaDTO1 = new RichiestaDTO();
    richiestaDTO1.setId(1L);
    RichiestaDTO richiestaDTO2 = new RichiestaDTO();
    assertThat(richiestaDTO1).isNotEqualTo(richiestaDTO2);
    richiestaDTO2.setId(richiestaDTO1.getId());
    assertThat(richiestaDTO1).isEqualTo(richiestaDTO2);
    richiestaDTO2.setId(2L);
    assertThat(richiestaDTO1).isNotEqualTo(richiestaDTO2);
    richiestaDTO1.setId(null);
    assertThat(richiestaDTO1).isNotEqualTo(richiestaDTO2);
  }

  @Test
  @Transactional
  public void testEntityFromId() {
    assertThat(richiestaMapper.fromId(42L)
                              .getId()).isEqualTo(42);
    assertThat(richiestaMapper.fromId(null)).isNull();
  }
}
