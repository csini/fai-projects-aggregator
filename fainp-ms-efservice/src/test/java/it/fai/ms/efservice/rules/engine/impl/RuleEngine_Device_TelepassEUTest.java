package it.fai.ms.efservice.rules.engine.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.mvel2.MVEL;

import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleDieselParticulateFilter;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome.RuleFailure;

public class RuleEngine_Device_TelepassEUTest {

  @Test
  public void testCategoryNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setCategory(null);
    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("category must be present");
  }

  @Test
  public void testCategoryIsSetButNotIsValid() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setCategory("M1");
    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("category must not be N1 or M1");
  }

  @Test
  public void testCategoryIsSetAxesEqualsZero() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setCategory("N2");
    ruleContextVehicle.setAxes(0);
    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("axes must be > 0 and < 5");
  }

  @Test
  @Ignore
  public void testWhenLoneVehicleGrossWeightIsLessThen3500AndLoneVehicleTareWeightIsLessThen300() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(555);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(777);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(4500);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("weight3500");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("weight is less then 3500");
  }

  @Test
  public void testMvel() {
    final Map<String, Object> context = new HashMap<>();
    RuleEngineVehicle vehicle = new RuleEngineVehicle(new RuleEngineVehicleId("::testVehicle::"));
    Float num = null;
    vehicle.setHeight(num);
    context.put("vehicle", vehicle);

    String str = "boolean b1 = (vehicle.height != null && vehicle.height > 3f);";
    List<String> strList = new LinkedList<>();
    strList.add(str);
    str = "if (!b1) return 'pippo';";
    strList.add(str);
    str = "return '';";
    strList.add(str);

    String expression = String.join("", strList);
    String eval = MVEL.eval(expression, context, String.class);
    boolean isEmpty = eval.equals("");
    assertThat(isEmpty).isFalse();
  }

  @Test
  public void testHeightIsNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(3502);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3502);
    ruleContextVehicle.setHeight(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Height must be set");
  }

  @Test
  public void testHeightLessOrEqualThan3mt() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(3502);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3502);
    ruleContextVehicle.setHeight(null);
    Float num = 3.00f;
    ruleContextVehicle.setHeight(num);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("height must be > 3 mt");
  }

  @Test
  public void testHeightGreaterThan3mt() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(20000);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(32000);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(150000);
    ruleContextVehicle.setHeight(3.01f);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void testTareNotSet() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Tara weight must be set");
  }

  @Test
  public void testTareLessEqual3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3500);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Tara weight must be greater than 3500 kg");
  }

  @Test
  public void testTareMaxValue() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(20001);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Max value of tara weight is 20000 kg");
  }

  @Test
  public void testGrossNotSet() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Gross weight must be set");
  }

  @Test
  public void testGrossLessEqual3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(3500);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Gross weight must be greater than 3500 kg");
  }

  @Test
  public void testGrossLsEqTare() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(19000);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(19000);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Gross weight must be greater than tara weight");
  }

  @Test
  public void testGrossMaxValue() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(19999);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(32001);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Max value of gross weight is 32000 kg");
  }

  @Test
  public void testTrainsetNotSet() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(3600);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Trainset weight must be set");
  }

  @Test
  public void testTrainsetLessEqual3500() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3501);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(3600);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3500);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Trainset weight must be greater than 3500 kg");
  }

  @Test
  public void testTrainsetLsGross() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(19000);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(31999);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(31998);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Trainset weight must be greater equal than gross weight");
  }

  @Test
  public void testTrainsetMaxValue() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.setHeight(3.01f);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(19000);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(31999);
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(150001);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Max value of trainset weight is 150000 kg");
  }

  @Test
  public void testWithAxesCategoryAndLoneVehicleTareWeight() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setCountry("::italia::");
    ruleContextVehicle.setLicensePlate("::licensePlate");
    ruleContextVehicle.setEuroClass("EURO2");
    ruleContextVehicle.setMake("::DigitalMill::");
    ruleContextVehicle.setType("::TRATTORE::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleTareWeight(3500);
    ruleContextVehicle.setHeight(3.1f);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("Tara weight must be greater than 3500 kg");
  }

  @Test
  public void testWithAxesNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setAxes(null);
    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    RuleFailure ruleFailure = ruleOutcome.getFailure()
                                         .get();
    String mess = ruleFailure.getMess();
    assertThat(mess).isEqualTo("axes must be present");
  }

  @Test
  public void whenCategoryIsM1() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("M1");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(9999);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("categoryN1M1");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("category must not be N1 or M1");
  }

  @Test
  public void whenCategoryIsN1() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("N1");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(9999);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("categoryN1M1");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("category must not be N1 or M1");
  }

  @Test
  public void whenThereAreAxesIsNull() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(null);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setEuroClass("EURO1");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(9999);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("axesnotset");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("axes must be present");
  }

  @Test
  public void whenThereAreNoaxes() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(0);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(9999);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("axesvalue");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("axes must be > 0 and < 5");
  }

  @Test
  public void whenThereIsNoCategory() {
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory(null);
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(9999);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               newRuleContextServiceType());
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("category");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("category must be present");
  }

  // Service AREA C
  @Test
  public void whenServiceIsAreaCAndLengthIsNotSet() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.setEuroClass("EURO1");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("typelength");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Vehicle type not AUTOBUS length must be <= 7,5mt");
  }

  @Test
  public void whenServiceIsAreaCAndFuelIsDieselAndEuroClassLessThan4() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("DIESEL");
    ruleContextVehicle.setEuroClass("EURO3");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("dieseleuro");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Vehicle diesel euro must be > 3");
  }

  @Test
  public void whenServiceIsAreaCAndFuelIsDieselAndEuroClass4AndNotHasParicularFilterInstalled() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("DIESEL");
    ruleContextVehicle.setEuroClass("EURO4");
    ruleContextVehicle.setDieselParticulateFilter(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("dieselfap");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Vehicle diesel euro 4 must have FAP");
  }

  @Test
  public void whenServiceIsAreaCAndFuelIsDieselAndEuroClass4AndParicularFilterInstalled() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("DIESEL");
    ruleContextVehicle.setEuroClass("EURO4");
    ruleContextVehicle.setDieselParticulateFilter(new RuleEngineVehicleDieselParticulateFilter(1));

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void whenServiceIsAreaCAndFuelIsBenzinaAndEuroClassLessThan1() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO0");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();

    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("benzina");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Vehicle benzina euro must be >= 1");
  }

  @Test
  public void whenServiceIsGermaniaAndGrossNotSet() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("PEDAGGI_GERMANIA"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(null);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("grossnotset");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Gross weight must be set");
  }

  @Test
  public void whenServiceIsGermaniaAndGrossSetButLessOrEqual7500() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("PEDAGGI_GERMANIA"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(7500);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("gross7500");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Gross weight must be greater than 7500 kg");
  }

  @Test
  public void whenServiceIsGermaniaAndNumberChassisNotSet() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("PEDAGGI_GERMANIA"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(7501);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");
    ruleContextVehicle.setNumberChassis(null);

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();
    assertThat(ruleOutcome.getOutcome()).isFalse();
    assertThat(ruleOutcome.getFailure()).isPresent();
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getCode()).isEqualTo("numberChassis");
    assertThat(ruleOutcome.getFailure()
                          .get()
                          .getMess()).isEqualTo("Vehicle must be set Number Chassis");
  }

  @Test
  public void whenServiceIsGermaniaIsOk() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("PEDAGGI_GERMANIA"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setLoneVehicleGrossWeight(7501);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");
    ruleContextVehicle.setNumberChassis("::number-chassis::");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  @Test
  public void whenServiceIsAreaCAndFuelIsBenzinaAndEuroClassGreaterThan0() {
    RuleEngineServiceType ruleEngineServiceType = new RuleEngineServiceType(new RuleEngineServiceTypeId("AREA_C"));
    RuleEngineVehicle ruleContextVehicle = newRuleContextVehicle();
    ruleContextVehicle.setAxes(2);
    ruleContextVehicle.setCategory("::category::");
    ruleContextVehicle.getWeight()
                      .setTrainsetGrossWeight(3555);
    ruleContextVehicle.setHeight(4f);
    ruleContextVehicle.setLength(7.5f);
    ruleContextVehicle.setFuelType("BENZINA");
    ruleContextVehicle.setEuroClass("EURO1");

    RuleEngine_Device_TelepassEU ruleEngine = new RuleEngine_Device_TelepassEU(newRuleContextDeviceType(), ruleContextVehicle,
                                                                               ruleEngineServiceType);
    RuleOutcome ruleOutcome = ruleEngine.executeRule();
    assertThat(ruleOutcome.getOutcome()).isTrue();
  }

  private RuleEngineDeviceType newRuleContextDeviceType() {
    return new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::"));
  }

  private RuleEngineServiceType newRuleContextServiceType() {
    return new RuleEngineServiceType(new RuleEngineServiceTypeId("::serviceTypeId::"));
  }

  private RuleEngineVehicle newRuleContextVehicle() {
    return new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::"));
  }

}
