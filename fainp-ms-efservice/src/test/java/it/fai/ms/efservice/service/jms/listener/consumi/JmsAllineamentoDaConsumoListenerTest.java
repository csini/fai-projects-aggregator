package it.fai.ms.efservice.service.jms.listener.consumi;

import static java.util.stream.Collectors.toSet;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.DatiAggiuntiviRichiesta;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.enumeration.KeyDatiAggiuntivi;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.service.fsm.FsmRichiestaUtils;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.exception.FsmExecuteCommandException;

@RunWith(MockitoJUnitRunner.class)
public class JmsAllineamentoDaConsumoListenerTest {

  private JmsAllineamentoDaConsumoListener listener;

  private static final String ANOMALY_CONSUMO_ALLINEAMENTO = "CONS_ALL001";

  private static final String EXPECTED_LICENCE_NUMBER = "FAI1234";

  private static final String EXPECTED_LICENCE_COUNTRY = "IT";

  private static final String EXPECTED_LICENCE_NUMBER_HGV        = "B107NTU";
  private static final String EXPECTED_COD_CONTRATTO_CLIENTE_HGV = "00046348";

  private static final TipoDispositivoEnum EXPECTED_TIPO_DISPOSITIVO     = TipoDispositivoEnum.TOLL_COLLECT;
  private static final TipoDispositivoEnum EXPECTED_TIPO_DISPOSITIVO_HGV = TipoDispositivoEnum.HGV;

  @Mock
  private FsmRichiestaUtils fsmRichiestaUtil;

  @Mock
  private NotificationService notificationService;

  private SimpleDateFormat sdf;

  @Before
  public void init() {
    sdf = new SimpleDateFormat("ddMMyyyy_HHmmssSSS");
    listener = new JmsAllineamentoDaConsumoListener(fsmRichiestaUtil, notificationService);
  }

  @Test
  public void receiveAllineamentoDaConsumo() throws FsmExecuteCommandException {
    AllineamentoDaConsumoMessage allineamentoDaConsumo = new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().deviceType(EXPECTED_TIPO_DISPOSITIVO)
                                                                                                                        .licensePlateNation(EXPECTED_LICENCE_COUNTRY)
                                                                                                                        .licensePlateNumber(EXPECTED_LICENCE_NUMBER)
                                                                                                                        .build();

    Richiesta expectedRichiesta = new Richiesta();
    expectedRichiesta.setIdentificativo(sdf.format(new Date()));
    List<Richiesta> list = new ArrayList<>();
    list.add(expectedRichiesta);
    when(fsmRichiestaUtil.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(EXPECTED_LICENCE_NUMBER, EXPECTED_LICENCE_COUNTRY,
                                                                                      EXPECTED_TIPO_DISPOSITIVO,
                                                                                      StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)).thenReturn(list);

    listener.consumeMessage(allineamentoDaConsumo);
    verify(fsmRichiestaUtil).changeStatusRichieste(list.stream()
                                                       .map(r -> r.getIdentificativo())
                                                       .collect(toSet()),
                                                   RichiestaEvent.MU_ATTIVO.name(), "", "");
    verifyZeroInteractions(notificationService);
  }

  @Test
  public void receiveAllineamentoDaConsumo_richiesta_not_found() {
    AllineamentoDaConsumoMessage allineamentoDaConsumo = new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().deviceType(EXPECTED_TIPO_DISPOSITIVO)
                                                                                                                        .licensePlateNation(EXPECTED_LICENCE_COUNTRY)
                                                                                                                        .licensePlateNumber(EXPECTED_LICENCE_NUMBER)
                                                                                                                        .build();

    when(fsmRichiestaUtil.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(EXPECTED_LICENCE_NUMBER, EXPECTED_LICENCE_COUNTRY,
                                                                                      EXPECTED_TIPO_DISPOSITIVO,
                                                                                      StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO)).thenReturn(new ArrayList<>());

    listener.consumeMessage(allineamentoDaConsumo);

    Map<String, Object> parameters = createParamterToSendAnomaly(allineamentoDaConsumo, StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);

    verify(fsmRichiestaUtil,
           times(1)).findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(EXPECTED_LICENCE_NUMBER, EXPECTED_LICENCE_COUNTRY,
                                                                                 EXPECTED_TIPO_DISPOSITIVO,
                                                                                 StatoRichiesta.IN_ATTESA_ALLINEAMENTO_DA_CONSUMO);
    verifyNoMoreInteractions(fsmRichiestaUtil);
    verify(notificationService).notify("CONS_ALL001", parameters);
  }

  private Map<String, Object> createParamterToSendAnomaly(AllineamentoDaConsumoMessage allineamentoDaConsumo, StatoRichiesta stato) {
    Map<String, Object> parameters = new HashMap<>();

    StringBuilder sb = new StringBuilder("No request found for [");
    sb.append("LicensePlate: ")
      .append(allineamentoDaConsumo.getLicensePlateNumber());
    sb.append(" - Country_LicensePlate: ")
      .append(allineamentoDaConsumo.getLicensePlateNation());
    sb.append(" - Device type: ")
      .append(allineamentoDaConsumo.getDeviceType());
    sb.append(" - Request status: ")
      .append(stato);

    Optional<String> contractCode = allineamentoDaConsumo.getContractCode();
    if (contractCode.isPresent()) {
      sb.append(" - ContractCode: ")
        .append(contractCode.get());
    }
    Optional<String> startDate = allineamentoDaConsumo.getStartDate();
    if (startDate.isPresent()) {
      sb.append(" - Start Date: ")
        .append(startDate.get());
    }
    Optional<String> endDate = allineamentoDaConsumo.getEndDate();
    if (endDate.isPresent()) {
      sb.append(" - End Date: ")
        .append(endDate.get());
    }

    sb.append("]");

    parameters.put("message", sb.toString());
    return parameters;
  }

  @Test
  public void receiveAllineamentoDaConsumoHGV() throws FsmExecuteCommandException {
    Date dateDa = new Date();
    Date dateA = new Date();
    AllineamentoDaConsumoMessage allineamentoDaConsumo = new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().contractCode(EXPECTED_COD_CONTRATTO_CLIENTE_HGV)
                                                                                                                        .deviceType(EXPECTED_TIPO_DISPOSITIVO_HGV)
                                                                                                                        .licensePlateNation(EXPECTED_LICENCE_COUNTRY)
                                                                                                                        .licensePlateNumber(EXPECTED_LICENCE_NUMBER_HGV)
                                                                                                                        .customerCode(EXPECTED_COD_CONTRATTO_CLIENTE_HGV)
                                                                                                                        .startDate(sdf.format(dateDa))
                                                                                                                        .endDate(sdf.format(dateA))
                                                                                                                        .build();
    List<Richiesta> listaRichieste = new ArrayList<Richiesta>();
    List<DatiAggiuntiviRichiesta> datiAggiuntivi = createDatiAggiuntivi(sdf.format(dateDa), sdf.format(dateA));
    Richiesta expectedRichiesta = createRichiesta(EXPECTED_TIPO_DISPOSITIVO_HGV, EXPECTED_COD_CONTRATTO_CLIENTE_HGV);
    expectedRichiesta.setDatiAggiuntivi(datiAggiuntivi);
    listaRichieste.add(expectedRichiesta);
    Richiesta richiesta = createRichiesta(EXPECTED_TIPO_DISPOSITIVO_HGV, EXPECTED_COD_CONTRATTO_CLIENTE_HGV);
    listaRichieste.add(richiesta);

    when(fsmRichiestaUtil.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(allineamentoDaConsumo.getLicensePlateNumber(),
                                                                                      allineamentoDaConsumo.getLicensePlateNation(),
                                                                                      allineamentoDaConsumo.getDeviceType(),
                                                                                      StatoRichiesta.INOLTRATO)).thenReturn(listaRichieste);
    listener.consumeMessage(allineamentoDaConsumo);
    Set<String> identifierRequests = new HashSet<>();
    identifierRequests.add(expectedRichiesta.getIdentificativo());
    verify(fsmRichiestaUtil).changeStatusRichieste(identifierRequests, RichiestaEvent.MU_CONSUMO.name(), "", "");
    verifyZeroInteractions(notificationService);
  }

  private Richiesta createRichiesta(TipoDispositivoEnum deviceType, String codContratto) {
    return new Richiesta().tipoDispositivo(new TipoDispositivo().nome(deviceType))
                          .contratto(new Contratto().codContrattoCliente(codContratto))
                          .identificativo(sdf.format(new Date()));
  }

  private List<DatiAggiuntiviRichiesta> createDatiAggiuntivi(String dataDa, String dataA) {
    List<DatiAggiuntiviRichiesta> dars = new ArrayList<>();
    DatiAggiuntiviRichiesta dar = new DatiAggiuntiviRichiesta();
    dar.setChiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_DA);
    dar.setValore(dataDa);
    dars.add(dar);

    dar = new DatiAggiuntiviRichiesta();
    dar.setChiave(KeyDatiAggiuntivi.DATA_PRENOTAZIONE_A);
    dar.setValore(dataA);
    dars.add(dar);
    return dars;
  }

  @Test
  public void receiveAllineamentoDaConsumoHGV_richiesta_not_found() {
    AllineamentoDaConsumoMessage allineamentoDaConsumo = new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().contractCode(EXPECTED_COD_CONTRATTO_CLIENTE_HGV)
                                                                                                                        .deviceType(EXPECTED_TIPO_DISPOSITIVO_HGV)
                                                                                                                        .licensePlateNation(EXPECTED_LICENCE_COUNTRY)
                                                                                                                        .licensePlateNumber(EXPECTED_LICENCE_NUMBER_HGV)
                                                                                                                        .customerCode(EXPECTED_COD_CONTRATTO_CLIENTE_HGV)
                                                                                                                        .startDate(new Date().toString())
                                                                                                                        .endDate(new Date().toString())
                                                                                                                        .build();

    List<Richiesta> listaRichieste = new ArrayList<Richiesta>();
    when(fsmRichiestaUtil.findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(allineamentoDaConsumo.getLicensePlateNumber(),
                                                                                      allineamentoDaConsumo.getLicensePlateNation(),
                                                                                      allineamentoDaConsumo.getDeviceType(),
                                                                                      StatoRichiesta.INOLTRATO)).thenReturn(listaRichieste);
    listener.consumeMessage(allineamentoDaConsumo);

    Map<String, Object> parameters = createParamterToSendAnomaly(allineamentoDaConsumo, StatoRichiesta.INOLTRATO);

    verify(fsmRichiestaUtil,
           times(1)).findByAssociazioneAndCountryAndTipoDispositivo_NomeAndStato(allineamentoDaConsumo.getLicensePlateNumber(),
                                                                                 allineamentoDaConsumo.getLicensePlateNation(),
                                                                                 allineamentoDaConsumo.getDeviceType(),
                                                                                 StatoRichiesta.INOLTRATO);
    verifyNoMoreInteractions(fsmRichiestaUtil);
    verify(notificationService).notify(ANOMALY_CONSUMO_ALLINEAMENTO, parameters);
  }

}
