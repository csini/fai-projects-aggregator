/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.type.FsmDispositivo;

/**
 * @author Luca Vassallo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FsmDispositivoNotFoundInCacheTest {

  @Autowired
  private FsmDispositivo fsmDispositivo;

  private Dispositivo dispositivo;

  // private TestContextManager manager;

  @Before // Set-up
  public void setUp() throws Exception {
    // this.manager = new TestContextManager(getClass());
    // this.manager.prepareTestInstance(this);

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    dispositivo = new Dispositivo();
    dispositivo.setStato(StatoDispositivo.ACCETTATO_DA_FORNITORE);
    Random r = new Random();
    long number = (long) (r.nextDouble() * Integer.MAX_VALUE);
    dispositivo.setId(number);

    // cacheService.clearCache();
  }

  /**
   * @throws Exception
   */
  @Test
  public void Fsm_001_DispositivoNotFoundInCacheTest() throws Exception {
    assertCambioStatoDispositivo(DispositivoEvent.IN_SPEDIZIONE, StatoDispositivo.ATTIVO_IN_ATTESA_SPEDIZIONE);
    assertCambioStatoDispositivo(DispositivoEvent.SPEDITO_A_FAI, StatoDispositivo.ATTIVO_SPEDITO_A_FAI);
    assertCambioStatoDispositivo(DispositivoEvent.SPEDITO_DA_FAI, StatoDispositivo.ATTIVO_SPEDITO_DA_FAI);
    assertCambioStatoDispositivo(DispositivoEvent.BLOCCO_FURTO, StatoDispositivo.BLOCCATO_FURTO);
  }

  @Test
  public void Fsm_002_DispositivoNotFoundInCacheTest() throws Exception {
    dispositivo.setStato(StatoDispositivo.ATTIVO);
    assertCambioStatoDispositivo(DispositivoEvent.RIENTRO, StatoDispositivo.RIENTRATO_DA_RESTITUIRE_AL_FORNITORE);
    assertCambioStatoDispositivo(DispositivoEvent.RIENTRO, StatoDispositivo.RIENTRATO_RESTITUITO_AL_FORNITORE);
  }

  private void assertCambioStatoDispositivo(DispositivoEvent event, StatoDispositivo statoAtteso) throws Exception {
    fsmDispositivo.executeCommandToChangeState(event, dispositivo);
    StatoDispositivo stato = dispositivo.getStato();
    assertThat(stato).isEqualTo(statoAtteso);
  }

}