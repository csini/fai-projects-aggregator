package it.fai.ms.efservice.service;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;

public class ServiziAttivi {

  private final String italia     = "PEDAGGI_ITALIA";
  private final String austria    = "PEDAGGI_AUSTRIA";
  private final String svizzera   = "PEDAGGI_SVIZZERA";
  private final String parking    = "PARCHEGGI";
  private final String fuel       = "CARBURANTI";
  private final String portogallo = "PEDAGGI_PORTOGALLO";

  private List<StatoDispositivoServizio> sdss;

  @Before
  public void setUp() throws Exception {
    sdss = new ArrayList<>();
    sdss.add(newStatoDispositivoServizio(austria, StatoDS.ATTIVO));
    sdss.add(newStatoDispositivoServizio(italia, StatoDS.ATTIVO));
    sdss.add(newStatoDispositivoServizio(parking, StatoDS.NON_ATTIVO));
    sdss.add(newStatoDispositivoServizio(fuel, StatoDS.ATTIVO));
    sdss.add(newStatoDispositivoServizio(svizzera, StatoDS.NON_ATTIVO));
    sdss.add(newStatoDispositivoServizio(portogallo, StatoDS.ATTIVO));
  }

  @Test
  public void test() {
    Dispositivo dispositivo = newDispositivo(TipoDispositivoEnum.TRACKYCARD);
    Set<TipoServizio> serviceFiltered = filterServiziAttivi(sdss, dispositivo);
    assertThat(serviceFiltered).isNotNull();
    assertThat(serviceFiltered).isNotEmpty();
    assertThat(serviceFiltered).hasSize(3);

    dispositivo = newDispositivo(TipoDispositivoEnum.TELEPASS_EUROPEO);
    serviceFiltered = filterServiziAttivi(sdss, dispositivo);
    assertThat(serviceFiltered).isNotNull();
    assertThat(serviceFiltered).isNotEmpty();
    assertThat(serviceFiltered).hasSize(4);
  }

  private Dispositivo newDispositivo(TipoDispositivoEnum deviceType) {
    Dispositivo device = new Dispositivo();
    device.setSeriale("::serialNumber::");
    device.setStato(StatoDispositivo.ATTIVO);
    device.setTipoDispositivo(new TipoDispositivo().nome(deviceType));
    return device;
  }

  private StatoDispositivoServizio newStatoDispositivoServizio(String nomeServizio, StatoDS stato) {
    StatoDispositivoServizio sds = new StatoDispositivoServizio();
    sds.setTipoServizio(newTipoServizio(nomeServizio));
    sds.setStato(stato);
    return sds;
  }

  private TipoServizio newTipoServizio(String nomeServizio) {
    TipoServizio service = new TipoServizio();
    service.setNome(nomeServizio);
    return service;
  }

  private Set<TipoServizio> filterServiziAttivi(List<StatoDispositivoServizio> sdss, Dispositivo dispositivo) {
    return sdss.parallelStream()
               .filter(sds -> sds.getStato()
                                 .name() == StatoDS.ATTIVO.name())
               .filter(sds -> !isDeviceTrackyCard(dispositivo) || (isDeviceTrackyCard(dispositivo) && sds.getTipoServizio() != null
                                                                   && !sds.getTipoServizio()
                                                                          .getNome()
                                                                          .equalsIgnoreCase("PEDAGGI_AUSTRIA")))
               .map(sds -> sds.getTipoServizio())
               .collect(toSet());
  }

  private boolean isDeviceTrackyCard(Dispositivo device) {
    return device.getTipoDispositivo() != null && device.getTipoDispositivo()
                                                        .getNome()
                                                        .equals(TipoDispositivoEnum.TRACKYCARD);
  }

}
