package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.AbstractCommonTest;
import it.fai.ms.efservice.domain.ClienteFai;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Produttore;
import it.fai.ms.efservice.domain.StatoContratto;
import it.fai.ms.efservice.repository.ClienteFaiRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.ProduttoreRepository;
import it.fai.ms.efservice.service.ClienteFaiServiceExt;
import it.fai.ms.efservice.service.ContrattoServiceExt;
import it.fai.ms.efservice.service.ProduttoreServiceExt;
import it.fai.ms.efservice.service.dto.ContrattoDTO;
import it.fai.ms.efservice.service.mapper.ContrattoMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;
import it.fai.ms.efservice.web.rest.vm.ContrattoRequestExt;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class ContrattoResourceExtTest extends AbstractCommonTest {

  private ContrattoServiceExt  contrattoService  = mock(ContrattoServiceExt.class);
  private ClienteFaiServiceExt clienteFaiService = mock(ClienteFaiServiceExt.class);
  private ContrattoMapper      contrattoMapper   = mock(ContrattoMapper.class);
  private ProduttoreServiceExt produttoreService = mock(ProduttoreServiceExt.class);

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  private MockMvc restContrattoMockMvc;

  private ContrattoResourceExt contrattoResourceExt;

  private String codiceCliente = null;

  private TipoDispositivoEnum deviceType = null;

  UriComponentsBuilder builder;

  @Autowired
  ContrattoRepositoryExt contrattoRepository;

  @Autowired
  ProduttoreRepository produttoreRepository;

  @Autowired
  ClienteFaiRepository clienteFaiRepository;

  String SIMEONI = "0046348";

  @Before
  public void setUp() throws Exception {
    contrattoResourceExt = new ContrattoResourceExt(contrattoService, clienteFaiService, produttoreService, contrattoMapper);

    restContrattoMockMvc = MockMvcBuilders.standaloneSetup(contrattoResourceExt)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter)
        .build();

    codiceCliente = "0012345";
    deviceType = TipoDispositivoEnum.TOLL_COLLECT;
  }

  @Test
  public void updateCodiceContratto_exists() throws Exception {
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = clienteFaiRepository.findOneByCodiceCliente(SIMEONI);
    Produttore produttore = produttoreRepository.findOne(1L);

    Contratto c = new Contratto();
    c.setCodContrattoCliente(RandomString(7));
    c.setClienteFai(clienteFai);
    c.setProduttore(produttore);
    c.setDataModificaStato(Instant.now());
    c.setStato(StatoContratto.ATTIVO);
    c.setPaeseRiferimentoIva(RandomString(2));

    c = contrattoRepository.saveAndFlush(c);
    assertThat(sizeBefore + 1).isEqualTo(contrattoRepository.count());

    ContrattoDTO dto = new ContrattoDTO();
    dto.setCodContrattoCliente(c.getCodContrattoCliente());

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_UPDATE_CONTRACT_CODE + "/" + c.getId());

    restContrattoMockMvc.perform(put(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isBadRequest());

    contrattoRepository.delete(c.getId());
    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
  }

  @Test
  public void updateCodiceContratto_failed() throws Exception {
    ContrattoDTO dto = new ContrattoDTO();

    restContrattoMockMvc.perform(put(ContrattoResourceExt.API_UPDATE_CONTRACT_CODE))
    .andExpect(status().isNotFound());

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_UPDATE_CONTRACT_CODE + "/");
    dto.setCodContrattoCliente(RandomString(7));

    restContrattoMockMvc.perform(put(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isNotFound());

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_UPDATE_CONTRACT_CODE + "/1");
    dto.setCodContrattoCliente("");

    restContrattoMockMvc.perform(put(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void checkContrattoSuccess() throws Exception {
    when(contrattoService.checkContrattoByCodiceClienteAndTipoDispositivo(codiceCliente, deviceType)).thenReturn(true);
    restContrattoMockMvc.perform(get("/api/public/checkcontratto/" + codiceCliente + "/" + deviceType))
    .andExpect(status().isOk());
    verify(contrattoService, times(1)).checkContrattoByCodiceClienteAndTipoDispositivo(codiceCliente, deviceType);
  }

  @Test
  public void checkContrattoNotFound() throws Exception {
    codiceCliente = "::codiceCliente::";
    when(contrattoService.checkContrattoByCodiceClienteAndTipoDispositivo(codiceCliente, deviceType)).thenReturn(false);
    restContrattoMockMvc.perform(get("/api/public/checkcontratto/" + codiceCliente + "/" + deviceType))
    .andExpect(status().isNotFound());
    verify(contrattoService, times(1)).checkContrattoByCodiceClienteAndTipoDispositivo(codiceCliente, deviceType);
  }

  @Test
  public void checkContrattoNotFoundException() throws Exception {
    deviceType = null;
    restContrattoMockMvc.perform(get("/api/public/checkcontratto/" + codiceCliente + "/" + deviceType))
    .andExpect(status().isBadRequest());
    verify(contrattoService, times(0)).checkContrattoByCodiceClienteAndTipoDispositivo(codiceCliente, null);
  }

  @Test
  public void testCreateContrattoFailed_001() throws Exception {
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setId(7L);

    Produttore produttore = new Produttore();
    produttore.setId(1L);
    produttore.setNome("::produttore::");

    ContrattoRequestExt dto = new ContrattoRequestExt();
    dto.setIdProduttore(produttore.getId());
    dto.setCodiceCliente(SIMEONI);
    String codiceContratto = RandomString();
    dto.setCodiceContratto(codiceContratto);
    dto.setDataModificaStato(Instant.now());
    dto.setStato(StatoContratto.ATTIVO);

    when(clienteFaiService.findByCodiceCliente(dto.getCodiceCliente())).thenReturn(clienteFai);
    when(produttoreService.findById(dto.getIdProduttore())).thenReturn(produttore);
    when(contrattoService.findByCodiceContrattoAndNomeProduttore(codiceContratto, produttore.getNome())).thenReturn(Optional.of(new Contratto()));
    
//    when(contrattoService.save(Mockito.any())).thenReturn(newContratto(dto));

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_CREATE_CONTRACT);

    restContrattoMockMvc.perform(post(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().is4xxClientError());

    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
  }
  
  @Test
  public void testCreateContrattoFailed_002() throws Exception {
    List<StatoContratto> statusContracts = Arrays.asList(StatoContratto.ATTIVO, StatoContratto.SOSPESO);
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setId(7L);

    Produttore produttore = new Produttore();
    produttore.setId(1L);
    produttore.setNome("::produttore::");

    ContrattoRequestExt dto = new ContrattoRequestExt();
    dto.setIdProduttore(produttore.getId());
    dto.setCodiceCliente(SIMEONI);
    String codiceContratto = RandomString();
    dto.setCodiceContratto(codiceContratto);
    dto.setDataModificaStato(Instant.now());
    dto.setStato(StatoContratto.ATTIVO);

    when(clienteFaiService.findByCodiceCliente(dto.getCodiceCliente())).thenReturn(clienteFai);
    when(produttoreService.findById(dto.getIdProduttore())).thenReturn(produttore);
    when(contrattoService.findByCodiceContrattoAndNomeProduttore(codiceContratto, produttore.getNome())).thenReturn(Optional.ofNullable(null));
    List<Contratto> contracts = new ArrayList<>();
    contracts.add(new Contratto());
    when(contrattoService.findAllByProduttoreAndCodiceClienteAndStatusIn(produttore.getNome(), SIMEONI, statusContracts)).thenReturn(contracts);
    
//    when(contrattoService.save(Mockito.any())).thenReturn(newContratto(dto));

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_CREATE_CONTRACT);

    restContrattoMockMvc.perform(post(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().is4xxClientError());

    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
  }
  
  @Test
  public void testCreateContrattoSuccess() throws Exception {
    List<StatoContratto> statusContracts = Arrays.asList(StatoContratto.ATTIVO, StatoContratto.SOSPESO);
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setId(7L);

    Produttore produttore = new Produttore();
    produttore.setId(1L);
    produttore.setNome("::produttore::");

    ContrattoRequestExt dto = new ContrattoRequestExt();
    dto.setIdProduttore(produttore.getId());
    dto.setCodiceCliente(SIMEONI);
    String codiceContratto = RandomString();
    dto.setCodiceContratto(codiceContratto);
    dto.setDataModificaStato(Instant.now());
    dto.setStato(StatoContratto.ATTIVO);

    when(clienteFaiService.findByCodiceCliente(dto.getCodiceCliente())).thenReturn(clienteFai);
    when(produttoreService.findById(dto.getIdProduttore())).thenReturn(produttore);
    when(contrattoService.findByCodiceContrattoAndNomeProduttore(codiceContratto, produttore.getNome())).thenReturn(Optional.ofNullable(null));
    when(contrattoService.findAllByProduttoreAndCodiceClienteAndStatusIn(produttore.getNome(), SIMEONI, statusContracts)).thenReturn(null);
    
    when(contrattoService.save(Mockito.any())).thenReturn(newContratto(dto));

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_CREATE_CONTRACT);

    restContrattoMockMvc.perform(post(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                 .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().is2xxSuccessful());

    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
  }

  private Contratto newContratto(ContrattoRequestExt dto) {
    Contratto contract = new Contratto().codContrattoCliente(dto.getCodiceContratto()).dataModificaStato(dto.getDataModificaStato()).stato(dto.getStato());
    contract.setId(System.currentTimeMillis());
    return contract;
  }
  
  
  @Test
  public void testSetContrattoPrimario() throws Exception {
    long sizeBefore = contrattoRepository.count();

    ClienteFai clienteFai = new ClienteFai();
    clienteFai.setId(7L);
    clienteFai.setCodiceCliente(SIMEONI);

    Produttore produttore = new Produttore();
    produttore.setId(1L);
    produttore.setNome("::produttore::");

    ContrattoRequestExt dto = new ContrattoRequestExt();
    dto.setIdProduttore(produttore.getId());
    dto.setCodiceCliente(SIMEONI);
    String codiceContratto = RandomString();
    dto.setCodiceContratto(codiceContratto);
    dto.setDataModificaStato(Instant.now());
    dto.setStato(StatoContratto.ATTIVO);
    
    String identifierContracts = "identifier-001";
    
    Contratto contratto = newContratto(dto);
    contratto.setIdentificativo("identifier-001");
    contratto.setClienteFai(clienteFai);
    contratto.setProduttore(produttore);
    when(contrattoService.findByIdentificativo(identifierContracts)).thenReturn(Optional.of(contratto));
    when(contrattoService.findAllByProduttoreAndCodiceAzienda(produttore, SIMEONI)).thenReturn(null);
    
    when(contrattoService.save(Mockito.any())).thenReturn(newContratto(dto));

    builder = UriComponentsBuilder.fromUriString(ContrattoResourceExt.API_CONTRATTO_PRIMARIO + "?identifier=identifier-001");

    restContrattoMockMvc.perform(post(builder.build().toString())
                                 .contentType(TestUtil.APPLICATION_JSON_UTF8))
    .andExpect(status().is2xxSuccessful());

    assertThat(sizeBefore).isEqualTo(contrattoRepository.count());
    
    verify(contrattoService).save(Mockito.any());
    
  }

}
