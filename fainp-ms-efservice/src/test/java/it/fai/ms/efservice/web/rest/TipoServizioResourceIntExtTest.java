package it.fai.ms.efservice.web.rest;


import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.CategoriaServizio;
import it.fai.ms.efservice.repository.CategoriaServizioRepositoryExt;
import it.fai.ms.efservice.repository.TipoServizioRepositoryExt;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoServizioResourceIntExtTest {


	@Autowired
	private TipoServizioResourceExt tipoServizioResourceExt;

	@Autowired
	private TipoServizioRepositoryExt tipoServizioRepositoryExt;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private CategoriaServizioRepositoryExt categoriaServizioRepositoryExt;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTipoServizioMockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.restTipoServizioMockMvc = MockMvcBuilders.standaloneSetup(tipoServizioResourceExt)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

	@After
	public void clean_all() {
	}

	@Test
	public void getListaCategoriaServizio() throws Exception {
		String codiceAzienda = "0073935";
		String categoriaServizioName = "PEDAGGI";

        CategoriaServizio categoriaServizio = categoriaServizioRepositoryExt.findByNome(categoriaServizioName);

		int tipoServizioSize = tipoServizioRepositoryExt.findByCategoriaServizioOrderByOrdinamento(categoriaServizio).size();

        restTipoServizioMockMvc.perform(get(TipoServizioResourceExt.BASE_PATH + TipoServizioResourceExt.SERVIZI_UTENTE_LIST_PATH))
            .andExpect(status().isBadRequest());

        restTipoServizioMockMvc.perform(get(
            TipoServizioResourceExt.BASE_PATH + TipoServizioResourceExt.SERVIZI_UTENTE_LIST_PATH
            + "?codiceAzienda=" + codiceAzienda
            + "&categoriaServizio=" + categoriaServizioName
        ))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(tipoServizioSize)));
	}

}
