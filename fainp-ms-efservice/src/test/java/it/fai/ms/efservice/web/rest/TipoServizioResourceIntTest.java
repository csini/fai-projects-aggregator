package it.fai.ms.efservice.web.rest;

import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.service.TipoServizioService;
import it.fai.ms.efservice.service.dto.TipoServizioDTO;
import it.fai.ms.efservice.service.mapper.TipoServizioMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoServizioResource REST controller.
 *
 * @see TipoServizioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class TipoServizioResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIZIONE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIZIONE = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_BUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_NOME_BUSINESS = "BBBBBBBBBB";

    private static final String DEFAULT_TESTO_PROMO = "AAAAAAAAAA";
    private static final String UPDATED_TESTO_PROMO = "BBBBBBBBBB";

    private static final Long DEFAULT_ORDINAMENTO = 1L;
    private static final Long UPDATED_ORDINAMENTO = 2L;

    private static final String DEFAULT_DOCUMENTI = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENTI = "BBBBBBBBBB";

    @Autowired
    private TipoServizioRepository tipoServizioRepository;

    @Autowired
    private TipoServizioMapper tipoServizioMapper;

    @Autowired
    private TipoServizioService tipoServizioService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoServizioMockMvc;

    private TipoServizio tipoServizio;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoServizioResource tipoServizioResource = new TipoServizioResource(tipoServizioService);
        this.restTipoServizioMockMvc = MockMvcBuilders.standaloneSetup(tipoServizioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoServizio createEntity(EntityManager em) {
        TipoServizio tipoServizio = new TipoServizio()
            .nome(DEFAULT_NOME)
            .descrizione(DEFAULT_DESCRIZIONE)
            .nomeBusiness(DEFAULT_NOME_BUSINESS)
            .testoPromo(DEFAULT_TESTO_PROMO)
            .ordinamento(DEFAULT_ORDINAMENTO)
            .documenti(DEFAULT_DOCUMENTI);
        return tipoServizio;
    }

    @Before
    public void initTest() {
        tipoServizio = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoServizio() throws Exception {
        int databaseSizeBeforeCreate = tipoServizioRepository.findAll().size();

        // Create the TipoServizio
        TipoServizioDTO tipoServizioDTO = tipoServizioMapper.toDto(tipoServizio);
        restTipoServizioMockMvc.perform(post("/api/tipo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoServizio in the database
        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeCreate + 1);
        TipoServizio testTipoServizio = tipoServizioList.get(tipoServizioList.size() - 1);
        assertThat(testTipoServizio.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testTipoServizio.getDescrizione()).isEqualTo(DEFAULT_DESCRIZIONE);
        assertThat(testTipoServizio.getNomeBusiness()).isEqualTo(DEFAULT_NOME_BUSINESS);
        assertThat(testTipoServizio.getTestoPromo()).isEqualTo(DEFAULT_TESTO_PROMO);
        assertThat(testTipoServizio.getOrdinamento()).isEqualTo(DEFAULT_ORDINAMENTO);
        assertThat(testTipoServizio.getDocumenti()).isEqualTo(DEFAULT_DOCUMENTI);
    }

    @Test
    @Transactional
    public void createTipoServizioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoServizioRepository.findAll().size();

        // Create the TipoServizio with an existing ID
        tipoServizio.setId(1L);
        TipoServizioDTO tipoServizioDTO = tipoServizioMapper.toDto(tipoServizio);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoServizioMockMvc.perform(post("/api/tipo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TipoServizio in the database
        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoServizioRepository.findAll().size();
        // set the field null
        tipoServizio.setNome(null);

        // Create the TipoServizio, which fails.
        TipoServizioDTO tipoServizioDTO = tipoServizioMapper.toDto(tipoServizio);

        restTipoServizioMockMvc.perform(post("/api/tipo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDTO)))
            .andExpect(status().isBadRequest());

        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoServizios() throws Exception {
        // Initialize the database
        tipoServizioRepository.saveAndFlush(tipoServizio);

        // Get all the tipoServizioList
        restTipoServizioMockMvc.perform(get("/api/tipo-servizios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoServizio.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].descrizione").value(hasItem(DEFAULT_DESCRIZIONE.toString())))
            .andExpect(jsonPath("$.[*].nomeBusiness").value(hasItem(DEFAULT_NOME_BUSINESS.toString())))
            .andExpect(jsonPath("$.[*].testoPromo").value(hasItem(DEFAULT_TESTO_PROMO.toString())))
            .andExpect(jsonPath("$.[*].ordinamento").value(hasItem(DEFAULT_ORDINAMENTO.intValue())))
            .andExpect(jsonPath("$.[*].documenti").value(hasItem(DEFAULT_DOCUMENTI.toString())));
    }

    @Test
    @Transactional
    public void getTipoServizio() throws Exception {
        // Initialize the database
        tipoServizioRepository.saveAndFlush(tipoServizio);

        // Get the tipoServizio
        restTipoServizioMockMvc.perform(get("/api/tipo-servizios/{id}", tipoServizio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoServizio.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.descrizione").value(DEFAULT_DESCRIZIONE.toString()))
            .andExpect(jsonPath("$.nomeBusiness").value(DEFAULT_NOME_BUSINESS.toString()))
            .andExpect(jsonPath("$.testoPromo").value(DEFAULT_TESTO_PROMO.toString()))
            .andExpect(jsonPath("$.ordinamento").value(DEFAULT_ORDINAMENTO.intValue()))
            .andExpect(jsonPath("$.documenti").value(DEFAULT_DOCUMENTI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTipoServizio() throws Exception {
        // Get the tipoServizio
        restTipoServizioMockMvc.perform(get("/api/tipo-servizios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoServizio() throws Exception {
        // Initialize the database
        tipoServizioRepository.saveAndFlush(tipoServizio);
        int databaseSizeBeforeUpdate = tipoServizioRepository.findAll().size();

        // Update the tipoServizio
        TipoServizio updatedTipoServizio = tipoServizioRepository.findOne(tipoServizio.getId());
        // Disconnect from session so that the updates on updatedTipoServizio are not directly saved in db
        em.detach(updatedTipoServizio);
        updatedTipoServizio
            .nome(UPDATED_NOME)
            .descrizione(UPDATED_DESCRIZIONE)
            .nomeBusiness(UPDATED_NOME_BUSINESS)
            .testoPromo(UPDATED_TESTO_PROMO)
            .ordinamento(UPDATED_ORDINAMENTO)
            .documenti(UPDATED_DOCUMENTI);
        TipoServizioDTO tipoServizioDTO = tipoServizioMapper.toDto(updatedTipoServizio);

        restTipoServizioMockMvc.perform(put("/api/tipo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDTO)))
            .andExpect(status().isOk());

        // Validate the TipoServizio in the database
        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeUpdate);
        TipoServizio testTipoServizio = tipoServizioList.get(tipoServizioList.size() - 1);
        assertThat(testTipoServizio.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testTipoServizio.getDescrizione()).isEqualTo(UPDATED_DESCRIZIONE);
        assertThat(testTipoServizio.getNomeBusiness()).isEqualTo(UPDATED_NOME_BUSINESS);
        assertThat(testTipoServizio.getTestoPromo()).isEqualTo(UPDATED_TESTO_PROMO);
        assertThat(testTipoServizio.getOrdinamento()).isEqualTo(UPDATED_ORDINAMENTO);
        assertThat(testTipoServizio.getDocumenti()).isEqualTo(UPDATED_DOCUMENTI);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoServizio() throws Exception {
        int databaseSizeBeforeUpdate = tipoServizioRepository.findAll().size();

        // Create the TipoServizio
        TipoServizioDTO tipoServizioDTO = tipoServizioMapper.toDto(tipoServizio);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoServizioMockMvc.perform(put("/api/tipo-servizios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoServizioDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoServizio in the database
        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoServizio() throws Exception {
        // Initialize the database
        tipoServizioRepository.saveAndFlush(tipoServizio);
        int databaseSizeBeforeDelete = tipoServizioRepository.findAll().size();

        // Get the tipoServizio
        restTipoServizioMockMvc.perform(delete("/api/tipo-servizios/{id}", tipoServizio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TipoServizio> tipoServizioList = tipoServizioRepository.findAll();
        assertThat(tipoServizioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoServizio.class);
        TipoServizio tipoServizio1 = new TipoServizio();
        tipoServizio1.setId(1L);
        TipoServizio tipoServizio2 = new TipoServizio();
        tipoServizio2.setId(tipoServizio1.getId());
        assertThat(tipoServizio1).isEqualTo(tipoServizio2);
        tipoServizio2.setId(2L);
        assertThat(tipoServizio1).isNotEqualTo(tipoServizio2);
        tipoServizio1.setId(null);
        assertThat(tipoServizio1).isNotEqualTo(tipoServizio2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoServizioDTO.class);
        TipoServizioDTO tipoServizioDTO1 = new TipoServizioDTO();
        tipoServizioDTO1.setId(1L);
        TipoServizioDTO tipoServizioDTO2 = new TipoServizioDTO();
        assertThat(tipoServizioDTO1).isNotEqualTo(tipoServizioDTO2);
        tipoServizioDTO2.setId(tipoServizioDTO1.getId());
        assertThat(tipoServizioDTO1).isEqualTo(tipoServizioDTO2);
        tipoServizioDTO2.setId(2L);
        assertThat(tipoServizioDTO1).isNotEqualTo(tipoServizioDTO2);
        tipoServizioDTO1.setId(null);
        assertThat(tipoServizioDTO1).isNotEqualTo(tipoServizioDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tipoServizioMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tipoServizioMapper.fromId(null)).isNull();
    }
}
