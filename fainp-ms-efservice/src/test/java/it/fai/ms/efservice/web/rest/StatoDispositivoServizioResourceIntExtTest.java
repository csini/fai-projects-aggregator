package it.fai.ms.efservice.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.config.AbstractCommonTest;
import it.fai.ms.efservice.domain.Contratto;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.StatoDispositivoServizio;
import it.fai.ms.efservice.domain.TipoDispositivo;
import it.fai.ms.efservice.domain.TipoServizio;
import it.fai.ms.efservice.domain.enumeration.StatoDS;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.repository.StatoDispositivoServizioRepositoryExt;
import it.fai.ms.efservice.repository.TipoServizioRepository;
import it.fai.ms.efservice.service.dto.ChangeStatoDispositivoServizioDTO;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class StatoDispositivoServizioResourceIntExtTest extends AbstractCommonTest {

  @Autowired
  private StatoDispositivoServizioRepositoryExt statoDispositivoServizioRepository;

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  @Autowired
  StatoDispositivoServizioResourceExt statoDispositivoServizioResource;

  @Autowired
  DispositivoRepository dispositivoRepository;

  @Autowired
  TipoServizioRepository tipoServizioRepository;

  @Autowired
  ContrattoRepositoryExt contrattoRepository;

  private MockMvc restStatoDispositivoServizioMockMvc;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);

    this.restStatoDispositivoServizioMockMvc = MockMvcBuilders.standaloneSetup(statoDispositivoServizioResource)
        .alwaysDo(MockMvcResultHandlers.print())
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setControllerAdvice(exceptionTranslator)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  @After
  public void after() { }

  @Test
  public void changeStatoDispositivoServizio() throws Exception {
    long size_before = dispositivoRepository.count();
    long sds_before = statoDispositivoServizioRepository.count();

    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(StatoDispositivoServizioResourceExt.API_CHANGE);

    TipoServizio ts = tipoServizioRepository.findOne(4L);
    TipoDispositivo tipoDispositivo = new TipoDispositivo();
    tipoDispositivo.setId(2L);
    Contratto contratto = new Contratto();
    contratto.setId(17L);

    Dispositivo device = new Dispositivo()
        .dataPrimaAttivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .seriale(RandomString())
        .identificativo(RandomString())
        .tipoDispositivo(tipoDispositivo)
        .contratto(contratto)
        .stato(StatoDispositivo.ACCETTATO_DA_FORNITORE);

    device = dispositivoRepository.save(device);       
    assertThat(size_before + 1).isEqualTo(dispositivoRepository.count());

    ChangeStatoDispositivoServizioDTO dto = new ChangeStatoDispositivoServizioDTO()
        .uuid(device.getIdentificativo())
        .dataAttivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .dataDisattivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .pan(RandomString())
        .stato(StatoDS.ATTIVO)
        .tipoServizio(TipoServizioEnum.valueOf(ts.getNome()));

    restStatoDispositivoServizioMockMvc.perform(post(builder.build().toString())
                                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isOk())
    .andReturn();

    assertThat(sds_before + 1).isEqualTo(statoDispositivoServizioRepository.count());
    StatoDispositivoServizio sds = statoDispositivoServizioRepository.findFirstByOrderByIdDesc();
    assertThat(sds.getDispositivo().getId()).isEqualTo(device.getId());
    assertThat(sds.getTipoServizio().getId()).isEqualTo(ts.getId());
    assertThat(sds.getPan()).isEqualTo(dto.getPan());
    assertThat(sds.getStato()).isEqualTo(dto.getStato());

    dto = new ChangeStatoDispositivoServizioDTO()
        .uuid(device.getIdentificativo())
        .dataAttivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .dataDisattivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .pan(RandomString())
        .stato(StatoDS.NON_ATTIVO)
        .tipoServizio(TipoServizioEnum.valueOf(ts.getNome()));

    restStatoDispositivoServizioMockMvc.perform(post(builder.build().toString())
                                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isOk());

    assertThat(sds_before + 1).isEqualTo(statoDispositivoServizioRepository.count());
    sds = statoDispositivoServizioRepository.findFirstByOrderByIdDesc();
    assertThat(sds.getDispositivo().getId()).isEqualTo(device.getId());
    assertThat(sds.getTipoServizio().getId()).isEqualTo(ts.getId());
    assertThat(sds.getPan()).isEqualTo(dto.getPan());
    assertThat(sds.getStato()).isEqualTo(dto.getStato());

    statoDispositivoServizioRepository.delete(sds.getId());
    dispositivoRepository.delete(device.getId());

    assertThat(size_before).isEqualTo(dispositivoRepository.count());
    assertThat(sds_before).isEqualTo(statoDispositivoServizioRepository.count());
  }

  @Test
  public void changeStatoDispositivoServizio_failed() throws Exception {
    long size_before = dispositivoRepository.count();
    long sds_before = statoDispositivoServizioRepository.count();
    
    UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(StatoDispositivoServizioResourceExt.API_CHANGE);
    
    ChangeStatoDispositivoServizioDTO dto = new ChangeStatoDispositivoServizioDTO()
        .uuid(RandomString())
        .dataAttivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .dataDisattivazione(Instant.now().truncatedTo(ChronoUnit.MILLIS))
        .pan(RandomString())
        .stato(StatoDS.ATTIVO)
        .tipoServizio(TipoServizioEnum.CRISTALLI);

    restStatoDispositivoServizioMockMvc.perform(post(builder.build().toString())
                                                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                .content(TestUtil.convertObjectToJsonBytes(dto)))
    .andExpect(status().isBadRequest());

    assertThat(size_before).isEqualTo(dispositivoRepository.count());
    assertThat(sds_before).isEqualTo(statoDispositivoServizioRepository.count());
  }

}
