package it.fai.ms.efservice.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.dto.CheckDispositiviDTO;
import it.fai.ms.efservice.service.impl.DispositivoServiceImpl;
import it.fai.ms.efservice.web.rest.DispositivoResourceExt;
import it.fai.ms.efservice.web.rest.TestUtil;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DispositivoResourceExtTest {

  private DispositivoResourceExt deviceResourceExt;

  private DispositivoServiceImpl                  deviceService                           = mock(DispositivoServiceImpl.class);
  private DispositivoServiceExt                   deviceServiceExt                        = mock(DispositivoServiceExt.class);
  private ContrattoServiceExt                     contrattoService                        = mock(ContrattoServiceExt.class);
  private TipoDispositivoServiceExt               deviceTypeService                       = mock(TipoDispositivoServiceExt.class);
  private DispositiviDaSpedireCacheService        dispositiviDaSpedireCache               = mock(DispositiviDaSpedireCacheService.class);
  private ModificationOperationsDeviceTypeService modificationOperationsDeviceTypeService = mock(ModificationOperationsDeviceTypeService.class);

  @Autowired
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Autowired
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Autowired
  private ExceptionTranslator exceptionTranslator;

  private MockMvc restDeviceMockMvc;

  private Map<String, String> veicoli;
  String                      codiceCliente   = null;
  TipoDispositivoEnum         tipoDispositivo = null;

  @Before
  public void setUp() throws Exception {
    veicoli = new HashMap<>();
    deviceResourceExt = new DispositivoResourceExt(deviceService, deviceServiceExt, contrattoService, deviceTypeService,
                                                   dispositiviDaSpedireCache, modificationOperationsDeviceTypeService);

    restDeviceMockMvc = MockMvcBuilders.standaloneSetup(deviceResourceExt)
                                       .alwaysDo(MockMvcResultHandlers.print())
                                       .setCustomArgumentResolvers(pageableArgumentResolver)
                                       .setControllerAdvice(exceptionTranslator)
                                       .setMessageConverters(jacksonMessageConverter)
                                       .build();

    codiceCliente = "0012345";
    tipoDispositivo = TipoDispositivoEnum.TRACKYCARD;
  }

  @Test
  public void when_checkDeviceExists_then_Exception() throws Exception {
    CheckDispositiviDTO dto = createDtoCheckDispositivi();
    when(deviceServiceExt.checkDispositivoExists(dto)).thenReturn(newLicePlateList());
    restDeviceMockMvc.perform(post(DispositivoResourceExt.API_CHECK_DISPOSITIVI))
                     .andExpect(status().isBadRequest());
  }

  @Test
  public void when_checkDeviceExists_then_notActiveDeviceFound() throws Exception {
    CheckDispositiviDTO dto = createDtoCheckDispositivi();
    when(deviceServiceExt.checkDispositivoExists(dto)).thenReturn(newLicePlateList());
    restDeviceMockMvc.perform(post(DispositivoResourceExt.API_CHECK_DISPOSITIVI).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                .content(TestUtil.convertObjectToJsonBytes(dto)))
                     .andExpect(status().isOk());
  }

  @Test
  public void when_checkDeviceExists_then_deviceFound() throws Exception {
    CheckDispositiviDTO dto = createDtoCheckDispositivi();
    when(deviceServiceExt.checkDispositivoExists(dto)).thenReturn(null);
    restDeviceMockMvc.perform(post(DispositivoResourceExt.API_CHECK_DISPOSITIVI).contentType(TestUtil.APPLICATION_JSON_UTF8)
                                                                                .content(TestUtil.convertObjectToJsonBytes(dto)))
                     .andExpect(status().isOk());
  }

  private List<String> newLicePlateList() {
    List<String> list = new ArrayList<>();
    String[] array = veicoli.keySet()
                            .toArray(new String[] {});
    for (int i = 0; i < 2; i++) {
      list.add(veicoli.get(array[i]));
    }
    return list;
  }

  private CheckDispositiviDTO createDtoCheckDispositivi() {
    CheckDispositiviDTO dto = new CheckDispositiviDTO();
    dto.setTipoDispositivo(tipoDispositivo);
    dto.setCodiceCliente(codiceCliente);
    veicoli = new HashMap<>();
    veicoli.put("000001", "::targa1::");
    veicoli.put("000002", "::targa2::");
    veicoli.put("000003", "::targa3::");
    veicoli.put("000004", "::targa4::");
    veicoli.put("000005", "::targa5::");
    dto.setVeicoli(veicoli);
    return dto;
  }

}
