package it.fai.ms.efservice.rules.engine.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class RuleEngineServiceTypeTest {

  @Test
  public void testEqualsObject() {
    RuleEngineServiceType ruleEngineServiceType1 = new RuleEngineServiceType(new RuleEngineServiceTypeId("::id::"));
    RuleEngineServiceType ruleEngineServiceType2 = new RuleEngineServiceType(new RuleEngineServiceTypeId("::id::"));
    assertThat(ruleEngineServiceType1).isEqualTo(ruleEngineServiceType2);
  }

  @Test
  public void testHashCode() {
    RuleEngineServiceType ruleEngineServiceType1 = new RuleEngineServiceType(new RuleEngineServiceTypeId("::id::"));
    RuleEngineServiceType ruleEngineServiceType2 = new RuleEngineServiceType(new RuleEngineServiceTypeId("::id::"));
    assertThat(ruleEngineServiceType1.hashCode()).isEqualTo(ruleEngineServiceType2.hashCode());
  }

  @SuppressWarnings("unused")
  @Test(expected = IllegalArgumentException.class)
  public void testRuleEngineServiceTypeWhenIdIsNull() {
    new RuleEngineServiceType(null);
  }

}
