package it.fai.ms.efservice.rules.engine.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class EuroClassConverterTest {

  @Test
  public void testConvertEuroClassToMappingTelepass_when_euroClass_isNull() {

    String euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToMappingTelepass(null);

    assertThat(euroClassToMappingTelepass).isEqualTo("00");
  }

  @Test
  public void testConvertEuroClassToMappingTelepass_when_euroClass_isEmpty() {

    String euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToMappingTelepass("");

    assertThat(euroClassToMappingTelepass).isEqualTo("00");
  }

  @Test
  public void testConvertEuroClassToMappingTelepass_when_euroClass_isBlank() {

    String euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToMappingTelepass(" ");

    assertThat(euroClassToMappingTelepass).isEqualTo("00");
  }

  @Test
  public void testConvertEuroClassToMappingTelepass_when_euroClass_isntMapped() {

    String euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToMappingTelepass("::notMapped::");

    assertThat(euroClassToMappingTelepass).isEqualTo("00");
  }

  @Test
  public void testConvertEuroClassToMappingTelepass_when_euroClass_is01() {

    String euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToMappingTelepass("EURO1");

    assertThat(euroClassToMappingTelepass).isEqualTo("01");
  }

  @Test
  public void testConvertEuroClassToInteger_when_euroClass_isNull() {

    int euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToInteger(null);

    assertThat(euroClassToMappingTelepass).isEqualTo(0);
  }

  @Test
  public void testConvertEuroClassToInteger_when_euroClass_isEmpty() {

    int euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToInteger("");

    assertThat(euroClassToMappingTelepass).isEqualTo(0);
  }

  @Test
  public void testConvertEuroClassToInteger_when_euroClass_isBlank() {

    int euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToInteger(" ");

    assertThat(euroClassToMappingTelepass).isEqualTo(0);
  }

  @Test
  public void testConvertEuroClassToInteger_when_euroClass_isntMapped() {

    int euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToInteger("::notMapped::");

    assertThat(euroClassToMappingTelepass).isEqualTo(0);
  }

  @Test
  public void testConvertEuroClassToInteger_when_euroClass_is1() {

    int euroClassToMappingTelepass = EuroClassConverter.convertEuroClassToInteger("EURO1");

    assertThat(euroClassToMappingTelepass).isEqualTo(1);
  }

}
