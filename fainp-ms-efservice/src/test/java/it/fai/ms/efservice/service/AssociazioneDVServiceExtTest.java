package it.fai.ms.efservice.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.domain.AssociazioneDV;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.AssociazioneDVRepository;
import it.fai.ms.efservice.repository.ContrattoRepositoryExt;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.mapper.AssociazioneDVMapper;
import it.fai.ms.efservice.service.mapper.DispositivoMapper;

@RunWith(MockitoJUnitRunner.class)
public class AssociazioneDVServiceExtTest {

  private AssociazioneDVServiceExt associazioneDVService;
  @Mock
  private AssociazioneDVMapper     mockAssociazioneDVMapper;
  @Mock
  private AssociazioneDVRepository mockAssociazioneDVRepository;
  @Mock
  private DispositivoMapper        mockDispositivoMapper;
  @Mock
  private DispositivoServiceExt    mockDispositivoServiceExt;
  
  private ModificationOperationsDeviceTypeService mockModificationOperationsDeviceService;
  
  @Mock
  private ContrattoRepositoryExt mockContrattoRepositoryExt;


  @Before
  public void setUp() throws Exception {
    associazioneDVService = new AssociazioneDVServiceExt(mockAssociazioneDVRepository, mockDispositivoMapper, mockAssociazioneDVMapper,
                                                         mockDispositivoServiceExt, mockModificationOperationsDeviceService,mockContrattoRepositoryExt);
  }

  @Test
  public void testFindAssociazioneDVbyIdDispositivo() {
    associazioneDVService.findAssociazioneDVbyIdDispositivo(newDeviceId());

    then(mockAssociazioneDVRepository).should()
                                      .findByDispositivoId(newDeviceId());
    then(mockAssociazioneDVRepository).shouldHaveNoMoreInteractions();
    then(mockAssociazioneDVMapper).shouldHaveZeroInteractions();
    then(mockDispositivoMapper).shouldHaveZeroInteractions();
  }

  @Test
  @Ignore
  public void testFindDispositiviByVehicle() {
    given(mockAssociazioneDVRepository.findByUuidVeicolo(newVehicleUuid())).willReturn(newAssociazioneDV());

    List<DispositivoDTO> devicesDto = associazioneDVService.findDispositiviByVehicle(newVehicleUuid(), "0046348");

    then(mockAssociazioneDVRepository).should()
                                      .findByUuidVeicolo(newVehicleUuid());

    assertThat(devicesDto).containsExactlyInAnyOrder(newDeviceDtoWithoutPendingOperation(), newDeviceDtoWithPendingOperation());

    DispositivoDTO dispositivoDTO1 = devicesDto.get(0);
    DispositivoDTO dispositivoDTO2 = devicesDto.get(1);

    assertThat(dispositivoDTO1).isEqualToComparingOnlyGivenFields(newDeviceDtoWithoutPendingOperation(), "operazionePending");
    assertThat(dispositivoDTO2).isEqualToComparingOnlyGivenFields(newDeviceDtoWithPendingOperation(), "operazionePending");
  }

  private List<AssociazioneDV> newAssociazioneDV() {
    List<AssociazioneDV> deviceVehicleRelationship = new LinkedList<>();
    AssociazioneDV relationship1 = new AssociazioneDV();
    relationship1.setDispositivo(newDeviceWithoutYellowState());
    deviceVehicleRelationship.add(relationship1);
    AssociazioneDV relationship2 = new AssociazioneDV();
    relationship2.setDispositivo(newDeviceWithYellowState());
    deviceVehicleRelationship.add(relationship2);
    return deviceVehicleRelationship;
  }

  private Dispositivo newDeviceWithYellowState() {
    Dispositivo device = new Dispositivo();
    device.setStato(StatoDispositivo.ATTIVO_SPEDITO_A_FAI);
    return device;
  }

  private Dispositivo newDeviceWithoutYellowState() {
    Dispositivo device = new Dispositivo();
    device.setStato(StatoDispositivo.ATTIVO);
    return device;
  }

  private DispositivoDTO newDeviceDtoWithoutPendingOperation() {
    DispositivoDTO deviceDto = new DispositivoDTO();
    deviceDto.setOperazionePending(false);
    return deviceDto;
  }

  private DispositivoDTO newDeviceDtoWithPendingOperation() {
    DispositivoDTO deviceDto = new DispositivoDTO();
    deviceDto.setOperazionePending(true);
    return deviceDto;
  }

  private Long newDeviceId() {
    return new Long(666);
  }

  private String newVehicleUuid() {
    return "::vehicleUuid::";
  }

}
