package it.fai.ms.efservice.rules.engine.repository;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.infinispan.Cache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import it.fai.ms.efservice.rules.engine.model.RuleContext;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineDeviceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceType;
import it.fai.ms.efservice.rules.engine.model.RuleEngineServiceTypeId;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicle;
import it.fai.ms.efservice.rules.engine.model.RuleEngineVehicleId;
import it.fai.ms.efservice.rules.engine.model.RuleOutcome;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleEntityKey;
import it.fai.ms.efservice.rules.engine.repository.entity.RuleOutcomeEntity;
import it.fai.ms.efservice.service.NotificationPreconditionVehicleAnomalyService;

@RunWith(MockitoJUnitRunner.class)
public class RuleOutcomeRepositoryImplTest {

  @Mock
  private Cache<RuleEntityKey, RuleOutcomeEntity>       cache;
  private RuleOutcomeRepositoryImpl                     ruleOutcomeRepository;
  private NotificationPreconditionVehicleAnomalyService notificationPreconditionService;

  @Before
  public void setUp() throws Exception {
    ruleOutcomeRepository = new RuleOutcomeRepositoryImpl(cache, notificationPreconditionService);
  }

  @Test
  public void testCount() {
    ruleOutcomeRepository.count();

    verify(cache).size();
    verifyNoMoreInteractions(cache);
  }

  @Test
  public void testLoad() {
    ruleOutcomeRepository.find(new RuleEngineServiceTypeId("::serviceTypeId::"), new RuleEngineDeviceTypeId("::deviceTypeId::"),
                               new RuleEngineVehicleId("::vehicleId::"));

    final RuleEntityKey key = new RuleEntityKey();
    key.setDeviceTypeId("::deviceTypeId::");
    key.setServiceTypeId("::serviceTypeId::");
    key.setVehicleId("::vehicleId::");
    verify(cache).get(key);
  }

  @Test
  public void testSave() {
    RuleContext ruleContext = new RuleContext(new RuleEngineServiceType(new RuleEngineServiceTypeId("::serviceTypeId::")),
                                              new RuleEngineDeviceType(new RuleEngineDeviceTypeId("::deviceTypeId::")),
                                              new RuleEngineVehicle(new RuleEngineVehicleId("::vehicleId::")));
    RuleOutcome ruleOutcome = new RuleOutcome();
    ruleOutcomeRepository.save(ruleContext, ruleOutcome);

    final RuleEntityKey key = new RuleEntityKey();
    key.setDeviceTypeId("::deviceTypeId::");
    key.setServiceTypeId("::serviceTypeId::");
    key.setVehicleId("::vehicleId::");
    final RuleOutcomeEntity val = new RuleOutcomeEntity(true);

    verify(cache).put(key, val);
  }

}
