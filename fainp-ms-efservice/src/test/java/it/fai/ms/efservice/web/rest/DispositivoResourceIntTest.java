package it.fai.ms.efservice.web.rest;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.FaiefserviceApp;

import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.repository.DispositivoRepository;
import it.fai.ms.efservice.service.DispositivoService;
import it.fai.ms.efservice.service.dto.DispositivoDTO;
import it.fai.ms.efservice.service.mapper.DispositivoMapper;
import it.fai.ms.efservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import it.fai.ms.efservice.domain.enumeration.ModalitaSpedizione;
/**
 * Test class for the DispositivoResource REST controller.
 *
 * @see DispositivoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaiefserviceApp.class)
public class DispositivoResourceIntTest {

    private static final String DEFAULT_SERIALE = "AAAAAAAAAA";
    private static final String UPDATED_SERIALE = "BBBBBBBBBB";

    private static final StatoDispositivo DEFAULT_STATO = StatoDispositivo.INIZIALE;
    private static final StatoDispositivo UPDATED_STATO = StatoDispositivo.ATTIVO_SPEDITO_DA_FAI;

    private static final Instant DEFAULT_DATA_SPEDIZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_SPEDIZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DATA_PRIMA_ATTIVAZIONE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATA_PRIMA_ATTIVAZIONE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TRACKING_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TRACKING_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_SHIPMENT_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_SHIPMENT_REFERENCE = "BBBBBBBBBB";

    private static final ModalitaSpedizione DEFAULT_MODALITA_SPEDIZIONE = ModalitaSpedizione.A_FAI;
    private static final ModalitaSpedizione UPDATED_MODALITA_SPEDIZIONE = ModalitaSpedizione.A_CLIENTE;

    private static final String DEFAULT_IDENTIFICATIVO = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFICATIVO = "BBBBBBBBBB";

    @Autowired
    private DispositivoRepository dispositivoRepository;

    @Autowired
    private DispositivoMapper dispositivoMapper;

    @Autowired
    private DispositivoService dispositivoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDispositivoMockMvc;

    private Dispositivo dispositivo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DispositivoResource dispositivoResource = new DispositivoResource(dispositivoService);
        this.restDispositivoMockMvc = MockMvcBuilders.standaloneSetup(dispositivoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Dispositivo createEntity(EntityManager em) {
        Dispositivo dispositivo = new Dispositivo()
            .identificativo(DEFAULT_IDENTIFICATIVO)
            .seriale(DEFAULT_SERIALE)
            .stato(DEFAULT_STATO)
            .dataSpedizione(DEFAULT_DATA_SPEDIZIONE)
            .dataPrimaAttivazione(DEFAULT_DATA_PRIMA_ATTIVAZIONE)
            .trackingNumber(DEFAULT_TRACKING_NUMBER)
            .shipmentReference(DEFAULT_SHIPMENT_REFERENCE)
            .modalitaSpedizione(DEFAULT_MODALITA_SPEDIZIONE);
        return dispositivo;
    }

    @Before
    public void initTest() {
        dispositivo = createEntity(em);
    }

    @Test
    @Transactional
    public void createDispositivo() throws Exception {
        int databaseSizeBeforeCreate = dispositivoRepository.findAll().size();

        // Create the Dispositivo
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(dispositivo);
        restDispositivoMockMvc.perform(post("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isCreated());

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeCreate + 1);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getSeriale()).isEqualTo(DEFAULT_SERIALE);
        assertThat(testDispositivo.getStato()).isEqualTo(DEFAULT_STATO);
        assertThat(testDispositivo.getDataSpedizione()).isEqualTo(DEFAULT_DATA_SPEDIZIONE);
        assertThat(testDispositivo.getDataPrimaAttivazione()).isEqualTo(DEFAULT_DATA_PRIMA_ATTIVAZIONE);
        assertThat(testDispositivo.getTrackingNumber()).isEqualTo(DEFAULT_TRACKING_NUMBER);
        assertThat(testDispositivo.getShipmentReference()).isEqualTo(DEFAULT_SHIPMENT_REFERENCE);
        assertThat(testDispositivo.getModalitaSpedizione()).isEqualTo(DEFAULT_MODALITA_SPEDIZIONE);
    }

    @Test
    @Transactional
    public void createDispositivoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dispositivoRepository.findAll().size();

        // Create the Dispositivo with an existing ID
        dispositivo.setId(1L);
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(dispositivo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDispositivoMockMvc.perform(post("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkSerialeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dispositivoRepository.findAll().size();
        // set the field null
        dispositivo.setSeriale(null);

        // Create the Dispositivo, which fails.
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(dispositivo);

        restDispositivoMockMvc.perform(post("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isBadRequest());

        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatoIsRequired() throws Exception {
        int databaseSizeBeforeTest = dispositivoRepository.findAll().size();
        // set the field null
        dispositivo.setStato(null);

        // Create the Dispositivo, which fails.
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(dispositivo);

        restDispositivoMockMvc.perform(post("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isBadRequest());

        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDispositivos() throws Exception {
        // Initialize the database
        dispositivoRepository.saveAndFlush(dispositivo);

        // Get all the dispositivoList
        restDispositivoMockMvc.perform(get("/api/dispositivos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dispositivo.getId().intValue())))
            .andExpect(jsonPath("$.[*].seriale").value(hasItem(DEFAULT_SERIALE.toString())))
            .andExpect(jsonPath("$.[*].stato").value(hasItem(DEFAULT_STATO.toString())))
            .andExpect(jsonPath("$.[*].dataSpedizione").value(hasItem(DEFAULT_DATA_SPEDIZIONE.toString())))
            .andExpect(jsonPath("$.[*].dataPrimaAttivazione").value(hasItem(DEFAULT_DATA_PRIMA_ATTIVAZIONE.toString())))
            .andExpect(jsonPath("$.[*].trackingNumber").value(hasItem(DEFAULT_TRACKING_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].shipmentReference").value(hasItem(DEFAULT_SHIPMENT_REFERENCE.toString())))
            .andExpect(jsonPath("$.[*].modalitaSpedizione").value(hasItem(DEFAULT_MODALITA_SPEDIZIONE.toString())));
    }

    @Test
    @Transactional
    public void getDispositivo() throws Exception {
        // Initialize the database
        dispositivoRepository.saveAndFlush(dispositivo);

        // Get the dispositivo
        restDispositivoMockMvc.perform(get("/api/dispositivos/{id}", dispositivo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dispositivo.getId().intValue()))
            .andExpect(jsonPath("$.seriale").value(DEFAULT_SERIALE.toString()))
            .andExpect(jsonPath("$.stato").value(DEFAULT_STATO.toString()))
            .andExpect(jsonPath("$.dataSpedizione").value(DEFAULT_DATA_SPEDIZIONE.toString()))
            .andExpect(jsonPath("$.dataPrimaAttivazione").value(DEFAULT_DATA_PRIMA_ATTIVAZIONE.toString()))
            .andExpect(jsonPath("$.trackingNumber").value(DEFAULT_TRACKING_NUMBER.toString()))
            .andExpect(jsonPath("$.shipmentReference").value(DEFAULT_SHIPMENT_REFERENCE.toString()))
            .andExpect(jsonPath("$.modalitaSpedizione").value(DEFAULT_MODALITA_SPEDIZIONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDispositivo() throws Exception {
        // Get the dispositivo
        restDispositivoMockMvc.perform(get("/api/dispositivos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDispositivo() throws Exception {
        // Initialize the database
        dispositivoRepository.saveAndFlush(dispositivo);
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().size();

        // Update the dispositivo
        Dispositivo updatedDispositivo = dispositivoRepository.findOne(dispositivo.getId());
        updatedDispositivo
            .identificativo(UPDATED_IDENTIFICATIVO)
            .seriale(UPDATED_SERIALE)
            .stato(UPDATED_STATO)
            .dataSpedizione(UPDATED_DATA_SPEDIZIONE)
            .dataPrimaAttivazione(UPDATED_DATA_PRIMA_ATTIVAZIONE)
            .trackingNumber(UPDATED_TRACKING_NUMBER)
            .shipmentReference(UPDATED_SHIPMENT_REFERENCE)
            .modalitaSpedizione(UPDATED_MODALITA_SPEDIZIONE);
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(updatedDispositivo);

        restDispositivoMockMvc.perform(put("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isOk());

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate);
        Dispositivo testDispositivo = dispositivoList.get(dispositivoList.size() - 1);
        assertThat(testDispositivo.getSeriale()).isEqualTo(UPDATED_SERIALE);
        assertThat(testDispositivo.getStato()).isEqualTo(UPDATED_STATO);
        assertThat(testDispositivo.getDataSpedizione()).isEqualTo(UPDATED_DATA_SPEDIZIONE);
        assertThat(testDispositivo.getDataPrimaAttivazione()).isEqualTo(UPDATED_DATA_PRIMA_ATTIVAZIONE);
        assertThat(testDispositivo.getTrackingNumber()).isEqualTo(UPDATED_TRACKING_NUMBER);
        assertThat(testDispositivo.getShipmentReference()).isEqualTo(UPDATED_SHIPMENT_REFERENCE);
        assertThat(testDispositivo.getModalitaSpedizione()).isEqualTo(UPDATED_MODALITA_SPEDIZIONE);
    }

    @Test
    @Transactional
    public void updateNonExistingDispositivo() throws Exception {
        int databaseSizeBeforeUpdate = dispositivoRepository.findAll().size();

        // Create the Dispositivo
        DispositivoDTO dispositivoDTO = dispositivoMapper.toDto(dispositivo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDispositivoMockMvc.perform(put("/api/dispositivos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dispositivoDTO)))
            .andExpect(status().isCreated());

        // Validate the Dispositivo in the database
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Ignore// non e' una cosa che amo fare ! (J)
    @Transactional
    public void deleteDispositivo() throws Exception {
        // Initialize the database
        dispositivoRepository.saveAndFlush(dispositivo);
        int databaseSizeBeforeDelete = dispositivoRepository.findAll().size();

        // Get the dispositivo
        restDispositivoMockMvc.perform(delete("/api/dispositivos/{id}", dispositivo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Dispositivo> dispositivoList = dispositivoRepository.findAll();
        assertThat(dispositivoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Dispositivo.class);
        Dispositivo dispositivo1 = new Dispositivo();
        dispositivo1.setId(1L);
        Dispositivo dispositivo2 = new Dispositivo();
        dispositivo2.setId(dispositivo1.getId());
        assertThat(dispositivo1).isEqualTo(dispositivo2);
        dispositivo2.setId(2L);
        assertThat(dispositivo1).isNotEqualTo(dispositivo2);
        dispositivo1.setId(null);
        assertThat(dispositivo1).isNotEqualTo(dispositivo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DispositivoDTO.class);
        DispositivoDTO dispositivoDTO1 = new DispositivoDTO();
        dispositivoDTO1.setId(1L);
        DispositivoDTO dispositivoDTO2 = new DispositivoDTO();
        assertThat(dispositivoDTO1).isNotEqualTo(dispositivoDTO2);
        dispositivoDTO2.setId(dispositivoDTO1.getId());
        assertThat(dispositivoDTO1).isEqualTo(dispositivoDTO2);
        dispositivoDTO2.setId(2L);
        assertThat(dispositivoDTO1).isNotEqualTo(dispositivoDTO2);
        dispositivoDTO1.setId(null);
        assertThat(dispositivoDTO1).isNotEqualTo(dispositivoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dispositivoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dispositivoMapper.fromId(null)).isNull();
    }
}
