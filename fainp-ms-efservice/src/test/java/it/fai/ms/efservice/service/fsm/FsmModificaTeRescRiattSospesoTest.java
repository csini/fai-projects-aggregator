/**
 * 
 */
package it.fai.ms.efservice.service.fsm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import it.fai.ms.common.jms.dto.DispositivoFSMChangeStatusDTO;
import it.fai.ms.efservice.FaiefserviceApp;
import it.fai.ms.efservice.domain.Dispositivo;
import it.fai.ms.efservice.domain.OrdineCliente;
import it.fai.ms.efservice.domain.Richiesta;
import it.fai.ms.efservice.domain.enumeration.StatoRichiesta;
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta;
import it.fai.ms.efservice.service.fsm.bean.telepass.TelepassEuEventiOutboundDTO;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.RichiestaEvent;
import it.fai.ms.efservice.service.fsm.type.telepass.eu.FsmModificaTeRescRiattSospensione;
import it.fai.ms.efservice.service.jms.config.JmsSenderConfigurationTest;

/**
 * @author Luca Vassallo
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FaiefserviceApp.class, JmsSenderConfigurationTest.class })
@Ignore
public class FsmModificaTeRescRiattSospesoTest {

  @Autowired
  private FsmModificaTeRescRiattSospensione fsmModifica;

  private Richiesta richiesta;

  private String uuidRichiesta = null;

  private List<String> uuidDispositivi = null;

  private TelepassEuEventiOutboundDTO telepassEuEventiOutboundDTO;

  @Before // Set-up
  public void setUp() throws Exception {

    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "admin"));
    SecurityContextHolder.setContext(securityContext);

    uuidDispositivi = new ArrayList<>();

    richiesta = new Richiesta();
    richiesta.setId(RandomUtils.nextLong());
    richiesta.setStato(StatoRichiesta.ATTIVO_PER_MODIFICA_TE);
    uuidRichiesta = UUID.randomUUID()
                        .toString();
    richiesta.setIdentificativo(uuidRichiesta);

    Set<Dispositivo> dispositivos = new HashSet<>();
    for (int i = 0; i < 2; i++) {
      Dispositivo disp = new Dispositivo();
      disp.setId(RandomUtils.nextLong());
      String uuidDispositivo = UUID.randomUUID()
                                   .toString();
      disp.setIdentificativo(uuidDispositivo);
      uuidDispositivi.add(uuidDispositivo);
      dispositivos.add(disp);
    }
    OrdineCliente oc = new OrdineCliente();
    richiesta.setOrdineCliente(oc.identificativo(UUID.randomUUID()
                                                     .toString()));
    richiesta.setDispositivos(dispositivos);
  }

  @Test
  public void FsmModificaTeRescRiattSospeso_001_01() throws Exception {
    richiesta.setTipo(TipoRichiesta.SOSPENSIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.SOSPESO);
    // telepassEuEventiOutboundDTO = new
    // TelepassEuEventiOutboundDTO(TipoEventoOutbound.MODIFICA_EVENTO_CONTRATTO_SOSPENSIONE, uuidRichiesta,
    // richiesta.getOrdineCliente()
    // .getIdentificativo(),
    // null); TODO managed
    verify(JmsSenderConfigurationTest.getJmsSender()).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);

    DispositivoFSMChangeStatusDTO dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(0),
                                                                                        DispositivoEvent.SOSPENSIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
    dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(1), DispositivoEvent.SOSPENSIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
  }

  @Test
  public void FsmModificaTeRescRiattSospeso_001_02() throws Exception {
    richiesta.setTipo(TipoRichiesta.SOSPENSIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.SOSPESO);
    assertChangeStateFsmTe(RichiestaEvent.MU_RICHIESTA_RIATTIVAZIONE, StatoRichiesta.RIATTIVATO);

    // telepassEuEventiOutboundDTO = new
    // TelepassEuEventiOutboundDTO(TipoEventoOutbound.MODIFICA_EVENTO_CONTRATTO_SOSPENSIONE, uuidRichiesta,
    // richiesta.getOrdineCliente()
    // .getIdentificativo(),
    // null); TODO managed
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);

    DispositivoFSMChangeStatusDTO dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(0),
                                                                                        DispositivoEvent.SOSPENSIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
    dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(1), DispositivoEvent.SOSPENSIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);

    dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(0), DispositivoEvent.ATTIVAZIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
    dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(1), DispositivoEvent.ATTIVAZIONE.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
  }

  @Test
  public void FsmModificaTeRescRiattSospeso_002() throws Exception {
    richiesta.setTipo(TipoRichiesta.RESCISSIONE);
    assertChangeStateFsmTe(RichiestaEvent.INITIAL, StatoRichiesta.CESSATO);

    // telepassEuEventiOutboundDTO = new
    // TelepassEuEventiOutboundDTO(TipoEventoOutbound.MODIFICA_EVENTO_CONTRATTO_CHIUSURA, uuidRichiesta,
    // richiesta.getOrdineCliente()
    // .getIdentificativo(),
    // null); TODO managed
    verify(JmsSenderConfigurationTest.getJmsSender()).publishTelepassEuEventiOutboundDTO(telepassEuEventiOutboundDTO);

    DispositivoFSMChangeStatusDTO dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(0),
                                                                                        DispositivoEvent.REVOCA.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
    dispositivoFsmDTO = new DispositivoFSMChangeStatusDTO(uuidDispositivi.get(1), DispositivoEvent.REVOCA.name());
    verify(JmsSenderConfigurationTest.getJmsSender(), times(2)).publishFsmCommandDispositivoMessage(dispositivoFsmDTO);
  }

  private void assertChangeStateFsmTe(RichiestaEvent event, StatoRichiesta state) throws Exception {
    boolean availableCommand = fsmModifica.isAvailableCommand(event, richiesta);
    assertTrue(availableCommand);

    fsmModifica.executeCommandToChangeState(event, richiesta);
    StatoRichiesta fsmStatus = richiesta.getStato();
    assertEquals(state.name(), fsmStatus.name());
  }

}
