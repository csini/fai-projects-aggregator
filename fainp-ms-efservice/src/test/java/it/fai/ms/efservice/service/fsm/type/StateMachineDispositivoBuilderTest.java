package it.fai.ms.efservice.service.fsm.type;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent;
import it.fai.ms.efservice.service.fsm.enumeration.FsmType;

@RunWith(MockitoJUnitRunner.class)
public class StateMachineDispositivoBuilderTest {

  @Mock
  private StateMachineFactory<StatoDispositivo, DispositivoEvent> mockStateMachineFactory;
  private StateMachineDispositivoBuilder                                     stateMachineBuilder;

  @Before
  public void setUp() throws Exception {
    stateMachineBuilder = new StateMachineDispositivoBuilder(mockStateMachineFactory);
  }

  @Test
  public void testBuild() {
    StateMachine<StatoDispositivo, DispositivoEvent> mockStateMachine = mock(StateMachine.class);
    when(mockStateMachine.getId()).thenReturn("::stateMachineId::");

    when(mockStateMachineFactory.getStateMachine(newStateMachineId())).thenReturn(mockStateMachine);

    stateMachineBuilder.build();

    then(mockStateMachineFactory).should()
                                 .getStateMachine(newStateMachineId());
  }

  private String newStateMachineId() {
    return FsmType.DISPOSITIVO.fsmName();
  }

}
