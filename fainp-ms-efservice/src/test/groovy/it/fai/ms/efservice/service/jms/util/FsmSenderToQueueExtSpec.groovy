package it.fai.ms.efservice.service.jms.util

import it.fai.ms.efservice.domain.*
import it.fai.ms.efservice.domain.enumeration.TipoRichiesta
import it.fai.ms.efservice.service.jms.JmsTopicSenderService
import it.fai.ms.integration.system.MessageSystemConfiguration
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.*

@SpringBootTest
class FsmSenderToQueueExtSpec extends Specification {
    @Mock
    Logger log
    @Mock
    JmsTopicSenderService senderJmsService
    @InjectMocks
    FsmSenderToQueue fsmSenderToQueue

  @InjectMocks
  MessageSystemConfiguration messageSystemConfiguration

    def setup() {
        MockitoAnnotations.initMocks(this)
      log.info("AAAAAAAAAAAAAAAAAAAAAAAA")
    }

    @Unroll
    def 'send Message To where richiesta=#richiesta then expect: #expectedResult'() {
        expect:
          fsmSenderToQueue.sendMessageTo(richiesta) == true
          assert expectedResult == true
        where:
        richiesta                                                                                                                                                                                                                                                                                             || expectedResult
        new Richiesta(identificativo: "ABCD", tipo: TipoRichiesta.NUOVO_DISPOSITIVO, contratto: new Contratto(codContrattoCliente: "00064816"), ordineCliente: new OrdineCliente(identificativo: "123", clienteAssegnatario: new ClienteFai(codiceCliente: "0073935"))) || true
    }

    @Unroll
    def "send Message For Change Stato Dispositivo where dispositivo=#dispositivo and event=#event"() {
        expect:
        fsmSenderToQueue.sendMessageForChangeStatoDispositivo(dispositivo, event)
        assert expectedResult  //todo - validate something

        where:
        dispositivo                                       | event                                                                  || expectedResult
        new Dispositivo(identificativo: "ABCD") | it.fai.ms.efservice.service.fsm.enumeration.DispositivoEvent.ACCETTATO || true
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
