docker run -d --rm -p 9000:9000 \
    -e KAFKA_BROKERCONNECT=192.168.1.4:9092 \
    -e JVM_OPTS="-Xms32M -Xmx64M" \
    -e SERVER_SERVLET_CONTEXTPATH="/" \
    obsidiandynamics/kafdrop
