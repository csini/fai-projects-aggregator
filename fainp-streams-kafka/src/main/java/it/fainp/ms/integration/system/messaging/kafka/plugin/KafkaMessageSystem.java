package it.fainp.ms.integration.system.messaging.kafka.plugin;

import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;

import java.util.concurrent.ExecutionException;

public interface KafkaMessageSystem {
    void sendTopicMessage(String topicName, FaiGatewayMessage message) throws ExecutionException, InterruptedException;
}
