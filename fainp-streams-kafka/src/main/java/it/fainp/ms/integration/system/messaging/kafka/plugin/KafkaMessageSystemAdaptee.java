package it.fainp.ms.integration.system.messaging.kafka.plugin;

import it.fainp.ms.integration.system.messaging.kafka.gateway.PublishGateway;
import it.fainp.ms.integration.system.messaging.kafka.gateway.PublishGatewayImpl;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;

import javax.inject.Inject;
import java.util.concurrent.ExecutionException;

public class KafkaMessageSystemAdaptee implements KafkaMessageSystem {
    
    private PublishGateway publisher;
    
    public KafkaMessageSystemAdaptee() {
        this(new PublishGatewayImpl());
    }
    
    @Inject
    public KafkaMessageSystemAdaptee(PublishGateway publisher) {
        this.publisher = publisher;
    }
    
    @Override
    public void sendTopicMessage(String topicName, FaiGatewayMessage message) throws ExecutionException, InterruptedException {
        publisher.doPublish(topicName, message);
    }
}
