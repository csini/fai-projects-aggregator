package it.fainp.ms.integration.system.messaging.kafka.gateway;

import it.fainp.ms.integration.system.messaging.kafka.AbstractScenario;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import it.fainp.ms.integration.system.messaging.kafka.serde.FaiGatewayMessageSerde;
import it.fainp.ms.integration.system.messaging.kafka.util.logging.LambdaLogger;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class PublishGatewayImpl extends AbstractScenario
    implements PublishGateway {
  
  private static final LambdaLogger log = LambdaLogger.getLogger(PublishGatewayImpl.class);
  protected String sourceTopic;
  protected String destTopic;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy");
  private String gatewayName = "1";
  
  public PublishGatewayImpl() {
    super();
  }
  
  
  private KafkaSender<String, FaiGatewayMessage> buildSender() {
    SenderOptions producerOptions = super.senderOptions()
        .producerProperty(ProducerConfig.ACKS_CONFIG, "all");
    
    return KafkaSender.create(producerOptions);
    
  }
  
  @Override
  public void doPublish(String sourceTopic, FaiGatewayMessage message) throws ExecutionException, InterruptedException {
    //int count = 10;
    //flux(message).blockLast();
    //CountDownLatch latch = new CountDownLatch(count);
    sendMessages(sourceTopic, message);
    //latch.await(5, TimeUnit.SECONDS);
  }
  
  @Override
  protected ReceiverOptions<String, FaiGatewayMessage> receiverOptions() {
    return super.receiverOptions()
        .consumerProperty(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
  }
  
  protected SenderOptions<String, FaiGatewayMessage> senderOptions() {
    Map<String, Object> props = new HashMap<>();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "sample-producer");
    props.put(ProducerConfig.ACKS_CONFIG, "all");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, FaiGatewayMessageSerde.instance().serializer().getClass());
    return SenderOptions.create(props);

        /*
        return super.senderOptions()
                .producerProperty(ProducerConfig.ACKS_CONFIG, "all")
                .producerProperty(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "TransactionalSend");
        */
  }
  
  
  private Flux<SenderResult<Integer>>
  sendAndCommit(Flux<ConsumerRecord<String, FaiGatewayMessage>> flux,
                KafkaSender<String, FaiGatewayMessage> sender) {
    return sender.send(flux.map(r -> SenderRecord.<String, FaiGatewayMessage, Integer>create(transform("1", r.value()),
        Integer.valueOf("1"))))
        .concatWith(sender.transactionManager().commit());
  }
  
  private ProducerRecord<String, FaiGatewayMessage> transform(String key, FaiGatewayMessage value) {
    //noinspection NamedArgsPositionMismatch
    
    return new ProducerRecord<String, FaiGatewayMessage>(destTopic, key, value);
  }
  
  protected Flux<?> flux(FaiGatewayMessage message) {
    
    try (
        FaiKafkaSender sender =
            new FaiKafkaSender(buildSender(senderOptions()));
    ) {
      ReceiverOptions<String, FaiGatewayMessage> receiverOptions =
          receiverOptions(Collections.singleton(sourceTopic));
      KafkaReceiver<String, FaiGatewayMessage> receiver = KafkaReceiver.create(receiverOptions);
      return receiver.receiveExactlyOnce(sender.transactionManager())
          .concatMap(f -> sendAndCommit(f, sender))
          .onErrorResume(e -> sender.transactionManager().abort().then(Mono.error((Throwable) e)))
          .doOnCancel(() -> close());
    }
  }
  
  protected void sendMessages(String topic,
                              FaiGatewayMessage message) throws InterruptedException {
    
    try (
        FaiKafkaSender sender =
            new FaiKafkaSender(buildSender(senderOptions()));
    ) {
      sender.<Integer>send(Flux.just(message)
          .map(i -> SenderRecord.create(new ProducerRecord<String, FaiGatewayMessage>(topic, i.getId(), message), i.getId())))
          .doOnError(e -> log.error("Send failed", e))
          .subscribe(r -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SSS z dd MMM yyyy");
            RecordMetadata metadata = r.recordMetadata();
            System.out.printf("Message %s sent successfully, topic-partition=%s-%d offset=%d timestamp=%s\n",
                r.correlationMetadata(),
                metadata.topic(),
                metadata.partition(),
                metadata.offset(),
                dateFormat.format(new Date(metadata.timestamp())));
            //latch.countDown();
          });
    }
  }
  
  protected void close() {
    log.info("...Closeing");
        /*
        if (sender != null)
            sender.close();
        for (Disposable disposable : disposables)
            disposable.dispose();

         */
  }
}
