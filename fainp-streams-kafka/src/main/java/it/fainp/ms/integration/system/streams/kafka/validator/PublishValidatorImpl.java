package it.fainp.ms.integration.system.streams.kafka.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublishValidatorImpl implements PublishValidator {

    private Logger LOGGER = LoggerFactory.getLogger(PublishValidatorImpl.class);


    @Override
    public void calculateResult(PublishEvent publishEvent) {
        LOGGER.info("Processing " + publishEvent.getType() + " " +
                publishEvent.getMessage());
    }
}
