package it.fainp.ms.integration.system.streams.kafka.validator;

public class PublishEvent {
    private String id;
    private String type;
    private String message;
    private String gateway = "1";

    public PublishEvent(String id, String type, String message, String gateway) {
        this.id = id;
        this.type = type;
        this.message = message;
        this.gateway = gateway;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }




    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }



}
