package it.fainp.ms.integration.system.messaging.kafka.pojos;

import java.util.UUID;

/**
 * Envelop the message to send
 */
public class FaiGatewayMessage {

    private String id;
    /**
     * Category of the event generating the messsage to send
     */
    private String type = "NONE";
    /**
     * ID of the client sending the message
     */
    private String clientId;

    /**
     * Message to send
     */
    private Object message;

    public FaiGatewayMessage() {
        this.id=UUID.randomUUID().toString();
    }

    public FaiGatewayMessage(String id, String type, Object message) {
        this.id = id;
        this.type = type;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
