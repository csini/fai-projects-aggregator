package it.fainp.ms.integration.system.messaging.kafka;

import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import it.fainp.ms.integration.system.messaging.kafka.serde.FaiGatewayMessageSerde;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractScenario {
    private static final Logger log = LoggerFactory.getLogger(AbstractScenario.class);

    //private static final String BOOTSTRAP_SERVERS = "192.168.1.4:9092";
    protected static final String BOOTSTRAP_SERVERS = "localhost:9092";

    String groupId = "sample-group";

    protected SenderOptions<String, FaiGatewayMessage> senderOptions() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "sample-producer");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        //If the request fails, the producer can automatically retry,
        props.put(ProducerConfig.RETRIES_CONFIG,0);
        //Specify buffer size in config
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
        //Reduce the no of requests less than 0
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);

        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, FaiGatewayMessageSerde.instance().serializer().getClass());
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.4:9092");

        return SenderOptions.create(props);
    }

    protected ReceiverOptions<String, FaiGatewayMessage> receiverOptions() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "sample-consumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, Serdes.String().deserializer().getClass());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                FaiGatewayMessageSerde.instance().deserializer().getClass());
        return ReceiverOptions.<String, FaiGatewayMessage>create(props);
    }

    public ReceiverOptions<String, FaiGatewayMessage> receiverOptions(Collection<String> topics) {
        return receiverOptions()
                .addAssignListener(p -> log.info("Group {} partitions assigned {}", groupId, p))
                .addRevokeListener(p -> log.info("Group {} partitions assigned {}", groupId, p))
                .subscription(topics);
    }

    protected KafkaSender<String, FaiGatewayMessage> buildSender(
            SenderOptions<String, FaiGatewayMessage> senderOptions) {
        KafkaSender sender = KafkaSender.create(senderOptions);
        return sender;
    }
}
