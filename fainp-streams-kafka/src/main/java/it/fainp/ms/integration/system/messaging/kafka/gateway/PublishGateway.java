package it.fainp.ms.integration.system.messaging.kafka.gateway;

import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;

import java.util.concurrent.ExecutionException;

public interface PublishGateway {

    void doPublish(String sourceTopic, FaiGatewayMessage message) throws ExecutionException, InterruptedException;
}
