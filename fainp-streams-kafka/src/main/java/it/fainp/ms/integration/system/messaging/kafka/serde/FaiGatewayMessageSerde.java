package it.fainp.ms.integration.system.messaging.kafka.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public class FaiGatewayMessageSerde {

    private FaiGatewayMessageSerde() {
    }

    public  static Serde<FaiGatewayMessage> instance(){
        return Serdes.serdeFrom(new FaiGatewayMessageSerializer(),new FaiGatewayMessageDeserializer());
    }

    public  static Serde<FaiGatewayMessage> instanceEx(){
        Serde<FaiGatewayMessage> serdeObject = SerdeUtils.instance().buildBasicSerde(
                (topic,obj) ->{
                    byte[] retVal = null;
                    ObjectMapper objectMapper = new ObjectMapper();
                    try {
                        retVal = objectMapper.writeValueAsString(obj).getBytes();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return retVal;
                },
                (topic,bytes) -> {
                    ObjectMapper mapper = new ObjectMapper();
                    FaiGatewayMessage obj = null;
                    try {
                        obj = mapper.readValue(bytes, FaiGatewayMessage.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return obj;
                }
        );
        return serdeObject;
    }
}
