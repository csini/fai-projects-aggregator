package it.fainp.ms.integration.system.streams.kafka.validator;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import reactor.core.publisher.Flux;
import reactor.kafka.receiver.KafkaReceiver;
import reactor.kafka.receiver.ReceiverOptions;
import reactor.kafka.receiver.ReceiverRecord;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class PublishValidatorListenerImpl {

    private static final Logger log = LoggerFactory.getLogger(PublishValidatorListenerImpl.class.getName());

    private KafkaReceiver kafkaReceiver;
    private Random r = new Random();
    private ObjectMapper objectMapper = new ObjectMapper();
    private PublishValidator publishValidator;
    public PublishValidatorListenerImpl(PublishValidator publishValidator){
        this.publishValidator = publishValidator;

        final Map<String, Object> consumerProps = new HashMap<>();
        consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
        consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerProps.put(ConsumerConfig.CLIENT_ID_CONFIG, "payment-validator-1");
        consumerProps.put(ConsumerConfig.GROUP_ID_CONFIG, "payment-validator");
        consumerProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.1.4:9092");

        ReceiverOptions<Object, Object> consumerOptions = ReceiverOptions.create(consumerProps)
              .subscription(Collections.singleton("unconfirmed-transactions"))
              .addAssignListener(partitions -> log.debug("onPartitionsAssigned {}", partitions))
              .addRevokeListener(partitions -> log.debug("onPartitionsRevoked {}", partitions));

            kafkaReceiver = KafkaReceiver.create(consumerOptions);

            /**
             * We create a receiver for new unconfirmed transactions
             */
            ((Flux<ReceiverRecord>) kafkaReceiver.receive())
                    .doOnNext(r -> {
                        /**
                         * Each unconfirmed payment we receive, we convert to a PaymentEvent and process it
                         */
                        final PublishEvent publishEvent = fromBinary((String) r.value(), PublishEvent.class);
                        processEvent(publishEvent);
                        r.receiverOffset().acknowledge();
                    })
                    .subscribe();
        }

        private void processEvent(PublishEvent paymentEvent) {
            publishValidator.calculateResult(paymentEvent);
        }

        private <T> T fromBinary(String object, Class<T> resultType) {
            try {
                return objectMapper.readValue(object, resultType);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }


