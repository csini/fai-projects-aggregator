package it.fainp.ms.integration.system.messaging.kafka.spi;

import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import it.fai.ms.integration.system.sock.api.spi.FaiMessageSystemProvider;
import it.fainp.ms.integration.system.messaging.kafka.plugin.FaiMessageSystemAdaptor;

public class KafkaMessageSystemProvider
      implements FaiMessageSystemProvider {
  @Override
  public FaiMessageSystem get() {
    return new FaiMessageSystemAdaptor();
  }
}
