package it.fainp.ms.integration.system.messaging.kafka.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class FaiGatewayMessageSerializer implements Serializer<FaiGatewayMessage> {
    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, FaiGatewayMessage faiGatewayMessage) {
        byte[] retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            retVal = objectMapper.writeValueAsString(faiGatewayMessage).getBytes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retVal;

    }

    @Override
    public void close() {

    }
}
