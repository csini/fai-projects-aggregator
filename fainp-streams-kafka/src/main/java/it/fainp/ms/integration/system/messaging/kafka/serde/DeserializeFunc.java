package it.fainp.ms.integration.system.messaging.kafka.serde;


    @FunctionalInterface
    public interface DeserializeFunc<T> {
        T deserialize(final String topic, final byte[] bytes);
    }

