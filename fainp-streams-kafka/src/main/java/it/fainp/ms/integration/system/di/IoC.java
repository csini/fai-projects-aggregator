package it.fainp.ms.integration.system.di;

import javax.inject.Inject;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class IoC {
    private static Map<Class<?>, Class<?>> types = new HashMap<>(2);

    public static void register(Class<?> contract, Class<?> implementation) {
        types.put(contract, implementation);
    }

    public static <T> T resolve(Class<T> contract)
            throws InstantiationException,
            IllegalAccessException,
            InvocationTargetException {
        Class<?> impl = types.get(contract);
        Constructor<?>[] constructors = impl.getConstructors();
        AtomicReference<Constructor<?>> defConstr = new AtomicReference<>(null);

        AtomicReference<T> obj = new AtomicReference<T>();
        Stream.of(constructors).forEach(c -> {
            T value = null;
            try {
                Annotation a = c.getAnnotation(Inject.class);
                Parameter[] parray = c.getParameters();
                if (parray.length == 0) {
                    defConstr.set(c);
                } else {
                    Object[] args = new Object[parray.length];

                    for (int i = 0; i < parray.length; i++) {
                        Class pclass = parray[i].getType();
                        Object inst = resolve(pclass);
                        args[i] = inst;
                    }
                    value = (T) c.newInstance(args);

                    if (value == null && defConstr.get() != null)
                        value = (T) defConstr.get().newInstance();
                }
                obj.set(value);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        });
        return obj.get();
    }

    ;


}
