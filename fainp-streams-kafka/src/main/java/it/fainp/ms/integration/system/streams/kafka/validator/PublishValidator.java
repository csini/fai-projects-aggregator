package it.fainp.ms.integration.system.streams.kafka.validator;

import it.fainp.ms.integration.system.streams.kafka.validator.PublishEvent;


public interface PublishValidator {
    void calculateResult(PublishEvent paymentEvent);
}
