package it.fainp.ms.integration.system.messaging.kafka.serde;

import it.fainp.ms.integration.system.messaging.kafka.util.bytes.ByteArrayUtils;
import it.fainp.ms.integration.system.messaging.kafka.util.logging.LambdaLogger;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class SerdeUtils {

    private static final LambdaLogger LOGGER = LambdaLogger.getLogger(SerdeUtils.class);

    private static SerdeUtils utils = null;
    private SerdeUtils() {
    }

    public static SerdeUtils instance(){
        if(utils == null)
            utils = new SerdeUtils();
        return utils;
    }

    /**
     * Creates a basic serializer using the passed stateless function and not implementing close or configure
     *
     * @param serializeFunc The function to serialize T to a byte[]
     * @param <T>           The type of object to serialize
     * @return A byte[] representation of T
     */
    public <T> Serializer<T> buildBasicSerializer(final SerializeFunc<T> serializeFunc) {
        return new Serializer<T>() {

            @Override
            public void configure(Map<String, ?> map, boolean b) {

            }

            @Override
            public byte[] serialize(final String topic, final T data) {
                return serialize(topic, data);
            }

            @Override
            public void close() {
            }
        };
    }

    /**
     * Builds a Deserializer of T with the passed stateless function and no configure or close implementations
     */
    public <T> Deserializer<T> buildBasicDeserializer(final DeserializeFunc<T> deserializeFunc) {
        return new Deserializer<T>() {
            @Override
            public void configure(final Map<String, ?> configs, final boolean isKey) {
            }

            @Override
            public T deserialize(final String topic, final byte[] bData) {
                return deserializeFunc.deserialize(topic, bData);
            }

            @Override
            public void close() {
            }
        };
    }

    /**
     * Builds a Serde for T using a basic Serializer and Deserializer that do not implement configure or close
     */
    public <T> Serde<T> buildBasicSerdeEx(final SerializeFunc<T> serializeFunc, final DeserializeFunc<T> deserializeFunc) {
        return Serdes.serdeFrom(buildBasicSerializer(serializeFunc), buildBasicDeserializer(deserializeFunc));
    }

    public <T> Serde<T> buildBasicSerde(final SerializeFunc<T> serializeFunc, final DeserializeFunc<T> deserializeFunc) {
        return Serdes.serdeFrom(buildBasicSerializer(serializeFunc), buildBasicDeserializer(deserializeFunc));
    }

    /**
     * Helper method for testing that a serde can serialize its object and deserialize it
     * back again. Throws a {@link RuntimeException } if the objects don't match.
     * @param serde
     * @param obj
     * @param <T>
     */
    public <T> void verify(final Serde<T> serde, final T obj) {
        final String dummyTopic = "xxx";
        byte[] bytes = serde.serializer().serialize(dummyTopic, obj);

        LOGGER.trace(() -> String.format("object form: %s", obj));
        LOGGER.trace(() -> String.format("byte form: %s", ByteArrayUtils.byteArrayToHex(bytes)));

        T deserializedObj = serde.deserializer().deserialize(dummyTopic, bytes);
        if (!obj.equals(deserializedObj)) {
            throw new RuntimeException(String.format("Original [%s] and de-serialized [%s] values don't match", obj, deserializedObj));
        }
    }
}
