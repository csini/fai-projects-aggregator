package it.fainp.ms.integration.system.messaging.kafka.plugin;


import it.fai.ms.integration.system.sock.api.interfaces.FaiMessageSystem;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class FaiMessageSystemAdaptor implements FaiMessageSystem {
    private KafkaMessageSystem adaptee;

    public FaiMessageSystemAdaptor() {
        this(new KafkaMessageSystemAdaptee());
    }
    
    @Inject
    public FaiMessageSystemAdaptor(KafkaMessageSystem adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void publishMessage(String s, Serializable serializable) {
        FaiGatewayMessage message = new FaiGatewayMessage();
        message.setId(UUID.randomUUID().toString());
        message.setType("NONE");
        message.setMessage(serializable);
        try {
            adaptee.sendTopicMessage(s,message);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
