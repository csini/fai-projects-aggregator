package it.fainp.ms.integration.system.messaging.kafka.serde;


    @FunctionalInterface
    public interface SerializeFunc<T> {
        byte[] serialize(final String topic, final T obj);
    }

