package it.fainp.ms.integration.system.messaging.kafka.gateway;

import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaOutbound;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderResult;
import reactor.kafka.sender.TransactionManager;

import java.io.Closeable;
import java.io.IOException;
import java.util.function.Function;

public class FaiKafkaSender implements Closeable, KafkaSender {
    KafkaSender<String, FaiGatewayMessage> sender;

    public FaiKafkaSender(KafkaSender<String, FaiGatewayMessage> sender) {
        this.sender = sender;
    }

    @Override
    public Flux<SenderResult> send(Publisher publisher) {
        return sender.send(publisher);
    }

    @Override
    public Flux<Flux<SenderResult>> sendTransactionally(Publisher publisher) {
        return sender.sendTransactionally(publisher);
    }

    @Override
    public TransactionManager transactionManager() {
        return sender.transactionManager();
    }

    @Override
    public KafkaOutbound createOutbound() {
        return sender.createOutbound();
    }

    @Override
    public Mono doOnProducer(Function function) {
        return sender.doOnProducer(function);
    }

    @Override
    public void close() {
        if(sender != null){
            sender.close();
            sender = null;
        }
    }
}
