package it.fainp.ms.integration.system.messaging.kafka.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.fainp.ms.integration.system.messaging.kafka.pojos.FaiGatewayMessage;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class FaiGatewayMessageDeserializer implements Deserializer<FaiGatewayMessage> {
    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public FaiGatewayMessage deserialize(String s, byte[] bytes) {
        ObjectMapper mapper = new ObjectMapper();
        FaiGatewayMessage obj = null;
        try {
            obj = mapper.readValue(bytes, FaiGatewayMessage.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }



    @Override
    public void close() {

    }
}
