package it.fainp.ms.integration.system.messaging.kafka.serde;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serde;

public class JsonObjectSerde {

    private JsonObjectSerde() {
    }

    public  static Serde<Object> instance(){
        Serde<Object> serdeObject = SerdeUtils.instance().buildBasicSerde(
                (topic,obj) ->{
                    byte[] retVal = null;
                    ObjectMapper objectMapper = new ObjectMapper();
                    try {
                        retVal = objectMapper.writeValueAsString(obj).getBytes();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return retVal;
                },
                (topic,bytes) -> {
                    ObjectMapper mapper = new ObjectMapper();
                    Object obj = null;
                    try {
                        obj = mapper.readValue(bytes, Object.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return obj;
                }
        );
        return serdeObject;
    }
}
