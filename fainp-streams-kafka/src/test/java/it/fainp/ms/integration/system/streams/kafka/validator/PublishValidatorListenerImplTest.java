package it.fainp.ms.integration.system.streams.kafka.validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import reactor.kafka.receiver.KafkaReceiver;

import java.util.Random;

import static org.mockito.Mockito.*;

class PublishValidatorListenerImplTest {
    @Mock
    Logger log;
    @Mock
    KafkaReceiver kafkaReceiver;
    @Mock
    Random r;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    PublishValidator publishValidator;
    @InjectMocks
    PublishValidatorListenerImpl publishValidatorListenerImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme
